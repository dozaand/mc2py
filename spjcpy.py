# -*- coding:utf-8 -*-
import argparse
import os,re
from pathlib import Path
import shutil

"""copy input file to directory"""

def load_spj(p:Path):
    spjout=p.stem
    with open(p,"r",encoding="cp1251") as f:
        data = f.read()
    filelist = [Path(i) for i in re.findall(r"file=(.+)\x1d",data)]
    dirs = set([i.parent for i in filelist])



    for i in sorted(dirs):
        try:
            os.makedirs(spjout/i)
        except:
            pass

    for i in filelist:
        shutil.copy(i,spjout/i)







if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='conf')
    parser.add_argument("-o",'--outdir', default = "spjout", help='copy only sources')
    parser.add_argument("spj", help='spj file')
    parser.add_argument("-i",'--inplace', action="store_true", help='remove from dirs all files not listed in spj')
    args = parser.parse_args()
    load_spj(Path(args.spj))
