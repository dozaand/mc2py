#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
�������� ������ ������ ����� 7zip
"""
import subprocess as sp
import cPickle
import os
import random


def Randnm():
    while 1:
        s = str(random.randint(0, 2000000000))+".7z"
        if not os.path.exists(s):
            break
    return s


def Compress(data):
    u"""�������� ������ 7z"""
    fnm = Randnm()
    z7 = sp.Popen("7z a -si -mmt -m0=LZMA2 %s" % fnm, shell=1, stdin=sp.PIPE)
    z7.communicate(data)
    with open(fnm, "rb") as f:
        res = f.read()
    os.remove(fnm)
    return res


def CompressObj(data):
    u"""�������� ������� 7z"""
    s = cPickle.dumps(data, 2)
    sc = Compress(s)
    return sc


def DeCompress(data):
    u"""���������� ������ 7z"""
    fnm = Randnm()
    with open(fnm, "wb") as f:
        f.write(data)
    try:
        z7 = sp.Popen("7z e -so -mmt %s" %
                      fnm, shell=1, stdout=sp.PIPE, stdin=sp.PIPE)
        res = z7.communicate()
    finally:
        os.remove(fnm)
    return res[0]


def DeCompressObj(data):
    u"""���������� ������� 7z"""
    sc = DeCompress(data)
    s = cPickle.loads(sc)
    return s


# import numpy as np
# a=np.random.normal(0,1,1000)
# sc=CompressObj(a)
# a1=DeCompressObj(sc)
