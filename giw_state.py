#!/bin/env/python
# -*- coding:utf-8 -*-

import argparse
from mc2py.giw_util import Tmfa_file

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="giw", description="copy data to clipboard")
    parser.add_argument("--vpt", "-v",  help=u"vpt file")
    parser.add_argument("--output", "-o", help=u"output file name")
    parser.add_argument("file", help=u"cat command line to clopboard if no args - clear clipboard")
    args = parser.parse_args()
    fil = Tmfa_file(args.file,args.vpt)
    fil.saveH5(args.output)
    


