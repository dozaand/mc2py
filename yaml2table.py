#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
extract table data from word document
command: yaml2table.py -o x.docx -c totbl.yaml a1.yaml

totbl.yaml:
cols : # Какие колонки вывести в doc
  - N  # Автонумерация
  - Задача
  - Срок
  - Отв. исполнители
  - Соисполнители
  - Примечания
section :
  orientation : landscape

"""

from docx import Document
from docx.enum.section import WD_ORIENT
from docx.shared import Cm
import yaml
import argparse

def data2docx(docname, table_data, cfg):
    doc = Document()
    try:
        section = doc.sections[-1]
        section.orientation = {"portrait":WD_ORIENT.PORTRAIT,
                               "landscape":WD_ORIENT.LANDSCAPE,
                               0:WD_ORIENT.PORTRAIT,
                               1:WD_ORIENT.LANDSCAPE}[cfg["section"]["orientation"]]
        if section.orientation:
            section.page_width,section.page_height = section.page_height,section.page_width
#        section.left_margin=Cm(2)
#        section.right_margin = Cm(1)
    except:
        pass
    n_row = sum([len(i) for i in table_data])+1
    cols = cfg["cols"]
    n_col=len(cols)
    tbl = doc.add_table(n_row,n_col)
    tbl.allow_autofit = True
    tbl.autofit = True
    tbl.style = 'Table Grid'
    for i,txt in enumerate(cols):
        tbl.rows[0].cells[i].text=txt
    irow=0
    for iblock, block in enumerate(table_data):
        for isect, dat in enumerate(block):
            row=tbl.rows[irow+1]
            try:
                if hasattr(dat, "keys"):
                    for i, v in enumerate(cols):
                        if v ==  "N":
                            row.cells[i].text = "{0}.{1}".format(iblock,isect)
                        else:
                            row.cells[i].text = str(dat[v])
                else:
                    c0 = row.cells[0]
                    c1 = row.cells[-1]
                    cm = c0.merge(c1)
                    cm.text = str(dat)
                    cm.paragraphs[0].runs[0].bold = True
            except:
                pass
            irow+=1
    doc.save(docname)

#data2docx(docname, table_data, cfg)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="extract table from ")
    parser.add_argument("-o", "--output", help=u"output file name")
    parser.add_argument("-c", "--config", help=u"configuration file")
    parser.add_argument("file", help="input file name")
    args = parser.parse_args()
    with open(args.file, "rt", encoding="utf-8") as f:
        table_data = list(yaml.load_all(f))
    with open(args.config, "rt", encoding="utf-8") as f:
        cfg = yaml.load(f)
    data2docx(args.output, table_data, cfg)
