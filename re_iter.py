#!/bin/env/python
# -*- coding: utf-8 -*-
u"""
seqviental regular expression search
"""

import re
import numpy as np
import codecs

is_float=re.compile(r"[ \t\n]*([-+]?\d*\.?\d+([eE][-+]?\d+)?)",re.M)
is_int = re.compile(r"[ \t\n]*([-+]?([0-9][xob])?\d+)",re.M)

class ReIter(object):
    u"""итератор для поиска данных в заданном массиве"""
    def __init__(self,file_name=None,data=None,encoding="utf-8",nothrow=0):
        self.encoding=encoding
        self.nothrow=nothrow
        if file_name:
            self.read(file_name)
        if data:
            self.data=data
            self.pos=0

    def iter(self,rexp,f=None,igrp=1,skip=None,maxcount=None):
        if not hasattr(rexp,"match"):
            rexp=re.compile(rexp)
        if skip and not hasattr(skip,"match"):
            skip=re.compile(skip)
        data,pos=self.data,self.pos
        ld=len(data)
        if maxcount is None:
            maxcount=4000000000
        count=0
        while pos<ld:
            if count >= maxcount:
                self.pos=pos
                return
            count+=1
            if skip:
                fnd=skip.match(data,pos)
                if not fnd is None:
                    pos=fnd.end()
            fnd=rexp.match(data,pos)
            if fnd:
                if f:
                    grp=fnd.groups()
                    foundstr = grp[igrp-1] if grp else fnd.group(0)
                    yield f(foundstr)
                else:
                    yield fnd
                pos=fnd.end()
            else:
                self.pos=pos
                return

    def endl(self,f=None):
        u"""skip to end of line"""
        return self.match(r".*\n",f=f)

    def array(self,rexp=is_float,dtype='f',igrp=1,skip=None,maxcount=None):
        u"""parse list of items of form (skip?rexp)+"""
        return np.array(list(self.iter(rexp,float,igrp,skip,maxcount)),dtype=dtype)

    def list(self,rexp,f=None,igrp=1,skip=None,maxcount=None):
        u"""parse list of items of form (skip?rexp)+"""
        return list(self.iter(rexp,f,igrp,skip,maxcount))

    def match(self,rexp,f=None,igrp=1,flags=None):
        u"""find next item from current position"""
        data,pos=self.data,self.pos
        if not hasattr(rexp,"match"):
            if flags:
                rexp=re.compile(rexp,flags)
            else:
                rexp=re.compile(rexp)
        fnd=rexp.match(data,pos)
        if fnd is None:
            if self.nothrow:
                return None
            raise Exception("not found")
        self.pos=fnd.end()
        if f:
            grp=fnd.groups()
            foundstr = grp[igrp-1] if grp else fnd.group(0)
            return f(foundstr)
        else:
            return fnd

    def search(self,rexp,f=None,igrp=1,flags=None):
        u"""find next item from current position"""
        data,pos=self.data,self.pos
        if not hasattr(rexp,"search"):
            if flags:
                rexp=re.compile(rexp,flags)
            else:
                rexp=re.compile(rexp)
        fnd=rexp.search(data,pos)
        if fnd is None:
            if self.nothrow:
                return None
            raise Exception("not found")
        self.pos=fnd.end()
        if f:
            grp=fnd.groups()
            foundstr = grp[igrp-1] if grp else fnd.group(0)
            return f(foundstr)
        else:
            return fnd

    def int(self):
        return self.search(is_int,int)

    def float(self):
        return self.search(is_float,float)

    def head(self,l=50):
        u"""return head of current position"""
        return self.data[self.pos:self.pos+l]

    def read(self,nm):
        with codecs.open(nm,"r",encoding=self.encoding) as f:
            file_data = f.read().replace("\r\n","\n")
        self.data=file_data
        self.pos=0

    def seek(self,pos=0):
        self.pos=pos

    def empty(self):
        return self.pos>=len(self.data)
