#!/usr/bin/env python
# -*- coding: cp1251 -*-

import cPickle
# try:
from mc2py.mopc import *
# except:
#    raise Exception("fail to import giw - try to run giw exe")
import numpy as np
import re
import mc2py.fold
import random
import time
from copy import copy
import h5py

# ������� ���� ����������
import mc2py.bgk as bgk


class ModelError(Exception):
   def __init__(self, txt):
      Exception.__init__(self, txt)

def h5py_save(file_name,var):
    u"""dump opc data to h5py file
    file_name - ��� ����� hdf5, ���� ��������� ������
    var - ������ ����������, ������� ����� ������� � hdf5,
    ���� var == "*", �� ����� �������� � ���� ��� ������������������ � ������� ������ ����������
    """
    if var=="*":
        var = Tmfa.opc.list('*')
    with h5py.File(file_name,"w") as f:
        for (nm,val,status,time) in Tmfa.opc.iread(var,sync=1):
            if status=="Good":
                try:
                    f[nm]=np.array(val)
                except:
                    pass

def genh5contents(f5):
    for k,v in f5.iteritems():
        data=v[...]
        if len(data.shape):
            if(len(data.shape)==2):
                yield (k,data.T)
            else:
                yield (k,data)
        else:
            yield (k,data.tolist())

def h5py_load(file_name):
    u"""load from h5py to model"""
    with h5py.File(file_name,"r") as f5:
        list(Tmfa.opc.iwrite(list(genh5contents(f5))))

class giwstate:
    _alloc = 1
    _free = 2
    _fix = 3
    _restore = 4

    def __init__(self):
        v.YMGIWSTATECMD = giwstate._alloc
        RET()
        self.id = v.YMGIWSTATEID

    def fix(self):
        v.YMGIWSTATEID = self.id
        v.YMGIWSTATECMD = giwstate._fix
        RET()

    def restore(self):
        v.YMGIWSTATEID = self.id
        v.YMGIWSTATECMD = giwstate._restore
        RET()


def BurnTo(t, w=100):
    """��������� �� �������� ����������� ����� �������� ��� �������� w"""
    v.YMWSTATN = w
    v.YMFLAGSTAT = 1
    v.YZBORMODE = 2
    oldfast = v.YMFAST
    v.YMFAST = 50000
    v.YMFLAG_BRN = 1
    SetHgrp3([1, 1, 1])
    RET()
    while v.YMTIME_BRN < t and v.YMBOR_COR > 0.1:
        RET()
    v.YMFLAG_BRN = 0
    v.YMFAST = oldfast
    if v.YMTIME_BRN < t:
        print v.YMBOR_COR, v.YMTIME_BRN, t
        raise Exception("fail burn to")

def ModelStart():
    u"""��������� ������
    """
    while v["$_MODELISSTOPPED"]:
        v.OPCCMD=12 #start model
        time.sleep(0.01)

def ModelStop():
    u"""������������� ������
    """
    while v["$_MODELISSTOPPED"]==False:
        v.OPCCMD=13 #stop model
        time.sleep(0.01)

def ModelWait(condition_line,sleeptime=0.05):
    u"""������� ���������� ������� �������, ����� ������������� ������
    condition_line - ������� � ���� �������
    sleeptime=0.05 - ��� �� �������, ������� ��������� � ������
    """
    ModelStart()
    while not eval(condition_line):
        time.sleep(sleeptime)
    ModelStop()

def ModelLocks(On=True):
    u"""����(��� �����) ���������� � �����"""
    if On:
        v.HB113T1_KEY=1 #��������� ��1
        v.HB113T55_KEY=1 #��������� ��3
        v.HB113T77_KEY=1 #��������� ��4
        v.HB113T65_KEY=1 #��������� ���
        v.HB113T68_KEY=1 #��������� ���
        v.reset_all=1 #����� ���� ������������
    if On==False:
        v.HB113T1_KEY=0 #����������� ��1
        v.HB113T55_KEY=0 #����������� ��3
        v.HB113T77_KEY=0 #����������� ��4
        v.HB113T65_KEY=0 #����������� ���
        v.HB113T68_KEY=0 #����������� ���
        v.reset_all=1 #����� ���� ������������


def ChooseNKSYA(mod="NKS"):
    u"""�������� ����� ������ YA ��� NKS
    mod = "YA" - ������ � ���������� ��������� �� ������� � ���������
    mod = "NKS" - ������ � ���������� ��������� �� ������� ��������
    """
    if mod=="YA" and v.YZTABLINKNKS:
        v.YZKEYLINK2+=1
        print u"���������� ������ � ����� YA"
        ModelWait("v.YZTABLINK1")
        print u"���������� ������ � ����� YA"
    elif mod=="NKS" and v.YZTABLINKNKS==False:
        v.YZKEYLINK2+=1
        print u"���������� ������ � ����� NKS"
        ModelWait("v.YZTABLINKNKS")
        print u"���������� ������ � ����� NKS"

def RelaxState():
    u"""������ ���������"""
    perem=["HB113T1_KEY","HB113T55_KEY","HB113T77_KEY","HB113T65_KEY","HB113T68_KEY","YMFLAGSTAT","YZBORREG"] #�����
    ModelLocks()
    v.YMFLAGSTAT=1
    SetCbRegulator(1)
    ModelWait("abs(v.YMDRNEW)<0.000001")
    print u'������������=',v.YMDRNEW
    v.YMFLAGSTAT=0
    SetCbRegulator(1)
    v.ASUT_RD1KEY=1
    ModelWait("abs(v.YMDRNEW)<0.00000001")
    RelaxDTDT()
    print u'������������=',v.YMDRNEW
    SetCbRegulator(1,on=0)

def RelaxDTDT(stepmode=False,size=100.):
    u"""������ �� DT1KDT ����������� ������� ����. 1 ������� �� �������"""
    dtmas=np.ones(size)
    if stepmode:
        ModelStop()
        while dtmas.mean()>0.1:
            for i in range(size):
                dtmas[i]=abs(v.DT1KDT)
                RET()
            #print "dtmean=",dtmas.mean()
        print u"������ ���������"
        print "dtmean=",dtmas.mean()
    else:
        v.YMFLAGSTAT=1
        SetCbRegulator(1)
        ModelWait("abs(v.YMDRNEW)<0.000001")
        print u'������������1=',v.YMDRNEW
        v.YMFLAGSTAT=0
        SetCbRegulator(1)
        v.ASUT_RD1KEY=1
        ModelWait("abs(v.YMDRNEW)<0.00000001")
        print u'������������2=',v.YMDRNEW
        dtmas=np.arange(size,step=1)
        dtmas.fill(100.)
        ModelStart()
        while dtmas.max()>0.1:
            if v["$_MODELISSTOPPED"]:
                error_msg = 'Model broke'
                raise ModelError, error_msg
            dtmas[0]=abs(v.DT1KDT)
            dtmas=np.roll(dtmas,1)
            time.sleep(0.1)
            #for i in range(size):
            #    dtmas[i]=abs(v.DT1KDT)
            #    time.sleep(0.1)
            #print "dtmax=",dtmas.max()
        print u"������ ���������"
        print "dtmax=",dtmas.max()
        ModelStop()

def RelaxByVar(var,size=50):
    u"""���� ������ ��������� ����������"""
    #dvmas=np.ones(size)
    #dvmas=np.random.randint(0,200,100)
    dvmas=np.arange(size/10,step=0.1)
    ModelStart()
    while dvmas.std()>0.1:
        dvmas[0]=v[var]
        dvmas=np.roll(dvmas,1)
        time.sleep(0.2)
        #print "dvmas=",dvmas.std()
    #print u"������ ��������� �� ",var.encode("latin1")
    #print "dvmas.std()=",dvmas.std()
    ModelStop()


def MoveTo(H_new, f=None):
    H = GetHgrp3()
    step = 20
    shag = copy(step)
    step = (H_new-H)/step
    while shag != 0:
        H += step
        SetHgrp3(H)
        shag -= 1
        RET()
        if f != None:
            f()


def Nstp(n):
    for i in xrange(n):
        try:
            RET()
        except:
            pass


def Kz():
    """������������� ������������� �������� �� ������"""
    fi = v.YMFISF
    vv = fi.reshape((-1, 10))
    u = np.ones(163, dtype='f')
    r = np.dot(vv.T, u)
    return r/sum(r)


def KzW(Wnom=3e9):
    """������������� �������� �� ������ (�� �� ����)"""
    fi = v.YMFISF
    vv = fi.reshape((-1, 10))
    u = np.ones(163, dtype='f')
    r = np.dot(vv.T, u)
    return r/Wnom


def RelaxCore():
    """������������ ���������� ��� ����"""
    borold = v.YZBORMODE
    v.YZBORMODE = 2
    statold = v.YMFLAGSTAT
    v.YMFLAGSTAT = 1
    i = 0
    fastold = v.YMFAST
    v.YMFAST = 100000
    while abs(v.YMDRNEW) > 3e-6:
        try:
            RET()
        except:
            pass
        i += 1
        if i > 100:
            v.YMFAST = fastold
            try:
                RET()
            except:
                pass
            raise Exception("fail to relax core")
    v.YMFAST = fastold
    v.YMFLAGSTAT = statold
    v.YZBORMODE = borold


def IcamRD():
    """���������� ���� ����� �������� � ����. ���������
    ��������� ������
    ������ ������� - ����� ���������
    ������ ������� - ����� ������
    ������ ������� - ����� ������ (����� �����)
    """
    camcan = [1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 3, 1, 1, 1, 1,
              3, 1, 1, 1, 1, 3, 1, 3, 1, 1, 3]  # ���������� ����� � ������
    complect1 = [2, 12, 22]
    complect2 = [7, 17, 27]
    res = [i for i in mc2py.fold.NSplit(v.YQ_I_AKNP, camcan)]
    return np.array([[res[i-1] for i in complect1], [res[i-1] for i in complect2]])


def SetMFADrive(W=None, hgrp=None, Tin=None, G=None, block=None, kamp=None, plant=None, burn=None, static=1):
    """��������� ��������� ������� ������
    W [%]
    hgrp [%]
    Tin [C]
    G [T/h]
    static - ���� 1 �� ������ ����������� ����� ����� ������ �������� (�� ��� �������)
    """
#    loadedplant=MFAGet("plant_name")[:4]
    doLoadCamp = 0
    needRelax = 0
    if static:
        v.YMFLAGSTAT = 1
    else:
        v.YMFLAGSTAT = 0
        v.YMFAST = 1

    if block:
        if block != v.YMBLOCKNUMBER0:
            doLoadCamp = 1
    else:
        block = v.YMBLOCKNUMBER0

    if kamp:
        if kamp != v.YMLOADNUMBER:
            doLoadCamp = 1
    else:
        kamp = v.YMLOADNUMBER

    if doLoadCamp:
        v.YM_N_KAMP_TO_LOAD = kamp
        v.YMBLOCKNUMBER_TO_LOAD = block
        v.YM_XIPI_LDBRNBEG = 1
        needRelax = 1

    if burn and abs(burn-v.YMTIME_BRN) > 0.2:
        BurnTo(burn)
    if hgrp != None:
        hgrp = np.array(hgrp)
        if static:
            v.YSHGRP = hgrp
            needRelax = 1
        else:
            nstep = int(max(abs(hgrp-v.YSHGRP))/(4*2./355))+1
            if nstep <= 1:
                nstep = 2
            delta = (hgrp-v.YSHGRP)/(nstep-1)
            h0 = v.YSHGRP
            for i in xrange(nstep):
                v.YSHGRP = h0+i*delta
                RET()

    if Tin != None:
        tmp = v.YHTIN_LOOP
        tmp[:4] = np.array(Tin)
        v.YHTIN_LOOP = tmp
        needRelax = 1
    if G != None:
        tmp = v.YHGIN_LOOP
        tmp[:4] = np.array(G)
        v.YHGIN_LOOP = tmp
        needRelax = 1
    if W:
        v.YMWSTATN = W
        v.YMINTPOW_SET = W
        v.YMFLAGSTAT = 1
        v.YZBORMODE = 2
        needRelax = 1
    if static:
        try:
            RET()
        except:
            pass
        if needRelax:
            RelaxCore()
    else:
        v.YMFLAGSTAT = 0
        try:
            RET()
        except:
            pass


# ��������������� ������ �������� ��������������
def GetDp():
    """�������� �������� �� ��� [��]"""
    return np.array([v.Y314B01, v.Y315B01, v.Y316B01, v.Y317B01])*1e5


def GetTout():
    """�������� ����������� �� ������ [C]"""
    return np.array([v.BiX1PETLi_Temp, v.BiX2PETLi_Temp, v.BiX3PETLi_Temp, v.BiX4PETLi_Temp])


def GetTin():
    """������� ����������� �� ������ [C]"""
    return v.yhT_leg


def SetTin(val):
    """������� ����������� �� ������ [C]"""
    v.YHTIN_LOOP = val


def GetG():
    """������ �� ������ [����/���]"""
    return v.yhG_leg


def SetG(val):
    """������ �� ������ [����/���]"""
    v.YHGIN_LOOP = val


def SetHgrp3(v3):
    """��������� ��������� �����"""
    val = np.ones(10, dtype='d')
    val[-3:] = v3[:]
    v.YSHGRP = val


def GetHgrp3():
    """��������� ��������� �����"""
    return v.YSHGRP[-3:]


def RandomRod3():
    """��������� ��������� ��������� ��������"""
    hr = np.random.uniform(0, 1, 3)
    if hr[0] < 0.5:
        hr[0] /= 10
    else:
        v = 1-hr[0]
        hr[0] = 1-v/10
    val = np.ones(10, dtype='d')
    val[-3:] = hr[:]
    return val


class Tbgkdata:
    """��������� ��� ���������� ������ �� BGK"""
    bgkvectrs = """YMFISF YMRP_POW YHTFU_F0 YHTMIX_F0 YMRO_XEN YMRO_JOD YMRO_PRO YMRO_SAM
    YMZPOLD1 YMZPOLD2 YMZPOLD3 YMZPOLD4 YMZPOLD5 YMZPOLD6
    YMRP_C01 YMRP_C02 YMRP_C03 YMRP_C04 YMRP_C05 YMRP_C06 YMRP_C07 YMRP_C08 YMRP_C09 YMRP_C10 YMRP_C11 YMRP_C12""".split()

    def __init__(self):
        self.data = {}
        self.bgk = {}
        for i in self.bgkvectrs:
            self.data[i] = []

    def append(self):
        for i in self.bgkvectrs:
            dat = getattr(v, i)
            self.data[i].append(dat)

    def ClearData(self):
        for i in self.bgkvectrs:
            self.data[i] = []

    def flush(self):
        for i in self.bgkvectrs:
            self.data[i] = []
        self.bgk = {}

    def MakeBgk(self, nv=30):
        """����������� ������ � ���"""
        for i in self.bgkvectrs:
            self.bgk[i] = bgk.Bgk(self.data[i], nv)
            self.data[i] = []
        return self.bgk

    def Errors(self, j=0):
        """������ ������������� ����������� ������� �����"""
        return dict([(i, bgk.NprNorm(self.data[i][j], bgk.Residual(self.bgk[i], self.data[i][j], np.dot))) for i in self.bgkvectrs])


def SetCbRegulator(state,dynamic=0,on=True):
    """��������� ��������� ������� ����������
    state - �������� � �������: True, � ���������: False
    on - �������� ��������� (�� ����� ��� ���)
    dynamic - ����������� ������� �� �������� v.YMINTPOW_SET � �������� (1) ������� �����, ����� ��������� ����� �������������
    """
    if v.YZTABLINKNKS: #���� ����� ���
        if state:
            v.YZBORMODE = 2
            v.YMINTPOW_SET_FLAG = dynamic
        else:
            v.YZBORMODE = 1
            v.YMINTPOW_SET_FLAG = dynamic
    else:   #���� ����� YA
        if on:
            if state:
                v.YZBORREG=1 #�������� ������ ���������
                RET() #��� ���� ������ ���� �� �������� �������������
                v.YZBORMODE = 2 #������ � �������
                v.YMINTPOW_SET_FLAG = dynamic
            else:
                v.YZBORREG=1
                v.YZBORMODE = 1
                v.YMINTPOW_SET_FLAG = dynamic
        else:
            v.YZBORREG=0

bipr_indexs=np.array([158,159,160,161,162,163,149,150,151,152,153,
   154,155,156,157,139,140,141,142,143,144,145,146,
   147,148,128,129,130,131,132,133,134,135,136,137,
   138,116,117,118,119,120,121,122,123,124,125,126,
   127,103,104,105,106,107,108,109,110,111,112,113,
   114,115,89,90,91,92,93,94,95,96,97,98,99,100,101,
   102,76,77,78,79,80,81,82,83,84,85,86,87,88,62,63,
   64,65,66,67,68,69,70,71,72,73,74,75,49,50,51,52,53,
   54,55,56,57,58,59,60,61,37,38,39,40,41,42,43,44,45,
   46,47,48,26,27,28,29,30,31,32,33,34,35,36,16,17,18,
   19,20,21,22,23,24,25,7,8,9,10,11,12,13,14,15,1,2,3,
   4,5,6])

def index_up_dn(mfa_index):
    u"""��������� �� �������� ���, �������� ����"""
    global bipr_indexs
    return bipr_indexs[mfa_index-1]

def test_index_up_dn():
    assert(index_up_dn(1)==158)

def reorder_up_dn(arr_mfa_index):
    u"""��������� �� �������� ���, �������� ����"""
#    arr_mfa_index[...] = arr_mfa_index[bipr_indexs-1,...]
    arr_mfa_index[...] = arr_mfa_index[bipr_indexs-1,...][...]

def test_reorder_up_dn_single():
    a=np.arange(163)
    a+=1
    reorder_up_dn(a)
    assert(a[0]==158)

def test_reorder_up_dn_mult():
    a=np.arange(163)
    a+=1
    aa=np.vstack([a,a]).T
    reorder_up_dn(aa)
    assert(aa[0,0]==158 and aa[0,1]==158)


