#!/usr/bin/env python
# -*- coding: cp1251 -*-
#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      AND
#
# Created:     23.07.2010
# Copyright:   (c) AND 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import os,sys,re,numpy,glob
from mc2py.evaldict import *

from name2mfa import nm2ind

def applyloop(first,ind,arr):
    u"""��������� ������������ � ������ ����� ������� first"""
    extr=[arr[i] for i in ind]
    extr.insert(0,first)
    extr.pop()
    for i,obj in zip(ind,extr):
        arr[i]=obj
def applyloopcyc(ind,arr):
    u"""��������� ������������ � ������ ����� ������� ��������� �����������"""
    extr=[arr[i] for i in ind]
    v=extr.pop()
    extr.insert(0,v)
    for i,obj in zip(ind,extr):
        arr[i]=obj

class Tloop:
    def __init__(self,name):
        self.name=name
        self.chains=[]
    def add_line(self,line):
        a=[int(i) for i in re.findall("\d+",line)]
        self.chains.append(a)
    def process(self,arr):
        u"���������� ���� ������������ � ������"
        if self.name == "None":
            for ch in self.chains:
                applyloopcyc(ch,arr)
        else:
            for ch in self.chains:
                applyloop(self.name,ch,arr)

class Tperm:
    u"����� ������������ ���� ��� ������ ������ ����� ������ ��� ��� ������ ����� �������"
    def __init__(self,n):
        self.data=range(n)

def readshu(a):
    u"��������� shu ���� �� ��� ������ � --->"
    lin=[i.strip() for i in open(a,"r").readlines()]
    loops=[]
    loop=None
    for l in lin:
        if not re.search("--+>",l):
            if loop:loops.append(loop)
            loop=Tloop(l)
        else:
            loop.add_line(l)
    if loop:loops.append(loop)
    return loops

def newfuel(loops):
    u"�������� ����� ������ �������"
    return [i.name for i in loops]

def lshu2shu360(loops,replasenames=1):
    u"""�� ������ ������ ������������ ���� ���� � ���� ������� ������ ����� ������
��������� - ������ � ������ ������� ������� ����������� ������ ������ ������
���� ����� �� �� ���������� ��������
���� ������ - ��� �������
"""
    iperm=range(0,164)
    for l in loops:
        l.process(iperm)
    if replasenames:
        return permnames2index(iperm)
    else:
        return iperm

def name2index(id):
    u"�������� ���������� ��� �� ������ ���� ������ ��� ����� ������� � nm2mfa ����� ������� ��� ���������"
    if type(id) is int:
        return id
    try:
        return nm2ind[id].bippar+1000
    except:
        return id

def permnames2index(iperm):
    u"� ������������ �������� ������� �� �������"
    return [name2index(i) for i in iperm]

def __s(li):
    return sepr('\n',li)

def perm2str(perm):
    jl="\n".join(map(str,perm))
    return """&burnf
simper=1,
kkb=
%s
&end
""" % jl


def rewrite(a,b,replasenames=1):
    res=lshu2shu360(readshu(a),replasenames)
    res=perm2str(res[1:])
    f=open(b,"w")
    f.write(res)
    f.close()

def allfu():
    st=set()
    for nm in glob.glob("b??/k??/shu.dat"):
        res=readshu(nm)
        for i in newfuel(res):
            st.add(i)
    return st

def ConvertAllShu():
    "����������� ��� shu ����� � �������� �����������"
    for nm in glob.glob("b??/k??/"):
        rewrite(nm+"shu.dat",nm+"shu.360")


def main(a,b):
#    rewrite(a,b)
#    print allfu()
    ConvertAllShu()
#nm=r"b03\k20"
#rewrite(nm+"/shu.dat",nm+"/shu.360")

if __name__ == '__main__':
    main(sys.argv[1],sys.argv[2])