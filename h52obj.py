#!/bin/env/python
# -*- coding:utf-8 -*-

u"""
импорт и экспорт небольших hdf5 как словарей питона
"""

import h5py

def dump(obj,file_name):
    u"""python object to h5py file"""
    with h5py.File(file_name,"w") as f:
        for nm in obj:
            _dump(nm,obj[nm],f)

def _dump(nm,obj,node):
    if hasattr(obj,"keys"):
        g = node.create_group(nm)
        for nm in obj:
            _dump(nm,obj[nm],g)
    else:
        node[nm]=obj


def _load(f):
    if hasattr(f,"keys"):
        return {nm:_load(f[nm]) for nm in f}
    else:
        return f[...]

def load(file_name):
    u"""load h5py as python dict"""
    with h5py.File(file_name,"r") as f:
        return {nm:_load(f[nm]) for nm in f}


#a={"a":np.zeros(3),"b":{"a":np.zeros(4),"b":np.zeros((3,8))}}
#dump(a,"out.h5")
#b=load("out.h5")

