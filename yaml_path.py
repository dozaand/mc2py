#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
load paths from yaml file
"""

import yaml
import os


class YamlPath(object):
    u"""load paths from yaml file"""
    def __init__(self, nm):
        with open(nm,"r") as f:
            self.obj = yaml.load(f)
        self.root = os.path.abspath(self.obj["root_dir"])

    def __getitem__(self, key):
        v = str(self.obj[key])
        if os.path.isabs(v):
            return v
        else:
            return os.path.join(self.root, v)

def test_YamlPath():
    nm="tmp.yaml"
    with open(nm,"w") as f:
        data = """1 : E:/1
2 : 2
root_dir : c:/a
"""
        f.write(data)
    cfg = YamlPath(nm)
    assert(cfg[1] == 'E:/1')
    assert(cfg[2] == 'c:\\a\\2')
    os.remove(nm)
