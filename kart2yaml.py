#! python
# -*- coding:utf-8 -*-
u"""
передедываем картограмму в yaml файл с картограммой
"""

import os, sys, glob
import numpy as np
from mc2py.geom import File2Geom
from mc2py.fold import conv_val,fmap
import codecs
import yaml
import argparse


#file_in = "a.dat"
#file_out = "a.yaml"

def data2yaml(file_in,file_out,s):
    u"""преобразование обычного текста в картограмму"""
    g,d= File2Geom(file_in)
    keys = list(set(d))
    g.symmetry = s
    data_to_save=fmap({
    "geom":{
        "bounds":[[int(i[0]),int(i[1])] for i in g.bounds],
        "symmetry":g.symmetry,
        "y0":g.y0},
    "keys":keys,
    "kart":d
    },conv_val,conv_val)

    with codecs.open(file_out,"w",encoding="utf-8") as f1:
        yaml.dump(data_to_save,f1,allow_unicode=1)

def repl_ext(nm,new_ext):
    u"""замена расширения на заданное"""
    n,e = os.path.splitext(nm)
    return n+new_ext

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="convert kart to yaml format")
    parser.add_argument("-o", "--output", default="", help=u"sourse directory")
    parser.add_argument("-s", "--symmetry", default=6, help=u"sourse directory")
    parser.add_argument("input", default="*.dat", help=u"wildcards for input files")
    args = parser.parse_args()
    for i in glob.glob(args.input):
        if args.output:
            out_nm = args.output
        else:
            out_nm = repl_ext(args.input, ".yaml")
        data2yaml(args.input,out_nm,args.symmetry)


