#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
�������� ������ xipi �� ���������� xipi  � hdf5 ����
"""

import h5py
from mc2py.fortrannamelist import ReadFileNamelists, ReadNameListBody
from os.path import join
import numpy as np
import glob
import datetime
import time
import os
import argparse
from mc2py.util import Double2DateTime

"""
1) ������������ ������:
	1-�� ������� - "� ���"
	2-�� ������� - "� ogl"
	3-�� ������� - "� bippar"
	4-�� ������� - "����������� �� ���� ���"
2) if ("� bippar" .ge. 900 .and. "����������� �� ���� ���" .eq. XXXXX) then
   ������ ����� � bippar ���
3) if ("� bippar" .ge. 900 .and. "����������� �� ���� ���" .ne. XXXXX) then
   ������ ����� � bippar ���, � "����������� �� ���� ���" ����� �� ������� ���������
4) if ����� � ��� �� ���� "� ���" "� ogl" ������������� ������ ����
   "� bippar" "����������� �� ���� ���", �� � ���������� ��� ��� �������������
   ������ ����� ��� (���� �� �������� ��� ��� ���������� � ���������� ����,
   ���� ���� ��������� ����� ����������)
5) if ("� ogl" .ge. 500) then ������ ����� � ogl ���


����������� ��� ���� 3

6) "� bippar" = "� bippar" + 300 � ��������� ��������������� ��������� � ����� shu.360

���������� ��� ���� 1

7) "� bippar" = "� bippar" + 600 � ��������� ��������������� ��������� � ����� shu.360
"""

# ��� ����������� ���� � ����� ����� ������ ����������� � �����������
core_mask = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],dtype=np.bool)
core_index = np.array(np.where(core_mask>0)[0],dtype='H')

mappers = [
    [1, 7, 10, '30FL'],
[2, 2, 9, '44FLB'],
[3, 1, 7, '44FL'],
[4, 5, 8, '42FLB'],
[5, 4, 6, '42FL'],
[6, 9, 1, '16FL'],
[6, 9, 301, '16FL'],
[7, 11, 2, '20FS'],
[8, 13, 4, '33FS'],
[9, 15, 5, '32FS'],
[10, 17, 3, '30FS'],
[11, 23, 12, '42FLD'],
[12, 85, 11, '30FLB'],
[13, 94, 900, 'XXXXX'],
[14, 140, 45, '390AB'],
[15, 141, 51, '390A'],
[16, 144, 47, '382AB'],
[16, 144, 92, '382B5'],
[17, 146, 46, '382A'],
[17, 146, 91, '382A5'],
[18, 149, 50, '294AA'],
[19, 150, 49, '294A'],
[20, 153, 901, 'XXXXX'],
[21, 155, 85, '287GA'],
[22, 165, 87, '287AA'],
[23, 166, 86, '287A'],
[24, 169, 80, '39UGA'],
[24, 169, 94, '390GA'],
[25, 172, 79, '35UGA'],
[25, 172, 93, '353GA'],
[26, 193, 43, '430GA'],
[26, 193, 44, '430GO'],
[26, 193, 383, '430GO'],
[26, 193, 81, '43UGA'],
[27, 223, 83, '39R6A'],
[28, 203, 90, '390GO'],
[28, 203, 381, '390GO'],
[29, 226, 385, '13AU'],
[30, 229, 386, '22AU'],
[30, 229, 389, '22AUM'],
[31, 232, 387, '30AV5'],
[32, 235, 388, '39AWU'],
[33, 238, 88, '398GO'],
[33, 238, 390, 'B40E6'],
[34, 241, 902, '44AZU'],
[35, 197, 89, '439GO'],
[35, 197, 95, '439GT'],
[35, 550, 391, 'B44G6'],
[36, 500, 63, '481TS'],
[36, 500, 97, '481TM'],
[36, 500, 106, '481TN'],
[37, 502, 13, '418R6'],
[38, 504, 15, '417R9'],
[39, 506, 16, '458R6'],
[40, 508, 98, '399A9'],
[41, 510, 100, '438A9'],
[42, 512, 108, '481EN'],
[43, 514, 109, '481ES'],
[44, 516, 652, '16ZS'],
[45, 518, 658, '24ZS'],
[46, 520, 662, '24ZSA'],
[47, 522, 661, '24ZSB'],
[48, 524, 670, '24ZSC'],
[49, 526, 659, '362ZS'],
[50, 528, 663, '362ZB'],
[51, 530, 642, '35ZSZ'],
[52, 532, 616, '39ZSZ'],
[53, 534, 118, 'M47E9'],
[54, 536, 679, 'Z35A9'],
[55, 538, 676, 'Z43F6'],
[56, 539, 905, '438A9'],
[57, 540, 906, '481TN'],
[58, 541, 908, '481EN'],
[59, 542, 909, '481ES'],
[60, 543, 910, '417R9'],
[61, 544, 911, '418R6'],
[62, 545, 912, '439GT'],
[63, 546, 916, '458R6'],
[64, 547, 914, 'B44Z4'],
[65, 548, 915, 'B48Z4'],
[66, 549, 917, 'B49Z4'],
[67, 550, 392, '430GO'],
[68, 551, 918, 'A441E'],
[69, 552, 919, 'A442E'],
[70, 553, 920, 'A443E'],
[71, 554, 921, 'B49G6'],
[72, 555, 922, 'B49z4'],
[73, 556, 923, 'B47Z4'],
[74, 557, 924, 'C150'],
[75, 558, 925, 'C240'],
[76, 559, 926, 'C300'],
[77, 560, 927, 'C330'],
[78, 561, 928, 'U30G2'],
[79, 562, 929, '36FLB'],
[80, 563, 930, '36FL'],
[81, 564, 931, '42FLD'],
[82, 565, 932, '44FLD'],
[83, 564, 933, 'A40E6'],
[84, 565, 934, 'A44G6'],
[85, 564, 935, 'A44Z4'],
[86, 565, 936, 'A47Z4'],
[87, 564, 937, 'A49G6'],
[88, 565, 938, 'D49Z4'],
[89, 566, 939, 'D49G6'],
[90, 567, 940, 'B40E6'],
[91, 568, 941, 'B44G6'],
[92, 569, 942, 'B44Z4'],
[93, 570, 943, 'B47Z4'],
[94, 571, 944, 'B49G6'],
[95, 572, 945, 'C49Z4'],
[96, 573, 946, 'C49G6']
]
# ������������� ������������ ������ ����� � ������ �������� � ������������ �������
ogl2n = dict([[row[1], row[-1]] for row in mappers])
mfa2n = dict([[row[0], row[-1]] for row in mappers])
bipr2n = dict([[row[2], row[-1]] for row in mappers])


def tostamp(t):
    return time.mktime(t.timetuple())


def JoinByT(grnli):
    """������ ��� �������"""
    year = grnli["NYEAR"]
    month = grnli["NMONTH"]
    if hasattr(grnli["IDAT"], "__len__"):
        idays = np.array(grnli["IDAT"])
    else:
        idays = np.array([grnli["IDAT"]])
    dt = [datetime.timedelta(seconds=int((i-1)*24*3600)) for i in idays]
    initime = datetime.datetime(year, month, 1)
    t0 = [tostamp(initime+ddt) for ddt in dt]
    return t0


def FuType(t):
    """��� shu ��������������� ����"""
    if t > 1000:
        try:
            n = bipr2n[t-1000]
            return n
        except:
            return "???"+str(t-1000)
    else:
        return str(t-1) # ������ ��������� � ����������� �� ����


def FuTypeMapn(t):
    """��� mapn ��������������� ����"""
    try:
        return bipr2n[t]
    except:
        return "???"+str(t)


def LoadKamp(dr):
    """�������� ������ �� ��������"""
    try:
        shu = ReadFileNamelists(join(dr, "shu.360"))["BURNF"]
        shu["KKB"] = np.array(map(FuType, shu["KKB"]))
    except IOError:
        shu = None
        pass
    try:
        burboc = ReadFileNamelists(join(dr, "burboc"))["BURNF"]
        burboc["MAPN"] = np.array(map(FuTypeMapn, burboc["MAPN"]))
#        burboc=None
    except IOError:
        burboc = None
    grnlist = []
    for i in glob.glob(join(dr, "GRN/grn*")):
        grn = ReadNameListBody(i)
        res = np.array(grn["EF"]).reshape((-1, 20))
        grn["Teff"] = list(res[:, 0])
        grn["H10"] = list(res[:, 1])
        grn["Cb"] = list(res[:, 2])
        grn["W"] = list(res[:, 3])
        grn["Tin"] = list(res[:, 4])
        grn["G"] = list(res[:, 6])
        grn.pop('EF')
        grnlist.append(grn)
#        grn["idat"]=list(grn["idat"])
        print i
    grnlic = {}
    grnlic['t'] = np.hstack([JoinByT(v) for v in grnlist])
    for i in "Teff H10 Cb W Tin G".split():
        grnlic[i] = np.hstack([np.array(v[i], dtype='f') for v in grnlist])
    return dict(shu=shu, grn=grnlic, burboc=burboc)


def Copy2H5(f, obj, path=''):
    u"""����������� �������� �������� � ����"""
    if hasattr(obj, "iteritems"):
        for k, v in obj.iteritems():
            Copy2H5(f, v, path+"/"+str(k))
    else:
        if not obj is None:
            f[path] = obj


def ImportKamp(path, f, force):
    u"""������������ ����� ��������"""
    p, kamp = os.path.split(path)
    p, block = os.path.split(p)
    p, plant = os.path.split(p)
    p0 = "/".join([plant, block, kamp])
    res = LoadKamp(path)
    if force and p0 in f:
        del f[p0]
    Copy2H5(f, res, p0)
    timeset = f[p0]["grn/t"]
    timeset.attrs["beg"]=str(Double2DateTime(timeset[0]))
    timeset.attrs["end"]=str(Double2DateTime(timeset[-1]))


def AddIndex(f):
    nm = "core_index"
    if not nm in f:
        f[nm] = core_index
        g=f[nm]
        g.attrs["example"]="core_refl[index]=core[:];core[:]=core_refl[index]"
        g.attrs["N"] = len(core_index)

def LoadAll(mask, out, force=0):
    u"""������������ �� ���������� �� �����"""
    with h5py.File(out, libver='latest') as f:
        AddIndex(f)
        for m in mask:
            for i in glob.glob(m):
                print i
                if i[-2:] != '00':
                    ImportKamp(i.lower(), f, force)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="import xipi grn data to h5 format file")
    parser.add_argument("-o", "--out", default="xipi.h5", help=u"output file")
    parser.add_argument(
        "-f", "--force", action='store_true', help=u"output file")
    parser.add_argument("mask", nargs="*", default=[
                        "xipi1/*/B*/k*"], help=u"mask for input directorys")
    args = parser.parse_args()
    LoadAll(args.mask, args.out, args.force)

