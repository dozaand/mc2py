#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import h5py
from br2mainframe import br2mainframe as br2mainframe_base
import copy
from mc2py.util import newcwd
import os


# Implementing mainframe
class br2mainframe(br2mainframe_base):
    def __init__(self, parent, obj1, obj2):
        """�������������, �� ���� �������� ��� hdf5 �������:obj1,obj2"""
        br2mainframe_base.__init__(self, parent)
        # ��������� ������ ��� ����������
        curdir = os.path.split(__file__)[0]
        with newcwd(curdir):
            formIconLoc = wx.IconLocation(r'bitmaps/vegastrike.ico', 0)
            self.SetIcon(wx.IconFromLocation(formIconLoc))
        # �������������� ����������� ��������
        self.obj = [obj1, obj2]
        self.currItemIndex = [[0], [0]]
        self.SelectIndex = [0, 0]
        self.currPath = [["[\"obj\"]"], ["[\"obj\"]"]]
        self.listlist = (self.m_listCtrl1, self.m_listCtrl1)
        self.UpdateView()

        # �������
        # self.ItemActivated(0,0)
        # self.ItemActivated(0,2)
        # self.ItemActivated(0,2)
        # self.ItemActivated(0,2)
        # self.ItemActivated(0,2)

    def UpdateView(self):
        """���������� ��������� ���� ListCntr"""
        self.UpdateObjectView(0)

    def UpdateObjectView(self, i):
        """���������� ��������� ������ ListCntr, �� ������ i"""
        li = self.listlist[i]
        # ����� ��� ��� �������� ������, ��������, ����� �� ��� �������
        # ���� �� ��������� - �� �������� ������
        # ���� ������ �������� ������ - �� �������� ������
        parPathList = copy.copy(self.currPath[i])
        parPathList.pop()
        parPath = self.PathConstructor(i, parPathList)
        if parPath != "":
            parObj = eval("self.obj[i]%s" % parPath)
        else:
            path = self.PathConstructor(i, self.currPath[i])
            currObj = eval("self.obj[i]%s" % path)
            parObj = currObj  # ������ �� ��������� �������� ������

        if hasattr(parObj, "__iter__"):
            path = self.PathConstructor(i, self.currPath[i])
            currObj = eval("self.obj[i]%s" % path)
            if hasattr(currObj, "__iter__"):
                for ind, nm in enumerate(currObj):
                    li.InsertStringItem(ind, str(nm))
            else:
                li.InsertStringItem(0, str(currObj))

            # ���������� ����� �������� ��� ����������� ������ �������
            if self.currPath[i] != ["[\"obj\"]"]:
                li.InsertStringItem(0, "..")
                li.InsertStringItem(0, ".")
            # ��������� ��������� ������
            li.Select(self.SelectIndex[i])
            self.SetStatusText("%s" % path)
        else:
            self.SetStatusText(u"������� �������. ����������� ����.")

    def ItemActivated(self, i, selectedPos):
        """���������� ��������� ������ i-�� ListCntr ��� ���������(������ � ������) � ��������� ������� selectedPos"""
        li = self.listlist[i]
        itname = li.GetItemText(selectedPos)  # ��� ���������� ����
        if itname == ".":  # ����� � ��������� �������
            self.currPath[i] = ["[\"obj\"]"]
            self.currItemIndex[i] = [0]
            self.SelectIndex[i] = 0
        if itname == "..":  # ����� �� ���� ����� �����
            popPath = self.currPath[i].pop()
            popIndex = self.currItemIndex[i].pop()
            self.SelectIndex[i] = popIndex
        if itname != "." and itname != "..":  # ����� �� ���� ����� ������
            path = self.PathConstructor(i, self.currPath[i])
            currObj = eval("self.obj[i]%s" % path)
            elstr = self._GetPathElement(
                currObj, selectedPos - 2)  # 2 ��������, ��������� � ListCntr ���� ����������� (.) � (..), � � ����� ������� ����� ����� ���
            self.currPath[i].append(elstr)
            self.currItemIndex[i].append(selectedPos)
            self.SelectIndex[
                i] = 1  # ������������� ������� �������� ����� (..)

        li.DeleteAllItems()
        self.UpdateObjectView(i)

##        print "----------------------------------"
##        print self.currPath
##        print self.currItemIndex
##        print "ItemAcitvated %s" %  self.PathConstructor(i,self.currPath[i])
##        print "----------------------------------"

    def _GetPathElement(self, obj, index):
        """���������� ������� ������� ����, ��������: [1], .a, ['xxx']"""
        if hasattr(obj, "keys"):  # �������
            key = sorted(obj.keys())[
                index]  # ���������� � python3 ���� ����� �� ����������
            str = "[\"%s\"]" % key
            return str
        elif hasattr(obj, "__getitem__"):  # ������ �� ������� � ������������ ��������
            str = "[%s]" % index
            return str
        elif hasattr(obj, "__iter__"):  # ������������ ������� (���� set)
            self.listview = [i for i in obj]
            str = "[%s]" % index
            return str
        elif hasattr(obj, "__dict__") or hasattr(obj, "_fields_"):  # ������
            key = self._GetAllKeys(obj)[index]
            str = ".%s" % key
            return str
        else:
            str = ""
            return str

    def _GetAllKeys(self, obj):
        """���������� ��� ������ ����� ���� �����, �.�. ���������� __dict__, _fields_ � ��� ��������"""
        key = list(obj.__dict__.keys())
        if hasattr(obj, "_fields_"):
            key += [el[0] for el in obj._fields_]
        proplist = [name for (name, sobj) in inspect.getmembers(
            obj.__class__, inspect.isdatadescriptor) if isinstance(sobj, property)]
        return sorted(key+proplist)

    def m_listCtrl1OnListItemActivated(self, event):
            # TODO: Implement m_listCtrl1OnListItemActivated
        selectedPos = event.m_itemIndex
        self.ItemActivated(0, selectedPos)
        event.Skip()

    def PathConstructor(self, i, pathList):
        """�������� ������� ���� � ������� �� ������"""
        path = ""
        for el in pathList:
            path = path+el
        return path

    def m_listCtrl1OnLeftDClick(self, event):
        # TODO: Implement m_listCtrl1OnLeftDClick
        # print "DbClick"
        event.Skip()

    def m_listCtrl1OnListItemSelected(self, event):
        # TODO: Implement m_listCtrl1OnListItemSelected
        # self.currItem[0]= event.m_itemIndex
        # print "Item Select %s" % self.currItem[0]
        event.Skip()

        # Handlers for mainframe events.
    def Back(self, event):
                # TODO: Implement Back
        i = 0
        if self.currPath[i] != ["[\"obj\"]"]:  # ������ �� ������������ ����� � �������� ������
            li = self.listlist[i]
            popPath = self.currPath[i].pop()
            popIndex = self.currItemIndex[i].pop()
            self.SelectIndex[i] = popIndex
            li.DeleteAllItems()
            self.UpdateObjectView(i)

    def Forward(self, event):
                # TODO: Implement Forward
        i = 0
        li = self.listlist[i]
        selectedPos = li.GetFirstSelected()
        self.ItemActivated(0, selectedPos)
        pass
