# возбуждение Xe колебаний
import numpy as np
import math
from pathlib import Path
import matplotlib.pyplot as plt
from mc2py.zmq_script_client import *



def xe_oscilation(soc):
    """Возбуждение ксеноновых колебаний"""
    soc["#YM#ymtime_re8"] = 0 # сбрасываем время
    soc["#YM#ymtime"] = 0 # сбрасываем время
    soc.step()
    soc["YZKEYLINK2"] = soc["YZKEYLINK2"] + 1 # переходим в НКС
    soc['#YM#YMFLAGSTAT']=1 # включаем статику
    soc.step()
    soc['YZBORREG']=1   # включаем внешнее управление бором
    soc.step()
    soc['YZBORMODE']=2  # бор в автомат
    soc.step()


    # включаем блокировки защит
    soc['KEY1_SHUNT_AZ1'] = 1
    soc['KEY_SHUNT_URB'] = 1
    soc['KEY_SHUNT_PZ1'] = 1
    soc['KEY_SHUNT_PZ2'] = 1
    soc['klarm_pos'] = 1  # *однократное отключение АРМ *
    soc['ymflag_xe'] = 1  # расчет динамики Xe в статике
    soc['YMRP_FAST']=1 # убираем ускорение расчета остаточного энеговыдленеия
    soc['ymfast'] = 1 # убираем ускорние расчета модели активной зоны
    soc['YZREGRPINS_KEY'] = 1 # активировать ввод положения групп в %
    soc.step(4)
    # установка температуры на входе в реактор
    soc["YMTMIX_IN_INPUT_USE"] = 1
    soc["YMTMIX_IN_INPUT"]= 296# soc["SVRK_THNAVR"]
    soc.step()
   

    # устанавливаем мощность в 85 %
    soc["YMINTPOW_SET"] = 85
    soc.step(4)
    soc["YM_STROBE_POWSET"] = 1
    soc.step(4)
    soc['YZBORMODE']=2  # бор в автомат
    soc.step()


    # делаем укачку по реактивности
    while ( 0.01 < math.fabs( float(soc['YMDRNEW_proc']) )  ):
        soc.step(4)

    suz=[1.03,1.03,1.03,1.03,1.03,1.03,   1.03,1.03,1.03,1.03,1.03,0.7]
    soc['#YS#YSHGRP'] = suz
    soc["YMFAST"] = 100

    tcurr = float(soc["#YM#ymtime"])
    tend=2*3600 + tcurr
    while  tcurr < tend:
        soc.step(8)
        tcurr = float(soc["#YM#ymtime"])
    print ("2 часа прошло")

    suz=[1.03,1.03,1.03,1.03,1.03,1.03,   1.03,1.03,1.03,1.03,1.03,0.9]
    soc['#YS#YSHGRP']=suz
    soc["YMFAST"] = 1
    soc.step(4)

    soc['YZBORMODE']=1  # бор в дистанцию
    soc.step()
    soc['#YM#YMFLAGSTAT']=0 # выключаем статику
    soc.step(4)
    print ("Xe колебания возбуждены")




soc = Tscript_drv("tcp://localhost:5555")
xe_oscilation(soc)
