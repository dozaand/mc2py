#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""show data on cartogramm plot or text from json,pickle or h5 files"""
import h5py
from mc2py.showkart import kshow
import argparse
import re
import os
import numpy as np
from mc2py.scutil import ld
from pylab import plot, show

# s=r"Arh3b_2.h5//@['RU/KV'][100000].reshape((-1,163))[1]"
# s=Arh3b_2.h5//@['RU/KV'][100000].reshape((163,-1))[:,1]
# s=r"data/current_params.json"
cmd = "python sk.py -mt data/dynMatr.pkl//@['Id2Xe'][0]"


def ExtractData(s):
    """������ ���� ������ � ����� � ����� - ���������� ������ ����//���� � �����"""
    res = re.match(r"(?P<fnm>[^/]+(/[^/]+)*)(//(?P<expr>.+))?$", s)
    gd = res.groupdict()
    fil, ext = os.path.splitext(gd['fnm'])
    if ext == ".h5":
        with h5py.File(gd['fnm']) as f:
            return eval(gd["expr"].replace("@", "f"))
    elif ext in ".py .pkl .json".split():
        f = ld(gd['fnm'])
        if gd["expr"]:
            return eval(gd["expr"].replace("@", "f"))
        else:
            return f


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="showD", description="show data on kart")
    parser.add_argument(
        "-m", "--method", default='k', help=u"method of data show [k,p,t]")
    parser.add_argument(
        "datadsc", nargs="*", help=u"input data: data file or file/pathon file")
    parser.add_argument("-f", "--fmt", help=u"set data format")
    args = parser.parse_args()
    res = map(ExtractData, args.datadsc)
    for i in res:
        if args.method == 'k':
            kshow(i, show=1, format=args.fmt)
        elif args.method == 'p':
            plot(i)
            show()
        elif args.method == 't':
            print i
        else:
            raise Exception("undefined option %s" % args.method)
