#!/usr/bin/env
# -*- coding: utf-8 -*-

import h5py
import datetime
from mc2py.util import DateTime2Double,Double2DateTime
import numpy as np
import argparse

def get_range(g):
    gg=g["grn/t"]
    return (gg[0],gg[-1])

def get_kamp_burn(db_name, plant, block, t):
    u"""
    преобразование даты в выгорание и название кампании
    return kamp,burn
    """
    with h5py.File(db_name,"r") as f5:
        g=f5[plant][block]
        ranges = dict([[k,get_range(gkamp)] for k,gkamp in g.items()])
        float_t = DateTime2Double(t)
        # linear search kamp
        res=None
        for k,rg in ranges.items():
            if rg[0]<=float_t<rg[1]:
                res=k
                break;
        if res is None:
            raise ValueError("time not in any company")
        gk=g[res]["grn"]
        tset = gk["t"][...]
        index = np.searchsorted(tset, float_t)
        # make lower bound
        if index > 0:
            index-=1
        if index > len(tset)-2:
            index = len(tset)-2

        w0 = gk["W"][index]
        w1 = gk["W"][index+1]
        b0 = gk["Teff"][index]
        b1 = gk["Teff"][index+1]
        t0 = gk["t"][index]
        t1 = gk["t"][index+1]
        b = b0+(b1-b0)*(float_t-t0)/(t1-t0)# linear interpolation
    return res,b
#    sut=3600*24
#    b = (2*b0*sut -(float_t - t0)*((-2 + float_t - t0)*w0 + (-float_t + t0)*w1))/(2.*sut)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="search kamp and burn for given plant, block time")
    parser.add_argument("data", nargs=5, help=u"plant block date time : examp: kaln b01 1992-06-11 03:23:01 xipi.h5")
    args = parser.parse_args()
    plant, block, dd, tt, db_name = args.data
    t = datetime.datetime.strptime(" ".join([dd,tt]),"%Y-%m-%d %H:%M:%S")
    kamp,burn = get_kamp_burn(db_name, plant, block, t)
    print kamp,burn
