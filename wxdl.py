#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""������ ������������ �������� �� ��������� ������ ��� ����������

� ����������� ���������
from MyProject1MyDialog1 import main as mm

.....
   ����� ������� ������ ������ ����� ������� Result - ��������� ������ �������

    def OnMove(self, event):
        pos = event.GetPosition()
        self.posCtrl.SetValue("%s, %s" % (pos.x, pos.y))
        r=mm(self)
        return r

� ��������� ��� ��������� ���������
  r=RunDlg(MyProject1MyDialog1,parent)

"""

import wx


def RunDlg(dlgcls, parent=None, **kvargs):
    u"""run any dialog """
    if not parent:
        if "app" not in globals():
            global app
            app = wx.PySimpleApp(redirect=False)
        dlg = dlgcls(parent, **kvargs)
        app.SetTopWindow(dlg)
        dlg.ShowModal()
        res = dlg.Result()
        dlg.Destroy()
        app.MainLoop()
    else:
        dlg = dlgcls(parent, **kvargs)
        dlg.ShowModal()
        res = dlg.Result()
        dlg.Destroy()
    return res
