#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import os
import numpy as np
import matplotlib
import cPickle
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
import matplotlib.patches as pat
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar

import geom


def Msg(parent, msg):
    dlg = wx.MessageDialog(parent, msg,
                           '���������',
                           wx.OK | wx.ICON_INFORMATION
                           )
    dlg.ShowModal()
    dlg.Destroy()


def evex(st):
    u"""�������� ������������� ��������� � ����� �� ���������� - ��������� ������"""
    try:
        return eval(st)
    except:
        return st


def GetSelString(lst):
    u"""�� ������ ����� ��������� ������"""
    return lst.GetString(lst.GetSelection())


class PlotFrame(wx.Frame):
    u""" The main frame of the application"""
    title = u'�������� ����������'

    def __init__(self, gm, keys, data):
        wx.Frame.__init__(self, None, -1, self.title)
        self.geom = gm
        self.keys = keys
        self.data = data
        n = gm.ntot
        for k in keys:
            if k not in data:
                data[k] = ["" for i in xrange(n)]
            else:
                assert(len(data[k]) == n)
        self.replot = 0

        self.CreateMenu()
        self.statusbar = self.CreateStatusBar()
        self.CreateMainPanel()
        v = self.kartId

#        self.textbox.SetValue(' '.join(map(str, self.data)))
#        self.replot=1
#        self.draw_figure()

    def getKartId(self):
        u"��������� ������� ������� �����������"
        res = self.list_box_kart.GetSelection()
        if res < 0:
            self.setKartId(0)
            return 0
        else:
            return res

    def setKartId(self, value):
        u"��������� ������� �����������"
#        if self.list_box_kart.GetSelection()!=value:
        self.list_box_kart.SetSelection(value)
        self.UpdateValueList()
        self.draw_figure()
    kartId = property(getKartId, setKartId, u"������������� �����������")

    def UpdateValueList(self, pos=0):
        self.list_box_kart_items.Clear()
        self.currKart = GetSelString(self.list_box_kart)
        li = map(str, self.keys[self.currKart])
        self.list_box_kart_items.InsertItems(li, 0)
        self.valId = 0

    def CreateMenu(self):
        self.menubar = wx.MenuBar()

        menu_file = wx.Menu()

        m_expt = menu_file.Append(
            -1, "Save &All\tCtrl-A", u"��������� ��� ����������")
        self.Bind(wx.EVT_MENU, self.OnSaveAll, m_expt)

        menu_help = wx.Menu()
        m_about = menu_help.Append(-1, "&About\tF1", "About the demo")
        self.Bind(wx.EVT_MENU, self.OnAbout, m_about)

        self.menubar.Append(menu_file, "&File")
        self.menubar.Append(menu_help, "&Help")

        self.SetMenuBar(self.menubar)

    def OnSavePlot(self, event):
        file_choices = "PNG (*.png)|*.png"

        dlg = wx.FileDialog(
            self,
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="plot.png",
            wildcard=file_choices,
            style=wx.SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)

    def OnExport(self, event):
        u"""��������� ������� �����������"""
        file_choices = "*.* (*.krt)|*.*"

        dlg = wx.FileDialog(
            self,
            message="��������� ������� ����������� ��� ...",
            defaultDir=os.getcwd(),
            defaultFile="a.dat",
            wildcard=file_choices,
            style=wx.SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            with open(path, "wt") as f:
                dat = self.data[self.getKartIdName()]
                for e in dat:
                    f.write(str(e)+"\n")
                self.flash_status_message("Saved to %s" % path)

    def OnExit(self, event):
        self.Destroy()

    def OnAbout(self, event):
        msg = u""" �������� ����������
������������ ��� �������������� ���������� ������"""
        dlg = wx.MessageDialog(self, msg, "About", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()

    def CreateMainPanel(self):
        u""" �������� ������� ������"""
        self.panel = wx.Panel(self)
        panel = self.panel

        # Create the mpl Figure and FigCanvas objects.
        # 5x4 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig = Figure((5.0, 4.0), dpi=self.dpi)
        self.canvas = FigCanvas(panel, -1, self.fig)

        self.toolbar = NavigationToolbar(self.canvas)

        self.axes = self.fig.add_subplot(111)
        self.canvas.mpl_connect('pick_event', self.on_pick)

        choices = self.keys.keys()
        choices.sort()

        self.list_box_kart = wx.CheckListBox(panel, -1, choices=choices)
        self.Bind(wx.EVT_CHECKLISTBOX,
                  self.ChangeKartList, self.list_box_kart)
        self.Bind(wx.EVT_LISTBOX, self.ChangeKart, self.list_box_kart)

        choices = self.keys[choices[0]]
        choices.sort()
        self.list_box_kart_items = wx.ListBox(panel, -1, choices=choices)
        self.Bind(
            wx.EVT_LISTBOX, self.ChangeCurrFiller, self.list_box_kart_items)

        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)

        self.vbox1 = wx.BoxSizer(wx.VERTICAL)

        self.vbox1.Add(wx.StaticText(
            panel, -1, u"�����������\n"), 0, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox1.Add(self.list_box_kart, 1, wx.EXPAND)
        self.vbox1.Add(wx.StaticText(
            panel, -1, u"������\n"), 0, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox1.Add(self.list_box_kart_items, 1, wx.EXPAND)

        self.hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox.Add(self.vbox, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.hbox.Add(self.vbox1, 0, wx.EXPAND)

        self.panel.SetSizer(self.hbox)
        self.hbox.Fit(self)
        self.kartId = 0
        self.setKart("aaa")
        self.OnRenameKart(None)

    def ChangeCurrFiller(self, event):
        self.valId = event.GetSelection()

    def ChangeKart(self, event):
        self.kartId = event.GetSelection()

    def ChangeKartList(self, event):
        pass

    def on_pick(self, event):
        olddat = self.data[self.currKart][event.ind[0]]
        if olddat == self.clonedata:
            self.data[self.currKart][event.ind[0]] = ""
        else:
            self.data[self.currKart][event.ind[0]] = self.clonedata
        self.draw_figure()

    def draw_figure(self):
        u""" Redraws the figure"""
        ax = self.axes
        ax.clear()
        ax.grid(1)

        dat = self.data[self.currKart]

        gm = self.geom

        if gm.symmetry == 6:
            r = 1.1547/2  # 2/sqrt(3)
            angle = 0
        else:
            r = 1.4/2  # 2/sqrt(3)
            angle = math.pi/4
        patches = []
        fig = self.fig

        xmin = 0
        ymin = 0
        xmax = 0
        ymax = 0

        i = 0
        for y, x in gm.FIterYX():
            e = pat.RegularPolygon((
                x, y), gm.symmetry, radius=r, orientation=angle)
            patches.append(e)
            e = ax.text(x, y, str(dat[
                        i]), horizontalalignment='center', verticalalignment='center')
            xmin = min(xmin, x)
            ymin = min(ymin, y)
            xmax = max(xmax, x)
            ymax = max(ymax, y)
            i += 1

        rect = [xmin-1, xmax+1, ymin-1, ymax+1]
        poly = matplotlib.collections.PatchCollection(
            patches, cmap=matplotlib.cm.jet, picker=5)
        ax.add_collection(poly)
        ax.axis(rect)

# ������ ���������
        colordict = dict([(nm, float(i)) for i, nm in enumerate(
            [""]+self.list_box_kart_items.GetStrings())])
        farr = np.array([colordict.get(nm, 0.) for nm in dat])
        poly.set_array(farr)
        self.canvas.draw()

    def flash_status_message(self, msg, flash_len_ms=1500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER,
            self.on_flash_status_off,
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)

    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')

    def OnInsertKart(self, event):
        dlg = wx.TextEntryDialog(
            self, u'�������� ��� ��� �����������',
            u'�������� ����� �����������', 'Python')
        dlg.SetValue("")
        if dlg.ShowModal() == wx.ID_OK:
            nm = dlg.GetValue()
            if nm not in self.keys.keys():
                self.kart = nm
                self.flash_status_message(u"�������� �����������: "+nm)
            else:
                Msg(self, u"����������� � ����� ������ ��� ����")
        dlg.Destroy()

    def OnDeleteKart(self, event):
        nm = self.kart
        self.data.pop(nm)
        self.keys.pop(nm)
        self.UpdateKartList()

    def OnRenameKart(self, event):
        dlg = wx.TextEntryDialog(
            self, u'�������� ����� ��� ��� �����������',
            u'��������������', 'Python')
        nm = self.kart
        dlg.SetValue(nm)
        if dlg.ShowModal() == wx.ID_OK:
            newnm = dlg.GetValue()
            if newnm not in self.keys.keys():
                v = self.data.pop(nm)
                self.data[newnm] = v
                v = self.keys.pop(nm)
                self.keys[newnm] = v
                self.UpdateKartList(newnm)
                self.flash_status_message(u"������������� �����������: "+nm)
            else:
                Msg(self, u"����������� � ����� ������ ��� ����")
        dlg.Destroy()

    def OnInsertVal(self, event):
        u"""���������� ������ ��������"""
        dlg = wx.TextEntryDialog(
            self, u'�������� ����� ��������',
            u'���������� ������ ��������', 'Python')
        dlg.SetValue("")
        if dlg.ShowModal() == wx.ID_OK:
            nm = evex(dlg.GetValue())
            if nm not in self.keys.values():
                li = self.keys[self.kart]
                li.append(nm)
                li.sort()
                self.keys[self.kart] = li
                self.UpdateKartList(self.kart)
                self.flash_status_message(u"�������� ��������: "+nm)
            else:
                Msg(self, u"����� ��� ��� ����")
        dlg.Destroy()

    def OnDeleteVal(self, event):
        u"""�������� ��������"""
        ival = self.valId  # ��������� ������� ������

        kart = self.kart  # ����� ������� �����������
        li = self.keys[kart]  # �������� ������ ��������� ��������
        if len(li) > 1:
            v = li.pop(ival)
            self.keys[kart] = li  # ������� ������ �� ������ ��������
            dat = self.data[kart]
            self.data[kart] = ["" if v == i else i for i in dat]
            newpos = ival-1 if ival != 0 else 0
            self.UpdateValueList(newpos)
            self.draw_figure()

    def OnRenameVal(self, event):
        u"""�������������� ������ ��������"""
        pass


class TVarSrc(object):
    u""""""
    def __init__(self):
        u""""""
        self.tmin
        self.tmax
        self.varnames = {}
        self.dsc = {}
        self.units = {}


class Tvarappl(object):
    u""""""
    pass


if __name__ == '__main__':
    app = wx.PySimpleApp(redirect=True)
    keys = {}
    keys["fuel"] = ["a", "b", "c"]
    keys["suz"] = map(str, range(10))
    data = {}
    data["fuel"] = range(163)
    app.frame = PlotFrame(geom.Wwer1000(), keys, data)
    app.frame.Show()
    app.MainLoop()
