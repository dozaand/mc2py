#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
����������� ������ �����������
"""

import numpy as np

betkln = np.array([0.00021, 0.0014, 0.00126,
                  0.00252, 0.00074, 0.00027], dtype='f')
lamkln = np.array([0.0124398, 0.0305082,
                  0.111438, 0.301368, 1.13631, 3.01368], dtype='f')
Lkaln = 1e-3


def AssignParams(self, bet, lam, L):
    if type(bet) is list:
        bet = np.array(bet)
    if type(lam) is list:
        lam = np.array(lam)
    assert(len(lam) == len(bet))
    self.betts = bet
    self.bet = np.sum(bet)
    self.lamda = lam
    self.L = L


class TReactim(object):
    u"""����������� ����������"""
    @property
    def dt(self):
        u"""��� ��������� ������ �� �������"""
        return self._dt

    @dt.setter
    def dt(self, dt):
        self._dt = dt
        ldt = self.lamda*dt
        eldt = np.exp(-ldt)
        self.k1 = eldt
        self.k2 = (1-eldt)/ldt

    def __init__(self, bet=betkln, lam=lamkln, L=Lkaln):
        u"""������������� �����������"""
        AssignParams(self, bet, lam, L)
        self.a = bet/self.bet
        self.nbet = len(bet)

        self.psi = np.zeros(self.nbet, dtype='f')
        self.Iold = 1
        self._dt = 0
        self.dt = 0.25

    def __call__(self, I):
        assert(I >= 0)
        di = I-self.Iold
        if self.Iold < 0:
            self.psi = I
        else:
            self.psi = (self.psi-self.Iold)*self.k1-self.k2*di+I
        self.Iold = I
        self.ro = 1.-np.sum(self.a*self.psi)/self.Iold
        return self.ro

    def Flush(self, I):
        u"""��������� ����������� � ����������� ��������� ��� �������� ������� �������"""
        self.psi = I
        self.Iold = I


class TtrReact(object):
    u"""���������� � ���������� ������ ������� ����� ��������"""
    def __init__(self, bet=betkln, lam=lamkln, L=Lkaln):
        AssignParams(self, bet, lam, L)

    @property
    def dt(self):
        u"""��� ��������� ������ �� �������"""
        return self._dt

    @dt.setter
    def dt(self, dt):
        self._dt = dt
        denom = 1./(1+dt*self.lam)
        self.k0 = denom
        self.k1 = self.betts*denom*(dt/self.L)

    def __call__(self, w):
        self.c *= self.k1
        self.c += self.k0*w
        w0 = self.w
        self.ro = self.bet+(self.L/w)*((w-w0)/dt-np.dot(self.c, self.betts))
        self.w = w0
        return self.ro

    def Flush(self, w):
        u"""��������� ����������� � ����������� ��������� ��� �������� ������� �������"""
        self.w = w
        self.c = (w/self.L)*self.betts/self.lam


class TpkGenerator(object):
    u"""��������� ������� �������� ��������"""
    def __init__(self, bet=betkln, lam=lamkln, L=Lkaln):
        AssignParams(self, bet, lam, L)
        self.Flush(1)

    def __call__(self, ro):
        mult = (dt/self.L)*self.lam/(1+dt*self.lam)
        p = mult*self.c*self.L+w0
        w = p/(1-dt*((ro-self.bet)/self.L+np.dot(mult, self.betts)))
        c = self.c
        self.c = (c*self.L + self.betts*dt*w)/(self.L*(1 + dt*self.lam))
        self.w = w
        return w

    def Flush(self, w):
        u"""��������� ����������� � ����������� ��������� ��� �������� ������� �������"""
        self.w = w
        self.c = (w/self.L)*self.betts/self.lam


def Reactimtest(I):
    r = TReactim(bet=[0.00021, 0.0014, 0.00126, 0.00252, 0.00074, 0.00027])
    r.dt = 0.25
    r.Flush(I[0])
    return [r(i) for i in I]
# print Reactimtest([1]*10)
# print Reactimtest([1,2,3,4,5,6])
