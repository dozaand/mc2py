#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
this module extends matplotlib plot to plot functions

"""

import numpy as np
from pylab import plot, show


def PlotPointsInner(f, mi, ma, fmi, fma, globalMin, globalMax, epsilon=0.01):
    u"""выбор точек для отрисовки функии выбираем точки так, чтобы получить погрешность меньше заданной"""
    x2 = (mi+ma)/2
    fnew = f(x2)
    if fnew < globalMin:
        globalMin = fnew
    if fnew > globalMax:
        globalMax = fnew
    delta = globalMax-globalMin

    if abs((fmi+fma)/2-fnew) > (delta*epsilon):
        (a, b) = PlotPointsInner(f, mi, x2, fmi,
                                 fnew, globalMin, globalMax, epsilon)
        (c, d) = PlotPointsInner(f, x2, ma,
                                 fnew, fma, globalMin, globalMax, epsilon)
        return (a+c, b+d)
    else:
        return ([mi, x2, ma], [fmi, fnew, fma])


def PlotPoints(f, mi, ma, epsilon=0.01):
    u"""Из функции и области определения делаем наборы точек для отрисовки"""
    fmi = f(mi)
    fma = f(ma)
    globalMin = min(fmi, fma)
    globalMax = max(fmi, fma)
    if fmi == fma:
        fma += max(1e-6, (1e-6*fma))
    (a, b) = PlotPointsInner(f, mi, ma,
                             fmi, fma, globalMin, globalMax, epsilon)
    return (np.array(a), np.array(b))

#(x,y)=PlotPoints(np.sin,0.,5.)


def fplot(*args, **kvargs):
    if "range" in kvargs:
        rg = map(float, kvargs["range"])
        del kvargs["range"]
    if "eps" in kvargs:
        eps = kvargs["eps"]
        del kvargs["eps"]
    else:
        eps = 0.01
    atr = []
    for i in args:
        if hasattr(i, "__call__"):
            atr.extend(PlotPoints(i, rg[0], rg[1], eps))
        else:
            atr.append(i)
    atr = tuple(atr)
    plot(*atr, **kvargs)

# fplot([0,1,2,3],[4,5,6,7],np.sin,"o",np.cos,"--",np.tan,range=[0,7],eps=0.01)
# show();


# plot(x,y);show()
# def main():
#    pass

# if __name__ == '__main__':
#	main()
