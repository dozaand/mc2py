#!/usr/bin/env python
# -*- coding: cp1251 -*-

import cPickle
# try:
from opc.mopc import *
# except:
#    raise Exception("fail to import giw - try to run giw exe")
import numpy as np
import re
import mc2py.fold
import random
from copy import copy

# ������� ���� ����������
import mc2py.bgk as bgk


class giwstate:
    _alloc = 1
    _free = 2
    _fix = 3
    _restore = 4

    def __init__(self):
        #v.YMGIWSTATECMD = giwstate._alloc
        #RET()
        #self.id = v.YMGIWSTATEID
        pass

    def fix(self):
        #v.YMGIWSTATEID = self.id
        #v.YMGIWSTATECMD = giwstate._fix
        #RET()
        v.YSAVE_FLAG_INPUT = 1
        RET(5)

    def restore(self):
        #v.YMGIWSTATEID = self.id
        #v.YMGIWSTATECMD = giwstate._restore
        #RET()
        v.YLOAD_FLAG_INPUT = 1
        RET(5)


def BurnTo(t, w=100):
    """��������� �� �������� ����������� ����� �������� ��� �������� w"""
    v.YMWSTATN = w
    v.YMFLAGSTAT = 1
    v.YZBORMODE = 2
    oldfast = v.YMFAST
    v.YMFAST = 50000
    v.YMFLAG_BRN = 1
    SetHgrp3([1, 1, 1])
    RET()
    while v.YMTIME_BRN < t and v.YMBOR_COR > 0.1:
        RET()
    v.YMFLAG_BRN = 0
    v.YMFAST = oldfast
    if v.YMTIME_BRN < t:
        print v.YMBOR_COR, v.YMTIME_BRN, t
        raise Exception("fail burn to")


def MoveTo(H_new, NH=10, Hnums=[-3,-2,-1], f=None):
    H = np.array(GetHgrp3(Hnums))
    step = 20
    shag = copy(step)
    step = (np.array(H_new)-H)/step
    while shag != 0:
        H += step
        _SetHgrp3(H, NH, Hnums)
        v.YSREGRPINS_SELOE = 1
        shag -= 1
        RET()
        if f != None:
            f()
    v.YMTIME = 0
    RET()


def Nstp(n):
    for i in xrange(n):
        try:
            RET()
        except:
            pass


def Kz():
    """������������� ������������� �������� �� ������"""
    fi = v.YMFISF
    vv = fi.reshape((-1, 10))
    u = np.ones(163, dtype='f')
    r = np.dot(vv.T, u)
    return r/sum(r)


def KzW(Wnom=3e9, Nz=10):
    """������������� �������� �� ������ (�� �� ����)"""
    erheat = 0.925
    fi = v.YMFISF * erheat
    vv = fi.reshape((-1, Nz))
    u = np.ones(163, dtype=np.float64)
    r = np.dot(vv.T, u)
    return r/Wnom


def RelaxCore():
    """������������ ���������� ��� ����"""
    borold = v.YZBORMODE
    v.YZBORMODE = 2
    statold = v.YMFLAGSTAT
    v.YMFLAGSTAT = 1
    i = 0
    fastold = v.YMFAST
    v.YMFAST = 100000
    while abs(v.YMDRNEW) > 3e-6:
        try:
            RET()
        except:
            pass
        i += 1
        if i >  500:
            v.YMFAST = fastold
            try:
                RET()
            except:
                pass
            raise Exception("fail to relax core")
    v.YMFAST = fastold
    v.YMFLAGSTAT = statold
    v.YZBORMODE = borold


def IcamRD(plant='kaln'):
    """
    ���������� ���� ����� �������� � ����. ���������
    ��������� ������
    ������ ������� - ����� ���������
    ������ ������� - ����� ������
    ������ ������� - ����� ������ (����� �����)
    """
    camcan = None
    complect1 = None
    complect2 = None
    if plant == "kaln":
        camcan = [1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 3, 1, 1, 1, 1,
                  3, 1, 1, 1, 1, 3, 1, 3, 1, 1, 3]  # ���������� ����� � ������
        complect1 = [2, 12, 22]
        complect2 = [7, 17, 27]
        res = [i for i in mc2py.fold.NSplit(v.YQ_I_AKNP, camcan)]
        #raise Exception('Must to correct IcamRD for kaln')
    elif plant == "novo2":
        camcan = [3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1]  # ���������� ����� � ������
        complect1 = [1, 5, 9, 13]
        complect2 = [3, 7, 11, 15]
        res = v.YQ_I_AKNP_CHAN

    return np.array([[res[i-1] for i in complect1], [res[i-1] for i in complect2]], dtype=np.float64)


def set_upz():
    v.YS06S31_GMV = 182
    v.YS08S25_GMV = 182
    v.YS10S31_GMV = 182
    v.YS06S31_GM09 = 1
    v.YS08S25_GM09 = 1
    v.YS10S31_GM09 = 1
    RET()


def SetMFADrive(W=None, hgrp=None, Tin=None, G=None, block=None, kamp=None, plant=None, burn=None, static=1):
    """��������� ��������� ������� ������
    W [%]
    hgrp [%]
    Tin [C]
    G [T/h]
    static - ���� 1 �� ������ ����������� ����� ����� ������ �������� (�� ��� �������)
    """
#    loadedplant=MFAGet("plant_name")[:4]
    doLoadCamp = 0
    needRelax = 0
    if static:
        v.YMFLAGSTAT = 1
    else:
        v.YMFLAGSTAT = 0
        v.YMFAST = 1

    if block:
        if block != v.YMBLOCKNUMBER0:
            doLoadCamp = 1
    else:
        block = v.YMBLOCKNUMBER0

    if kamp:
        if kamp != v.YMLOADNUMBER:
            doLoadCamp = 1
    else:
        kamp = v.YMLOADNUMBER

    if doLoadCamp:
        v.YM_N_KAMP_TO_LOAD = kamp
        v.YMBLOCKNUMBER_TO_LOAD = block
        v.YM_XIPI_LDBRNBEG = 1
        needRelax = 1

    if burn and abs(burn-v.YMTIME_BRN) > 0.2:
        BurnTo(burn)
    if hgrp != None:
        v.YSREGRPINS_SELOE = 1
        hgrp = np.array(hgrp)
        if static:
            v.YSHGRP = hgrp
            needRelax = 1
        else:
            nstep = int(max(abs(hgrp-v.YSHGRP))/(4*2./355))+1
            if nstep <= 1:
                nstep = 2
            delta = (hgrp-v.YSHGRP)/(nstep-1)
            h0 = v.YSHGRP
            for i in xrange(nstep):
                v.YSHGRP = h0+i*delta
                RET()

    if Tin != None:
        tmp = v.YHTIN_LOOP
        tmp[:4] = np.array(Tin)
        v.YHTIN_LOOP = tmp
        needRelax = 1
    if G != None:
        tmp = v.YHGIN_LOOP
        tmp[:4] = np.array(G)
        v.YHGIN_LOOP = tmp
        needRelax = 1
    if W:
        v.YMWSTATN = W
        v.YMINTPOW_SET = W
        v.YMFLAGSTAT = 1
        v.YZBORMODE = 2
        needRelax = 1
    if static:
        try:
            RET()
            RET()
        except:
            pass
        if needRelax:
            RelaxCore()
    else:
        v.YMFLAGSTAT = 0
        try:
            RET()
        except:
            pass

# ��������������� ������ �������� ��������������
def GetDp():
    """�������� �������� �� ��� [��]"""
    return np.array([v.Y314B01, v.Y315B01, v.Y316B01, v.Y317B01])*1e5


def GetTout():
    """�������� ����������� �� ������ [C]"""
    #return np.array([v.BiX1PETLi_Temp, v.BiX2PETLi_Temp, v.BiX3PETLi_Temp, v.BiX4PETLi_Temp])
    return [v.YA11T810, v.YA12T811, v.YA13T812, v.YA14T813]


def GetTin():
    """������� ����������� �� ������ [C]"""
    #return v.yhT_leg
    return [v.YA11T782, v.YA12T783, v.YA13T784, v.YA14T785]


def SetTin(val):
    """������� ����������� �� ������ [C]"""
    v.YHTIN_LOOP = val


def GetG():
    """������ �� ������ [����/���]"""
    return v.yhG_leg


def SetG(val):
    """������ �� ������ [����/���]"""
    v.YHGIN_LOOP = val


def _SetHgrp3(v3, NH=10, Hnums=[-3,-2,-1]):
    """��������� ��������� �����"""
    val = v.YSHGRP
    for el, num in zip(v3, Hnums):
        val[num] = el
    v.YSREGRPINS_SELOE = 1
    v.YSHGRP = val

def SetHgrp3(v3, NH=10, Hnums=[-3,-2,-1]):
    """��������� ��������� �����"""
    _SetHgrp3(v3, NH, Hnums)
    #MoveTo(v3, NH, Hnums)

def GetHgrp3(Hnums=[-3,-2,-1]):
    """��������� ��������� �����"""
    res = []
    suz = v.YSHGRP
    for num in Hnums:
        res.append(suz[num])
    return res


def RandomRod3():
    """��������� ��������� ��������� ��������"""
    hr = np.random.uniform(0, 1, 3)
    if hr[0] < 0.5:
        hr[0] /= 10
    else:
        v = 1-hr[0]
        hr[0] = 1-v/10
    val = np.ones(10, dtype='d')
    val[-3:] = hr[:]
    return val


class Tbgkdata:
    """��������� ��� ���������� ������ �� BGK"""
    #bgkvectrs = """YMFISF YMRP_POW YHTFU_F0 YHTMIX_F0 YMRO_XEN YMRO_JOD YMRO_PRO YMRO_SAM
    #YMZPOLD1 YMZPOLD2 YMZPOLD3 YMZPOLD4 YMZPOLD5 YMZPOLD6
    #YMRP_C01 YMRP_C02 YMRP_C03 YMRP_C04 YMRP_C05 YMRP_C06 YMRP_C07 YMRP_C08 YMRP_C09 YMRP_C10 YMRP_C11 YMRP_C12""".split()

    bgkvectrs = """YMFISF YMRP_POW YMRO_XEN YMRO_JOD""".split()

    def __init__(self):
        self.data = {}
        self.bgk = {}
        for i in self.bgkvectrs:
            self.data[i] = []

    def append(self):
        for i in self.bgkvectrs:
            dat = getattr(v, i)
            self.data[i].append(dat)

    def ClearData(self):
        for i in self.bgkvectrs:
            self.data[i] = []

    def flush(self):
        for i in self.bgkvectrs:
            self.data[i] = []
        self.bgk = {}

    def MakeBgk(self, nv=30):
        """����������� ������ � ���"""
        for i in self.bgkvectrs:
            self.bgk[i] = bgk.Bgk(self.data[i], nv)
            self.data[i] = []
        return self.bgk

    def Errors(self, j=0):
        """������ ������������� ����������� ������� �����"""
        return dict([(i, bgk.NprNorm(self.data[i][j], bgk.Residual(self.bgk[i], self.data[i][j], np.dot))) for i in self.bgkvectrs])


def SetCbRegulator(state, dynamic=1):
    """��������� ��������� ������� ����������
    state - �������� -1
    dynamic - ����������� ������� �� �������� v.YMINTPOW_SET � ��������
    """
    if state:
        v.YZBORMODE = 2
        #v.YMINTPOW_SET_FLAG = dynamic
    else:
        v.YZBORMODE = 0
        #v.YMINTPOW_SET_FLAG = dynamic
