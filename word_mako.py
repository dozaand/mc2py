#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import subprocess as sp
import codecs
import yaml
import argparse
from contextlib import contextmanager
import shutil

from mako.template import Template
from mako.lookup import TemplateLookup
import re

#in_file = "example.docx"
#in_file = "Doc1.docx"
#out_file = "example_prc.docx"
#data_file = "a.yaml"

def join_contents(text):
    text=re.sub(u"""[‘’]""",u"""'""",text.group(0))
    mygroup=u"".join(re.findall(ur"<w:t[^<]*>(.+?)</w:t>",text,re.M))
    return re.sub(ur"<w:t[^<]*>(.+)</w:t>",ur"<w:t>"+mygroup+"</w:t>",text,flags=re.M|re.DOTALL)

def word_srunt(text):
    u"""ищем шаблоны мако и собираем вместе"""
    return re.sub(ur"<w:t[^<]*> *\$\{[^}]+\} *</w:t>",join_contents,text,flags=re.M|re.DOTALL)
#    return re.sub(ur"<w:sdtContent>(.+?)</w:sdtContent>",join_contents,text,flags=re.M|re.DOTALL)

ipict=0
itable=0
ichapt=0
isect=0
isubsect=0
joined_data={}

def figure(label=None,caption=None):
    global ipict
    global ichapt
    global isect
    global isubsect
    ipict+=1
    _ichapt = unicode(ichapt) if ichapt else ""
    _isect = u"."+unicode(isect) if isect else ""
    _isubsect = u"."+unicode(isubsect) if isubsect else ""
#    return u"Рисунок "+unicode(ipict)
    return u"Рисунок "+_ichapt+_isect+_isubsect+"."+unicode(ipict)

def table(label=None,caption=None):
    global itable
    global ichapt
    global isect
    global isubsect
    itable+=1
    _ichapt = unicode(ichapt) if ichapt else ""
    _isect = u"."+unicode(isect) if isect else ""
    _isubsect = u"."+unicode(isubsect) if isubsect else ""
#    return u"Рисунок "+unicode(ipict)
    return u"Таблица "+_ichapt+_isect+_isubsect+"."+unicode(itable)

def chapter(text=None,number=None):
    global ipict
    global ichapt
    global isect
    global isubsect
    if number:
        ichapt=number-1
    isect=0
    ipict=0
    isubsect=0
    ichapt+=1
    if text:
       return u"{num} {text}".format(num=ichapt,text=text)
    else:
       return u""
#    return u""

def section(text=None,number=None):
    global ipict
    global ichapt
    global isect
    global isubsect
    if number:
        isect=number-1
    isubsect=0
    isect+=1
    return u""

def subsection(text=None,number=None):
    global ipict
    global ichapt
    global isect
    global isubsect
    if number:
        isubsect=number-1
    isubsect+=1
    return u""


@contextmanager
def docx_dir(file_name):
    try:
        dir_name=os.path.splitext(file_name)[0]+"_unpacked"
        sp.check_call("7z x -y {ifi} -o{dir_name}".format(ifi = file_name,dir_name=dir_name))
        yield dir_name
    finally:
        shutil.rmtree(dir_name)

def make_data_dict(data_file):
    global joined_data
    if(data_file):
        with codecs.open(data_file,"r",encoding='utf-8') as f:
            data =yaml.load(f)
    else:
        data={}
    joined_data=dict(chapter=chapter,section=section,subsection=subsection,figure=figure,include=include,
                     table=table,ipict=ipict,itable=itable,ichapt=ichapt,isect=isect,isubsect=isubsect)
    joined_data.update(data)

def subst_data(cont):
    """ подстановка данных"""
    global joined_data
    cont_j=word_srunt(cont)
    tpl=Template(cont_j)
    cont1=tpl.render_unicode(**joined_data)
    return cont1

def include(file_name):
    with docx_dir(file_name) as word_dir:
        contents=os.path.join(word_dir,"word/document.xml")
        with codecs.open(contents,"r",encoding = "utf-8") as f:
            cont=f.read()
        body=re.search(ur"<w:body>(.+)</w:body>",cont,re.M|re.DOTALL).group(1)
        body1 = subst_data(body)
    return body1

def convert(in_file, out_file, data_file,rmdir=0):
    make_data_dict(data_file)
    with docx_dir(in_file) as word_dir:
        contents=os.path.join(word_dir,"word/document.xml")
        with codecs.open(contents,"r",encoding = "utf-8") as f:
            cont=f.read()
        cont1=subst_data(cont)
        with codecs.open(contents,"w",encoding = "utf-8") as f:
            f.write(cont1)

        os.chdir(word_dir)
        sp.check_call("7z a -r -tzip ../{ifi} *.*".format(ifi = out_file),shell=1)
        os.chdir("..")


#convert(in_file, out_file, data_file)
# интерфейс для консоли
if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="word_mako",description="substitute data to word_mako infile outfile")
    parser.add_argument("-d","--data",default="",help=u"dile with data")
    parser.add_argument("-o","--output",help=u"output file")
    parser.add_argument("file",help=u"input file")
    args = parser.parse_args()
    convert(args.file,args.output,args.data)

