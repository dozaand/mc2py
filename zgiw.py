#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mc2py.zmq_script_client import *
import numpy as np
import re
import mc2py.fold
import random
import time
from copy import copy


# импорты моих алгоритмов
import mc2py.bgk as bgk

#socket.RCVTIMEO = 3000  # время ожидания данных

def getv(ip="localhost", port="5555"):
    return Tscript_drv(f"tcp://{ip}:{port}")

def getv1():
    return Tscript_drv("tcp://localhost:5556")


class ModelError(Exception):
   def __init__(self, txt):
      Exception.__init__(self, txt)



class giwstate:
    _alloc = 1
    _free = 2
    _fix = 3
    _restore = 4

    def __init__(self):
        self.v = getv()
        self.v.YMGIWSTATECMD = giwstate._alloc
        self.v.step()
        self.id = float(self.v.YMGIWSTATEID)

    def fix(self):
        self.v.YMGIWSTATEID = self.id
        self.v.YMGIWSTATECMD = giwstate._fix
        self.v.step()

    def restore(self):
        self.v.YMGIWSTATEID = self.id
        self.v.step()
        self.v.YMGIWSTATECMD = giwstate._restore
        self.v.step()



def BurnTo(t, w=100, fast = 2500, vsoc=None):
    """выгорание на заданные эффективные сутки вгорание при мощности w"""
    if vsoc is None: vsoc = getv()
    vsoc.YMFLAGSTAT = 1
    vsoc['#YM#YMFLAGSTAT'] = 1
    vsoc.YMWSTATN = w
    vsoc.YMINTPOW_SET = w
    vsoc.YMINTPOW_SET_FLAG = True
    vsoc.YZBORMODE = 2
    oldfast = float(vsoc.YMFAST)
    # vsoc.YMFAST = 10000
    #vsoc.YMFAST = 50000
    vsoc["YMFAST"] = fast
    print(vsoc["YMFAST"])
    vsoc.step()
    vsoc.ymrp_equilib_ = 1
    while float(vsoc.ymrp_counter) != 0: vsoc.step()
    vsoc.YMFLAG_BRN = 1
    vsoc["#YM#YMFLAG_BRN"]=1
    vsoc.YZREGRPINS_KEY = 1
    SetHgrp3([1, 1, 1])
    vsoc.step()
    vsoc["#YM#YMFLAG_BRN"]=1
    vsoc.YMFLAG_BRN = 1
    # while float(vsoc.YMTIME_BRN) < t and float(vsoc.YMBOR_COR) > 0.05:
    #     vsoc.step()
    while float(vsoc.YMTIME_BRN) < t: vsoc.step()
    vsoc.YMFLAG_BRN = 0
    vsoc["#YM#YMFLAG_BRN"]=0
    vsoc.YMFAST = oldfast
    # if float(vsoc.YMTIME_BRN) < t:
    #     raise Exception("fail burn to")



def MoveTo(H_new, NH=10, Hnums=[-3,-2,-1], f=None):
    v = getv()
    H = np.array(GetHgrp3(Hnums))
    step = 20
    shag = copy(step)
    step = (np.array(H_new)-H)/step
    while shag != 0:
        H += step
        _SetHgrp3(H, NH, Hnums)
        v.YZREGRPINS_KEY = 1
        v.YSREGRPINS_SELOE = 1
        shag -= 1
        v.step()
        if f != None:
            f()
    v.YMTIME = 0
    v.step()


def Nstp(n):
    v = getv()
    v.step(n)


def Kz():
    """Относительное распределение мощности по высоте"""
    fi = v.YMFISF[...]
    vv = fi.reshape((-1, 10))
    u = np.ones(163, dtype='f')
    r = np.dot(vv.T, u)
    return r/sum(r)


def KzW(Wnom=3e9, Nz=10):
    """Распределение мощности по высоте (Вт на слой)"""
    v = getv()
    erheat = 0.925
    fi = v.YMFISF[...] * erheat
    vv = fi.reshape((-1, Nz))
    u = np.ones(163, dtype=np.float64)
    r = np.dot(vv.T, u)
    return r/Wnom


def RelaxCore():
    """установление равновесия для зоны"""
    v = getv()
    v.syncro(1)
    # v["#YV#YHFLAG_NKS"]=1
    # v.YZKEYLINK2 = 1
    v.step()
    borold = float(v.YZBORMODE)
    v.YZBORMODE = 2
    statold = float(v.YMFLAGSTAT)
    v.YMFLAGSTAT = 1
    v['#YM#YMFLAGSTAT'] = 1
    """v.YMINTPOW_SET = 100.0
    v.YM_STROBE_POWSET = 1
    v.step(n=5)
    v.YZBORMODE = 2
    v.step()
    v.YZBORMODE = 1
    v.step()
    v.YZBORMODE = 2
    v.step()"""
    i = 0
    fastold = float(v.YMFAST)
    v.YMFAST = 5000
    while abs(float(v.YMDRNEW)) > 3e-5:
        try:
            v.step()
        except:
            pass
        i += 1
        if i > 100:
            v.YMFAST = float(fastold)
            try:
                v.step()
            except:
                pass
            raise Exception("fail to relax core")
    v.YMFAST = fastold
    v.YMFLAGSTAT = statold
    v['#YM#YMFLAGSTAT'] = statold
    v.YZBORMODE = borold
    v.step(n=10)


def IcamRD(vsoc=None, plant='novo2'):
    """
    возвращаем токи камер рабочего и пром. диапазона
    вложенные списки
    первый уровень - номер комплекта
    второй уровень - номер канала
    третий уровень - номер камеры (снизу вверх)
    vsoc : клиент для скрипт-сервера в модели
    """
    if vsoc is None: vsoc = getv()
    camcan = None
    complect1 = None
    complect2 = None
    if plant == "kaln":
        camcan = [1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 3, 1, 1, 1, 1,
                  3, 1, 1, 1, 1, 3, 1, 3, 1, 1, 3]  # количество камер в канале
        complect1 = [2, 12, 22]
        complect2 = [7, 17, 27]
        res = [i for i in mc2py.fold.NSplit(vsoc["YQ_I_AKNP"][...], camcan)]
        #raise Exception('Must to correct IcamRD for kaln')
    elif plant in """novo2 laes2 rupur akku""".split():
        camcan = [3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1]  # количество камер в канале
        complect1 = [1, 5, 9, 13]
        complect2 = [3, 7, 11, 15]
        res_=vsoc["YQ_I_AKNP_CHAN"][...].reshape((3,30)).T
        res = res_

    return np.array([[res[i-1] for i in complect1], [res[i-1] for i in complect2]], dtype=np.float64)


def set_upz(is_return_flag=True):
    v = getv()
    v.syncro(1)
    v["10JDA06S31_GM05"] = 1
    v["10JDA08S25_GM05"] = 1
    v["10JDA10S31_GM05"] = 1
    v["10JDA06S31_GMV"] = 190
    v["10JDA08S25_GMV"] = 190
    v["10JDA10S31_GMV"] = 190

    v.YZREGRPINS_KEY = 0

    # Снятие шунтов
    if v['#YV#KEY_SHUNT_URB'][...] % 2:
        v['#YV#KEY_SHUNT_URB'] += 1
    # if v['#YV#KEY_SHUNT_PZ1'][...] % 2:
    #     v['#YV#KEY_SHUNT_PZ1'] += 1
    # if v['#YV#KEY_SHUNT_PZ2'][...] % 2:
    #     v['#YV#KEY_SHUNT_PZ2'] += 1
    if v['#YV#KEY1_SHUNT_AZ1'][...] % 2:  # Шунт на АЗ. С шунтом на АЗ не ,будет срабатывать упз по кнопочке
        v['#YV#KEY1_SHUNT_AZ1'] += 1

    v.step()
    try: v['11JDY01CH403_SBV1'] = 1
    except Exception: v['12JDY02CH404_S1'] = 1 # Если Кнопки 11JDY01CH403_SBV1 нет в модели, иницируем срабатывание УПЗ через схемы control_ro
    v.step(n=10)
    if is_return_flag:
        v.YZREGRPINS_KEY = 1

    # Установка шунтов
    if not v['#YV#KEY_SHUNT_URB'][...] % 2:
        v['#YV#KEY_SHUNT_URB'] += 1
    if not v['#YV#KEY_SHUNT_PZ1'][...] % 2:
        v['#YV#KEY_SHUNT_PZ1'] += 1
    if not v['#YV#KEY_SHUNT_PZ2'][...] % 2:
        v['#YV#KEY_SHUNT_PZ2'] += 1
    if not v['#YV#KEY1_SHUNT_AZ1'][...] % 2:
        v['#YV#KEY1_SHUNT_AZ1'] += 1
    # v["CLRRP01A_GM"] = True
    # v["CLRRP01B_GM"] = True
    # v["CLADSZAZ_GM"] = True
    # v["CLBDSZAZ_GM"] = True


    # v['11JDY01CH403_SBV1'] = 0
    try: v['11JDY01CH403_SBV1'] = 0
    except Exception: v['12JDY02CH404_S1'] = 0 # Если Кнопки 11JDY01CH403_SBV1 нет в модели, иницируем срабатывание УПЗ через схемы control_ro
    v.step(1)


def SetMFADrive(W=None, hgrp=None, Tin=None, G=None, block=None, kamp=None, plant=None, burn=None, static=1, v=None):
    """Установка граничных условий модели
    W [%]
    hgrp [%]
    Tin [C]
    G [T/h]
    static - если 1 то делать статический режим иначе просто динамика (но без падения)
    """
    if v is None: v = getv()
    doLoadCamp = 0
    needRelax = 0
    if static:
        v.YMFLAGSTAT = 1
    else:
        v.YMFLAGSTAT = 0
        v.YMFAST = 1

    if block:
        if block != float(v.YMBLOCKNUMBER0):
            doLoadCamp = 1
    else:
        block = float(v.YMBLOCKNUMBER0)

    if kamp:
        if kamp != float(v.YMLOADNUMBER):
            doLoadCamp = 1
    else:
        kamp = float(v.YMLOADNUMBER)

    if doLoadCamp:
        v.YM_N_KAMP_TO_LOAD = kamp
        v.YMBLOCKNUMBER_TO_LOAD = block
        v.YM_XIPI_LDBRNBEG = 1
        needRelax = 1

    if burn and abs(burn-float(v.YMTIME_BRN)) > 0.2:
        BurnTo(burn, w=W)

    if hgrp is not None:
        v.YZREGRPINS_KEY = 1
        v.YSREGRPINS_SELOE = 1
        hgrp = np.array(hgrp)
        if static:
            # v.YSHGRP = hgrp
            v["#YS#YSHGRP"] = hgrp
            needRelax = 1
        else:
            nstep = int(max(abs(hgrp-v.YSHGRP[...]))/(4*2./355))+1
            if nstep <= 1:
                nstep = 2
            delta = (hgrp-v.YSHGRP[...])/(nstep-1)
            h0 = v.YSHGRP[...]
            for i in xrange(nstep):
                # v.YSHGRP = h0+i*delta
                v["#YS#YSHGRP"] = h0+i*delta
                v.step()
    if Tin != None:
        tmp = v.YHTIN_LOOP[...]
        tmp[:4] = np.array(Tin)
        v.YHTIN_LOOP = tmp
        needRelax = 1
    if G != None:
        tmp = v.YHGIN_LOOP[...]
        tmp[:4] = np.array(G)
        v.YHGIN_LOOP = tmp
        needRelax = 1
    if W:
        v.YMWSTATN = W
        v.YMINTPOW_SET = W
        v.YMINTPOW_SET_FLAG = W
        v.YMFLAGSTAT = 1
        v.YZBORMODE = 2
        v.ymrp_equilib_ = 1
        while float(v.ymrp_counter) != 0: v.step()
        v.step()
        needRelax = 1
    if static:
        try:
            v.step(n=2)
        except:
            pass
        if needRelax:
            # RelaxCore()
            pass
    else:
        v.YMFLAGSTAT = 0
        try:
            v.step()
        except:
            pass


# предварительный расчет напорной характеристики
def GetDp(vsoc=None):
    """перепады давления на ГЦН [Па]"""
    if vsoc is None: vsoc = getv()
    return np.array([float(vsoc.Y314B01), float(vsoc.Y315B01), float(vsoc.Y316B01), float(vsoc.Y317B01)])*1e5

def GetTout(vsoc=None):
    """выходная температура по петлям [C]"""
    if vsoc is None: vsoc = getv()
    return [float(vsoc.SVRK_TGN[0]), float(vsoc.SVRK_TGN[1]), float(vsoc.SVRK_TGN[2]), float(vsoc.SVRK_TGN[3])]

def GetTin(vsoc=None):
    """входная температура по петлям [C]"""
    if vsoc is None: vsoc = getv()
    # Tin_bound = np.mean(vsoc["YHTMIXB_F"][...].reshape((163, 44))[:, 0])
    # return [np.mean(Tin_bound) - 273.15] * 4
    return [float(vsoc["YHTMIX_AV"]) - (16.)]*4
    # return [float(vsoc["YHTMIX_AV"]) - float(vsoc["YHDTMIX_AV"]) / 2.]*4
    # return [float(v.SVRK_THN[0]), float(v.SVRK_THN[1]), float(v.SVRK_THN[2]), float(v.SVRK_THN[3])]

def SetTin(val, vsoc=None):
    """входная температура по петлям [C]"""
    if vsoc is None: vsoc = getv()
    v.YHTIN_LOOP = val

def GetG(vsoc=None):
    """расход по петлям [Тонн/Час]"""
    if vsoc is None: vsoc = getv()
    return v.yhG_leg[...]

def SetG(val, vsoc=None):
    """расход по петлям [Тонн/Час]"""
    if vsoc is None: vsoc = getv()
    v.YHGIN_LOOP = val

def _SetHgrp3(v3, NH=10, Hnums=[-3,-2,-1], v=None):
    """установка положения групп"""
    # 1.03 - столько показывает наша модель на верхнем концевике, СГИУ выдает при этом 1.0
    if v is None: v = getv()
    val = v.YSHGRP[...]
    for el, num in zip(v3, Hnums):
        val[num] = el * 1.03
    v.YZREGRPINS_KEY = 1
    v.YSREGRPINS_SELOE = 1
    # v.YSHGRP = val
    v["#YS#YSHGRP"] = val

def SetHgrp3(v3, NH=10, Hnums=[-3,-2,-1], v=None):
    """установка положения указанной NH-группы"""
    _SetHgrp3(v3, NH, Hnums, v=v)

def SetHgrp3_12gr(v3):
    """установка положения 12-ой группы"""
    v = getv()
    # v.YSHGRP[-3:] = v3
    v["#YS#YSHGRP"][-1] = v3

def GetHgrp3(Hnums=[-3,-2,-1], v=None):
    """получение положения групп"""
    if v is None: v = getv()
    res = []
    # 1.03 - столько показывает наша модель на верхнем концевике, СГИУ выдает при этом 1.0
    suz = v.YSHGRP[...] / 1.03
    for num in Hnums:
        res.append(suz[num])
    return res
    
def RandomRod3():
    """установка случайных положений стержней"""
    hr = np.random.uniform(0, 1, 3)
    if hr[0] < 0.5:
        hr[0] /= 10
    else:
        v = 1-hr[0]
        hr[0] = 1-v/10
    val = np.ones(10, dtype='d')
    val[-3:] = hr[:]
    return val

class Tbgkdata:
    """контейнер для накопления данных по BGK"""
    #bgkvectrs = """YMFISF YMRP_POW YHTFU_F0 YHTMIX_F0 YMRO_XEN YMRO_JOD YMRO_PRO YMRO_SAM
    #YMZPOLD1 YMZPOLD2 YMZPOLD3 YMZPOLD4 YMZPOLD5 YMZPOLD6
    #YMRP_C01 YMRP_C02 YMRP_C03 YMRP_C04 YMRP_C05 YMRP_C06 YMRP_C07 YMRP_C08 YMRP_C09 YMRP_C10 YMRP_C11 YMRP_C12""".split()

    #Вектор параметров используемых для построения bgk
    # bgkvectrs = """YMFISF YMRP_POW YMRO_XEN YMRO_JOD SVRK_DNBR16""".split()
    bgkvectrs = """YMFISF YMRP_POW YMRO_XEN YMRO_JOD""".split()
    #Вектор переведенных параметров используемых для построения bgk
    bgkvectrs_translated = """w Rp Xe Id""".split()

    #Вектор параметров для динамических коэффициентов
    bgkvectrs_hdf = """h offset wt I""".split()

    def __init__(self):
        self.v = getv()
        self.data = {}
        self.bgk = {}
        for i in self.bgkvectrs + self.bgkvectrs_hdf:
            self.data[i] = []

        # ---------------------
        # Хочц посмотреть как меняется плотность теплоносителя
        self.data["YHROMIX_AV"] = []
        # ---------------------

        # self.kk = 0 # ???

    def append_old(self):
        grp = mk_grp(self.v, self.bgkvectrs)
        grp.get()
        for i in self.bgkvectrs:
            self.data[i].append(grp.v[i])
            # dat = getattr(self.v, i)
            # self.data[i].append(list(dat[...]))

    def append(self, wnom, plant='novo2', Hnums=[-3,-2,-1], Icam0=None):
        self.append_old()
        self.data['h'].append(GetHgrp3(Hnums, v=self.v))
        self.data['offset'].append(float(self.v.YMOFFSET))
        # self.kk += 1
        # if self.kk % 20 == 0:
            # print("RP1 = ", 1 - np.sum(self.v.YMRP_POW[...]) / wnom)
            # print("RP2 = ", 1 - (float(self.v["YMRP_TOTAL_MWT"]) * 1e6) / wnom)
            # print("WT1 = ", (np.sum(self.v.YMFISF[...]) / wnom) * 100.)
            # print("WT2 = ", float(self.v["ymintpow"]))
            # print("RES1 = ", (1 - np.sum(self.v.YMRP_POW[...]) / wnom) * (np.sum(self.v.YMFISF[...]) / wnom) * 100)
            # print("RES1 = ", (1 - float(self.v["YMRP_TOTAL"]) / wnom) * float(self.v["ymintpow_n"]))
        # self.data['wt'].append((1 - float(self.v["YMRP_TOTAL"]) / wnom) * float(self.v["YMINTPOW_N"]))
        self.data['wt'].append((1 - np.sum(self.v.YMRP_POW[...]) / wnom) * (np.sum(self.v.YMFISF[...]) / wnom) * 100)
        if Icam0 is not None:
            self.data["I"].append(IcamRD(self.v, plant) / Icam0)
        else:
            self.data["I"].append(IcamRD(self.v, plant))
        
        # ---------------------
        # Хочу посмотреть как меняется плотность теплоносителя
        self.data["YHROMIX_AV"].append(float(self.v["YHROMIX_AV"]))
        # ---------------------

        # self.data["dnbr"].append(float(self.v.SVRK_DNBR16_MIN_SINGLE))
        # self.data["ntvs"].append(float(self.v.SVRK_DNBR16_MIN_SINGLE_NTVS))
        # self.data["nsloy"].append(float(self.v.SVRK_DNBR16_MIN_SINGLE_NSLOY))


    def ClearData(self):
        for i in self.bgkvectrs:
            self.data[i] = []

    def flush(self):
        for i in self.bgkvectrs:
            self.data[i] = []
        self.bgk = {}

    def MakeBgk(self, nv=30):
        """компрессуем данные в БГК"""
        for i in self.bgkvectrs:
            self.bgk[i] = bgk.Bgk(self.data[i], nv)
            self.data[i] = []
        return self.bgk

    def Errors(self, j=0):
        """оценка относительной погрешности расчета полей"""
        return dict([(i, bgk.NprNorm(self.data[i][j], bgk.Residual(self.bgk[i], self.data[i][j], np.dot))) for i in self.bgkvectrs])


def SetCbRegulator(state, dynamic=1):
    """установка состояния борного регулятора
    state - включить -1
    dynamic - отслеживать уставку по мощности v.YMINTPOW_SET в динамике
    """
    v = getv()
    if state:
        v.YZBORMODE = 2
        #v.YMINTPOW_SET_FLAG = dynamic
    else:
        v.YZBORMODE = 0
        #v.YMINTPOW_SET_FLAG = dynamic

def step(n=1):
    v = getv()
    v.step(n)

class TProtocol:
    def __init__(self,model,var):
        self.model=model
        self.var=[(i,[]) for i in var]

    def __call__(self):
        for k,v in self.var.iteritems():
            v.append(self.model[k][...])


class Tinc:
    """Класс для линейного изменения параметра модели"""
    def __init__(self,v,var_nm,endval,nstep, ovr = False):
        """
        Инициализация для линейного изменения парметра
        :param v: связь с моделью
        :param var_nm: имя параметра модели
        :param endval: конечное значение параметра
        :param nstep: количество шагов модели
        :param ovr: запрет на изменения параметра другими системами
        """
        self.v=v
        self.name=var_nm
        self.cnt=0 # количество шагов, пройденных моделью
        self.endcnt=nstep
        self.delta=( endval- float(self.v[var_nm]) )/self.endcnt # величина изменения параметра за шаг
        self.v.funs[var_nm]=self
        self.ovr=ovr  # параметр возможности дальнейшего изменения переменной
    def __call__(self, n):
        """
        Функция изменения параметра, которая будет вызываться на каждом шаге модели
        :param n: количесвто шагов, которое должна выполнить модель
        :return:
        """
        # устанавливаем новое значение параметра модели
        if self.ovr:
            self.v.ovron(self.name, float(self.v[self.name]) + self.delta * n)
            # увеличиваем количество шагов модели
            self.cnt += n
            # когда заданное количесвто шагов пройдено,
            # то надо разрегистрировать переменную
            if self.endcnt<=self.cnt:
                self.v.ovroff(self.name)
        else:
            self.v[self.name] = float(self.v[self.name]) + self.delta*n
            # увеличиваем количество шагов модели
            self.cnt += n

    def __del__(self):
        """Удаление из коннектора к модели функции изменения данной переменной"""
        del self.v.funs[self.name]






if __name__ == '__main__':
    v = getv()
    v.YMFISF[0] = 1
    print(type(v.YMFISF[0]))