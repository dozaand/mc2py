#!/usr/bin/env python
# -*- coding: cp1251 -*-

"""
 ��������� ���� ��� ��������� ���������
  �������� ��������� � ����
  Y _i_j=A _i_j_k * B ^k+ C _i_j
  ������� ����� ������ �������� ������ ��� ��� �����
  ��������������� �����
  G
  delta

"""
from pyparsing import *
import pyparsing as pp

ident = Word(alphas, alphanums)
inum = Word(nums).setParseAction(lambda tok: int(tok[0]))

fracp = Literal('.')+Optional(inum)
exppart = Word('EeDd', exact=1)+Optional(Word('+-', exact=1))+inum
fnum = Combine(Optional(Literal('-'))+((
    inum+Optional(fracp)+exppart) | (inum+fracp)))
fnum.setParseAction(lambda x: float(x[0]))


tensordecl = ident+":"+delimitedList(ident, ",")
indexid = inum | ident
Mindex = Word("^_", exact=1)+indexid
tensor = ident+OneOrMore(Mindex)
LP = Literal("(")
RP = Literal(")")
expr = Forward()
simpexpr = (fnum | inum | tensor | (LP+expr+RP) | ident)
exprmul = delimitedList(simpexpr, '*')
expr << exprmul+ZeroOrMore(Word("+-", exact=1)+exprmul)
assexpr = tensor+'='+expr
code = OneOrMore(assexpr)

data = """
a_i_j=b_i_j + (c^k * d_i_k_j+35.5*c^k_i * d_k_j)
x_i=a^j_i*y_j+2
"""


class Tindex(object):
    pass


class TensG(object):
    u"""���������� ������"""
    def __init__(self, *arg):
        self._dims = arg

    def u(self, ind):
        pass

    def l(self, ind):
        pass

t1 = TensG(2, 3, 5, 5)
t1.u(i).l(j)
