#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
шуфлер для ключа который нужен для моделей гива
"""
import argparse
import random
import re
import numpy as np
from mako.template import Template

#file=["GIWLicense_ИБРАЭ_01_1801.xml","keycode.h"]


def make_key(file):
    with open(file[0], "r", encoding="cp1251") as f:
        d = f.read()
    key = re.search('CUSTOMER_KEY="(.+)"', d)
    if key:
        data = key.group(1)
    else:
        print("fail CUSTOMER_KEY= not found")
        return
    n = len(data)
    perm = list(range(n))
    random.shuffle(perm)
    letters = np.array(list(data))
    newletters = letters[perm]
    word = "".join(newletters)
    nlett = len(word)
    perm_string = ",".join([str(i) for i in perm])
    tpl = Template("""
    {
      char bufxxl[${nlett}+1]={0};
      int xl_indexes[${nlett}]={${perm_string}};
      char bufdata[]="${word}";
      for(int i=0;i<${nlett};++i){bufxxl[xl_indexes[i]]=bufdata[i];}
      strcpy((char*)vpt + 24, bufxxl);
    }
    """)
    res = tpl.render(**locals())
    with open(file[1], "w", encoding="cp1251") as f:
        f.write(res)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="create series of data")
    parser.add_argument("files", nargs="*", help=u"create code for data shuffle")
    args = parser.parse_args()

    make_key(args.files)

