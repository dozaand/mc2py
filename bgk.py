#!/usr/bin/env python
# -*- coding: cp1251 -*-

import numpy as np
import math


def DefaultDot(a, b):
    u"""dot �� ���������"""
    return a.dot(b)


def _gennormallk(k, v, dot, linkomb):
    u"""������������� �������� ���������� ��� �������� ��������� ������������ � �������� ����������
k  ������������
v  �������
dot ��������� ������������
lk �������� ����������
"""
    res = linkomb(k, v)
    nor = 1./math.sqrt(dot(res, res))
    return nor*res


def lk(koef, vset):
    u"""������ �������� ���������� ��������, ���������� �� a+=b, k*v,a=b"""
    res = koef[0]*vset[0]
    for k, v in list(zip(koef, vset))[1:]:
        res += k*v
    return res


def NumpyLk(koef, vset):
    u"""������ �������� ���������� ��������, ���������� �� numpy.dot"""
    return np.dot(koef, vset)


def Bgk(vset, nredu, fdot=DefaultDot, linkomb=None, W=None, eltype='d'):
    u"""������ ������ ������� ���������
vset  - �������� ����� ��������
nredu - ���������� �������� � ���
fdot  - ������� ��� ������� ���������� ������������
���� fdot �� ������ �� ������ ��� ������������ x.dot(y)
W - ������� ������� ��� ��������
eltype - ���������� ����� ������������ � ������� ��� ��������� ���������
"""
    n = len(vset)
    if nredu > n:
        raise Exception(u"n<n")
    if linkomb == None:
        linkomb = lk
    A = np.zeros((n, n), eltype)
    if W == None:
        for i in range(n):
            for j in range(i, n):
                fd = fdot(vset[i], vset[j])
                A[i, j] = fd
                A[j, i] = fd
    else:
        for i in range(n):
            for j in range(i, n):
                fd = W[i]*W[j]*fdot(vset[i], vset[j])
                A[i, j] = fd
                A[j, i] = fd
    lam, v = np.linalg.eigh(A)
    lam_v = zip(abs(lam), np.transpose(v))
##    lam_v.sort(reverse=1)
    lam_v = sorted(lam_v, reverse=1, key = lambda x: x[0])
    sr = lam_v[:nredu]
    return [_gennormallk(ev, vset, fdot, linkomb) for k, ev in sr]


def Koeffs(bgk, v, fdot=DefaultDot):
    u"""������ ������������� ���������� �� ������"""
    return [fdot(b, v) for b in bgk]


def K2vector(bgk, k, linkomb=NumpyLk):
    u"""�� ������������� �������� ������"""
    return linkomb(k, bgk)


def Proection(bgk, v, fdot=DefaultDot, linkomb=NumpyLk):
    u"""�������� ������� �� �����"""
    k = Koeffs(bgk, v, fdot)
    return K2vector(bgk, k, linkomb)


def Residual(bgk, v, fdot=DefaultDot, linkomb=NumpyLk):
    u"""������� ����� ����� �������� � ��� ��������� � �����"""
    return v - Proection(bgk, v, fdot, linkomb)


class Tbgk:
    u""" ����� ������� ��������� """
    def __init__(self, vset, nredu, fdot=DefaultDot, linkomb=None, W=None, eltype='d'):
        self.fdot = fdot
        self.linkomb = linkomb
        self.eltype = eltype
        if linkomb == None:
            self.linkomb = lk
        self.bas = Bgk(vset, nredu, fdot, linkomb, W, eltype)

    def v2k(self, v):
        u"""������ ������������� ���������� �� ������"""
        return [self.fdot(b, v) for b in self.bas]

    def k2v(self, k):
        u"""�� ������������� �������� ������"""
        return self.linkomb(k, self.bas)

    def proection(v):
        u"""�������� ������� �� �����, �.�. ������"""
        k = self.v2k(v)
        return self.k2v(k)


class TbgkFilter(object):
    u"""������ �� ������ ��� - ������ ������ - ������� ��������� ����������"""
    def __init__(self, signal, nsignal, noise=None, nnoise=None, Wsignal=None, Wnoise=None, fdot=DefaultDot, linkomb=None, eltype='d'):
        u"""signal - ����� ��������� ��������
        nsignal   - ������ ������ ��� ������������� �������
        noise     - ����� ���������  ������ � �����
        nnoise    - ������ ������ ��� ������������� �����
        Wsignal   - ������ ����� ��� ��������� ������������������ ��������
        Wnoise    - ������ ����� ��� ��������� ������������������ �����
        fdot      - �������� ��� ������� ��������� ������������
        linkomb   - �������� ��� ���������� �������� ����������
        eltype    - ��� ������ ����������� ��� �������
        """
        self.fdot = fdot
        signalBgk = Tbgk(signal, nsignal, fdot, linkomb, Wsignal, eltype)
        ss = np.matrix(signalBgk.bas)
        if(not noise == None):
            noiseBgk = Tbgk(noise, nnoise, fdot, linkomb, Wsignal, eltype)
            sn = np.matrix(noiseBgk.bas)
            snmatr = np.matrix(ss.T*sn, dtype=eltype)
            self.noiseBgk = noiseBgk
        else:
            snmatr = np.matrix(ss.T, dtype=eltype)
            self.noiseBgk = None
        snsn = snmatr*snmatr.T
        self.Ri = (np.eye(len(snsn))-snsn).I
        self.signalBgk = signalBgk

    def __call__(self, x):
        """���������� ���������� ����������� ��� �������� ������� x"""
        if not self.noiseBgk == None:
            deerr = x-self.noiseBgk.proection(x)
        else:
            deerr = x
        koeff = self.signalBgk.v2k(deerr)
        return self.signalBgk.k2v(koeff)

    def v2k(self, x):
        """���������� ���������� ����������� ������������� ���������� ��� �������� ������� x"""
        if not self.noiseBgk == None:
            deerr = x-self.noiseBgk.proection(x)
        else:
            deerr = x
        return self.signalBgk.v2k(deerr)


def NprNorm(v, dv):
    u"""������������� ������ ������������� �������
    v  ��������������� � ����� ������
    dv ������� ����� �������� �������� � ��� ��������� � �����
       """
    return math.sqrt(np.dot(dv, dv)/np.dot(v, v))

# nv=10
# ns=100
# sig=np.random.random((ns,nv))

# noi=np.random.random((ns,nv))
# bgkf=TbgkFilter(sig,5,noi,5)
