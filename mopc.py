#!/usr/bin/env python
# -*- coding: cp1251 -*-

import opc.OpenOPC as OpenOPC
import numpy as np
import time
#opc.connect('ICONICS.SimulatorOPCDA.2')
#opc['YMINTPOW']=2
#v=opc.read('YMINTPOW',sync=1)

class Tmfa(object):
    opc = OpenOPC.client()
    opc.connect('GiwServerOPC')
    varlist=[]
    def __init__(self,nm):
        self.name=nm
        self._io=None
        v=self.d
#        if not nm in Tmfa.varlist:
#            Tmfa.opc.remove("py")
#            Tmfa.opc.read(Tmfa.varlist,group="py",sync=1)

    @property
    def io(self):
        self._io=Tmfa.opc.read(self.name,sync=1)
        return self._io
    @property
    def d(self):
        """��������� ������"""
        self._io=Tmfa.opc.read(self.name,sync=1)
        v=self._io[0]
        if hasattr(v,"__len__"):
            return np.array(self._io[0])
        else:
            return self._io[0]
    @d.setter
    def d(self,x):
        """��������� ������"""
        Tmfa.opc[self.name]=x
    def restart(self):
        Tmfa.opc.close()
        opc.connect('GiwServerOPC')

#def RegVars(li1,dkt):
#    li=li1.split()
#    Tmfa.opc.read(li[1::2],group="py",sync=1)
#    for pynm,nm in zip(li[0::2],li[1::2]):
#        dkt[pynm]=Tmfa(nm)

_gcmd=Tmfa("OPCCMD")
_cStart=12
_cStop=13
_cStartStep=14

def MFAGet(nm):
    res=Tmfa.opc.read(nm,sync=1)
    if not res[0]:
        res=Tmfa.opc.read(nm,sync=1)
    return res[0]

def MFASet(nm,data):
    Tmfa.opc[nm]=data

def GStart():
    """������ ������ mfa"""
    _gcmd.d=_cStart
def GStop():
    """������� ������ mfa"""
    _gcmd.d=_cStop
    while _gcmd.d:
        pass

def GStep():
    """���� ��� mfa"""
    _gcmd.d=_cStartStep



def RegVars(li1,g=globals()):
    li=li1.split()
    for pynm,nm in zip(li[0::2],li[1::2]):
        exec("%(pynm)s=Tmfa('%(nm)s')" %locals(),g)

def RegVars1(li1,g=globals()):
    li=li1.split()
    for nm in li:
        exec("%(nm)s=Tmfa('%(nm)s')" %locals(),g)


def OpcClose():
    Tmfa.opc.close()
#li1="w YMINTPOW hsuz YSHGRP key YZREGRPINS_KEY"

#RegVars(li1)


class Tvl:
    def __init__(self):
        """����������������� ���������� �� ops"""
        pass
    def __getattr__(self,nm):
        for i in xrange(10):
            res=Tmfa.opc.read(nm,sync=1)
            if res[1]=='Good':
                if isinstance(res[0],basestring):
                    return res[0]
                elif hasattr(res[0],"__len__"):
                    return np.array(res[0])
                else:
                    return res[0]

            time.sleep(0.2)

        raise Exception("var not found %s" % nm)
    def __setattr__(self,nm,val):
        Tmfa.opc[nm]=val
    def __getitem__(self,vv):
        return self.__getattr__(vv)
    def __setitem__(self,nm,val):
        self.__setattr__(nm,val)

v=Tvl()

def MS():
    RET()

# def RET():
#    """���� ��� mfa"""
#    old=v.YMSTEPDONE
#    _gcmd.d=_cStartStep
#    for i in xrange(100):
#        if v.YMSTEPDONE!=old:
#            return
#        time.sleep(0.05)
#    raise Exception("fail do step")

#def RET():
#    """���� ��� mfa"""
#    _gcmd.d=_cStartStep
#    for i in xrange(100):
#        if _gcmd.d==0:
#            return
#        time.sleep(0.05)
#    raise Exception("fail do step")

#def RET():
#    """���� ��� mfa"""
#    v.OPCCMD=14
#    for i in xrange(100):
#        if v.OPCCMD==0:
#            return
#        time.sleep(0.05)
#    raise Exception("fail do step")

def RET(nstep=1):
     """���� ��� mfa"""
     for j in xrange(nstep):
         v.OPCCMD=21#|0x04000000 ����� �������� ���������� ������� ����, ����� giw �� ����� ��������� � ���� ������
                     #�� ����� ��������� ����, ����� ��������� ���������� ������ � �������
         v._OPCCMD=nstep
         time.sleep(0.01)
         for i in xrange(50):
             if v.OPCCMD==0:
                 return
             time.sleep(0.1)
         raise Exception("fail do step")

# def RET(nstep=1):
#    istep_old=v.YMSTEPDONE
#    while v["$_MODELISSTOPPED"]:
#        v.OPCCMD=12 #start model
#         time.sleep(0.01)
#    while v.YMSTEPDONE<nstep+istep_old:
#             time.sleep(0.05)
#    while v["$_MODELISSTOPPED"]==False:
#        v.OPCCMD=13 #stop model
#        time.sleep(0.01)

def RETU():
    """���� ��� mfa"""
    _gcmd.d=_cStartStep
