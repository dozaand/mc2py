#!/usr/bin/env python
# -*- coding: cp1251 -*-

"""
make python objects
pymakeobj

advantages:
pymakeobj vs gnu make or pymake
pymakeobj not limited to file targets
pymakeobj execute python commands but not shell commands -> more sutable for python script
pymakeobj time stamp not limited to time -> intensive computatins can be done correctly

disadvantages:
it is nesasary to describe build target as python class

any class must implement
self.dependences - description of predessors of target
Build method for updating object from self.dependences

warnings:
    @makable implements methods
    Make - check evaluation times for dependences and rebuild object if it is older then some of dependences
    ClearDeps - remove temporary objects

class declaration may be done in 3 ways:

1 for secviental execution
@makable
class Texample_target:
    def __init__(self):
        self.dependences=[a,b,c]
    def Build(self):
        .....

2 or for multithreadind execution
@makableMT
class Texample_target:

3 or for multiprocess execution
@makableMT
class Texample_target:

or for distributed evaluation (not implemented)
@makableMD
class Texample_target:

"""

import subprocess as sp
import threading
import os
import sys
import re
# import multiprocessing as mp
# import Process,Lock,Pool


# from datetime import datetime
import time

_versionLock = threading.Lock()

_maxThrd = 4
_sem = threading.Semaphore(value=_maxThrd)
_VersionCnt = 0

__all__ = ["makable", "TTargetFile", "TSrcFile"]


def VersionNowT():
    """������ �������� - UNIX time"""
    return time.time()


def InvalidVersionT():
    """������ �������� - UNIX time"""
    return 0.


def VersionNowI():
    """������ �������� - ������"""
    try:
        _versionLock.acquire()
        _VersionCnt += 1
    finally:
        _versionLock.release()
    return _VersionCnt


def InvalidVersionI():
    """������ �������� - ������"""
    return 0

VersionNow = VersionNowT
InvalidVersion = InvalidVersionT


def SetTmark():
    """������������ � �������� ������� ��������� �����"""
    global VersionNow, InvalidVersion
    VersionNow = VersionNowT
    InvalidVersion = InvalidVersionT


def SetImark():
    """������������ � �������� ������� ���������������� �������"""
    global VersionNow, InvalidVersion
    VersionNow = VersionNowI
    InvalidVersion = InvalidVersionI


def _GetVer(obj):
    """���������� ������ �� �������� ������� ��� �� ������� �� ���� ������"""
    if hasattr(obj, "Version"):
        return obj.Version()
    if hasattr(obj, "_p_mtime"):
        return obj._p_mtime


class TNode:
    """������ ������� ����� ���� ������"""
    def __init__(self):
        self.version = InvalidVersion()

    def Make(self):
        """��������� ��� ����������� ���������� 1 ���� ��� ��������� ����� 0"""
        for item in self.deps:
            if hasattr(item, "Make"):
                item.Make()
        sv = self.Version()
        if not sv:
            outofdate = [item for item in self.deps if item]
            self.Build(outofdate)
            self.version = VersionNow()
            return
        outofdate = [item for item in self.deps if _GetVer(item) > sv]
        if outofdate:
            self.Build(outofdate)
            self.version = VersionNow()

    def Clear(self):
        for item in self.deps:
            if hasattr(item, "Clear"):
                item.Clear()

    def Version(self):
        """������ �� ��������� - ����� ����������� ���������� - �� ����������� �� ������ � ������"""
        return self.version


def LockAndRun(f):
    try:
        _sem.acquire()
        f()
    except:
        _sem.release()
        raise


class TNodeMT(TNode):
    """������ ������� ����� ���� ������"""
    def __init__(self):
        TNode.__init__(self)

    def Make(self):
        thrdlist = []
        for i, item in enumerate(self.deps):
            if hasattr(item, "Make"):
                thrd = threading.Thread(target=LockAndRun, args=(item.Make,))
                thrdlist.append(thrd)
                thrd.start()
        for thrd in thrdlist:
            thrd.join()

        sv = self.Version()
        outofdate = [item for item in self.deps if _GetVer(item) > sv]
        if outofdate:
            self.Build(outofdate)
            self.version = VersionNow()


def makable(cls):
    def __init__(self, *arg, **kvarg):
        cls.__init__(self, *arg, **kvarg)
        TNode.__init__(self)
    newtype = type(cls.__name__, (TNode, cls), {"__init__": __init__})
    return newtype


def makableMT(cls):
    def __init__(self, *arg, **kvarg):
        cls.__init__(self, *arg, **kvarg)
        TNodeMT.__init__(self)
    newtype = type(cls.__name__, (TNodeMT, cls), {"__init__": __init__})
    return newtype


# def makable(cls):
##    class Tcls(cls,TNode):
##        def __init__(self,*argv,**argh):
##            cls.__init__(self,*argv,**argh)
##            TNode.__init__(self)
##    return Tcls
##
# def makableMT(cls):
##    class Tcls(cls,TNodeMT):
##        def __init__(self,*argv,**argh):
##            cls.__init__(self,*argv,**argh)
##            TNodeMT.__init__(self)
##    return Tcls

#######################################################################
              #########    ���������� �����    #########

@makable
class TAll(TNode):
    """������������ ���� �����"""
    def __init__(self, *arg):
        TNode.__init__(self)
        self.deps = arg

    def Build(self, outofdate):
        print "do all"


@makableMT
class TAllMT(TNodeMT):
    """������������ ���� �����"""
    def __init__(self, *arg):
        TNode.__init__(self)
        self.deps = arg

    def Build(self, outofdate):
        print "do all"


class TSrcFile:
    """�������� ���� - ����"""
    def __init__(self, nm):
        self.filename = nm
        self.deps = []

    def Version(self):
        if os.path.exists(self.filename):
            return os.stat(self.filename).st_mtime
        else:
            return InvalidVersionT()


class TTargetFile(TSrcFile):
    """������������� ���� - ����"""
    def __init__(self, nm):
        TSrcFile.__init__(self, nm)

    def Clear(self):
        os.remove(self._filename)


class TTargetFiles:
    """������������� ���� - ����"""
    def __init__(self, tag, src):
        self._tag = tag
        self.filename = tag
        self._src = src

    def Clear(self):
        for i in self._tag:
            try:
                os.remove(i)
            except:
                print "failed to remove file %s" % i

    def Version(self):
        if os.path.exists(self._tag[0]):
            return os.stat(self._tag[0]).st_mtime
        else:
            return InvalidVersionT()


#######################################################################
              #########    LaTeX    #########
def BuildLaTeX(fil):
    """������ ���������� �����"""
    p, f = os.path.split(fil)
    fb = os.path.splitext(f)[0]
    cwd = os.getcwd()
    if p:
        os.chdir(p)
    os.system("latex %s.tex" % fb)
    os.system("bibtex %s.aux" % fb)
    os.system("latex %s.tex" % fb)
    os.system("pdflatex.exe %s.tex" % fb)
    if p:
        os.chdir(cwd)


@makable
class TlatexRes(TTargetFiles):
    def __init__(self, src):
        subdeps = _TexAutoDep(src)
        allsrc = [src]+subdeps
        self.deps = [TSrcFile(i) for i in allsrc]
        p, f = os.path.split(self.deps[0].filename)
        fb = os.path.splitext(f)[0]
        ff = os.path.join(p, fb)
        dest = [
            ff+"."+i for i in "dvi pdf bbl blg log aux lof lot out toc".split()]
        TTargetFiles.__init__(self, dest, src)

    def Build(self, outofdate):
        p, f = os.path.split(self.deps[0].filename)
        fb = os.path.splitext(f)[0]
        cwd = os.getcwd()
        if p:
            os.chdir(p)
##        os.system("latex %s.tex" %fb)
##        os.system("bibtex %s.aux"%fb)
##        os.system("latex %s.tex"%fb)
##        os.system("pdflatex.exe %s.tex"%fb)

        sp.check_call("latex %s.tex" % fb, shell=1)
        sp.check_call("bibtex %s.aux" % fb, shell=1)
        sp.check_call("latex %s.tex" % fb, shell=1)
        sp.check_call("pdflatex.exe %s.tex" % fb, shell=1)
        sp.check_call("l2r.bat %s.tex" % fb, shell=1)
        if p:
            os.chdir(cwd)


class _TTexAutoDep:
    def __init__(self):
        """���� ����������������"""
        self.db = {}

    def AutoDep(self, fname):
        filename = os.path.abspath(fname)
        if filename in self.db:
            return
        filedir, fil = os.path.split(filename)
        wd = os.getcwd()
        if filedir:
            os.chdir(filedir)
        with open(filename, "rt") as f:
            txt = f.read()
        incfiles = [os.path.abspath(fnd.group(
            1))+".tex" for fnd in re.finditer(r"^ *\input{(.+)}", txt)]
        inpfiles = [os.path.abspath(fnd.group(
            1))+".tex" for fnd in re.finditer(r"^ *\include{(.+)}", txt)]
        picts = [os.path.abspath(fnd.group(1)) for fnd in re.finditer(
            r"\includegraphics.+{(.+)}", txt)]
        tottex = incfiles+inpfiles
        ifiles = tottex
        pfiles = picts
        for i in tottex:
            subi, subp = self.AutoDep(i)
            ifiles = ifiles+subi
            pfiles = pfiles+subp
        if filedir:
            os.chdir(wd)
        ifiles = list(set(ifiles))
        pfiles = list(set(pfiles))
        self.db[filename] = (ifiles, pfiles)
        return (ifiles, pfiles)


def _TexAutoDep(fname):
    filename = os.path.abspath(fname)
    obj = _TTexAutoDep()
    obj.AutoDep(filename)
    obj.db[filename]
    allpict = [i+ext for i in obj.db[filename][1] for ext in [".eps", ".pdf"]]
    return obj.db[filename][0]+allpict

#######################################################################
              #########    BorlandC    #########


@makable
class TobjBcc(TTargetFile):
    """��� ���� - ��������� ����"""
    def __init__(self, nm, options="debug"):
        (self.basename, self.ext) = os.path.splitext(nm)
        TTargetFile.__init__(self, self.basename+".obj")
        self.deps = [TSrcFile(nm)]
        if options == 'debug':
            self.options = "-Od -v "
        elif options == 'release':
            self.options = "-Od -v "
        else:
            self.options = options

    def Build(self, outofdate):
        cmd = "bcc32 -c {opt} -o {obj} {src}".format(
            src=self.basename+self.ext, obj=self.basename+".obj", opt=self.options)
        res = sp.check_call(cmd, shell=1)

#######################################################################
              #########    MSVC    #########


@makable
class TObjMSVC(TTargetFile):
    """��������� ��� MSVC"""
    def __init__(self, nm, options="debug"):
        (self.basename, self.ext) = os.path.splitext(nm)
        TTargetFile.__init__(self, self.basename+".obj")
        self.deps = [TSrcFile(nm)]
        if options == 'debug':
            self.options = "-Od "
        elif options == 'release':
            self.options = "-Ox "
        else:
            self.options = options

    def Build(self, outofdate):
        cmd = "cl -c {opt} -out:{obj} {src}".format(
            src=self.basename+self.ext, obj=self.basename+".obj", opt=self.options)
        sp.check_call(cmd, shell=1)


@makableMT
class TlibMSVC(TTargetFile):
    """�������� ���������� ��� MSVC"""
    def __init__(self, out, srcfiles, options="debug"):
        TTargetFile.__init__(self, out)
        self.src = srcfiles
        self.target = out
        self.deps = [TObjMSVC(nm, options=options) for nm in self.src]

    def Build(self, outofdate):
        flist = [i.filename for i in outofdate]
        if os.path.exists(self.target):
            obj = " ".join(flist)
            sp.check_call("lib /out:{libname} {libname} {obj}".format(
                libname=self.target, obj=obj))
        else:
            obj = " ".join(flist)
            sp.check_call("lib /out:{libname} {obj}".format(
                libname=self.target, obj=obj))


#######################################################################
              #########    Keil    #########
keilroot = r"C:\apps\Keil"


@makable
class TObjKeil(TTargetFile):
    """��������� ��� Keil"""
    clname = os.path.join(keilroot, r"ARM\BIN40\armcc.exe")
    keilincl1 = os.path.join(keilroot, r"ARM\INC")
    keilincl2 = os.path.join(keilroot, r"ARM\INC\ARM")

    def __init__(self, nm, cpu="ARM7TDMI"):
        (self.basename, self.ext) = os.path.splitext(nm)
        TTargetFile.__init__(self, self.basename+".o")
        self.deps = [TSrcFile(nm)]
        self.cpu = cpu
        self.include = [self.keilincl1, self.keilincl2]
        self._MakeOpt()

    def _MakeOpt(self):
        """�������� ����� �����������"""
        ilist = " ".join(["-I {i}".format(i=x) for x in self.include])
        self.options = """--cpp -c -O0 {ilist} --cpu {cpu} """.format(
            ilist=ilist, cpu=self.cpu)

    def Build(self, outofdate):
        cmd = "{cl} {opt} -o {obj} {src}".format(
            cl=self.clname, src=self.basename+self.ext, obj=self.basename+".o", opt=self.options)
        sp.check_call(cmd, shell=1)


@makable
class TImgKeil(TTargetFile):
    """����� ��� Keil"""
    linkname = os.path.join(keilroot, r"ARM\BIN40\armlink.exe")
    libpath = os.path.join(keilroot, r"ARM\RV31\LIB\armlib")

    def __init__(self, nm, deps, cpu="ARM7TDMI"):
        (self.basename, self.ext) = os.path.splitext(nm)
        TTargetFile.__init__(self, self.basename+".axf")
        self.deps = [TObjKeil(nm, cpu=cpu) for nm in deps]
        self.cpu = cpu
        self.options = r"--libpath {lib}\ --cpu {cpu}".format(
            lib=self.libpath, cpu=self.cpu)

    def Build(self, outofdate):
        objlist = " ".join([dep.filename for dep in self.deps])
        cmd = "{link} {opt} -o {afx} {src}".format(
            link=self.linkname, src=objlist, afx=self.basename+".afx", opt=self.options)
        sp.check_call(cmd, shell=1)

#######################################################################
              #########    itel fortran & c++    #########
keilroot = r"C:\Program Files\Intel\ComposerXE-2011"


@makable
class TObjIntel(TTargetFile):
    """��������� ��� Keil"""
    compiler = os.path.join(keilroot, r"\bin\ia32\ifort.exe")

    def __init__(self, nm):
        (self.basename, self.ext) = os.path.splitext(nm)
        TTargetFile.__init__(self, self.basename+".obj")
        self.deps = [TSrcFile(nm)]
        self._MakeOpt()

    def _MakeOpt(self):
        """�������� ����� �����������"""
        self.options = """-c """

    def Build(self, outofdate):
        cmd = "{compiler} {opt} -o {obj} {src}".format(
            compiler=self.compiler, src=self.basename+self.ext, obj=self.basename+".o", opt=self.options)
        sp.check_call(cmd, shell=1)
