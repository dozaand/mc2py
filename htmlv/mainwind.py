#!/usr/bin/env python
# -*- coding: cp1251 -*-
import wx
import os
import sys

import wx.html as html
import wx.lib.wxpTag


class MyHtmlWindow(html.HtmlWindow):
    def __init__(self, parent, id):
        html.HtmlWindow.__init__(
            self, parent, id, style=wx.NO_FULL_REPAINT_ON_RESIZE)
        if "gtk2" in wx.PlatformInfo:
            self.SetStandardFonts()

    def OnLinkClicked(self, linkinfo):
        super(MyHtmlWindow, self).OnLinkClicked(linkinfo)

    def OnSetTitle(self, title):
        super(MyHtmlWindow, self).OnSetTitle(title)

    def OnCellMouseHover(self, cell, x, y):
        super(MyHtmlWindow, self).OnCellMouseHover(cell, x, y)

    def OnCellClicked(self, cell, x, y, evt):
        if isinstance(cell, html.HtmlWordCell):
            sel = html.HtmlSelection()
        super(MyHtmlWindow, self).OnCellClicked(cell, x, y, evt)

# This filter doesn't really do anything but show how to use filters


class MyHtmlFilter(html.HtmlFilter):
    def __init__(self, log):
        html.HtmlFilter.__init__(self)
        self.log = log

    # This method decides if this filter is able to read the file
    def CanRead(self, fsfile):
        self.log.write("CanRead: %s\n" % fsfile.GetMimeType())
        return False

    # If CanRead returns True then this method is called to actually
    # read the file and return the contents.
    def ReadFile(self, fsfile):
        return ""


class TestHtmlPanel(wx.Panel):
    def __init__(self, parent, frame):
#    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.NO_FULL_REPAINT_ON_RESIZE)
        self.frame = frame
        self.cwd = os.path.split(sys.argv[0])[0]

        if not self.cwd:
            self.cwd = os.getcwd()
        if frame:
            self.titleBase = frame.GetTitle()

#        html.HtmlWindow_AddFilter(MyHtmlFilter())

        self.html = MyHtmlWindow(self, -1)
        self.html.SetRelatedFrame(frame, self.titleBase + " -- %s")
#        self.html.SetRelatedStatusBar(0)

        self.printer = html.HtmlEasyPrinting()

        self.box = wx.BoxSizer(wx.VERTICAL)
        self.box.Add(self.html, 1, wx.GROW)

        subbox = wx.BoxSizer(wx.HORIZONTAL)

        btn = wx.Button(self, -1, "Load File")
        self.Bind(wx.EVT_BUTTON, self.OnLoadFile, btn)
        subbox.Add(btn, 1, wx.GROW | wx.ALL, 2)

        btn = wx.Button(self, -1, "Load URL")
        self.Bind(wx.EVT_BUTTON, self.OnLoadURL, btn)
        subbox.Add(btn, 1, wx.GROW | wx.ALL, 2)

        btn = wx.Button(self, -1, "With Widgets")
        self.Bind(wx.EVT_BUTTON, self.OnWithWidgets, btn)
        subbox.Add(btn, 1, wx.GROW | wx.ALL, 2)

        btn = wx.Button(self, -1, "Back")
        self.Bind(wx.EVT_BUTTON, self.OnBack, btn)
        subbox.Add(btn, 1, wx.GROW | wx.ALL, 2)

        btn = wx.Button(self, -1, "Forward")
        self.Bind(wx.EVT_BUTTON, self.OnForward, btn)
        subbox.Add(btn, 1, wx.GROW | wx.ALL, 2)

        btn = wx.Button(self, -1, "Print")
        self.Bind(wx.EVT_BUTTON, self.OnPrint, btn)
        subbox.Add(btn, 1, wx.GROW | wx.ALL, 2)

        btn = wx.Button(self, -1, "View Source")
        self.Bind(wx.EVT_BUTTON, self.OnViewSource, btn)
        subbox.Add(btn, 1, wx.GROW | wx.ALL, 2)

        self.box.Add(subbox, 0, wx.GROW)
        self.SetSizer(self.box)
        self.SetAutoLayout(True)

        # A button with this ID is created on the widget test page.
        self.Bind(wx.EVT_BUTTON, self.OnOk, id=wx.ID_OK)

        self.OnShowDefault(None)
#        self.html.LoadPage("a.html")

    def ShutdownDemo(self):
        # put the frame title back
        if self.frame:
            self.frame.SetTitle(self.titleBase)

    def OnShowDefault(self, event):
        name = os.path.join(self.cwd, "a.html")
        self.html.LoadPage(name)

    def OnLoadFile(self, event):
        dlg = wx.FileDialog(self, style=wx.OPEN,
                            wildcard='HTML Files|*.htm;*.html', )

        if dlg.ShowModal():
            path = dlg.GetPath()
            self.html.LoadPage(path)

        dlg.Destroy()

    def OnLoadURL(self, event):
        dlg = wx.TextEntryDialog(self, "Enter a URL")

        if dlg.ShowModal():
            url = dlg.GetValue()
            self.html.LoadPage(url)

        dlg.Destroy()

    def OnWithWidgets(self, event):
        os.chdir(self.cwd)
        name = os.path.join(self.cwd, opj('data/widgetTest.htm'))
        self.html.LoadPage(name)

    def OnOk(self, event):
        pass

    def OnBack(self, event):
        if not self.html.HistoryBack():
            wx.MessageBox("No more items in history!")

    def OnForward(self, event):
        if not self.html.HistoryForward():
            wx.MessageBox("No more items in history!")

    def OnViewSource(self, event):
        import wx.lib.dialogs

        source = self.html.GetParser().GetSource()

        dlg = wx.lib.dialogs.ScrolledMessageDialog(self, source, 'HTML Source')
        dlg.ShowModal()
        dlg.Destroy()

    def OnPrint(self, event):
        self.printer.GetPrintData().SetPaperId(wx.PAPER_LETTER)
        self.printer.PrintFile(self.html.GetOpenedPage())


class HtmlPanel(wx.Frame):
# init database
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition, size=wx.Size(
            500, 300), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        self.m_panel1 = TestHtmlPanel(self, self)
        bSizer1.Add(self.m_panel1, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(bSizer1)
        self.Layout()

        self.Centre(wx.BOTH)

# def ed(obj):
    "�������������� ������������� �������"
if "app" not in globals():
    global app
    app = wx.PySimpleApp(redirect=False)
frame = HtmlPanel(None)
frame.Show(True)
app.MainLoop()
