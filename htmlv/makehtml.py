#!/usr/bin/env python
# -*- coding: cp1251 -*-

"""
 ������������ html ��� ������ dot
"""
import subprocess as sp
import os


def MakeDotHtml(htmlnaame, dotname):
    """��������� html"""
    filenm, ext = os.path.splitext(dotname)
    cmd = """dot.exe -O -Tcmapx -Tgif %s""" % dotname
    sp.call(cmd)
    with open(dotname+".cmapx", "rt") as f:
        mapping = f.read()
    proto = r"""<h3>ECRAN</h3>
<DIV ALIGN=CENTER>

{mapping}

<IMG SRC="{dotname}.gif" USEMAP="#G"><BR>

</DIV>
""".format(**locals())
    with open(filenm+".html", "wt") as f:
        f.write(proto)


MakeDotHtml("a.html", "a.dot")
