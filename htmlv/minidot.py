#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
������� ����������� � dot ��������
"""
from mc2py.evaldict import EvalDict
import subprocess as sp
import codecs


def RunDot(code, fil):
    # -Tcmapx -ox.map -Tgif -ox.gif
    p = sp.Popen("dot -Tcmapx -o{fil}.map -Tpng -o{fil}.png".format(
        fil=fil), shell=1, stdin=sp.PIPE)
    p.communicate(code.encode("utf8"))

code = u"""digraph G {
main [label="���"]
main -> parse -> execute;
main -> init;
main -> cleanup;
execute -> make_string;
execute -> printf
init -> make_string;
main -> printf;
execute -> compare;
}
"""

RunDot(code, "xxx")


class Tgvopt(object):
    pass


class Tnode(object):
    def __init__(self):
        self.parents = []
        self.childrens = []
        self.opt = Tgvopt()

    def SetOpt(*arg):
        for k, v in arg.iteritems():
            self.opt


class Tedge(object):
    def __init__(self, e1, e2):
        self.e1 = e1
        self.e2 = e2
        self.opt = Tgvopt()


def ListChildrens(nd, path, lev=1000):
    """������������ ���� ��������"""
    if nd in path or lev == 0:
        return
    path.append(nd)
    yield nd
    for i in nd.childrens:
        ListChildrens(i.e2, path, lev-1)


def Mkdot(node, lev=500):
    """������ ����"""

    s = """digraph G
{{
 {body}
}}
""".format(**locals)
    return s

a = Tnode()
b = Tnode()
