#! /usr/bin/env python

# Variant of "which".
# On stderr, near and total misses are reported.
# '-l<flags>' argument adds ls -l<flags> of each file found.

import sys
if sys.path[0] in (".", ""):
    del sys.path[0]

import sys
import os
from stat import *


def Name(fil, first=1):
    u"""
search file name for given executable
"""
    if first:
        return Names([fil])[0]
    else:
        return Names([fil])


def Names(fils):
    u"""
search file name for executables list
"""
    pathlist = os.environ['PATH'].split(os.pathsep)
    sts = 0
    res = []
    for prognm in fils:
        ident = ()
        drb, prog = os.path.split(prognm)
        if drb:
            if os.path.exists(prognm):
                res.append(os.path.abspath(prognm))
        else:
            for dr in pathlist:
                filename = os.path.join(dr, prog)
                try:
                    st = os.stat(filename)
                except os.error:
                    continue
                if not S_ISREG(st[ST_MODE]):
                    raise LookupError(filename + ': not a disk file')
                else:
                    mode = S_IMODE(st[ST_MODE])
                    if mode & 0b111:
                        if not ident:
    #                        print filename
                            res.append(filename)
                            ident = st[:3]
                        else:
                            if st[:3] == ident:
                                s = 'same as: '
                            else:
                                s = 'also: '
                            res.append(filename)
                    else:
                        raise LookupError(filename + ': not executable')
    return res

# Name(r"C:\apps\Notepad++\notepad++.exe")
if __name__ == '__main__':
    for i in sys.argv[1:]:
        res = Name(i)
        print(res)
