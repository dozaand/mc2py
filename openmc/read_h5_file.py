import numpy as np
import h5py



def conver_xs_for_sketch_from_hdf5(universe="101", openmc_file_name = "mgxs.h5", sketch_file_name = ["XS_10.DAT","XP.DAT"] ):
    """
    file_name - путь к файлу с данными, полученными по OPENMC
    sketch_file_name - пути к файлам для SKETCH
    """
    print (openmc_file_name)

    data = h5py.File(openmc_file_name)

    for av, dev in zip(np.array(data["universe"][universe]["absorption"]["average"]),
                       np.array(data["universe"][universe]["absorption"]["std. dev."])):
        if av !=0:
            print(dev*100/av)
        else:
            print(dev, av)

    number_Egroup = data["universe"][universe]["absorption"]["average"].size

    react = {"D": "transport", "SA": "absorption", "nuSF": "nu-fission", "SF": "fission","tot":"total"} #, "scatter": "nu-scatter matrix"}
    with open(sketch_file_name[0], "w") as f:
        f.write("1 19\n\n")
        data4file = []
        for Egroup in range(number_Egroup):
            for nmsketch, nmopenmc in react.items():
                # for av in np.array(data["universe"]["5"][nmopenmc]["average"]):
                if nmopenmc == "transport":
                    #one_del_3tr = 1/ (3* float(data["universe"][universe][nmopenmc]["average"][Egroup]) )
                    one_del_3tr = float(data["universe"][universe][nmopenmc]["average"][Egroup])
                    stra_to_print = "{:.5E} ".format(  one_del_3tr )
                else:
                    stra_to_print = "{:.5E} ".format(data["universe"][universe][nmopenmc]["average"][Egroup])

                f.write(stra_to_print)

            f.write("\n")
        f.write("\n")
        for Egroup_row in range(number_Egroup):
            for Egroup_col in range(number_Egroup):
                f.write("{:.5E} ".format( data["universe"][universe]["nu-scatter matrix"]["average"][Egroup_col, Egroup_row]) )
            if Egroup_row == number_Egroup - 1: f.write("1")
            f.write("\n")

    with open(sketch_file_name[1], "w") as f:
        for av in data["universe"][universe]["chi"]["average"]:
            f.write("{:.5E} ".format(  av ) )

    # state = h5.File("/home/anatoly/Desktop/ximena/statepoint.100.h5", "r")


if __name__ == "__main__":
    import os
    universe="108"

    curdir="/home/anatoly/Desktop/ximena/MET-1000_fa"
    new_dir = os.path.join(curdir, universe)
    os.mkdir(new_dir)

    sketch_file_names = [ os.path.join(new_dir,"XS_10.DAT"),
                         os.path.join(new_dir,"XP.DAT")]

    conver_xs_for_sketch_from_hdf5( universe= universe,
                                    openmc_file_name="/home/anatoly/Desktop/ximena/MET-1000_fa/mgxs/mgxs.h5",
                                    sketch_file_name=sketch_file_names)

    print ("Convertation from OPENMC to SKETCH complete")
