import os
import openmc
import numpy as np
import math
from MET_1000.materials import fuel_type1,fuel_type2,fuel_type3, fuel_type4, fuel_type5, clading, coolant, structure, helium


def get_plane(H, boundary="reflective/transmissive"):
    """H размер под ключ"""
    edge_length = H / math.sqrt(3)
    R = edge_length
    # установкавнешних границ для чехла ТВС
    bord = {
        1: openmc.Plane(a=-1 / (H / 2), b=0, c=0, d=1, boundary_type=boundary),
        2: openmc.Plane(a=-1 / H, b=1 / R, c=0, d=1, boundary_type=boundary),
        3: openmc.Plane(a=1 / H, b=1 / R, c=0, d=1, boundary_type=boundary),
        4: openmc.Plane(a=1 / (H / 2), b=0, c=0, d=1, boundary_type=boundary),
        5: openmc.Plane(a=1 / H, b=-1 / R, c=0, d=1, boundary_type=boundary),
        6: openmc.Plane(a=-1 / H, b=-1 / R, c=0, d=1, boundary_type=boundary),
    }

    return bord


def set_ugolki_2d(bord_outer, bord_inner, p1,p2,p3, material, name=""):
    """Задание уголков для ТВС
    bord_outer внешняя граница
    bord_inner внутренняя граница
    z_up верхняя граница чехла
    z_down нижняя граница чехла
    p1,p2,p3 плоскости для отсечения углов у чехла
    material материал
    name имя для идентификации

    возвращает
    cells  набор из 6 ячеек
    """


    cells = {}

    name_cell1 = "{} sector 1".format(name)
    coolant_cell1 = openmc.Cell(name=name_cell1, fill=material)
    coolant_cell1.region = -bord_outer[1]  & +bord_inner[1]   & -p1 & +p3
    cells[1] = coolant_cell1

    name_cell2 = "{} sector 2".format(name)
    coolant_cell2 = openmc.Cell(name=name_cell2, fill=material)
    coolant_cell2.region = -bord_outer[2] & +bord_inner[2]    & +p1 & -p2
    cells[2] = coolant_cell2

    name_cell3 = "{} sector 3".format(name)
    coolant_cell3 = openmc.Cell(name=name_cell3, fill=material)
    coolant_cell3.region = -bord_outer[3] & +bord_inner[3]    & +p2 & +p3
    cells[3] = coolant_cell3

    name_cell4 = "{} sector 4".format(name)
    coolant_cell4 = openmc.Cell(name=name_cell4, fill=material)
    coolant_cell4.region = -bord_outer[4]  & +bord_inner[4]   & -p3 & +p1
    cells[4] = coolant_cell4

    name_cell5 = "{} sector 5".format(name)
    coolant_cell5 = openmc.Cell(name=name_cell5, fill=material)
    coolant_cell5.region = -bord_outer[5]  & +bord_inner[5]   & -p1 & +p2
    cells[5] = coolant_cell5

    name_cell6 = "{} sector 6".format(name)
    coolant_cell6 = openmc.Cell(name=name_cell6, fill=material)
    coolant_cell6.region = -bord_outer[6] & +bord_inner[6]  & -p2 & -p3
    cells[6] = coolant_cell6

    return cells




if __name__ == "__main__":
    os.environ["OPENMC_CROSS_SECTIONS"] = "/home/anatoly/Desktop/ximena/cross_section_all/cross_sections.xml"

    import h5py

    mats = openmc.Materials(
        (fuel_type1, fuel_type2, fuel_type3, fuel_type4, fuel_type5, clading, coolant, structure, helium))
    mats.export_to_xml()


    mats.export_to_xml()

    # z boundaries
    boundary = "reflective"
    z_up = openmc.ZPlane(z0=17.16, boundary_type=boundary)
    z_down = openmc.ZPlane(z0=-17.16, boundary_type=boundary)

    ########################################
    # tvel geome
    r_pin = openmc.ZCylinder(r=0.64720 / 2 )
    fuel_cell = openmc.Cell()
    fuel_cell.fill = fuel_type1
    fuel_cell.region = -r_pin

    r_clad = openmc.ZCylinder(r=0.77134 / 2)
    clad_cell = openmc.Cell(fill=clading, region=+r_pin & -r_clad )

    pin_to_pin_dist = 0.8966  # cm


    H = pin_to_pin_dist
    edge_length = H / math.sqrt(3)
    R = edge_length

    p1 = openmc.Plane(a=1/H, b=1/R, c=0, d=0)
    p2 = openmc.XPlane(x0=0)
    p3 = openmc.Plane(a=-1/H, b=1/R, c=0, d=0)



    bord_inner = get_plane(H=pin_to_pin_dist - 0.1, boundary="transmission")

    coolant_cell_inner = openmc.Cell(name='coolant_cell_inner', fill=coolant)
    coolant_cell_inner.region = +r_clad & -bord_inner[1] & -bord_inner[2] & -bord_inner[3] & -bord_inner[4] & - \
    bord_inner[5] & -bord_inner[6]




    bord_outer = get_plane(H=pin_to_pin_dist, boundary="reflective")

    cells = set_chanel_2d(bord_outer=bord_outer, bord_inner=bord_inner, p1=p1,p2=p2,p3=p3,  material=coolant, name="chanel")

    pin_universe = openmc.Universe()
    pin_universe.add_cell(fuel_cell)
    pin_universe.add_cell(clad_cell)
    pin_universe.add_cell(coolant_cell_inner)
    for one_cell in cells.values():
        pin_universe.add_cell(one_cell)

    geom = openmc.Geometry(pin_universe)
    geom.export_to_xml()




    # Plot
    p = openmc.Plot()
    p.filename = 'cell_plot_example'
    p.width = (2, 2)
    p.pixels = (1000, 1000)
    p.color_by = 'material'
    p.colors = {fuel_type1: 'red', coolant: 'blue', clading: 'yellow', coolant: 'green'}

    plots = openmc.Plots([p])
    plots.export_to_xml()
    openmc.plot_geometry()

    # #Computing settings
    # batches = 100
    # inactive =50
    # particles =50000

    # Computing settings
    batches = 50
    inactive = 10
    particles = 5000

    set = openmc.Settings()
    set.batches = batches
    set.inactive = inactive
    set.particles = particles
    set.output = {'tallies': True}

    set.export_to_xml()

    # Creating an MGXS-library
    groups = openmc.mgxs.EnergyGroups()
    groups.group_edges = np.array([0.0, 0.46500E+01, 0.10000E+02, 0.21500E+02, 0.46500E+02, 0.10000E+03,
                                   0.21500E+03, 0.46500E+03, 0.10000E+04, 0.21500E+04, 0.46500E+04,
                                   0.10000E+05, 0.21500E+05, 0.46500E+05, 0.10000E+06, 0.20000E+06,
                                   0.40000E+06, 0.80000E+06, 0.14000E+07, 0.25000E+07, 0.40000E+07, 0.65000E+07,
                                   20.0e6])

    mgxs_lib = openmc.mgxs.Library(geom)
    mgxs_lib.energy_groups = groups

    mgxs_lib.mgxs_types = ['transport', 'nu-fission', 'fission', 'nu-scatter matrix', 'chi', 'absorption']

    mgxs_lib.domain_type = 'material'
    mgxs_lib.domains = geom.get_all_materials().values()

    mgxs_lib.by_nuclide = False
    mgxs_lib.build_library()

    tallies = openmc.Tallies()

    mgxs_lib.add_to_tallies_file(tallies, merge=True)

    tallies.export_to_xml()

    openmc.run(threads=4)

    sp = openmc.StatePoint('statepoint.100.h5')
    mgxs_lib.load_from_statepoint(sp)

    fuel_mgxs = mgxs_lib.get_mgxs(fuel_type1, 'nu-fission')
    fuel_mgxs_1 = mgxs_lib.get_mgxs(fuel_type1, 'absorption')

    fuel_mgxs_2 = mgxs_lib.get_mgxs(fuel_type1, 'fission')
    fuel_mgxs_3 = mgxs_lib.get_mgxs(fuel_type1, 'transport')
    fuel_mgxs_4 = mgxs_lib.get_mgxs(fuel_type1, 'nu-scatter matrix')
    fuel_mgxs_5 = mgxs_lib.get_mgxs(fuel_type1, 'chi')

    fuel_mgxs.print_xs()
    fuel_mgxs_1.print_xs()
    fuel_mgxs_2.print_xs()
    fuel_mgxs_3.print_xs()
    fuel_mgxs_4.print_xs()
    fuel_mgxs_5.print_xs()

    mgxs_lib.build_hdf5_store(filename='mgxs.h5')
