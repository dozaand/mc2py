#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
генерация простеньких шаблонов файлов проекта для code_blocks и msvc_10
"""

import mako
from mako.template import Template
import codecs
import os
import uuid
import argparse

pattern=u"""<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<CodeBlocks_project_file>
    <FileVersion major="1" minor="6" />
    <Project>
        <Option title="${project_name}" />
        <Option pch_mode="2" />
        <Option compiler="gcc" />
        <Build>
            <Target title="Debug">
                <Option output="bin/Debug/${project_name}" prefix_auto="1" extension_auto="1" />
                <Option object_output="obj/Debug/" />
                <Option type="${typ}" />
                <Option compiler="gcc" />
                <Compiler>
                    <Add option="-std=c++11" />
                    <Add option="-g" />
%for item in includes:
                    <Add directory="${item}" />
%endfor
                </Compiler>
                <Linker>
%for item in libs:
                    <Add library="${item}" />
%endfor
%for item in lib_dirs:
                    <Add directory="${item}" />
%endfor
                </Linker>
            </Target>
            <Target title="Release">
                <Option output="bin/Release/${project_name}" prefix_auto="1" extension_auto="1" />
                <Option object_output="obj/Release/" />
                <Option type="1" />
                <Option compiler="gcc" />
                <Compiler>
                    <Add option="-std=c++11" />
                    <Add option="-O2" />
%for item in includes:
                    <Add directory="${item}" />
%endfor
                </Compiler>
                <Linker>
                    <Add option="-s" />
%for item in libs:
                    <Add library="${item}" />
%endfor
%for item in lib_dirs:
                    <Add directory="${item}" />
%endfor
                </Linker>
            </Target>
        </Build>
        <Compiler>
            <Add option="-Wall" />
            <Add option="-fexceptions" />
        </Compiler>
%for item in src:
        <Unit filename="${item}" />
%endfor
        <Extensions>
            <code_completion />
            <envvars />
            <debugger />
            <lib_finder disable_auto="1" />
        </Extensions>
    </Project>
</CodeBlocks_project_file>
"""

def gen_prj_code_blocks(project_file,src,project_name=None,includes=[],libs=[],lib_dirs=[],prj_typ="exe"):
    u"""create code blocks project file"""
    project_type={"exe":1,"static_lib":2,"dll":3}
    if project_name is None:
        project_name = os.path.splitext(os.path.split(project_file)[1])[0]
    typ=project_type[prj_typ]
    cbprj = Template(pattern)
    res = cbprj.render(**locals())
    with codecs.open(project_file,"w",encoding="utf-8") as f:
        f.write(res)

##out_file_name="out.cbp"
##project_name = "part0"
##includes=["c:/ert","..",ur"c:\bbb\ddd",ur"c:\русская с пробелом\dir"]
##libs=["a.lib","b.lib","c.lib"]
##lib_dir=[".","mylib_dir"]
##src=["a.cpp","b.cpp","tmp/c.cpp"]

msvc_pattr_sln = """Microsoft Visual Studio Solution File, Format Version 11.00
# Visual Studio 2010
Project("{${uuid_sln}}") = "${project_name}", "${project_name}.vcxproj", "{${uuid_vcprj}}"
EndProject
Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution
		Debug|Win32 = Debug|Win32
		Release|Win32 = Release|Win32
	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution
		{${uuid_vcprj}}.Debug|Win32.ActiveCfg = Debug|Win32
		{${uuid_vcprj}}.Debug|Win32.Build.0 = Debug|Win32
		{${uuid_vcprj}}.Release|Win32.ActiveCfg = Release|Win32
		{${uuid_vcprj}}.Release|Win32.Build.0 = Release|Win32
	EndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
EndGlobal
"""

msvc_pattrr_prj = """<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <ItemGroup Label="ProjectConfigurations">
    <ProjectConfiguration Include="Debug|Win32">
      <Configuration>Debug</Configuration>
      <Platform>Win32</Platform>
    </ProjectConfiguration>
    <ProjectConfiguration Include="Release|Win32">
      <Configuration>Release</Configuration>
      <Platform>Win32</Platform>
    </ProjectConfiguration>
  </ItemGroup>
  <PropertyGroup Label="Globals">
    <ProjectGuid>{${uuid_vcprj}}</ProjectGuid>
    <Keyword>Win32Proj</Keyword>
    <RootNamespace>${project_name}</RootNamespace>
  </PropertyGroup>
  <Import Project="$(VCTargetsPath)\Microsoft.Cpp.Default.props" />
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'" Label="Configuration">
    <ConfigurationType>${typ}</ConfigurationType>
    <UseDebugLibraries>true</UseDebugLibraries>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Release|Win32'" Label="Configuration">
    <ConfigurationType>${typ}</ConfigurationType>
    <UseDebugLibraries>false</UseDebugLibraries>
    <WholeProgramOptimization>true</WholeProgramOptimization>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <Import Project="$(VCTargetsPath)\Microsoft.Cpp.props" />
  <ImportGroup Label="ExtensionSettings">
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'">
    <Import Project="$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" Condition="exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">
    <Import Project="$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" Condition="exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <PropertyGroup Label="UserMacros" />
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'">
    <LinkIncremental>true</LinkIncremental>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">
    <LinkIncremental>false</LinkIncremental>
  </PropertyGroup>
  <ItemDefinitionGroup Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'">
    <ClCompile>
      <PrecompiledHeader>
      </PrecompiledHeader>
      <WarningLevel>Level3</WarningLevel>
      <Optimization>Disabled</Optimization>
      <PreprocessorDefinitions>WIN32;_DEBUG;_CONSOLE;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <AdditionalIncludeDirectories>${include}</AdditionalIncludeDirectories>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <GenerateDebugInformation>true</GenerateDebugInformation>
      <AdditionalLibraryDirectories>${lib_dirs}</AdditionalLibraryDirectories>
      <AdditionalDependencies>kernel32.lib;user32.lib;gdi32.lib;winspool.lib;comdlg32.lib;advapi32.lib;shell32.lib;ole32.lib;oleaut32.lib;uuid.lib;odbc32.lib;odbccp32.lib;%(AdditionalDependencies);${libs}</AdditionalDependencies>
    </Link>
  </ItemDefinitionGroup>
  <ItemDefinitionGroup Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">
    <ClCompile>
      <WarningLevel>Level3</WarningLevel>
      <PrecompiledHeader>
      </PrecompiledHeader>
      <Optimization>MaxSpeed</Optimization>
      <FunctionLevelLinking>true</FunctionLevelLinking>
      <IntrinsicFunctions>true</IntrinsicFunctions>
      <PreprocessorDefinitions>WIN32;NDEBUG;_CONSOLE;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <AdditionalIncludeDirectories>${include}</AdditionalIncludeDirectories>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <GenerateDebugInformation>true</GenerateDebugInformation>
      <EnableCOMDATFolding>true</EnableCOMDATFolding>
      <OptimizeReferences>true</OptimizeReferences>
      <AdditionalLibraryDirectories>${lib_dirs}</AdditionalLibraryDirectories>
      <AdditionalDependencies>kernel32.lib;user32.lib;gdi32.lib;winspool.lib;comdlg32.lib;advapi32.lib;shell32.lib;ole32.lib;oleaut32.lib;uuid.lib;odbc32.lib;odbccp32.lib;%(AdditionalDependencies);${libs}</AdditionalDependencies>
    </Link>
  </ItemDefinitionGroup>
  <ItemGroup>
%for item in src:
    <ClCompile Include="${item}" />
%endfor
  </ItemGroup>
  <Import Project="$(VCTargetsPath)\Microsoft.Cpp.targets" />
  <ImportGroup Label="ExtensionTargets">
  </ImportGroup>
</Project>
"""

def gen_prj_msvc(project_file,src,project_name=None,includes=[],libs=[],lib_dirs=[],prj_typ="exe"):
    uuid_sln = str(uuid.uuid1()).upper()
    uuid_vcprj = str(uuid.uuid1()).upper()

    project_type={"exe":"Application","static_lib":"StaticLibrary","dll":"DynamicLibrary"}
    if project_name is None:
        project_name = os.path.splitext(os.path.split(project_file)[1])[0]

    typ=project_type[prj_typ]
    sln = Template(msvc_pattr_sln)
    res = sln.render(**locals())
    with codecs.open(project_file,"w",encoding="utf-8") as f:
        f.write(res)

    include = ";".join(includes)
    lib_dirs = ";".join(lib_dirs)
    libs = ";".join(libs)

    vcxproj = os.path.splitext(project_file)[0]+".vcxproj"

    sln = Template(msvc_pattrr_prj)
    res = sln.render(**locals())
    with codecs.open(vcxproj,"w",encoding="utf-8") as f:
        f.write(res)

#gen_prj_msvc("msvc_ex.sln","a.cpp b.cpp".split(),includes="d/a.h b.h".split(),libs=["a.lib"],lib_dirs=["my_lib_dir"])
nm="simple_project"
sconstruct_template="""import os

env = Environment(tools=['mingw',"packs_db"],ENV=os.environ)
pkg = env.UsePacks([])
lib=SConscript(['SConscript.py'], exports='env')
instdir = os.environ["usr_bin"]
obj_list = [lib]
env["PACK"] = dict(name="${nm}",version="0.0",LIBS=[lib[0].name])
inst = env.PackInstall(None,obj_list)
Alias("install",inst)
"""

script_template="""import os
Import('env')
src=Glob("src/*.cpp")
if env["CC"]=='gcc':
    env.Append(CCFLAGS=["-g","-std=c++11"])
if env["CC"]=='cl':
    env.Append(CPPFLAGS=["-MDd","-EHsc"])
lib = env.Library("${{CC}}_{nm}",src)
Return("lib")
"""

def default_part(nm):
    os.mkdir(nm)
    os.chdir(nm)
    os.mkdir("src")
    sln = Template(sconstruct_template)
    res = sln.render(**locals())
    with open("SConstruct","w") as f:
        f.write(res)
    res = script_template.format(nm=nm)
    with open("SConscript","w") as f:
        f.write(res)

def CreatePrj(target, source, env):
    # build project file
    
    if env["CC"]=='gcc':
        gen = gen_prj_code_blocks
        ext=".cbp"
    elif env["CC"]=='cl':
        gen = gen_prj_msvc
        ext=".sln"
    src = [i.sources[0].abspath for i in source[0].sources]
    gen(target+ext,src,
        target,includes=env["CPPPATH"],libs=env["LIBS"],lib_dirs=env["LIBPATH"],prj_typ="exe"
        )
    return None

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="create projects templates for popular compilers ")
    parser.add_argument("-s", "--src", nargs="*", help=u"source files")
    parser.add_argument("-i", "--includes", nargs="*", help=u"include files search paths")
    parser.add_argument("-l", "--libs", nargs="*", help=u"libs to be linked to project")
    parser.add_argument("-b", "--bintype",choices=["exe","static_lib","dll"], default="exe", help=u"target type")
    parser.add_argument("--lib_dirs", nargs="*", help=u"library search paths")
    parser.add_argument("-o","--output", help=u"project file name")
    parser.add_argument("-t","--target", nargs="*", choices=["code_blocks","msvc10"],default=["code_blocks"],help=u"target developers tools")
    parser.add_argument("-p","--part", help=u"name of part")
    args = parser.parse_args()
    if hasattr(args,"part"):
      default_part(args.part)
    if "msvc10" in  args.target:
      gen_prj_msvc(args.output,args.src,args.includes,args.libs,args.lib_dirs,args.bintype)
    if "code_blocks" in  args.target:
      gen_prj_code_blocks(args.output,args.src,args.includes,args.libs,args.lib_dirs,args.bintype)
