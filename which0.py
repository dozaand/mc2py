#! /usr/bin/env python

import sys
import os
import glob

def Name(fil):
    u"""
search file name for executables list
"""
    pathlist = os.environ['PATH'].split(os.pathsep)
    for dr in pathlist:
        for i in glob.glob(os.path.join(dr,fil)):
            return i

# Name(r"C:\apps\Notepad++\notepad++.exe")
if __name__ == '__main__':
    for i in sys.argv[1:]:
        res = Name(i)
        print(res)
