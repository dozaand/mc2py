#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
code to restart processes (for debugging web servers)
"""

import os
import subprocess as sp
import signal
from mc2py.svld import *
import argparse
#sp.check_call("taskkill /F /T /PID 2776", shell=True)
# интерфейс для консоли
if __name__ == '__main__':
    yfile="running.yaml"
    parser = argparse.ArgumentParser(prog="restartprog",description="start or restart prog")
    parser.add_argument("-t","--terminate_all",action='store_true',help=u"search files in subdirs")
    parser.add_argument("prog",nargs="*",help=u"prog to restart")
    data={}
    if os.path.exists(yfile):
        data=ld(yfile)
    args = parser.parse_args()
    if args.terminate_all:
        os.remove(yfile)
        for i in data:
            try:
                os.kill(data[i], signal.SIGINT)
            except:
                print("fail terminate {0} process {1}".format(i,data[i]))
    else:
        command_list = args.prog
        command = " ".join(command_list)

        if command in data:
            try:
                os.kill(data[command], signal.SIGINT)
            except:
                print("fail terminate {0} process pid: {1}".format(command,data[command]))
            finally:
                del data[command]
        pipe = sp.Popen(command_list)
        data[command] = pipe.pid
        sv(data,yfile)



