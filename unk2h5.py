#!/usr/bin/env python
# -*- coding: cp1251 -*-

import h5py
import numpy as np
from mc2py.evaldict import Scanf
import argparse
import re


def ImportUnkLib(infile, outfile, force):
    with h5py.File(outfile) as f:
        with open(infile, "rt") as ff:
            ff = iter(ff.read())
            try:
                while 1:
                    ind, somenum, s, sortname = Scanf(ff, "ifnn")
                    sName = "unk/"+re.sub(" ", "_", sortname.strip())
                    if force and sName in f:
                        del f[sName]
                    s, polytype = Scanf(ff, "ns")
                    n_x, n_y = Scanf(ff, "ii")
                    x_names = Scanf(ff, "s"*n_x)
                    y_names = Scanf(ff, "s"*n_y)
                    axiesMin = Scanf(ff, "f"*n_x)
                    axiesMax = Scanf(ff, "f"*n_x)
                    f[sName+"/axiesMin"] = axiesMin
                    f[sName+"/axiesMax"] = axiesMax
                    for i in range(n_y):
                        nterm, sectionname = Scanf(ff, "is")
                        koeff = np.array(Scanf(ff, "f"*nterm), dtype='f')
                        termdat = Scanf(ff, "i"*(n_x*nterm))
                        term = np.array(termdat, dtype='B').reshape((-1, n_x))
                        g = f.create_group(
                            "{sName}/sections/{sectionname}".format(**locals()))
                        g["koeffs"] = koeff
                        g["terms"] = term
                    spl, s = Scanf(ff, "sn")
                    nSplinePoints, nvar = Scanf(ff, "ii")
                    burnpoints = np.array(Scanf(
                        ff, "f"*nSplinePoints), dtype='f')
                    splinedat = np.array(Scanf(ff, "f"*(
                        nSplinePoints-1)*nvar*4), dtype='f').reshape((nvar, 4, nSplinePoints-1))
                    f["{sName}/t".format(**locals())] = burnpoints
                    for i, sectionname in enumerate(y_names):
                        f["{sName}/sections/{sectionname}/spline".format(
                            **locals())] = splinedat[i].T
                    s = Scanf(ff, "n")
            except:
                pass


def Test():
    ifile = "../VVER1000_KLN3.lib"
#    ifile=r"D:\distr\wwer\wwer1000\INPUT\lib_kln_work4.dat"
    ImportUnkLib(ifile, "outx.h5", 1)

#Test()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="import unk macrosection library to h5 format file")
    parser.add_argument("--out", default="unk.h5", help=u"output file")
    parser.add_argument(
        "-f", "--force", action='store_true', help=u"output file")
    parser.add_argument("infile", help=u"input file")
    args = parser.parse_args()
    ImportUnkLib(args.infile, args.out, args.force)
