#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
extract table data from word document
"""

from docx import Document
import yaml
import argparse
import datetime


def docx_extract_table(docname, itable, colinfo):
    data = []
    doc = Document(docname)
    for i in itable:
        tabl = doc.tables[i]
        for row in tabl.rows:
            try:
                d = {}
                for k, v in colinfo.items():
                    d[v] = row.cells[k].text
                data.append(d)
            except:
                pass
    return data


def t_corr(data):
    """"try to convert dates"""
    for row in data:
        for k in row:
            try:
                row[k] = datetime.datetime.strptime(row[k], "%d.%m.%Y").date()
            except:
                pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="extract table from ")
    parser.add_argument("-o", "--output", help=u"output file name")
    parser.add_argument("-w", "--linewidth", default=160,
                        type=int, help=u"output file name")
    parser.add_argument("-c", "--config", help=u"configuration file")
    parser.add_argument("file", help="input file name")
    args = parser.parse_args()
    with open(args.config, "rt", encoding="utf-8") as f:
        cfg = yaml.load(f)
    table_data = docx_extract_table(args.file, cfg["tables"], cfg["cols"])
    t_corr(table_data)
    with open(args.output, "wt", encoding="utf-8") as f:
        yaml.dump_all(table_data, f, default_flow_style=False,
                      allow_unicode=True, width=args.linewidth)
