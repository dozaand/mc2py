#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
������ � �������� ���� ��� ���� �� �� ������ ���������� ����� ���������
"""
import _winreg

# k=_winreg.OpenKey(_winreg.HKEY_CURRENT_USER,"Software\\WinEdt 6",0,_winreg.KEY_ALL_ACCESS)
# with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE,r"SYSTEM\CurrentControlSet\Control\Session Manager\Environment",0,_winreg.KEY_ALL_ACCESS) as k:
### get:
###    (val,typ)=_winreg.QueryValueEx(k,"Inst")
###    print "key is ",val
### set:
###    _winreg.SetValueEx(k,"Inst",0,_winreg.REG_SZ,u'216092928')
### del :
##    q=_winreg.QueryValueEx(k,'PAT')
##    print str(q[0])
##    #_winreg.DeleteValue(k,"Inst")
##
##
# with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE,r"SYSTEM\CurrentControlSet\Control\Session Manager\Environment",0,_winreg.KEY_SET_VALUE) as k:
### get:
###    (val,typ)=_winreg.QueryValueEx(k,"Inst")
###    print "key is ",val
### set:
###    _winreg.SetValueEx(k,"Inst",0,_winreg.REG_SZ,u'216092928')
### del :
##    _winreg.SetValueEx(k,'python_lib',0,_winreg.REG_SZ,r"C:\\Python26\\libs")


def SetEnvironment(dic):
    u"""���������� (��� ������) � ���������� ��������� path �������� ������ ��� windows"""
    keys = dic.keys()
    exist = []
    for key in keys:
        try:
            with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, r"SYSTEM\CurrentControlSet\Control\Session Manager\Environment", 0, _winreg.KEY_ALL_ACCESS) as k:
                val = str(_winreg.QueryValueEx(k, key)[0])
                exist = val.split(';')
        except:
            pass
        for el in exist:
            if el not in dic[key]:
                dic[key].append(el)
        values = ''
        for el in dic[key]:
            values += el
            values += ';'
        with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, r"SYSTEM\CurrentControlSet\Control\Session Manager\Environment", 0, _winreg.KEY_SET_VALUE) as k:
            _winreg.SetValueEx(k, key, 0, _winreg.REG_SZ, values)

if __name__ == '__main__':
    dic = {'PATH1': ['C', 'q']}
    SetEnvironment(dic)
