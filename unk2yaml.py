#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import argparse
import re
import yaml
from mc2py.re_iter import *
import subprocess as sp
import os
import logging
import shutil

def sstrip(obj):
    try:
        v=re.sub("__+","_",re.sub(r"[ :,\.]","_",obj.strip()))
        return v
    except:
        print "decode error ",obj
        return None


symb_18 = re.compile("(.{18})",re.MULTILINE)

# копим все термы
def Term2Index(mp,terms):
    return np.array(map(lambda x:mp[tuple(x)],terms),dtype='H').tolist()

def ReduseTerms(li):
    u"""собираем термы от всех сечений в один список меняем термы на индексы"""
    a=set()
    for ts in li:
        for ter in ts["term"]:
            a.add(tuple(ter))
    ats=np.array(sorted(list(a)),dtype="B")
    mp=dict([(tuple(ter),i) for i,ter in enumerate(ats)])
    for ts in li:
        ts["term"]=Term2Index(mp,ts["term"]) # меняем термы на индексы
    dkt = {}
    for i in li:
        j=i.pop("grpname")
        dkt[j]=i
    return ats.tolist(),dkt


def ImportUnkLib(infile,outdir=None,force=1,is_zip=0):

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh = logging.FileHandler('unk2yaml.log',mode="w")
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.WARNING)
    console_handler.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(console_handler)    

    if outdir is None:
        outdir=os.path.splitext(infile)[0]
        if outdir == infile:
            outdir+="_dir"
    if not os.path.exists(outdir):
        os.makedirs(outdir)


    itr=ReIter(infile,encoding='cp1251')

    while(not itr.empty()):
        res={}
        ind = itr.int()
        somenum = itr.float()
        itr.endl()
        itr.endl()
        sortname = itr.endl(sstrip)

        itr.endl()
        sName=sortname+".yaml"
        polytype=itr.search('"(.)"',str)
        n_x = itr.int()
        n_y = itr.int()
        itr.endl()
        x_names = itr.list(symb_18,maxcount=n_x,f=sstrip,skip="\n")
        itr.endl()
        y_names = itr.list(symb_18,maxcount=n_y,f=sstrip,skip="\n")
        axiesMin = itr.list(is_float,maxcount=n_x,f=float,skip="\n")
        itr.endl()
        axiesMax = itr.list(is_float,maxcount=n_x,f=float,skip="\n")
        itr.endl()

#        res["xNames"]=x_names
#        res["yNames"]=y_names
        res["axiesMin"]=axiesMin
        res["axiesMax"]=axiesMax

        sectinfo=[]
        for i in range(n_y):
            nterm = itr.int()
            grpname = itr.endl(sstrip)
            koeff=itr.list(is_float,f=float,maxcount=nterm,skip="\n")
            term=[]
            for iterm in range(nterm):
                v = itr.list(is_int,f=int,maxcount=n_x)
                itr.endl()
                term.append(v)
            sectinfo.append(dict(grpname=grpname, koeff=koeff, term=term))
        ats,si = ReduseTerms(sectinfo)
        res["terms"]=ats
        res["sections"] = si
        spl = (itr.endl(f=str)).strip()
        if "SPLINE" == spl:
            nSplinePoints = itr.int()
            nvar = itr.int()
            burnpoints = itr.list(is_float,maxcount=nSplinePoints,f=float,skip="\n")
            splinedat = itr.list(is_float,maxcount=((nSplinePoints-1)*nvar*4),f=float,skip="\n")
            spl = itr.endl(f=str)
            spl = itr.endl(f=str)
            res["spline"]=splinedat
            res["t"]=burnpoints
        f_name = os.path.join(outdir, sName)

        logger.info(sortname)
        if (not os.path.exists(f_name)) or force:
            with codecs.open(f_name,"w",encoding="utf-8") as f:
                yaml.safe_dump(res,f,allow_unicode=True)
        else:
            logger.warning("skiping {0}".format(f_name))
    if is_zip:
        logger.info("begin compression")
        sp.check_call("7z a -r -ms=off -m5=lzma2 {0}.7z {0}".format(outdir))
        logger.info("remove temp files")
        shutil.rmtree(outdir)
        logger.info("task done")



if __name__=="__main__":
    parser = argparse.ArgumentParser(prog="import unk macrosection library to yaml format files")
    parser.add_argument("-o","--out",default=None,help=u"output directory name")
    parser.add_argument("-f","--force",action='store_true',help=u"owerwrite file if exists")
    parser.add_argument("-z","--zip",action='store_true',help=u"output file")
    parser.add_argument("infile",help=u"input file")
    args = parser.parse_args()
    ImportUnkLib(args.infile, args.out, args.force,args.zip)
