#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
���� ������ ����������� ������ ��� � ����������
� �������������� ������� � ���� �������� ���������� �������� �������
��
y(x)=\sum_{i=1}^N(k_i\cdot f_i(x))
������� ��� ��� ����� �������� ��� ����������� ������� ��� ��� � ����
"""
import numpy as np
import math
from int_bsearch import *
from scipy import linalg
from numpy.linalg import norm


def MakeFun(body, argl):
    u"""������������ �������� �� ����������� �� �������� � ������ ����������
>>> f=MakeFun("x*y","x,y")
>>> f([5,6])
30"""
    s = """
def fun(vector):
    %(argl)s=vector
    return %(body)s
""" % locals()
    exec(s)
    return fun


def MakeFunList(fl, argl):
    u"""������������� ������� ������� �� �� ����������� ��������
������ �������������
>>> ff=MakeFunList("x*y,x+y","x,y")
>>> [ff[0]([5,6]),ff[1]([5,6])]
[30, 11]
"""
    li = fl.split(",")
    return [MakeFun(body, argl) for body in li]


def DefaultDot(a, b):
    u"""dot �� ���������"""
    return a.dot(b)


def NumbDot(a, b):
    u"""dot ��� �����"""
    return a * b


def NpDot(a1, a2):
    u"""��������� ������������ ���������� ��� ������������ ��������"""
    return np.dot(a1.ravel(), a2.ravel())


class Tfit(object):
    u"""����� ��� ������������� ������� � ���� �������� ���������� �������"""

    def __init__(self, func, dot=DefaultDot, dtype='f', solve_type='lu'):
        u"""func - ����������� ����� �������
            solve_type - lu, lstsq
         """
        self._func = func
        self._dotfun = dot
        n = len(func)
        self.koeff = np.zeros(n, dtype=dtype)
        self._yfi = np.zeros(n, dtype=dtype)
        self._fifj = np.zeros((n, n), dtype=dtype)
        self.solve_type = solve_type
        self.Ok = 0

    def _Update(self, x, y, w=1):
        u"""���������� ������"""
        fi = np.array([f(x) for f in self._func])
        n = len(self._func)
        w2 = w * w
        for i in xrange(n):
            for j in xrange(i, n):
                v = w2 * self._dotfun(fi[i], fi[j])
                if i == j:
                    self._fifj[i, j] += v
                else:
                    self._fifj[i, j] += v
                    self._fifj[j, i] += v
        for i in xrange(n):
            self._yfi[i] += w2 * self._dotfun(fi[i], y)
        self.Ok = 0

    def _koeff(self):
        u"""���������� ������������� ����������"""
        if self.solve_type == 'lu':
            self.koeff = linalg.solve(self._fifj, self._yfi)
        elif self.solve_type == 'lstsq':
            self.koeff, self.resid, self.rank, self.sigma = linalg.lstsq(
                self._fifj, self._yfi)
        self.Ok = 1

    def Koeff(self):
        if not self.Ok:
            self._koeff()
        return self.koeff

    def insert(self, x, y, w=1):
        u"""��� ���������� ����� ����� ���������
        �������������� ��� ���� ��� ��� �� �� 1"""
        self._Update(x, y, w)

    def add(self, x, y, w=None):
        u"""��������� ������� ������"""
        if w is None:
            for xx, yy in zip(x, y):
                self.insert(xx, yy)
        else:
            for xx, yy, ww in zip(x, y, w):
                self.insert(xx, yy, ww)

    def remove(self, x, y, w=1):
        u"""��������� �����"""
        self._Update(x, y, -w)

    def residual(self, x, y):
        u"""��������� y- sum k_i*f_i(x)"""
        return y - self.__call__(x)

    def __call__(self, x):
        u"""�������� ������� � �������� �����"""
        if not self.Ok:
            self._koeff()
        y = self._func[0](x) * self.koeff[0]
        for f, k in zip(self._func, self.koeff)[1:]:
            y += f(x) * k
        return y

    def clear(self):
        u"""������������� � ��������� ���������"""
        self.Ok = 0
        self._yfi[:] = 0.
        self._fifj[...] = 0.


def Fit(data, func_string, var_string, dot=NumbDot, solve_type='lu'):
    u"""
    solve_type - lu, lstsq
    ����� ������ Fit ����� �� ��������
    ����� ����������� ������ �� mathematica
>>> xx=np.linspace(0,6,100)
>>> x=np.sin(xx)
>>> y=np.cos(xx)
>>> d=np.sin(x)*np.cos(2.25*y)
>>> data=np.vstack([x,y,d]).T
>>> ft=Fit(data,"1,x,y","x,y")
>>> print round(ft.residual([x[5],y[5]],d[5]),4)
-0.2673
    """
    fl = MakeFunList(func_string, var_string)
    fit = Tfit(fl, dtype='d', dot=dot, solve_type=solve_type)
    l, m = data.shape
    fit.add(data[:, :m - 1], data[:, m - 1:])
    return fit


class TerFit(Tfit):
    u"""����� ��� ������������� ������� � ���� �������� ���������� ������� +
     ������ ����������� ������� �������������"""

    def __init__(self, func, dot=DefaultDot, dtype='f'):
        u"""func - ����������� ����� �������"""
        self._func = func
        self._dotfun = dot
        n = len(func)
        self.koeff = np.zeros(n, dtype=dtype)
        self._yfi = np.zeros(n, dtype=dtype)
        self._fifj = np.zeros((n, n), dtype=dtype)
        self.Ok = 0
        self.err = 0
        self._XX = np.zeros((n + 1, n + 1))
        self.KK = np.zeros((n + 1, n + 1))
        self.n = 0
        self.npoint = 0

    def _Update(self, x, y, w=1):
        u"""���������� ������"""
        fi = [f(x) for f in self._func]
        n = len(self._func)
        self.n += w
        if w < 0:
            self.npoint -= 1
            w2 = -w * w
        else:
            self.npoint += 1
            w2 = w * w

        for i in xrange(n):
            for j in xrange(i, n):
                v = w2 * self._dotfun(fi[i], fi[j])
                if i == j:
                    self._fifj[i, j] += v
                else:
                    self._fifj[i, j] += v
                    self._fifj[j, i] += v
        self._XX[1:, 1:] = self._fifj
        for i in xrange(n):
            self._yfi[i] += w2 * self._dotfun(fi[i], y)
        self._XX[1:, 0] = self._yfi
        self._XX[0, 1:] = self._yfi
        self._XX[0, 0] += w2 * self._dotfun(y, y)
        self._koeff()
        K = np.zeros((n + 1, 1))
        K[1:, 0] = self.koeff[:]
        K[0, 0] = -1
        for in1, val1 in enumerate(K):
            for in2, val2 in enumerate(K):
                self.KK[in1, in2] = val1 * val2
        if self.npoint > 3:
            s = (self.KK.ravel() * self._XX.ravel()).sum()
        else:
            s = 0
        self.err = s
        s = 0
        self.Ok = 1

    def _Update1(self):
        u"""���������� ������, � ������ ���� ������ ��������� �����,
         � ������� �������� �����"""
        self._XX[1:, 1:] = self._fifj
        self._XX[1:, 0] = self._yfi
        self._XX[0, 1:] = self._yfi
        self._koeff()
        n = self._fifj.shape[0]
        K = np.zeros((n + 1, 1))
        K[1:, 0] = self.koeff[:]
        K[0, 0] = -1
        for in1, val1 in enumerate(K):
            for in2, val2 in enumerate(K):
                self.KK[in1, in2] = val1 * val2

    def clear(self):
        u"""������������� � ��������� ���������"""
        self.Ok = 0
        self._yfi[:] = 0.
        self.koeff[:] = 0.
        self._fifj[...] = 0.
        self._XX[...] = 0.
        self.KK[...] = 0.
        self.err = 0.
        self.n = 0
        self.npoint = 0


def testfit():
    u""" ��������� fit
>>> testfit()
True True
True True
    u"""
    x = np.arange(0, 5.01, 0.1)
    fp = 0.1 * np.sin(x) + 0.5 * np.cos(x)
    fpn = np.array(
        [0.597955, 0.490419, 0.514771, 0.578865, 0.481088, 0.454185, 0.40308,
         0.430243, 0.497313, 0.298163, 0.450676, 0.243524, 0.314247, 0.266261,
         0.111344, 0.184799, 0.0125907, 0.103887, -
         0.0361252, -0.123705,
         -0.0229981, -0.0824346, -0.144198, -
         0.221252, -0.212143, -0.269826,
         -0.371057, -0.362607, -0.426364, -
         0.536253, -0.452706, -0.570163,
         -0.442617, -0.505514, -0.425283, -
         0.56705, -0.564654, -0.498194,
         -0.38211, -0.527335, -0.323526, -
         0.272634, -0.261073, -0.251556,
         -0.163151, -0.204035, -0.196112, -
         0.0463849, -0.138279, 0.0544704,
         -0.0395492])
    fit = Tfit([np.sin, np.cos], dtype='d')
    fit.add(x, fp)
    print norm(fit.Koeff() - [0.1, 0.5]) < 1e-16, norm(np.array([fit(0), fit(5)]) - np.array([0.5, 0.0459386652653])) < 1e-6
    fit.clear()
    fit.add(x, fpn)
    print norm(fit.Koeff() - [0.10626132, 0.48586675]) < 1e-6, norm(np.array([fit(0), fit(5)]) - np.array([0.48586675428763587, 0.03592546414829044])) < 1e-6


def k0(x):
    return 1


def k1(x):
    return x


def k2(x):
    return x**2


def k3(x):
    return x**3


class TsmSpline4(object):

    def __init__(self, Eifun, dot, eps=1e-4):
        self.Eifun = Eifun
        self.fit4 = TerFit([k0, k1, k2, k3], dot=dot)
        self.eps = eps
        self.gn = self._AddFunct()
        self._endOfData = 0

    def TransformMatrix(self, tt0):
        u"""�������������� ������� ��� �������� ����� �������"""
        ff = self.fit4._fifj.copy()
        yf = self.fit4._yfi.copy()
        self.fit4.clear()
        A = np.array([[1, 0, -3 * tt0**(-2), 2 * tt0**(-3)], [0, 1, -2 * tt0**(-1), tt0**(
            -2)], [0, 0, 3 * tt0**(-2), -2 * tt0**(-3)], [0, 0, -1 * tt0**(-1), tt0**(-2)]])
        ffs = np.dot(np.dot(A, ff), A.T)
        yfs = np.dot(A, yf)
        kks, resid, rank, sigma = linalg.lstsq(ffs, yfs)
#            kks=np.linalg.solve(ffs, yfs)
        # �������� �������
        ffn = self.fit4._fifj
        yfn = self.fit4._yfi
        ffn[:2, :2] = ffs[2:, 2:]
        yfn[:2] = yfs[2:] - np.dot(ffs[2:, :2], kks[:2])

    def Error(self, XX):
        "����� ��������� ����������"
        KK1 = self.fit4.KK
        XX1 = self.fit4._XX - XX
        s = (KK1.ravel() * XX1.ravel()).sum()
        return s

    def _AddFunct(self):
        # t0- ����� ��� ������ ����� ��������� tn ����� ��� ��������� �����
        # ���������a
        self.tn = self.t

        while True:
            t0 = self.tn
            self.fit4._Update1()
            XX = self.fit4._XX.copy()
            t = self.t
            y = self.y
            self.fit4.insert(t - t0, y)
            while self.Error(XX) < self.eps:
                # while self.Error(XX)/(self.fit4.n)<self.eps or self.fit4.n<4:
                yield
                if self._endOfData:
                    break
                told = t
                t = self.t
                y = self.y
                self.fit4.insert(t - t0, y)
            if (told - t0) < 0.001:
                pass
            self.fit4.remove(t - t0, y)
            koeff = self.fit4.koeff
            f0 = koeff[0]
            df0 = koeff[1]
            self.Eifun(self.tn, f0, df0)

            tt0 = told - t0

            if self._endOfData:
                f1 = koeff[0] + \
                    (koeff[1] + (koeff[2] + koeff[3] * tt0) * tt0) * tt0
                df1 = koeff[1] + (2 * koeff[2] + 3 * koeff[3] * tt0) * tt0
                self.Eifun(t, f1, df1)
                return
            self.tn = told
            self.TransformMatrix(tt0)

    def EndOfData(self):
        self._endOfData = 1
        try:
            self.gn.next()
        except StopIteration:
            return

    def insert(self, t, y):
        self.t = t
        self.y = y
        self.gn.next()


def linear_combination(fl, koeff):
    u"""return function - linear combinations"""
    return lambda x: np.dot(np.array(koeff), [f(x) for f in fl])


def str_to_linear_combination(funs, args, koeff):
    u"""������ ������� �� ���������� ��������"""
    fl = fit.MakeFunList(funs, args)
    return linear_combination(fl, koeff)


# res=([],[])
##
# def sav(t,f,df):
# print t,f,df
# res[0].append(t)
# res[1].append((f,df))
##
##
# x=np.arange(0,10.01,0.01)
# v=(np.sin(x))**2+np.cos(x)
##
##
# fit4=TerFit([k0,k1,k2,k3],dot=NumbDot)
# fit4.add(x,v)
# sm=TsmSpline4(sav,NumbDot,eps=1e-4)
# for t,data in zip(x,v):
# sm.insert(t,data)
# sm.EndOfData()
##
# def Spl4Interp(t,data):
# tt=data[0]
# dd=data[1]
# i=int_bsearch(tt,t)
# t1=tt[i+1]-tt[i]
# tcurr=-tt[i]+t
# f0,df0=dd[i]
# f1,df1=dd[i+1]
# f0=dd[0][i]
# df0=dd[1][i]
# f1=dd[0][i+1]
# df1=dd[1][i+1]
# return f0 + df0*tcurr - ((3*f0 - 3*f1 + (2*df0 + df1)*t1)*tcurr**2)/t1**2 + ((2*f0 - 2*f1 + (df0 + df1)*t1)*tcurr**3)/t1**3
##
# tims=np.array([0., 0.2, 0.4, 0.6, 0.8, 1., 1.2, 1.4, 1.6, 1.8, 2., 2.2, 2.4, 2.6, 2.8, 3.])
# dat=np.array([[0., 1.], [0.198669, 0.980067], [0.389418, 0.921061], [0.564642, \
# 0.825336], [0.717356, 0.696707], [0.841471, 0.540302], [0.932039, \
# 0.362358], [0.98545, 0.169967], [0.999574, -0.0291995], [0.973848, \
# -0.227202], [0.909297, -0.416147], [0.808496, -0.588501], [0.675463, \
# -0.737394], [0.515501, -0.856889], [0.334988, -0.942222], [0.14112, \
# -0.989992]]).T
# print Spl4Interp(0.5,([0,1],[(0,0),(1,0)]))
##
##
##
# a=[]
# for t in x:
# a.append(Spl4Interp(t,res))
# koef=np.array([v[0] for v in res[1]])
# times=np.array(res[0])
# plot(x,a-np.exp(-3*x))
# plot(x,a-(np.sin(x))**2-np.cos(x))
# plot(x,a,x,(np.sin(x))**2+np.cos(x))
# plot(tims,np.sin(tims)-a)
# show()
##
# Spl4Interp(t,dat)
##
