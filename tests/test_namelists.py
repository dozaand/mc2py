#!/usr/bin/env python
# -*- coding: cp1251 -*-

import unittest
import os
from mc2py.fortrannamelist import ReadNamelists, ReadFileNamelists, ReadNameListBody


class TestGlobalFunctions(unittest.TestCase):

    def setUp(self):
        fc = """
nyear= 1984, nmonth= 6,
 ef=              # some comment
     0.49 3*0.0,
 ef(1)= 55,55,
 s='asda'
"""
        fc1 = "&nml1\n"+fc+"\n/&nml2\n"+fc+"\n&end"
        with open("tnm1.dat", "wt") as f:
            f.write(fc)
        with open("tnm2.dat", "wt") as f:
            f.write(fc1)

    def tearDown(self):
        os.remove("tnm1.dat")
        os.remove("tnm2.dat")

    def testReadFileNamelists(self):
        res = ReadFileNamelists("tnm2.dat")
        ref = {
            'NML1': {'EF': [55, 55, 0.0, 0.0], 'NMONTH': 6, 'NYEAR': 1984, 'S': 'asda'},
            'NML2': {'EF': [55, 55, 0.0, 0.0], 'NMONTH': 6, 'NYEAR': 1984, 'S': 'asda'}}
        self.assertEqual(ref, res)

    def testReadNameListBody(self):
        res = ReadNameListBody("tnm1.dat")
        ref = {'EF': [
            55, 55, 0.0, 0.0], 'NMONTH': 6, 'NYEAR': 1984, 'S': 'asda'}
        self.assertEqual(ref, res)

if __name__ == '__main__':
    unittest.main()
