#!/usr/bin/env python
# -*- coding: utf-8 -*-

import yaml
import numpy as np
import h5py
import re
import os
import argparse


t2t={'real32':np.float32,'real64':np.float64}

def ts(lin):
    x=re.split("[ \\*]+",lin)
    return (t2t[x[0]],tuple([int(i) for i in x[1:]]))

#types=[re.split(i["Type"]) for i in data_s]
def ibrplt2h5(plt_file, plt_file_dsc, output_file):
    u"""
    convert ibrae plot file version3 to hdf5 file
    """
    with open(plt_file_dsc,"r") as f:
        data=yaml.load(f)

    ofs0=data["file"]["First record offset"]
    step=data["file"]["Record stride"]
    nrec=data["file"]["Record count"]

    with h5py.File(output_file,"w") as f5, open(plt_file,"rb") as f:
        for rec in data["vars"]:
            typ,shp=ts(rec["Type"])
            shp=(nrec,)+shp[::-1]
            nm=rec['Variable']
            dat=np.zeros(shp,dtype=typ)
            objofs=rec['Offset']
            for i in range(nrec):
                f.seek(ofs0+step*i+objofs)
                tmp=np.fromfile(f,dtype=typ,count=dat[0].size)
                dat[i,...]=tmp.reshape(dat[i].shape)
            f5.create_dataset(name=nm,data=dat)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="convert ibrae plot file to h5 format")
    parser.add_argument("-o", "--output", default="", help=u"sourse directory")
    parser.add_argument("input", help=u"input plt file")
    args = parser.parse_args()
    file_input=args.input
    file=os.path.splitext(file_input)[0]
    file_dsc=file+".yaml"
    if args.output:
        file_output=args.output
    else:
        file_output=file+".h5"

    ibrplt2h5(file_input,file_dsc,file_output)

