from pathlib import Path
"""приводим все к нижнему регистру"""

def rnlwr(p):
    for i in Path(p).glob("*"):
        nm = i.name.lower()
        i.rename(i.parent/nm)
    for i in Path(p).glob("*"):
        if i.is_dir():
            rnlwr(i)

def spj_lower(p):
    for i in p.glob("**/*.spj"):
        with open(i,"r",encoding="cp1251") as f:
            data=f.read()
        with open(i,"w",encoding="cp1251") as f:
            f.write(data.lower())


p = Path(".")
rnlwr(p)
spj_lower(p)