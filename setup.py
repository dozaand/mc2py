#!/usr/bin/env python

from distutils.core import setup

setup(name='mc2py',
      version='1.0',
      description='Energy tools',
      author='Andrey Semenov',
      author_email='dozaand@mail.ru',
      url='',
      packages=['distutils', 'distutils.command'],
     )