#!/usr/bin/env python
# -*- coding: utf-8 -*-

import zmq
import numpy as np
import struct
import time
from threading import Thread
from contextlib import contextmanager
from mc2py.fold import mapf

commandlist = "STEP,STOP,MULTISTEP,SET_DATA,GET_DATA,SET_MODE,GET_MODE,GET_VAR_LIST,CREATE_GROUP,SET_GROUP,GET_GROUP,DELETE_GROUP,GET_TYPE_NAMES,TO_SYNCRO,FROM_SYNCRO,SAVE_STATE,LOAD_STATE,WAIT_FOR_STEP,GET_SYNCRO_MODE,FIX_VAR,RELEASE_VAR,LAST_USER_COMMAND".split(
    ",")
type
for i, v in enumerate(commandlist):
    vars()[v] = np.array([i])


class TZMQScriptException(Exception):
    """ исключение в случае если переменные не найдены или не совпадает их длина"""

    def __init__(self, msg):
        super(TZMQScriptException, self).__init__(msg)


class Text_data(object):
    """ драйвер блока данных заданного типа"""

    def __init__(self, source, data, name):
        self.source = source
        self.name = name
        self.data = data

    def get(self):
        self.data[...] = self.source.get(self.name)

    def put(self):
        self.source.put(self.name, self.data)

    def __getitem__(self, sl):
        return self.data[sl]

    def __setitem__(self, sl, data):
        self.data[sl] = data
        self.put()

    def __repr__(self):
        return self.data.__repr__()

    def __iadd__(self, data):
        self.data += data
        self.put()
        return self

    def __isub__(self, data):
        self.data -= data
        self.put()
        return self

    def __imul__(self, data):
        self.data *= data
        self.put()
        return self

    def __idiv__(self, data):
        self.data /= data
        self.put()
        return self

    def __len__(self):
        return len(self.data)

    def __sub__(self, other):
        return self.data - other

    def __add__(self, other):
        return self.data + other

    def __float__(self):
        """if data.shape == (1,):
            [self.data[i]=float(self.data[i]) for i in range(len(self.data))]
            return data
        else:"""
        return float(self.data[...])

class Text_data_tuple:

    def __init__(self, source, data, name):
        self.source = source
        self.name = name
        self.data = data

        self.multiData = [Text_data(source, data_, name_) for data_, name_ in zip(data, name)]

    def __getitem__(self, index):
        return self.data[index]

    def __setitem__(self, index, val):
        if type(index) is slice:
            for name_, data_ in zip(self.name[index], val):
                self.source._var_db[name_][...] = np.array(data_)
                self.source.put(name_, self.source._var_db[name_])

        else:
            self.source._var_db[self.name[index]][...] = np.array(val)
            self.source.put(self.name[index], self.source._var_db[self.name[index]])


    def __repr__(self):
        mass =  [float(i) for i in self.data]
        return  str(mass)


class Tscript_drv(object):
    """ клиент сервера сценариев
    """

    def __init__(self, addr, code="cp1251"):
        """
        :param: addr - адрес по которому надо получать данные
        :param: code - кодировка, в которой ищутся имена в модели
        """
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REQ)
        self._var_db = {}
        self.socket.connect(addr)
        self.funs = {}  # функции выполняемые на каждом шагу

        self.is_syncro = False
        self.addr = addr
        self.encoding = code

        self.orvon_vars = []

    def is_syncro_ex(self):
        self.socket.send(GET_SYNCRO_MODE.data)
        res = self.socket.recv()
        return True if res == b'1' else False

    def __getattr__(self, index):
        """получить значение"""
        if index in self.__dict__:
            return self.__dict__[index]
        else:
            return self.__getitem__(index)

    def __setattr__(self, index, data):
        """установить значение"""
        if index in self.__dict__ or index in ["context", "socket", "_var_db", "funs", "is_syncro", "is_syncro_ex",
                                               "addr", "encoding", "orvon_vars"]:
            self.__dict__[index] = data
        else:
            self.__setitem__(index, data)

    def __getitem__(self, index):
        """
        получение значения с сервера
        :param: index - имя переменной
        """
        """сделали фичу для засасывания массива перемнных типа tuple"""
        if (type(index) is tuple):
            data = [self.get(name) for name in index]
            for k, name in enumerate(index):
                self._var_db[name] = data[k]
            return Text_data_tuple(self, data, index)
        else:
            data = self.get(index)
            self._var_db[index] = data
            return Text_data(self, data, index)

    def rrr(self):
        data_ = np.zeros(5, dtype='i')
        vvv = type(data_)
        vvv = type(data_)

    def __setitem__(self, index, data):
        """
        отсылка нового значения
        :param: index - имя переменной
        :param: data - данные. Должны быть numpy array.
        """
        if not issubclass(type(data), Text_data):
            if not type(data) is np.ndarray:
                data = np.array(data)
            if not index in self._var_db:
                # если переменной еще не было в кэше, то нужно его получить первый раз, чтобы узнать ее shape
                self[index]
            self._var_db[index][...] = data
            self.put(index, self._var_db[index])

    def get(self, index):
        self.socket.send_multipart([GET_DATA.data, index.encode(self.encoding)])
        mp = self.socket.recv_multipart()
        if not mp[0]:
            raise TZMQScriptException("var not found " + index)

        if mp[0].startswith("[".encode(self.encoding)) or mp[0].startswith("(".encode(self.encoding)):
            typ = eval(mp[0])
        else:
            typ = mp[0]
        data = np.fromstring(mp[1], dtype=typ)
        if data.shape == (1,):
            return np.array(data[0])
        else:
            return data

    def put(self, index, data):
        self.socket.send_multipart([SET_DATA.data, index.encode(self.encoding), data.data])
        res = self.socket.recv()
        if not res:
            raise TZMQScriptException("fail set value for " + index)

    def stop(self):
        "send stop command to model"
        self.socket.send(STOP.data)
        res = self.socket.recv()

    def step(self, n=1):
        """do some steps of model"""
        if n == 1:
            self.socket.send(STEP.data)
            res = self.socket.recv()
        else:
            self.socket.send_multipart([MULTISTEP.data, struct.pack("i", n)])
            res = self.socket.recv()
        for i in self.funs.values():
            i(n)

    def astep(self, n=1):
        """
        do some steps of model in background thread
        usage:

        ft=v.astep(2)

        for i in range(10):
            print("on substep",i)
            do_some()

        ft.join()

        """
        obj = Thread(target=self.step, args=(n,))
        obj.start()
        return obj

    def save_state(self, name):
        u"""do simulator save_state"""
        if not r".sta" in name: name += r".sta"
        self.socket.send_multipart([SAVE_STATE.data, name.encode()])
        return self.socket.recv()

    def load_state(self, name):
        u"""do simulator load_state"""
        if not r".sta" in name: name += r".sta"
        self.socket.send_multipart([LOAD_STATE.data, name.encode()])
        return self.socket.recv()

    def names(self):
        """получение перечня всех имен переменных"""
        self.socket.send(GET_VAR_LIST.data)
        return self.socket.recv_multipart()

    def syncro(self, i):
        """установка режима синхронизации"""
        # print("syncro ", self.addr, i)
        if i:
            self.socket.send(TO_SYNCRO.data)
        else:
            self.socket.send(FROM_SYNCRO.data)
        res = self.socket.recv()
        self.is_syncro = bool(i)

    def ovron(self, name, val):
        """Запрет записи и удержание пременной name на значении val"""
        val = str(val)
        self.socket.send_multipart([FIX_VAR.data, name.encode(self.encoding), val.encode(self.encoding)])
        if name not in self.orvon_vars: self.orvon_vars.append(name)
        return self.socket.recv()

    def ovroff(self, name):
        """Отмена запрета записи"""
        # if not name:
        #     for name_ in self.orvon_vars:
        #         self.orvoff(name_)
        self.socket.send_multipart([RELEASE_VAR.data, name.encode(self.encoding)])
        if name in self.orvon_vars:
            try:
                self.orvon_vars.remove(name)
            except ValueError:
                pass
        return self.socket.recv()

    def ovroff_all(self):
        """Отменяем все введенные запреты записи"""
        for name in self.orvon_vars:
            self.orvoff(name)


class d_group(object):
    """менеджер контекста для работы с группами переменных"""

    def __init__(self, drv, varlist):
        """
        конструктор контекста
        При конструировании запрашиваем переменные поштучно - лень было оптимизировать
        :param: drv - клиент сервера сценариев типа Tscript_drv
        :param: varlist - спиок строк - имен переменных которые должны быть включены в группу.
        """
        self.drv = drv
        self.v = dict([(i, drv[i].data) for i in varlist])

    def __enter__(self):
        """при инициализации контекста запоминаем индекс контекста"""
        self.drv.socket.send_multipart([CREATE_GROUP.data] + [i.encode("utf-8") for i in self.v.keys()])
        self._idnum = self.drv.socket.recv()
        return self

    def __exit__(self, typ, value, traceback):
        """при выходе из контекста просим модель удалить контекст"""
        self.drv.socket.send_multipart([DELETE_GROUP.data, self._idnum])
        res = self.drv.socket.recv()

    def exit(self):
        self.__exit__(None, None, None)

    def get(self):
        """получение значений всех переменных"""
        self.drv.socket.send_multipart([GET_GROUP.data, self._idnum])
        data = self.drv.socket.recv_multipart()
        for src, target in zip(data, self.v.values()):
            if target.shape is ():
                target.fill(np.frombuffer(src, dtype=target.dtype)[0])
            else:
                target[:] = np.frombuffer(src, dtype=target.dtype)

    def set(self):
        """установка в модели значений всех переменных"""
        self.drv.socket.send_multipart(
            [SET_GROUP.data, self._idnum] + [i.data for i in self.v.values()])
        res = self.drv.socket.recv()


def mk_grp(client, varlist):
    """создание и активация группы"""
    grp = d_group(client, varlist)
    grp.__enter__()
    return grp


class d_groupL(object):
    """менеджер контекста для работы с группами переменных в виде вложенных массивов или словарей 
       данные будут помещаться в значения этих структур данных
    """

    def __init__(self, drv, varshape):
        """
        конструктор контекста
        При конструировании запрашиваем переменные поштучно - лень было оптимизировать
        :param: drv - клиент сервера сценариев типа Tscript_drv
        :param: varlist - спиок строк - имен переменных которые должны быть включены в группу.
        """
        self.drv = drv

        class Tfunctor:
            def __init__(self, drv):
                self.var_list = []
                self.data_list = []
                self.drv = drv

            def __call__(self, var_name):
                d = drv[var_name].data
                self.var_list.append(var_name)
                self.data_list.append(d)
                return d

        f = Tfunctor(drv)
        self.v = mapf(varshape, f)
        self._var_list = f.var_list
        self._data_list = f.data_list

    def __enter__(self):
        """при инициализации контекста запоминаем индекс контекста"""
        self.drv.socket.send_multipart([CREATE_GROUP.data] + [i.encode("utf-8") for i in self._var_list])
        self._idnum = self.drv.socket.recv()
        return self

    def __exit__(self, typ, value, traceback):
        """при выходе из контекста просим модель удалить контекст"""
        self.drv.socket.send_multipart([DELETE_GROUP.data, self._idnum])
        res = self.drv.socket.recv()

    def exit(self):
        self.__exit__(None, None, None)

    def get(self):
        """получение значений всех переменных"""
        self.drv.socket.send_multipart([GET_GROUP.data, self._idnum])
        data = self.drv.socket.recv_multipart()
        for src, target in zip(data, self._data_list):
            if target.shape is ():
                target.fill(np.frombuffer(src, dtype=target.dtype)[0])
            else:
                target[:] = np.frombuffer(src, dtype=target.dtype)

    def set(self):
        """установка в модели значений всех переменных"""
        #        print("dts",[i for i in self._data_list])
        self.drv.socket.send_multipart(
            [SET_GROUP.data, self._idnum] + [i.data for i in self._data_list])
        res = self.drv.socket.recv()


def mk_grpL(client, varlist):
    """создание и активация группы с заданной формой данных"""
    grp = d_groupL(client, varlist)
    grp.__enter__()
    return grp


class AppendTS:
    def __init__(self, g, timeset):
        self.g = g
        self.timeset = timeset
        self.ind = 0

    def __call__(self, n):
        self.ind += n
        self.g.get()
        self.timeset["$T"].append(self.ind)
        for k, v in self.g.v.items():
            self.timeset[k].append(np.copy(v))


@contextmanager
def StepLogger(drv, varlist, name="logger"):
    with d_group(drv, varlist) as g:
        timeset = {k: [] for k in g.v.keys()}
        timeset["$T"] = []
        fl = AppendTS(g, timeset)
        drv.funs[name] = fl
        yield timeset
    for k, v in timeset.items():
        timeset[k] = np.array(v)
    del drv.funs[name]


def regProp(cls, nm, nmClass):
    countError = 0
    nmError = []
    mas = np.array(nm)
    if mas.shape == () or type(nm) == tuple:
        regProp_(cls, nm, nmClass)
    else:
        for i in mas:
            try:
                regProp_(cls, i, nmClass)
                countError += 1
                nmError.append(i)
            except:
                pass
        if (countError > 1):
            raise Exception(f"{nmError} were registered in the model. Please leave one variable")
        if (countError == 0):
            raise Exception(f"{nm} were not registered in the model.")


def regProp_(cls, nm, nmClass):
    """регистрация свойств в классе"""

    def setf(obj, val):
        obj.soc[nm] = val

    def getf(obj):
        if obj.soc[nm][...].shape == ():
            return float(obj.soc[nm][...])
        else:
            return obj.soc[nm][...]
    
    def gett(obj):
        return obj.soc[nm]

    def setfS(obj, val):
        mas = obj.soc[nm][:]
        mas[:] = '\0'
        mas[:len(val)] = [i.to_bytes(1, "big") for i in val.encode('cp1251')]
        obj.soc[nm] = mas

    def getfS(obj):
        return obj.transformName(nm)
    try:
        if (cls.lastModel.soc[nm][...].dtype == "S1"):
            prop = property(getfS, setfS)
        else:
            prop = property(getf, setf)
    except:
        if type(nm) == tuple:
            prop = property(gett, setf)
        else:
            raise Exception
    setattr(cls, nmClass, prop)