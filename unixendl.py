from pathlib import Path 
from itertools import chain

for i in chain(Path(".").glob("**/*.cpp"),Path(".").glob("**/*.h"),Path(".").glob("**/*.c"),Path(".").glob("**/*.f"),Path(".").glob("**/*.for"),Path(".").glob("**/*.f90")):
    print(i)
    with open(i,"rb") as f:
        d=f.read()
    d1 = d.replace(b"\r\n",b"\n")
    with open(i,"wb") as f:
        f.write(d1)
