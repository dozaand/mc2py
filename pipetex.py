#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import re
import codecs

def load_tex(nm):
    fullnm = os.path.abspath(nm)
    dr, fl = os.path.split(fullnm)
    res=[]
    for s in codecs.open(nm, "r",encoding="utf-8").readlines():
        v = re.search(r"^[^%]+\input{(.+)}",s)
        if v:
            new_file=os.path.join(dr,v.group(1))+".tex"
            res.extend(load_tex(new_file))
            continue
        v = re.search(r"^[^%]+\include{(.+)}",s)
        if v:
            new_file=os.path.join(dr,v.group(1))+".tex"
            res.extend(load_tex(new_file))
            continue
        res.append(s)
    return res


def correct_picts(s):
    if re.search("includegraphics",s):
        s=re.sub(r"includegraphics{([\w\.]+)}",r"includegraphics{images/\1.png}" , s)
        s=re.sub(r"includegraphics(\[.+\]){([\w\.]+)}",r"includegraphics\1{images/\2.png}" , s)
    return s

def correct_power(s):
    return re.sub(ur"\$\^", "$ ^", s)

def correct_width(s):
    return re.sub(ur"\| *p[^|]+(?=\|)", "|c", s)

eqtype = None

def correct_eq(s):
    global eqtype
    if re.search("begin{multline}", s):
        if eqtype is "multline":
            raise Exception("seq")
        eqtype = "multline"
        return re.sub("multline","equation",s)
    if re.search("end{multline}", s):
        eqtype = None
        return re.sub("multline","equation",s)
    if not eqtype is None:
        return re.sub(r"\\\\","\n\\end{equation}\n\\\\begin{equation}\n" , s)
    return s

def correct_gather(s):
    global eqtype
    if re.search("begin{gather}", s):
        if eqtype is "multline":
            raise Exception("seq")
        eqtype = "multline"
        return re.sub("gather","equation",s)
    if re.search("end{gather}", s):
        eqtype = None
        return re.sub("gather","equation",s)
    if not eqtype is None:
        s = re.sub(r"\\notag", "", s)
        return re.sub(r"\\\\","\n\\end{equation}\n\\\\begin{equation}\n" , s)
    return s

def correct_longtable(data):
    it = data.__iter__()
    for s in it:
        if re.search(r"\\begin{longtable}",s):
            ss=re.sub(r"{longtable}(\[.+\])*","{tabular}",s)
            s=it.next()
            if re.search(r"caption{",s):
                s = re.sub(ur"\\label{.+}", "", s)
                s = re.sub(ur"\\caption{((.+{.+})*.*)}",r"\1",s)
                yield u"таблица "+s
                yield ss
            else:
                yield ss
                yield s
            for si in it:
                if re.search(r"\\end{longtable}",si):
                    s=re.sub(r"{longtable}(\[.+\])*","{tabular}",si)
                    break
                yield si
        yield s


#with codecs.open("aa.tex","w",encoding="utf8") as f:
#    f.write(ss)


if __name__ == '__main__':
    data=load_tex(sys.argv[1])
    data=map(correct_picts,data)
    data=map(correct_eq,data)
    data=map(correct_gather,data)
#    data=map(correct_power,data)
    data=map(correct_width,data)
    data=list(correct_longtable(data))
    ss = "".join(data)
    sys.stdout.write(ss.encode("utf-8"))
