#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""
������� ��� ���������� � ���������� zodb
"""
import transaction
from ZODB import FileStorage, DB
from ZODB.config import storageFromURL
import time
import glob
import os
import datetime
# import __builtin__

# ��� ��� ���������� �������
from persistent import Persistent
from BTrees.OOBTree import OOBTree
from BTrees.IOBTree import IOBTree
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping

from ZODB.blob import Blob
from ZODB.blob import BlobStorage


class dbopen(object):
    u"""
    ����� ��� ���������� ����������� ����
    nm - ��� ������� ��� �������� ���� ������
    pack - ������� �������� ���� ������ ��� �������� ����������"""
    def __init__(self, nm, pack=0):
        self.nm = nm
        self.pack = pack

    def open(self):
        u"""�������� ����"""
        if self.nm[-3:] == ".fs":
            self.storage = FileStorage.FileStorage(self.nm)
        else:
            self.storage = storageFromURL(self.nm)
        self.db = DB(self.storage)
        self.conn = self.db.open()
        self.root = self.conn.root()

    def close(self):
        u"""��������� ����"""
        self.conn.close()
        if self.pack:
            self.db.pack()
        self.db.close()

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type:
            transaction.abort()
        else:
            transaction.commit()
        self.close()

    def Exit(self):
        self.__exit__(None, None, None)


class dbopenT(dbopen):
    u"""��� ������ ������� ���������� tuple (db,conn,root)"""
    def __init__(self, nm, pack=0):
        super(dbopenT, self).__init__(nm, pack)

    def __enter__(self):
        self.open()
        return (self.db, self.conn, self.root)


def ZodbObj(root, key, constr, *arg, **kvarg):
    u"""����������� ������� ������������ � ��
>>> with dbopen("a.fs") as db:
...     db.root.clear()
...     a=ZodbObj(db.root,"a",PersistentList)
...     a.extend([1,2,3,4,5,6,7])
...     transaction.commit()
...     a=None
>>> with dbopen("a.fs") as db:
...     a=ZodbObj(db.root,"a",PersistentList)
...     print a
>>> DelDb("a")
[1, 2, 3, 4, 5, 6, 7]
"""
    if key in root:
        return root[key]
    else:
        obj = constr(*arg, **kvarg)
        root[key] = obj
        return obj


def DelDb(nm):
    u"""�������� ������ ���� ������"""
    names = [nm+".fs"]+glob.glob(nm+".fs.*")
    map(os.remove, names)
