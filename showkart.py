#!/usr/bin/env python
# -*- coding: cp1251 -*-
#-------------------------------------------------------------------------------
# Name:        geom.py
# Purpose:     ����� ��� ��� ������� �������������� ������ �� �������� � ��������� ������
#
# Author:      and
#
# Created:     25.11.2010
# Copyright:   (c) and 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as pat
import math
import mc2py.geom as geom
from pylab import show
import argparse
import copy

import matplotlib.pyplot as plt
plt.rc('font', family='serif')
plt.rc('font', serif='Times New Roman')
plt.rc('font', size='12')


def ShowKart( gm, data, text=[],  title="", pltshow=0, fmt="{:.2f}", dnLim=0, upLim=0, savepath="",gray=False, color = "white"):
    u"""������ ����������� �� �������� ������"""
    texter = np.array(text)

    if not hasattr(data, "ndim"):
        data = np.array(data)
    assert(gm.ntot == len(data))
    if gm.symmetry == 6:
        r = 1.1547/2 #  2/sqrt(3)
        angle = 0
    else:
        r = 1.4/2  # 2/sqrt(3)
        angle = math.pi/4
    patches = []

    fig = plt.figure(figsize=(12, 10))
    ax = fig.add_subplot(111, aspect='equal')

    # add_axes takes [left, bottom, width, height]
    border_width = 0.05
    ax_size = [0+border_width, 0+border_width,
               1-0*border_width, 1-2*border_width]
    ax = fig.add_axes(ax_size)

    xmin = 0
    ymin = 0
    xmax = 0
    ymax = 0



    i = 0
    for y, x in gm.FIterYX():
        e = pat.RegularPolygon((x, y), gm.symmetry, radius=r, orientation=angle, )

        if len(text)!=0:
            if len(texter.shape)==1:
                try:
                    txt = fmt.format( float(texter[i]) )
                except:
                    txt =texter[i]
                plt.text(x, y, txt, color=color, horizontalalignment='center', verticalalignment='center')

            if len(texter.shape)==2:
                dy=0.2
                for ind in range( texter.shape[0]):
                    txt = texter[ind,i]
                    plt.text(x, y+dy, txt, color = color,  horizontalalignment='center', verticalalignment='center')
                    dy-=0.2

        else:
            try:
                txt = fmt.format( float(data[i]) )
                plt.text(x, y, txt, color=color, horizontalalignment='center', verticalalignment='center')
            except:
                txt = data[i]
                plt.text(x, y, txt, color=color, horizontalalignment='center', verticalalignment='center')


        patches.append(e)


        xmin = min(xmin, x)
        ymin = min(ymin, y)
        xmax = max(xmax, x)
        ymax = max(ymax, y)
        i += 1

    rect = [xmin-1, xmax+1, ymin-1, ymax+1]
    if gray:
        poly = matplotlib.collections.PatchCollection(
            patches, cmap=matplotlib.cm.Greys_r)
    else:
#        poly = matplotlib.collections.PatchCollection(
#            patches, cmap=matplotlib.cm.jet)
        poly = matplotlib.collections.PatchCollection(
            patches, cmap=matplotlib.cm.rainbow)

    ax.add_collection(poly)
    ax.axis(rect)
    poly.set_array(data)
    # ��������� �������� �������� ��� ������ �����������
    if upLim != dnLim:
        poly2 = copy.copy(poly)
        step = (upLim-dnLim)/float(len(data)-1)
        newdata = np.arange(dnLim, upLim+step, step)
        poly2.set_array(newdata)
        fig.colorbar(poly2)  # �������� �������� ������� ����� �����������
    else:
        fig.colorbar(poly)  # �������� �������� ������� ����� �����������


    plt.title(title)

    if savepath:
        plt.savefig(savepath)

    if pltshow:
        plt.show()


def kshow(data, text=[], tit="", show=0, savepath="", format="{:f}", updn=0, dnLim=0, upLim=0,gray=False, color="white"):
    l = len(data)

    if l == 163: # ����������� ���� ���� - 1000
        gm = geom.TPlane(geom.koordwwer, symmetry=6, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)
    elif l == 1884: # ����������� ���� ���� - 1000
        gm = geom.TPlane(geom.koordrbmk1884, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray,color=color)
    elif l == 2488: # ����������� ������ ����������� ���� - 1000
        gm = geom.TPlane(geom.koordrbmk2488, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)
    elif l == 331: # ����������� ��� ���� - 1000
        gm = geom.TPlane(geom.koordwwer1000_tvs, symmetry=6, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)
    elif l == 289: # ����������� ��� ��� PWR
        gm = geom.TPlane(geom.koordpwr_tvs, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray,color=color)

    elif l==12: # ����������� ������������ �������
        gm = geom.TPlane(geom.koordwwer1000_tvs_pel, updn=updn)
        data = np.arange(gm.ntot)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray,color=color)

    elif l==61: # ����������� ����
        gm = geom.TPlane(geom.koordsvbr, symmetry=6, updn=updn)
        data = np.arange(gm.ntot)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)

    elif l==66: # ����������� 1/6 �� ��� ����
        gm = geom.TPlane(geom.koordwwer1000_66, symmetry=6, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)


    elif l==397: # ����������� ����� � �����������
        gm = geom.TPlane(geom.koord_shelf_r, symmetry=6, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)

    elif l==241: # ����������� ����� � �����������
        gm = geom.TPlane(geom.koord_shelf, symmetry=6, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)


    elif l==169: # ����������� ����� �� 300 �������� ����
        gm = geom.TPlane(geom.koord_brest, symmetry=6, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)

    elif l==199: # ����������� ����� �� 300 �������� ����
        gm = geom.TPlane(geom.coord_rithm_core, symmetry=6, updn=updn)
        ShowKart(gm, data,text,  title=tit, pltshow=show,
                 savepath=savepath, fmt=format, dnLim=dnLim, upLim=upLim,gray=gray, color=color)





def test():
    """���� ������ ����������� ��� ����-1000 � ����-1000"""
    #kshow(range(163), show=1, updn=1)
    #kshow(range(1884), show=1)

    data=np.arange(61)
    kshow(data, show=1, format="{%d}",updn=1)
    a=1

    # data = np.ones(331)
    # pos =[57,70,75,102,106,111,127,142,160,165,170,188,203,219,224,228,255,260,273]
    # pos.reverse()
    # for ind,el in enumerate(data):
    #     if ind in pos:
    #         data[ind]=ind
    # kshow(data, show=1, format="{%d}",updn=1)
    #kshow(range(12), show=1, format="{%d}")

if __name__ == '__main__':
    test()
#    data = np.loadtxt(sys.argv[1])
#    with open(sys.argv[1], "rb") as f:
#        data = f.read().split()
#     data =  np.arange(17*17)
#     data2 = data.reshape(17, 17)
#     data3 = list(data2)
#     data3.reverse()
#     data4=np.array(data3)
#     data5 = data4.ravel()
#     kshow(data5, show=1, updn=0)
#     a=1


