#!/usr/bin/env python
# -*- coding: cp1251 -*-

import os
import sys
import time

import wx
import wx.gizmos as gizmos
import wx.lib.inspection
import wx.lib.mixins.treemixin
from wx.lib.mixins import listctrl

import numpy as np
from ZODB import FileStorage, DB
from persistent import Persistent
from BTrees.OOBTree import OOBTree
from BTrees.IOBTree import IOBTree
import transaction
import sys

import inspect
# import ModForm as MF

wildcard = "All files (*.*)|*.*"


_COL_NAME = 0
_COL_NAME_W = 175
_COL_VALUE = 1
_COL_VALUE_W = 280
_COL_TYPE = 2
_COL_TYPE_W = 50
_COL_UNITS = 3
_COL_UNITS_W = 50
_COL_DSC = 4
_COL_DSC_W = 180


def _mdict_(obj):
    d0 = dict(obj.__dict__)
    if hasattr(obj, "_fields_"):
        d0.update(dict(obj._fields_))
    proplist = [(name, sobj) for (name, sobj) in inspect.getmembers(
        obj.__class__, inspect.isdatadescriptor) if isinstance(sobj, property)]
    if proplist:
        d0.update(dict(proplist))
    return d0


class _TObjInfo:
    u"""���������� �� ������� (������� ���) ������ ������������ ��� ����������� ��� ����������� � ����������� ������"""
    def __init__(self, obj, name=None, units=None, dsc=None):
        self.dsc = dsc
        self.units = units
        self.obj = obj
        self.name = name
        self.size = 0
        if hasattr(obj, "__len__"):
            self.size = len(obj)
        elif hasattr(obj, "__dict__"):
            # self.size=len(obj.__dict__)
            self.size = len(_mdict_(obj))

        if hasattr(obj, "__class__"):
            self.typ = unicode(obj.__class__)
        else:
            self.typ = unicode(type(obj))

    def __repr__(self):
        return "%s=%s,%s,%s " % (self.name, self.obj, self.units, self.size)


class TEdTree(wx.lib.mixins.treemixin.VirtualTree, wx.gizmos.TreeListCtrl, listctrl.ListCtrlAutoWidthMixin):
    def __init__(self, parent, obj):
        super(TEdTree, self).__init__(parent, style=
                                      wx.TR_DEFAULT_STYLE
                                      | wx.TR_FULL_ROW_HIGHLIGHT
                                      | wx.TR_COLUMN_LINES
                                      )
        listctrl.ListCtrlAutoWidthMixin.__init__(self)
        self.panel = parent
        self.AddColumn("...")
        self.AddColumn(u"��������")
        self.AddColumn(u"���")
        self.AddColumn(u"������� ���������")
        self.AddColumn(u"��������")
        self.SetMainColumn(_COL_NAME)  # the one with the tree in it...
        self.SetColumnWidth(_COL_NAME, _COL_NAME_W)
        self.SetColumnWidth(_COL_VALUE, _COL_VALUE_W)
        self.SetColumnWidth(_COL_TYPE, _COL_TYPE_W)
        self.SetColumnWidth(_COL_UNITS, _COL_UNITS_W)
        self.SetColumnWidth(_COL_DSC, _COL_DSC_W)

        self.cache = {}
        self.root = _TObjInfo(obj)
        self.cache[()] = self.root

    def ObjInfoR(self, indexes):
        if indexes in self.cache:
            return self.cache[indexes]
        else:
            res = self.ObjInfo(indexes, self.root.obj, 0)
            self.cache[indexes] = res
            return res

    def ObjInfo(self, indexes, parent, level):
        u"""�������� ��� ������� � ��� �������� � ���� ������
indexes, ������ ���� (1,0,2,3) ���������� ��������� �� ������ �����������
parent, ������ � ������� �������� ��������� ���� ������
level ������� ����������
"""
        if len(indexes) == 0:
            return self.root
        if hasattr(parent, '__getitem__'):
            # ������������� �������
            if hasattr(parent, "keys"):
                # �����������
                nm = list(parent.keys())[indexes[level]]
                info = _TObjInfo(parent[nm], unicode(nm))
            else:
                # ������ � �������
                info = _TObjInfo(parent[indexes[
                                 level]], unicode(indexes[level]))
        elif hasattr(parent, "__dict__"):
            # ������
#            nm=parent.__dict__.keys()[indexes[level]]
            nm = list(_mdict_(parent).keys())[indexes[level]]
            eldsc = None
            elunits = None
            if hasattr(parent, "_dsc_"):
                if nm in parent._dsc_:
                    eldsc = parent._dsc_[nm]
            if hasattr(parent, "_units_"):
                if nm in parent._units_:
                    elunits = parent._units_[nm]
#            info=_TObjInfo(parent.__dict__[nm],nm,units=elunits,dsc=eldsc)
            info = _TObjInfo(getattr(parent, nm), nm, units=elunits, dsc=eldsc)
        if (len(indexes)-1) == level:
            return info
        else:
            return self.ObjInfo(indexes, info.obj, level+1)

    def OnGetItemText(self, indexes, col=0):
        info = self.ObjInfoR(indexes)

        if col == _COL_NAME:
            return info.name
        elif col == _COL_VALUE:
            return unicode(info.obj)
        elif col == _COL_TYPE:
            return info.typ
        elif col == _COL_DSC:
            return info.dsc if info.dsc else ""
        elif col == _COL_UNITS:
            return info.units if info.units else ""

    def OnGetChildrenCount(self, indexes):
#        print "call of OnGetChildrenCount "+unicode(indexes)
        info = self.ObjInfoR(indexes)
#        print "is:",info.size
        return info.size


class EditPanel(wx.Frame):
    def __init__(self, parent, obj):
# init database
        caption = u'������ ��������'

        wx.Frame.__init__(self, parent, -1, caption, size=(800, 500))

        self.tree = TEdTree(self, obj)

        menubar = wx.MenuBar()
        menu = wx.Menu()
        menu.Append(-1, "open", u"���������� �����")
        menu.Append(-1, "close", u"���������� �����")
        menubar.Append(menu, "File")
        self.SetMenuBar(menubar)
#        self.Bind(wx.EVT_POPULATE_TREE_ITEM,self.f)
#        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.tree.RefreshItems()


def ed(obj):
    u"�������������� ������������� �������"
    if "app" not in globals():
        global app
        app = wx.PySimpleApp(redirect=True)
    frame = EditPanel(None, obj)
    frame.Show(True)
    app.MainLoop()


def main(arg):
    fil, ext = os.path.splitext(arg)
    if ext == '.pkl':
        import numpy as np
        import cPickle
        with open(arg, "rb") as f:
            obj = cPickle.load(f)
        ed(obj)
    else:
        storage = FileStorage.FileStorage(arg)
        db = DB(storage)
        connection = db.open()
        root = connection.root()
        sxema = root["sxema"]
        exec(sxema)
        ed(root)
#    app.MainLoop()
        connection.close()
        db.close()

if __name__ == '__main__':
    if os.path.exists(sys.argv[1]):
        main(sys.argv[1])
