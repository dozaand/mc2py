#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
copy data to clipboard
 clip.py  aaa bbb ccc  result in "aaa bbb ccc" in clipboard
 clip.py  -i aaa bbb ccc  result in cat aaa bbb ccc in clipboard
 clip -o [con] - copy clipboard to console
 clip -o somefile.txt - copy clipboard to somefile.txt
"""

from Tkinter import Tk
import os
import argparse
import fileinput


def CopyToClipboard(txt):
    u"""����������� ������ � ��������"""
    r = Tk()
    r.withdraw()
    r.clipboard_clear()
    r.clipboard_append(txt)
    r.destroy()


def CopyFromClipboard():
    u"""����������� ������ � ��������"""
    r = Tk()
    r.withdraw()
    s = r.clipboard_get()
    r.destroy()
    return s

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="clip", description="copy data to clipboard")
    parser.add_argument(
        "--input", "-i", nargs="+", help=u"cat files to clopboard")
    parser.add_argument("--output", "-o", nargs="?",
                        const="con", help=u"copy from clipboard to file or console")
    parser.add_argument(
        "text", nargs="*", help=u"cat command line to clopboard if no args - clear clipboard")
    args = parser.parse_args()
    if args.input:
        tot = []
        for nm in args.input:
            try:
                with open(nm, "rt") as f:
                    s = f.read()
                tot.append(s)
            except:
                print "fail cat %s" % nm
        CopyToClipboard("\n".join(tot))
    elif args.output:
        if args.output == "con":
            print CopyFromClipboard()
        else:
            with open(args.output, "t") as f:
                f.write(CopyFromClipboard())
    else:
        CopyToClipboard(" ".join(args.text))
