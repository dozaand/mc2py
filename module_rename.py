#!/usr/bin/env python
# -*- coding: cp1251 -*-

"""������ � ������ ����� ������"""
import os
# from contextlib import nested
import argparse
import glob


def MultiFileReplace(filelist, pattr, res):
    for name in filelist:
        oldname = name+".old"
        os.rename(name, oldname)
        with open(oldname, "rb") as f:
            data = f.read()
        newdata = data.replace(pattr, res)
        with open(name, "wb") as f:
            f.write(newdata)


# MultiFileReplace(["bgk.dat"],"__main__","ake.usersoft.db.sxema")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="picle modeule rename util")
    parser.add_argument(
        "--input", "-i", required=True, help=u"��� ������ �� ��������������")
    parser.add_argument(
        "--output", "-o", required=True, help=u"��� ������ ����� ��������������")
    parser.add_argument(
        "--mask", "-m", help=u"����� ���������� ������ ��� ���������")
    parser.add_argument(
        "files", nargs='*', help=u"�������� ������ ��� ���������")
    args = parser.parse_args()
    flist = args.files
    if args.mask:
        flist += glob.glob(args.mask)
    MultiFileReplace(flist, args.input, args.output)
