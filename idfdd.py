#!/usr/bin/env python
# -*- coding: cp1251 -*-
import os
import sys
import glob
import string
import getopt
import re
import cPickle

from pyparsing import *
import pyparsing as pp

from ZODB import FileStorage, DB
from persistent import Persistent
from BTrees.OOBTree import OOBTree
from BTrees.IOBTree import IOBTree
import transaction

storage = FileStorage.FileStorage("mdb")
db = DB(storage)
connection = db.open()
root = connection.root()

inum = Word(nums)
ident = Word(alphas, alphanums)
LP = Suppress('(')
RP = Suppress(')')
dimdsc = ident | inum

vardsc = (LineStart()+Word("WV", exact=1)+ident+Optional(LP+delimitedList(dimdsc, ':')+RP)+ident +
          LineStart()+Word("IOSUPHKFJNQ", exact=1)("cls")+ident("name")+Optional(LP+delimitedList(dimdsk, ':')+RP)+Optional(Word(printables))+Optional(quotedString("'")))


def ScanIdf(nm):
    u"""��������� �� idf ������ � ����������"""
    with open(nm, "rt") as f:
        data = f.read().upper()
    rr = re.compile("^([IOSUPHKFJNQ]) +([A-z_][A-z0-9_]*)", re.M | re.DOTALL)
    li1 = [[i.group(2), i.group(1)] for i in rr.finditer(data)]
    return li1

nm = r"D:\distr\wwer\wwer1000\MODELS\YM\FH\YM.IDF"
res = ScanIdf(nm)


def Find(dir, mask):
    "����� ����� �� �������� �����"
    for root, dirs, files in os.walk(dir):
        for name in files:
            if glob.fnmatch.fnmatch(name, mask):
                yield os.path.abspath(os.path.join(root, name))  # �������� ������ ��� �����


class TProject(Persistent):
    def __init__(self, directory):
        pass

if not "prj" in root:
    prj = TProject(".")
    root["prj"] = prj
else:
    prj = root["prj"]


connection.close()
db.close()
