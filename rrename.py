#! /bin/env/python
# -*- coding:utf-8 -*-
u""" As unix rename but regular expression syntax"""

import argparse
import shutil,os
import re

# собственно код
def file_iter(root,recursive=False,symlinks=False):
    if recursive:
        for di, dl, fl in os.walk(root,followlinks=symlinks):
            for f in fl:
                yield os.path.join(di, f)
    else:
        di, dl, fl = os.walk(root).next()
        for f in fl:
            yield os.path.join(di, f)

# интерфейс для программного использования
def rrename(src,target,root=".", force=False, recursive=False, symlinks=False):
    ex = re.compile(src)
    ifail = 0
    for oldname in file_iter(root,recursive,symlinks):
        newname, n = ex.subn(target,oldname)
        if n:
            if force and os.path.exists(newname):
                os.remove(newname)
            try:
                os.rename(oldname,newname)
            except OSError:
                ifail+=1
                print "fail rename {0} -> {1}".format(oldname,newname)
    return ifail

# интерфейс для консоли
if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="rrename",description="rename files by regular expression replace")
    parser.add_argument("-r","--recursive",action='store_true',help=u"search files in subdirs")
    parser.add_argument("-f","--force",action='store_true',help=u"force file overwrite")
    parser.add_argument("-s","--symlinks",action='store_true',help=u"follow symlinks")
    parser.add_argument("-d","--directory",help=u"root directory")
    parser.add_argument("patterns",nargs="2",help=u"input_pattern output_pattern")
    args = parser.parse_args()
    root = args.directory
    if not root:
        root = "."
    sys.exit(rrename(args.patterns[0],args.patterns[1],root,args.force,args.recursive,args.symlinks))
