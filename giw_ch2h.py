#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
convert *.ch file from smm.exe output to *.h with static mapping of data
"""

import argparse
import re
import os
from mako.template import Template

def scan_ch(file_name):
    with open(file_name, "rt") as f_in:
        data=f_in.read()
    spw_list =[(int(i[0]),int(i[1])) for i in  re.findall("char +SPW_(\d+)\[(\d+)\]",data)]
    constlist = [(i[0],int(i[1])) for i in re.findall(r"#define +(\w+) +(\d+)\n",data)]
    varlist=[] # данных недостаточно чтобы получить полную длину объекта
    id_obj,sys_name=re.search(r"int __declspec\(dllexport\) +(v_\w+_(\w+))", data).groups()
    return dict(spw_list=spw_list, varlist=varlist, constlist=constlist,
                id_obj=id_obj, sys_name=sys_name)

ttrans={"CHARACTER1":"char",
       "LOGICAL1":"bool",
       "INTEGER1":"bool",
       "INTEGER2":"short",
       "INTEGER4":"int",
       "INTEGER8":"long long",
       "REAL4":"float",
       "REAL8":"double"
       }

def rm_range(d):
    """
    >>> rm_range("1,2,3,1:20,6,-5:5")
    '1,2,3,20,6,11'
    """
    return re.sub(r"(-?\d+):(-?\d+)",lambda x:str(int(x.group(2))-int(x.group(1))+1),d)


def fortran_type_to_c_type(rec,dkt):
    """"делаем сишный тип из фортрановского
>>> rec=('INTEGER', '4', 'SHOW_II', '(10,-2:5)','10,-2:5')
>>> dkt={"SHOW_II":{}}
>>> fortran_type_to_c_type(rec,dkt)
>>> dkt
{'SHOW_II': {'typ': 'int', 'shape': ['8', '10']}}
"""
    typ,typlen,nm,tmp,shp = rec
    if typ=='CHARACTER' and typlen != "1":
        if shp:
            shp = typlen+","+shp
        else:
            shp = typlen
        typlen = "1"
    ctyp = ttrans[typ + typlen]
    if shp:
            shape = rm_range(shp).split(",")
            shape.reverse()
            dim="[" + ("][".join(shape)) + "]"
    else:
        shape=[]
        dim=""
    dkt[nm].update({"shape":shape,"typ":ctyp,"dim":dim})


def scan_fh(file_name):
    """"сканирование fh для построения альтернативного интерфейса"""
    with open(file_name, "rt") as f_in:
        data=f_in.read()
    spw_list = [(int(i[0]),int(i[1])) for i in  re.findall("LOGICAL\*1 SPW_(\d+)\(0:(\d+)\)",data)]\
              +[(int(i[0]),int(i[1])) for i in  re.findall("CHARACTER\*1 SPW_(\d+)\(0:(\d+)\)",data)]
    constlist = [(i[0],int(i[1])) for i in re.findall(r"integer,parameter::(\w+)=(\d+)",data)]
    vardict = {i[2]:dict(x=i[2],spw=int(i[0]),addr=int(i[1])) for i in re.findall(r"equivalence\(SPW_(\d+)\((\d+)\),(\w+)\)",data)}
    vartype = re.findall("(\w+)\*(\d+) +(\w+)(\(([\d,\-:]+)\))?",data)
    id_obj,sys_name=re.search(r"\!MS\$ATTRIBUTES ALIAS:'(v_\w+_(\w+))'::", data).groups()
    var_name_list=[]
    for i in vartype:
        if not re.match("SPW_\d+",i[2]):
            var_name_list.append(i[2])
            fortran_type_to_c_type(i,vardict)

    varlist=[vardict[i] for i in var_name_list]

    return dict(spw_list=spw_list, varlist=varlist, constlist=constlist,
                id_obj=id_obj, sys_name=sys_name)


tpl_var_init = Template("""=*(${typ} (*)${dim})(SPW_${spw}+${addr})""")
tpl_var_decl = Template("""${typ} (&${x})${dim}""")

def varinit(i):
    """print var initialisation block
>>> varinit({'typ': 'int', 'addr': 3101040, 'spw': 0, 'dim': '[4][10]', 'shape': ['4', '10'], 'x': 'SHOW_II'})
'=*(int (*)[4][10])(SPW_0+3101040)'
>>> varinit({'typ': 'int', 'addr': 3101040, 'spw': 0, 'dim': '', 'shape': [], 'x': 'SHOW_II'})
'=*(int (*))(SPW_0+3101040)'
    """
    return tpl_var_init.render(**i)


def vardecl(i):
    """var declaration
>>> vardecl({'typ': 'int', 'addr': 3101040, 'spw': 0, 'dim': '[4][10]', 'shape': ['4', '10'], 'x': 'SHOW_II'})
'int (&SHOW_II)[4][10]'
>>> vardecl({'typ': 'int', 'addr': 3101040, 'spw': 0, 'dim': '', 'shape': [], 'x': 'SHOW_II'})
'int (&SHOW_II)'
"""
    return tpl_var_decl.render(**i)

tpl_h = Template("""#pragma once
extern "C"
{
%for spw_id,spw_size in spw_list:
extern char SPW_${spw_id}[${spw_size}];
%endfor
}
namespace ${sys_name}
{
%for const_name,const_val in constlist:
const int ${const_name} = ${const_val};
%endfor

%for i in varlist:
${prefix} ${vardecl(i)};
%endfor

//#include <map>
//extern std::map<string, void*> vardict;

}
extern int __declspec(dllexport) ${id_obj};
""")


tpl_cpp = Template("""#include <sstream>
#include <fstream>
#include <map>

using namespace std;

#include "${include_name}"
namespace ${sys_name}
{
%for i in varlist:
 ${vardecl(i)}${varinit(i)};
%endfor


void spw_save_${sys_name}(const char* dir_nm)
{
 stringstream ss;
 ofstream s;
 %for spw_id,spw_size in spw_list:
 ss.seekp(0);
 ss<<dir_nm<<"/spw_"<<${spw_id}<<".bin";
 s.open(ss.str().c_str(),ios::binary);
 s.write(SPW_${spw_id},${spw_size});
 s.close();
 %endfor
}

void spw_load_${sys_name}(const char* dir_nm)
{
 stringstream ss;
 ifstream s;
 %for spw_id,spw_size in spw_list:
 ss.seekp(0);
 ss<<dir_nm<<"/spw_"<<${spw_id}<<".bin";
 s.open(ss.str().c_str(),ios::binary);
 s.read(SPW_${spw_id},${spw_size});
 s.close();
 %endfor
}
map<string, void*> vardict;
int init_vardict()
{
%for i in varlist:
 vardict["${i["x"]}"] = SPW_${i["spw"]}+${i["addr"]};
%endfor
 return 0;
}
int ivardict=init_vardict();
}
int ${id_obj};
""")


tpl_spw = Template("""extern "C"
{
 %for spw_id,spw_size in spw_list:
 char SPW_${spw_id}[${spw_size}];
 %endfor
}
""")


def ch2h(file_name, file_name_h, file_name_out_cpp, file_name_out_spw):
    u"""
    convert *.ch file from smm.exe output to *.h with static mapping of data
    """
    ext=os.path.splitext(file_name)[1]
    if ext in [".fh",".FH"]:
        data = scan_fh(file_name)
    else:
        data = scan_ch(file_name)
    data["varinit"] = varinit
    data["vardecl"] = vardecl
    if file_name_out_cpp:
        data["include_name"] = os.path.split(file_name_h)[1]
        res = tpl_cpp.render(**data)
        with open(file_name_out_cpp, "wt") as f:
            f.write(res)
        data["prefix"] = "extern "
    else:
        data["prefix"] = "static "

    res = tpl_h.render(**data)
    with open(file_name_h, "wt") as f:
        f.write(res)
    if file_name_out_spw:
        res = tpl_spw.render(**data)
        with open(file_name_out_spw, "wt") as f:
            f.write(res)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="convert ch to normal h file")
    parser.add_argument("file", help=u"source ch file from smm")
    parser.add_argument("-o", "--output", help=u"output file name")
    parser.add_argument("--cpp", default="", help=u"cpp_file_name")
    parser.add_argument("--spw", default="",
                        help=u"cpp file for dummy spw arrays")
    args = parser.parse_args()
    ch2h(args.file, args.output, args.cpp, args.spw)
