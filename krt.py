#!/usr/bin/env python
# -*- coding: cp1251 -*-
#-------------------------------------------------------------------------------
# Name:        d1
# Purpose:     �������� ���������� ������ ��������
#
# Author:      and
#
# Created:     04.09.2010
# Copyright:   (c) and 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------
"""
��������� �����������
"""
import matplotlib
matplotlib.interactive(True)
matplotlib.use("WXAgg")
import matplotlib.pyplot as plt
from matplotlib.pylab import *
from matplotlib import rc

rc('font', **{'family': 'verdana'})
rc('text.latex', unicode=True)
rc('text.latex', preamble='\usepackage[utf8]{inputenc}')
rc('text.latex', preamble='\usepackage[russian]{babel}')


koordwwer = [[1, 7], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13], [0, 14], [
    1, 14], [1, 15], [2, 15], [3, 15], [4, 15], [5, 15], [6, 15], [8, 14]]
koordrbmk2488 = [
    [23, 33], [18, 38], [16, 40], [14, 42], [12, 44], [11, 45], [10, 46], [9,
                                                                           47], [8, 48], [7, 49], [6, 50], [5, 51], [4, 52], [4, 52], [3, 53], [3,
                                                                                                                                                53], [2, 54], [2, 54], [1, 55], [1, 55], [1, 55], [1, 55], [1, 55], [0,
                                                                                                                                                                                                                     56], [0, 56], [0, 56], [0, 56], [0, 56], [0, 56], [0, 56], [0, 56], [0,
                                                                                                                                                                                                                                                                                          56], [0, 56], [1, 55], [1, 55], [1, 55], [1, 55], [1, 55], [2, 54], [2,
                                                                                                                                                                                                                                                                                                                                                               54], [3, 53], [3, 53], [4, 52], [4, 52], [5, 51], [6, 50], [7, 49], [8,
                                                                                                                                                                                                                                                                                                                                                                                                                                    48], [9, 47], [10, 46], [11, 45], [12, 44], [14, 42], [16, 40], [18,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     38], [23, 33]]
koordrbmk1884 = [
    [25, 39], [22, 42], [20, 44], [18, 46], [17, 47], [16, 48], [15, 49], [14,
                                                                           50], [13, 51], [12, 52], [11, 53], [11, 53], [10, 54], [10, 54], [9,
                                                                                                                                             55], [9, 55], [9, 55], [8, 56], [8, 56], [8, 56], [8, 56], [8, 56], [8,
                                                                                                                                                                                                                  56], [8, 56], [8, 56], [8, 56], [8, 56], [8, 56], [8, 56], [8, 56], [8,
                                                                                                                                                                                                                                                                                       56], [9, 55], [9, 55], [9, 55], [10, 54], [10, 54], [11, 53], [11,
                                                                                                                                                                                                                                                                                                                                                      53], [12, 52], [13, 51], [14, 50], [15, 49], [16, 48], [17, 47], [18,
                                                                                                                                                                                                                                                                                                                                                                                                                        46], [20, 44], [22, 42], [25, 39]]


def hextrans(x, y):
    return (x - y*0.5, y*0.866025)  # sqrt(3)/2


def etrans(x, y):
    return (x, y)


def sktchow(data, koord=koordwwer, geom=6, title=""):
    verts = [(0., 0.), (1., 0.), (1., 1.), (0., 1.)]
    if geom == 6:
        trans = hextrans
        r = 1.1547/2  # 2/sqrt(3)
        angle = 0
    else:
        trans = etrans
        r = 1.4/2  # 2/sqrt(3)
        angle = 3.1418/4

    y = 0
    xkoord = []
    ykoord = []
    for [beg, end] in koord:
        y += 1
        for x in range(beg, end):
            xt, yt = trans(x, y)
            xkoord.append(xt)
            ykoord.append(yt)
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    matplotlib.pyplot.scatter(
        xkoord, ykoord, s=360., c=data, marker='h', cmap=matplotlib.cm.jet)
    matplotlib.pyplot.colorbar()
    matplotlib.pyplot.title(title)


def main():
    sktchow(range(163), title="asd")
    savefig("out.pdf")
    pass

if __name__ == '__main__':
    main()
