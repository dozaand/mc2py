#/bin/env/python
# -*- coding utf-8 -*-
import yaml
import numpy as np


def binary_yaml_load(dsk_file, data_file=None):
    u"""load data from binary file by description in yaml file
dsk_file:
---
a.dat # name of input binary file (optional if data_file is specified)
---
[yampr,f,3] # var description name type len
---
[pupr,i,2]
---
[v,f,1]

for i in binary_yaml_load("tmp.dat"):
    print i
"""
    with open(dsk_file, "r") as fdsk:
        itr = yaml.load_all(fdsk)
        if not data_file:
            data_file = itr.next()
        with open(data_file, "rb") as f:
            for dsk in itr:
                nm, typ, arrlen = dsk
                yield (nm, np.fromfile(f, dtype=typ, count=arrlen))
