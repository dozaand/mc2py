#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
������� ��� ������ � ctypes
- ��������� �������� �� ������ �������� ����������
- ��������� ������ ����� ��� ������ � ������ ����������
- ����� �������� �������� ���������� pack
"""
from ctypes import *
import codecs


def dscCT(vars, pack=None):
    u"""��������� ����������� � ������ �������� ����������
������ ����:
[
 [Wnom,(c_int*8)*2,"description","units",None],
 [Wnom1,(c_int*8)*2,"description","units",[1,2,3]]
]"""
    def mf(cls, vars=vars, pack=pack):
        clsdict = {}
        if pack:
            clsdict["_pack_"] = pack
        clsdict["_fields_"] = [(i[0], i[1]) for i in vars]
        clsdict["_dsc_"] = dict([(i[0], i[2]) for i in vars])
        clsdict["_units_"] = dict([(i[0], i[3]) for i in vars])
        clsdict["_defaultvals_"] = dict([(i[0], i[4]) for i in vars])

        def defaults(self):
            for nm, val in self._defaultvals_.iteritems():
                if val != None:
                    if hasattr(val, "__len__"):
                        l = len(val)
                        obj = getattr(self, nm)
                        if hasattr(obj, "__setslice__"):
                            obj.__setslice__(0, l, val)
                        else:
                            obj.__init__(*val)
                    else:
                        setattr(self, nm, val)
        clsdict["SetDefaults"] = defaults
        clsdict.update(cls.__dict__)
        newcls = type(cls.__name__, (Structure,), clsdict)
        return newcls
    return mf

# _TAkeData=[["VersionNumber",c_int,u"����� ������ ����",u"",1],
##["Wnom",c_float,u"����������� �������� ��",u"%",3e9],
##["kOffset",c_float,u"����������� ������������� ������  ���������� �������� -0.05 +0.05",u"�.�.",0.],
##["kI",c_float*3,u"����������� ������������� ����� �� �� ����� ���������",u"",[1.,1.,1.]]
##]
##
##@dskCT(_TAkeData)
# class TAkeData:
##    """������ ��� ���"""
##    def __init__(self):
##        pass


def _IsStruct(typ):
    u"""��������� ��� ctype ������ ���� ���������"""
    return typ.__class__.__name__ == "StructType"


def _IsArray(typ):
    u"""��������� ��� ctype ������ ���� ���������"""
    return typ.__class__.__name__ == "ArrayType"


def _CTypeName(typ):
    u"""���������� �� ��� ���� ������ ��� �������� ���� ������� � ����� �� ����������"""
    dd = {c_int: "int", c_byte: "unsigned char", c_char: "char", c_float:
          "float", c_double: "double", c_long: "long", c_longlong: "long long"}
    if typ in dd:
        return (dd[typ], "")
    if hasattr(typ, "__class__"):
        if typ.__class__.__name__ == "ArrayType":
            l = typ._length_
            subtyp = typ._type_
            stpre, stpast = _CTypeName(subtyp)
            return (stpre, "[%s]" % l+stpast)
        elif typ.__class__.__name__ == "StructType":
            return (typ.__name__, "")
    else:
        raise Exception("unclassifiable type")


def _CtypeDeclVar(v):
    u"""�������������� ���������� ��������� ����"""
    (nm, typ) = v
    pre, post = _CTypeName(typ)
    return "{pre} {nm}{post};".format(pre=pre, nm=nm, post=post)


class TClassDecls:
    def __init__(self):
        self.flush()

    def Add(self, obj):
        u"""�������������� ���������� ��������� ����"""
        if obj != None:
            typ = type(obj)
        if not issubclass(typ, Structure):
            print("strange")
        if typ in self.classdsc:
            return
        vardecl = ""
        for nm, styp in typ._fields_:
            if _IsStruct(styp):
                self.Add(getattr(obj, nm))
            vardecl += _CtypeDeclVar((nm, styp))+"\n"
        self.classorder.append(typ)
        pre, post = _CTypeName(typ)
        dsc = """class %(pre)s
{
public:
 %(pre)s();
%(vardecl)s};
""" % locals()
        self.classdsc[typ] = dsc
      # make constructor
        if not typ in self.clsconstructor:
            classnm = pre
            varinitlist = ""
            for nm, styp in typ._fields_:
                if hasattr(obj, "_dsc_"):
                    varinitlist += ("//"+obj._dsc_[nm]+"\n")
                varinitlist += _InitVarText(nm, getattr(obj, nm))+"\n"
            constructor = """\
%(classnm)s::%(classnm)s()
{
  %(varinitlist)s}
""" % locals()
            self.clsconstructor[typ] = constructor

    def flush(self):
        self.classorder = []  # �������� �����
        self.classdsc = {}  # ����������� - ���-> ������ ���������� ������
        self.clsconstructor = {}  # ����������� - ���-> ������ �����������

    def TxtDeclarations(self):
        return "\n".join([self.classdsc[i] for i in self.classorder])

    def TxtConstructors(self):
        return "\n".join([self.clsconstructor[i] for i in self.classorder])

    def WriteCode(self, filename):
        u"""��������� ���� ��� ���� ������� (������ filename.h � filename.cpp)"""
        txt = self.TxtDeclarations()
        hdrtxt = u"""\
#ifndef {filename}_h
#define {filename}_h
{txt}
#endif
""".format(**locals())
        with codecs.open(filename+".h", "wt", encoding='cp1251') as f:
            f.write(hdrtxt)
        txt = self.TxtConstructors()
        cpptxt = u"""\
#include "{filename}.h"
{txt}
""".format(**locals())
        with codecs.open(filename+".cpp", "wt", encoding='cp1251') as f:
            f.write(cpptxt)


def _InitVarText(nm, obj):
    u"""����� ������������� ���������� ���� ctype
>>> a=(c_int*3)()
>>> a[:]=[1,23,3]
>>> print(_InitVarText("a",a))
{
 long tmp[3]={1,23,3};
 for(int i=0;i<3;++i)
 {
a[i]=tmp[i];
 }
}
>>> a=c_double(3.45)
>>> print _InitVarText("a",a)
a=3.45;
"""
    typ = type(obj)
    if _IsArray(typ):
        subtyp = typ._type_
        if not (_IsArray(subtyp) or _IsStruct(subtyp)):
            l = typ._length_
            pre, post = _CTypeName(typ)
            data = ",".join(map(str, obj[:]))
        return """\
{
 %(pre)s tmp%(post)s={%(data)s};
 for(int i=0;i<%(l)s;++i)
 {
   %(nm)s[i]=tmp[i];
 }
}""" % locals()
    elif _IsStruct(typ):
        return "// class self init %s,%s" % (nm, typ)
    elif hasattr(obj, "value"):
        val = obj.value
        return "%(nm)s=%(val)s;" % locals()
    else:
        return "%(nm)s=%(obj)s;" % locals()


def WriteCode(obj, filename=None):
    u"""�������� ��� ��� ������� ������� ���� ctypes"""
    if not filename:
        filename = obj.__class__.__name__
    cd = TClassDecls()
    cd.Add(obj)
    cd.WriteCode(filename)


def _ct_set_field(src, dest, fieldnm):
    u"""������� ��������� ����"""
    sdest = getattr(dest, fieldnm)
    if not (hasattr(sdest, "_length_") or hasattr(sdest, "_fields_")):
        setattr(dest, fieldnm, src)
    else:
        ct_copy(src, sdest)


def ct_copy(src, dest):
    u"""�������� ���������� �� src � dest ��� ���� dest ctype ���"""
    if hasattr(dest, "_fields_"):
        for nm, typ in dest._fields_:
            _ct_set_field(getattr(src, nm), dest, nm)
    elif hasattr(dest, "_length_"):
        sybtyp = type(dest)._type_
        if _IsStruct(sybtyp) or _IsArray(sybtyp):
            for s, d in zip(src, dest):
                ct_copy(s, d)
        else:
            dest[:] = src
    else:
        dest = src
