#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
apply mako render to data
"""

import mako
from mako.template import Template
from mako.lookup import TemplateLookup
import codecs
import argparse
import yaml
import os

def gen_file_names(args):
    u"""
    predefined keys:
    t - body of tempplate file
    T - path and body of template
    it - index of template
    d - body of data file i.e. "data1" for "/home/bin/data1.dat"
    D - path and body of data file i.e. "/home/bin/data1" for "/home/bin/data1.dat"
    id - index of data file i.e. "1" for "/home/bin/data1.dat"
    """
    Ntpl=len(args.templates)
    Ndata=len(args.data)
    Ntot = Ndata * Ntpl
    if args.output and len(args.output)==Ntot:
        return args.output
    if args.output:
        tpl_string=args.output[0]
    else:
        tpl_string="${t}_${d}.out"
        if Ntpl == 1:
            tpl_string="${d}.out"
        if Ndata==1:
            tpl_string="${t}.out"

    tpl=Template(tpl_string,strict_undefined=0)
    res=[]
    for it,t_ in enumerate(args.templates):
        T,tmp=os.path.splitext(t_)
        tmp,t=os.path.split(T)
        for idat,d_ in enumerate(args.data):
            D,tmp=os.path.splitext(d_)
            tmp,d=os.path.split(D)
            x=tpl.render(D=D,d=d,T=T,t=t,it=it,id=idat)
            res.append(x)
    return res

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="create series of data")
    parser.add_argument("-l", "--lookup", nargs="*",  default =["."],help=u"source templates")
    parser.add_argument("-t", "--templates", nargs="*", help=u"source templates")
    parser.add_argument("-o", "--output", nargs="*",help=u"""output file_names or template for name possible args
    t - body of tempplate file
    T - path and body of template
    it - index of template
    d - body of data file i.e. "data1" for "/home/bin/data1.dat"
    D - path and body of data file i.e. "/home/bin/data1" for "/home/bin/data1.dat"
    id - index of data file i.e. "1" for "/home/bin/data1.dat"
    """)
    parser.add_argument("-d","--data", nargs="*", help=u"source data files")
    args = parser.parse_args()

    out_names = gen_file_names(args)
    mylookup = TemplateLookup(directories=args.lookup, strict_undefined=True)
    i_file=0

# load all data
    data=[]
    for i in args.data:
        with codecs.open(i,"r",encoding="utf-8") as f:
            data.append(yaml.load(f))
# subst data to templates
    for t in args.templates:
        tpl = Template(filename=t, module_directory='.', lookup=mylookup, strict_undefined=True)
        for i in data:
            with codecs.open(out_names[i_file],"w",encoding="utf-8") as f:
                f.write(tpl.render(**i))
            i_file+=1
