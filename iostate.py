﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
Функции для чтения и записи состояний получаемых при работе с моделью на Южноукраинском проекте.
Состояния получаются в виде словаря "имя переменной"-"значение" Тип значения numpy.array
"""
import os
import numpy as np
from mc2py.evaldict import BinRead, BinWrite
import cPickle

typecode = map(np.dtype, ['c', 'b', 'h', 'i', 'f', 'd'])
type2index = dict([(t, i) for i, t in enumerate(typecode)])
typelen = [1, 1, 2, 4, 4, 8]


def ReadStateFile(filename):
    with open(filename, "rb") as f:
        try:
#            count,=BinRead(f,"i")
            res = {}
#            for i in xrange(count):
            while True:
                nl, typ = BinRead(f, "BB")
                varnm = f.read(nl)
                length, = BinRead(f, "i")
                data = np.fromfile(f, dtype=typecode[
                                   typ], count=length/typelen[typ])
                res[varnm] = data
        except:
            pass
    return res


def WriteStateFile(data, filename):
    u"""
    write dict data
    all values in data numpy arrays
    """
    with open(filename, "wb") as f:
#        BinWrite(f,"i",len(data))
        for varnm, v in data.iteritems():
            itype = type2index[v.dtype]
            BinWrite(f, "BB", len(varnm), itype)
            f.write(varnm)
            BinWrite(f, "i", typelen[itype]*len(v))
            v.tofile(f)


# nm=r""
# nm=r"C:\#SU\ukr\data\main20_.sta"
def CompareStates(st1, st2, patt="", cmpf=lambda a, b: np.linalg.norm(a-b)/(np.linalg.norm(a)+1e-5) < 1e-1):
    """
    Сравнение состояний модели st1 и st2 по всем переменным кроме тех, имена
    которых удовлетворяют регулярному выражению patt. Переменные считаются одинаковыми,
    если примененная к ним функция cmpf возвращает 0
    """
    nmdiff = lambda nm: re.match(patt, nm, re.I)
    res = {"cmnm": set(),
           "left_only": set(),
           "right_only": set(),
           "diff": {}}
    res["cmnm"] = set(st1.keys()).intersection(st2.keys())
    res["left_only"] = set(st1.keys()).difference(res["cmnm"])
    res["right_only"] = set(st2.keys()).difference(res["cmnm"])
    for nm in res["cmnm"]:
        try:
            if(not cmpf(st1[nm], st2[nm]) and not nmdiff(nm)):
                res["diff"][nm] = (st1[nm]-st2[nm])
        except:
            print nm
            pass
    return res
