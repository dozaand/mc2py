#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
циклическая запись в hdf5
"""

import numpy as np
import h5py
import os


class Tcbh5:
    def __init__(self,f5,name,size,dat,dtype=None):
        u"""
         Циклический архив
        \param
        \param f5  указатель на файл с данными
        \param name имя последовательности внутри файла h5  в которую будут сохраняться данные
        \param size максимальный размер сохраняемой последовательности
        \param dat пример сохраняемых данных
        \param ddtype тип данных (вводится при желании изменить тип данных)

        """
        try:
            shape = (size,) + dat.shape
            if dtype==None:
                dtype = dat.dtype

        except AttributeError:
            shape = (size,)
            if dtype==None:
                dtype = type(dat)
                if "str" in str(dtype):
                    dtype = "S%s"%len(dat)

        if name in f5:
            self.data=f5[name]
            if not shape is None:
                assert(shape==self.data.shape)
            if not dtype is None:
                assert(dtype==self.data.dtype)
        else:
            assert(not shape is None)
            assert(not dtype is None)
            self.data = f5.create_dataset(name, dtype=dtype, shape=shape)
            self.data.attrs["pos"]=0 # позиция с которой надо писать последующие данные
            self.data.attrs["count"]=0 # полное количество добавленных элементов
    def append(self,d):
        pos=self.data.attrs["pos"]
        count=self.data.attrs["count"]
        self.data[pos,...]=d
        pos+=1
        count+=1
        if pos>=self.data.shape[0]:
            pos=0
        self.data.attrs["pos"] = pos
        self.data.attrs["count"] = count
    def _rindex(self,i):
        pos=self.data.attrs["pos"]
        count=self.data.attrs["count"]
        shape = self.data.shape
        if count<shape[0]:
            j=count-i-1
            if j<0:
                raise IndexError("aaa")
            return j
        else:
            j=pos-i-1
            if j<0:
                j+=shape[0]
            if j<0:
                raise IndexError("aaa")
            else:
                return j

    def __getitem__(self,i):
        return self.data[self._rindex(i)]
    def __setitem__(self,i,v):
        self.data[self._rindex(i),...]=v
        return self.data[self._rindex(i),...]



def test_Tcbh5():
    import ctypes
    a = np.zeros(19)
    #a = 1
    with h5py.File(r"D:\Projects\aa.h5") as f5:
        ba = Tcbh5(f5,"name",20,a,ddtype = ctypes.c_float)
        j=1
        for i in range(6):
            a[0] = j
            #a = j
            ba.append(a)
            j+=1
        res =  ba.data[:,0]
        #res =  ba.data[:]
        #assert(np.all(np.arange(1,len(res)+1,dtype='d') == res))
        for i in range(10):
            a[0] = j
            #a = j
            ba.append(a)
            j+=1
        res =  ba.data[:,0]
        #res =  ba.data[:]
        #assert(np.all(np.arange(1,len(res)+1,dtype='d') == res))

        for i in range(20):
            a[0] = j
            #a = j
            j+=1
            ba.append(a)
        res =  ba.data[:,0]
        #res =  ba.data[:]
        ref=np.arange(26,11,-1,dtype='d')
        res=np.array([res[ba.reverce_index(i)] for i in range(20)])
        #assert(np.all(ref==res))
    os.remove("aa.h5")

##if __name__ == '__main__':
##    import ctypes
##    with h5py.File(r"D:\Projects\aa.h5") as f5:
##        #a=np.array("1264234")
##        a="123"
##        ba = Tcbh5(f5,"name",20,a)
##        ba.append(a)
##        aaa=1


