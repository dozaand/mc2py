#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""������ namelist �� fortran
� ��������� ������ ����������� � �����
��� ���� �������: ������ ��������� ������ ��� ������������ � ��������� ����� �������
"""
from pyparsing import *
import numpy as np

__all__ = ["ReadNamelists", "ReadFileNamelists"]
# ParserElement.DEFAULT_WHITE_CHARS=""


def pp(t):
    return int(t[0])


def Rep(t):
    u"""��������� ����������"""
    if len(t) == 3:
        return [t[-1]]*t[0]
    else:
        return t

# base rules
ident = Word(alphas, alphanums+'_').setParseAction(lambda t: t[0].upper())
inum = Word(nums)
inum.setParseAction(pp)
neg = Optional(Word('+-', exact=1))
iVnum = neg+inum
iVnum.setParseAction(pp)
fracp = Literal('.')+Optional(inum)
exppart = Word('EeDd', exact=1)+Optional(Word('+-', exact=1))+inum
fnum = Combine(Optional(Literal('-'))+((
    inum+Optional(fracp)+exppart) | (inum+fracp)))
fnum.setParseAction(lambda x: float(x[0]))
T = (CaselessLiteral(".true.") | CaselessLiteral(".t.")
     | CaselessLiteral("t")).setParseAction(lambda t: True)
F = (CaselessLiteral(".false.") | CaselessLiteral(".f.")
     | CaselessLiteral("f")).setParseAction(lambda t: False)
lit = QuotedString("'")

# namelist rules
begnml = Suppress("&")+ident("namelistname")
endnml = Suppress(CaselessLiteral("&end") | '/')
LP = Suppress('(')
RP = Suppress(')')
lval = ident("keyname")+Optional(LP+iVnum("keypos")+RP)
item = fnum | iVnum | lit | T | F
itemr = (Optional(iVnum+Literal('*'))+item).setParseAction(Rep)
itemrlist = (itemr+ZeroOrMore(Optional(Suppress(
    ','))+itemr))  # .setParseAction(lambda x:x[0])
eq = Group(lval("keyid")+Suppress(
    '=')+itemrlist("val")+Optional(Suppress(",")))
nml = (begnml+OneOrMore(eq)("eqlist")+endnml).ignore(Regex("#.*"))
namelistbody = (OneOrMore(eq)("eqlist")).ignore(Regex("#.*"))


def MakeDatList(dat, as_nparray=0):
    u"""�� ����� ��������� �������� ���� namelist - �� ������ ��������"""
    nm2len = {}
    for i in dat.eqlist:
        l = len(i.val[:])
        if i.keyid.keypos:
            l += i.keyid.keypos-1
        maxl = max(nm2len.get(i.keyid.keyname, 0), l)
        nm2len[i.keyid.keyname] = maxl
    datlist = dict([[nm, [0]*nm2len[nm]] for nm in nm2len.keys()])
    for i in dat.eqlist:
        l = 0
        siz = len(i.val[:])
        if i.keyid.keypos:
            l += i.keyid.keypos-1
        datlist[i.keyid.keyname][l:l+siz] = i.val[:]
    for key, val in datlist.iteritems():
        datlist[key] = val[0] if len(val) == 1 else (
            np.array(val) if as_nparray else val)
    return datlist


def ReadNamelists(txt, asdict=True, as_nparray=0):
    u"""������������� ����� ��� �������
���->namelist
namelist - ���� �������
���-> ��������
����� ����� ���� � ������� (����� ��������� ������ ������������� namelist)
"""
    if asdict:
        namelists = {}
    else:
        namelists = []

    data = [i[0] for i in nml.scanString(txt)]
    dat = data[0]
    for dat in data:
        nm2len = {}
        datlist = MakeDatList(dat, as_nparray)
        if asdict:
            namelists[dat.namelistname] = datlist
        else:
            namelists.append([dat.namelistname, datlist])

    return namelists


def ReadFileNamelists(filename, asdict=True, as_nparray=0):
    u"""������ ������ nasmelist �� �����"""
    with open(filename, "r") as f:
        txt = f.read()
    return ReadNamelists(txt, asdict, as_nparray)


def ReadNameListBody(fil, as_nparray=0):
    u"""������ ��� �� ��� ��������� &beg
    &end"""
    with open(fil, "rt") as f:
        strdata = f.read()
    dat = namelistbody.parseString(strdata)
    return MakeDatList(dat, as_nparray)

if __name__ == '__main__':
    u"""������������ namelist � pickle"""
    import sys
    import cPickle
    for nm in sys.argv[1:]:
        res = ReadFileNamelists(nm)
        with open(nm+".pkl", "wb") as f:
            cPickle.dump(res, f, 2)
