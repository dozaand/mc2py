#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import re,sys
from itertools import izip

_floatnum=re.compile(r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?")
