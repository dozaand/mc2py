#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
import os
import sys
import numpy as np
import matplotlib
import pickle
import codecs
import math
import yaml
from fold import conv_val,fmap
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
import matplotlib.patches as pat
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar

import geom


def Msg(parent, msg):
    dlg = wx.MessageDialog(parent, msg,
                           'Сообщение',
                           wx.OK | wx.ICON_INFORMATION
                           )
    dlg.ShowModal()
    dlg.Destroy()


def evex(st):
    u"""пытаемся преобразовать выражение в число не получается - оставляем строку"""
    try:
        return eval(st)
    except:
        return st


def GetSelString(lst):
    u"""из списка берем выбранную строку"""
    return lst.GetString(lst.GetSelection())


class KartEdFrame(wx.Frame):
    u""" The main frame of the application"""
    title = u'Редактор картограмм'


    def __init__(self, gm, keys, data):
        wx.Frame.__init__(self, None, -1, self.title)
        self.geom = gm
        self.keys = keys
        self.data = data
        n = gm.ntot
        for k in keys:
            if k not in data:
                data[k] = ["" for i in range(n)]
            else:
                assert(len(data[k]) == n)
        self.replot = 0

        self.CreateMenu()
        self.statusbar = self.CreateStatusBar()
        self.CreateMainPanel()
        v = self.kartId

#        self.textbox.SetValue(' '.join(map(str, self.data)))
#        self.replot=1
#        self.draw_figure()

    def getKartId(self):
        u"получение индекса текущей картограммы"
        res = self.list_box_kart.GetSelection()
        if res < 0:
            self.setKartId(0)
            return 0
        else:
            return res

    def setKartId(self, value):
        u"установка текущей картограммы"
#        if self.list_box_kart.GetSelection()!=value:
        self.list_box_kart.SetSelection(value)
        self.UpdateValueList()
        self.draw_figure()
    kartId = property(getKartId, setKartId, u"идентификатор картограммы")

    def UpdateValueList(self, pos=0):
        self.list_box_kart_items.Clear()
        self.currKart = GetSelString(self.list_box_kart)
        if self.currKart:
            li = list(map(str, self.keys[self.currKart]))
            self.list_box_kart_items.InsertItems(li, 0)
            self.valId = 0

    def getKart(self):
        u"получение имени текущей картограммы"
        res = self.kartId
        return self.list_box_kart.GetString(res)

    def setKart(self, value):
        u"установка текущей картограммы по заданному имени"
        try:
            if value in self.keys.keys():
                itemindex = self.keys.keys().index(value)
                self.kartId = itemindex
            else:
                val = ["" for i in range(self.geom.ntot)]
                self.data[value] = val
                self.keys[value] = [""]
                self.UpdateKartList(value)
        except:
            Msg(self, u"Ошибка установки картограммы для картограммы " + value)
    kart = property(getKart, setKart, u"имя картограмы картограммы")

    def getKartIdName(self):
        u"получение индекса текущей картограммы"
        return self.list_box_kart.GetString(self.list_box_kart.GetSelection())

    def getValId(self):
        u"получение индекса текущего свойства"
        res = self.list_box_kart_items.GetSelection()
        if res < 0:
            self.setValId(0)
            return 0
        else:
            return res

    def setValId(self, value):
        u"установка индекса текущего свойства"
#        if self.list_box_kart_items.GetSelection()!=value:
        self.list_box_kart_items.SetSelection(value)
        self.clonedata = self.list_box_kart_items.GetString(value)
    valId = property(getValId, setValId, u"установка текущего свойства")

    def UpdateKartList(self, val=None):
        u"""приводим список картограмм в соответствие с внутренними данными"""
        self.list_box_kart.Clear()
        li = self.keys.keys()
        li.sort()
        self.list_box_kart.InsertItems(li, 0)
        if val:
            pos = li.index(val)
        else:
            pos = 0
        self.kartId = pos

    def CreateMenu(self):
        self.menubar = wx.MenuBar()

        menu_file = wx.Menu()

        m_expt = menu_file.Append(
            -1, "Save &All\tCtrl-A", u"сохранить все кртограммы")
        self.Bind(wx.EVT_MENU, self.OnSaveAll, m_expt)

        m_expt = menu_file.Append(
            -1, "&Load All\tCtrl-L", u"загрузить картограммы")
        self.Bind(wx.EVT_MENU, self.OnLoadAll, m_expt)

        m_expt = menu_file.Append(
            -1, "Save &Plot\tCtrl-P", u"сохранить в виде картинки")
        self.Bind(wx.EVT_MENU, self.OnSavePlot, m_expt)

        menu_file.AppendSeparator()

        m_expt = menu_file.Append(
            -1, "&Export \tCtrl-E", u"Экспорт текущей картограммы")
        self.Bind(wx.EVT_MENU, self.OnExport, m_expt)

        m_expt = menu_file.Append(
            -1, "Import\tCtrl-I", u"Импорт текущей картограммы")
        self.Bind(wx.EVT_MENU, self.OnImport, m_expt)

        menu_file.AppendSeparator()

        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.OnExit, m_exit)

        menu_edit = wx.Menu()
        m_expt = menu_edit.Append(
            -1, "Insert Kart", u"вставить новую картограмму")
        self.Bind(wx.EVT_MENU, self.OnInsertKart, m_expt)

        m_expt = menu_edit.Append(-1, "Delete Kart", u"удалить картограмму")
        self.Bind(wx.EVT_MENU, self.OnDeleteKart, m_expt)

        m_expt = menu_edit.Append(
            -1, "Rename Kart", u"переименовать картограмму")
        self.Bind(wx.EVT_MENU, self.OnRenameKart, m_expt)

        menu_edit.AppendSeparator()

        m_expt = menu_edit.Append(
            -1, "Insert val", u"вставить элемент картограммы")
        self.Bind(wx.EVT_MENU, self.OnInsertVal, m_expt)

        m_expt = menu_edit.Append(
            -1, "Delete val", u"удалить элемент картограммы")
        self.Bind(wx.EVT_MENU, self.OnDeleteVal, m_expt)

        m_expt = menu_edit.Append(
            -1, "Rename val", u"переименовать элемент картограммы")
        self.Bind(wx.EVT_MENU, self.OnRenameVal, m_expt)

        menu_help = wx.Menu()
        m_about = menu_help.Append(-1, "&About\tF1", "About the demo")
        self.Bind(wx.EVT_MENU, self.OnAbout, m_about)

        self.menubar.Append(menu_file, "&File")
        self.menubar.Append(menu_edit, "&Edit")
        self.menubar.Append(menu_help, "&Help")

        self.SetMenuBar(self.menubar)

    def OnSaveAll(self, event):
        u"""сохраняем все"""
        file_choices = "KRT (*.krt)|*.*"

        dlg = wx.FileDialog(
            self,
            message="Сохраняем все картограммы как ...",
            defaultDir=os.getcwd(),
            defaultFile="a.krt",
            wildcard=file_choices,
            style=wx.SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            # with open(path, "wb") as f:
            #     pickle.dump((self.geom, self.keys, self.data), f, 2)
            data_to_save=fmap({
            "geom":{
                "bounds":[[int(i[0]),int(i[1])] for i in self.geom.bounds],
                "symmetry":self.geom.symmetry,
                "y0":self.geom.y0},
            "keys":self.keys,
            "kart":self.data
            },conv_val,conv_val)
            with codecs.open(path,"w",encoding="utf-8") as f1:
                yaml.dump(data_to_save,f1,allow_unicode=1)
                self.flash_status_message("Saved to %s" % path)

    def OnLoadAll(self, event):
        u"""загружаем все"""
        file_choices = "KRT (*.krt)|*.*"

        dlg = wx.FileDialog(
            self,
            message="Сохраняем все картограммы как ...",
            defaultDir=os.getcwd(),
            defaultFile="a.krt",
            wildcard=file_choices,
            style=wx.OPEN)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
#            with open(path, "rb") as f:
#                (self.geom, self.keys, self.data) = pickle.load(f)
#                self.kartId = 0
#                self.flash_status_message("Load from  %s" % path)

            with codecs.open(path, "r",encoding="utf-8") as f:
                data = yaml.load(f)
                #(self.geom, self.keys, self.data) = pickle.load(f)
            g = data["geom"]
            self.geom = geom.TPlane(g["bounds"],g["y0"],g["symmetry"])
            self.keys = data["keys"]
            self.data = data["kart"]
            self.UpdateKartList()
            self.kartId = 0
            self.flash_status_message("Load from  %s" % path)

    def OnSavePlot(self, event):
        file_choices = "PNG (*.png)|*.png"

        dlg = wx.FileDialog(
            self,
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="plot.png",
            wildcard=file_choices,
            style=wx.SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)

    def OnExport(self, event):
        u"""сохраняем текущую картограмму"""
        file_choices = "*.* (*.krt)|*.*"

        dlg = wx.FileDialog(
            self,
            message="Сохраняем текущую картограмму как ...",
            defaultDir=os.getcwd(),
            defaultFile="a.dat",
            wildcard=file_choices,
            style=wx.SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            with open(path, "wt") as f:
                dat = self.data[self.getKartIdName()]
                for e in dat:
                    f.write(str(e)+"\n")
                self.flash_status_message("Saved to %s" % path)

    def OnImport(self, event):
        u"""грузим текущую картограмму"""
        file_choices = "*.* (*.krt)|*.*"

        dlg = wx.FileDialog(
            self,
            message="Загружаем текущую картограмму из ...",
            defaultDir=os.getcwd(),
            defaultFile="a.dat",
            wildcard=file_choices,
            style=wx.OPEN)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            with open(path, "rt") as f:
                res = map(evex, f.read().split())
                nm = self.getKartIdName()
                kkeys = list(set(self.keys[nm]+map(str, res)))
                kkeys.sort()
                self.keys[nm] = kkeys
                if len(res) == len(self.data[nm]):
                    self.data[nm] = res
                    self.flash_status_message("Load from  %s" % path)
                    self.kartId = self.kartId

    def OnExit(self, event):
        self.Destroy()

    def OnAbout(self, event):
        msg = u""" редактор картограмм предназначен для редактирования дискретных данных"""
        dlg = wx.MessageDialog(self, msg, "About", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()

    def CreateMainPanel(self):
        u""" Создание главной панели"""
        self.panel = wx.Panel(self)
        panel = self.panel

        # Create the mpl Figure and FigCanvas objects.
        # 5x4 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig = Figure((5.0, 4.0), dpi=self.dpi)
        self.canvas = FigCanvas(panel, -1, self.fig)

        self.toolbar = NavigationToolbar(self.canvas)

        self.axes = self.fig.add_subplot(111)
        self.canvas.mpl_connect('pick_event', self.on_pick)

        choices = list(self.keys.keys())
        choices.sort()

        self.list_box_kart = wx.CheckListBox(panel, -1, choices=choices)
        self.Bind(wx.EVT_CHECKLISTBOX,
                  self.ChangeKartList, self.list_box_kart)
        self.Bind(wx.EVT_LISTBOX, self.ChangeKart, self.list_box_kart)

        choices = list(self.keys[choices[0]])
        choices.sort()
        self.list_box_kart_items = wx.ListBox(panel, -1, choices=choices)
        self.Bind(
            wx.EVT_LISTBOX, self.ChangeCurrFiller, self.list_box_kart_items)

        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)

        self.vbox1 = wx.BoxSizer(wx.VERTICAL)

        self.vbox1.Add(wx.StaticText(
            panel, -1, u"картограмма\n"), 0, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox1.Add(self.list_box_kart, 1, wx.EXPAND)
        self.vbox1.Add(wx.StaticText(
            panel, -1, u"данные\n"), 0, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox1.Add(self.list_box_kart_items, 1, wx.EXPAND)

        self.hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox.Add(self.vbox, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.hbox.Add(self.vbox1, 0, wx.EXPAND)

        self.panel.SetSizer(self.hbox)
        self.hbox.Fit(self)
        self.kartId = 0
#        self.setKart("aaa")
#        self.OnRenameKart(None)

    def ChangeCurrFiller(self, event):
        self.valId = event.GetSelection()

    def ChangeKart(self, event):
        self.kartId = event.GetSelection()

    def ChangeKartList(self, event):
        pass

    def on_pick(self, event):
        olddat = self.data[self.currKart][event.ind[0]]
        if olddat == self.clonedata:
            self.data[self.currKart][event.ind[0]] = ""
        else:
            self.data[self.currKart][event.ind[0]] = self.clonedata
        self.draw_figure()

    def draw_figure(self):
        u""" Redraws the figure"""
        ax = self.axes
        ax.clear()
        ax.grid(1)
        if not self.currKart in self.data:
            self.currKart = self.data.keys()[0]

        dat = self.data[self.currKart]

        gm = self.geom

        if gm.symmetry == 6:
            r = 1.1547/2  # 2/sqrt(3)
            angle = 0
        else:
            r = 1.4/2  # 2/sqrt(3)
            angle = math.pi/4
        patches = []
        fig = self.fig

        xmin = 0
        ymin = 0
        xmax = 0
        ymax = 0

        i = 0
        for y, x in gm.FIterYX():
            e = pat.RegularPolygon((
                x, y), gm.symmetry, radius=r, orientation=angle)
            patches.append(e)
            e = ax.text(x, y, str(dat[
                        i]), horizontalalignment='center', verticalalignment='center')
            xmin = min(xmin, x)
            ymin = min(ymin, y)
            xmax = max(xmax, x)
            ymax = max(ymax, y)
            i += 1

        rect = [xmin-1, xmax+1, ymin-1, ymax+1]
#        poly = matplotlib.collections.PatchCollection(
#            patches, cmap=matplotlib.cm.jet, picker=5)
        poly = matplotlib.collections.PatchCollection(
            patches,cmap=matplotlib.cm.bone, picker=5)
        ax.add_collection(poly)
        ax.axis(rect)

# делаем расцветку
        colordict = dict([(nm, float(i)) for i, nm in enumerate(
            [u""]+self.list_box_kart_items.GetStrings())])
        farr = np.array([colordict.get(str(nm), 0.) for nm in dat]+[-100000.])
        poly.set_array(farr)
        self.canvas.draw()

    def flash_status_message(self, msg, flash_len_ms=1500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER,
            self.on_flash_status_off,
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)

    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')

    def OnInsertKart(self, event):
        dlg = wx.TextEntryDialog(
            self, u'выберете имя для картограммы',
            u'Вставить новую картограмму', 'Python')
        dlg.SetValue("")
        if dlg.ShowModal() == wx.ID_OK:
            nm = dlg.GetValue()
            if nm not in self.keys.keys():
                self.kart = nm
                self.flash_status_message(u"вставили картограмму: "+nm)
            else:
                Msg(self, u"картограмма с таким именем уже есть")
        dlg.Destroy()

    def OnDeleteKart(self, event):
        nm = self.kart
        self.data.pop(nm)
        self.keys.pop(nm)
        self.UpdateKartList()

    def OnRenameKart(self, event):
        dlg = wx.TextEntryDialog(
            self, u'выберете новое имя для картограммы',
            u'переименование', 'Python')
        nm = self.kart
        dlg.SetValue(nm)
        if dlg.ShowModal() == wx.ID_OK:
            newnm = dlg.GetValue()
            if newnm not in self.keys.keys():
                v = self.data.pop(nm)
                self.data[newnm] = v
                v = self.keys.pop(nm)
                self.keys[newnm] = v
                self.UpdateKartList(newnm)
                self.flash_status_message(u"переименовали картограмму: "+nm)
            else:
                Msg(self, u"картограмма с таким именем уже есть")
        dlg.Destroy()

    def OnInsertVal(self, event):
        u"""вставление нового значения"""
        dlg = wx.TextEntryDialog(
            self, u'выберете новое значение',
            u'пополнение списка значений', 'Python')
        dlg.SetValue("")
        if dlg.ShowModal() == wx.ID_OK:
            nm = evex(dlg.GetValue())
            if nm not in self.keys.values():
                li = self.keys[self.kart]
                li.append(nm)
                li.sort()
                self.keys[self.kart] = li
                self.UpdateKartList(self.kart)
                self.flash_status_message(u"вставили значение: "+nm)
            else:
                Msg(self, u"такое имя уже есть")
        dlg.Destroy()

    def OnDeleteVal(self, event):
        u"""удаление значения"""
        ival = self.valId  # запомнили текущий индекс

        kart = self.kart  # взяли текущую картограмму
        li = self.keys[kart]  # получили список возможных значений
        if len(li) > 1:
            v = li.pop(ival)
            self.keys[kart] = li  # удаляем индекс из списка значений
            dat = self.data[kart]
            self.data[kart] = ["" if v == i else i for i in dat]
            newpos = ival-1 if ival != 0 else 0
            self.UpdateValueList(newpos)
            self.draw_figure()

    def OnRenameVal(self, event):
        u"""переименование нового значения"""
        pass


def test():
    app = wx.PySimpleApp()
    keys = {}
    keys["fuel"] = ["a", "b", "c"]
    keys["suz"] = map(str, range(10))
    data = {}
    data["fuel"] = range(163)
    app.frame = KartEdFrame(geom.Wwer1000(), keys, data)
    app.frame.Show()
    app.MainLoop()

if __name__ == '__main__':
    if len(sys.argv) == 1:
        test()
    else:
        keys = {}
        data = {}
        gm0 = None

        for nm in sys.argv[1:]:
            gm, d = geom.File2Geom(nm)
            if gm0:
                if not gm0 == gm:
                    raise Exeption("error read %s - geom error" % nm)
            ks = list(set(d))
            keys[nm] = ks
            data[nm] = d

        app = wx.PySimpleApp()
        app.frame = KartEdFrame(gm, keys, data)
        app.frame.Show()
        app.MainLoop()
