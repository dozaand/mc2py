import os
import SCons.Builder
import SCons.Tool
u"""build package"""

def packs_db(env):
    """A Tools to manage packs of libs"""
    from mc2py.py_pkg_config import UsePacks, PackInfo
    env.AddMethod(UsePacks, "UsePacks")

    def InstallPackFun(target, source, env):
        # install files fo predefined directory
        instdir = os.environ["usr_bin"]
        inst = env.Install(instdir,source)
        pkg_local_dkt = dict(installed_files =[i.abspath for i in inst],
            CPPPATH = env.fs.Dir('..').abspath,
            version = "1.0",
            depends = env["PACK_DEPENDS"])
        pkg_local_dkt.update(env["PACK"])
        pkg_local = PackInfo(**pkg_local_dkt)
        pkg = env["PACK_DB"][0]
        pkg.update(pkg_local)
        pkg.save()
#        Execute(inst)
        return None

    bld = SCons.Builder.Builder(action = InstallPackFun)
    env.Append(BUILDERS={"PackInstall":bld})

    def CreatePrj(target, source, env):
        # build project file
        
        if env["CC"]=='gcc':
            from mc2py.gen_prj import gen_prj_code_blocks as gen
            ext=".cbp"
        elif env["CC"]=='cl':
            from mc2py.gen_prj import gen_prj_msvc as gen
            ext=".sln"
        src = [i.sources[0].abspath for i in source[0].sources]
        gen(target[0].name+ext,src,
            target[0].name,includes=env["CPPPATH"],libs=env["LIBS"],lib_dirs=env["LIBPATH"],prj_typ="exe"
            )
        return None

    bld = SCons.Builder.Builder(action = CreatePrj)
    env.Append(BUILDERS = {'ProjectFile' : bld})

def exists(env):
    return 1
def generate(env):
    packs_db(env)