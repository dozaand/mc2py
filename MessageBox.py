#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx


class MessageBox(object):
    def __init__(self, name, msg):
        dlg = wx.MessageDialog(None, msg, name,
                               wx.OK | wx.ICON_INFORMATION
                               # wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL |
                               # wx.ICON_INFORMATION
                               )
        dlg.ShowModal()
        dlg.Destroy()
