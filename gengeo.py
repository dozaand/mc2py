#!/bin/env python3
"""
generating geo files

"""

#/usr/bin/env python3

from itertools import cycle
from collections import namedtuple

LineRef=namedtuple("LineRef", ["line","side"])

class Pnt:
    def __init__(self,p,lcar=None):
        self.p=p
        self.lcar=lcar
        self.ph=None
        self.cnt=0
    def __str__(self):
        x,y=self.p
        if self.lcar:
            return f"Point({self.cnt}) = {{{x}, {y}, 0, {self.lcar}}};"
        else:
            return f"Point({self.cnt}) = {{{x}, {y}, 0}};"

class Line:
    def __init__(self,p1,p2,ph=None):
        self.p=[p1,p2]
        self.ph=ph
        self.cnt=0

    def __str__(self):
        p0,p1=self.p
        l=f"Line({self.cnt}) = {{{p0.cnt}, {p1.cnt}}};"
        if self.ph:
            return f"""{l}\nPhysical Curve("{self.ph}", {self.cnt}) = {{ {self.cnt} }};"""
        else:
            return l

def get_ipnt(lin:tuple,ind:int):
    l,orient = lin
    if orient==1:
        return l.p[ind]
    else:
        i = 1-ind
        return l.p[i]

class Polygone:
    def __init__(self,lines:list,ph=None):
        self.points=[i.p[0] for i,sign in lines]
        self.lines=lines
        assert(get_ipnt(lines[0],0) is get_ipnt(lines[-1],1))
        self.ph=ph
        self.cnt=0

    def __str__(self):
        jn = ", ".join([str(i.cnt*sign) for i,sign in self.lines])
        l = f"""Curve Loop({self.cnt}) = {{ {jn} }};
Plane Surface({self.cnt}) = {{ {self.cnt} }};"""
        if self.ph:
            return f"""{l}\nPhysical Surface("{self.ph}", {self.cnt}) = {{ {self.cnt} }};"""
        else:
            return l


class Geo:
    def __init__(self):
        self.clear()

    def clear(self):
        self.objs=[]
        self.lines={}
        self.pcnt = 1
        self.lcnt = 1
        self.scnt = 1

    def add_pnt(self,p,lcar=None):
        obj = Pnt(p,lcar)
        obj.cnt = self.pcnt
        self.pcnt +=1
        self.objs.append(obj)
        return obj

    def add_line(self,p1,p2,ph=None):
        i,j = (p1.cnt,p2.cnt)
        if (i,j) in self.lines:
            return LineRef(self.lines[(i,j)],1)
        elif (j,i) in self.lines:
            return LineRef(self.lines[(j,i)],-1)
        else:
            obj = Line(p1,p2)
            self.objs.append(obj)
            obj.cnt=self.lcnt
            self.lcnt+=1
            self.lines[(i,j)]= obj
            return LineRef(obj,1)

    def add_poly(self,lin:list,ph=None):
        assert(type(lin[0]) is Pnt)
        lr = lin[1:]+[lin[0]]
        ll = [self.add_line(i,j) for i,j in zip(lin,lr)]
        obj = Polygone(ll,ph)
        obj.cnt=self.scnt;
        self.scnt+=1
        self.objs.append(obj)
        return obj
    
    def write(self,nm):
        txt="\n".join([str(i) for i in self.objs])
        with open(nm,"w") as f:
            f.write(txt)