#!/usr/bin/env python
# -*- coding: cp1251 -*-

import os
import threading
import glob
import subprocess as sp
from time import mktime
import datetime
from dateutil import tz
import re
import numpy as np


def dsc(vars):
    u"""���������, ����������� � ������ �������� ����������"""
    def mf(cls, li=vars):
        cls._dsc_ = dict([(i[0], i[1]) for i in li])
        cls._units_ = dict([(i[0], i[2]) for i in li])
        cls._fld_ = [i[0] for i in li]
        return cls
    return mf


def dscVT(vars):
    u"""���������, ����������� � ������ �������� ����������"""
    def mf(cls, li=vars):
        cls._type_ = dict([(i[0], i[1]) for i in li])
        cls._dsc_ = dict([(i[0], i[2]) for i in li])
        cls._units_ = dict([(i[0], i[3]) for i in li])
        cls._defaults_ = dict([(i[0], i[4]) for i in li])
        return cls
    return mf


def Dsc(name, typ=None, dsc="-", units="-", val=None):
    return [name, typ, dsc, units, val]

_rulett = u'� � � � � � �  � � � � � � � � � � � � � � � � � �  �  �   � � � � � � '
_rulettU = _rulett.upper()
_enlett = 'a b v g d e yo g z i i k l m n o p r s t u f x c ch sh sch b b bi ee u ya '
_enlettU = _enlett.upper()

_ru2En = dict(zip((_rulettU+_rulett).split(), (_enlettU+_enlett).split()))


def _Ru2En(lt):
    if lt in _ru2En:
        return _ru2En[lt]
    else:
        return lt


def RuTrans(str):
    u"""������� �������� � �������� �� ����������
>>> RuTrans(u'asd���� ����')
u'asdMama Papa'
"""
    return "".join([_Ru2En(i) for i in str])


def ienumerate(li):
    return [(j, i) for i, j in enumerate(li)]


def timing(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        start_time = _time.time()
        result = f(*args, **kwds)
        print("function: {} used {:.3f} seconds".format(
            f.__name__, _time.time() - start_time))
        return result
    return wrapper


def ForcedOpen(filename, openkeys):
    u"""��������� ���� � ������� ���������� ���� ����"""
    dirnm = os.path.dirname(filename)
    if not os.path.exists(dirnm):
        os.makedirs(dirnm)
    return open(filename, openkeys)


def SaveVars(nm, di, fileName="Output.dat"):
    u"""���� �� ������� nm �� ������� di � ���� fileName"""
    nmli = nm.split()
    with open(fileName, "wt") as f:
        for i in nmli:
            print >>f, i, "=", di[i]


def SaveAllVars(di, fileName="Output.dat"):
    u"""���� �� ������� nm �� ������� di � ���� fileName"""
    nmli = sorted(di.keys())
    with open(fileName, "wt") as f:
        for i in nmli:
            print >>f, i, "=", di[i]


def strany(obj):
    u"""������ str, ������� �������� � � ���������� ��������, ��� ������ Python ���� 3"""
    if hasattr(obj, "encode"):
        return obj.encode('cp1251')
    else:
        return str(obj)


class PeriodTimer(threading.Thread):
    u"""������������� ������ �������� ����� � �������� - ���������
>>> class Tx:
...    def __init__(self):
...        self.a=2
...    def pta(self):
...        print "self.a=",self.a
>>> aaa=Tx()
>>> obj=PeriodTimer(2, Tx.pta, (aaa,))
>>> obj.start()
>>> obj.cancel()
self.a= 2
self.a= 2
self.a= 2
self.a= 2
self.a= 2
"""
    def __init__(self, interval, fun, args=[], kwargs={}, chktime=0.2):
        threading.Thread.__init__(self)
        self.interval = interval
        self.fun = fun
        self.args = args
        self.kwargs = kwargs
        self._chktime = chktime
        self._lock = threading.Lock()  # ��� �������� ������� - ����� �������
        self._do_cancel = 0

    def cancel(self):
        self.signal_stop()
        self.join()

    def signal_stop(self):
        self._lock.acquire()
        self._do_cancel = 1
        self._lock.release()

    def run(self):
        while 1:
            (self.fun)(*self.args, **self.kwargs)
            t = 0
            while t < self.interval:
                self._lock.acquire()
                v = self._do_cancel
                self._lock.release()
                if v:
                    return
                time.sleep(self._chktime)
                t += self._chktime


def PathFind(pattr, root="."):
    u"""������ find �� bash"""
    for di, dl, fl in os.walk(root):
        for f in glob.fnmatch.filter(fl, pattr):
            yield os.path.join(di, f)

def PathFindDir(pattr, root="."):
    u"""������ find �� bash"""
    for di, dl, fl in os.walk(root):
        for f in glob.fnmatch.filter(dl, pattr):
            yield os.path.join(di, f)

Find = PathFind
FindDir = PathFindDir


def eps2png(infil):
    u"""����������� � png"""
    di, fil = os.path.split(infil)
    fil = os.path.splitext(fil)[0]
    sp.call(r"C:\apps\gs\gs8.64\bin\gswin32c.exe -dSAFER -dBATCH -dNOPAUSE -dGraphicsAlphaBits=4 -sDEVICE=png16m -sOutputFile=%(fil)s.png %(fil)s.eps" %
            locals(), shell=1)


class Stm:
    u"""�������� ������� (������� ����)
��� ��� �������������
������� ����� ����������� �� Stm
� ��� ������� - ��������������� ���������� � ���������� �������
� �������� ���������� �������� � ���� ���������� � ����
self.Next(self.State1)
��� ����������� ������� ���������� __call__
a(event)
"""
    def __init__(self):
        self._next = None

    def __call__(self, event):
        return (self._next)(event)

    def Next(self, f):
        self._next = f


class StmIter:
    u"""�������� ������� - �������� ������� ������������������

class TmsmIter(StmIter):
    def __init__(self,it):
        StmIter.__init__(self,it)
    def StateStart(self,ev):
        print "enter stateStart"
        if ev==1:
            self.Next(self.State1)
        elif ev==2:
            self.Next(self.State2)
        else:
            self.Next(self.StateStop)
    def State1(self,ev):
        print "enter state1"
        if ev==1:
            self.Next(self.State1)
        elif ev==2:
            self.Next(self.State2)
        else:
            self.Next(self.StateStop)
    def State2(self,ev):
        print "enter state2"
        if ev==1:
            self.Next(self.State1)
        elif ev==2:
            self.Next(self.State2)
        else:
            self.Next(self.StateStop)

for i in TmsmIter([1,1,1,2,1,2,1,1,0,1,2,3]):
    pass
"""
    def __init__(self, _iterobj):
        self._iterobj = _iterobj

    def __iter__(self):
        u"""������� ��� ��������� ��������� � ������ ���������"""
        self._next = self.StateStart
        self._iter = self._iterobj.__iter__()
        return self

    def Next(self, f):
        u"""��������� ���������� ��������� �������� self.Next(self.MyState)"""
        self._next = f

    def next(self):
        u"""������� ��� ��������� ���������� �������� ���������"""
        return (self._next)(self._iter.next())

    def StateStop(self, ev):
        u"""���������������� ������� ��������� ���������"""
        raise StopIteration()


def DirSize(nm):
    u"""return total directory files size"""
    s = 0
    for root, dirs, files in os.walk(nm):
        for fil in files:
            s += os.path.getsize(os.path.join(root, fil))
    return s


class newcwd:
    u"""��� ��������� ����� ������� ���������� ������� with
    with newcwd("c:/tmp"):
         f1()
         f2()
    � ����� ����������� ������ ����� ������� ������������ ������� ����������
    with newcwd():
         f1()
         f2()

"""
    def __init__(self, nwd=None):
        self.oldcwd = os.getcwd()
        self.newcwd = nwd

    def __enter__(self):
        if self.newcwd:
            os.chdir(self.newcwd)
        return self.newcwd

    def __exit__(self, exc_type, exc_value, traceback):
        os.chdir(self.oldcwd)


class Ccwd:
    u"""��� ��������� ����� ������� ���������� ������� with
    with Ccwd(locals()):
         f1()
         f2()
"""
    def __init__(self, cont):
        if "__file__" in cont:
            nwd = os.path.dirname(os.path.abspath(cont["__file__"]))
        else:
            nwd = os.getcwd()
        self.oldcwd = os.getcwd()
        self.newcwd = nwd

    def __enter__(self):
        os.chdir(self.newcwd)
        return self.newcwd

    def __exit__(self, exc_type, exc_value, traceback):
        os.chdir(self.oldcwd)

class safe_open:
    u"""backup file then open for write, restore old version in the case of unhandled exceptions"""
    def __init__(self, name,fun,*args):
        self.name=name
        self.bak=self.name+".bak"
        self.fun=fun
        self.args=args
    def __enter__(self):
        if os.path.exists(self.bak):
            os.remove(self.bak)
        if os.path.exists(self.name):
            os.rename(self.name,self.bak)
        self.f=self.fun(self.name,*self.args)
        return self.f

    def __exit__(self, exc_type, exc_value, traceback):
        self.f.close()
        if not exc_type is None:
            if os.path.exists(self.bak):
                os.remove(self.name)
                os.rename(self.bak,self.name)

def local_datetime_2_utc(local):
    u"""������� �� ���������� ������� � UTC
    �� �����
    local ��������� �����
    �� ������
    utc ������������� �����

    ������
    loca=datetime.datetime.now()
    utc = local_datetime_2_utc(loca)
    print loca
    print utc
    """
    # ������ ��������� ����
    from_zone = tz.tzlocal()  # ��������� ��������� ����
    to_zone = tz.tzutc()   # ��������� ���� UTC
    # ������������� ��������� ��������� ����
    local = local.replace(tzinfo=from_zone)
    # ��������� ��������� ���� � ���� UTC
    utc = local.astimezone(to_zone)
    return utc


def utc_datetime_2_local(utc):
    u"""������� �� ���������� ������� � UTC
    �� �����
    utc ��������� �����
    �� ������
    loca ������������� �����

    ������
    utc=datetime.datetime.now()
    loca = local_datetime_2_utc(utc)
    print loca
    print utc
    """
    # ������ ��������� ����
    from_zone = tz.tzutc()   # ��������� ���� UTC
    to_zone = tz.tzlocal()  # ��������� ��������� ����
    # ������������� UTC ��������� ����
    utc = utc.replace(tzinfo=from_zone)
    # ��������� UTC ���� � ���������
    local = utc.astimezone(to_zone)
    return local


def DateTime2Double(t):
    u"""����� � �������� � 1 ������ 1970 ����"""
    return mktime(t.timetuple())+1e-6*t.microsecond


def Double2DateTime(timestamp):
    u"""�������������� � ������� �����"""
    return datetime.datetime.fromtimestamp(timestamp)


def Double2UDateTime(timestamp):
    u"""�������������� � ������� �����"""
    return datetime.datetime.utcfromtimestamp(timestamp)


def Time2Index(tsec_array, t):
    u"""
    search index:tsec_array[index]<=DateTime2Double(dt)
    """
    return np.searchsorted(tsec_array, DateTime2Double(t))


def Str2DateTime(s):
    """�������������� ������ �� �����"""
    return datetime.datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")


def __SetDscUnits(cls):
    u"""������� ��� ������������ ������ - ������� ���������� � �����"""
    cls._dsc_ = {}
    cls._units_ = {}
    for nm, dsc, units, val in cls._defaults:
        cls._dsc_[nm] = dsc
        cls._units_[nm] = units


def dflt(cls):
    def finit(self, *arg, **kvarg):
        self._setdefaults()
        cls._init_old_(self, *arg, **kvarg)

    def _setdefaults(self):
        for rec in self._defaults:
            if not rec[-1] is None:
                setattr(self, rec[0], rec[-1])
# typ=type(cls.__name__,(cls,),{"__init__":finit,"_setdefaults":_setdefaults})
    cls._init_old_ = cls.__init__
    cls.__init__ = finit
    cls._setdefaults = _setdefaults
    __SetDscUnits(cls)
    return cls


def UnsetFields(obj):
    u"""�������� ��������, �� �� �������������, �����"""
    if hasattr(obj, "_dsc_"):
        s1 = set(dir(obj))
        s2 = set(obj._dsc_.keys())
        return s2.difference(s1)
    else:
        return set()


def UndescribedFields(obj):
    u"""�������� ����� ������ ������� �� �������"""
    s1 = set()
    if hasattr(obj, "__dict__"):
        s1.update(obj.__dict__.keys())
    if hasattr(obj, "_fields_"):
        s1.update([i[0] for i in obj._fields_])
    if hasattr(obj, "_dsc_"):
        s2 = set(obj._dsc_.keys())
        return s1.difference(s2)
    else:
        return s1


def check_output(cmd, shell=0):
    p = sp.Popen(cmd, shell=shell, stdout=sp.PIPE)
    return p.communicate()[0]


def PyExplorer(fil):
    u"""������ ���������� � windows � ������������ � ������������"""
    ext = os.path.splitext(fil)[1]
    typ0 = check_output('assoc %s' % ext, shell=1)
    typ = typ0.strip().split("=")[1]
    cmd = check_output('ftype %s' % typ, shell=1)
    cmd1 = re.sub("^.+=", "", cmd).strip()
    if "%1" in cmd1:
        cmd2 = cmd1.replace("%1", fil)
    else:
        cmd2 = "%s %s" % (cmd1, fil)

    sp.check_call(cmd2, shell=0)


def Create_tmp_file_name():
    u"""
    create temporary file name
    """
    t = datetime.datetime.now()
    return t.strftime("tmp_%Y_%m_%d_%H_%M_%S.dat")


class Tmp_file_name:
    u"""
    controlling temporary file
>>> with Tmp_file_name() as f:
>>>     open(f,"w").write("QQQ")
>>> print os.path.exists(f)
False
    """
    def __enter__(self):
        self.name = Create_tmp_file_name()
        return self.name

    def __exit__(self, typ, value, traceback):
        if os.path.exists(self.name):
            os.remove(self.name)

class scanner(object):
    u"""scan file by regular expressions"""
    def __init__(self,data):
        self._data=data
        self._pos=0
        self.debug_print=0
    def __call__(self,pattr,fun=None,group=1):
        fnd = re.search(pattr,self._data[self._pos:],re.M)
        self._pos += fnd.end()
        if fnd:
            if self.debug_print:
                print("found:",fnd.group(0))
            return fun(fnd.group(group)) if fun else fnd.group(1)
        else:
            raise Exception("not found")

def ffmt(file_in,file_out,**kvargs):
    u"""ffmt("a.dat","aaa.dat",keys="222",v=34)"""
    data=open(file_in,"r").read()
    open(file_out,"w").write(data.format(*kvargs))


if __name__ == '__main__':
    # ������ ��������: ���������� ������� -> utc -> double -> utc -> ��������� �������
    loca1 = datetime.datetime.now()
    utc1 = local_datetime_2_utc(loca1)
    dbl = DateTime2Double(utc1)
    utc2 = Double2DateTime(dbl)
    loca2 = utc_datetime_2_local(utc2)

    print(loca1)
    print(loca2)
    print(utc1)
    print(utc2)
