#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import ctypes
import string
import struct
import numpy as np
from datetime import datetime
from mc2py.fold import Accumulate
from mc2py.util import RuTrans, DateTime2Double, Double2DateTime, Str2DateTime, Double2UDateTime, Time2Index
from mc2py.int_bsearch import *
import h5py
import argparse
import re
import glob


class Trecinfo(object):
    pass

    def i2f(self, i):
        u"""перевод short в float"""
        return self.scale*i


def ReadGarantDsc(file):
    u"""чтение оглавления файла гаранта"""
    dsc = []
    with open(file, "r", encoding='utf-8') as f:
        for line in f.readlines():
            if not re.match("\*-*", line):
                a = Trecinfo()
                words = line.split()
                ig = 0
                a.name = words[ig]
                ig += 1
                a.eName = RuTrans(a.name)
                a.length = int(words[ig])
                ig += 1
                a.scale = 2.**-int(words[ig])
                ig += 1
                a.diapMin = float(words[ig])
                ig += 1
                a.diapMax = float(words[ig])
                ig += 1
                a.units = words[ig]
                ig += 1
                a.syst = words[ig]
                ig += 1
                a.comment = " ".join(words[ig:])
                a.adr = 0
                dsc.append(a)
    # f.close()
    return dsc


def MakeGarantBlock(dsc):
    u"""делаем блок данных гаранта"""
    li = [(Ru2En(v.name), ctypes.c_short) for v in dsc]

    class TgarantRec:
        _fields_ = li
        pass
    return TgarantRec


def Mkt(sec):
    u"""секунды в дату и время"""
    try:
        return datetime.fromtimestamp(sec-4*3600)
    except:
        print(sec)
        raise


class TSvrkArhDat:
    u"""данные гаранта"""
    _dsc_ = {}
    _units_ = {}

    def __init__(self, sec):
        self.t = Mkt(sec)
        self.tsec = sec
        pass


def GuessRecSize(filename):
    u"""
    пробуем вычислить размер блока на основании размера файла
    """
    fsize = os.path.getsize(filename)
    if fsize % 2406 == 0:
        if fsize % 7098 == 0:
            TSvrkArh.blksiz = int(input(
                "define block size 2406 for kalinin blocks 1,2 7098 for kalinin blocks 3,4"))
        else:
            TSvrkArh.blksiz = 2406
            return 2406
    else:
        if fsize % 7098 == 0:
            return 7098
        else:
            raise Exception("AiAi ai file size is not factor of 7096 or 2406")


class TSvrkArh:
    u"""читалка данных из архива"""
    def __init__(self, fdsc, filename):
        self.blksiz = GuessRecSize(filename)
        self.dsc = ReadGarantDsc(fdsc)
        for rec in self.dsc:
            TSvrkArhDat._dsc_[rec.eName] = rec.comment
            TSvrkArhDat._units_[rec.eName] = rec.units
        adr = res = [0]+Accumulate([el.length for el in self.dsc])
        for dsc, adr in zip(self.dsc, adr):
            dsc.adr = adr
        self.fileSize = os.path.getsize(filename)//self.blksiz
        self.f = open(filename, 'rb')
        self._MakeT2pos()

    @staticmethod
    def _Block2Data(data):
        t, l = struct.unpack('Ih', data[:6])
        arr = np.fromstring(data[6:], dtype='h')
        arr.byteswap(1)
        return t, arr

    def _MakeT2pos(self):
        self.timestep = 200
        self.tset = []
        for i in range(0, self.fileSize-1, self.timestep):
            t, arr = self.get(i)
            self.tset.append(Mkt(t))

    def get(self, pos):
        u"""извлечение данных из файла гаранта"""
        if pos < 0:
            pos = 0
        if pos >= self.fileSize:
            pos = self.fileSize-1
        self.f.seek(self.blksiz*pos)
        data = self.f.read(self.blksiz)
        return TSvrkArh._Block2Data(data)

    def _TransformRec(self, tsec, arr):
        u"""преобразование массива данных в структуру"""
        dat = TSvrkArhDat(tsec)
        for dsc in self.dsc:
            tmp = np.array(arr[
                           dsc.adr:dsc.adr+dsc.length], dtype='f')*dsc.scale
            if len(tmp) == 1:
                setattr(dat, dsc.eName, tmp[0])
            else:
                setattr(dat, dsc.eName, tmp)
        return dat

    def GetStruct(self, pos):
        u"""извлечение данных из файла гаранта и преобразование их в структуру"""
        tsec, arr = self.get(pos)
        return self._TransformRec(tsec, arr)

    def IterStruct(self, ibeg=0, iend=-1, step=1):
        u"""итерирование всех записей базы"""
        if iend < 0:
            iend = self.fileSize+iend
        self.f.seek(self.blksiz*ibeg)
        for i in range(ibeg, iend, step):
            data = self.f.read(self.blksiz)
            yield self._TransformRec(*TSvrkArh._Block2Data(data))
            if step > 1:
                self.f.read(selfblksiz*(step-1))

    def close(self):
        self.f.close()

    def __del__(self):
        self.close()

    def Range(t1, t2):
        u"""возвращаем пару позиций - интервал времен t1,t2"""
        return self.Time2Pos(t1), self.Time2Pos(t2)

    def Time2Pos(self, tim):
        u"""перевод времени в позицию"""
        pos = int_bsearch(self.tset, tim)
        pos *= self.timestep
        subtset = []
        for i in range(0, self.timestep):
            t, arr = self.get(i+pos)
            subtset.append(Mkt(t))
        subpos = int_bsearch(subtset, tim)
        return pos+subpos


# nshort=(TSvrkArh.blksiz - 6)/2
# arh=TSvrkArh('garant1.dsc',r'D:\kalininake\GARANT3\ARH\C05\Arh3b.dat')
# arh=TSvrkArh('garant1.dsc',r'D:\kalinindata\GARANT3\ARH\C05\Arh3b.dat')
# pos=arh.Time2Pos(datetime(2010, 4, 26, 1, 38))
# res=[ss for ss in arh.IterStruct(5,10)]

def Appendh5StructFile(sdsc, inp, outp):
    dsc = ReadGarantDsc(sdsc)
    syslist = list(set([RuTrans(i.syst) for i in dsc]))
    tl = []
    for i in dsc:
        if i.length == 1:
            tl.append((str(i.eName), 'f'))
        else:
            tl.append((str(i.eName), ('f', i.length)))
    if os.path.exists(outp):
        os.remove(outp)
    typ = np.dtype(tl)
    f = h5py.File(outp)
    f.create_dataset("data", dtype=typ, compression="gzip", shape=(50,))
    f.close()


def PathNm(i):
    u"""путь к данным в гаранте"""
    return "/"+str(RuTrans(i.syst))+"/"+str(i.eName)


def Makebuf(dsc, arh, blocksize):
    """создаем блок данных для буферизации передачи в hdf5 (Ускорение компрессии)"""
    res = {}
    el = arh.GetStruct(0)
    res["t"] = np.zeros(blocksize, dtype='I')
    for i in dsc:
        k = PathNm(i)
        res[k] = np.zeros((blocksize,)+getattr(el, i.eName).shape, dtype='f')
    return res


def AppendHDF5File(sdsc, inp, outp, append=0):
    arhopt = "gzip"
    arh = TSvrkArh(sdsc, inp)
    dsc = arh.dsc
    l = arh.fileSize
    recsize = l
#    l/=100
    if not append:
        if os.path.exists(outp):
            os.remove(outp)
    if not os.path.exists(outp):
        isnewfile = 1
    else:
        isnewfile = 0
    with h5py.File(outp, 'w') as f:
        if isnewfile:
            syslist = list(set([RuTrans(i.syst) for i in dsc]))
            # делаем ось времени
            dset = f.create_dataset("t", compression=arhopt, shape=(
                l,), maxshape=(None,), dtype='d')
    #        dset=f.create_dataset("tdict",shape=(l/1024,2),maxshape=(None,))
            for i in syslist:
                f.create_group(i)
            for i in dsc:
                try:
                    if i.length > 1:
                        dset = f.create_dataset(PathNm(i), compression=arhopt, shape=(
                            l, i.length), maxshape=(None, i.length))
                    else:
                        dset = f.create_dataset(PathNm(
                            i), compression=arhopt, shape=(l,), maxshape=(None,))
                    dset.attrs["units"] = i.units
                    dset.attrs["dsc"] = i.comment
                except:
                    print("fail constuct ", PathNm(i))
            for d in dsc:
                p = "%s/%s" % (RuTrans(d.syst), d.eName)
                dataset = f[p]
                dataset.attrs["units"] = d.units
                dataset.attrs["dsc"] = d.comment

            istart = 0  # начальная позиция
        else:
            istart = len(f["t"])  # начальная позиция
            recsize = l+istart
            f["t"].resize((recsize,))
            for i in dsc:
                try:
                    if i.length > 1:
                        f[PathNm(i)].resize((recsize, i.length))
                    else:
                        f[PathNm(i)].resize((recsize,))
                except:
                    print("fail resize", PathNm(i))
        blocksize = 10000
        buf = Makebuf(dsc, arh, blocksize)
        ii0 = 0
        for ii, el in enumerate(arh.IterStruct(0, l)):
            if ii % 100 == 0:
                print(100.*ii/recsize)
            if (ii % blocksize) == 0 and ii != 0:
                print("begin compression")
                k = "t"
                f[k][ii0+istart:ii+istart] = buf[k][:ii-ii0, ...]
                for i in dsc:
                    k = PathNm(i)
                    f[k][ii0+istart:ii+istart] = buf[k][:ii-ii0, ...]
                ii0 = ii
                print("end compression")
            buf["t"][ii-ii0] = getattr(el, "tsec")
            for i in dsc:
                k = PathNm(i)
                buf[k][ii-ii0, ...] = getattr(el, i.eName)[...]
        if ii % blocksize and ii != 0:
            print("begin compression")
            k = "t"
            f[k][ii0+istart:ii+istart+1] = buf[k][:ii-ii0+1, ...]
            for i in dsc:
                k = PathNm(i)
                f[k][ii0+istart:ii+istart+1] = buf[k][:ii-ii0+1, ...]
            ii0 = ii
            print("end compression")
        t = f["t"]
        t.attrs["t0"] = str(Double2DateTime(t[0]))
        t.attrs["t1"] = str(Double2DateTime(t[-1]))


# AppendHDF5File("garant1.dsk","Arh3b.dat","bb.h5")
# import cProfile
# cProfile.run('AppendHDF5File("garant1.dsk","Arh3b.dat","bb.h5")')

# import tables
# tables.test()

def Test():
    sdsc = r"garant1.dsc"
    sdsc = r"Avrk3b.dsc"
    inp = "Arh3b.dat"
    inp = r"D:\kalinindata\GARANT3\ARH\C02\Arh3b_2.dat"
    outp = "Arh3b_2.h5"
    import time
    t0 = time.clock()
    AppendHDF5File(sdsc, inp, outp)
    t1 = time.clock()
    print('time=', t1-t0)
# Test()


def AddComments(sdsc, filename):
    u"""добавляем описания переменных в файл"""
    with h5py.File(filename) as f5:
        dsc = ReadGarantDsc(sdsc)
        for d in dsc:
            p = "%s/%s" % (RuTrans(d.syst), d.eName)
            dataset = f5[p]
            dataset.attrs["units"] = d.units
            dataset.attrs["dsc"] = d.comment


def Extract(filein, fileout, t0, t1):
    u"""extract data from hdf5 file"""
    fin = h5py.File(filein, "r")
    fout = h5py.File(fileout)
    t = fin["t"]
    it0 = Time2Index(t, t0)
    it1 = Time2Index(t, t1)
    tn = fout.create_dataset("t", compression="gzip", data=fin["t"][it0:it1])
    tn.attrs["t0"] = str(Double2DateTime(t[it0]))
    tn.attrs["t1"] = str(Double2DateTime(t[it1]))

    for dsys in fin.keys():
        if dsys != u"t":
            dr = fout.create_group(dsys)
            drold = fin[dsys]
            for j in drold:
                dset = dr.create_dataset(j, data=drold[
                                         j][it0:it1], compression="gzip")
                for k in drold[j].attrs:
                    dset.attrs[k] = drold[j].attrs[k]
    fin.close()
    fout.close()

# filein="C02.h5"
# fileout="C02_extr.h5"
# import datetime
# t0=datetime.datetime(2006, 5, 20, 20, 40)
# t1=datetime.datetime(2006, 5, 21, 13, 19, 59)
# Extract(filein,fileout,t0,t1)


def H5timerangeinfo(filein):
    u"""return t0,t1,n"""
    fin = h5py.File(filein, "r")
    t = fin["t"]
    return dict(t0=t[0], t1=t[-1], n=len(t))

# filenames=[filein,fileout]


def Join(filenames, fileout):
    u"""collect files together"""
    inf = [(i, H5timerangeinfo(i)) for i in filenames]
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="import garant data to h5 format file")
    parser.add_argument("-o", "--out", help=u"output file name")
    parser.add_argument("-d", "--dsc", help=u"data description file")
    parser.add_argument(
        "-a", "--append", action='store_true', help=u"append data to file")
    parser.add_argument("files", nargs="*", help=u"input files")
    args = parser.parse_args()
    append = args.append
    for inmask in args.files:
        for infile in glob.glob(inmask):
            AppendHDF5File(args.dsc, infile, args.out, append=append)
            append = True
