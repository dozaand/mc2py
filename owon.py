#!/usr/bin/env python
# -*- coding: cp1251 -*-
#-------------------------------------------------------------------------------
# Name:        owon
# Purpose:     reading data from oscilloscope owon HDS2062M
#
# Author:      Andrey A. Semenov dozaand@mail.ru
# ref          reverce engeneering done by http://sourceforge.net/projects/owondriver/
# Created:     28.08.2011
# Copyright:   (c) and 2011
# Licence:     GNU GENERAL PUBLIC LICENSE Version 3
#-------------------------------------------------------------------------

u"""
reading data from oscilloscope owon HDS2062M

"""
import usb
import usb.util
import struct
import numpy as np
import time


def _decodeTimebase(i):
    factor = [5, 10, 25]
    timebase = factor[i % 3]
    i /= 3
    while i:
        i -= 1
        timebase *= 10
    return timebase


class OscData(object):
    _dsc_ = {"chname": "chanal name",
             "blklen": "size of data for this channal",
             "samples1": "long int holding the count of samples",
             "samples2": "long int holding the count of samples",
             "index": "current data pointer in buffer (to do continuos output)",
             "timebasecode": "ensoding of time resolution",
             "vertsenscode": "vertical sensitivity",
             "last": "offset of end of data for thos block",
             "data": "voltage data"
             }

    def __init__(self, owonbuffer, offset):
        self.chname, self.blklen, self.samples1, self.samples2, self.index, self.timebasecode, self.vertsenscode = struct.unpack_from(
            "<3s4II4xI", owonbuffer, offset)
        sdata = np.frombuffer(
            owonbuffer, dtype='h', count=self.samples1, offset=offset+51)
        self.data = sdata*(Hds2062._kVscale*Hds2062._vsens[self.vertsenscode])
        self.Dt = (Hds2062._hsens[self.timebasecode]*Hds2062._kNtinterv)
        self.dt = self.Dt/self.samples1
        self.n = self.samples1
        self.last = offset+self.blklen+3

    def t(self):
        u"""get timeset"""
        return np.linspace(0., self.Dt, self.samples1)


def ReadBuf(owonbuf):
    u"""read all chanals from ovon buffer"""
    offset = 10
    res = []
    n = len(owonbuf)
    while offset < n:
        a = OscData(owonbuf, offset)
        res.append(a)
        offset = a.last
    return res

_USB_LOCK_VENDOR = 0x5345
_USB_LOCK_PRODUCT = 0x1234
_OWON_START_DATA_CMD = "START"


class Hds2062(object):
    u"""����� ��� ��������� �������������"""
    DEFAULT_INTERFACE = 0x00
    VECTORGRAM_FILE_HEADER_LENGTH = 10
    VECTORGRAM_BLOCK_HEADER_LENGTH = 51

    # encoding range in Y direction [v]
    _vsens = [-1, 5., 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1., 2., 5.]
    # encoding range in X direction [c]
    _hsens = [1e-9*_decodeTimebase(i) for i in range(0x1f+1)]
    _kVscale = 4./127
    _kNtinterv = 12.

    def __init__(self):
        self._open()

    def _open(self):
        dev = usb.core.find(
            idVendor=_USB_LOCK_VENDOR, idProduct=_USB_LOCK_PRODUCT)

        if dev is None:
            raise ValueError('Device not found')
        dev.set_configuration()
        cfg = dev[0]
        cfg.set()
        dev.set_configuration(cfg)
        intf = cfg[(0, 0)]
        self.intf = intf
        epR = intf[0]
        epW = intf[1]

        self.dev = dev
        self.epR = epR
        self.epW = epW
        # devinfo=usb.control.get_descriptor(dev,0x12,usb.util.DESC_TYPE_DEVICE,0,0)

    def _close(self):
#        self.dev.reset()
        usb.util.release_interface(self.intf)

    def rdbuf(self):
        dev = self.dev
        epR = self.epR
        epW = self.epW
        usb.control.clear_feature(dev, usb.control.ENDPOINT_HALT, epW)
        res = epW.write(_OWON_START_DATA_CMD)
        usb.control.clear_feature(dev, usb.control.ENDPOINT_HALT, epR)
        bufdata = epR.read(0xc)
        owonDataBufferSize = struct.unpack("<i", buffer(bufdata)[:4])[0]
        owonDataBuffer = epR.read(owonDataBufferSize)
        return owonDataBuffer

    def Get(self):
        dat = self.rdbuf()
        return ReadBuf(dat)

    def Collect(dt=1):
        u"""data collection for specified time interval"""
        collected = []
        index = []
        dat = self.Get()
        t = 0.
        Dt = dat[0].Dt
        dtin = dat[0].dt
        tsleep = Dt*0.8

        npoint = dat[0].samples1
        index.append(dat[0].index)
        collected.append([row.data for row in dat])
        t += dat[0]
        tt0 = time.clock()
        while True:
            if time.clock()-tt0 > dt:
                break

##    def CollectTo(fil,dt=1):
##        """collect data dt seconds and write it to file"""
##        with open(fil,"wb") as f:
##            dat=self.Get()
##            t0=0.
##            tsleep=dat[0].Dt*0.8
##            while t<dt:

from pylab import plot
from mc2py.Redactor.tv.listed import led

osc = Hds2062()
res = []
for i in range(20):
    res.append(osc.Get())
    time.sleep(0.8)

t1 = res[0][0].t()
t2 = res[1][0].t()
plot(res[0][0].t(), res[0][0].data)
i = 3
plot(res[i][0].t(), res[i][0].data)
import cPickle
with open("a.dat", "wb") as f:
    cPickle.dump(res, f, 2)

aa = [x[0].index for x in res]
t = res[0].t()
plot(t, res[0].data)
