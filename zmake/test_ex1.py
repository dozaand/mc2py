#!/usr/bin/env python
# -*- coding: cp1251 -*-
import unittest
from mc2py.zmake import *


def Test1():
    """
>>> a=[1,2,3]
>>> print list(_SubitemsList(a))
[(0, 1), (1, 2), (2, 3)]
>>> a={"a":1,"b":2}
>>> print list(_SubitemsList(a))
[('a', 1), ('b', 2)]
>>> class Ta(object):
...     def __init__(self):
...         self.a=1
...         self.b=2
>>> a=Ta()
>>> print list(_SubitemsList(a))
[('a', 1), ('b', 2)]
    """
    pass

# a=[1,2,3]
# print list(_SubitemsList(a))
# a={"a":1,"b":2}
# print list(_SubitemsList(a))
# class Ta(object):
##    def __init__(self):
##        self.a=1
##        self.b=2
# a=Ta()
# print list(_SubitemsList(a))

# a=[1,2,3]
# _SetField(a,1,22)
# print list(_SubitemsList(a))
# a={"a":1,"b":2}
# _SetField(a,1,22)
# print list(_SubitemsList(a))
# a=Ta()
# _SetField(a,"vv",22)
# print list(_SubitemsList(a))


class Tsum(TMakeble, PersistentList):
    def __init__(self, *args):
        super(Tsum, self).__init__()
        self.extend(args)

    def Build(self):
        print "do sum"
        self.val = sum([i.val for i in self.data])


class Tprod(TMakeble, UserList):
    def __init__(self, *args):
        super(Tprod, self).__init__()
        self.extend(args)

    def Build(self):
        print "do prod"
        p = 1
        for i in self.data:
            p *= i.val
        self.val = p


class Test_SubitemsList(unittest.TestCase):
    """_SubitemsList"""

    class Ta(object):
        def __init__(self):
            self.a = 1
            self.b = 2

    def test_1_ListList(self):
        """list"""
        a = [1, 2, 3]
        self.assertEqual(list(_SubitemsList(a)), [(0, 1), (1, 2), (2, 3)])

    def test_2_ListDict(self):
        u"""dict"""
        a = {"a": 1, "b": 2}
        self.assertEqual(list(_SubitemsList(a)), [('a', 1), ('b', 2)])

    def test_3_ListList(self):
        u"""class"""
        a = Test_SubitemsList.Ta()
        self.assertEqual(list(_SubitemsList(a)), [('a', 1), ('b', 2)])

    def test_4_SetList(self):
        u"""ins list"""
        a = [1, 2, 3]
        _SetField(a, 1, 22)
        self.assertEqual(list(_SubitemsList(a)), [(0, 1), (1, 22), (2, 3)])

    def test_5_SetDict(self):
        u"""ins dict"""
        a = {"a": 1, "b": 2}
        _SetField(a, 1, 22)
        self.assertEqual(sorted(list(_SubitemsList(
            a))), [(1, 22), ('a', 1), ('b', 2)])

    def test_6_SetDict(self):
        u"""ins class"""
        a = Test_SubitemsList.Ta()
        _SetField(a, "vv", 22)
        self.assertEqual(sorted(list(_SubitemsList(
            a))), [('a', 1), ('b', 2), ('vv', 22)])

    def setUp(self):
        pass

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
