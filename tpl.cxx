//делаем топливную ячейку
%%clycle_pt
<cycle>
%for (auto it = zones.rbegin(); it != zones.rend(); ++it)
%{
 <circle cx="${it->r[0]}" cy="${it->r[1]}"  r="${it->R}" fill="${it->material.color}"/>
%}
</cycle>
%%

//любой текст между файлами
%%file1
<svg width="${w}" height="${h}">
%(
for(auto& i:pins)
{
i.pt(s);
}
%)
</svg>
%%
