#!/usr/bin/env python
# -*- coding: utf-8 -*-

import h5py
import numpy as np
import os

def shp_data(nrec,iWRo):
    l_i = iWRo[:,2]-(iWRo[:,1]-1)
    l_k = iWRo[:,4]-(iWRo[:,3]-1)
    shp=[tuple([i for i in (nrec,kk,ii) if i>1]) for kk,ii in zip(l_k,l_i)]
    shp0=[(0,)]+[(kk,ii) for kk,ii in zip(l_k,l_i)] # append dt Добавил начальный ноль и TAU
    length = [np.prod(i) for i in shp0]
    offst = np.cumsum(length)
    return shp,offst

def korres_convert(file_in="korres",file_out="korres.h5"):
    "конвертация результатов расчета по корсару в hdf5"
    file_size=os.path.getsize(file_in)
    f=open(file_in,"rb")
    lenb,lWRb,nWR,lentit = np.fromfile(f,dtype='i',count=4)
    piWRo = np.fromfile(f,dtype='i',count=nWR)
    iWRob = piWRo.reshape((-1,6))
    iWRo = np.vstack([[14,1,1,1,1,1001], iWRob])
    bb1=np.fromfile(f,dtype='S40',count=lWRb)
    b1=["TAU"]+[i.decode("cp1251").strip() for i in bb1]
    btitle = np.fromfile(f,dtype="S{}".format(lentit),count=1)
    title = btitle[0].decode("cp1251").strip()

    # decode type
    iTx = iWRo[:,-1]%2
    lx1 = np.array((iWRo[:,-1]-iTx)/1000,dtype='i')
    # count rec
    pos=f.tell()
    nrec = int((file_size - pos) / (lenb * 8))


    # делаем в памяти образ данных
    shp,offst = shp_data(nrec,iWRo)
    ts=["i","d"]
    dataset=[np.zeros(s,dtype=ts[t]) for s,t in zip(shp,iTx)]

    # присваиваем данные
    for irec in range(nrec):
        buf=np.fromfile(f,dtype="d",count=lenb)
        for i in range(len(dataset)):
            if(dataset[i][irec].shape):
                dataset[i][irec][...]=buf[offst[i]:offst[i+1]]
            else:
                dataset[i][irec]=buf[offst[i]:offst[i+1]]

    with h5py.File(file_out,"w") as f5:
        for n,d in zip(b1,dataset):
            try:
                f5[n]=d
            except:
                print("fail add",n)
        f5["TAU"].attrs["name"]=title


if __name__ == '__main__':
    if len(sys.argv)==3:
        korres_convert(sys.argv[1],sys.argv[2])
