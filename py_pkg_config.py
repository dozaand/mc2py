##!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
functions for package management
"""
import os
import yaml
import argparse
import codecs
import copy

def _string_to_list(dkt):
    u"""covert "a"->["a"] """
    for k,v in dkt.iteritems():
        if isinstance(v, dict):
            _string_to_list(v)
        elif isinstance(v, basestring):
            dkt[k] = [v]

def _list_to_string(dkt):
    u"""covert "a"->["a"] """
    for k,v in dkt.iteritems():
        if isinstance(v, dict):
            _list_to_string(v)
        elif len(v)==1:
            dkt[k] = v[0]

def y_load(file_name,encoding="utf-8"):
    with codecs.open(file_name,"r",encoding=encoding) as f:
        db = yaml.load(f)
    return db


class PackInfo(object):
    u"""infromation about packages"""

    def __init__(self,file_name="pkg.yaml",encoding="utf-8",name=None,**kvargs):
        u"""constructed from file or by name and props
        pkg = PackInfo("a.yaml")
        pkg = PackInfo(name="pack1",LIB="a.lib",CPPPATH=Dir(".").abspath)
        """
        self.file = os.path.abspath(file_name)
        self.dir = os.path.split(self.file)
        if name:
            self.db = {name:kvargs}
        else:
            if os.path.exists(self.file):
                self.db=y_load(file_name)
        _string_to_list(self.db)

    def packlist(self,res,nm):
        u"""remove union of all dependent packeges"""
        res.add(nm)
        try:
            for i in self.db[nm]["depends"]:
                if not i in res:
                    self.packlist(res,i)
        except:
            pass

    def __getitem__(self,ind):
        u"""get union of options for all dependent packeges"""
        namelist = ind.split("/")
        pack = set()
        self.packlist(pack,namelist[0])
        path = namelist[1:]
        optionlist = set()
        for pack_name in pack:
            elm=self.db[pack_name]
            try:
                for path_element in path:
                    elm = elm[path_element]
                optionlist.update(elm)
            except KeyError:
                pass
        return list(optionlist)

    def getopt(self,pack_list,ind):
        u"""get union of options for all dependent packeges"""
        pack = set()
        for nm in pack_list:
            self.packlist(pack,nm)
        path = ind.split("/")
        optionlist = set()
        for pack_name in pack:
            elm=self.db[pack_name]
            try:
                for path_element in path:
                    elm = elm[path_element]
                optionlist.update(elm)
            except KeyError:
                pass
        return list(optionlist)

    def update(self,obj):
        u"""update database by new values"""
        self.db.update(obj.db)

    def replace(self,part_name,**kvargs):
        u"""syntax shuger update only one package information"""
        self.db.update({part_name:kvargs})

    def save(self,file_name=None,encoding="utf-8"):
        u"""save data to file"""
        if file_name is None:
            file_name = self.file
        db = copy.deepcopy(self.db)
        _list_to_string(db)
        with codecs.open(file_name,"w", encoding=encoding) as f:
            yaml.dump(db,f)


def UsePacks(env,depends,parts_file=None):
    u"""fill environment form database
    env environment to be updated
    depends - list of packeges to be used
    return database object - It must be saved in install package action
    """
    if parts_file is None:
        parts_file = os.path.join(os.environ["usr_bin"],"pkg.yaml")
    pkg = PackInfo(parts_file)
    res={}
    for opts in ["CPPPATH","LIBS","LIBPATH"]:
        res[opts] = pkg.getopt(depends,env.subst("$CC/{0}".format(opts)))
    env.AppendUnique(**res)
    env.AppendUnique(PACK_DEPENDS=depends)
    env.AppendUnique(PACK_DB=[pkg])
    return pkg

##if __name__ == '__main__':
##    parser = argparse.ArgumentParser(prog="rrename",description="rename files by regular expression replace")
##    parser.add_argument("-r","--recursive",action='store_true',help=u"search files in subdirs")
##    parser.add_argument("-f","--force",action='store_true',help=u"force file overwrite")
##    parser.add_argument("-s","--symlinks",action='store_true',help=u"follow symlinks")
##    parser.add_argument("-d","--directory",help=u"root directory")
##    parser.add_argument("patterns",nargs="2",help=u"input_pattern output_pattern")
##    args = parser.parse_args()
##    root = args.directory
##    if not root:
##        root = "."
##    sys.exit(rrename(args.patterns[0],args.patterns[1],root,args.force,args.recursive,args.symlinks))