#!/usr/bin/env python
# -*- coding: cp1251 -*-
u""" ��������� ������"""
import os
import numpy as np
from ecran_3d.ecran_alg.fifo import Fifo


class Median_filter():
    u"""����� ��� ��������� ����������"""
    def __init__(self, size):
        u"""������������� ���������� ������
        size ����������� ����������� ���� ������
        """
        self.size = size # ����������� ����������� ������
        self.fifo =  Fifo( self.size ) # ������� ��� ��������� ���������������-��������� ������� ��������� ��������


    def filtration(self,data):
        u"""��������� ������ ��������� ��������"""
        # ��������� ������ � FIFO
        self.fifo.append( data )
        # ���������� ��������� ���������
        median_res = np.median( self.fifo.data, axis=0 )
        return median_res

    def clear(self):
        u"""������� �������������"""
        self.fifo.clear()





#---------------------------------------------------------------------------
if __name__ == '__main__':
    obj = Median_filter(3)
    for el in range(7):
        res = obj.filtration(el)
        print(obj.fifo.data, res)
    pass








