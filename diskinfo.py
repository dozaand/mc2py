#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
 ��� ������� ��������� ������� ������� ����� � ���������� ������������
"""
import os
import win32file


def diskinfo(p=None):
    u"""���������� ���������� ���������� ����� �� �����"""
    if not p:
        p = os.path.splitdrive(os.getcwd())[0]
    secsPerClus, bytesPerSec, nFreeClus, totClus = win32file.GetDiskFreeSpace(
        p)
    return secsPerClus * bytesPerSec * totClus, secsPerClus * bytesPerSec * nFreeClus

if __name__ == '__main__':
    print diskinfo()
