#! /bin/env/python
# -*- coding:utf-8 -*-
u""" As unix rename but regular expression syntax"""
import nose
from nose import with_setup
import shutil,os
from mc2py.rrename import rrename

# парадигма - разработка через тестирование
def mk_fil(data):
    with open("a.dat","w") as f:
        f.write(str(data))
    return "a.dat"

def make_test_dir():
    os.renames(mk_fil(1),"aaa/a1.dat")
    os.renames(mk_fil(2),"aaa/a2.dat")
    os.renames(mk_fil(3),"aaa/bbb/a3.dat")
    os.renames(mk_fil(3),"aaa/bbb/a4.dat")
    os.renames(mk_fil(3),"aaa/bbb/4a.dat")

def rm_test_dir():
    shutil.rmtree("aaa")

@with_setup(make_test_dir, rm_test_dir)
def test_rrename_nr():
    rrename(ur"([a-z]+)(\d+)",ur"\2\1","aaa")
    assert(os.path.exists("aaa/1a.dat") and
       os.path.exists("aaa/2a.dat") and
       os.path.exists("aaa/bbb/a3.dat") and
       os.path.exists("aaa/bbb/a4.dat")
          )

@with_setup(make_test_dir, rm_test_dir)
def test_rrename_r():
    rrename(ur"([a-z]+)(\d+)",ur"\2\1","aaa",recursive=1)
    assert(os.path.exists("aaa/1a.dat") and
       os.path.exists("aaa/2a.dat") and
       os.path.exists("aaa/bbb/3a.dat") and
       os.path.exists("aaa/bbb/4a.dat") and
       os.path.exists("aaa/bbb/a4.dat")
          )

@with_setup(make_test_dir, rm_test_dir)
def test_rrename_rf():
    rrename(ur"([a-z]+)(\d+)",ur"\2\1","aaa",recursive=1,force=1)
    assert(os.path.exists("aaa/1a.dat") and
       os.path.exists("aaa/2a.dat") and
       os.path.exists("aaa/bbb/3a.dat") and
       os.path.exists("aaa/bbb/4a.dat") and
       not os.path.exists("aaa/bbb/a4.dat")
          )
# символьные ссылки лениво тестировать

nose.main()