#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""
���������� � make
����� ������� ������ ����� mtime - ����� ��������� ����������
���� ��������� � ����������
������ ����� deps - ������ ������������
cmd - ����� ���������� ���� res=cmd(task,deps)
"""
# from mc2py.pymakeobj import makable
from mc2py.zdb import *
import os
import glob
import json
import hashlib


def _SubitemsList(obj):
    u"""�������� ��� ������ ��������"""
    if hasattr(obj, "iteritems"):  # �������
        for key, val in obj.iteritems():
            yield key, val
    elif hasattr(obj, "__iter__"):  # ������ �� ������� � ������������ ��������
        for key, val in enumerate(obj):
            yield key, val
    elif hasattr(obj, "__dict__"):  # ������
        for key, val in obj.__dict__.iteritems():
            yield key, val


def _SetField(obj, k, v):
    u"""��������� ��������"""
    if hasattr(obj, "__setitem__"):  # �������
        obj[k] = v
    elif hasattr(obj, "__dict__"):  # ������
        setattr(obj, k, v)


class _VerCounter(object):
    u"""������� ������"""
    def __init__(self):
        self._cnt = 0

    def inc(self):
        self._cnt += 1

    def __call__(self):
        return self._cnt


class TMakeble(Persistent):
    u"""������������ ������������ �����������"""
    _MversionGlob = _VerCounter(
    )  # ���� ����� ������ ������ �� ���������� ������ ����� �� ������ ����������� ������ ����������

    def __init__(self):
        super(TMakeble, self).__init__()
        self._MversionGlob = TMakeble._MversionGlob
        self._makeVersion = 0
        self._makeDone = 0

    def _Make(self, *args, **kvargs):
        u""""""
        needbuild = 0
        ver = self._makeVersion
        for k, v in _SubitemsList(self):
            if issubclass(type(v), TMakeble):
                if not v._makeDone:
                    v._Make(*args, **kvargs)
                # � ������ ����� ���� ��� �� ��������� - ������ ����������� ����� ���� ��� ���������
                if self._makeVersion == 0 or (self._makeVersion < v._makeVersion):
                    needbuild = 1
                    _SetField(self, k, v)
                    ver = max(ver, v._makeVersion)
        self._Check()
        if needbuild:
            self.Build(*args, **kvargs)
            self._makeVersion = ver
        self._makeDone = 1

    def _Check(self):
        u"""�������� ����������� ��� �������� ������� ���������"""
        pass

    def _ResetMakeFlags(self):
        u"""����� ������ make"""
        self._makeDone = 0
        for k, v in _SubitemsList(self):
            if issubclass(type(v), TMakeble):
                if v._makeDone:
                    v._ResetMakeFlags()

    def Build(self, *args, **kvargs):
        if hasattr(self, "BuildCmd"):
            (self.BuildCmd)(*args, **kvargs)

    def Make(self, *args, **kvargs):
        u"""������������ �������"""
        self._Make(*args, **kvargs)
        self._ResetMakeFlags()
#        transaction.commit()

    def _Touch(self):
        u"""������� ������� � ����������� ���������"""
        self._MversionGlob.inc()
        self._makeVersion = self._MversionGlob()


class TMParam(TMakeble):
    u"""����� (��� ����� ������ ������) ������������ �������� �������"""
    def __init__(self, v):
        super(Tparam, self).__init__()
        self.val = v  # ������ �� ���������

    def vset(self, _v):
        self._Touch()
        self._val = _v

    def vget(self):
        return self._val
    val = property(vget, vset)


class FileByDate(TMakeble):
    u"""�������� �������������� ������ - ��������� ������������ �� ���� ������� ���������"""
    def __init__(self, fnm):
        super(FileByDate, self).__init__()
        self.filename = fnm
        self._oldmtime = self.mtime
        self._Touch()

    def _Check(self):
        t = self.mtime
        if t != self._oldmtime:
            self._Touch()
            self._oldmtime = self.mtime

    @property
    def mtime(self):
        if os.path.exists(self.filename):
            return os.stat(self.filename).st_mtime
        else:
            return 0


class FileByMD5(TMakeble):
    u"""�������� �������������� ������ - ��������� ������������ �� ����������� ����� �����"""
    def __init__(self, fnm):
        super(FileByMD5, self).__init__()
        self.filename = fnm
        self._oldhash = None
        self._Check()

    def _Check(self):
        t = self.hash
        if t != self._oldhash:
            self._Touch()
            self._oldhash = t

    @property
    def hash(self):
        if os.path.exists(self.filename):
            return hashlib.md5(open(self.filename, "rb").read()).digest()
        else:
            return ""


class FileJsonMD5(FileByMD5):
    u"""�������� �������������� ������ - ��������� ������������ �� ����������� ����� �����"""
    def __init__(self, fnm):
        super(FileJsonMD5, self).__init__(fnm)

    def _Check(self):
        t = self.hash
        if t != self._oldhash:
            self._Touch()
            with open(self.filename, "rt") as f:
                self.val = json.load(f)
            self._oldhash = self.hash


if __name__ == '__main__':
#    a=FileObj("a.json")
    print 'ok'
