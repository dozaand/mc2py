#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import sys
import pyparsing as pp
from pyparsing import *

file_name = None
i_line = None

def linf(s,l):
    global i_line
    pos = lineno(l,s)
    if pos != i_line:
        i_line=pos
        return '#line {} "{}"\n'.format(pos, file_name)
    else:
        return ""

def text_block(s,l,t):
    "экранировка спец символов и разбивка на строки"
    global file_name
    l0 = t[0].replace('"','\\"')
    l0 = l0.replace("\n",r"\n")
    return '{}s<<"{}";'.format(linf(s,l), l0)

text = pp.Regex(r'([^(%\n$]|%(?!%)|\((?!%)|\n(?!%)|\$(?!{))+') + Optional(Literal("\n"))
text.setParseAction(text_block)
block_code = Suppress(Literal("(%")) + SkipTo("%)",include=True)

def pt_block_code(s,l,t):
    return linf(s,l)+t[0]

block_code.setParseAction(pt_block_code)
line_code = Suppress(LineStart()+Regex("%(?!%)")) + SkipTo("\n",include=True)
line_code.setParseAction(pt_block_code)
expr = Literal("${") + SkipTo("}",include=True)
def pt_expr_code(s,l,t):
    return '{}s<<({});'.format(linf(s,l),t[1])
expr.setParseAction(pt_expr_code)
#expr.setParseAction(lambda t:'s<<({});'.format(t[1]))
body_item = line_code | block_code | expr | text
tpl_body = OneOrMore(body_item) + Suppress("%%")

def jn(t):
    return "\n".join(t)
tpl_body.setParseAction(jn)

tpl = Suppress(SkipTo("%%",include=True)) + SkipTo("\n",include=True) + tpl_body

def write_files(t):
    "во время парсинга пишем выходные файлы"
    nm = t[0]
    body =t[-1]
    with open(nm+".h","w",encoding="utf-8") as f:
        f.write(body)
    return ""

def fail_value(s, loc, expr, err):
    print(err)
    raise ParseFatalException(s, loc, "Wrong value")

tpl.setParseAction(write_files)

tpl_file = OneOrMore(tpl)
tpl_file.setFailAction(fail_value)
tpl_file.leaveWhitespace()

def process_file(filename):
    with open(filename,"r",encoding="utf-8") as f:
        tpl_file.parseFile(f, parseAll=True)
#        data = f.read()
#    tpl_file.parseString(data)

def test_parser():
    process_file("tpl.cxx")

#test_parser

if __name__=="__main__":
    for i in sys.argv[1:]:
        try:
            file_name = i
            process_file(i)
        except:
            print("error in file "+i)
