#!/usr/bin/env python
# -*- coding: cp1251 -*-

import os
import sys
import glob
import shutil
import stat


def f(func, path, exc_info):
    print "chmod for", path
    os.chmod(path, stat.S_IWUSR)

for root, dirs, files in os.walk("."):
    if ".svn" in dirs:
        nm = os.path.join(root, ".svn")
        shutil.rmtree(nm, onerror=f)
        print nm
