# -*- coding: utf-8 -*-
u"""
чтение и запись примитивных типов
"""
import os
import codecs


def sv_json(obj, file_name):
    import json
    with open(file_name, "w", encoding="utf-8") as f:
        json.dump(obj, f, indent=2)


def sv_yaml(obj, file_name):
    import yaml
    with codecs.open(file_name, "w", encoding="utf-8") as f:
        yaml.safe_dump(obj, f, allow_unicode=1)


def sv_ini(obj, file_name):
    import ConfigParser
    cfg = ConfigParser.ConfigParser()
    for k, v in obj.iteritems():
        cfg.add_section(k)
        for k1, v1 in v.iteritems():
            cfg.set(k, k1, v1)
    with codecs.open(file_name, "w", encoding="utf-8") as f:
        cfg.write(f)


def sv_pkl(obj, file_name):
    import cPickle
    with open(file_name, "wb") as f:
        cPickle.dump(obj, f, 2)


def sv_msg(obj, file_name):
    import msgpack
    with open(file_name, "wb") as f:
        f.write(msgpack.packb(obj))


def sv_h5(obj, file_name):
    pass


def ld_json(file_name):
    import json
    with codecs.open(file_name, "r", encoding="utf-8") as f:
        data = json.load(f)
    return data


def ld_yaml(file_name):
    import yaml
    with codecs.open(file_name, "r", encoding="utf-8") as f:
        data = yaml.load(f)
    return data


def ld_ini(file_name):
    import ConfigParser
    cfg = ConfigParser.ConfigParser()
    with codecs.open(file_name, "r", encoding="utf-8") as f:
        cfg.readfp(f)
    return cfg._sections


def ld_pkl(file_name):
    import cPickle
    with open(file_name, "rb") as f:
        data = cPickle.load(f)
    return data


def ld_msg(file_name):
    import msgpack
    with open(file_name, "rb") as f:
        data = msgpack.unpackb(f.read())
    return data


def ld_h5(file_name):
    pass

ld_map = {
    ".json": ld_json,
    ".yaml": ld_yaml,
    ".ini": ld_ini,
    ".pkl": ld_pkl,
    ".msg": ld_msg,
    ".h5": ld_h5
}

sv_map = {
    ".json": sv_json,
    ".yaml": sv_yaml,
    ".ini": sv_ini,
    ".pkl": sv_pkl,
    ".msg": sv_msg,
    ".h5": sv_h5
}


def sv(obj, file_name):
    ext = os.path.splitext(file_name)[1]
    sv_map[ext](obj, file_name)


def ld(file_name):
    ext = os.path.splitext(file_name)[1]
    return ld_map[ext](file_name)


def test():
    data = {"a": 1, "b": "long"}
    sv_json(data, "a.json")
    b = ld_json("a.json")
    sv_yaml(data, "a.yaml")
    b = ld_yaml("a.yaml")
    sv_pkl(data, "a.pkl")
    b = ld_pkl("a.pkl")
    sv_msg(data, "a.msg")
    b = ld_msg("a.msg")
    data1 = {"a": {"x": 3, "y": "eng"}, "b": {"x": 3, "y": 4}}
    obj = data1
    sv_ini(data1, "a.ini")
    b = ld_ini("a.ini")
