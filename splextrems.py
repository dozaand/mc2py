#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""����� ������������� ����� �� ������� � � ��������"""

#-------------------------------------------------------------------------------
# Author:      Andrey A. Semenov
# Created:     15.12.2010
# Copyright:   (c) and 2010
# Licence:     GPLv3
#-------------------------------------------------------------------------

import numpy as np
import scipy as sp
from scipy import interpolate


def SplExtremums(spl):
    u"""����� ���� ���������� �������"""
    vx = spl.get_knots()
    der = [spl.derivatives(x)[1] for x in vx]
    s = interpolate.InterpolatedUnivariateSpline(vx, der)
    xext = list(s.roots())
    if xext[0] != vx[0]:
        xext.insert(vx[0], 0)
    if xext[-1] != vx[-1]:
        xext.append(vx[-1])
    return [(x, float(spl(x))) for x in xext]


def SplMin(spl):
    u"""����� �������� �� �������"""
    return min(SplExtremums(spl), key=lambda x: x[1])


def SplMax(spl):
    u"""����� ��������� �� �������"""
    return max(SplExtremums(spl), key=lambda x: x[1])


def TableMax(vx, vy):
    u"""����� ��������� ��� ������� ������������� �� ������� ������� x_{max},y_{max}"""
    return SplMax(interpolate.InterpolatedUnivariateSpline(vx, vy))


def TableMin(vx, vy):
    u"""����� �������� ��� ������� ������������� �� ������� ������� x_{max},y_{max}"""
    return SplMin(interpolate.InterpolatedUnivariateSpline(vx, vy))


def Sabs(v, eps):
    return eps**2*len(v)


def Sdrel(v, eps):
    delta = np.max(v)-np.min(v)
    return (eps*delta)**2*len(v)


def Srel(v, eps):
    delta = np.max(np.abs(v))
    return (eps*delta)**2*len(v)
