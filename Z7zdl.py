#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
������ ���������� ��� ������ 7zip
"""

import subprocess as sp
import os
import datetime
import shutil
import glob
import mc2py.util
import cPickle
import mc2py.fold
import datetime as dt
import argparse
import time


def DirSize(nm):
    u"""return total directory files size"""
    s = 0
    for root, dirs, files in os.walk(nm):
        for fil in files:
            s += os.path.getsize(os.path.join(root, fil))
    return s


class Z7Arh(object):
    u"""��������� ������ """
    def __init__(self, fil, bufsize=1024, id='', exc=[]):
        u""" id - �������� ���� ������
            exc - ������ ��� ���� ����� (� ��������������) � ����� ������"""
        self.bufsize = bufsize
        self.storageName = os.path.abspath(fil)
        self.storagePath = os.path.split(self.storageName)[0]
        self.rootName = os.path.split(self.storageName)[1].split('.')[0]
        self.extractPath = r'D:\a'
        self.id = id
        self.exc = exc
        self._open()

    def _GetArhList(self, min, max, ident=""):
        u"""��������� ������ ������ �� �������� �����"""
        pass

    def append(self, data, ident="", t=None):
        u"""���������� ������ � ����������
        ident - �������� ����� ������ ����� ��������� ����� a/pp/25 ����� ����� ������ � ����� ����������
        data- ������
        t- ����� ���������� � ���� datetime"""
        if not t:
            t = datetime.datetime.now()
        if not os.path.exists(self.storageName):
            os.makedirs(self.storageName)
        cdir = os.path.join(self.storageName, ident)
        cdir = os.path.join(cdir, t.strftime(r"%Y\%m\%d"))
        if not os.path.exists(cdir):
            os.makedirs(cdir)
        targetFileName = os.path.join(cdir, t.strftime("%H%M%S"))
        with open(targetFileName, "wb") as f:
            cPickle.dump(data, f, 2)

    def flush(self):
        u"""������� ������ � �����"""
        cmd = "7z a {dirnm} -mmt -m7=LZMA2 -ms=e{buf}k {dirnm}".format(
            dirnm=self.storageName, buf=self.bufsize)
        sp.check_call(cmd, shell=1)
        for i in glob.glob(self.storageName+"/*"):
            shutil.rmtree(i)

    def _open(self):
        self.direct = []
        if self.id != '':
            self.id += '\\'
        cl = sp.Popen(r'7z l {dirnm} {root}\{ident}????'.format(
            dirnm=self.storageName, ident=self.id, root=self.rootName), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
        _list = cl.communicate()
        _list = _list[0].split('\n')[16:-3]
        for el in _list:
            if '....A' in el:
                date = el.split()[-1].split('\\')[-4:]
                h = date[-1][:2]
                m = date[-1][2:4]
                s = date[-1][4:]
                date[-1] = h
                date.append(m)
                date.append(s)
                self.direct.append(dt.datetime(*map(int, date)))
        self.direct.sort()

    def keys(self, min, max):
        u"""��������� ������ ������ (����� ����� �� �������) ����������� � ��������� ������ min,max"""
        keys = []
        i1 = 0
        i2 = 0
        step = 0
        while i1 == 0:
            if min <= self.direct[step]:
                i1 = 1
                begin = self.direct[step]
            step += 1
        while i2 == 0:
            if max <= self.direct[step]:
                i2 = 1
                end = self.direct[step]
            if step == len(self.direct)-1:
                end = self.direct[-1]
            step += 1
        begin1 = self.direct.index(begin)
        end1 = self.direct.index(end)
        for el in self.direct[begin1:end1]:
            keys.append(el)
        return keys

    def values(self, min, max):
        u"""��������� ������ �������� (����� ����� �� �������) ����������� � ��������� ������ min,max"""
        values = []
        keys = self.keys(min, max)
        cmd = '7z x {dirnm} -o{dirout} '.format(
            dirnm=self.storageName, dirout=self.extractPath)
        for ind in keys:
            nm = ind.strftime(r"%Y\%m\%d\%H%M%S")
            cmd += "{root}\{ident}{nm} ".format(
                nm=nm, ident=self.id, root=self.rootName)
            if len(cmd) > 5000:
                # cmd+=" {out} -r -y".format(out=out)
                cmd += "-r -y"
                sp.check_call(cmd, shell=1)
                cmd = '7z x {dirnm} -o{dirout} '.format(
                    dirnm=self.storageName, dirout=self.extractPath)
        cmd += "-r -y"
        sp.check_call(cmd, shell=1)
        os.chdir(self.extractPath)
        files = list(glob.glob("*/*/*/*/*/*/*/*/*"))
        for el in files:
            f = open(el, 'rb')
            values.append(cPickle.load(f))
        return values

    def items(self, min, max):
        u"""��������� ������ ������ � �������� (����� ����� �� �������) ����������� � ��������� ������ min,max"""
        keys = self.keys(min, max)
        values = self.values(min, max)
        items = {}
        for ind, val in enumerate(keys):
            items[val] = values[ind]

    def minKey(self):
        u"""����������� ��������� ������ �������"""
        return self.direct[0]

    def maxKey(self):
        u"""������������ ��������� ������ �������"""
        return self.direct[-1]


# aa=Z7Arh('asd.7z',exc=[1,2])
# begin=aa.direct[0]
# end=aa.direct[10]
# g=aa.values(begin,end)

KB = 1024
MB = KB*KB


def Z7LogDirFlush(dirnm, arhnm=None, buf_size=MB*16):
    u"""���������� ������ ���������� � �����"""
    if arhnm:
        assert(arhnm != dirnm)
        lgdir = arhnm
    else:
        lgdir = "log_"+dirnm
    cmd = "7z a %s -mmt -ms=e%sk %s" % (dirnm, buf_size/KB, lgdir)
    sp.check_call(cmd, shell=1)
    for i in glob.glob(lgdir+"/*"):
        shutil.rmtree(i)


def Z7LogDir(dirnm, arhnm=None, time=None, bufsiz=MB*16, flush=0):
    u"""
������� ������ ������ �������� � ���������� � ������ ��� ������ ������
� ����� ������ ����� ����� - ������ 7zip
time - ��������� ����� ������
bufsiz - ������ ���������� ������� ������������ �������� - �� ��������� 16"""
    buf_size = bufsiz
    if arhnm:
        assert(arhnm != dirnm)
        lgdir = arhnm
    else:
        lgdir = "log_"+dirnm
    if not os.path.exists(lgdir):
        os.mkdir(lgdir)
    if time:
        t = time
    else:
        t = datetime.datetime.now()
    cdir = os.path.join(lgdir, t.strftime(r"%Y\%m\%d"))
    if not os.path.exists(cdir):
        os.makedirs(cdir)
    targetdir = os.path.join(cdir, t.strftime("%H%M%S"))
    shutil.copytree(dirnm, targetdir)
    siz = DirSize(lgdir)
    if siz > buf_size or flush:
        Z7LogDirFlush(dirnm, arhnm, buf_size)


def Z7repack(root, dest="dirtolog", exclude="fii_dkev fir_dkev fiv_dkev W_dker fizr.bl fizv.bl baz".split()):
    u"""����������� ������ �� rar ��� arj ������ ����� � 7z ����� ������� Z7Arh
    dest - ��� ��������� ����������
    exclude �������� ������ �� ���������� ��������"""
    arhnm = dest
    dest = "log_"+arhnm
    if os.path.exists(dest):
        shutil.rmtree(dest)
    l = len(root)
    j = 0
    for fil in glob.glob(root+"/*.dat/*.rar")+glob.glob(root+"/*.dat/*.arj"):
        tdstr = fil[l+1:-4]
        ext = fil[-3:].lower()
        t = datetime.datetime.strptime(tdstr, "%y%m%d.dat\\%H%M%S")
        if not os.path.exists(dest):
            os.makedirs(dest)
        if ext == "rar":
            sp.check_call("rar e -y {arh} {dest}".format(
                arh=fil, dest=dest), shell=1)
        elif ext == "arj":
            sp.check_call("arj e -y {arh} {dest}".format(
                arh=fil, dest=dest), shell=1)
        for nm in exclude:
            rmname = os.path.join(dest, nm)
            if os.path.exists(rmname):
                os.remove(rmname)
        Z7LogDir(dest, arhnm, time=t)
        shutil.rmtree(dest)
        print "done ", fil
        j += 1
    Z7LogDirFlush(dest, arhnm)


def Extract(arh, datefrom, dateto):
    pass


def Timing():
    t0 = time.clock()
    sp.check_call("rar e -y tmp/090131.dat/000908.rar  aaaaaaa", shell=1)
    t1 = time.clock()
    print "rar 1 srez unpack time :", t1-t0

    t0 = time.clock()
    sp.check_call(
        "7z e -y  -oaaaaaaa dirtolog.7z log_dirtolog/2009/02/02/*.*", shell=1)
    t1 = time.clock()
    print "7z 1 srez unpack time :", t1-t0

    t0 = time.clock()
    for nm in glob.glob("tmp/090131.dat/*.rar"):
        sp.check_call("rar x -y {nm} aaaaaaa".format(nm=nm), shell=1)
    t1 = time.clock()
    print "rar 1 day unpack time :", t1-t0

    t0 = time.clock()
    sp.check_call(
        "7z e -y -so -oaaaaaaa dirtolog.7z log_dirtolog/2009/01/31/*/*.* >aaa", shell=1)
    t1 = time.clock()
    print "7z 1 day unpack time :", t1-t0

# root=r"tmp"
# Z7repack(root)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog=u"Z7zdl.py")  # ,help=u"���������� ���������� � 7z �����"
    parser.add_argument(
        "-r", help=u"����������� ��������� ������� � ���������� dir/090131.dat/000908.rar")
    parser.add_argument("--add", "-a", help=u"���������� ����� ����������")
    parser.add_argument(
        "-t", help=u"����� ��� ������� �������������� ���������� ���� �� ������ - ������� �����")
    parser.add_argument(
        "-f", default="%y-%m-%d %H:%M:%S", help=u"������ ���� � �������")
    parser.add_argument("arh", nargs=1, help=u"��� ������")
    parser.add_argument("dir", nargs=1, help=u"��� ������������� ����������")
    args = parser.parse_args()
    if args.add:
        if args.t:
            time = datetime.datetime.strptime(args.t, args.f)
        else:
            time = datetime.datetime.now()
        Z7LogDir(args.dir, args.arh, time=time)
    if args.r:
        Z7repack(root)
