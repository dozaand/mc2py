##!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
functions for package management
"""

import nose
from nose import with_setup
from py_pkg_config import PackInfo, _string_to_list, _list_to_string
import os

def setup_func():
    data="""
part1 :
    depends : [part2, part3]
    some_sclar_key : sk1
    some_list_key : [list_item1, list_item2]
    kk :
        some_sclar_key : b
part2 :
    depends : [part4, part5]
    some_sclar_key : sk2
    some_list_key : [list_item1, list_item3]
part3 :
    some_sclar_key : sk3
    some_list_key : [list_item1, list_item2]
part4 :
    some_sclar_key : sk4
part5 :
    some_sclar_key : sk4
part6 :
    some_sclar_key : sk4
"""
    with open("test_file.yaml","w") as f:
        f.write(data)

def teardown_func():
    os.remove("test_file.yaml")

def setup_func_u():
    setup_func()
    data = """
part5 :
    some_sclar_key : sk4_ex
    v : sk5_ex
"""
    with open("test_file_u.yaml","w") as f:
        f.write(data)

def teardown_func_u():
    teardown_func()
    os.remove("test_file_u.yaml")


@with_setup(setup_func, teardown_func)
def test_packlist():
    db = PackInfo("test_file.yaml")
    lst = set()
    db.packlist(lst, "part1")
    assert(lst == set("part1 part2 part3 part4 part5".split()))

@with_setup(setup_func_u, teardown_func_u)
def test_update():
    db = PackInfo("test_file.yaml")
    db1 = PackInfo("test_file_u.yaml")
    db.update(db1)
    key = db["part1/v"]
    assert("sk5_ex" in key)
    key = db["part1/some_sclar_key"]
    assert("sk4_ex" in key)

def test_string_to_list():
    dkt={"a":"a", "b":{"c":"d"}}
    _string_to_list(dkt)
    assert(dkt["a"][0] == "a")
    assert(dkt["b"]["c"][0] == "d")

def test_list_to_string():
    dkt={"a":"a", "b":{"c":"d"}}
    _string_to_list(dkt)
    _list_to_string(dkt)
    assert(dkt["a"] == "a")
    assert(dkt["b"]["c"] == "d")

@with_setup(setup_func, teardown_func)
def test_getopt():
    db = PackInfo("test_file.yaml")
    lst = db.getopt(["part1","part2"],"some_list_key")
    assert(lst == ['list_item1', 'list_item2', 'list_item3'])

#test_getopt()
#setup_func()
#test_packlist()
#teardown_func()
#result = nose.run()
if __name__ == '__main__':
    nose.runmodule()

