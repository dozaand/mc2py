#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
conversion simple gmsh mesh of first order to h5 file
"""
import numpy as np
import h5py
import argparse
import os

msh_fun = {}


def fskip(f, key, res):
    "пропуск незнакомых пар "
    endkey = key.replace("$", "$End")
    while(1):
        lin = f.readline().strip()
        if not lin:
            break
        if lin == endkey:
            return None
    raise Exception(endkey + " not found, end of file")


def app(f, key, res):
    "применяем операцию"
    if key in msh_fun:
        msh_fun[key](f, key, res)
    fskip(f, key, res)


def read_fmt(f, key, res):
    res[key] = f.readline().strip()
msh_fun["$MeshFormat"] = read_fmt


def read_names(f, key, res):
    nphis = int(f.readline().strip())
    phis = {}
    for i in range(nphis):
        i1, i2, nm = (f.readline().strip()).split()
        phis[nm[1:-1]] = int(i2) - 1
    res[key] = phis
msh_fun["$PhysicalNames"] = read_names


def read_nodes(f, key, res):
    nnode = int(f.readline().strip())
    nodes = np.fromfile(f, dtype='f', sep=' ',
                        count=nnode * 4).reshape((nnode, 4))
    res[key] = nodes[:, (1, 2)]
msh_fun["$Nodes"] = read_nodes


def read_elements(f, key, res):
    nelements = int(f.readline().strip())
    elments = np.fromfile(f, dtype='i', sep=' ',
                          count=nelements * 8).reshape((nelements, 8))
    if not np.all(elments[:, 1] == 2):
        raise Exception("invalid mesh type is not 2")
    res[key] = elments
msh_fun["$Elements"] = read_elements


def msh2hdf5(in_file_name, out_file_name):
    """собственно конвертация в hdf5"""
    res = {}
    with open(in_file_name, "r") as f:
        while(f):
            k = f.readline().strip()
            app(f, k, res)
            if len(res) == 4:
                break
    with h5py.File(out_file_name, "w") as f5:
        f5["nodes"] = res["$Nodes"]
        elments = res["$Elements"]
        elt = elments[:, (5, 6, 7)] - 1
        etypes = elments[:, 3] - 1
        g = f5.create_group("grp")
        names = res["$PhysicalNames"]
        for name in names:
            ind = (etypes == names[name])
            g[name] = elt[ind]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="convert gmsh msh file to h5")
    parser.add_argument("-o", "--out", default=u"", help=u"output file name")
    parser.add_argument("file", help=u"input msh file name")
    args = parser.parse_args()
    if args.out:
        out = args.out
    else:
        out = os.path.splitext(args.file)[0] + ".h5"
    msh2hdf5(args.file, out)
