import os.path
import __builtin__


def ModuleRelativePath(mod, nm):
    u"""
>>> print ModuleRelativePath(r"C:\PROJECTS\mc2py\mc2lib.eee","aaa.dat")
C:\PROJECTS\mc2py\aaa.dat
"""
    return os.path.join(os.path.dirname(mod), nm)
__builtin__.__dict__["ModuleRelativePath"] = ModuleRelativePath
