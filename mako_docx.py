#!/usr/bin/env python
# -*- coding: utf-8 -*-

from docx import Document
import docx.enum.text as tu
import mako
from mako.lookup import TemplateLookup
from mako.template import Template
import argparse

ipict=0
itable=0
ichapt=0
isect=0
isubsect=0
joined_data={}

def figure(label=None,caption=None):
    global ipict
    global ichapt
    global isect
    global isubsect
    ipict+=1
    _ichapt = unicode(ichapt) if ichapt else ""
    _isect = u"."+unicode(isect) if isect else ""
    _isubsect = u"."+unicode(isubsect) if isubsect else ""
#    return u"Рисунок "+unicode(ipict)
    return u"Рисунок "+_ichapt+_isect+_isubsect+"."+unicode(ipict)

def table(label=None,caption=None):
    global itable
    global ichapt
    global isect
    global isubsect
    itable+=1
    _ichapt = unicode(ichapt) if ichapt else ""
    _isect = u"."+unicode(isect) if isect else ""
    _isubsect = u"."+unicode(isubsect) if isubsect else ""
#    return u"Рисунок "+unicode(ipict)
    return u"Таблица "+_ichapt+_isect+_isubsect+"."+unicode(itable)

def chapter(text=None,number=None):
    global ipict
    global ichapt
    global isect
    global isubsect
    if number:
        ichapt=number-1
    isect=0
    ipict=0
    isubsect=0
    ichapt+=1
    if text:
       return u"{num} {text}".format(num=ichapt,text=text)
    else:
       return u""
#    return u""

def section(text=None,number=None):
    global ipict
    global ichapt
    global isect
    global isubsect
    if number:
        isect=number-1
    isubsect=0
    isect+=1
    return u""

def subsection(text=None,number=None):
    global ipict
    global ichapt
    global isect
    global isubsect
    if number:
        isubsect=number-1
    isubsect+=1
    return u""

def make_data_dict(data_file):
    global joined_data
    if(data_file):
        with codecs.open(data_file,"r",encoding='utf-8') as f:
            data =yaml.load(f)
    else:
        data={}
    joined_data=dict(chapter=chapter,section=section,subsection=subsection,figure=figure,
                     table=table,ipict=ipict,itable=itable,ichapt=ichapt,isect=isect,isubsect=isubsect)
    joined_data.update(data)



def convert(in_file, out_file, data_file):
    make_data_dict(data_file)
    doc = Document(in_file)
    for par in doc.paragraphs:
        ptext=par.text
        if "$" in ptext:
            par.text=Template(ptext).render(**joined_data)
    doc.save(out_file)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="word_mako",description="substitute data to word_mako infile outfile")
    parser.add_argument("-d","--data",default="",help=u"dile with data")
    parser.add_argument("-o","--output",help=u"output file")
    parser.add_argument("file",help=u"input file")
    args = parser.parse_args()
    convert(args.file,args.output,args.data)

