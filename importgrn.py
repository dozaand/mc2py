#!/usr/bin/env python3

import numpy as np
import yaml
#from ruamel.yaml import YAML
#yaml = YAML(typ="safe")
import os
from pathlib import Path
import io
import f90nml
from datetime import datetime, timedelta

sec_per_day = 3600. * 24

def load_grn_file(nm: Path) -> list:
    """считываем один grn файл 
    в результате возвращаем таблицу (см xipibrn строка 395)
    Teff power(N/Nnom%)P(ata)  Hsuz (cm)  Tbx1(K) Gin1(kg/c)  Tbx2(K) Gin2(kg/c)  Tbx3(K) Gin3(kg/c)  Tbx4(K) Gin4(kg/c)\n";
  Teff    Hsuz[cm] Cb[g/kg]  W[МВт]    Tin[C]   ngrp   G[Tonn/h]  grp Hsuz 
 154.22,  332.4,   4.87,     3225.0e3, 295.825, 12,    86084,     13*0.0,
    """
    print(nm)
    with open(nm,"r") as f:
        data = f.read()

    prs = f"""&a
    {data}
    /"""

    nml = f90nml.read(io.StringIO(prs))

    year = nml["a"]["nyear"]
    month = nml["a"]["nmonth"]
    t0 = datetime(year, month, 1)
    # время плавающе в днях поэтому плавающе превращаю в timedelta
    idat = nml["a"]["idat"]
    if not type(idat) is list:
        idat = [idat]
    t = [t0+timedelta(seconds=(i-1)*sec_per_day) for i in idat]
    dat = np.array(nml["a"]["ef"]).reshape((-1, 20)).tolist()
    if len(t) != len(dat):
        raise Exception(f"number of dates {len(t)} != number of rows len(dat) in file {nm}")
    for ti,di in zip(t,dat):
        di.insert(0,ti)
    return dat


def load_grn_dir(nm: Path) -> list:
    """загрузка всех grn файлов из текущей директории"""
    res=[]
    for i in nm.glob("grn????"):
        a=load_grn_file(i)
        res.extend(a)
    res.sort(key=lambda x:x[0])
    return res


class Tidx:
    def __init__(self, nm):
        self.keys = nm.split()
        for i,n in enumerate(self.keys):
            setattr(self,n,i)
        self.num = len(self.keys)


def save_yaml(file_name: Path, data: list,shu:list):
    dat_i = np.array(data)
    i_in = Tidx("t teff H0 Cb W Tin iH0 G iH1 H1")
    i_out = Tidx("t teff Cb W Tin G H")
    dat_o = np.zeros((len(dat_i), i_out.num), dtype=np.object)
    times = dat_i[:, i_in.t]
    t0 = times[0]
    t = [(i-t0).total_seconds()/(3600*24) for i in times]
    dat_o[:, i_out.t] = t
    dat_o[:, i_out.teff] = dat_i[:, i_in.teff]
    dat_o[:, i_out.W] = dat_i[:, i_in.W]
    dat_o[:, i_out.Cb] = dat_i[:, i_in.Cb]
    dat_o[:, i_out.Tin] = dat_i[:, i_in.Tin]
    dat_o[:, i_out.G] = dat_i[:, i_in.G]

    # магия сузов
    iH1 = int(max(dat_i[:, i_in.iH1]))
    iH0 = int(max(dat_i[:, i_in.iH0]))
    N = len(times)

    if iH1 > 0:
        iH = [iH0,iH1]
        H = np.c_[dat_i[:, i_in.H0],dat_i[:, i_in.H1]]
    else:
        iH = [iH0]
        H = np.c_[dat_i[:, i_in.H0]]

    for i in range(N):
        dat_o[i, i_out.H] = H[i].tolist()

    # магия форматированного вывода
    res = f"t0: {t0}\niH: {iH}\n"
    res += f"cols: {yaml.dump(i_out.keys,default_flow_style=True)}d:\n"
    res += "\n".join([f"  - {i}" for i in dat_o.tolist()])
    res += f"\nshu: {shu}"
    with open(file_name, "w") as f:
        f.write(res)

# with open(file_name, "r") as f:
#     ddd = yaml.safe_load(f)

def convert_all(xipi_name: Path):
    for i in xipi_name.glob("b??/k??/grn"):
        print(i)
        data = load_grn_dir(i)
        try:
            shu = f90nml.read(i.parent/"shu.360")["burnf"]["KKB"]
        except:
            shu = []
        save_yaml(i.parent/"grn.yaml", data, shu)
if __name__ == "__main__":
	convert_all(Path("."))


