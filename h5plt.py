#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""��������� ������� �������� ������ ������ �� hdf5 ��� ������ matplotlib
��������� ������ � �������� ����������� �����
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import h5py
import argparse


# 3

class xarange(object):
    def __init__(self, x0, x1, dt):
        self.x0 = x0
        self.x1 = x1
        self.dt = dt

    def __getitem__(self, i):
        if isinstance(i, slice):
            dt = self.dt
            return np.arange(i.start*dt, i.stop*dt, i.step*dt)
        if(i < 0):
            return self.x1+self.dt*(i+1)
        else:
            return self.x0+self.dt*i

    def __len__(self):
        return int(self.x1-self.x0)/self.dt


def ZoomX(ax, val):
    (mi, ma) = ax.get_xlim()
    median = (ma+mi)/2
    delta = ((ma-mi)*val)/2
    (mi, ma) = (median-delta, median+delta)
    ax.set_xlim(mi, ma)


def ZoomY(ax, val):
    (mi, ma) = ax.get_ylim()
    median = (ma+mi)/2
    delta = ((ma-mi)*val)/2
    (mi, ma) = (median-delta, median+delta)
    ax.set_ylim(mi, ma)


def ShiftX(ax, val):
    (mi, ma) = ax.get_xlim()
    sh = (ma-mi)*val
    ax.set_xlim(mi+sh, ma+sh)


def ShiftY(ax, val):
    (mi, ma) = ax.get_ylim()
    sh = (ma-mi)*val
    ax.set_ylim(mi+sh, ma+sh)


class ZoomMixinObj(object):
    def __init__(self, ax=None, factor=2, shift=0.3):
        self.factor = factor
        self.shift = shift
        self.ax = ax
        self.xlim = ax.get_xlim()
        self.ylim = ax.get_ylim()

    def OnKey(self, event):
    #        print 'you pressed', event.key, event.xdata, event.ydata
        ax = self.ax
        if event.key == '+':
            ZoomX(ax, 1./self.factor)
            ZoomY(ax, 1./self.factor)
        elif event.key == '-':
            ZoomX(ax, self.factor)
            ZoomY(ax, self.factor)
        elif event.key == 'a':
            ZoomX(ax, 1./self.factor)
        elif event.key == 'd':
            ZoomX(ax, self.factor)
        elif event.key == 'w':
            ZoomY(ax, 1./self.factor)
        elif event.key == 'x':
            ZoomY(ax, self.factor)
        elif event.key in 'h':
            if hasattr(self, "xlim") and hasattr(self, "ylim"):
                ax.set_xlim(ax, self.xlim)
                ax.set_ylim(ax, self.ylim)
        elif event.key == '4':
            ShiftX(ax, -self.shift)
        elif event.key == '6':
            ShiftX(ax, self.shift)
        elif event.key == 'up' or event.key == '8':
            ShiftY(ax, self.shift)
        elif event.key == 'down' or event.key == '2':
            ShiftY(ax, -self.shift)

        canvas = event.canvas
        canvas.draw()


class SZoomMixinObj(ZoomMixinObj):
    """slise mixin"""
    def __init__(self, ax, args, factor=2, shift=0.3):
        super(self).__init__(ax, factor, shift)

    def OnKey(self, event):
        ax = self.ax


class TZoomPlot(object):
    def __init__(self, xdata, ydata, npnt=1024, factor=2, shift=0.3):
#        assert(len(xdata)==len(ydata))
        assert(1 < factor)
        assert(npnt > 1)
        self.npnt = npnt
        self.iBeg = 0
        self.iEnd = len(ydata)
        self.step = self.Step()
        self.xdata = xdata

        self.ydata = ydata
        self.factor = factor
        self.shift = shift

        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        xx = xdata[self.iBeg:self.iEnd:self.step]
        yy = ydata[self.iBeg:self.iEnd:self.step]
        (line,) = self.ax.plot(xx, yy, "+-")
        self.xlim = self.ax.get_xlim()
        self.kx = float(len(ydata))/(xdata[
            0]-xdata[-1])  # ������� ���������� � �������
        self.ylim = self.ax.get_ylim()
        self.cid1 = self.fig.canvas.mpl_connect(
            'button_press_event', self.OnClick)
        self.cid2 = self.fig.canvas.mpl_connect('key_press_event', self.OnKey)
        self.line = line

    def close(self):
        self.fig.canvas.mpl_disconnect(self.cid1)
        self.fig.canvas.mpl_disconnect(self.cid2)
        self.fig.clf()

    def show(self):
        plt.show()

    def Step(self):
        """calculate step in points"""
        step = int((self.iEnd-self.iBeg)/self.npnt)
        if step == 0:
            step = 1
        self.step = step
        return step

    def RePlot(self):
        self.AdjustRange()
        yy = data[self.iBeg:self.iEnd:self.step]
        xx = xdata[self.iBeg:self.iEnd:self.step]
        self.line.set_data(xx, yy)
        self.fig.canvas.draw()

    def OnClick(self, event):
        print 'button=%d, x=%d, y=%d, xdata=%f, ydata=%f' % (
            event.button, event.x, event.y, event.xdata, event.ydata)

    def ZoomX(self, val):
        ZoomX(self.ax, val)

    def ZoomY(self, val):
        ZoomY(self.ax, val)

    def ShiftX(self, val):
        ShiftX(self.ax, val)

    def ShiftY(self, val):
        ShiftY(self.ax, val)

    def AdjustIndex(self, i0, bound, isUpper):
        lmax = len(self.ydata)
        i = min(max(0, i0), lmax-1)
        f = self.xdata[i]
        control = 0
        while 1:
            control += 1
            if control > 1000:
                raise RuntimeError("fail AdjustIndex")
            idelta = int((f-bound)*self.kx)
            if idelta == 0:
                if not isUpper:
                    i -= 5
                    if i < 0:
                        i = 0
                else:
                    i += 5
                    if i > lmax:
                        i = lmax
                return i
            i += idelta
            if i >= lmax:
                return lmax
            if i < 0:
                return 0
            f = self.xdata[i]

    def AdjustRange(self):
        (axmi, axma) = self.ax.get_xlim()
        self.iBeg = self.AdjustIndex(self.iBeg, axmi, 0)
        self.iEnd = self.AdjustIndex(self.iEnd, axma, 1)
        self.Step()

    def OnKey(self, event):
#        print 'you pressed', event.key, event.xdata, event.ydata
        if event.key == '+':
            self.ZoomX(1./self.factor)
            self.ZoomY(1./self.factor)
        elif event.key == '-':
            self.ZoomX(self.factor)
            self.ZoomY(self.factor)
        elif event.key == 'a':
            self.ZoomX(1./self.factor)
        elif event.key == 'd':
            self.ZoomX(self.factor)
        elif event.key == 'w':
            self.ZoomY(1./self.factor)
        elif event.key == 'x':
            self.ZoomY(self.factor)
        elif event.key in 'h':
            self.ax.set_xlim(self.xlim)
            self.ax.set_ylim(self.ylim)
            self.iBeg = 0
            self.iEnd = len(self.ydata)
            self.step = self.Step()
        elif event.key == 'left':
            self.ShiftX(-self.shift)
        elif event.key == 'right':
            self.ShiftX(self.shift)
        elif event.key == 'up':
            self.ShiftY(self.shift)
        elif event.key == 'down':
            self.ShiftY(-self.shift)

        self.RePlot()

##    step=lData/npnt

# xdata=np.linspace(0,1,100000)
# data=np.sin(1000*xdata)*np.exp(-2*xdata)

# f5=h5py.File("Arh3b_2.h5")
# xdata=f5["t"]
# data=f5["RU/NAKZ"]
# f5.close()

f5 = h5py.File(r"D:\data\kalinindata\GARANT3\ARH\C02\c02.h5")
data = f5["RU/NAKZ"]
xdata = f5["t"]
# data=dict(data=f5,path="05091/ecg",slice="@,0")
# zp=TZoomPlot(xdata,data)


def zplot(*args):
    u"""
    add method for plotting hdf5 series
    """
    fig = plt.figure()
    plt.plot(*args)
    ax = plt.gca()
    zo = ZoomMixinObj(ax=ax)
    cid1 = fig.canvas.mpl_connect('key_press_event', zo.OnKey)
    plt.show()
#    fig.canvas.mpl_disconnect(cid1)


def szplot(*args, **kvarg):
    print args
    print kvarg
    fig = plt.figure()
    plt.plot(*args, **kvarg)
    ax = plt.gca()
    zo = SZoomMixinObj(ax, args)
    cid1 = fig.canvas.mpl_connect('key_press_event', zo.OnKey)
    plt.show()


szplot(xdata, data, "+-", npoint=15)

# a[(slice(0,3),15)]


# zp.show()

f5.close()

# if __name__ == '__main__':
##    parser = argparse.ArgumentParser(prog="showD",description="show data on kart")
##    parser.add_argument("-m","--method",default='k',help=u"method of data show [k,p,t]")
##    parser.add_argument("datadsc",nargs="*",help=u"input data: data file or file/pathon file")
##    parser.add_argument("-f","--fmt",help=u"set data format")
##    args = parser.parse_args()
##    res=map(ExtractData,args.datadsc)
##    for i in res:
##        if args.method=='k':
##            kshow(i,show=1,format=args.fmt)
##        elif args.method=='p':
##            plot(i);show()
##        elif args.method=='t':
##            print i
##        else:
##            raise Exception("undefined option %s" % args.method)
