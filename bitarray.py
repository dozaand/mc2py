
def int2buf(arr,fmt):
    a= ["{{:0{}b}}".format(f).format(i) for f,i in zip(fmt,arr)]
    a.reverse()
    literal="".join(a)
    clit=("0"*(8-len(literal)%8))+literal
    spl=[chr(int(clit[i:i+8],2)) for i in range(0,len(clit),8)]
    spl.reverse()
    asbuf="".join(spl)
    asint=int(literal,2)
    return {"buf":asbuf,"int":asint,"str":literal}

def buf2int(asbuf,fmt):
    asstring=("".join(["{:08b}".format(ord(i)) for i in asbuf][::-1]))
    ltot=len(asstring)
    csum=[0]
    for i in fmt:
        csum.append(csum[-1]+i)
    return [int(asstring[ltot-csum[i+1]:ltot-csum[i]],2) for i in range(len(fmt))]
