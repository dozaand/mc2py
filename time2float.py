#!/usr/bin/env python
# -*- coding: cp1251 -*-

"""������ � ����� ������ ������� �� ��������� �����"""

import sys
import os
import re
import datetime
from matplotlib.dates import num2date, date2num


def DateTime2float(f1, f2):
    u"""������������ ���� ���, ����� ���� ����� - ���������� �� ��������� ����� ��������� ��� ����� �� ������� ������ � � ������� ������"""
    with open(f1, "rb") as fi:
        data = fi.read()
    ch = re.compile(
        r"(?P<dtmyA>\d\d\d\d\-\d\d\-\d\d[ \t]+\d+:\d\d:\d\d)|(?P<dtmy>\d\d\d\d\.\d\d\.\d\d[ \t]+\d+:\d\d:\d\d)|(?P<dtA>\d\d\-\d\d\-\d\d\d\d +\d+:\d\d:\d\d)|(?P<dt>\d\d\.\d\d\.\d\d\d\d +\d+:\d\d:\d\d)|(?P<word>.)")
    with open(f2, "wb") as f:
        for match in ch.finditer(data):
            gd = match.groupdict()
            if gd["word"] != None:
                f.write(gd["word"])
            elif gd["dt"] != None:
                d = datetime.datetime.strptime(gd["dt"], "%d.%m.%Y %H:%M:%S")
                f.write(str(date2num(d)))
            elif gd["dtA"] != None:
                d = datetime.datetime.strptime(gd["dtA"], "%d-%m-%Y %H:%M:%S")
                f.write(str(date2num(d)))
            elif gd["dtmy"] != None:
                d = datetime.datetime.strptime(gd["dtmy"], "%Y.%m.%d %H:%M:%S")
                f.write(str(date2num(d)))
            elif gd["dtmyA"] != None:
                d = datetime.datetime.strptime(
                    gd["dtmyA"], "%Y-%m-%d %H:%M:%S")
                f.write(str(date2num(d)))

# DateTime2float("a.dat","a.dat.t")

if __name__ == '__main__':
    if len(sys.argv) == 3:
        DateTime2float(sys.argv[1], sys.argv[2])
