#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""
������������ ������ � hdf5
"""

import numpy as np
import h5py
import os


class h5abuf(object):
    """����������� ��� �������� � ����� h5, ������� � ������� � ������ ������ ���������"""
    def __init__(self, obj, bufsize=1024):
        self.obj = obj
        self.bufsize = bufsize

    def __enter__(self):
        self.buf = np.zeros((self.bufsize,)+self.obj.shape[
                            1:], dtype=self.obj.dtype)
        self.i = 0
        self.j = 0
        return self

    def __Exit__(self):
        self.__exit__(None, None, None)

    def __exit__(self, a, b, c):
        self.Flush()

    def Flush(self):
        self.obj[self.j:self.j+self.i, ...] = self.buf[:self.i, ...]
        self.j += self.i
        self.i = 0

    def append(self, v):
        if self.i == self.bufsize:
            self.Flush()
        else:
            if hasattr(v, 'shape'):
                self.buf[self.i, ...] = v
            else:
                self.buf[self.i] = v
            self.i += 1


def Rcreate(obj, buftree, arhtree, bufsize):
    u"""����������� �������� ������ ������"""
    for k, v in obj.iteritems():
        if hasattr(v, "iteritems"):
            bchild = buftree.create_group(k)
            achild = arhtree.create_group(k)
            Rcreate(v, bchild, achild, bufsize)
            print( k, v)
        else:
            shp = v.shape
            shpbuf = (bufsize,)+shp
            shparh = (0,)+shp
            typ = v.dtype
            arhtree.create_dataset(
                k, dtype=typ, compression="gzip", shape=shparh)
            buftree.create_dataset(k, dtype=typ, shape=shpbuf)


def RAppend(obj, buftree, arhtree, bufsize):
    u"""����������� �������� ������ ������"""
    for k, v in obj.iteritems():
        if hasattr(v, "iteritems"):
            bchild = buftree.create_group(k)
            achild = arhtree.create_group(k)
            Rcreate(v, bchild, achild, bufsize)
            print( k, v)
        else:
            shp = v.shape
            shpbuf = (bufsize,)+shp
            shparh = (0,)+shp
            typ = v.dtype
            arhtree.create_dataset(
                k, dtype=typ, compression="gzip", shape=shparh)
            buftree.create_dataset(k, dtype=typ, shape=shpbuf)


class h5Treebuf(object):
    u"""���� ����� �������� ����������� � ��������� ������ ��� ������ � �������� ������
    ����������� ������ -������������ ������� ��������� ������
    obj - ������� ������ ������� ����� �����������
    buftree - ����� ������ ���� �����������
    arhtree - ����� ��� ��������� �����������
    """
    def __init__(self, obj, buftree, arhtree, bufsize=1024):
        self.buftree = buftree
        self.arhtree = arhtree
        buftree.attrs["size"] = bufsize
        buftree.attrs["count"] = 0
        Rcreate(obj, buftree, arhtree, bufsize)

    def append(self, obj=None):
        pass

    def Flush(self):
        self.obj[self.j:self.j+self.i, ...] = self.buf[:self.i, ...]
        self.j += self.i
        self.i = 0


class H5membuf(object):
    u"""����������� ��� �������� � ����� h5, ������� � ������� � ������ ������ ���������"""
    def __init__(self, obj, f5, h5name, bufsize=1024,memsize=None,dtype=None):
        u"""
        obj - ������� ���� ��� ����� �������� � ������ (������ ���� ������ � shape � ���� ������� ����� �������� � h5 ����)
        f5,  ��������� �� ���� � �������
        h5name, ��� ������������������ ������ ����� h5  � ������� ����� ����������� ������
        bufsize - ����� ������ ������
        memsize - ������ � ������ ������ ������ - ���� ����� �� bufsize �� ������������
        """
        self.obj = obj
        if memsize:
            if hasattr(obj,"nbytes"):
                nbytes=obj.nbytes
            else:
                nbytes={float:8,int:4}[type(obj)]
            self.bufsize=max(1,memsize/nbytes)
        else:
            self.bufsize = bufsize
        self.f5 = f5
        self.h5name = h5name
        self.dtype=dtype
        self.compression="gzip"

    def __enter__(self):
        shparh = (self.bufsize,)+self.obj.shape
        empty_shape = (0,)+self.obj.shape
        if self.dtype:
            typ = self.dtype
        else:
            typ = self.obj.dtype
        self.buf = np.zeros(shparh, dtype=typ)
        self.i = 0
        if(not self.h5name in self.f5):
            self.data = self.f5.create_dataset(
                self.h5name, dtype=typ, compression=self.compression, shape=empty_shape, maxshape=(None,)+self.obj.shape)
            self.j = 0
        else:
            self.data = self.f5[self.h5name]
            if hasattr(self.data, 'shape'):
                self.j = self.data.shape[0]
            else:
                self.j = 0
        return self

    def Exit(self):
        self.__exit__(None, None, None)

    def __exit__(self, a, b, c):
        if a is None:
            self.Flush()
        else:
            raise

    def Flush(self):
        if hasattr(self.data, 'shape'):
            shp = self.data.shape[0]
        else:
            shp = 0
        if shp < self.j + self.i:
            self.data.resize(self.j + self.i, 0)
        self.data[self.j:self.j+self.i, ...] = self.buf[:self.i, ...]
        self.j += self.i
        self.i = 0

    def append(self, v):
        if self.i == self.bufsize:
            self.Flush()
        if hasattr(v, 'shape'):
            self.buf[self.i, ...] = v
        else:
            try:
                self.buf[self.i] = v
            except:
                print( "qq")
        self.i += 1

class H5memCircularBuffer(H5membuf):
    u"""����������� ��� �������� � ����� h5, �����������"""

    def __init__(self, obj, f5, h5name, bufsize=1024,nblock=10,memsize=None,dtype=None):
        u"""
        \param obj �������� ����, ��� ���� ���������
        \param f5  ��������� �� ���� � �������
        \param h5name, ��� ������������������ ������ ����� h5,  � ������� ����� ����������� ������
        \param bufsize  ����� ������ ������
        \param nblock ������ ������������ ������ (������������ ����� ������� � �����),
        \param memsize  ������ � ������ ������ ������ - ���� ����� �� bufsize �� ������������
        \param dtype ��� ������ ������ ���� ����� ��������� � ���� ������ ������� ���� (�������� float ������ double)
        """
        super(H5memCircularBuffer,self).__init__(obj, f5, h5name, bufsize,memsize,dtype)
        self.nblock=nblock
        self.start_index=0
        self._filled=0
        self.compression=None

    def Flush(self):
        if hasattr(self.data, 'shape'):
            shp = self.data.shape[0]
        else:
            shp = 0
        totLen = self.j + self.i
        if shp < totLen:
            if totLen > self.bufsize*self.nblock:
                self.j=0
                totLen=self.i
                self._filled=1
            else:
                self.data.resize(totLen, 0)
        self.data[self.j:totLen, ...] = self.buf[:self.i, ...]
        self.j += self.i
        if self._filled:
            self.start_index=totLen
        self.i = 0

    def reverce_index(self,i):
        u"""����� ������ � �����, ������� ������������� ������ ���������� i ����� �����
        0 - ������ ��������� ������
        1 - ������������� � ��� �����.
        """
        if self._filled:
            j=self.start_index-i-1
            if j<0:
                j+=len(self.data)
            if j<0:
                raise IndexError("aaa")
            return j
        else:
            j=len(self.data)-i
            if j<0:
                raise IndexError("aaa")
            return j

def Test_H5membuf():
    a = np.zeros(20)
    b = np.zeros((20, 30))
    f5 = h5py.File("aa.h5")
    with H5membuf(a, f5, "a", 10) as ba, H5membuf(b, f5, "b", 10) as bb:
        for i in range(25):
            a[0] = i+1
            b[...] = i+1
            ba.append(a)
            bb.append(b)
            print( i)
    f5.close()


def Test_H5membufCirc():
    a = np.zeros(19)
    with h5py.File("aa.h5") as f5, H5memCircularBuffer(a, f5, "a", 10, 2) as ba:
        j=1
        for i in range(6):
            a[0] = j
            ba.append(a)
            j+=1
        ba.Flush()
        res =  ba.data[:,0]
        assert(np.all(np.arange(1,len(res)+1,dtype='d') == res))
        for i in range(10):
            a[0] = j
            ba.append(a)
            j+=1
        ba.Flush()
        res =  ba.data[:,0]
        assert(np.all(np.arange(1,len(res)+1,dtype='d') == res))

        for i in range(20):
            a[0] = j
            j+=1
            ba.append(a)
        ba.Flush()
        res =  ba.data[:,0]
        ref=np.arange(26,11,-1,dtype='d')
        res=np.array([res[ba.reverce_index(i)] for i in range(20)])
        assert(np.all(ref==res))
    os.remove("aa.h5")





class H5structbuf(object):
    u"""����������� ��� �������� � ����� h5, ������� � ������� � ������ ������ ���������"""
    def __init__(self, obj, f5, h5name, bufsize=1024):
        u"""
        obj - ������� ���� ��� ����� �������� � ������ (������ ���� ������ � shape � ���� ������� ����� �������� � h5 ����)
        bufsize - ����� ������ ������
        memsize - ������ � ������ ������ ������ - ���� ����� �� bufsize �� ������������
        """
        self.obj = obj
        self.dtype = obj.dtype
        self.bufsize = bufsize
        self.f5 = f5
        self.h5name = h5name

    def __enter__(self):
        typ = self.dtype
        self.buf = np.zeros((self.bufsize,), dtype=typ)
        self.i = 0
        if(not self.h5name in self.f5):
            self.data = self.f5.create_dataset(
                self.h5name, dtype=typ, compression="gzip", shape=(0,), maxshape=(None,))
            self.j = 0
        else:
            self.data = self.f5[self.h5name]
            self.j = self.data.shape[0]
        return self

    def Exit(self):
        self.__exit__(None, None, None)

    def __exit__(self, a, b, c):
        if a is None:
            self.Flush()
        else:
            raise

    def Flush(self):
        shp = self.data.shape
        if shp[0] < self.j + self.i:
            self.data.resize(self.j + self.i, 0)
        self.data[self.j:self.j+self.i] = self.buf[:self.i]
        self.j += self.i
        self.i = 0

    def append(self, v):
        if self.i == self.bufsize:
            self.Flush()
        self.buf[self.i] = v
        self.i += 1


def Test_H5structmembuf():
    dt = np.dtype([
        ('datetime_detect', np.float64, 1),
        ('value', np.float64, 1),
        ("dsc",np.str_, 1024),
        ('level', np.int, 1),
        ('datetime_check', np.float64, 1),
        ('checked', np.int, 1),
        ('checked_user', np.str_, 40)
        ])

    obj = np.zeros(1,dtype=dt)[0]

    with h5py.File("aa.h5") as f5:
        with H5structbuf(obj, f5, "a", 10) as ba:
            for i in range(25):
                obj['level'] = i
                ba.append(obj)


if __name__ == '__main__':
    #Test_H5membuf()
    #Test_H5membufCirc()
    #Test_H5structmembuf()
    pass

