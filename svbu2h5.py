#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import numpy as np
import h5py
import struct
from collections import namedtuple
from ctypes import *
import copy
import pylab as plt
import json
from mc2py.svld import sv,ld
import datetime
import yaml

#os.chdir(r"D:\data\rta")
def ptst(obj):
    """печать структуры"""
    for i in obj._fields_:
        print(i[0],getattr(obj,i[0]))

RSIZE=0x1000;
RTHIST_LEN_INSTANCENAME     = 19       # Max length of partition name
RTHIST_LEN_DIR              = 63       # Max length of partition directory
RTHIST_LEN_PARTITIONNAME    = 19       # Max length of partition name
RTHIST_LEN_COMMENT          = 79       # Max length of partition comment
RTHIST_LEN_VERSION          = 7        # Max length of partition version
RTHIST_LEN_PVID             = 63       # Max length of a PV identifier
RTHIST_LEN_PVTEXT           = 63       # Max length of a PV text
RTHIST_LEN_CATTEXT          = 31       # Max length of a Category text
RTHIST_LEN_PVPARAMS         = 255      # Max length of PV params
RTHIST_ANA_BOUNDCOUNT       = 4        # Max number of bound pairs for an analog PV
RTHIST_ANA_ACCCOUNT         = 2        # Max number of Accumulation Values
RTHIST_LEN_TEXTVALUE        = 1024     # Max length of text values
RTHIST_PVNR_ILLEGAL   = -1       # Illegal internal PVNr
RTHIST_PVNR_UNDEFINED = -2       # Undefined PV Number

def ldf(file,fmt):
    """ загрузка бинарных значений по формату из файла"""
    s=struct.calcsize(fmt)
    return struct.unpack(fmt,file.read(s))

class SRtHistAnaConf2(Structure):
    _pack_ = 1
    _fields_ = [
        ("m_PVNr",c_long),  #     Internal HISTORIAN PVNr
        ("m_PVId",c_char*(RTHIST_LEN_PVID+1) ),  #     PVId (имя сигнала ккs)
        ("m_ValidSince",c_ulonglong),  #     Time since validity of configuration
        ("m_MinInterval",c_uint),  #     Minimum archivation interval [secs] (used for generation of cyclic data repetition)
        ("m_Reserved",c_int*2),  #     For future use
        ("m_AccTypes",c_bool*RTHIST_ANA_ACCCOUNT),  #     Analog value pre-accumulation &lt;I&gt;(membership in PAF-1/2)&lt;/I&gt;
        ("m_CategoryNr",c_short),  #     Category Number
        ("m_ProcCatNr",c_short),  #     Processing Category Number
        ("m_DimNr",c_short),  #     Dimension Number
        ("m_StorageFormat",c_byte),  #     Analog value storage format
        ("pad1",c_byte),  #     Analog value storage format
        ("pad2",c_byte),  #     Analog value storage format
        ("pad3",c_byte),  #     Analog value storage format
        ("m_StorageCycle",c_uint),  #     Analog value storage cycle [msec], 0 means "none"
        ("m_AcqCycle",c_uint),  #     Analog value acquisition cycle [msec], 0 means "use default"
        ("m_PVText",c_char*(RTHIST_LEN_PVTEXT+1)),  #     PV text from PLS configuration (описание сигнала)
        ("m_CategoryText",c_char*(RTHIST_LEN_CATTEXT+1)),  #     Category text
        ("m_PVParams",c_char*(RTHIST_LEN_PVPARAMS+1)),  #     Parameters separated by semicolon
        ("padl",c_long),  # ???
        ("m_BoundLow",c_double*RTHIST_ANA_BOUNDCOUNT),  #     Bounds low
        ("m_BoundHigh",c_double*RTHIST_ANA_BOUNDCOUNT),  #     Bounds high
        ("m_RangeLow",c_double),  #     Range low
        ("m_RangeHigh",c_double),  #     Range high
        ("m_RawRangeLow",c_int),  #     Raw Range low
        ("m_RawRangeHigh",c_int),  #     Raw Range high
        ("m_AcqDeadband",c_double),  #     Acquisition deadband (absolute)
        ("m_HistDeadband",c_double),  #     Historian deadband (absolute)
        ("m_CompressionMethod",c_byte),  #     Historian compression method
        ("m_Stepped",c_bool),  #     Show stepped (i.e. NOT interpolated)
        ("m_RoundDigits",c_short),  #     Round digits
        ("pad10",c_short),  #     Round digits
        ("pad11",c_short)  #     Round digits
        ]

class SRtHistBinRecord2(Structure):
    _pack_ = 1
    _fields_ = [
        ("m_PVNr",c_ulong),  #     Internal HISTORIAN PVNr
        ("m_PVType",c_byte),  #     PV type (for values see ERtHistPVType)
        ("m_BitNr",c_char),  #     Bit Number for Narx PVs
        ("m_UserNr",c_short),  #     User number
        ("m_Value",c_ushort),  #     Binary value
        ("m_HistState",c_ushort),  #     Historian status
        ("m_AnaValue",c_double),  #     Double value (for analog PV)
        ("m_FieldTime",c_ulonglong),  #     Field time
        ("m_PVState",c_longlong),  #     Status information
        ("m_Text",c_ulong),  #     Text information (IMalloc!) ! c_char_p
        ("m_UserText",c_ulong),  #     User text information ! c_char_p
        ("m_OldValue",c_ushort),  #     Old (=previous) binary value
        ("m_OldPVState",c_longlong)  #     Old (=previous) status information
        ]

class SRtHistBinConf2(Structure):
    _pack_ = 1
    _fields_ = [
        ( "m_PVNr", c_ulong),  # Internal HISTORIAN PVNr
        ( "m_PVId", c_char*(RTHIST_LEN_PVID + 1)),  # PVId
        ( "m_ValidSince", c_ulonglong),  # Time since validity of configuration
        ( "m_MinInterval", c_uint),  # Minimum archivation interval [secs] (used for generation of cyclic data repetition)
        ( "m_CategoryNr", c_short),  # Category number
        ( "m_ProcCatNr", c_short),  # Processing category number
        ( "m_NaryCatNr", c_short),  # Nary category Number
        ( "m_BinValueNr", c_short),  # Binary value string number
        ( "m_PVText", c_char*(RTHIST_LEN_PVTEXT + 1)),  # PV text from PLS configuration
        ( "m_CategoryText", c_char*(RTHIST_LEN_CATTEXT + 1)),  # Category text
        ( "m_PVParams", c_char*(RTHIST_LEN_PVPARAMS + 1))  # Parameters separated by semicolon
        ]

class hdr1(Structure):
    _pack_ = 1
    _fields_ = [
    ("rec_type",c_short), # тип записи -1 - маркер времени но если в файле с данными -2  - аналог или дискрета
    ("s2",c_short),       # подтип  1 - аналоговый  2- дискретный
    ("num",c_long)       # Количество записей
    ]

class BlkInfo(Structure):
    _pack_ = 1
    _fields_ = [
            ("d", c_uint, 3),
            ("s", c_uint, 3),
            ("t", c_uint, 2)
            ]

# bb=BlkInfo()
# bb.t=3
# with open("aa.dat","wb") as f:
#     f.write(bb)

# cfg_file = "RtHistCfg.dat"
# file_name = "0_2017_01_29w"

# cfg_len=os.path.getsize(cfg_file)
# cfg = open(cfg_file,"rb")
# ha = open(file_name+".ana","rb")
# hb = open(file_name+".bin","rb")
# hdr = hdr1()

# e_ana = SRtHistAnaConf2()
# e_di = SRtHistBinConf2()
# di={}
# ana={}

def load_elements_group(cfg, n, obj, dkt, hdr):
    for i in range(hdr.num):
        cfg.readinto(obj)
        dkt[obj.m_PVNr] = copy.copy(obj)

def sv_json(obj, file_name):
    import json
    with open(file_name, "w", encoding="utf-8") as f:
    # with open(file_name, "w") as f:
        json.dump(obj, f, indent=2)

def sv_yaml(obj, file_name):
    # with open(file_name, "w", encoding="utf-8") as f:
    with open(file_name, "w", encoding="utf-8") as f:
        yaml.dump(obj, f)

def load_elements(cfg_file):
    cfg_len=os.path.getsize(cfg_file)
    cfg = open(cfg_file,"rb")
    di={}
    ana={}
    do_move=0
    pos=0
    c_ana=0
    c_di=0
    hdr = hdr1()
    e_ana = SRtHistAnaConf2()
    e_di = SRtHistBinConf2()
    while pos < cfg_len:
        cfg.seek(pos)
        pos = cfg.tell()
        cfg.readinto(hdr)

        if hdr.rec_type == 0xA0D:
            cfg.seek(pos+2)
            cfg.readinto(hdr)
            do_move = 1

        if hdr.rec_type == -2:
            if hdr.s2 == 1: # ana
                load_elements_group(cfg, hdr.num, e_ana, ana, hdr)
                c_ana+=1
            elif hdr.s2 == 2: #discret
                load_elements_group(cfg, hdr.num, e_di, di, hdr)
                c_di+=1

        if pos%1e6==0:
            print("pos=",pos)
        pos += RSIZE
    
    res_ana={}
    for i,v in ana.items():
        res_ana[v.m_PVId.decode("cp1251")] = v.m_PVText.decode("cp1251")
    # sv_yaml(res_ana,"ana.yaml")

    res_di={}
    for i, v in di.items():
        res_di[v.m_PVId.decode("cp1251")] = v.m_PVText.decode("cp1251")
    # sv_yaml(res_di,"di.yaml")

    return res_ana, res_di



# do_move=0
# pos=0
# c_ana=0
# c_di=0
# while pos < cfg_len:
#     cfg.seek(pos)
#     pos = cfg.tell()
#     cfg.readinto(hdr)

#     if hdr.rec_type == 0xA0D:
#         cfg.seek(pos+2)
#         cfg.readinto(hdr)
#         do_move = 1

#     if hdr.rec_type == -2:
#         if hdr.s2 == 1: # ana
#             load_elements_group(cfg, hdr.num, e_ana, ana)
#             c_ana+=1
#         elif hdr.s2 == 2: #discret
#             load_elements_group(cfg, hdr.num, e_di, di)
#             c_di+=1

#     if pos%100==0:
#         print("pos=",pos)
#     pos += RSIZE
