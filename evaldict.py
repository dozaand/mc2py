#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import re
import datetime
import struct


class EvalDict:
    u"""usage: "___%(ech(a,'asd'))s _____" % evaldict.EvalDict() - interpolate strings"""
    def __init__(self, globals=None, locals=None):
        if globals is None:
            globals = sys._getframe(1).f_globals
        self.globals = globals
        if locals is None:
            locals = sys._getframe(1).f_locals
        self.locals = locals

    def __getitem__(self, key):
        key = key % self
        return eval(key, self.globals, self.locals)


def seprp(arg, sep=","):
    "make string from arg separated by sep and pepend sep"
    return reduce(lambda x, y: unicode(x)+sep+unicode(y), arg, "")


def sepr(arg, sep=","):
    "make string from arg separated by sep"
    if len(arg) == 0:
        return ""
    else:
        return reduce(lambda x, y: unicode(x)+sep+unicode(y), arg)


def seprnl(arg):
    return sepr(arg, "\n")


def Import(filename):
    u"""import data from file
return list of string data
data is list of float,int or string data
"""
# ch=re.compile(r"(?P<fnum>[+\-]?\d*\.\d+([EeDd][+\-]?\d+)?)|(?P<inum>\d+)|(?P<word
# >[\wА-Яа-я]+)")
    ch = re.compile(
        r"(?P<ry>\d\d\.\d\d\.\d\d\d\d)|(?P<y>\d\d\d\d\.\d\d\.\d\d)|(?P<dtmy>\d\d\d\d\-\d\d\-\d\d +\d+:\d\d:\d\d)|(?P<dt>\d\d\-\d\d\-\d\d\d\d +\d+:\d\d:\d\d)|(?P<fnum>[+\-]?\d*\.?\d+([EeDd][+\-]?\d+)?)|(?P<inum>\d+)|(?P<word>[\.\wА-Яа-я\\*\^/0-9\(\)\[\]\{\}>\-]+)")
    f = open(filename, "r")
    filedata = []
    for line in f.readlines():
        linedata = []
        for match in ch.finditer(line):
            gd = match.groupdict()
            if gd["fnum"] != None:
                linedata.append(float(gd["fnum"]))
            elif gd["inum"] != None:
                linedata.append(int(gd["inum"]))
            elif gd["word"] != None:
                linedata.append(gd["word"])
            elif gd["dt"] != None:
                linedata.append(datetime.datetime.strptime(
                    gd["dt"], "%d-%m-%Y %H:%M:%S"))
            elif gd["dtmy"] != None:
                linedata.append(datetime.datetime.strptime(
                    gd["dtmy"], "%Y-%m-%d %H:%M:%S"))
            elif gd["y"] != None:
                linedata.append(datetime.datetime.strptime(
                    gd["y"], "%Y.%m.%d"))
            elif gd["ry"] != None:
                linedata.append(datetime.datetime.strptime(
                    gd["ry"], "%d.%m.%Y"))

        filedata.append(linedata)
    f.close()
    return filedata

byLine = 1


def FileIter(filename):
    u"""итерируем файл по словам или по строкам
если byLine
1 - по строкам
0 - по словам
"""
    global byLine
    f = open(filename, "r")
    for line in f.xreadlines():
        if byLine:
            yield line.rstrip()
        else:
            for word in line.split():
                yield word
                if byLine:
                    break
    f.close()


def BinRead(f, fmt):
    u"чтение двоичного файла по заданной строке формата fmt фрмат из struct"
    l = struct.calcsize(fmt)
    buf = f.read(l)
    return struct.unpack(fmt, buf)


def BinWrite(f, fmt, *data):
    u"запись двоичного файла по заданной строке формата fmt фрмат из struct"
    buf = struct.pack(fmt, *data)
    f.write(buf)


def Scanf(itr, fmt):
    """ format chars i,f,s,n
    i - int
    f - float
    c - char
    s - string
    n - string to newline
    """
    fmtdkt = dict(i=int, f=float, s=lambda x: x)
    res = []
    for ifmt in fmt:
        if ifmt in "ifs":
            for i in itr:
                if not i.isspace():
                    break
            buf = [i]
            for i in itr:
                if i.isspace():
                    break
                else:
                    buf.append(i)
            s = "".join(buf)
            res.append(fmtdkt[ifmt](s))
        else:
            buf = []
            for i in itr:
                if i != '\n':
                    buf.append(i)
                else:
                    break
            res.append("".join(buf))
    return res


def FileAsLetters(ff):
    for i in ff:
        for j in i:
            yield j
