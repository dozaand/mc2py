﻿.. _install-index:
=================================
GUI элементы
=================================

.. toctree::
   :maxdepth: 2

   gui/copyto
   gui/datetimectrl
   gui/kartedit
   gui/loadplugins
   gui/oedit
   gui/showkart
   gui/wxdl
