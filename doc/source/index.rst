﻿.. Ecran documentation master file, created by
   sphinx-quickstart on Mon May 30 22:49:29 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Добро пожаловать в документацию mc2py
=================================

Содержание:

.. toctree::
   :maxdepth: 4
   
   gui
   io
   math
   clip
   ctutils
   diskinfo
   ed
   fold
   geom
   idfdd
   idfddmongo
   latex
   module_rename
   modulerelative
   multiplot
   npfile
   oct_dec
   OrderedSet
   phc
   pymakeobj
   rmsvn
   scutil
   somesort
   sorteddict
   time2float
   util
   winregexamp

Индексы и таблицы
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

