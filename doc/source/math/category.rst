﻿=================================
Категория линейных пространств
=================================

------------------------------
Назначение
------------------------------

Для произвольного пользовательского класса доопределяет операции
 :math:`\vec{a}+\vec{b}`, :math:`\vec{a}-\vec{b}`, :math:`k*\vec{a},\vec{a}*k` используя для этого операции :math:`\vec{a}+=\vec{b}, \vec{a}*=k`


.. automodule:: mc2py.category
   :members:








