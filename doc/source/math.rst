﻿=================================
Математика
=================================

.. toctree::
   :maxdepth: 2

   math/bgk
   math/category
   math/expfilter
   math/fit
   math/int_bsearch
   math/smoothpack
   math/splextrems
   math/tensgen
   math/tspline
   
