#!/usr/bin/env python
# -*- coding: cp1251 -*-

#import wxversion
#wxversion.ensureMinimal('2.8')

import numpy as np
import matplotlib as mpl

##if __name__ == '__main__':
##    mpl.use("WXAgg")
##    mpl.interactive(True)

import matplotlib.pyplot as plt
from pylab import figure, show
#from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
from matplotlib.widgets import Cursor
from matplotlib.widgets import MultiCursor
from matplotlib.ticker import Formatter
from matplotlib.dates import YearLocator, MonthLocator, DateFormatter,DayLocator,HourLocator


import datetime
import random

##from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
##from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure

from matplotlib.colors import colorConverter

##import wx


#-------------------------------------------------------------------------------
class TGraphic(object):
    """������ ������"""
    def __init__(self,xdata,ydata=None,title="",razmernost="",color="r",lineType="-"):
        self.x=xdata
        self.y=ydata
        self.title=title
        self.razmernost=razmernost
        self.color=color
        self.lineType=lineType
        self.xmin=None
        self.xmax=None
        self.ymin=None
        self.ymax=None
        self.xlabel=""

    def SetXMinMax(self, mina=0 ,maxa=1):
        self.xmin=mina
        self.xmax=maxa

    def SetYMinMax(self, mina=0 ,maxa=1):
        self.ymin=mina
        self.ymax=maxa

    def SetXLabel(self, label=""):
        self.xlabel=label


    def Plot(self, pltshow=1, savepath=""):
        fig = figure( figsize=(12, 10) )
        ax = fig.add_subplot(111,aspect='equal')

        border_width = 0.05
        ax_size = [0+border_width, 0+border_width,
                   1-2*border_width, 1-2*border_width]
        ax = fig.add_axes(ax_size)

        if self.x is None:
            ax.plot(self.y,self.lineType)
        else:
            ax.plot(self.x,self.y,self.lineType)
            ax.set_xlim( self.x[0], self.x[-1] )

        if self.xmin!=None and self.xmin!=None:
            ax.set_xlim( self.xmin, self.xmax )
        if self.ymin!=None and self.ymin!=None:
            ax.set_ylim( self.ymin, self.ymax )


        if self.xlabel!="":
            ax.set_xlabel(self.xlabel)

        ax.set_ylabel(self.razmernost)
        ax.autoscale_view()
        ax.grid(True)
        ax.legend(shadow=True, fancybox=True)

        # ��������� ����� ��������� �� ��� �������
        fig.autofmt_xdate()
        # �������� ���������� ��� � - �������� �� ������ ����� � ��������
        if type(self.x[0]) == datetime.datetime :
            formatter = DateFormatter('%Y.%m.%d %H:%M:%S')
            # format the ticks
            #ax.xaxis.set_major_locator(DayLocator())
            #ax.xaxis.set_minor_locator(HourLocator())
            ax.xaxis.set_major_formatter(formatter)
        else:
            pass


        # set useblit = True on gtkagg for enhanced performance
        cursor = Cursor(ax, useblit=True, color=self.color, linewidth=2 )

        if savepath!="":
            plt.savefig(savepath)

        if pltshow:
            plt.show()


##-------------------------------------------------------------------------------
class TGraphicsList(object):
    u"""����� ��������"""
    def __init__(self,gList=[]):
        self.fig=plt.figure( figsize=(12, 11) )
        self.fig.canvas.mpl_connect('close_event', self.OnReset)
        self.grfList=gList[:]
        self.firstRun=1
        self.xlable=None

    def Add(self,currgrf):
        u"""���������� �������"""
        self.grfList.append(currgrf)

    def SetXLabel(self, xlable):
        u"""������ ������� ��� ��� OX"""
        self.xlable=xlable

    def Plot(self, pltshow=1, savepath=""):
        # ���� ���� �� ���� ������ ����
        if len(self.grfList)>0:
            #if not self.firstRun:
            # ������� �������� �����
            self.fig.clear()
            self.fig.canvas.draw()

            # � ����� ���������� ��� ������� � ����������� ����������������� �������� �������

            # ������� ����� ��������������� ������������
            razm=[]
            xlabel=""
            razmX={}
            razmY={}
            for el in self.grfList:
                if el.razmernost not in razm:
                    razm.append(el.razmernost)
                    # � ������������� �������� ������� ��� ������ ��������
                    if el.xlabel!="":
                        xlabel=el.xlabel

                    # ��������� ��� ��������� �������� �� ����
                    razmX[el.razmernost]=[]
                    razmY[el.razmernost]=[]


            # �������� ���������� ��� ������ ����������� �� ��������� � ��������
            for el in self.grfList:
                if el.ymin!=None:
                    razmY[el.razmernost].append(el.ymin)
                if el.ymax!=None:
                    razmY[el.razmernost].append(el.ymax)
                if el.xmin!=None:
                    razmX[el.razmernost].append(el.xmin)
                if el.xmax!=None:
                    razmX[el.razmernost].append(el.xmax)

            # �������� ���������� ��������� � �������� ��������
            xax={}
            yay={}
            for rkey in razmX:
                if len(razmX[rkey])!=0:
                    xv_min,xv_max= min( razmX[rkey] ),  max( razmX[rkey] )
                    xax[rkey]=(xv_min,xv_max)

                if len(razmY[rkey])!=0:
                    yv_min,yv_max= min( razmY[rkey] ),  max( razmY[rkey] )
                    yay[rkey]=(yv_min,yv_max)



            simbols=["o","v","^","<",">","d","s","p","*","h","H","D"]
            # Make a list of colors cycling through the rgbcmyk series.
            colors = [colorConverter.to_rgba(c) for c in ('r','g','b','c','y','m','k')]

            # �������� ������ ���� - ��� ������������ ������ MultiCursor
            axes=[]
            # ������ ���� ������� (������, ��������)
            prevax=None # ��� � �������� ����, ���������� ��� ���������� ����: ����������� ��������� ������� �� �
            razmSize=len(razm)
            for grfEl in self.grfList:
                rPos=razm.index(grfEl.razmernost) # ������ ����������� � ������ �������
                ax = self.fig.add_subplot(razmSize,1,rPos , sharex=prevax)

##                border_width = 0.05
##                ax_size = [0+border_width, 0+border_width,
##                       1-2*border_width, 1-2*border_width]
##                ax = self.fig.add_axes(ax_size)

                if grfEl.x is None:
                    ax.plot(grfEl.y, label=grfEl.title )
                else:
                    pind=int(random.uniform(0,len(simbols)))

                    # ����� ������ ��������� ��������
                    if grfEl.lineType=="-":
                        strSimb="%s-"% simbols[pind]
                    else:
                        strSimb=grfEl.lineType
                    ax.plot(grfEl.x,grfEl.y, strSimb,label=grfEl.title, color=grfEl.color )

                # �������� �������� ��� ����������� ��������
                if grfEl.razmernost in xax:
                    ax.set_xlim( xax[grfEl.razmernost][0], xax[grfEl.razmernost][1] )
                if grfEl.razmernost in yay:
                    ax.set_ylim( yay[grfEl.razmernost][0], yay[grfEl.razmernost][1] )

                ax.set_ylabel(grfEl.razmernost)
                #params = {'legend.fontsize': 14,
                #'legend.linewidth': 1}
                #plt.rcParams.update(params)
                ax.legend(shadow=True, fancybox=False)
                ax.grid(True)
                if self.xlable!=None:
                    ax.set_xlabel(self.xlable)
                axes.append(ax)

                # �������� ���������� ��� � - �������� �� ������ ����� � ��������
                if type(grfEl.x[0]) == datetime.datetime :
                    formatter = DateFormatter('%Y.%m.%d %H:%M:%S')
                    #ax.fmt_xdata = DateFormatter('%Y.%m.%d %H:%M:%S')
                    # format the ticks
                    #ax.xaxis.set_major_locator(DayLocator())
                    #ax.xaxis.set_minor_locator(HourLocator())
                    ax.xaxis.set_major_formatter(formatter)

            # ��������� ����� ��������� �� ��� �������
            self.fig.autofmt_xdate()

            # ���������� ������
            multi = MultiCursor(self.fig.canvas, axes,  color='r', lw=1)


            # ����������
            if savepath!="":
                plt.savefig(savepath)

            #����������
            if pltshow:
                show()


    def OnReset(self,event):
        """����� ��������� ������� ��� ��������"""
        self.grfList=[]
        self.firstRun=1
        plt.close()





#-------------------------------------------------------------------------------
##class TGraphicsList(object):
##    """����� ��������"""
##    def __init__(self):
##        self.fig=plt.figure()
##        self.fig.canvas.mpl_connect('close_event', self.OnReset)
##        self.grfList=[]
##
##    def Add(self,currgrf):
##        """���������� �������"""
##        self.grfList.append(currgrf)
##
##    def Plot(self):
##        # ���� ���� �� ���� ������ ����
##        if len(self.grfList)>0:
##            # ������� �������� �����
##            self.fig.clear()
##            self.fig.canvas.draw()
##            # � ����� ���������� ��� ������� � ����������� ����������������� �������� �������
##
##            # ������� ����� ��������������� ������������
##            razm=[]
##            for el in self.grfList:
##                if el.razmernost not in razm:
##                    razm.append(el.razmernost)
##
##            # �������� ������ ���� - ��� ������������ ������ MultiCursor
##            axes=[]
##            # ������ ���� ������� (������, ��������)
##            razmSize=len(razm)
##            for grfEl in self.grfList:
##                rPos=razm.index(grfEl.razmernost) # ������ ����������� � ������ �������
##                ax = self.fig.add_subplot(razmSize,1,rPos)
##                if grfEl.x is None:
##                    ax.plot(grfEl.y, label=grfEl.title)
##                else:
##                    ax.plot(grfEl.x,grfEl.y, label=grfEl.title)
##
##                ax.set_ylabel(grfEl.razmernost)
##                ax.legend(shadow=True, fancybox=True)
##                ax.grid(True)
##                axes.append(ax)
##
##            # ���������� ������
##            multi = MultiCursor(self.fig.canvas, axes,  color='r', lw=1)
##            # ��������� ����� ��������� �� ��� �������
##            self.fig.autofmt_xdate()
##            # ����������
##            self.fig.canvas.draw()
##
##
##    def OnReset(self,event):
##        """����� ��������� ������� ��� ��������"""
##        self.grfList=[]
##
##
##
###-------------------------------------------------------------------------------
##class CanvasFrame(wx.Frame):
##    def __init__(self, grfs=[]):
##        wx.Frame.__init__(self,None,-1,
##                         'CanvasFrame',size=(550,350))
##        #self.SetBackgroundColour(wx.NamedColor("WHITE"))
##        # ���������� �������� � ����������
##        self.grflist=TGraphicsList(grfs)
##        self.grflist.Plot()
##        self.canvas = FigureCanvas(self, -1, self.grflist.fig)
##
##        self.sizer = wx.BoxSizer(wx.VERTICAL)
##        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
##        self.SetSizer(self.sizer)
##        self.Fit()
##
##        self.add_toolbar()  # comment this out for no toolbar
##
##
##    def add_toolbar(self):
##        self.toolbar = NavigationToolbar2Wx(self.canvas)
##        self.toolbar.Realize()
##        if wx.Platform == '__WXMAC__':
##            # Mac platform (OSX 10.3, MacPython) does not seem to cope with
##            # having a toolbar in a sizer. This work-around gets the buttons
##            # back, but at the expense of having the toolbar at the top
##            self.SetToolBar(self.toolbar)
##        else:
##            # On Windows platform, default window size is incorrect, so set
##            # toolbar width to figure width.
##            tw, th = self.toolbar.GetSizeTuple()
##            fw, fh = self.canvas.GetSizeTuple()
##            # By adding toolbar in sizer, we are able to put it at the bottom
##            # of the frame - so appearance is closer to GTK version.
##            # As noted above, doesn't work for Mac.
##            self.toolbar.SetSize(wx.Size(fw, th))
##            self.sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
##        # update the axes menu on the toolbar
##        self.toolbar.update()
##
##
##    def OnPaint(self, event):
##        self.canvas.draw()








#-------------------------------------------------------------------------------
if __name__ == '__main__':
    import numpy as np
    import datetime

    a=[]
    k=[]
    c=datetime.datetime(2011,12,5,3,12,6)

    for index in range (3000):
        a.append(c+datetime.timedelta(index))
        k.append(index)

    name = "Test"
    gr = TGraphic(a,k,name)

    grl=TGraphicsList()
    grl.Add(gr)
    grl.Plot()


