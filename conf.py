import argparse
import os
import pickle
import subprocess as sp
from pathlib import Path
from mc2py.util import newcwd
import yaml
import re

"""код для управления ресурсами, типа scons или make но отличающийся тем, что к цели может вести несколько путей
   путь при этом стараемся выбрать кратчайший
   целей тоже может быть несколько
   При построении целей могут возникать отказы, которые должны приводить к перепостроению плана
   способ достижения метод динамического программирования

"""


class Taim:
    """цель"""
    def __init__(self):
        self.value=0 # полезность цели (в случае если код нужен - положительное число в случае если не нужен -отрицательная стоимость занимаемого места)
        self.neg_value = -1 # ущерб если цельне нужна для пользователя
        self.done = False # цель достигнута или нет

    def rules(self):
        """список правил которые могут привести к достижению данной цели по умолчанию пуст"""
        return []


class Trule:
    """правило достижения целей"""
    def __init__(self):
        self.target=None # цель которые достигаются при выполнении правила
        self.sources=[] # цели которые необходимы для выполнения данного правила
        self.cost=0 # время выполнения правила (нужно для скорейшего построения целей)

    def __call__(self):
        """вызов действий необходимых для достижения цели или целей"""
        pass


class Tcomp:
    """компютер - это список доступных целей
    изменение состояния делаем путем назначения новых целей или изменения стоимости старых
    """
    def __init__(self):
        self.aims=[]
        self.changed_aims=[]

    def change_aim_state(self, aim, value):
        """изменение ценности цели"""
        pass

    def make_plan(self):
        """делаем список целей которые надо сделать."""
        pass


HOME = Path(os.environ["HOME"])

confdir = HOME/".conf"
if not confdir.exists():
    os.mkdir(confdir)

def get_repo():
    repo = confdir/"repo"
    if not repo.exists():
        os.mkdir(repo)
    return repo



"""
пример построения требований при построении модели
"""
def init_git(workdir:str=".", repodir:str=None):
    """инициализация гитового репозитория и установка его в целевую машину."""
    p = Path(workdir)
    if repodir is None:
        repodir=get_repo()
    with newcwd(p):
        if not (p/".git").exists():
            sp.run("git init")
            sp.run("git add *")
            sp.run("git commit -m 'initial'")
            thisdir = p.absolute().name
            target = (Path(repodir)/(thisdir+".git")).as_posix()
            target = re.sub("(\w):",r"/\1",target)
            with newcwd(".."):
                sp.run(f"git clone --bare {thisdir} {target}")
            sp.run(f"git remote add origin {target}")
            sp.run("git branch --set-upstream-to=origin/master master")


def clone_repo(gitname:list, target_dir:str=None, repodir:str=None):
    if target_dir is None:
        target_dir="."
    p = Path(target_dir)
    if repodir is None:
        repodir=get_repo()
    for nm in gitname:
        repo = Path(repodir)/(nm+".git")
        with newcwd(p):
            if os.path.exists(nm):
                with newcwd(nm):
                    sp.run("git pull")
            else:
                sp.run(f"git clone --recursive {repo.as_posix()}")

# это те разделы конфига которые должны быть set
cfg_sets = ["used_in", "use"]


def to_sets(data):
    for i in cfg_sets:
        data[i] = set(data[i])
    return data


def from_sets(data):
    for i in cfg_sets:
        data[i] = list(data[i])
    return data


def get_conf(dir_path:Path):
    """возвращаем содержимое конфигурационного файла для данной директории"""
    file_name = dir_path / "conf.yaml"
    if not file_name.exists():
        dkt = {"used_in":[], "use":[]}
        with open(file_name,"w",encoding="utf-8") as f:
            yaml.dump(dkt,f, allow_unicode=True)
        return to_sets(dkt)
    else:
        with open(file_name,"r",encoding="utf-8") as f:
            data = yaml.load(f)
        return to_sets(data)


def set_conf(dir_path:Path, data:dict):
    file_name = dir_path / "conf.yaml"
    from_sets(data)
    with open(file_name,"w",encoding="utf-8") as f:
        yaml.dump(data,f, allow_unicode=True)
    to_sets(data)


def dirnm(nm:Path):
    return nm.absolute().name


def use(use_dir:Path, work_dir:Path=None):
    """помечаем что директория используется как подпроект
    1. делаем ссылку
    2. в каждой директории помечаем наличие связи (по абсолютному пути)
    """
    if work_dir is None:
        work_dir=Path(".")

    to_dir = (work_dir / dirnm(use_dir)).absolute()
    from_dir = use_dir.absolute()
    if os.path.exists(to_dir):
        raise runtime_error("directory exists")
    sp.run(f"mklink /J {to_dir}\\{from_dir} {from_dir}",shell=1)
    cfg = get_conf(work_dir)
    cfg["use"].add(use_dir.absolute())
    set_conf(use_dir, cfg)
    cfg_used = get_conf(work_dir)
    cfg_used["use"].add(work_dir.absolute())
    set_conf(use_dir, cfg)


# target_dir = r"d:\PROJECTS\deploy\proj"
# gitname = "lib1"
# repodir = r"d:\PROJECTS\deploy\repo"


##workdir = r"d:\PROJECTS\deploy\proj\lib2"
##repodir = r"d:\PROJECTS\deploy\repo"
##init_git(workdir,repodir)



# dbdir = "."
# dbname = os.path.join(dbdir,"info.pkl")
# if os.path.exists(dbname):
#     with open(dbname,"rb") as f:
#         inst_path = ""

def load_repolist():
    pass




if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="package manager", description="make package managment")
    parser.add_argument("-i","--install", nargs="*", help=u"пакеты которые надо доставить")
    parser.add_argument("-u","--uninstall", nargs="*", help=u"пакеты которые надо удалить")
    parser.add_argument("-p","--pack", action='store_true', help='Print more data')
    parser.add_argument("-d","--destination",  help='destination path of package')
    parser.add_argument("-f","--info", nargs="*", help=u"пакеты которые установлены")
    parser.add_argument("--repodir",default=None, help=u"repository directory")
    parser.add_argument("--workdir",default=".", help=u"repository directory")
    parser.add_argument("command", nargs="*", help=u"выолняемая команда и аргументы")
    args = parser.parse_args()
    if args.command[0] == "init_git":
        init_git(args.workdir,args.repodir)
    elif args.command[0] == "clone":
        clone_repo(args.command[1:],args.workdir,args.repodir)

# giw_stepper = Taim()
# giw_stepper_src = Taim()
# giw_stepper_src_git = Taim()

# giw_stepper_src.rules

# giw_stepper_lib = Taim()
# giw_stepper_lib_repo = Taim()

