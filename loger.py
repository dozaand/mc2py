#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""������ ��� ����������� ����������"""

import codecs
import datetime
# import time


class Loger(object):
    u"""������ ��� ����������� ������"""
    def __init__(self):
        self.create_file_obj()

    def create_file_obj(self):
        u"""�������� ����� ����� � ������� ����� ��� ������ ������������ ����������"""
        # d=datetime.datetime.utcnow()
        d = datetime.datetime.utcnow()
        log_file_name = r"%s.log" % d.strftime("%Y%m%d")
        self._f_log_obj = codecs.open(log_file_name, 'a+', encoding="cp1251")
                                      # �������� ����� � ������ �� �������� � ���������� ��� ������� ��������

    def log_information(self, stra=""):
        u"""����������� ����������
        stra ������ ��� �����������
        """
        # ��������� �������� ������� - � ������� UTC
        curr_time_UTC = datetime.datetime.utcnow().strftime(
            "%Y.%m.%d %H:%M:%S")
        curr_time_local = datetime.datetime.now().strftime("%Y.%m.%d %H:%M:%S")
        # time.sleep(0.5) ��� ������������ ������
        print curr_time_UTC, curr_time_local, stra
        self._fLogObj.write("%s;\t%s\n" % (
            curr_time_UTC, curr_time_local, stra))
        # ������������� ��������� ����� � ��������� �������
        self._f_log_obj.flush()

    def close(self, stra=""):
        u"""�������� ������� �����������
        stra c����� ��� �����������
        """
        # � ������ ������������� ������ ������ ����������
        if stra != ""
            self.log_information(stra)
        else:
            pass
        # �������� �����
        self._f_log_obj.close()
