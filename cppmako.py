#!/usr/bin/env python
# -*- coding: utf-8 -*-
u""" generate c++ template render
"""
import argparse
import codecs
import re

def proc(data):
    pos=0
    for i in re.finditer(ur"(<%(.+?)%>)|(\${(.+?)})",data):
        if i.start()!=pos:
            yield ('SS<<"'+data[pos:i.start()]+'";').replace("\n","\\n")
        grp=i.groups()
        if grp[1]:
            yield ""+grp[1]+" "
        if grp[-1]:
            yield "SS<<("+grp[-1]+");"
        pos=i.end()

def convert(file, output):
    with codecs.open(file,"r",encoding="utf-8") as f:
        data=f.read()
    res="\n".join(list(proc(data)))
    with codecs.open(output,"w",encoding="utf-8") as f:
        f.write(res)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="cpptempl",description="create c++ template function")
    parser.add_argument("-i","--input",default="",help=u"dile with data")
    parser.add_argument("-o","--output", help=u"output file")
    args = parser.parse_args()
    convert(args.input, args.output)

