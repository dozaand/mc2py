#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
�������� �������� ����������� ��������
"""
import xlwt
import numpy as np
from mc2py.fold import Transpose


def PutTbl(fil, tbl, w=None, startindex=None):
    u"""��������� �� ����� ����������� �������"""
    doc = xlwt.Workbook()
    sheet = doc.add_sheet('0', cell_overwrite_ok=True)
    if w:
        for i in range(len(w)):
            sheet.col(i).width = w[i]*100
    for irow, row in enumerate(tbl):
        if startindex is None:
            for icol, el in enumerate(row):
                if type(el) is np.float32:
                    el = float(el)
                sheet.write(irow, icol, el)
        else:
            sheet.write(irow, 0, startindex+irow)
            for icol, el in enumerate(row):
                if type(el) is np.float32:
                    el = float(el)
                sheet.write(irow, icol+1, el)

    doc.save(fil)


def Sheet2Table(sheet):
    return [sheet.row_values(i) for i in range(sheet.nrows)]


def SelectCols(sheet, colnames):
    u"""������� �������� �������"""
    hdr = sheet.row_values(0)
    try:
        colindexes = [hdr.index(i) for i in colnames]
    except ValueError as e:
        raise ValueError("index %s not found" % i)
    return Transpose([sheet.col_values(i, 1) for i in colindexes])


def main():
    pass

if __name__ == '__main__':
    main()
