#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""
�������� � �������������� ������������ �������� python.
��� ������ � json ���� ����������� ���������:

 arr=jed(arr,npp)

ced ������ ���������� � ������� ��� ��� ����� �� �����������
�� ����� ������:

 arr=ced(arr,npp)

�������� �������� ���� � ���������� ����� EDITOR, ��� ���������� �� ������

edlist="notepad++ sublime_text2 emacs vi notepad".split()

������� ����������� ����� �������������� ��� ������� ���������
"""

import pickle
import numpy as np
import subprocess as sp
import os
import inspect
import json
import yaml
import codecs
import datetime
from mc2py.util import Tmp_file_name

edlist = "notepad++ sublime_text2 emacs vi notepad".split()


def _GetAllKeys(obj):
    u"""���������� ��� ������ ����� ���� �����, �.�. ���������� __dict__, _fields_ � ��� ��������"""
    key = list(obj.__dict__.keys())
    if hasattr(obj, "_fields_"):
        key += [el[0] for el in obj._fields_]
    proplist = [name for (name, sobj) in inspect.getmembers(
        obj.__class__, inspect.isdatadescriptor) if isinstance(sobj, property)]
    return sorted(key+proplist)


def TextDump(obj, f, upkey=""):
    u"""
    return text form of tree-like object
    """
    th = np.get_printoptions()['threshold']
    np.set_printoptions(threshold='nan')
    try:
        if hasattr(obj, "keys"):  # �������
            key = sorted(
                obj.keys())  # ���������� � python3 ���� ����� �� ����������
            for key in obj.keys():
                subobj = obj[key]
                TextDump(subobj, f, upkey+"[\'{}\']".format(key))
        elif isinstance(obj, basestring):
            if upkey:
                f.write('{}=u"{}"\n'.format(upkey, obj))
        elif hasattr(obj, "shape"):  # numpy array ������������� �������� ��������� ����� ��� ������� ��� � ������ �������� ��������
            if upkey:
                f.write("{}=".format(upkey))
            ss = "{}\n".format(repr(obj)).replace("array", "np.array")
            f.write(ss)
        elif hasattr(obj, "__getitem__"):  # ������ �.�. ������� � ������������ ��������
            for key, subobj in enumerate(obj):
                subobj = obj[key]
                TextDump(subobj, f, upkey+"[{}]".format(key))
        elif hasattr(obj, "__iter__"):  # ������������ ������� (���� set)
            for subobj in obj:
                TextDump(subobj, f, upkey)
        elif hasattr(obj, "__dict__") or hasattr(obj, "_fields_"):  # ������
            if obj.__doc__:
                f.write(u"# {}\n".format(obj.__doc__))
            for key in _GetAllKeys(obj):
                subobj = getattr(obj, key)
                # create description of the field
                sdsc = u""
                if hasattr(obj, "dsc"):
                    if key in obj.dsc:
                        sdsc = obj.dsc[key]
                sunits = u""
                if hasattr(obj, "units"):
                    if key in obj.units:
                        sunits = obj.units[key]
                if sdsc or sunits:
                    f.write(u"# {} [{}]\n".format(sdsc, sunits))
                TextDump(subobj, f, upkey+".{}".format(key))
        else:
            if upkey:
                f.write("{}=".format(upkey))
            f.write(repr(obj)+"\n")
    finally:
        np.set_printoptions(threshold=th)


def TryEdit(filename, edit=None, cmdfmt=None):
    u"""
    try to edit some file by text editor.
    return True if test is edited? else return False
    filename - name of file to edit
    edit - user defined edit command
    """
#    edlist="notepad++ sublime_text2 emacs vi notepad".split()
    global edlist
    if edit is None:
        if "EDITOR" in os.environ:
            edt = os.environ["EDITOR"]
            edlist = [edt]+edlist
    else:
        edlist = [edit]+edlist
    for ed in edlist:
        try:
            cmd = "{} {}".format(ed, filename)
            t0 = os.path.getmtime(filename)
            sp.check_call(cmd, shell=1)
            t1 = os.path.getmtime(filename)
            return t1 != t0
        except:
            pass
    raise Exception("any editor from list: {0} \ncan't be started, define EDITOR in environment or correct path:\n{1}".format(
        edlist, os.environ["path"]))


def ped(obj, editcmd=None):
    u"""edit object by external editor as pickle
    """
    with Tmp_file_name() as fil:
        with open(fil, "wt") as f:
            pickle.dump(obj, f)
        if TryEdit(fil, editcmd):
            with open(fil, "rt") as f:
                obj0 = pickle.load(f)
            return obj0
        else:
            return obj


def jed(obj, editcmd=None):
    u"""edit object by external editor as json
    """
    with Tmp_file_name() as fil:
        with open(fil, "wt") as f:
            json.dump(obj, f, indent=2)
        if TryEdit(fil, editcmd):
            with open(fil, "rt") as f:
                obj0 = json.load(f)
            return obj0
        else:
            return obj


def yed(obj, editcmd=None):
    u"""edit object by external editor as json
    """
    with Tmp_file_name() as fil:
        with open(fil, "wt") as f:
            yaml.dump(obj, f, indent=2)
        if TryEdit(fil, editcmd):
            with open(fil, "rt") as f:
                obj0 = yaml.load(f)
            return obj0
        else:
            return obj


def cednparr(obj, editcmd=None):
    u"""edit file by external editor as number table - valid onlu for numpy arrays
    """
    with Tmp_file_name() as fil:
        np.savetxt(fil, obj)
        if TryEdit(fil, editcmd):
            res = np.loadtxt(fil, obj.dtype)
            obj[...] = res[...]
        return obj


def ced(obj, editcmd=None, encoding="cp1251"):
    u"""edit object by external editor as python code. exec is used!!!
    """
    if type(obj) is np.ndarray and len(obj.shape) <= 2:
        return cednparr(obj, editcmd)
    with Tmp_file_name() as fil:
        with codecs.open(fil, "w", encoding=encoding) as f:
            TextDump(obj, f, "obj")
        if TryEdit(fil, editcmd):
            with codecs.open(fil, "r", encoding=encoding) as f:
                cmd = f.read()
                exec(cmd)
        return obj

# class TSample1(object):
##    a=23
##    def __init__(self):
##        self.a="asd"
##        self.b=np.array([[1,2,3,4,5.],[1,2,3,4,5.]])
##        self.c=24234.234
##
# class TSample(object):
##    u"""��� ������������ �� ������"""
##    dsc=dict(a=u"������1",b=u"������2")
##    units=dict(a=u"�/�",b=u"��")
##    a=23
##    def __init__(self):
##        self.a="asd"
##        self.b=[1,2,[3,4]]
##        self.ss=TSample1()
##
a=[1,[2,4,5],3,dict(a=2,f=5)]
##
# npp=r"C:\apps\Notepad++\notepad++.exe"
#b=ped(a)
b=jed(a)
#b=yed(a)
# smp=TSample()
# ced(smp,npp)
# arr=np.zeros((2,3,3))
# arr=ced(arr,npp)
# arr=np.zeros((2,3))
# arr=ced(arr,npp)
