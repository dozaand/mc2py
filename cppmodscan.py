#!/bin/env python3
"""
составление дерева зависимостей модулей cpp
может потребоваться чтобы работать с модулями

Смысл - автоматическое выделение данных интерфейса (делаем просто export)
Шаблоны компилируются и попадают в кэш, откуда они значительно быстрее извлекаются

Имеет смысл когда все на шаблонах сделано

==========================================
размер 600k 10000 функций
с инклудами

полный ребилд 1.8
ребилд main   0.8
проверка без билда 0.222

с модулями

полный ребилд 2.059
ребилд main   0.701
проверка без билда 0.222

==========================================
размер 3M 50000 функций


с инклудами

полный ребилд 2.286
ребилд main   1.295    # чистый билд 1.01 # 3.5 - 12M
проверка без билда 0.243

с модулями

полный ребилд 3.793
ребилд main   0.801  # чистый билд 0.41  # 0.49 - 12M
проверка без билда 0.289



"""

import re
from pathlib import Path
from collections import defaultdict
from mako.template import Template

file2import={}
mod2file=defaultdict(set)

# сканирование файлов
for fin in Path(".").rglob("*.cpp"):
    with open(fin,"r",encoding="utf-8") as f:
        data=f.read()
    sfin=str(fin)
    mdl_import = set(re.findall("import +(.+);",data))
    file2import[sfin] = mdl_import
    mdl_export = set(re.findall("export +module +(.+);",data))
    for mod in mdl_export:
        mod2file[mod].add(sfin)

# составление перечня прямых зависимостей
file2file={}
for k,v in file2import.items():
    res=set()
    for modname in v:
        res.update(mod2file[modname])
    file2file[k]=res
    
res_make=[]
res_scons=[]
for k,v in file2file.items():
    if v:
        deps=" ".join(v)
        line=f'{k}: {deps}'
        res_make.append(line)

mod2file_list={}
for k,v in mod2file.items():
    if v:
        mod2file_list[k]=str([str(Path(i).with_suffix(".o")) for i in v])

file2mod_list={}
for k,v in file2import.items():
    if v:
        file2mod_list[str(Path(k).with_suffix(".o"))]=v

tpl=Template("""
%for k,v in mod2file_list.items():
env.Depends("gcm.cache/${k}.gcm",${v})
%endfor

%for k,v in file2mod_list.items():
%for vv in v:
env.Depends("${k}", 'gcm.cache/${vv}.gcm')
%endfor
%endfor
""")
res=tpl.render(mod2file_list=mod2file_list,file2mod_list=file2mod_list)

with open("deps.dat","w") as f:
    f.write(res)

#if __name__=="__main__":
