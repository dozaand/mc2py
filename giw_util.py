#!/bin/env/python
# -*- coding:utf-8 -*-

import pickle as cp
import glob,os,re
from mc2py.util import PathFind,PathFindDir
from itertools import chain
import shutil
import yaml
import codecs
from mc2py.fold import conv_val,fmap
import subprocess as sp
import numpy as np
import toolz
from toolz.recipes import partitionby
import h5py

def tolover():
    for i in chain(PathFind("*"),PathFind("*.*"),PathFindDir("*"),PathFindDir("*.*")):
        lwr=i.lower()
        if lwr != i:
            os.rename(i,lwr)

def include_lover():
    for i in PathFind("*.for"):
        with open(i,"r") as f:
            data=f.read()
        res=re.sub(r"include +'[\w\.]+'",lambda x:x.group(0).lower(),data,0,re.I)
        if res!=data:
            with open(i,"w") as f:
                f.write(res)

def spj_to_filelist(nm):
    """extract file names from spj"""
    with codecs.open(nm,"r",encoding="cp1251") as f:
        data=f.read()
    data1=data.lower()
    return re.findall("file=([^\x1d]+)",data1)

#def loop_some():

# for i in PathFind("*.vik"):
#     if re.search(r"work.+\.vik",i,re.I):
#         nm,ext=os.path.splitext(i)
#         print(i)
#         sp.call("git rm --cached {0}.idf".format(nm))
#         sp.call("git rm --cached {0}.ddf".format(nm))
#    sp.call("git add {0}".format(i))

def spj2yaml(pattr="*.spj", startdir=None):
    for i in PathFind(pattr, startdir):
        lst = fmap(spj_to_filelist(i),conv_val,conv_val)
        with codecs.open(i+".yaml","w",encoding="cp1251") as f:
            yaml.dump(lst,f,allow_unicode=1)


def delwork():
    for i in PathFindDir("work"):
        print(i)
        shutil.rmtree(i)

def rmfh():
    for i in PathFind("*.idf"):
        nm,ext=os.path.splitext(i)
        try:
            print(i)
            os.remove(nm+".fh")
        except:
            pass

def rm_gab():
    for i in chain(PathFind("*.pdb"),PathFind("*.log"),PathFind("*.sdf"),PathFind("*.ipch"),PathFind("*.suo")):
        os.remove(i)

#-------------------------------------------------------------------------
count_items=0

def ff(li):
    res=re.split(r"[ \t\*]+",li)
    res1=[]
    for i in res:
        try:
            a=int(i)
        except:
            a=i
        if(a!=''):
            res1.append(a)
    return res1

def tx_tail(f,seq):
    itr=iter(seq)
    for i in itr:
        if f(i):
            break
    for i in itr:
        yield i

typdkt={(4,4):np.float32,(4,8):np.double,(2,4):np.int,(2,2):np.short,(2,8):np.longlong,(1,1):np.uint8}

def i2typ(nitem,t,l):
    if t==3:
        typ=np.character
        nn=nitem*l
    else:
        typ=typdkt[(t,l)]
        nn=nitem
    return (nn,typ)

def to_dt(rec):
    global count_items
    if len(rec)==3:
        name="Xname"+str(count_items)
        count_items+=1
        typ=np.character
        L=rec[2]
        assert(rec[1]=="U")
    else:
        assert(len(rec)==6)
        name=rec[-1]
        L,typ=i2typ(rec[2],rec[3],rec[4])
    typsiz=np.dtype([(name,typ,L)]).itemsize
    obj_offset=rec[0]
    return ((typsiz,obj_offset),(name,typ,L))

def is_by_addr(x):
    return  "typ*len" in x

def filter_overlapped(li):
    global count_items
    pos=0
    for i in li:
        L,off = i[0]
        if off>pos:
            name="Xname"+str(count_items)
            count_items+=1
            yield (name,np.character,off-pos)
            pos=off
        if pos==off:
            yield i[1]
            pos+=L
        else:
            if off+L>pos:
                if not i[1][0].startswith("Xname"):
                    print(i)
                    raise Exception("shifted")


def decode_vpt(file_name):
    global count_items
    count_items = 0
    xprepres = sp.check_output("xprep.exe {0} -d".format(file_name))
    txt = re.split(r"[\r\n]+",xprepres.decode("cp1251"))
    
    items=list(toolz.map(to_dt,toolz.map(toolz.last,partitionby(lambda x:x[0],toolz.filter(lambda x:len(x)==3 or len(x)==6,toolz.map(ff,tx_tail(is_by_addr,txt)))))))
    filterd_items = list(filter_overlapped(items))
    return np.dtype(filterd_items)

class Tmfa_file(object):
    u"""
    загрузка и сохранение состояний mfa
    """
    def __init__(self,name,vpt=None,dtype=None):
        with open(name,"rb") as f:
            self.header=f.read(512)
            if dtype is None:
                if vpt is None:
                    self.vpt=re.search(r"\w+\.\w+\.vpt",self.header).group(0)
                else:
                    self.vpt=vpt
                dtype = decode_vpt(self.vpt)
            self.data = np.fromfile(f,dtype=dtype)

    def save(self,name):
        with open(name,"wb") as f:
            f.write(self.header)
            self.data.tofile(f)

    def saveH5(self,name):
        with h5py.File(name,"w") as f:
            for i in self.data.dtype.names:
                f[i]=self.data[i]

def vpt2dict(state_name,my_sys,vpt=None, abs_path=None):
    short_state_name = os.path.split(state_name)[-1]
    pkl_name = '{0}.pkl'.format(short_state_name)
    if abs_path:
        pkl_name = os.path.join(abs_path, pkl_name)
    if my_sys == 'Windows':
        #read data
        with open(state_name,"rb") as f:
            header=f.read(512)
            if vpt is None:
                vpt=re.search(r"\w+\.\w+\.vpt",header).group(0)
            data=f.read()
        #read vpt
        txt = re.split(r"[\r\n]+",sp.check_output("xprep.exe {0} -d".format(vpt)))
    #    items=list(toolz.map(to_dt,toolz.map(toolz.last,partitionby(lambda x:x[0],toolz.filter(lambda x:len(x)==3 or len(x)==6,toolz.map(ff,tx_tail(is_by_addr,txt)))))))
        datadsc={}
        for rec in toolz.map(ff,tx_tail(is_by_addr,txt)):
            if len(rec)==6:
                name=rec[-1]
                if "#" in name:
                    name=name.split("#")[-1]
                L,typ=i2typ(rec[2],rec[3],rec[4])
                typsiz=np.dtype([(name,typ,L)]).itemsize
                obj_offset=rec[0]
                obj = {"off":obj_offset,"len":typsiz,"nm":name,"typ":typ,"L":L}
                if name in datadsc:
                    obj_old = datadsc[name]
                    if typsiz > obj_old["len"]:
                        datadsc[name]=obj
                else:
                    datadsc[name] = obj
        res = {}
        for i in datadsc.itervalues():
            if i["typ"] is np.character:
                v = "".join(np.frombuffer(data,"S1",i["L"],i["off"]))
            else:
                v = np.frombuffer(data,i["typ"],i["L"],i["off"])
            res[i["nm"]]=v
        with open(pkl_name, "wb") as pkl:
            cp.dump(res, pkl, 2)
    else:
        with open(pkl_name, "rb") as pkl:
            res = cp.load(pkl)
    return res
