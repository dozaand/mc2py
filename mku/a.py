# Сценарий по выводу блока на МКУ

from mc2py.zmq_script_client import *
import numpy as np
import math
import re
from operator import truth
import matplotlib.pyplot as plt
from pathlib import Path
from print_model import print_model
from grafs_dynamic_2 import grafs_dynamic
from load_xe_jod_rp import load_xe_jod_rp_to_model
from cb_interp import cb_interp
import glob



soc=Tscript_drv("tcp://localhost:5555")



# открытие
soc['10KBA25AA001_x_ALOE']=True #  Открытие 1 блокирующей задвижки ( Лож ОТКРЫТЬ )
soc['10KBA25AA001_f_ALS ']=True #  Открытие 1 блокирующей задвижки ( Непр ком.ЗАКРЫТЬ )
soc['10KBA25AA002_x_ALOE']=True #  Открытие 1 блокирующей задвижки ( Лож ОТКРЫТЬ )
soc['10KBA25AA002_f_ALS ']=True #  Открытие 1 блокирующей задвижки ( Непр ком.ЗАКРЫТЬ )
soc.step()
# останов открытия
soc['10KBA25AA001_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
soc['10KBA25AA001_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )
soc['10KBA25AA002_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
soc['10KBA25AA002_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )








# закрытие задвижки 10KBA28AA002
#soc['10KBA28AA003_x_ALOE']= False # Остановка открытия регулятора ( Лож ОТКРЫТЬ )
#soc['10KBA28AA003_f_ALS'] = False # Остановка открытия регулятора ( Непр ком.ЗАКРЫТЬ )
#soc['10KBA28AA003_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
#soc['10KBA28AA003_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )

# закрытие задвижки 10KBA28AA202
#soc['10KBA28AA202_x_ALOE']= False # Остановка открытия регулятора ( Лож ОТКРЫТЬ )
#soc['10KBA28AA202_f_ALS'] = False # Остановка открытия регулятора ( Непр ком.ЗАКРЫТЬ )
#soc['10KBA28AA202_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
#soc['10KBA28AA202_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )

# закрытие задвижки 10KBA28AA002
#soc['10KBA28AA002_x_ALOE']= False # Остановка открытия регулятора ( Лож ОТКРЫТЬ )
#soc['10KBA28AA002_f_ALS'] = False # Остановка открытия регулятора ( Непр ком.ЗАКРЫТЬ )
#soc['10KBA28AA002_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
#soc['10KBA28AA002_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )










    
    
    
    