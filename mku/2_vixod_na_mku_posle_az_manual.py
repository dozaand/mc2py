# Сценарий по выводу блока на МКУ

from mc2py.zmq_script_client import *
import numpy as np
import math
import re
from operator import truth
import matplotlib.pyplot as plt
from pathlib import Path
from print_model import print_model
from grafs_dynamic_2 import grafs_dynamic
from load_xe_jod_rp import load_xe_jod_rp_to_model
from cb_interp import cb_interp
import glob

def vixod_na_mku_posle_az_manual(soc, folderpath):
    """Выходна МКУ после АЗ"""

    # загружаем состояние на МКУ
    #soc.load_state(r"D:\NOVO-2\KARRU_NV\MODEL\STATE\Olga\B01_K01_MCL_BOC_0_days_GENOFF_P1-2K-NORMA.sta")
    print_model(soc, "Выход на МКУ в ручном режиме")

    # удаляем файлы в папке с программой MKU01, чтобы они не мешали ей работать
    mku_folder ="C:\ENIKOTSO\output\mku01\MKU_DATA"
    pth = Path(mku_folder)
    for child in pth.glob('*'):
        if child.is_file():
            child.unlink()

    # отобразим флаг выхода на МКУ
    soc["MKU01_START_SCRIPT_POSLE_AZ_MANUAL"] = 1


    
    # время начала выхода на МКУ после АЗ, часы
    begining_after_az= float(soc["MKU_t_after_shut_down"]) #4
    
    # объект для интерполирования значения крит. концентрации борной кислоты по времени и температуре
    cb_interp_obj = cb_interp(folderpath)




    def step(nstep=1):
        #curr_time =float(soc["#YM#ymtime_re8"])
        #temper= float(soc["SVRK_THNAVR"])
        #cb_crit = cb_interp_obj.cb_interp(curr_time, temper)
        #soc['MKU01_CB_CRITICAL'] = cb_crit        
        soc.step(nstep)


    
    
    temper= float(soc["SVRK_THNAVR"])
    soc['MKU01_CB_CRITICAL'] = cb_interp_obj.cb_interp(begining_after_az*3600, temper)   

    # включаем блокировки защит
    soc['KEY1_SHUNT_AZ1'] = 1
    soc['KEY_SHUNT_URB'] = 1
    soc['KEY_SHUNT_PZ1'] = 1
    soc['KEY_SHUNT_PZ2'] = 1


    # убираем ускорения
    soc['#YM#YMRP_FAST']=1
    soc['#YM#ymfast'] = 1
    
    # зафиксируем мощность, чтоб у нас было как-бы МКУ
    soc['#YM#ymflag_xe'] = 1  # расчет динамики Xe в статике
    step()
    soc['#YM#YMFLAGSTAT']=1 # включаем статику
    step()
    soc['YZBORREG']=1   # включаем внешнее управление бором
    soc.step()
    soc['YZBORMODE']=1  # бор в дистанцию
    step(4)
    
    # концентрация борной кислоты для пуска
    cbor_pusk = float( soc['MKU01_CB_START'] ) # 8.14
    # введем 16 г/кг бора
    while float(soc['#YM#ymbor_cor']) < cbor_pusk:
        soc['#YM#ymbor_cor']+=0.5
        step()
    soc['#YM#ymbor_cor']=cbor_pusk
    # теперь, не боясь срабатывания АЗ, поднимаем ОР СУЗ
    # устанавливаем в положение как на МКУ
    suz=[1.03,1.03,1.03,1.03,1.03,1.03,   1.03,1.03,1.03,1.03,1.03,0.6]
    soc['#YS#YSHGRP']=suz
    step(36)
    
    # отлкючаем статитку
    soc['#YM#YMFLAGSTAT']=0 # включаем статику
    step()
    # отключаем внешний бор
    soc['YZBORREG']=0   # включаем внешнее управление бором
    step()
    # состояние для МКУ по зоне установлено
    
    
    
    soc['10KBC16BB001_LevSet']=8       # уровень в баке
    soc['10KBC16BB001_LevTunOn']=1     # начать наполнение
    soc['10KBC17BB001_LevSet']=8       # уровень в баке
    soc['10KBC17BB001_LevTunOn']=1     # начать наполнение
    # наполняем компенсатор
    soc['10JEF10BB001_LevSet']=5.1     # уровень в компенсаторе
    soc['10JEF10BB001_LevTunOn']=1     # начать наполнение
    # шагаем, пока заполняется КД
    step(16)
    soc['10KBA10BB001_LevSet']=2.2     # уровень в деаэраторе
    soc['10KBA10BB001_LevTunOn']=1     # начать наполнение
    
    
    print_model(soc, "Загрузка концентраций Xe и J")
    
        
    
    # загружаем концентрации Xe и Jod которые будут после несокльких часов срабатывания после АЗ
    xen, jod, c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14 = load_xe_jod_rp_to_model(fname_time_in_hour_after_az=r"xen_jod_{}.lod".format( int(begining_after_az) ), folderpath=folderpath)
    soc['#YM#YMRO_XEN'] = xen
    soc["#YM#YMRO_JOD"] = jod
    soc['#YM#YMRP_C01'] = c1
    soc['#YM#YMRP_C02'] = c2
    soc['#YM#YMRP_C03'] = c3 
    soc['#YM#YMRP_C04'] = c4 
    soc['#YM#YMRP_C05'] = c5 
    soc['#YM#YMRP_C06'] = c6 
    soc['#YM#YMRP_C07'] = c7 
    soc['#YM#YMRP_C08'] = c8 
    soc['#YM#YMRP_C09'] = c9 
    soc['#YM#YMRP_C10'] = c10
    soc['#YM#YMRP_C11'] = c11
    soc['#YM#YMRP_C12'] = c12
    soc['#YM#YMRP_C13'] = c13
    soc['#YM#YMRP_C14'] = c14

    step(4)
    soc["#YM#ymtime_re8"] = float(soc["MKU_t_after_shut_down"])*3600 # сбрасываем время
    soc["#YM#ymtime"] = float(soc["MKU_t_after_shut_down"])*3600 # сбрасываем время
    soc.step(16)


    
    # Запуск внешней программы МКУ01
    soc['MKU01_FLAG']=1
    print_model(soc, "Запуск программы MKU01")
    soc.step(8)

    
    
    soc['reset_all'] = True  # Сброс всей сигнализации по защитам и блокировкам
    soc['YZREGRPINS_KEY'] = 0 # дезактивацмя ввода положения группы в процентах
    step(4)
    soc['10KBC16BB001_LevTunOn']=0 # заканчиваем наполнять баки дистиллятом
    soc['10KBC17BB001_LevTunOn']=0 # заканчиваем наполнять баки дистиллятом
    soc['10JEF10BB001_LevTunOn']=0 # заканчиваем наполнять компенсатор
    soc['10KBA10BB001_LevTunOn']=0 # заканчиваем наполнять деаэратор


    soc['K1_H_AccelBor']=15
    soc['#YM#YMFAST']=15
    soc.step(30)


    # включаем блокировки защит
    soc['KEY1_SHUNT_AZ1'] = 0
    soc['KEY_SHUNT_URB'] = 0
    soc['KEY_SHUNT_PZ1'] = 0
    soc['KEY_SHUNT_PZ2'] = 0

    print_model(soc, "Работа в ручном режиме")

    # Завершение выхода в критику, останов подпитки ЧК
    while  float( soc['#YM#YMDRNEW_PROC'])<0.2 :
        step(16)
    # замедляемся, чтобы в ручном режиме успеть все сделать
    soc['K1_H_AccelBor']=10
    soc['#YM#YMFAST']=10

    # Завершение выхода в критику, останов подпитки ЧК
    while  float( soc['#YM#YMDRNEW_PROC'])<0.1 :
        step(16)           
    # замедляемся, чтобы в ручном режиме успеть все сделать
    soc['K1_H_AccelBor']=5
    soc['#YM#YMFAST']=5
    soc.step(30)

    while  float( soc['#YM#YMDRNEW_PROC'])<0 :
        step(16)
    # замедляемся, чтобы в ручном режиме успеть все сделать  
    soc['K1_H_AccelBor']=1
    soc['#YM#YMFAST']=1           
    # отобразим флаг выхода на МКУ
    soc["MKU01_START_SCRIPT_POSLE_AZ_MANUAL"] = 0
    print_model(soc, "Сценарий завершен!")
    
    
    
    
if __name__ == "__main__":    
   try:
       soc=Tscript_drv("tcp://localhost:5555")
       #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
       #folderpath=Path.cwd()
       block= int(float(soc["#YM#YMBLOCKNUMBER"]))
       kamp = int(float(soc["#YM#YMLOADNUMBER"]))
       folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)

       grafs_obj = grafs_dynamic(soc=soc, folderpath=folderpath)
       vixod_na_mku_posle_az_manual(soc,folderpath=folderpath)
   except:    
       print_model(soc, "Ошибка выполнения сценария!!!")


    
    
    
    