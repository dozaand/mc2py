# Сценарий подготовки входных данных для выхода в критику после срабатывания АЗ
# Сценарий позволяет создать заивисмость крити.концентрации борной кислоты от времени после сброса АЗ
# Так же подготавливает Xe поля для работы модели

from mc2py.zmq_script_client import *
import math
import copy
import re
from operator import truth
from pathlib import Path
from print_model import print_model


def delete_folder(folderpath):
    """удаление папки с содержимым"""
    if folderpath.is_dir():
        for sub in folderpath.iterdir():
            if sub.is_dir() :
                delete_folder(sub) 
            else :
                sub.unlink()
        folderpath.rmdir() # if you just want to delete dir content, remove this line


def set_begin_state(soc, cb_start=8.14, folderpath=Path.cwd()):
    print_model ( soc, "Подготовка начального состояния")
    # включаем блокировки защит
    soc['KEY1_SHUNT_AZ1'] = 1
    soc['KEY_SHUNT_URB'] = 1
    soc['KEY_SHUNT_PZ1'] = 1
    soc['KEY_SHUNT_PZ2'] = 1
    soc['klarm_pos'] = 1  # *однократное отключение АРМ *
    soc['#YM#ymflag_xe'] = 1  # расчет динамики Xe в статике
    soc['#YM#YMRP_FAST']=1 # убираем ускорение расчета остаточного энеговыдленеия
    soc['#YM#ymfast'] = 1 # убираем ускорние расчета модели активной зоны
    soc['YZREGRPINS_KEY'] = 1 # активировать ввод положения групп в %
    soc.step(4)
    soc['#YM#YMFLAGSTAT']=1 # включаем статику
    soc.step()
    soc['YZBORREG']=1   # включаем внешнее управление бором
    soc.step()
    soc['YZBORMODE']=2  # бор в автомат
    soc.step()
    # установка температуры на входе в реактор
    soc["#YM#YMTMIX_IN_INPUT_USE"] = 1
    soc["#YM#YMTMIX_IN_INPUT"]= 285# soc["SVRK_THNAVR"]
    soc.step()
    soc["YZKEYLINK2"] = soc["YZKEYLINK2"] + 1 # переходим в НКС
    soc.step()
    soc['YZBORMODE']=2  # бор в автомат

    soc.step(4)
    # делаем укачку по реактивности
    while ( 0.01 < math.fabs( float(soc['#YM#YMDRNEW_proc']) )  ):
        soc.step()


    # сделаем иммитацию подаения АЗ
    soc['YZBORMODE']=1  # бор в дистанцию
    soc.step()
    soc['#YM#YMFLAGSTAT']=0 # выключаем статику
    soc.step(4)
    soc['#ym#ymtime']=0 # сбрасываем время
    soc['#ym#ymtime_re8']=0 # сбрасываем время
    suz=[0,0,0,0,0,0, 0,0,0,0,0,0]
    soc['#YS#YSHGRP']=suz
    soc.step(4)
    print_model(soc, "Группы сброшены")

    # ждем пока мощность упадет до МКУ
    while  (0.001 < float(soc['#YM#ymintpow_n'])):
        soc.step()

    # зафиксируем мощность, чтоб у нас было как-бы МКУ
    soc['#YM#ymflag_xe'] = 1  # расчет динамики Xe в статике
    soc.step()
    soc['#YM#YMFLAGSTAT']=1 # включаем статику
    soc.step()
    soc['YZBORREG']=1   # включаем внешнее управление бором
    soc.step()
    soc['YZBORMODE']=1  # бор в дистанцию
    soc.step(4)


    # запомним эти значения для последующей оценки реактивности от ксенона и времени
    xe_after_az  = copy.copy( soc['#YM#YMRO_XEN'] )
    jod_after_az = copy.copy( soc['#YM#YMRO_JOD'] )
    time_after_az = float(soc['#ym#ymtime_re8'])


    # теперь, не боясь срабатывания АЗ, поднимаем ОР СУЗ
    # устанавливаем в положение как на МКУ
    suz=[1.03,1.03,1.03,1.03,1.03,1.03,   1.03,1.03,1.03,1.03,1.03,0.6]
    soc['#YS#YSHGRP']=suz
    soc.step(4)
    print_model(soc, "Группы подняты")


    #----------------------------------------------------
    def get_dro_dc(fname = "dro_dcb_xe.dat"):
        """функция для расчета зависимости реактивности от концентрации борной кислоты"""
        # начнем последовательный ввод бора для оценки dro/dc
        print_model(soc, "Расчет dro/dc ({})".format(fname))
        soc['#YM#ymbor_cor'] = 0.5
        soc.step(100)

        ro_cb_lst=[]
        while float(soc['#YM#ymbor_cor'])<=15:
            betta = float( soc["#YM#ymabeta0"] )*100
            ro_betta=float(soc['#YM#YMDRNEW_proc'])/betta
            cb=float(soc['#YM#YMBOR_COR'])
            soc.step(100)

            ro_cb_lst.append( (cb, ro_betta) )
    
            soc['#YM#ymbor_cor']+=1    
            soc.step()

        # запись полученных значений оценки расхода в файл
        outpath = folderpath /  fname
        with open( str(outpath), 'w') as f:
            for el in ro_cb_lst:
              f.write( "{}\t{}\n".format(el[0], el[1]) )
    #----------------------------------------------------
    
    # расчет при Хе, соответствующему номиналу
    get_dro_dc(fname = "dro_dcb_xe_nom.dat")
    # расчет при Хе=0
    soc["#YM#ymflag_xeoff"]=1
    soc.step(4)
    soc["#YM#ymflag_xecalc"]=1
    soc.step(4)
    get_dro_dc(fname = "dro_dcb_xe_zero.dat")



    soc.step(10)
    # восстаналиваем значения ксенона
    soc['#YM#YMRO_XEN'] = xe_after_az[:] 
    soc['#YM#YMRO_JOD'] = jod_after_az[:]
    soc.step(4)
    soc['#YM#ymtime'] = 0
    soc['#YM#ymtime_re8'] = 0

    # введем 16 г/кг бора
    soc['#YM#ymbor_cor']=cb_start
    soc["#YM#YMTMIX_IN_INPUT"] = 285
    soc.step(100)








def save_xe_iod(time_in_hour_after_az):
    """Запись концентрации йода и ксенона в Lod файл"""
    fname = "xen_jod_{}.lod".format( int(time_in_hour_after_az) )
    outpath = folderpath /  fname

    xe=soc['#YM#YMRO_XEN']
    jod=soc['#YM#YMRO_JOD']

    c1 =soc['#YM#YMRP_C01']
    c2 =soc['#YM#YMRP_C02']
    c3 =soc['#YM#YMRP_C03']
    c4 =soc['#YM#YMRP_C04']
    c5 =soc['#YM#YMRP_C05']
    c6 =soc['#YM#YMRP_C06']
    c7 =soc['#YM#YMRP_C07']
    c8 =soc['#YM#YMRP_C08']
    c9 =soc['#YM#YMRP_C09']
    c10=soc['#YM#YMRP_C10']
    c11=soc['#YM#YMRP_C11']
    c12=soc['#YM#YMRP_C12']
    c13=soc['#YM#YMRP_C13']
    c14=soc['#YM#YMRP_C14']



    with open( str(outpath), 'w') as f:
        for index in range(1, 3261):
            f.write( "ymro_xen({})={}\n".format(index, xe[index-1]) )
        for index in range(1, 3261):
            f.write( "ymro_jod({})={}\n".format(index, jod[index-1]) )

        for index in range(1, 3261):
            f.write( "ymrp_c01({})={}\n".format(index, c1[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c02({})={}\n".format(index, c2[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c03({})={}\n".format(index, c3[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c04({})={}\n".format(index, c4[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c05({})={}\n".format(index, c5[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c06({})={}\n".format(index, c6[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c07({})={}\n".format(index, c7[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c08({})={}\n".format(index, c8[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c09({})={}\n".format(index, c9[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c10({})={}\n".format(index, c10[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c11({})={}\n".format(index, c11[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c12({})={}\n".format(index, c12[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c13({})={}\n".format(index, c13[index-1]) )
        for index in range(1, 3261):
            f.write( "ymrp_c14({})={}\n".format(index, c14[index-1]) )


    return xe, jod , c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14


def save_to_file(filename = "krit_from_time.dat",time_bor=[]):
    outpath = folderpath /  filename
    with open( str(outpath), 'w') as f:
        for el in time_bor:
            t = el[0]
            temp =el[1]
            cb_krit = el[2]
            stra="{}\t{}\t{}\n".format(t,temp,cb_krit)
            f.write(stra)

# накопим данные по критической концентрации


def get_cb_kritical_from_time(soc, temper):
    """Запись крит концентрации борной кислоты при ксеноновых колебаниях"""
    print_model(soc, "Начало расчета крит.концентрации борной кислоты от времени") #,float(soc['#ym#ymtime_re8']))

    # теперь ускорим расчет XE и пишем данные о крит. концентрации
    # зафиксируем мощность, чтоб у нас было как-бы МКУ
    soc['#YM#ymflag_xe'] = 1  # расчет динамики Xe в статике
    soc.step()
    soc['#YM#YMFLAGSTAT'] = 1  # включаем статику
    soc.step()
    soc['YZBORREG'] = 1  # включаем внешнее управление бором
    soc.step()
    soc['YZBORMODE'] = 2  # бор в автомат
    soc.step()
    soc["#YM#YMTMIX_IN_INPUT_USE"] = 1
    soc["#YM#YMTMIX_IN_INPUT"] = temper
    soc.step()
    # морозим Xe
    soc["#YM#ymflag_xecalc"]=0
    # делаем укачку по реактивности
    while (0.005 < math.fabs(float(soc['#YM#YMDRNEW_proc']))):
        soc.step(4)
    # морозим Xe
    soc["#YM#ymflag_xecalc"]=1
    soc['#ym#ymtime'] = 0
    soc['#ym#ymtime_re8'] = 0
    soc.step()
    soc['YMFAST']=50
    #soc.step()


    for t in range(n_hour):
        t_curr_sec = float(soc['ymtime_re8'])
        t_next_sec = t_curr_sec + 3600
        # записываем данные раз в час
        print_model(soc, "Расчет критической концентрации борной кислоты {}, ч".format(t) )
        temper_in_model = float(soc["#YM#YMTMIX_IN_INPUT"])
        res = [t_curr_sec, temper_in_model, float(soc["#YM#YMBOR_COR"])]
        time_bor.append(res) 
        t_hour = int( t_curr_sec/3600. )
        save_xe_iod( time_in_hour_after_az = t_hour )
        while ( t_curr_sec < t_next_sec ):
            t_curr_sec = float(soc['#YM#ymtime_re8'])
            soc.step(16)



def get_ro_xe_from_time(soc, fname, folderpath):
    """Запись реактивности при ксеноновых колебяниях"""
    print_model(soc, "Начало расчета реактивности от Xe")
    soc["#YM#YMFAST"] = 1
    soc.step()
    # зафиксируем мощность, чтоб у нас было как-бы МКУ
    soc['#YM#ymflag_xe'] = 1  # расчет динамики Xe в статике
    soc.step()
    soc['#YM#YMFLAGSTAT'] = 1  # включаем статику
    soc.step()
    soc['YZBORREG'] = 1  # включаем внешнее управление бором
    soc.step()
    soc['YZBORMODE'] = 1  # бор в дистанцию
    soc.step()
    soc['#YM#ymbor_cor'] = float(soc["MKU01_CB_START"])
    soc.step(30)



    betta = float(soc['ymabeta0'])*100

    time_roxe={}
    time_metka=0
    soc['#ym#ymtime'] = 0 # сбрасываем время
    soc['#ym#ymtime_re8'] = 0 # сбрасываем время
    soc.step()
    soc["#YM#YMFAST"] = 50
    while ( float(soc['#YM#ymtime_re8'])< 3600*n_hour ):
        # записываем данные раз в час
        if time_metka <= float(soc['#ym#ymtime_re8']):
            t_sec = float(soc['#ym#ymtime_re8'])
            t_hour = int( t_sec/3600. )
            print_model(soc, "Расчет реактивности {}, ч".format(t_hour))
            time_roxe[t_sec] = float(soc['#YM#ymdrnew_proc']) / betta
            time_metka += 3600
        soc.step(16) 
    soc["YMFAST"] = 1


    outpath = folderpath /  fname
    with open( str(outpath), 'w') as f:
        for t in sorted(time_roxe.keys()):
            ro_xe=time_roxe[t]
            stra="{}\t{}\n".format(t,ro_xe)
            f.write(stra)



#--------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    try:
        soc=Tscript_drv("tcp://localhost:5555")
        #soc.load_state(r"D:\NOVO-2\KARRU_NV\MODEL\STATE\KIRIN\B01_K01_Nominal_BOC_0eff_days.sta")
        # создаем директорию на рабочем столе под результаты
        block= int(float(soc["#YM#YMBLOCKNUMBER"]))
        kamp = int(float(soc["#YM#YMLOADNUMBER"]))
        folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)
        print_model(soc, "Директория с данными: {}".format(folderpath) )
        # удаляем старую директорию и что в ней
        delete_folder(folderpath)
        # создаем новую директорию
        folderpath.mkdir(parents=True, exist_ok=True)


        # отобразим флаг выхода на МКУ
        soc["MKU01_START_SCRIPT_PODGOTOVKA"] = 1

        cb_stoianochnaia =  float( soc['MKU01_CB_START'] ) # 8.14
        n_hour=48  # количесвто часов, в течение которых мы копим данные

        set_begin_state(soc, cb_start=cb_stoianochnaia, folderpath=folderpath)
        xe_after_az  =soc['#YM#YMRO_XEN']
        jod_after_az =soc['#YM#YMRO_JOD']
        с1_after_az  =soc['#YM#YMRP_C01']
        с2_after_az  =soc['#YM#YMRP_C02']
        с3_after_az  =soc['#YM#YMRP_C03']
        с4_after_az  =soc['#YM#YMRP_C04']
        с5_after_az  =soc['#YM#YMRP_C05']
        с6_after_az  =soc['#YM#YMRP_C06']
        с7_after_az  =soc['#YM#YMRP_C07']
        с8_after_az  =soc['#YM#YMRP_C08']
        с9_after_az  =soc['#YM#YMRP_C09']
        с10_after_az =soc['#YM#YMRP_C10']
        с11_after_az =soc['#YM#YMRP_C11']
        с12_after_az =soc['#YM#YMRP_C12']
        с13_after_az =soc['#YM#YMRP_C13']
        с14_after_az =soc['#YM#YMRP_C14']

        soc.step(4)
    
        # получение заивсимости крит концентрации борной кислоты от времени при изменении концентрации ксенона
        time_bor=[]
        temper_lst = [280,286]
        for temper in temper_lst:
            get_cb_kritical_from_time(soc, temper=temper)
            soc['#YM#YMRO_XEN'] = xe_after_az[:]
            soc['#YM#YMRO_JOD'] = jod_after_az[:]
            soc['#YM#YMRP_C01']=с1_after_az[:]
            soc['#YM#YMRP_C02']=с2_after_az[:]
            soc['#YM#YMRP_C03']=с3_after_az[:]
            soc['#YM#YMRP_C04']=с4_after_az[:]
            soc['#YM#YMRP_C05']=с5_after_az[:] 
            soc['#YM#YMRP_C06']=с6_after_az[:]
            soc['#YM#YMRP_C07']=с7_after_az[:] 
            soc['#YM#YMRP_C08']=с8_after_az[:] 
            soc['#YM#YMRP_C09']=с9_after_az[:] 
            soc['#YM#YMRP_C10']=с10_after_az[:]
            soc['#YM#YMRP_C11']=с11_after_az[:]
            soc['#YM#YMRP_C12']=с12_after_az[:]
            soc['#YM#YMRP_C13']=с13_after_az[:]
            soc['#YM#YMRP_C14']=с14_after_az[:]
        save_to_file("krit_from_time.dat", time_bor= time_bor)

        soc.step(4)
    
        # получение зависимости реактинвости от времени при изменении концентрации ксенона
        get_ro_xe_from_time(soc, fname="ro_xe_from_time.dat", folderpath=folderpath)

        print_model(soc, "Сценарий завершен!")

        # отобразим флаг выхода на МКУ
        soc["MKU01_START_SCRIPT_PODGOTOVKA"] = 0

    except:    
        print_model(soc, "Ошибка выполнения сценария!!!")




