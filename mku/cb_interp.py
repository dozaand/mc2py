import numpy as np
from pathlib import Path

class cb_interp():
    """интерполяция крит. концентрации борной кислоты по времени и температуре"""
    def __init__(self, folderpath):
        outpath = folderpath /  "krit_from_time.dat"
        krit_from_time=np.genfromtxt( str(outpath) )
        krit_from_time=krit_from_time.transpose()

        temp=krit_from_time[1]
        # найдем уникальные размерности массивов
        self.xsize = len(np.unique(temp))
        self.ysize = int( len(temp)/self.xsize )
        # зделаем разбиение массивов
        self.time_sec=krit_from_time[0].reshape((self.xsize, self.ysize))
        self.time_hour=(krit_from_time[0]/3600).reshape((self.xsize, self.ysize))
        self.temp=krit_from_time[1].reshape((self.xsize, self.ysize))
        self.cbor=krit_from_time[2].reshape((self.xsize, self.ysize))


    def cb_interp(self,time_curr_sec, temp_curr):
        """интерполяция крит концентрации борной кислоты """
        cb_lst=[]
        temp_lst = []
        for i in range(self.xsize):
            cb=np.interp(time_curr_sec, self.time_sec[i], self.cbor[i]) 
            #print(cb)
            cb_lst.append(cb)
            temp_val = self.temp[i][0]
            #print(temp_val)
            temp_lst.append(temp_val)
        #print (cb_lst)
        #print (temp_lst)

        cb_result = np.interp(temp_curr, temp_lst, cb_lst) 
        return cb_result

    def get_from_temp(self,temper):
        """вернем список всех значений крит концентрации при данной температуре"""
        cb_lst=[]
        time_sec_lst = self.time_sec[0]
        for t_sec in self.time_sec[0]:
            cb_krit = self.cb_interp(t_sec, temper)
            cb_lst.append(cb_krit)
        return time_sec_lst, cb_lst







if __name__ == "__main__":  
    from mc2py.zmq_script_client import *  
    soc=Tscript_drv("tcp://localhost:5555")
    #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
    #folderpath=Path.cwd()
    block= int(float(soc["#YM#YMBLOCKNUMBER"]))
    kamp = int(float(soc["#YM#YMLOADNUMBER"]))
    folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)
    cb_obj = cb_interp(folderpath)
    time_curr_sec = 5000
    temp_curr=285
    res = cb_obj.cb_interp(time_curr_sec=time_curr_sec, temp_curr=temp_curr)
    print (time_curr_sec, temp_curr, res)
    
    
    from mpl_toolkits.mplot3d import axes3d
    import matplotlib.pyplot as plt
    from matplotlib import cm
    import copy    

    time_hour_min=min(cb_obj.time_hour[0])
    time_hour_max=max(cb_obj.time_hour[-1])

    temp_min=min(cb_obj.temp[0])
    temp_max=max(cb_obj.temp[-1])

    #x = y = np.arange(-3.0, 3.0, delta)
    x = np.arange(time_hour_min, time_hour_max)
    y = np.arange(temp_min, temp_max)
    X, Y = np.meshgrid(x, y)

    Z=copy.copy(X)
    for i in range( len(X) ):
        for k in range( len(X[0]) ):
            t=X[i][k]
            temp=Y[i][k]
            val = cb_obj.cb_interp(t*3600, temp)
            Z[i][k]=val


    #----------------------
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    ax.set_xlabel('Время после срабатывания АЗ, ч')
    ax.set_ylabel('Температура теплоносителя, °С')
    ax.set_zlabel('Критическая концентрация\n борной кислоты, г/кг')

    plt.show()






    # построение графиков
    fig, ax = plt.subplots()
    fig.set_size_inches(9, 6)

    t,cb280 = cb_obj.get_from_temp(temper=280)
    t,cb286 = cb_obj.get_from_temp(temper=286)
    t=t/3600

    line1, = ax.plot(t, cb280, "-", label=r"T = 280 $^0$C")
    line1, = ax.plot(t, cb286, "--", label=r"T = 286 $^0$C")
    ax.legend()
    ax.grid(True)
    ax.set_xlabel('Время после сброса АЗ, час')
    ax.set_ylabel('Критическая концентрация борной кислоты, г/кг')
    plt.show()
