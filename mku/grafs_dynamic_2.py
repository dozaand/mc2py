from mc2py.zmq_script_client import *
import numpy as np
from pathlib import Path
from scipy.interpolate import interp1d
from g_obj import g_class
from v_predel import v_predel_class
from scipy.interpolate import UnivariateSpline
from cb_interp import cb_interp


class grafs_dynamic():
    """Запитка графиков при выходе на МКУ после АЗ"""
    def __init__(self, soc =Tscript_drv("tcp://localhost:5555"), folderpath=Path.cwd()):

        self.soc= soc


        self.folderpath = folderpath
        self.n_interv =100
        
        # объект для интерполирования значения крит. концентрации борной кислоты по времени и температуре
        self.cb_interp_obj = cb_interp(self.folderpath)
        temper= float(soc["SVRK_THNAVR"])
        self.time_xe_sec, self.cbor_xe = self.cb_interp_obj.get_from_temp(temper)
        self.time_xe_hour = self.time_xe_sec/3600
        self.time_cbor_spline = UnivariateSpline(self.time_xe_hour , self.cbor_xe)
        self.time_cbor_spline.set_smoothing_factor(0.01)
        time_hour_model = float(self.soc["MKU_t_after_shut_down"])
        minv = max(time_hour_model,self.time_xe_hour[0])
        maxv = min(self.time_xe_hour[-1], time_hour_model + 20)
        #print (minv, maxv)
        
    
    
        M1K = float(self.soc['mass_reaktor'])/1000.
        self.g_obj = g_class(M1K_tonn = M1K)
    
    
        self.time_metka_hour=float(self.soc["#YM#YMTIME_RE8"])/3600

        cb_model = float(self.soc['ymbor_cor'])

        time_xe_small, cbor_xe_small= self.get_interp(x=self.time_xe_hour,y=self.cbor_xe, minv=minv, maxv=maxv, npoints=self.n_interv)
        self.soc["YV_T_XE"]= time_xe_small
        self.soc["YV_CB_XE"] = cbor_xe_small
        self.soc["YV_CB_XE_N_POINTS"] = len(time_xe_small)

        self.soc["YV_T_XE_T_MIN"] = time_xe_small[0]
        self.soc["YV_T_XE_T_MAX"] = time_xe_small[-1]

        cbvals = list(cbor_xe_small) + [ float(self.soc['ymbor_cor']) ] + [ float(self.soc["MKU01_CB_START"]) ]
        self.soc["YV_T_XE_CB_MIN"] = min(cbvals) - 0.3
        self.soc["YV_T_XE_CB_MAX"] = max(cbvals) + 0.3 

        self.soc["YV_CB_XE_UPDATE_FLAG"] = float(self.soc["YV_CB_XE_UPDATE_FLAG"]) + 1


        #----------------
        cb_curr = float(self.soc['ymbor_cor'])
        g_reglament_lst = [self.g_obj.G_xe(cb_curr=cb_curr, time_hour=t_hour, temper=temper) for t_hour in time_xe_small]
        self.soc["YV_T_G_XE"]=time_xe_small
        self.soc["YV_G_XE"]=g_reglament_lst
        self.soc["YV_G_XE_N_POINTS"]=len(time_xe_small)
        
        self.soc["YV_T_G_RECOMEND"]=time_xe_small
        self.soc["YV_G_RECOMEND"]=np.array(g_reglament_lst) + 10
        self.soc["YV_G_RECOMEND_N_POINTS"]=len(time_xe_small)


        dc_dt_lst=[ self.time_cbor_spline(t_hour,1) for t_hour in time_xe_small]
        self.soc["YV_dCB_dt"]=dc_dt_lst


        self.soc["YV_T_G_T_MIN"]=time_xe_small[0]
        self.soc["YV_T_G_T_MAX"]=time_xe_small[-1]
        self.soc["YV_T_G_G_MIN"]= 0
        self.soc["YV_T_G_G_MAX"]= 50


        self.v_predel_obj = v_predel_class(M1K_tonn = M1K)    

        self.soc["MKU_dro_dcb"]=self.v_predel_obj.dro_dcb
        betta = float(self.soc["ymabeta0"])*100
        self.soc["MKU_dro_dcb_proc"]=self.v_predel_obj.dro_dcb*betta



        self.soc["YV_T_G_LIMIT"]= time_xe_small
        self.soc["YV_G_LIMIT"]=  np.zeros(len(time_xe_small)) 
        self.soc["YV_G_LIMIT_N_POINTS"]=len(time_xe_small)

        v_ro_lst = np.zeros(len(time_xe_small)) 
        for ind, t in enumerate(time_xe_small):
            with_xe, without_xe = self.v_predel_obj.V_predel(t)
            v_ro_lst[ind] = with_xe

        self.soc["YV_V_RO_LIMIT"] = v_ro_lst

        self.soc["YV_G_UPDATE_FLAG"] = float(self.soc["YV_G_UPDATE_FLAG"]) + 1        


        #--------------
        self.soc["MKU_GRAFS_FLAG"]=True

    

    def get_interp(self, x,y, minv,maxv, npoints): 
        """Создает массивы с равномерной сеткой для нужного числа точек"""
        delta_x = (maxv - minv)/npoints
        interp = interp1d(x,y)
        x_small=[]
        y_small=[]
        for i in range(npoints):
            x_curr = minv + delta_x*i
            x_small.append(x_curr)
            y = interp(x_curr)
            y_small.append(y)
        return np.array(x_small), np.array(y_small)
    


    
if __name__ == "__main__":
    soc=Tscript_drv("tcp://localhost:5555")

    block= int(float(soc["#YM#YMBLOCKNUMBER"]))
    kamp = int(float(soc["#YM#YMLOADNUMBER"]))
    folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)
    
    #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
    #folderpath=Path.cwd()
    outpath = folderpath /  "krit_from_time.dat"
    
    grafs_obj = grafs_dynamic(soc=soc, folderpath=folderpath)
    #grafs_obj.step()
    #grafs_obj.soc.step(1)


