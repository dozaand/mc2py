import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from ocenka_ro_cb import get_dro_dc
from mc2py.zmq_script_client import *


class v_predel_class():
    """Расчет пределов по скоростям ввода реактивности"""
    def __init__(self, M1K_tonn):
        soc=Tscript_drv("tcp://localhost:5555")
        #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
        #folderpath=Path.cwd()
        block= int(float(soc["#YM#YMBLOCKNUMBER"]))
        kamp = int(float(soc["#YM#YMLOADNUMBER"]))
        folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)


        outpath = folderpath /  "ro_xe_from_time.dat"
        time_roxe=np.genfromtxt( str(outpath) )
        time_roxe = time_roxe.transpose()
        t_roxe_min=time_roxe[0]/60
        t_roxe_hour=time_roxe[0]/3600
        ro_xe=time_roxe[1]
        
        self.f_ro_min = UnivariateSpline(t_roxe_min, ro_xe)
        self.f_ro_min.set_smoothing_factor(0.01)

        # коэф чувствительности реактивности к концентрации борной кислты в реакторе
        self.dro_dcb=get_dro_dc(fname="dro_dcb_xe_nom.dat", folderpath=folderpath)

        self.M1K_tonn = M1K_tonn # масса теплоносителя 1 контура, т

	

    def V_predel(self, time_hour):
        """Расчет пределов скоростей ввода реактивности"""
        V_max = 0.02  # бэтта/мин  ограничение ТРБЭ
        V_xe = self.V_xe(time_hour) # бэтта/мин
        with_xe = (V_max - V_xe )*0.95 # учет погрешности
        without_xe = V_max
        return with_xe, without_xe

    def V_xe(self, time_hour):
        """Расчет скорости ввода реактивности из-за Xe, бэтта/мин"""
        t_min = time_hour*60
        V_xe = self.f_ro_min(t_min,1)  # берем 1 производную от функции
        return V_xe


    def V_cb(self, cb, dt_hour, G_tonn_per_hour):
        """Расчет скорости ввода положительной реактивности за счет ввода ЧК, бэтта/мин"""
        G_tonn_min = G_tonn_per_hour/60
        G = G_tonn_per_hour
        M1K = self.M1K_tonn
        res = -self.dro_dcb*G_tonn_min/M1K * cb * np.exp(-G*dt_hour/M1K)
        return res # бэтта/мин

    def g_max_possible(self, cb, t_hour, delta_g = 0.1):
        """Расчет максимально допустимого расхода"""
        v_predel = self.V_predel(t_hour)
        v_predel_with_xe = v_predel[0]
        G=0
        vcb = self.V_cb(cb=cb, dt_hour=1, G_tonn_per_hour=G)
        while vcb < v_predel_with_xe:
            vcb = self.V_cb(cb=cb, dt_hour=1, G_tonn_per_hour=G)
            g_max=G
            G = G + delta_g

        return g_max


if __name__ == "__main__":    
    v_predel_obj = v_predel_class(M1K_tonn = 227)
    g = v_predel_obj.g_max_possible(cb=8, t_hour=55)
    print (g)

