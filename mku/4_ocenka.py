import numpy as np
import math
from pathlib import Path
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
from scipy.interpolate import UnivariateSpline
from g_obj import g_class
from v_predel import v_predel_class
from cb_interp import cb_interp
from mc2py.zmq_script_client import *






class Cb_predict():
    def __init__(self, soc):
        """Инициализация данных"""        
        #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
        #folderpath=Path.cwd()
        block= int(float(soc["#YM#YMBLOCKNUMBER"]))
        kamp = int(float(soc["#YM#YMLOADNUMBER"]))
        self.folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)

        self.time_begining_after_az = float(soc["MKU_t_after_shut_down"])
        self.cbstart = float(soc["MKU01_CB_START"])      # 8.14
        self.G1 = float(soc["MKU01_CONDENSAT_G1"])       # 20
        self.wait = float(soc["MKU01_t_peremew"])        # 2
        self.G2 =  float(soc["MKU01_CONDENSAT_G2"])      # 10
        self.M1K = float(soc['mass_reaktor'])/1000.
        self.temper= float(soc["SVRK_THNAVR"])
        
        self.cbstoynka = float(soc["MKU01_CB_START"])      # стояночная концентрация борной кислоты# 8.14
        self.cbbeg = 5.114 # борная кислота на номинале до падения АЗ
        self.time_vvod_bor = 2.5 # время ввода бора до стояночной концентрации
        
        
        # объект для интерполирования значения крит. концентрации борной кислоты по времени и температуре
        cb_interp_obj = cb_interp(self.folderpath)
        self.time_sec_lst, self.cbor = cb_interp_obj.get_from_temp(self.temper)
        self.time_cbor_hour = np.array(self.time_sec_lst)/3600

        self.time_hour_lst=[]
        self.cb_curr_lst=[]
        self.cbkirt_lst=[]



    def clear_lst(self):
        """очистка списков с данными"""
        self.time_hour_lst=[]
        self.cb_curr_lst=[]
        self.cbkirt_lst=[]

    def curr_bor(self, cb0, G, delta_t_hour, M=250):
        """Определение концетрации борной кислоты в зависимости от времени"""
        cb_curr = cb0* np.exp(-G*delta_t_hour/M)
        return cb_curr


    def append_cb(self, time, cb_curr, cbcrit):
        """добавление данных о времени, текущем значении борной кислоты и текущей критической концентрации"""
        self.time_hour_lst.append(time)
        self.cb_curr_lst.append(cb_curr)
        self.cbkirt_lst.append(cbcrit)


    def get_cb_crit_final(self, t_hour):
        """получить заключительную оценку по крит концентрации борной кислоты"""
        # критическая концентрация борной кислоты которую будем получать в конце расчета
        self.cb_krit_final = np.interp(t_hour, self.time_cbor_hour, self.cbor)  
        return self.cb_krit_final
        

    def iteration(self):
        """Итеррирование для нахождения критической концентрации борной кислоты"""
        delta_t=0.01 # шаг по времени
        delta_cb = 1000 # условная разница для начала
        cb_krit_final = self.get_cb_crit_final(t_hour = 0)  # критическая концентрация борной кислоты которую будем получать в конце расчета
        cb_begin_peremew = self.cbstart # концнетрация борной кислоты на начало перемешивания

        delta_cb = 0 #пусть ервый раз проходит всегда #cb_begin_peremew - cb_krit_final
        
        i=0
        while delta_cb<1.2 and i<20:
            self.clear_lst() # очистка списка с данными
            time=0       # начальное время

            # ввод борной кислоты
            deltacb = (self.cbstoynka - self.cbbeg)/self.time_vvod_bor
            while time < self.time_vvod_bor:
                cbcurr = self.cbbeg + deltacb*time
                cbkrit = self.get_cb_crit_final(t_hour=time)
                time = time + delta_t
                self.append_cb(time, cbcurr, cbkrit)



            # время выдержки после АЗ - борная кислота без изменнений
            # вводим ОР СУЗ
            while time < self.time_begining_after_az:
                cbcurr = self.cbstart
                cbkrit = self.get_cb_crit_final(t_hour=time)
                time = time + delta_t
                self.append_cb(time, cbcurr, cbkrit)
            
            # идет подпитка с большим расходом
            cbprev = self.cbstart
            cbkrit = self.get_cb_crit_final(t_hour=time)
            delta = cbprev - cbkrit
            while delta>1.2:
                 cbcurr = self.curr_bor(cbprev, self.G1, delta_t, M=self.M1K)
                 if (i==0):
                    cbkrit = self.get_cb_crit_final(t_hour=time)
                 else:
                    cbkrit = cb_krit_final

                 delta = cbcurr - cbkrit
                 time = time + delta_t 

                 cbkrit = self.get_cb_crit_final(t_hour=time)
                 self.append_cb(time, cbcurr, cbkrit)
                 cbprev= cbcurr
            cb_begin_peremew = cbcurr # концентрация борно кислоты в момент начала перемешивания
    
    
            # перемешивание
            time_wait = time + self.wait
            while time < time_wait:
                 cbcurr= cbcurr
                 cbkrit = self.get_cb_crit_final(t_hour=time)
                 time = time + delta_t
                 self.append_cb(time, cbcurr, cbkrit)
    
    
    
            # подпитка ЧК с мылам расходом в пусковом диапазоне
            self.t_pusk_interval_beg = time # момент начала пускового интервала
            cbprev = cbcurr
            cbkrit = self.get_cb_crit_final(t_hour=time)
            delta = cbprev - cbkrit
            while delta>0:
                 cbcurr = self.curr_bor(cbprev, self.G2, delta_t, M=self.M1K)
                 cbkrit = self.get_cb_crit_final(t_hour=time)
                 delta = cbcurr - cbkrit
                 time = time + delta_t
                 self.append_cb(time, cbcurr, cbkrit)
                 cbprev = cbcurr

            cb_krit_final = cbcurr
            delta_cb = cb_begin_peremew - cb_krit_final
            i=i+1

            print("Сb перемешивание:",cb_begin_peremew,",\tCb критика: ", cb_krit_final, ",\tРазница Cb:", delta_cb)
        

soc = Tscript_drv("tcp://localhost:5555")
objcb = Cb_predict(soc)
objcb.iteration()

 #------------------------------------------------------------------------
outpath = objcb.folderpath /  "t{}_g{}_g{}.txt".format( int(objcb.time_begining_after_az), int(objcb.G1), int(objcb.G2))
with open( str(outpath), "w") as f:
    for ind in range(len(objcb.time_hour_lst)):
        stra = str(objcb.time_hour_lst[ind]) + "\t" + str(objcb.cb_curr_lst[ind]) +"\n"
        f.write( stra )

outpath = objcb.folderpath /  "cb_krit.txt"
with open( str(outpath), "w") as f:
    for ind in range(len(objcb.time_hour_lst)):
        stra = str(objcb.time_hour_lst[ind]) + "\t" + str(objcb.cbkirt_lst[ind]) + "\n"
        f.write( stra )
#------------------------------------------------------------------------


soc["MKU01_CB_CRITICAL"]=objcb.cb_curr_lst[-1]
#------------------------------
#soc["MKU01_t_dlitelnos"] = time_hour_lst[-1]
#soc["MKU01_prognoz_cb"] = cb_curr_lst[-1]


#------------------------------------------------------------------------
# построение графиков
fig, ax = plt.subplots()
fig.set_size_inches(9, 6)
line2, = ax.plot(objcb.time_hour_lst, objcb.cb_curr_lst, "-g", label="Текущая концентрация")
line1, = ax.plot(objcb.time_hour_lst, objcb.cbkirt_lst, "--m", label="Критическая концентрация")
###ax.legend()
ax.grid(True)
ax.set_xlabel('Время после сброса АЗ, ч')
ax.set_ylabel('Концентрация борной кислоты в реакторе, г/кг')
jpgpath = objcb.folderpath /  "cb_t{}_g{}_g{}.png".format( int(objcb.time_begining_after_az), int(objcb.G1), int(objcb.G2) )
plt.savefig( str(jpgpath) )
plt.show()
#------------------------------------------------------------------------


g_obj = g_class(M1K_tonn = objcb.M1K)
time_cbor_spline = UnivariateSpline(objcb.time_cbor_hour, objcb.cbor)
time_cbor_spline.set_smoothing_factor(0.01)
cbor_spl = [time_cbor_spline(thour) for thour in objcb.time_cbor_hour]

#------------------------------------------------------------------------------------------------------------
# построение графиков
fig, ax = plt.subplots()
fig.set_size_inches(9, 6)
line1, = ax.plot(objcb.time_cbor_hour, objcb.cbor, "-", label="Расчет")
line1, = ax.plot(objcb.time_cbor_hour, cbor_spl, "--", label="Сплайн аппроксимация")
ax.legend()
ax.grid(True)
ax.set_xlabel('Время после сброса АЗ, ч')
ax.set_ylabel('Критическая концентрация борной кислоты, г/кг')
plt.show()

#------------------------------------------------------------------------------------------------------------
# построение графиков
#cbor_spl = [time_cbor_spline(thour,1) for thour in objcb.time_cbor_hour]
#fig, ax = plt.subplots()
#fig.set_size_inches(9, 6)
#line1, = ax.plot(objcb.time_cbor_hour, cbor_spl)#, "-", label="Критическая концентрация борной кислоты. Сплайн")
#ax.grid(True)
#ax.set_xlabel('Время после сброса АЗ, ч')
#ax.set_ylabel('Скорость изменения\nкритической концентрации борной кислоты\nдля компенсации отравления Xe, (г/кг)/ч')
#plt.show()

#------------------------------------------------------------------
v_predel_obj = v_predel_class(M1K_tonn = objcb.M1K)


g_lst=[]
g_reglament_lst=[]
g_reglament_plus_ten_lst=[]
t_lst=[]
vcb_lst=[]
vxe_lst=[]
vfull_lst=[] # суммарная реактивность вводимая за счет ввода ЧК и за счет Xe процессов

for ind in range( len(objcb.cb_curr_lst) - 1):
    tprev = objcb.time_hour_lst[ind]
    cbprev = objcb.cb_curr_lst[ind] 
    tcurr = objcb.time_hour_lst[ind+1]
    cbcurr = objcb.cb_curr_lst[ind+1]


    dt = tcurr- tprev
    G = g_obj.G(cbcurr,cbprev, dt)

    if tcurr< objcb.time_begining_after_az:
        g_lst.append(0)
    else:
        g_lst.append(G)

    g_xe = g_obj.G_xe(cb_curr=cbcurr, time_hour=tcurr, temper=objcb.temper)   # Расход на компенсацию Xe отравления
    g_reglament_lst.append(g_xe)
    g_xe_ten = g_xe + 10 # более на 10 тонн
    g_reglament_plus_ten_lst.append(g_xe_ten)

      #--------
    vcb = v_predel_obj.V_cb(cbprev, dt, G)
    vxe = v_predel_obj.V_xe(tcurr) 
    vfull = vcb + vxe

    t_lst.append(tcurr)
    if tcurr< objcb.time_begining_after_az:
        vcb_lst.append(0)
        vxe_lst.append(vxe)
        vfull_lst.append(vxe)
    else:
        vcb_lst.append(vcb)
        vxe_lst.append(vxe)
        vfull_lst.append(vfull)


#--------------------
# Оценка скоростей
V_predel_lst=[]
V_predel_bez_Xe_lst=[]
for t_hour in objcb.time_hour_lst:
    with_xe, without_xe = v_predel_obj.V_predel(t_hour)
    V_predel_lst.append(with_xe)
    V_predel_bez_Xe_lst.append(without_xe)

#------------------------------------------------------------------------
# построение графиков
fig, ax = plt.subplots()
fig.set_size_inches(9, 6)
line1, = ax.plot(objcb.time_hour_lst, V_predel_bez_Xe_lst, ":r", label="Предел по скорости ввода в пусковом интервале")
line2, = ax.plot(t_lst, vcb_lst, "--",  label="За счет ввода ЧК")
line3, = ax.plot(t_lst, vxe_lst, "-.", label="За счет Xe процессов")
line4, = ax.plot(t_lst, vfull_lst, "-", color='purple', label="За счет ввода ЧК и Xe процессов ")

#line2, = ax.plot(time_hour_lst, V_predel_bez_Xe_lst, "-.", label="Предел по скорости ввода")
#line3, = ax.plot(time_hour_lst, V_predel_lst, "--r", label="Предел по скорости ввода с учетом Xe")
#line4, = ax.plot(time_hour_lst, V_predel_bez_Xe_lst, "-.", color="orange", label="Предел по скорости ввода без учета Xe")
###ax.legend()
ax.grid(True)
ax.set_ylim(-0.03,0.06)
ax.set_xlabel('Время после сброса АЗ, ч')
ax.set_ylabel(r'Скороcть ввода реактивности, $\beta_{эфф}$/мин', fontsize=15)
jpgpath = objcb.folderpath /  "v_max_t{}_g{}_g{}.png".format( int(objcb.time_begining_after_az),int(objcb.G1), int(objcb.G2) )
plt.savefig( str(jpgpath) )
plt.show()
#------------------------------------------------------------------------


t_hour_short1=[]
g_max_short1=[]
t_hour_short2=[]
g_max_short2=[]

t_hour_range = np.arange(objcb.time_hour_lst[0], objcb.time_hour_lst[-1], 0.2)
g_max=[]#[None] * len(t_hour_range)
for ind,t_hour in enumerate(t_hour_range):
    cb = np.interp(t_hour, objcb.time_hour_lst, objcb.cb_curr_lst)
    g = v_predel_obj.g_max_possible(cb, t_hour)
    g_max.append(g)

    if objcb.t_pusk_interval_beg < t_hour:
        t_hour_short2.append(t_hour)
        g_max_short2.append(g)
    else:
        t_hour_short1.append(t_hour)
        g_max_short1.append(g)







#------------------------------
# построение графиков
g_max_val = max(g_lst + g_max + [50])
fig, ax = plt.subplots()
fig.set_size_inches(9, 6)
line1, = ax.plot(t_lst, g_lst, "-", color="purple", label="Текущий расход")
line2, = ax.plot(t_lst, g_reglament_lst, "--",  label="Расход на компенсацию Xe отравления")
line3, = ax.plot(t_lst, g_reglament_plus_ten_lst, "-.", color="orange", label="Допустимый расход в пусковом интервале")
#line4, = ax.plot(t_hour_range, g_max, "-*r", label="Максимально возможный расход в пусковом интервале")
###line5, = ax.plot(t_hour_short1, g_max_short1, "-*k")
line6, = ax.plot(t_hour_short2, g_max_short2, "-*r", label="Максимально возможный расход в пусковом интервале")



###ax.legend(loc='upper right')
ax.grid(True)
ax.set_xlabel('Время после сброса АЗ, ч')
ax.set_ylabel('Расход чистого конденсата, т/ч')
###ax.set_ylim(0,g_max_val)
ax.set_ylim(0,50)
jpgpath = objcb.folderpath /  "g_t{}_g{}_g{}.png".format( int(objcb.time_begining_after_az),int(objcb.G1), int(objcb.G2) )
plt.savefig( str(jpgpath) )
plt.show()


