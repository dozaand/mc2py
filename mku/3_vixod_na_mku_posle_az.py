# Сценарий по выводу блока на МКУ

from mc2py.zmq_script_client import *
import numpy as np
import math
import re
from operator import truth
import matplotlib.pyplot as plt
from pathlib import Path
from print_model import print_model
from grafs_dynamic_2 import grafs_dynamic
from load_xe_jod_rp import load_xe_jod_rp_to_model
from cb_interp import cb_interp
import glob

def vixod_na_mku_posle_az(soc, folderpath):
    """Выходна МКУ после АЗ"""

    # загружаем состояние на МКУ
    #soc.load_state(r"D:\NOVO-2\KARRU_NV\MODEL\STATE\Olga\B01_K01_MCL_BOC_0_days_GENOFF_P1-2K-NORMA.sta")
    print_model(soc, "Выход на МКУ")

    # удаляем файлы в папке с программой MKU01, чтобы они не мешали ей работать
    mku_folder ="C:\ENIKOTSO\output\mku01\MKU_DATA"
    pth = Path(mku_folder)
    for child in pth.glob('*'):
        if child.is_file():
            child.unlink()

    # отобразим флаг выхода на МКУ
    soc["MKU01_START_SCRIPT_POSLE_AZ"] = 1


    
    # время начала выхода на МКУ после АЗ, часы
    begining_after_az= float(soc["MKU_t_after_shut_down"]) #4
    
    # объект для интерполирования значения крит. концентрации борной кислоты по времени и температуре
    cb_interp_obj = cb_interp(folderpath)




    def step(nstep=1):
        #curr_time =float(soc["#YM#ymtime_re8"])
        #temper= float(soc["SVRK_THNAVR"])
        #cb_crit = cb_interp_obj.cb_interp(curr_time, temper)
        #soc['MKU01_CB_CRITICAL'] = cb_crit        
        soc.step(nstep)


    
    
    temper= float(soc["SVRK_THNAVR"])
    soc['MKU01_CB_CRITICAL'] = cb_interp_obj.cb_interp(begining_after_az*3600, temper)   


    # убираем ускорения
    soc['#YM#YMRP_FAST']=1
    soc['#YM#ymfast'] = 1
    soc['reset_all'] = True  # Сброс всей сигнализации по защитам и блокировкам
    # включаем блокировки защит
    soc['KEY1_SHUNT_AZ1'] = 1  # Убираем шутны на защиты
    soc['KEY_SHUNT_URB']  = 1
    soc['KEY_SHUNT_PZ1']  = 1
    soc['KEY_SHUNT_PZ2']  = 1
    soc['klarm_pos']  = 1   # / *однократное отключение АРМ * /

    
    # зафиксируем мощность, чтоб у нас было как-бы МКУ
    soc['#YM#ymflag_xe'] = 1  # расчет динамики Xe в статике
    step()
    soc['#YM#YMFLAGSTAT']=1 # включаем статику
    step()
    soc['YZBORREG']=1   # включаем внешнее управление бором
    soc.step()
    soc['YZBORMODE']=1  # бор в дистанцию
    step(4)
    
    # концентрация борной кислоты для пуска
    cbor_pusk = float( soc['MKU01_CB_START'] ) # 8.14
    # введем 16 г/кг бора
    while float(soc['#YM#ymbor_cor']) < cbor_pusk:
        soc['#YM#ymbor_cor']+=0.5
        step()
    soc['#YM#ymbor_cor']=cbor_pusk
    # теперь, не боясь срабатывания АЗ, поднимаем ОР СУЗ
    # устанавливаем в положение как на МКУ
    suz=[1.03,1.03,1.03,1.03,1.03,1.03,   1.03,1.03,1.03,1.03,1.03,0.6]
    soc['#YS#YSHGRP']=suz
    step(36)
    
    # отлкючаем статитку
    soc['#YM#YMFLAGSTAT']=0 # включаем статику
    step()
    # отключаем внешний бор
    soc['YZBORREG']=0   # включаем внешнее управление бором
    step()
    # состояние для МКУ по зоне установлено
    
    
    
    soc['10KBC16BB001_LevSet']=8       # уровень в баке
    soc['10KBC16BB001_LevTunOn']=1     # начать наполнение
    soc['10KBC17BB001_LevSet']=8       # уровень в баке
    soc['10KBC17BB001_LevTunOn']=1     # начать наполнение
    # наполняем компенсатор
    soc['10JEF10BB001_LevSet']=5.1     # уровень в компенсаторе
    soc['10JEF10BB001_LevTunOn']=1     # начать наполнение
    # шагаем, пока заполняется КД
    step(16)
    soc['10KBA10BB001_LevSet']=2.2     # уровень в деаэраторе
    soc['10KBA10BB001_LevTunOn']=1     # начать наполнение
    
    
    print_model(soc, "Загрузка концентраций Xe и J")
    
        
    
    # загружаем концентрации Xe и Jod которые будут после несокльких часов срабатывания после АЗ
    xen, jod, c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14 = load_xe_jod_rp_to_model(fname_time_in_hour_after_az=r"xen_jod_{}.lod".format( int(begining_after_az) ), folderpath=folderpath)
    soc['#YM#YMRO_XEN'] = xen
    soc["#YM#YMRO_JOD"] = jod
    soc['#YM#YMRP_C01'] = c1
    soc['#YM#YMRP_C02'] = c2
    soc['#YM#YMRP_C03'] = c3 
    soc['#YM#YMRP_C04'] = c4 
    soc['#YM#YMRP_C05'] = c5 
    soc['#YM#YMRP_C06'] = c6 
    soc['#YM#YMRP_C07'] = c7 
    soc['#YM#YMRP_C08'] = c8 
    soc['#YM#YMRP_C09'] = c9 
    soc['#YM#YMRP_C10'] = c10
    soc['#YM#YMRP_C11'] = c11
    soc['#YM#YMRP_C12'] = c12
    soc['#YM#YMRP_C13'] = c13
    soc['#YM#YMRP_C14'] = c14

    step(4)
    soc["#YM#ymtime_re8"] = float(soc["MKU_t_after_shut_down"])*3600 # сбрасываем время
    soc["#YM#ymtime"] = float(soc["MKU_t_after_shut_down"])*3600 # сбрасываем время
    soc.step(16)


    
    # Запуск внешней программы МКУ01
    soc['MKU01_FLAG']=1
    print_model(soc, "Запуск программы MKU01")
    soc.step(8)
    
    
    
    
    soc['YZREGRPINS_KEY'] = 0 # дезактивацмя ввода положения группы в процентах
    step(4)
    soc['10KBC16BB001_LevTunOn']=0 # заканчиваем наполнять баки дистиллятом
    soc['10KBC17BB001_LevTunOn']=0 # заканчиваем наполнять баки дистиллятом
    soc['10JEF10BB001_LevTunOn']=0 # заканчиваем наполнять компенсатор
    soc['10KBA10BB001_LevTunOn']=0 # заканчиваем наполнять деаэратор
    
    print_model(soc, "Включение групп ТЭН")
    
    # Включение ТЭН
    soc['TEN_1GR_GM01']=True  #  Ложная команда включить  1          
    soc['TEN_1GR_GM04']=True  #  Непрохожднеие команды выключить  1  
    soc['TEN_2GR_GM01']=True  #  Ложная команда включить 2           
    soc['TEN_2GR_GM04']=True  #  Непрохожднеие команды выключить 2   
    soc['TEN_3GR_GM01']=True  #  Ложная команда включить 3           
    soc['TEN_3GR_GM04']=True  #  Непрохожднеие команды выключить 3   
    soc['TEN_4GR_GM01']=True  #  Ложная команда включить 4           
    soc['TEN_4GR_GM04']=True  #  Непрохожднеие команды выключить 4 
    step(16)
    
    # впрыск в КД откроется автоматически при росте давления
    # открываем запорную задвижку на блокировку толстого впрыска в КД
    #soc['10JEF11AA001_x_ALOE']=True #  ( Лож ОТКРЫТЬ )
    #soc['10JEF11AA001_f_ALS'] =True #  ( Непр ком.ЗАКРЫТЬ )
    # начинаем открывать впрыск в КД
    #soc['10JEF11AA201_x_ALOE'] = True  # Откртыие регулятора ( Лож ОТКРЫТЬ )
    #soc['10JEF11AA201_f_ALS'] = True  # Непр ком.ЗАКРЫТЬ    ( Непр ком.ЗАКРЫТЬ )
    #soc.step()
    # остановка открытие впрыска в КД
    #soc['10JEF11AA201_x_ALOE'] = False  # Снятие отказа на открытие
    #soc['10JEF11AA201_f_ALS'] = False  # Снятие отказа на непр. ком. Закрыть
    #step()
    
    
    print_model(soc, "Подпитка ЧК с большим расходом")
    soc['K1_H_AccelBor']=15
    soc['#YM#YMFAST']=15
    step()

    # открытие
    soc['10KBA25AA001_x_ALOE']=True #  Открытие 1 блокирующей задвижки ( Лож ОТКРЫТЬ )
    soc['10KBA25AA001_f_ALS ']=True #  Открытие 1 блокирующей задвижки ( Непр ком.ЗАКРЫТЬ )
    soc['10KBA25AA002_x_ALOE']=True #  Открытие 1 блокирующей задвижки ( Лож ОТКРЫТЬ )
    soc['10KBA25AA002_f_ALS ']=True #  Открытие 1 блокирующей задвижки ( Непр ком.ЗАКРЫТЬ )
    soc.step()
    # останов открытия
    soc['10KBA25AA001_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
    soc['10KBA25AA001_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )
    soc['10KBA25AA002_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
    soc['10KBA25AA002_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )
    soc.step()

    
    
    # при подходе к критической концентрации, остановим подпидку за 1.1 г/кг
    def zapas_do_kritiki():
        """Расчет запаса до критики"""
        #curr_time = float(soc["#YM#ymtime_re8"])
        #curr_temp = float(soc["SVRK_THNAVR"])
        #cb_crit =cb_interp_obj.cb_interp(curr_time, curr_temp)  
        #soc['MKU01_CB_CRITICAL'] = cb_crit
        zapas = float(soc['#YM#ymbor_cor']) - float(soc['MKU01_CB_CRITICAL'])
        return zapas
        
        
    # как только мы подошли к пусковому диапазону, начнем перемешивание
    while 1.2 < zapas_do_kritiki():
        step(8)
        #regul_vprisk_v_KD()
        #set_deaerator_level()
    
    print_model(soc, "Перемешивание")
    begin_peremew = float(soc["#YM#YMTIME_RE8"])
    soc['K1_H_AccelBor']=25
    soc['#YM#YMFAST']=25


    # закртыие
    soc['10KBA25AA001_x_ALOE']=False #  Открытие 1 блокирующей задвижки ( Лож ОТКРЫТЬ )
    soc['10KBA25AA001_f_ALS ']=False #  Открытие 1 блокирующей задвижки ( Непр ком.ЗАКРЫТЬ )
    soc['10KBA25AA002_x_ALOE']=False #  Открытие 1 блокирующей задвижки ( Лож ОТКРЫТЬ )
    soc['10KBA25AA002_f_ALS ']=False #  Открытие 1 блокирующей задвижки ( Непр ком.ЗАКРЫТЬ )
    soc.step(50)
    # останов закытия    
    soc['10KBA25AA001_x_ALS'] = False  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
    soc['10KBA25AA001_f_ALOE'] =False  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )
    soc['10KBA25AA002_x_ALS'] = False  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
    soc['10KBA25AA002_f_ALOE'] =False  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )


    step(300)
    
    
    
    # теперь ждем пока концентрации в зоне, Кд и узле подпитки выравняются
    def disbalance():
        #DISBALANCE1 = math.fabs(float(soc["ymbor_cor0"]) - float(soc["10KBA40CQ001"])) # зона - узел подпитки
        #soc["MKU01_CBOR_DISBALANCE"]=DISBALANCE1
        #disbalance_for_return = DISBALANCE1
        DISBALANCE2 = math.fabs(float(soc["#YM#ymbor_cor"]) - float(soc["10JEF10BB001_CCbor"])) # зона - КД
        disbalance_for_return = DISBALANCE2
        #if (DISBALANCE1 < DISBALANCE2) :
        #    soc["MKU01_CBOR_DISBALANCE"]=DISBALANCE2
        #    disbalance_for_return = DISBALANCE2
        #else:
        #    pass
        return disbalance_for_return
    
    
    while 0.5 < disbalance():
        step(8)
    end_peremew = float(soc["#YM#YMTIME_RE8"])

    delta_peremew_hour =  (end_peremew - begin_peremew)/3600.
    soc["MKU01_t_peremew"] = delta_peremew_hour
    print_model(soc, "Время перемешивания установленно из модели")
    step(30)
    
    print_model(soc, "Подпитка ЧК в пусковом интервале")
    soc['K1_H_AccelBor']=10
    soc['YMFAST']=10

    
    # Начало открытия регулятора для обеспечения расхода 10
    soc["10KBA28AA003_x_ALOE"] =True # Открытие 1 блокирующей задвижки(Лож ОТКРЫТЬ )
    soc["10KBA28AA003_f_ALS"] =True  # Открытие 1 блокирующей задвижки(Непр ком.ЗАКРЫТЬ )
    soc['10KBA28AA202_x_ALOE']= True # Открытие регулятора ( Лож ОТКРЫТЬ )
    soc['10KBA28AA202_f_ALS'] = True # Открытие регулятора ( Непр ком.ЗАКРЫТЬ )
    soc["10KBA28AA002_x_ALOE"] =True # Открытие 1 блокирующей задвижки(Лож ОТКРЫТЬ )
    soc["10KBA28AA002_f_ALS"] =True  # Открытие 1 блокирующей задвижки(Непр ком.ЗАКРЫТЬ )
    step(30)
    # Пошел чистый конденсат, активируем ускоренное размешивание
    soc['K1_H_AccelBor']=10
    soc['#YM#YMFAST']=10
    
    
    # Завершение выхода в критику, останов подпитки ЧК
    while  float( soc['#YM#YMDRNEW_PROC'])<-0.03 :
        step(8)
    
    # закрытие задвижки 10KBA28AA002
    soc['10KBA28AA003_x_ALOE']= False # Остановка открытия регулятора ( Лож ОТКРЫТЬ )
    soc['10KBA28AA003_f_ALS'] = False # Остановка открытия регулятора ( Непр ком.ЗАКРЫТЬ )
    soc['10KBA28AA003_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
    soc['10KBA28AA003_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )
    
    # закрытие задвижки 10KBA28AA202
    soc['10KBA28AA202_x_ALOE']= False # Остановка открытия регулятора ( Лож ОТКРЫТЬ )
    soc['10KBA28AA202_f_ALS'] = False # Остановка открытия регулятора ( Непр ком.ЗАКРЫТЬ )
    soc['10KBA28AA202_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
    soc['10KBA28AA202_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )
    
    # закрытие задвижки 10KBA28AA002
    soc['10KBA28AA002_x_ALOE']= False # Остановка открытия регулятора ( Лож ОТКРЫТЬ )
    soc['10KBA28AA002_f_ALS'] = False # Остановка открытия регулятора ( Непр ком.ЗАКРЫТЬ )
    soc['10KBA28AA002_x_ALS'] = True  # Закрытие регулятора ( Лож ЗАКРЫТЬ )
    soc['10KBA28AA002_f_ALOE'] =True  # Непр ком.ОТКРЫТЬ    ( Непр ком.ОТКРЫТЬ )
    
    soc['K1_H_AccelBor']=1
    soc['#YM#YMFAST']=1
    
    
    # завершение сценария
    soc["MKU01_START_SCRIPT_POSLE_AZ"] = 0
    print_model(soc, "Сценарий завершен!")    

    
    
    
    
    
if __name__ == "__main__":    
   try:
       soc=Tscript_drv("tcp://localhost:5555")
       #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
       #folderpath=Path.cwd()
       block= int(float(soc["#YM#YMBLOCKNUMBER"]))
       kamp = int(float(soc["#YM#YMLOADNUMBER"]))
       folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)

       grafs_obj = grafs_dynamic(soc=soc, folderpath=folderpath)
       vixod_na_mku_posle_az(soc,folderpath=folderpath)

   except:    
       print_model(soc, "Ошибка выполнения сценария!!!")


    
    
    
    