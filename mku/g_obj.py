import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from cb_interp import cb_interp
from ocenka_ro_cb import get_dro_dc
from mc2py.zmq_script_client import *


class g_class():
    """Расчет допустимого расхода"""
    def __init__(self, M1K_tonn):
        soc=Tscript_drv("tcp://localhost:5555")
        #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
        #folderpath=Path.cwd()
        block= int(float(soc["#YM#YMBLOCKNUMBER"]))
        kamp = int(float(soc["#YM#YMLOADNUMBER"]))
        folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)

        # объект для интерполирования значения крит. концентрации борной кислоты по времени и температуре
        self.cb_interp_obj = cb_interp(folderpath)

        outpath = folderpath /  "ro_xe_from_time.dat"
        time_roxe=np.genfromtxt( str(outpath) )
        time_roxe = time_roxe.transpose()
        t_roxe_min=time_roxe[0]/60
        t_roxe_hour=time_roxe[0]/3600
        ro_xe=time_roxe[1]
        
        self.ro_xe_hour = UnivariateSpline(t_roxe_hour, ro_xe)
        self.ro_xe_hour.set_smoothing_factor(0.01)


        self.cb_interp_obj = cb_interp(folderpath)
        temper= float(soc["SVRK_THNAVR"])
        self.time_xe_sec, self.cbor_xe = self.cb_interp_obj.get_from_temp(temper)
        self.time_xe_hour = self.time_xe_sec/3600
        self.time_cbor_spline = UnivariateSpline(self.time_xe_hour , self.cbor_xe)
        self.time_cbor_spline.set_smoothing_factor(0.01)



        # коэф чувствительности реактивности к концентрации борной кислты в реакторе
        self.dro_dcb=get_dro_dc(fname="dro_dcb_xe_nom.dat", folderpath=folderpath)

        self.M1K_tonn = M1K_tonn # масса теплоносителя 1 контура, т


    def G_xe(self, cb_curr, time_hour, temper):
        """Расход на компенсацию Xe отравления"""
        self.time_xe_sec, self.cbor_xe = self.cb_interp_obj.get_from_temp(temper)
        self.time_xe_hour = self.time_xe_sec/3600
        self.time_cbor_spline = UnivariateSpline(self.time_xe_hour , self.cbor_xe)
        self.time_cbor_spline.set_smoothing_factor(0.01)

        dc_dt = self.time_cbor_spline(time_hour,1)


        M = self.M1K_tonn

        if dc_dt<0:
            g_xe = -M*dc_dt/cb_curr
        else:
            g_xe = 0
        return g_xe


    def G(self, cbcurr,cbprev, dt):
        """Расход для перехода из одного значения cb в другое за время t"""
        M = self.M1K_tonn
        g= -M/dt * np.log(cbcurr/cbprev)
        return g
 

#    def g_balans(self, time_hour):
#        """Оценка расхода на компенсацию Xe отравления по балансной методике"""
#        droxe_dt = self.ro_xe_hour(time_hour, 1) # берем 1 производную от функции
#        delta_c = 1
#        if droxe_dt < 0:
#         dt=self.dro_dcb * delta_c/droxe_dt
#        else:
#         dt=0
#
#        M = self.M1K_tonn
#        cb_curr = self.time_cbor_spline(time_hour)
#        cb_next = self.time_cbor_spline(time_hour + dt)
#        G=M/delta_t *np.log(cb_curr/cb_next)
#
#        return G
#
#    def g_balans_2(self, time_hour):
#        """Оценка расхода на компенсацию Xe отравления по балансной методике"""
#        droxe_dt = self.ro_xe_hour(time_hour, 1) # берем 1 производную от функции
#        droxe2_dt = self.ro_xe_hour(time_hour, 2)/2 # берем 2 производную от функции
#        delta_c = 1
#        M = self.M1K_tonn
#
#        if 1e-5< droxe2_dt:
#         val = np.sqrt( droxe_dt*droxe_dt - 4*droxe2_dt*self.dro_dcb*delta_c ) + droxe_dt
#         dt = val/(2*droxe2_dt)
#
#         cb_curr = self.time_cbor_spline(time_hour)
#         G=M/dt *np.log(1 + 1/cb_curr)
#         #print(time_hour,  val,  cb_curr,  G, dt)
#  
#         #if time_hour<dt:
#         # G=0
#
#        else:
#         G=0
#
#        return G



if __name__ == "__main__":    
    import matplotlib.pyplot as plt
    g_obj = g_class(230)
    time_hour = np.arange(0,50,0.1)
    res0=[]
    res1=[]
    res2=[]
    for t in time_hour:
        r1 = g_obj.ro_xe_hour(t)
        r2 = g_obj.ro_xe_hour(t,1)
        r3 = g_obj.ro_xe_hour(t,2)
        res0.append(r1)
        res1.append(r2)
        res2.append(r3)



    fig, ax = plt.subplots()
    fig.set_size_inches(9, 6)
    line1, = ax.plot(time_hour, res0, "-", color="r", label="r0")
    line2, = ax.plot(time_hour, res1, "--", color="g", label="r1")
    line3, = ax.plot(time_hour, res2, "-.", color="b", label="r2")
    ax.legend(loc='upper right')
    ax.grid(True)
    ax.set_xlabel('Время после сброса АЗ, час')
    plt.show()


