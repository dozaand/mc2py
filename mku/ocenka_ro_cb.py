from mc2py.zmq_script_client import *
import matplotlib.pyplot as plt
from pathlib import Path
import numpy as np
from scipy.interpolate import CubicSpline





def get_ro_cb_derivative(fname="dro_dcb_xe_nom.dat", folderpath=Path.cwd()): 
    """Возвращает производную реактивности по ксенону от времени"""
    outpath = folderpath /  fname
    data=np.genfromtxt( str(outpath) )
    data = data.transpose()
    bor=data[0]
    ro=data[1]

    f = CubicSpline(bor,ro)
    return f, bor, ro


def get_dro_dc(fname="dro_dcb_xe_nom.dat", folderpath=Path.cwd()):
    """Получение коэф реактивности"""
    outpath = folderpath /  fname
    data=np.genfromtxt( str(outpath) )
    data = data.transpose()
    bor=data[0]
    ro=data[1]

    dro_dc = (ro[-1] - ro[0])/(bor[-1] - bor[0])

    return dro_dc


def plot_ro_cb(folderpath=""):
    f_nom, bor_nom, ro_nom = get_ro_cb_derivative(fname="dro_dcb_xe_nom.dat", folderpath=folderpath)
    f_zero, bor_zero, ro_zero = get_ro_cb_derivative(fname="dro_dcb_xe_zero.dat", folderpath=folderpath)

    cs_nom = [f_nom(b) for b in bor_nom]
    cs_zero = [f_zero(b) for b in bor_zero]

    # построение графиков
    fig, ax = plt.subplots()
    fig.set_size_inches(9, 6)

    line1, = ax.plot(bor_nom, ro_nom, "--", label="Xe Номинал")
    line2, = ax.plot(bor_zero, ro_zero, "-",label="Xe МКУ")

    ax.legend()
    ax.grid(True)
    #ax.set_title(r'Изменение реактивности при изменении концентрации борной кислоты')
    ax.set_xlabel('Концентрация борной кислоты, г/кг')
    ax.set_ylabel(r'Реактивность, $\beta_{эфф}$')

    jpgpath = folderpath /  "drodc.png"
    plt.savefig( str(jpgpath) )
    plt.show()

    drodc_nom = (ro_nom[-1] - ro_nom[0])/(bor_nom[-1] - bor_nom[0])
    drodc_zero = (ro_zero[-1] - ro_zero[0]) / (bor_zero[-1] - bor_zero[0])

    return drodc_nom, drodc_zero


def plot_dro_dcb( folderpath="" ):
    f_nom, bor_nom, ro_nom = get_ro_cb_derivative(fname="dro_dcb_xe_nom.dat", folderpath=folderpath)
    f_zero, bor_zero, ro_zero = get_ro_cb_derivative(fname="dro_dcb_xe_zero.dat", folderpath=folderpath)

    cs_nom = [f_nom(b,1) for b in bor_nom]
    cs_zero = [f_zero(b, 1) for b in bor_zero]

    # построение графиков
    fig, ax = plt.subplots()
    fig.set_size_inches(9, 6)

    line1, = ax.plot(bor_nom, cs_nom, "--", label="Xe ном ")
    line2, = ax.plot(bor_zero, cs_zero, "-",label="Xe мку")

    ax.legend()
    ax.grid(True)
    #ax.set_title(r'Изменение реактивности при изменении концентрации борной кислоты')
    ax.set_xlabel('Концентрация борной кислоты, г/кг')
    ax.set_ylabel(r'd Реактивность / dc, $\beta_{эфф}$/ г/кг')

    jpgpath = folderpath /  "drodc2.png"
    plt.savefig( str(jpgpath) )
    plt.show()




if __name__ == "__main__":
    soc=Tscript_drv("tcp://localhost:5555")
    #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
    #folderpath=Path.cwd()
    block= int(float(soc["#YM#YMBLOCKNUMBER"]))
    kamp = int(float(soc["#YM#YMLOADNUMBER"]))
    folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)


    drodc_nom, drodc_zero = plot_ro_cb(folderpath=folderpath)
    print (drodc_nom, drodc_zero)
    #plot_dro_dcb(folderpath=folderpath)
    