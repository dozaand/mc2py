import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from scipy.interpolate import CubicSpline


def get_ro_xe_derivative(fname="ro_xe_from_time.dat", folderpath=Path.cwd()):
    """Возвращает производную реактивности по ксенону от времени"""
    outpath = folderpath /  fname
    time_roxe=np.genfromtxt( str(outpath) )
    time_roxe = time_roxe.transpose()
    time=time_roxe[0]/3600
    ro_xe=time_roxe[1]

    f = CubicSpline(time,ro_xe)
    return f


def ocenka_ro_xe(fname="ro_xe_from_time.dat", folderpath=Path.cwd()):
    outpath = folderpath /  fname
    time_roxe=np.genfromtxt( str(outpath) )
    time_roxe = time_roxe.transpose()
    time_min=time_roxe[0]/60
    time_hour=time_roxe[0]/3600
    ro_xe=time_roxe[1]


    f_ro_min = CubicSpline(time_min,ro_xe)
    ro_spl = [f_ro_min(t) for t in time_min] # значения реактивности от времени
    V_ro_min = [f_ro_min(t,1) for t in time_min] # производная реактивности от времени или скорость [betta/мин]


    plot_time_ro_xe(time_hour, ro_xe, ro_spl)
    plot_der(time_hour, V_ro_min)


def plot_time_ro_xe(time_hour,  ro_xe, ro_spl) :
    # построение графиков
    fig, ax = plt.subplots()

    line1, = ax.plot(time_hour, ro_xe)
    #line2, = ax.plot(time_hour, ro_spl)

    ax.grid(True)

    ax.set_xlabel('Время после сброса АЗ, ч')
    ax.set_ylabel(r'Реактивность, $\beta_{эфф}$')


    jpgpath = folderpath /  "roxefromtime.png"
    plt.savefig( str(jpgpath) )
    plt.show()


def plot_der(time_hour, V_ro_min) :
    # построение графиков
    fig, ax = plt.subplots()
    fig.set_size_inches(9, 6)


    line2, = ax.plot(time_hour, V_ro_min)

    #ax.legend()
    ax.grid(True)
    #ax.set_ylim(-0.02,0.005)
    #ax.set_title(r'')
    ax.set_xlabel('Время после сброса АЗ, ч')
    ax.set_ylabel(r'$\frac{\partial \rho_{Xe}(t)}{\partial t}, \beta_{эфф}$/мин', fontsize=15)

    jpgpath = folderpath /  "droxe_from_time.png"
    plt.savefig( str(jpgpath) )
    plt.show()


if __name__ == "__main__":
    from mc2py.zmq_script_client import *
    # создаем директорию на рабочем столе под результаты
    #folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ"
    #folderpath=Path.cwd()
    block= int(float(soc["#YM#YMBLOCKNUMBER"]))
    kamp = int(float(soc["#YM#YMLOADNUMBER"]))
    folderpath = Path.home() / "Desktop" / "МКУ_после_АЗ_B{:02d}_K{:02d}".format(block, kamp)


    ocenka_ro_xe(fname="ro_xe_from_time.dat", folderpath=folderpath)
