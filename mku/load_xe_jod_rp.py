import re
from operator import truth
import numpy as np
from pathlib import Path


def load_xe_jod_rp_to_model(fname_time_in_hour_after_az, folderpath):
    """Установка концентраций Xe и jod"""     
    outpath = folderpath /  fname_time_in_hour_after_az
    with open( str(outpath), 'r') as f:
        xejod_lines = f.readlines()

    template_xen = "^ymro_xen\((\d+)\)=(.+)"
    xen=[]
    for line in xejod_lines:
        m = re.search(template_xen, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            xen.append(value)

    template_jod = "^ymro_jod\((\d+)\)=(.+)"
    jod=[]
    for line in xejod_lines:
        m = re.search(template_jod, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            jod.append(value)


    template_c1 = "^ymrp_c01\((\d+)\)=(.+)"
    c1=[]
    for line in xejod_lines:
        m = re.search(template_jod, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c1.append(value)

    template_c1 = "^ymrp_c01\((\d+)\)=(.+)"
    c1=[]
    for line in xejod_lines:
        m = re.search(template_c1, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c1.append(value)

    template_c2 = "^ymrp_c02\((\d+)\)=(.+)"
    c2=[]
    for line in xejod_lines:
        m = re.search(template_c2, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c2.append(value)

    template_c3 = "^ymrp_c03\((\d+)\)=(.+)"
    c3=[]
    for line in xejod_lines:
        m = re.search(template_c3, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c3.append(value)

    template_c4 = "^ymrp_c04\((\d+)\)=(.+)"
    c4=[]
    for line in xejod_lines:
        m = re.search(template_c4, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c4.append(value)

    template_c5 = "^ymrp_c05\((\d+)\)=(.+)"
    c5=[]
    for line in xejod_lines:
        m = re.search(template_c5, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c5.append(value)

    template_c6 = "^ymrp_c06\((\d+)\)=(.+)"
    c6=[]
    for line in xejod_lines:
        m = re.search(template_c6, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c6.append(value)

    template_c7 = "^ymrp_c07\((\d+)\)=(.+)"
    c7=[]
    for line in xejod_lines:
        m = re.search(template_c7, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c7.append(value)

    template_c8 = "^ymrp_c08\((\d+)\)=(.+)"
    c8=[]
    for line in xejod_lines:
        m = re.search(template_c8, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c8.append(value)

    template_c9 = "^ymrp_c09\((\d+)\)=(.+)"
    c9=[]
    for line in xejod_lines:
        m = re.search(template_c9, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c9.append(value)

    template_c10 = "^ymrp_c10\((\d+)\)=(.+)"
    c10=[]
    for line in xejod_lines:
        m = re.search(template_c10, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c10.append(value)

    template_c11 = "^ymrp_c11\((\d+)\)=(.+)"
    c11=[]
    for line in xejod_lines:
        m = re.search(template_c11, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c11.append(value)

    template_c12 = "^ymrp_c12\((\d+)\)=(.+)"
    c12=[]
    for line in xejod_lines:
        m = re.search(template_c12, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c12.append(value)

    template_c13 = "^ymrp_c13\((\d+)\)=(.+)"
    c13=[]
    for line in xejod_lines:
        m = re.search(template_c13, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c13.append(value)

    template_c14 = "^ymrp_c14\((\d+)\)=(.+)"
    c14=[]
    for line in xejod_lines:
        m = re.search(template_c14, line)
        if truth(m):
            index = m.group(1)
            value = m.group(2)
            c14.append(value)


    return xen, jod, c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14

