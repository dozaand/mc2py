#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import sys
import os
import cPickle

# ����������� ����
import ModForm

import numpy as np


def led(obj=[]):
    _cwd = os.getcwd()
    os.chdir(os.path.dirname(__file__))
    if "app" not in globals():
        global app
        app = wx.PySimpleApp(redirect=True)
    frame = ModForm.MainFrame(None, obj)
    frame.Show()
    os.chdir(_cwd)
    del _cwd
    app.MainLoop()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        with open(sys.argv[1], "rb") as f:
            obj = cPickle.load(f)
            led(obj)
