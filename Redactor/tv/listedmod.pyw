#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import ModForm
import numpy as np
import sys,os
import mc2py.evaldict as ed
import cPickle
from ctypes import *
from VirtualList import UseVirtualList,TObjInfo
from ecran.gui.chart.grafic import  TGraphic,TGraphicsList
import datetime
import random
import mc2py.geom as geom
from ecran.gui.PyCart3d.WxCartogrammaWXMainPanel import SampleFormFrame
import profile
from mc2py.util import Ccwd

def NpPlot(obj,objectName="",event=None):
    """���������� �������, �������� ������"""
    grf=TGraphic(ydata=obj, title=objectName)
    grf.Plot()

def ObjCustomActions(obj,name,parent):
    """�������� ��� ��������� �� ������� ��� �������� ������ ����, ������� � ��������"""
    if type(obj) == np.ndarray:
        return [[u"������ �������: %s"% name,"������ ������ - �� ��� x [0,1,...]",lambda event:NpPlot(obj,name)]]
    else:
        if hasattr(obj,"GetCustomActions"):
            return obj.GetCustomActions()
        else:
            return []

class TGrafKartPool:
    grafdict={} # ���� �������, ������� � �������
    cartdict={} #
    amountTGKP = 0 # ���������� ������� �������������� �����

    def __init__(self,frame):
        self.frame=frame

        self.frame.Bind( wx.EVT_TOOL, self.CopyFrame,id = wx.ID_copyframe)
        self.frame.m_li.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnShowPopup)
        self.frame.Bind( wx.EVT_CLOSE, self.OnClose)

    def OnShowPopup(self, event):
        # ������� ����������� ����
        self.popupmenu = wx.Menu()
        # �� �� ��������������� ������ �����
        item = self.popupmenu.Append(-1, u"������������� ��� ����� :)")
        self.frame.m_li.Bind(wx.EVT_MENU, self.frame.m_li.EditItemOnRightClick, item)
        # ��������, ����� �� ���������� ������ � ���� ����� �� ������� ����������� ��� ������
        try:
            self.CheckItemGrafProperty()
#            print "begin profile"
#            profile.run(self.CheckItemGrafProperty)
#            profile.run("self.CheckItemGrafProperty()")

            # �������� - ��� �� ���� �������� ��� �������
            for key in self.grafdict.keys():
                # �������� ���������� ���� ������ ���������� ���������� ��������
                if (len(self.grafdict[key].grfList)==0):
                    # ����������� ���� �� �������� ����
                    self.grafdict.pop(key)

            # ��������� ������ ���� � ���������
            item = self.popupmenu.Append(-1, u"����� ������")
            self.frame.m_li.Bind(wx.EVT_MENU, self.OnPopupItemSelected, item)

            if len(self.grafdict.keys()) > 0:
                sm = wx.Menu()
                # ��������� item � ����
                for key in sorted(self.grafdict.keys()):
                    item = sm.Append(-1, key)
                    self.frame.m_li.Bind(wx.EVT_MENU, self.OnPopupItemSelected, item)
                self.popupmenu.AppendMenu(-1, u"�������� �� ������:", sm)
        except:
            pass

        # ��������, ����� �� ���������� ����������� � ���� ����� �� ������� ����������� �� ������
        try:
            self.CheckItemCartProperty()

##            # �������� - ��� �� ���� ����������� ��� �������
##            for key in self.kartdict.keys():
##                # �������� ���������� ���� ������ ���������� ���������� ��������
##                if (len(self.kartdict[key].grfList)==0):
##                    # ����������� ���� �� �������� ����
##                    self.kartdict.pop(key)

            # ��������� ������ ���� � �������������
            item = self.popupmenu.Append(-1, u"����� �����������")
            self.frame.m_li.Bind(wx.EVT_MENU, self.OnPopupItemCartSelected, item)

##            if len(self.kartdict.keys()) > 0:
##                sm = wx.Menu()
##                # ��������� item � ����
##                for key in sorted(self.kartdict.keys()):
##                    item = sm.Append(-1, key)
##                    self.frame.m_li.Bind(wx.EVT_MENU, self.OnPopupItemSelected, item)
##                self.popupmenu.AppendMenu(-1, u"�������� �� ������:", sm)
        except:
            pass

        # ��������� �����, ��������� � ������
        obj=self.frame.m_li.GetSelectedObj()
        objName = self.frame.m_li.GetSelectedName()
        actionList=ObjCustomActions(obj,objName,event)
        for (name,elhelp,func) in actionList:
            item = self.popupmenu.Append(-1, name)
            self.frame.m_li.Bind(wx.EVT_MENU, func, item)

        # �������� ���� ���� � �����
        self.frame.m_li.PopupMenu(self.popupmenu)

    def OnPopupItemCartSelected(self, event):
        item = self.popupmenu.FindItemById(event.GetId())
        selectedKey = item.GetText()
        if selectedKey == u"����� �����������":
            grfNumber = ( len( self.cartdict.keys() ) + 1 )
            selectedKey=u"����������� �%s" %  grfNumber
        frame = SampleFormFrame(None, title="SampleFormFrame2", size=(1000,790))
        frame.SetCartParams(self.gmextern,  self.cart , self.nz)

        frame.Show()
        wx.MessageBox("You selected item !!!!!!!!!!!!!! '%s'" % selectedKey)

    def OnPopupItemSelected(self, event):
        item = self.popupmenu.FindItemById(event.GetId())
        selectedKey = item.GetText()
        if selectedKey == u"����� ������":
            grfNumber = ( len( self.grafdict.keys() ) + 1 )
            selectedKey=u"������ �%s" %  grfNumber
        self.PlotGraf(selectedKey)
        #wx.MessageBox("You selected item '%s'" % selectedKey)


##        # make a menu
##        menu = wx.Menu()
##        # Show how to put an icon in the menu
##        idx=wx.NewId()
##        menu.Append(idx,"����� ������")
##        self.frame.m_li.Bind(wx.EVT_MENU, self.OnPlotChart, id=idx)
##        #sm = wx.Menu()
##        #sm.Append(wx.NewId(), "sub item 1")
##        #sm.Append(wx.NewId(), "sub item 1")
##        #menu.AppendMenu(wx.NewId(), "�������� �� ������", sm)
##        # Popup the menu.  If an item is selected then its handler
##        # will be called before PopupMenu returns.
##        self.frame.m_li.PopupMenu(menu)
##        menu.Destroy()

    def CheckItemGrafProperty(self):
        """�������� ������ ����� - �������� �� �� ���������� ��� ����������� �� �������"""
        # �������� ��������� ��� �������� �������
#        t0=time.clock()
        self.timeKeys=[]
        self.data=[]
        self.fullpath=""
        self.title=""
        self.razmernost=""

        # ���� ���� � ���������� ��������
        stack=self.frame.m_li.stack
        # ������� ���������� ��������
        level=self.frame.m_li.level
        # �������� �� ����� ����� �� ��� ��� ���� �� �������� �������
        # ��� ���� ������ ������ ������� ������ ���� �����
        for i in range(level-1 ,-1 ,-1):
            if hasattr(stack[i].obj,"keys"):
                itr=stack[i].obj.iterkeys()
                key=itr.next()
                if (type(key) is datetime.datetime):
                    break
        fullpath=""
#        t1=time.clock()
#        print "1",t1-t0
        try:
            # �������� �������� - �������� �� ������ ��������
            if not hasattr(stack[i].obj,"keys"):
                raise KeyError("�� ���� ���������� ������,  �� ����� �������� �������� ����������� ������")#

            # ������� ���� � ���������� �������, ����� ����� ���� ������������ ������

            # ������� �� �������� ����� ����������� ���� ����
            obj=stack[i].obj
            # ��� ������� �� �������� � �������������� ����, ����� ��������� ����������� ������
            timeKeys=sorted(obj.keys())
            # ����� �� ��������� ��������� - ��������� ��������� ��������� �������� �� ������:
            tmaxel=100000
            if(len(timeKeys)>tmaxel):
                timeKeys=[timeKeys(ind) for ind in range(-1,-1*tmaxel,-1)]
                print "��������� ����� ��������� �� ������ 100 000"

            # ��������� ������ �� � �� ������� ����� ����������� �������������� ���� � �������
            # ��������� ������� ���� ������� ���������� ������������
            levelFrom=i+1
            levelTo=level-1

            # ���� � ������� ������� ������� ������ � ������, ���� ���� ���� �������
            if levelFrom > levelTo:
                raise Exception("�� ���� ���������� ������ ��� ����� ������� - ��� ������ �� �������� �������: float,int")

            # ������� �������������� ���� � �������
            path=""
            # � ����� ������� ���������� ������ � �������, ���� ���� ���������
            # ������� ��������� ������������� ������ �������� ����
            # ������� ������� ���� � ������ ����
            for ind in range(levelFrom, levelTo):
                elInd=stack[ind].ind
                elpath=stack[ind]._GetPathElement(elInd)
                path="%s%s" % (path,elpath)


            #�������� ���������� �������� ����:
            # ��������� ������� ������, ��������� � �������� ���������
            lastIndex=self.frame.m_li.GetSelectedIndex()
            # �������� ������� ���������� ������� � ��������� ���������� � ���
            lastObj=eval("obj[timeKeys[0]]%s"%path)
            info=TObjInfo(lastObj)
            # ��������� ���������� �������� ����
            elpath=info._GetPathElement(lastIndex)
            # ���������� � ������ ����
            path="%s%s" % (path,elpath)

            # ������ ���� � �������
            fullpath="obj[tkey]%s"%path
            # �� ������ ������ ���� �� ��������� �������
            #fromRootpath=self.frame.m_li.GetFullPath()

            # ������� � �������������� ��������������� ���� ���� ������
            data=eval("[obj[tkey]%s for tkey in timeKeys]"%path)
            #typeel=eval("obj[{0}]{1}".format(repr(timeKeys[0]),path))
            # �������� - �������� �� ������ (�������������, ��� ������� � ����� ����� ���� ���), �� ������� ��������� ���� ������� (float ��� int)
            # ���� ������ ����� ������� ����� ��������� �� �������
            fel=data[0]
            try:
                floattypeel= float( fel )
            except:
                raise TypeError("������������ ������ �� �������� �������� �������� ����: float,int,..")

            # ���������� ��������� �������� �������
            self.timeKeys=timeKeys
            self.data=data
            self.fullpath=fullpath
            self.title=self.frame.m_li.GetSelectedDsc()
            self.razmernost=self.frame.m_li.GetSelectedRazmernos()
            #wx.MessageBox("title:%s , razm: %s" % (self.title, self.razmernost) )

        except:
            raise TypeError("�� ���� ���������� ������ �� �������")

    def CheckItemCartProperty(self):
        """�������� ������ ����� - �������� �� �� ���������� ��� ����������� �� �����������"""
        try:
            # ���� ���� � ���������� ��������
            stack=self.frame.m_li.stack
            # ������� ���������� ��������
            level=self.frame.m_li.level
            # ��������� ���������� ���������� �������
            obj=self.frame.m_li.GetSelectedObj()

            gm1=geom.Rbmk1884()
            is1884=False
            if hasattr(obj, "size"):
                if obj.size % gm1.ntot == 0 :
                    is1884=True
                    self.nz= obj.size / gm1.ntot
                    self.gmextern=gm1
            if len(obj)% gm1.ntot == 0 :
                is1884=True
                self.nz= len(obj) / gm1.ntot
                self.gmextern=gm1



            gm2=geom.Rbmk2488()
            is2488=False
            if hasattr(obj, "size"):
                if obj.size % gm2.ntot == 0 :
                    is2488=True
                    self.nz= obj.size / gm2.ntot
                    self.gmextern=gm2
            if len(obj)% gm2.ntot == 0 :
                is2488=True
                self.nz= len(obj) / gm2.ntot
                self.gmextern=gm2



            if (is1884 or is2488):
                # ��������� ������ �� � �� ������� ����� ����������� �������������� ���� � �������
                # ��������� ������� ���� ������� ���������� ������������
                levelFrom=0
                levelTo=level-1
                # ���� � ������� ������� ������� ������ � ������, ���� ���� ���� �������
                if levelFrom > levelTo:
                    raise Exception("�� ���� ���������� ������ ��� ����� ������� - ��� ������ �� �������� �������: float,int")
                # ������� �������������� ���� � �������
                path=""
                # � ����� ������� ���������� ������ � �������, ���� ���� ���������
                # ������� ��������� ������������� ������ �������� ����
                # ������� ������� ���� � ������ ����
                for ind in range(levelFrom, levelTo):
                    elInd=stack[ind-1].ind
                    elpath=stack[ind]._GetPathElement(elInd)
                    path="%s%s" % (path,elpath)



                # ������ ���� � �������
                fullpath = path

                self.cart=obj
                self.cartfullpath=fullpath
                self.carttitle=self.frame.m_li.GetSelectedDsc()
                self.cartrazmernost=self.frame.m_li.GetSelectedRazmernos()

            else:
                raise TypeError("������������ ������ �� �������� ������������ ����")
        except:
            raise TypeError("�� ���� ���������� ������ �� �����������")



    def PlotGraf(self,selectedKey):
        """��������� �������"""
        timeKeys=self.timeKeys
        data=self.data
        fullpath=self.fullpath
        title=self.title
        razmernost=self.razmernost

##        print "pushkey: %s"% selectedKey
##        print self.grafdict.keys()
##        print "//--------------"
        # ��������, �� ����� ���������� ������� ��������� ����� ������ ��� ��������� ������� ����� ����������
        if selectedKey in self.grafdict.keys():
            # ����� ��� ��������� ����������
            grl=self.grafdict[selectedKey]
        else:
            # ������� ����� ����������
            # � ���������� � ��� ������
            grl=TGraphicsList()

        # ��������� ������
        grobj=TGraphic(data,timeKeys,title,razmernost)
        grl.Add(grobj)
        self.grafdict[selectedKey]=grl
        # ������
        grl.Plot()


    def PlotCart(self,selectedKey):
        """��������� �����������"""
        pass

    def CopyFrame( self, event ):
        """������ ������� CopyFrame ��� ������� ��������� �������"""
        # ���������� ���������� � ������� ����������
        _cwd=os.getcwd()
        os.chdir(os.path.dirname(__file__))
        # ������� ���� ����
        frame=ModForm.MainFrame(None,self.frame.obj)
        Tgkpool = self.__class__
        gp=Tgkpool(frame)
        # ���������� ������ �������� ��� ����� ����
        gp.frame.m_li.info=self.frame.m_li.info
        gp.frame.m_li.SetItemCount(len(self.frame.m_li.info))
        gp.frame.m_li.stack=self.frame.m_li.stack
        gp.frame.m_li.level=self.frame.m_li.level
        gp.frame.m_li.change=self.frame.m_li.change
        # ���������� ����� �� �������
        gp.frame.Bind( wx.EVT_TOOL, gp.CopyFrame,id = wx.ID_copyframe)
        gp.frame.m_li.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, gp.OnShowPopup)
        gp.frame.Bind( wx.EVT_CLOSE, gp.OnClose)

        # ����� ������� ���� ����� ��� �����������
        x,y = gp.frame.GetPosition()
        Tgkpool.amountTGKP = Tgkpool.amountTGKP + 2 # ������ ��� ������� OnClose ����������� 2 ����
        gp.frame.SetPosition( (x+5*Tgkpool.amountTGKP,y+5*Tgkpool.amountTGKP) )

        # �����������
        gp.frame.Show()
        # ������� ������� ����������
        os.chdir(_cwd)

    def OnClose(self,event):
        Tgkpool = self.__class__
        Tgkpool.amountTGKP =  Tgkpool.amountTGKP - 1
        event.Skip()

def led(obj=[]):
    with Ccwd(globals()):
        if "app" not in globals():
               global app
               app = wx.PySimpleApp(redirect=True)
        frame = ModForm.MainFrame(None,obj)
        gp=TGrafKartPool(frame)
        gp.frame.Show()
    app.MainLoop()

def ParseArg(arg):
    for nm in arg:
        if nm[-3:]==".py"  or nm[-3:]==".PY":
            __import__(nm)
        else:
            try:
                with open(nm,"rb") as f:
                    return cPickle.load(f)
            except:
                return ed.Import(nm)
            finally:
                f.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        led(ParseArg(sys.argv[1:]))
    else:
        pass
