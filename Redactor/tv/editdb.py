#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import sys
import os

# ����������� ������������ ������
import ModForm

import numpy as np

from ZODB import FileStorage, DB
from persistent import Persistent
from BTrees.OOBTree import OOBTree
from BTrees.IOBTree import IOBTree
import transaction

from persistent.list import PersistentList
from persistent.mapping import PersistentMapping


def led(obj=[]):
    _cwd = os.getcwd()
    os.chdir(os.path.dirname(__file__))
    if "app" not in globals():
        global app
        app = wx.PySimpleApp(redirect=True)
    frame = ModForm.MainFrame(None, obj)
    frame.Show()
    os.chdir(_cwd)
    del _cwd
    app.MainLoop()


def ParseArg(arg):
    for nm in arg:
        if nm[-3:] == ".py" or nm[-3:] == ".PY":
            __import__(nm)
        else:
            storage = FileStorage.FileStorage(nm)
            db = DB(storage)
            connection = db.open()
            root = connection.root()
            if "sxema" in root:
                exec(root["sxema"])
            led(root)
            transaction.commit()
            connection.close()
            db.pack()
            db.close()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        ParseArg(sys.argv[1:])
