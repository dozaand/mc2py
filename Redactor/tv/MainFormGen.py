# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Sep  8 2010)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import VirtualList

ID_EAS_CPICKLE_BIN = 1000
ID_IAS_CPICKLE_BIN = 1001
ID_SHELL = 1002
wx.ID_back = 1003
wx.ID_forward = 1004
wx.ID_copyframe = 1005
wx.ID_grafs = 1006
wx.ID_karts = 1007
wx.ID_info = 1008

###########################################################################
## Class RedactorFrame
###########################################################################


class RedactorFrame (wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"Редактор", pos=wx.DefaultPosition, size=wx.Size(
            597, 406), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        self.m_menubar1 = wx.MenuBar(0)
        self.m_menu4 = wx.Menu()
        self.m_menu2 = wx.Menu()
        self.m_menuItem31 = wx.MenuItem(
            self.m_menu2, wx.ID_ANY, u"As Text", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.m_menuItem31)

        self.m_menuItem4 = wx.MenuItem(
            self.m_menu2, wx.ID_ANY, u"As cPickle", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.m_menuItem4)

        self.EasCpickleBin = wx.MenuItem(
            self.m_menu2, ID_EAS_CPICKLE_BIN, u"As cPickle bin", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.EasCpickleBin)

        self.EasJson = wx.MenuItem(
            self.m_menu2, wx.ID_ANY, u"As Json", u"структуированный текст", wx.ITEM_NORMAL)
        self.m_menu2.AppendItem(self.EasJson)

        self.m_menu4.AppendSubMenu(self.m_menu2, u"Экспорт")

        self.m_menu4.AppendSeparator()

        self.m_menu41 = wx.Menu()
        self.m_menuItem5 = wx.MenuItem(
            self.m_menu41, wx.ID_ANY, u"As Text", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu41.AppendItem(self.m_menuItem5)

        self.m_menuItem6 = wx.MenuItem(
            self.m_menu41, wx.ID_ANY, u"As cPickle", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu41.AppendItem(self.m_menuItem6)

        self.IasCpickleBin = wx.MenuItem(
            self.m_menu41, ID_IAS_CPICKLE_BIN, u"As cPickle bin", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu41.AppendItem(self.IasCpickleBin)

        self.IasJson = wx.MenuItem(
            self.m_menu41, wx.ID_ANY, u"As Json", u"структуированный текст", wx.ITEM_NORMAL)
        self.m_menu41.AppendItem(self.IasJson)

        self.m_menu4.AppendSubMenu(self.m_menu41, u"Импорт")

        self.m_menuItem7 = wx.MenuItem(
            self.m_menu4, wx.ID_ANY, u"Дубль окна", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu4.AppendItem(self.m_menuItem7)

        self.shell = wx.MenuItem(
            self.m_menu4, ID_SHELL, u"shell" + u"\t" + u"Ctrl+E",
            u"Вызов интерпретатора. Заданы root,current,selected", wx.ITEM_NORMAL)
        self.m_menu4.AppendItem(self.shell)

        self.m_menubar1.Append(self.m_menu4, u"Файл")

        self.SetMenuBar(self.m_menubar1)

        self.m_statusBar1 = self.CreateStatusBar(1, wx.ST_SIZEGRIP, wx.ID_ANY)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        self.m_panel1 = wx.Panel(
            self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)

        self.m_li = VirtualList.UseVirtualList(self, self.obj)
        bSizer2.Add(self.m_li, 1, wx.ALL | wx.EXPAND, 5)

        self.m_panel1.SetSizer(bSizer2)
        self.m_panel1.Layout()
        bSizer2.Fit(self.m_panel1)
        bSizer1.Add(self.m_panel1, 1, wx.EXPAND, 5)

        self.SetSizer(bSizer1)
        self.Layout()
        self.m_toolBar2 = self.CreateToolBar(wx.TB_HORIZONTAL, wx.ID_ANY)
        self.m_toolBar2.AddLabelTool(wx.ID_back, u"back", wx.Bitmap(
            u"bitmaps/Back.png", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"Назад", wx.EmptyString)
        self.m_toolBar2.AddLabelTool(wx.ID_forward, u"forward", wx.Bitmap(
            u"bitmaps/Forward.png", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"Вперед", wx.EmptyString)
        self.m_toolBar2.AddSeparator()
        self.m_toolBar2.AddLabelTool(wx.ID_copyframe, u"copyframe", wx.Bitmap(
            u"bitmaps/Copy.png", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"Дубль окна", wx.EmptyString)
        self.m_toolBar2.AddLabelTool(wx.ID_grafs, u"grafs", wx.Bitmap(
            u"bitmaps/Graph.png", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"Графики", wx.EmptyString)
        self.m_toolBar2.AddLabelTool(wx.ID_karts, u"karts", wx.Bitmap(
            u"bitmaps/Kart.png", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"Картограммы", wx.EmptyString)
        self.m_toolBar2.AddLabelTool(wx.ID_info, u"info", wx.Bitmap(
            u"bitmaps/Info.png", wx.BITMAP_TYPE_ANY), wx.NullBitmap, wx.ITEM_NORMAL, u"Помощь", wx.EmptyString)
        self.m_toolBar2.Realize()

        self.Centre(wx.BOTH)

        # Connect Events
        self.Bind(wx.EVT_CLOSE, self.SaveAndClose)
        self.Bind(wx.EVT_MENU, self.ExportAsText,
                  id=self.m_menuItem31.GetId())
        self.Bind(wx.EVT_MENU, self.ExportAscPickle,
                  id=self.m_menuItem4.GetId())
        self.Bind(wx.EVT_MENU, self.ExportAscPickleB,
                  id=self.EasCpickleBin.GetId())
        self.Bind(wx.EVT_MENU, self.ExportAsJson, id=self.EasJson.GetId())
        self.Bind(wx.EVT_MENU, self.ImportAsText,
                  id=self.m_menuItem5.GetId())
        self.Bind(wx.EVT_MENU, self.ImportAscPickle,
                  id=self.m_menuItem6.GetId())
        self.Bind(wx.EVT_MENU, self.ImportAscPickleB,
                  id=self.IasCpickleBin.GetId())
        self.Bind(wx.EVT_MENU, self.ImportAsJson, id=self.IasJson.GetId())
        self.Bind(wx.EVT_MENU, self.CopyFrame, id=self.m_menuItem7.GetId())
        self.Bind(wx.EVT_MENU, self.OnCallShell, id=self.shell.GetId())
        self.Bind(wx.EVT_TOOL, self.Back, id=wx.ID_back)
        self.Bind(wx.EVT_TOOL, self.Forward, id=wx.ID_forward)
        self.Bind(wx.EVT_TOOL, self.CopyFrame, id=wx.ID_copyframe)
        self.Bind(wx.EVT_TOOL, self.Grafs, id=wx.ID_grafs)
        self.Bind(wx.EVT_TOOL, self.Karts, id=wx.ID_karts)
        self.Bind(wx.EVT_TOOL, self.Info, id=wx.ID_info)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def SaveAndClose(self, event):
        event.Skip()

    def ExportAsText(self, event):
        event.Skip()

    def ExportAscPickle(self, event):
        event.Skip()

    def ExportAscPickleB(self, event):
        event.Skip()

    def ExportAsJson(self, event):
        event.Skip()

    def ImportAsText(self, event):
        event.Skip()

    def ImportAscPickle(self, event):
        event.Skip()

    def ImportAscPickleB(self, event):
        event.Skip()

    def ImportAsJson(self, event):
        event.Skip()

    def CopyFrame(self, event):
        event.Skip()

    def OnCallShell(self, event):
        event.Skip()

    def Back(self, event):
        event.Skip()

    def Forward(self, event):
        event.Skip()

    def Grafs(self, event):
        event.Skip()

    def Karts(self, event):
        event.Skip()

    def Info(self, event):
        event.Skip()
