# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Sep  8 2010)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import os
import MainFormGen
import copy
from mc2py.util import Ccwd

###########################################################################
## Class MyFrame1
###########################################################################


class MainFrame (MainFormGen.RedactorFrame):
    def __init__(self, parent, obj, sort=0):
        self.obj = obj
        self.sort = sort

        super(MainFrame, self).__init__(parent)

        # ��������� ������ � ���� �����
        formIconLoc = wx.IconLocation(r'bitmaps/vegastrike.ico', 0)
        self.SetIcon(wx.IconFromLocation(formIconLoc))

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def Back(self, event):
        self.m_li._back()

    def Forward(self, event):
        self.m_li._next()

    def ExportAsText(self, event):
        self.m_li.EAT()

    def ExportAscPickle(self, event):
        self.m_li.EAP()

    def ExportAscPickleB(self, event):
        self.m_li.EAPB()

    def ExportAsJson(self, event):
        self.m_li.EAJSON()

    def ImportAsText(self, event):
        self.m_li.IAT()

    def ImportAscPickle(self, event):
        self.m_li.IAP()

    def ImportAscPickleB(self, event):
        self.m_li.IAPB()

    def ImportAsJson(self, event):
        self.m_li.IAJSON()

    def SaveAndClose(self, event):
        self.m_li._close()
        event.Skip()

    def CopyFrame(self, event):
##        _cwd=os.getcwd()
##        os.chdir(os.path.dirname(__file__))
        with Ccwd(globals()):
        # frame=MainFrame(None,self.obj)
            frame = copy.copy(self)
            frame.m_li.info = self.m_li.info
            frame.m_li.SetItemCount(len(self.m_li.info))
            frame.m_li.stack = self.m_li.stack
            frame.m_li.level = self.m_li.level
            frame.m_li.change = self.m_li.change
            frame.Show()
##        os.chdir(_cwd)

    def OnCallShell(self, event):
        from wx import py
##        confDir = wx.StandardPaths.Get().GetUserDataDir()
##        if not os.path.exists(confDir):
##            os.mkdir(confDir)
##        fileName = os.path.join(confDir, 'config')
        config = wx.FileConfig(localFilename="edconfig.cfg")
        config.SetRecordDefaults(True)
        filename = "edcmd.dat"
        if not os.path.exists(filename):
            with open(filename, "w") as f:
                pass
#        ARoot=self.m_li.root
        frame = py.crustslices.CrustSlicesFrame(config=config, dataDir=".",
                                                filename=None,
                                                locals={"root": self.m_li.root,
                                                        "selected": self.m_li.GetSelectedObj(),
                                                        "current": self.m_li.GetThisObj()
                                                        })
        frame.Show()

    def Update(self):
        self.m_li._refresh()

    def StatusMsg(self, msg):
        text = 'Path:'
        for el in msg:
            text = "%s%s>" % (text, el)

        self.SetStatusText(text)
