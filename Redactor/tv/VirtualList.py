#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import os
import cPickle as cP
import mc2py.evaldict as ed
import json
import inspect
from matplotlib import pylab
from pylab import *
import numpy as np
# import ModForm as MF

wildcard = "All files (*.*)|*.*"


def _GetAllKeys(obj):
    """���������� ��� ������ ����� ���� �����, �.�. ���������� __dict__, _fields_ � ��� ��������"""
    key = list(obj.__dict__.keys())
    if hasattr(obj, "_fields_"):
        key += [el[0] for el in obj._fields_]
    proplist = [name for (name, sobj) in inspect.getmembers(
        obj.__class__, inspect.isdatadescriptor) if isinstance(sobj, property)]
    return sorted(key+proplist)


class TObjInfo(object):
    """����� ��� ��������� ���������� �� ������� """
    def __init__(self, obj=[]):
        self.obj = obj  # ������, � ������� ����� �������� (�� ��� ������������ �� ������� ������)
        self.subindex = -1  # ������ � ������� ������
        self.subobj = None  # �������, ��������������� ���������� �������
        self.ind = -1  # ����� ������ ��� ������� ������� � ����������� ������

##    def inform(self,obj='',name='',descr=None):
##        """ ���������� ��� �������, ��������, ���, ��������"""
##        obj=self.obj
##        if hasattr(obj,"__getitem__") and hasattr(obj,"__len__"):
##            typ=str(type(obj))
##            val='-'
##        elif hasattr(obj,"__dict__"):
##            typ=str(type(obj))
##            val='-'
##        else:
##            typ=str(type(obj))
##            val=obj
##        return name,val,typ,descr

    def _GetPathElement(self, index):
        """���������� ������� ������� ����, ��������: [1], .a, ['xxx']"""
        self.subindex = index
        if hasattr(self.obj, "keys"):  # �������
            key = sorted(self.obj.keys())[
                index]  # ���������� � python3 ���� ����� �� ����������
            self.subobj = self.obj[key]
            str = "[\'%s\']" % key
            return str
        elif hasattr(self.obj, "__getitem__"):  # ������ �� ������� � ������������ ��������
            self.subobj = self.obj[index]
            str = "[%s]" % index
            return str
        elif hasattr(self.obj, "__iter__"):  # ������������ ������� (���� set)
            self.listview = [i for i in self.obj]
            self.subobj = self.listview[index]
            str = "[%s]" % index
            return str
        elif hasattr(self.obj, "__dict__") or hasattr(self.obj, "_fields_"):  # ������
            key = _GetAllKeys(self.obj)[index]
            self.subobj = getattr(self.obj, key)
            str = ".%s" % key
            return str
        else:
            self.subobj = None
            str = ""
            return str

##    def _GetAllKeys(self):
##        """���������� ��� ������ ����� ���� �����"""
##        key=list(self.obj.__dict__.keys())
##        if hasattr(self.obj,"_fields_"):
##            key+=[el[0] for el in self.obj._fields_]
##        proplist=[name for (name, obj) in inspect.getmembers(self.obj.__class__, inspect.isdatadescriptor) if isinstance(obj, property)]
##        vv=sorted(key+proplist)
##        return vv
    def _GetKey(self, index):
        """�� ������� ������� ���������� ������.
        ���������� �� ������������� ������� ����� ����,
        ������� ��������� �������� ���������
        """
        self.subindex = index
        if hasattr(self.obj, "KeyByIndex"):  # ������� � ������� ������������� ��������� ������ �� �������
            key = self.obj.KeyByIndex(index)
            self.subobj = self.obj[key]
            return key
        elif hasattr(self.obj, "keys"):  # �������
            key = sorted(self.obj.keys())[
                index]  # ���������� � python3 ���� ����� �� ����������
            self.subobj = self.obj[key]
            return key
        elif hasattr(self.obj, "__getitem__"):  # ������ �� ������� � ������������ ��������
            self.subobj = self.obj[index]
            return index
        elif hasattr(self.obj, "__iter__"):  # ������������ ������� (���� set)
            self.listview = [i for i in self.obj]
            self.subobj = self.listview[index]
            return index
        elif hasattr(self.obj, "__dict__") or hasattr(self.obj, "_fields_"):  # ������
            key = _GetAllKeys(self.obj)[index]
            self.subobj = getattr(self.obj, key)
            return key
        else:
            self.subobj = None
            return None

    def inf(self, item, colindex):
        """�� ������ ������ � ������� ���������� ������, ������� ���� ��������� � ����������� ������.
������� �������� ��� ���� ������ - ����� �����
"""
        key = self._GetKey(item)
        if colindex == 0:  # Ind
            return unicode(key)
        elif colindex == 1:  # Value
            obj = self.subobj
            if hasattr(obj, "__len__") or hasattr(obj, "__dict__"):
                if isinstance(obj, basestring):
                    return obj
                else:
#                    return "--"
                    return unicode(obj)
            else:
                return repr(obj)
        elif colindex == 2:  # Type
            return unicode(type(self.subobj))
        elif colindex == 3:  # Description
            if hasattr(self.obj, "_dsc_"):
                return self.obj._dsc_.get(key, "---")
            elif "_dsc_" in self.obj:
                return self.obj["_dsc_"].get(key, "---")
            else:
                return "???"
        elif colindex == 4:  # units
            if hasattr(self.obj, "_units_"):
                return self.obj._units_.get(key, "---")
            elif "_units_" in self.obj:
                return self.obj["_units_"].get(key, "---")
            else:
                return "???"

    def __len__(self):
        """���������� ������ �������"""
        if hasattr(self.obj, "__getitem__") and hasattr(self.obj, "__len__"):
            size = len(self.obj)
        elif hasattr(self.obj, "__dict__"):
            size = len(_GetAllKeys(self.obj))
        else:
            size = 0
        return size

    def __getitem__(self, ind):
        if hasattr(self.obj, '__getitem__'):
            # ������������� �������
            if hasattr(self.obj, "keys"):
                # �����������
                key = sorted(self.obj.keys())[ind]
                return self.obj[key]
            else:
                # ������ � �������
                return self.obj[ind]
        elif hasattr(self.obj, "__dict__") or hasattr(self.obj, "_fields_"):
            # ������
            key = _GetAllKeys(self.obj)[ind]
            return getattr(self.obj, key)

    def __setitem__(self, ind, val):
        if hasattr(self.obj, '__getitem__'):
            # ������������� �������
            if hasattr(self.obj, "keys"):
                # �����������
                key = sorted(self.obj.keys())[ind]
                self.obj[key] = val
            else:
                # ������ � �������
                self.obj[ind] = val
        elif hasattr(self.obj, "__dict__") or hasattr(self.obj, "_fields_"):
            # ������
            key = _GetAllKeys(self.obj)[ind]
            setattr(self.obj, key, val)

#------------------------------------------------------------------------------


class UseVirtualList(wx.ListCtrl):
    def __init__(self, MF, rootObj, sort=1):
        self.sort = sort
        wx.ListCtrl.__init__(
            self, MF.m_panel1, -1,
            style=wx.LC_REPORT | wx.LC_VIRTUAL | wx.LC_HRULES | wx.LC_VRULES
        )

#        self.InsertColumn(0, "Ind")
#        self.InsertColumn(1, "Value")
#        self.InsertColumn(2, "Type")
#        self.InsertColumn(3, "Description")
#        self.InsertColumn(4, "units")
        self.InsertColumn(0, u"������")
        self.InsertColumn(1, u"��������")
        self.InsertColumn(2, u"���")
        self.InsertColumn(3, u"��������")
        self.InsertColumn(4, u"��.���.")

        self.SetColumnWidth(0, 125)
        self.SetColumnWidth(1, 125)
        self.SetColumnWidth(2, 125)
        self.SetColumnWidth(3, 125)
        self.SetColumnWidth(4, 125)

        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnItemActivated)
        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.EditItemOnRightClick)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnItemSelected)

        self.MF = MF
        # self.status=[] # ������ � ��������� ����������� � ������� ����
        self.root = rootObj  # �������� ������
        self.stack = []  # ���� TObjInfo, ���������� ��� ������� �� �������
        # self.info=TObjInfo() # ���������� �� �������
        self.level = 0  # ������� �������
        if self.root != []:
            self._push(self.root)
        self.path = ''  # ���� � ����� ��������-�������
        self.change = 0  # ����, ���������������� � ����� ��������� � �������� ������� � ��� �����������

        # ��������� ������ ��������� �������
        self.Select(0)
        self._SetStatus()

    def _refresh(self):
        """"���������� ����������� ������ � ����������� ����������"""
        # �������� ������ ������� � ����������� ������
        # ������� ����
        # ���������� ������ �� ������ ������� � ����������� ������
        # ��������� ��� ������� � �����
        size = len(self.stack[self.level-1])
        self.DeleteAllItems()
        self.SetItemCount(size)
        self.RefreshItems(0, -1)

    def _push(self, obj):
        """�������� ������ � �����"""
        # �������� ���������� �� �������
        # ��������� -�������� �� ������ �������
        # ���� ������ �� �������,
        # �������� ���� �� �������� ������
        # ����������� �������, �.�. �� ������������� ������
        # ��������� ���������� �� ������� � ����
        # ��������� �����������
        self.info = TObjInfo(obj)
        size = len(self.info)
        if size != 0:
            self.stack = self.stack[0:self.level]
            self.level += 1
            self.stack.append(self.info)
            self._refresh()

    def _pop(self):
        """��������� ������ � �����"""
        # ���������, ����� �� �������� ������ -
        #     ����� ����� �� 1, �� ���� ��� ���� ������ ����� ������ � ����
        # ���� �������� �����, ��
        # �������� �������
        # ����������� ������ �� �����
        # ��������� �����������
        # ������������ ������� � ������� ���������
        if len(self.stack) > 1 and self.level > 1:
            self.level -= 1
            self.stack.pop()
            self._refresh()
            self.Select(self.stack[self.level-1].ind)

    def _next(self):
        """������� �� ���� ������� ������"""
        # ���������, ����� �� ��������� �� ����� ������
        # ���� ��������� �����,
        # ����������� �������
        # ��������� �����������
        if len(self.stack) > self.level:
            self.level += 1
            self._refresh()

    def _back(self):
        """����� �� ���� ������� �����"""
        # ���������: ����� �� �� ���������� �����
        # ���� �����, �� �������� �������
        # ��������� ���������� ������
        # ������������ ������� � ������� ���������
        if len(self.stack) > 1 and self.level > 1:
            self.level -= 1
            self._refresh()
            self.Select(self.stack[self.level-1].ind)

    def OnGetItemText(self, item, col):
        """����� ���������� � ������ ������������ ������"""
        # �������� ���������� �� ������� � ����������� ������
        # ������� ��� ����������
        info = self.stack[self.level-1]
        return info.inf(item, col)

    def GetSelectedIndex(self):
        """���������� ������ ������, ��������� � ���������"""
        return self.GetFirstSelected()

    def GetSelectedDsc(self):
        """��������� �������� ��� ������, ��������� � ���������"""
        rowSel = self.GetSelectedIndex()
        if rowSel >= 0:
            dsc = self.OnGetItemText(
                rowSel, 3)  # self.InsertColumn(3, u"��������")
            return dsc
        else:
            return None

    def GetSelectedObj(self):
        """��������� ���������� � ��������� �������"""
        rowSel = self.GetSelectedIndex()
        if rowSel >= 0:
            info = self.stack[self.level-1]
            obj = info[rowSel]
            return obj
        else:
            return None

    def GetThisObj(self):
        """��������� ���������� � ��������� �������"""
        info = self.stack[self.level-1]
        return info.obj

    def GetSelectedName(self):
        """��������� ���������� ��������(�������) �� ���������"""
        rowSel = self.GetSelectedIndex()
        if rowSel >= 0:
            dsc = self.OnGetItemText(
                rowSel, 0)  # self.InsertColumn(0, u"������")
            return dsc
        else:
            return None

    def GetSelectedRazmernos(self):
        """��������� ����������� ��� ������, ��������� � ���������"""
        rowSel = self.GetSelectedIndex()
        if rowSel >= 0:
            razm = self.OnGetItemText(
                rowSel, 4)  # self.InsertColumn(4, u"��.���.")
            return razm
        else:
            return None

    def GetLastKey(self):
        """��������� ����� ��� ���������� � ���� ������ ���������� (��������� ���� ����)"""
        # ��������� ������ ��������� � ���� ������
        # �������� - ������� �� ������
        # ������� ���������� �����
        rowSel = self.GetSelectedIndex()
        if rowSel >= 0:
            lastKey = self.OnGetItemText(rowSel, 0)
            return lastKey
        else:
            return None

    def GoInside(self, index):
        """����������� ������ ������� - ���������� �� �������"""
        # ����������� �� ������� ����� � ����� TObjInfo
        # ����������, ����� ������� ���� �������
        # ����� �������� � TObjInfo ������, ������� ������������� ����� ��������� ������
        # �������� ���������� �� ���� �������
        # ���� ������ �� �������� �������,
        # ��������� ��� � ���� � ����������
        info = self.stack[self.level-1]
        info.ind = index
        obj = info[info.ind]
        info1 = TObjInfo(obj)
        size = len(info1)
        if size != 0:
            self._push(obj)

    def OnItemActivated(self, event):
        """���� ������ Item """
        # �������� ������ ��������� ������
        # """����������� ������ ������� -���������� �� �������"""
        self.currentItem = event.m_itemIndex
        self.GoInside(self.currentItem)

    def EditItemOnRightClick(self, event):
        self.currentItem = self.GetSelectedIndex()
        info = self.stack[self.level-1]
        obj = info[self.currentItem]
##        if type(obj) is np.ndarray:
##            plot(obj)
##            show()
##        else:
        info1 = TObjInfo(obj)
        dlg = wx.TextEntryDialog(self, 'Items value', 'Changing Form')
        if isinstance(obj, basestring):
            msg = obj
            isstr = 1
        else:
            msg = repr(obj)
            isstr = 0
        dlg.SetValue(msg)
        if dlg.ShowModal() == wx.ID_OK:
            msg = dlg.GetValue()
            self.change = 1
            try:
                if isstr:
                    updatemsg = msg
                else:
                    updatemsg = eval(msg)
                info.__setitem__(self.currentItem, updatemsg)
                self._refresh()
            except:
                pass

    def GetFullPath(self):
        """��������� ������� ���� (�� ������� �������� ������)"""
        # ������� ���� �������
        # ��������� �������� ������
        # ���������� ���� ������� � ������� 0 ������, �.�. root, �� ��������������
        # ��� ������� �� ���� ��������� ����� ������
        # ����� �� ����� ������� �������� ����
        # ��������� ���� ���� ���� ������� (root � ��� ��� ����, � ��� ���� ��������� ����� �����)
        # ��������� ����� ��� ���������� � ���� ������ ���������� (��������� ���� ����)
        # ���������� ���������� ����� � ������ �������
        # �������
        status = []  # ������ � ��������� ����������� � ������� ����
        status.append('root')
        for level in range(0, self.level-1):
            selind = self.stack[level].ind
            elpath = self.stack[level]._GetKey(selind)
            status.append(elpath)

        lastKey = self.GetLastKey()
        status.append(repr(lastKey))

        return status

    def _SetStatus(self):
        """����� ��������� � ������ ��� �� �������"""
        # ��������� ������� ���� (�� ������� �������� ������)
        # ����� ����� ����
        status = self.GetFullPath()
        self.MF.StatusMsg(status)

    def OnItemSelected(self, event):
        """����� Item"""
        self.currentItem = event.m_itemIndex
        # ����� ��������� � ������ ���
        self._SetStatus()
        event.Skip()

    def _import(self):
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(),
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            self.path = dlg.GetPaths()
        dlg.Destroy()

    def _export(self):
        dlg = wx.FileDialog(
            self, message="Save file as ...", defaultDir=os.getcwd(),
            defaultFile="", wildcard=wildcard, style=wx.SAVE)
        dlg.SetFilterIndex(2)
        if dlg.ShowModal() == wx.ID_OK:
            self.path = dlg.GetPath()
        self.change = 0
        dlg.Destroy()

    def EAT(self):
        self._export()
        if self.path != '':
            file = open(self.path, 'w')
            file.write(repr(self.root))
            file.close()

    def EAP(self):
        self._export()
        if self.path != '':
            cP.dump(self.root, open(self.path, 'wb'))

    def EAPB(self):
        self._export()
        if self.path != '':
            cP.dump(self.root, open(self.path, 'wb'), 2)

    def EAJSON(self):
        self._export()
        try:
            with open(self.path, 'w') as f:
                json.dump(self.root, f, indent=2, encoding='cp1251')
        except:
            print "error write object to %s" % self.path

    def IAT(self):
        self._import()
        if self.path != '':
            self.root = ed.Import(self.path[0])
            self._push(self.root)

    def IAP(self):
        self._import()
        if self.path != '':
            self.root = cP.load(open(self.path[0], 'rb'))
            self._push(self.root)

    def IAPB(self):
        self._import()
        if self.path != '':
            self.root = cP.load(open(self.path[0], 'rb'))
            self._push(self.root)

    def IAJSON(self):
        self._import()
        try:
            with open(self.path, 'r') as f:
                self.root = json.load(
                    self.root, f, indent=2, encoding='cp1251')
                self._push(self.root)
        except:
            print "error read object from %s" % self.path

    def _close(self):
        if self.change == 1:
            dlg = wx.SingleChoiceDialog(
                self, u'�������� ������ ����������', u'������ ���������� ������',
                ['Save as Text', 'Save as cPickle',
                    'Save as binary cPickle', 'Save as json'],
                wx.CHOICEDLG_STYLE)
            if dlg.ShowModal() == wx.ID_OK:
                if dlg.GetStringSelection() == 'Save as Text':
                    self.EAT()
                if dlg.GetStringSelection() == 'Save as cPickle':
                    self.EAP()
                if dlg.GetStringSelection() == 'Save as binary cPickle':
                    self.EAPB()
                if dlg.GetStringSelection() == 'Save as json':
                    self.EAJSON()
            dlg.Destroy()
