#!/usr/bin/env python
# -*- coding: cp1251 -*-

from ZODB import FileStorage, DB
from persistent import Persistent
from BTrees.OOBTree import OOBTree
from BTrees.IOBTree import IOBTree
import transaction
import numpy as np

nm = "test.fs"
storage = FileStorage.FileStorage(nm)
db = DB(storage)
connection = db.open()
root = connection.root()
root["st1"] = OOBTree()
root["st2"] = OOBTree()
st1 = root["st1"]
st2 = root["st2"]
for i in range(50):
    st1[i] = i**2
    st2[i] = i**3

root["name"] = "name"
root["a"] = 234
root["c"] = np.zeros(500)

transaction.commit()
connection.close()
db.pack()
db.close()
