#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import ModGen
import numpy as np
import sys


def main(obj=[], order=[]):
    if "app" not in globals():
        global app
        app = wx.PySimpleApp(redirect=True)
    frame = ModGen.MainFrame(None, obj, order)
    frame.Show()
    app.MainLoop()


class Ta(object):
    def __init__(self, a, b, c):
        self.number = a
        self.name = b
        self.age = c

order_1 = ['number', 'name', 'age']
order_2 = ['����� �����', '���', "�������"]
order = [order_1, order_2]

aa = Ta(1, 'Grisha', 78)
bb = Ta(2, 'Petrovich', 7)
obj = {'1': [aa, bb], '2': [bb, aa]}


def ParseArg(arg):
    for nm in arg:
        if nm[-3:] == ".py" or nm[-3:] == ".PY":
            __import__(nm)
        else:
            try:
                with open(nm, "rb") as f:
                    return cPickle.load(f)
            except:
                return ed.Import(nm)
            finally:
                f.close()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(ParseArg(sys.argv[1:]))
    else:
        main(obj, order)
