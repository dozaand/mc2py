# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Sep  8 2010)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import FormGen

###########################################################################
## Class Filter
###########################################################################


class MainFrame (FormGen.Filter):

    def __init__(self, parent, obj, order):
        self.obj = obj
        self.order = order
        super(MainFrame, self).__init__(parent)

        def __del__(self):
            pass
