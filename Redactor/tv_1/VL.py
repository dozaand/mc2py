#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import wx.lib.mixins.listctrl as listmix


class Search(wx.ListCtrl, listmix.TextEditMixin):
    def __init__(self, MF, obj, order):
        wx.ListCtrl.__init__(
            self, MF.m_panel1, -1,
            style=wx.LC_REPORT | wx.LC_VIRTUAL | wx.LC_HRULES | wx.LC_VRULES
        )
        listmix.TextEditMixin.__init__(self)

        self.dat = {}
        self.obj = obj
        self.dict = {}
        for key, name in zip(order[0], order[1]):
            self.dict[name] = key
        self.colnames = order[1]
        self.chcolnames = self.colnames

        self.keys = self.obj.keys()
        self.current = self.keys[0]

        self.PopulateS()

        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnItemRightClick)

    def PopulateS(self):
        self.ClearAll()
        if self.chcolnames != []:
            for ind, el in enumerate(self.chcolnames):
                self.InsertColumn(ind, el)
                self.SetColumnWidth(ind, 125)
            self.SetItemCount(len(self.obj[self.keys[0]])+1)
        else:
            self.InsertColumn(0, 'None')
            self.SetColumnWidth(0, 125)
            self.SetItemCount(1)

    def SetVirtualData(self, index, col, data):
        self.dat[(index, self.chcolnames[col])] = data
        self.RefreshItems(0, -1)

    def _GetItem(self, item, col):
        if self.chcolnames != []:
            key = self.dict[self.chcolnames[col]]
        if item > 0:
            return self.obj[self.current][item-1].__dict__[key]
        else:
            return ''

    def OnGetItemText(self, item, col):
        if self.chcolnames != []:
            if (item, self.chcolnames[col]) in self.dat.keys():
                return self.dat[(item, self.chcolnames[col])]
            else:
                return self._GetItem(item, col)

    def OnItemRightClick(self, event):

        dlg = wx.MultiChoiceDialog(
            self, "Type of column", "Form to choose", self.colnames)
        selectedlist = [self.colnames.index(i) for i in self.chcolnames]
        dlg.SetSelections(selectedlist)

        if (dlg.ShowModal() == wx.ID_OK):
            selections = dlg.GetSelections()
            strings = [self.colnames[x] for x in selections]

        try:
            self.chcolnames = strings
        except:
            pass

        dlg.Destroy()

        self.PopulateS()
