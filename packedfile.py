#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""����� ����� bz2 � ���������� seek"""

import subprocess as sp
import bz2
import cStringIO
import cPickle
import numpy as np


class TSkBz2:
    def _OpenFile(nm):
        if not os.path.exists(nm):
            with open(nm, "wb") as f:
                pass
        return open(nm, "rb+")

    def __init__(self, name, blocksize=1024*1024*4):
        self.name = name
        self.output = cStringIO.StringIO()
        self.blk = blocksize
        self.packedbegs = np.zeros(
            1024, dtype=np.int64)  # �������� - ������� ������ ������ � ����������� �����
        self.begs = np.zeros(
            1024, dtype=np.int64)  # �������� - ������� ������ ������ � ������������� �����
        self.nblk = 1  # ������ ���������� ������
        nm = name+".bz2b"
        if not os.path.exists(nm):
            with open(nm, "wb") as f:
                pass
        self.f = open(nm, "rb+")
        self._LoadBlkData()
        # ���� ������������� ������ ��������� � ������
        nm = name+".bz2last"
        if os.path.exists(nm):
            with open(nm, "rb") as f:
                self.output.write(f.read())
        self.fo = open(nm, "ab")
        self._readfp = 0
        self.iread = None
        self.rdata = None

    def _LoadBlkData(self):
        u"""�������������� ����� �����"""
        nm = name+".bz2bd"
        if not os.path.exists(nm):
            self._SaveBlkData()
            return
        with open(name+".bz2bd", "rb") as f:
            (self.nblk, self.begs, self.packedbegs) = cPickle.load(f)

    def _SaveBlkData(self):
        u"""���������� ����� �����"""
        with open(name+".bz2bd", "wb") as f:
            cPickle.dump((self.nblk, self.begs, self.packedbegs), f, 2)

    def _ReallocDicts(self):
        l = len(self.begs)
        if self.nblk+2 > l:
            tmp = np.zeros(l*2, dtype=np.int64)
            tmp[:l] = self.begs[:]
            self.begs = tmp
            tmp = np.zeros(l*2, dtype=np.int64)
            tmp[:l] = self.packedbegs[:]
            self.packedbegs = tmp

    def _CompressBlk(self, n):
        u"""����� �� ������ ������"""
        self._ReallocDicts()
        s = bz2.compress(self.output.getvalue())
        self.packedbegs[self.nblk] = self.packedbegs[self.nblk-1]+len(s)
        self.begs[self.nblk] = self.begs[self.nblk-1]+n
        self.f.seek(0, 2)  # ��������� � �����
        self.f.write(s)
        self.output.seek(0)
        self.fo.truncate()

    def write(self, data):
        u"""������ �������������� ������ � ����� �����"""
        self.output.write(data)
        self.fo.write(data)
        n = output.tell()
        if n > self.blk:
            self._CompressBlk(n)

    def read(self, length=-1):
# numpy.searchsorted(a, v, side='left')
        self._readfp
        pass

    def seek(self, length):
        u"""������� � �������� ������� ��� ������"""
        pass

    def flush(self):
        u"""������� ������� - ������ ���� ������"""
        self.f.flush()
        self.fo.flush()
        self._SaveBlkData()

    def close(self):
        pass
