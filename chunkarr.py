#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
 �������� ��������� �������
"""
import pylzma
import cPickle
import numpy as np
import datetime
from itertools import izip

from ZODB import FileStorage, DB
from persistent import Persistent
from BTrees.OOBTree import OOBTree
from BTrees.IOBTree import IOBTree
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping
# import ZODB.FileStorage, zc.zlibstorage

from ZODB.blob import Blob
from ZODB.blob import BlobStorage
from int_bsearch import int_bsearchU

import bisect
import transaction


def BSearchRange(li, mi, ma):
    u"""�������� ����� ��������� �������� � ������� � ������������ �������� li
    ���������� (i,j)
    li[i]<=mi
    li[j]<=ma
    i in 0:len(li)
    j in 0:len(li)"""
    i = bisect.bisect_left(li, mi)
    j = bisect.bisect_right(li, ma, lo=i)
    return (i, j)


class DictCache:
    u"""������������ ������������ ������"""
    def __init__(self, size=5):
        self.size = size
        self._cache = dict()  # ���� ������ - ��������
        self._keylist = []  # ����� - � ������ ����� ������

    def __getitem__(self, key):
        u"""�� ����������� ���������� ����������� ������� ����� ���������� None"""
        if key in self._cache:
            v = self._cache[key]
            self._keylist.remove(key)
            self._keylist.insert(0, key)
        else:
            v = None
        return v

    def In(self, key):
        return key in self._cache

    def __setitem__(self, key, val):
        if key in self._cache:
            self._cache[key] = val
        else:
            self._keylist.insert(0, key)
            self._cache[key] = val
            if len(self._keylist) > self.size:
                keyToDrop = self._keylist.pop()
                self._cache.pop(keyToDrop)

    def flush(self):
        u"""����������� ����"""
        self._cache.clear()
        self._keylist = []


class Tcharr(Persistent):
    u"""������� ��������� �������
������ �������� ������:
����� � ������� � ������
np.array - ������� �������
����� ������������ ������ � ���������� ����������� � ����������� ��������� � �������
maxcnt=3000, ������������ ���������� ������� ������� �������� ��������������
overlap=100 ��� ���������� ������������� ���������� ������� �������� �������� �� � ��� ����������� overlap
��������� ����� �������� ������������������ ���������� ������
"""
#    sem=threading.Semaphore(6) # �������������� �������
    def __init__(self, maxcnt=3000, overlap=0, cachesize=1):
        self.maxcnt = maxcnt
        self.overlap = overlap
        self.blocks = OOBTree()
        self.lastdata = OOBTree()  # ��������� ������ - ����������
        self.cachesize = cachesize
        self.cache = DictCache(self.cachesize)
        self.cachet = DictCache(self.cachesize)
        self._maxKey = None

    def __setstate__(self, dkt):
        self.__dict__.update(dkt)
        if not hasattr(self, "cachesize"):
            self.cachesize = 1
            self.cache.flush()
            self.cachet.flush()
        else:
            self.cache = DictCache(self.cachesize)
            self.cachet = DictCache(self.cachesize)
#        if not hasattr(self,"_maxKey"):
#            self._maxKey=None

    def __getstate__(self):
        d1 = {}
        d1.update(self.__dict__)
        d1.pop("cache")
        d1.pop("cachet")
        return d1

    def append(self, t, v):
        u"""���������� ����� ������
        t - �����
        v - ������ - �������������� ���������� �����"""
        self.lastdata[t] = v
        self._maxKey = self.lastdata.maxKey()
        if len(self.lastdata) >= self.maxcnt:
            self._flush()

    def _flush(self):
        u"""�������� ��� �������� ������ � ����� (� ����� ����� ��� ���� �� ����������� ����������)"""
        blob = Blob()
        self.blocks[self.lastdata.minKey()] = blob
        f = blob.open("w")
        cPickle.dump(list(self.lastdata.keys()), f, 2)
        data = cPickle.dumps(list(self.lastdata.values()), 2)
        f.write(pylzma.compress(data))
        f.close()
        self.lastdata.clear()

    def _GetBlocksRange(self, mi, ma):
        u"""����������� �������� �� ������� � �������� ��������������� ������"""
        keys = list(self.blocks.keys())
        if len(keys) == 0:
            return (mi, ma)
        i = int_bsearchU(keys, mi)
        j = int_bsearchU(keys, ma)
        return (keys[i], keys[j])

    def _MakeCache(self):
        if not hasattr(self, "cache"):
            self.cache = DictCache(self.cachesize)
            self.cachet = DictCache(self.cachesize)

    def KeyByIndex(self, index):
        u"""���������� �������� ����� �� ������� (������� ��� ���������)"""
        iblock = index/self.maxcnt
        posInBlock = index-iblock*self.maxcnt
        if iblock < len(self.blocks):
            tb = list(self.blocks.keys())[iblock]
            if self.cachet.In(tb):
                times = self.cachet[tb]
            else:
###                print 'keyByIndex',tb
                block = self.blocks[tb]
                f = block.open('r')
                times = cPickle.load(f)
                self.cachet[tb] = times
                f.close()
            return times[posInBlock]
        else:
            return list(self.lastdata.keys())[posInBlock]

    def iterkeys(self, beg=None, end=None):
        return self.keys(beg, end)

    def keys(self, beg=None, end=None):
        u"""���������� �������� �� ��������"""
        if not beg or beg < self.minKey():
            beg = self.minKey()
        if not end or end > self.maxKey():
            end = self.maxKey()

        blockBeg, blockEnd = self._GetBlocksRange(beg, end)
        for tb, block in self.blocks.items(blockBeg, blockEnd):
            if self.cachet.In(tb):
                times = self.cachet[tb]
            else:
###                print 'keys',tb
                f = block.open('r')
                times = cPickle.load(f)
                f.close()
                self.cachet[tb] = times
            for t in times:
                if t <= end:
                    yield t
        for t in self.lastdata.keys(beg, end):
            yield t

    def itervalues(self, beg=None, end=None):
        return self.values(beg, end)

    def values(self, beg=None, end=None):
        if not beg or beg < self.minKey():
            beg = self.minKey()
        if not end or end > self.maxKey():
            end = self.maxKey()

        blockBeg, blockEnd = self._GetBlocksRange(beg, end)
        for tb, block in self.blocks.items(blockBeg, blockEnd):
            if self.cache.In(tb) and self.cachet.In(tb):
##                print 'cache values',tb
                data = self.cache[tb]
                times = self.cachet[tb]
            else:
##                print 'values',tb
                f = block.open('r')
                times = cPickle.load(f)
                self.cachet[tb] = times
                if not self.cache.In(tb):
                    data = cPickle.loads(pylzma.decompress(f.read()))
                    self.cache[tb] = data
                else:
                    data = self.cache[tb]
                f.close()
            ibeg, iend = BSearchRange(times, beg, end)
            for v in data[ibeg:iend]:
                yield v
        for v in self.lastdata.values(beg, end):
            yield v

    def iteritems(self, beg=None, end=None):
        return self.items(beg, end)

    def items(self, beg=None, end=None):
        u"""������������ �������� � ����"""
        if not beg or beg < self.minKey():
            beg = self.minKey()
        if not end or end > self.maxKey():
            end = self.maxKey()

        blockBeg, blockEnd = self._GetBlocksRange(beg, end)
        for tb, block in self.blocks.items(blockBeg, blockEnd):
            if self.cache.In(tb) and self.cachet.In(tb):
##                print 'cache items',tb
                data = self.cache[tb]
                times = self.cachet[tb]
            else:
##                print 'items',tb
                f = block.open('r')
                times = cPickle.load(f)
                self.cachet[tb] = times
                if not self.cache.In(tb):
                    data = cPickle.loads(pylzma.decompress(f.read()))
                    self.cache[tb] = data
                else:
                    data = self.cache[tb]
                f.close()

            ibeg, iend = BSearchRange(times, beg, end)
            for t, v in izip(times[ibeg:iend], data[ibeg:iend]):
                yield t, v
        for t, v in self.lastdata.iteritems(beg, end):
            yield t, v

    def __getitem__(self, key):
        return self.values(key).next()

    def minKey(self):
        if len(self.blocks) > 0:
            return self.blocks.keys()[0]
        else:
            return self.lastdata.minKey()

    def maxKey(self):
        return self._maxKey

    def __len__(self):
        return len(self.blocks)*self.maxcnt+len(self.lastdata)

    def clear(self):
        self.lastdata.clear()
        self.blocks.clear()
        self.cache.flush()
        self.cachet.flush()
        self._maxKey = None


class TcharrPV(Tcharr):
    u"""��������� � ��������������"""
    def __init__(self, maxcnt=3000, overlap=0, cachesize=1, previewstep=300):
        super(TcharrPV, self).__init__(
            maxcnt=maxcnt, overlap=overlap, cachesize=cachesize)
        self._preview = OOBTree()
        self._previewstep = previewstep

    def append(self, t, v):
        super(TcharrPV, self).append(t, v)
        if self.__len__() % self._previewstep == 0:
            self._preview[t] = v

    def clear(self):
        super(TcharrPV, self).clear()
        self._preview.clear()


# from pylab import plot,show
# from mc2py.Redactor.tv.listedmod import led
# from oedit import ed
# import garant
##
# def Impgar():
##    arhg=garant.TSvrkArh('garant1.dsk',r'D:\kalinindata\GARANT3\ARH\C01\Arh3b.dat')
##
##    storage = FileStorage.FileStorage("tmp/data.fs")
##    blob_storage = BlobStorage("tmp/blb", storage)
##    db = DB(blob_storage)
##    connection = db.open()
##    root = connection.root()
##    if "arh" in root:
##        arh=root["arh"]
##    else:
##        arh=Tcharr(10)
##        root["arh"]=arh
##
##    if len(arh)==0:
##        for i,rec in enumerate(arhg.IterStruct()):
##            if i==10:break
##            arh.append(rec.t,rec)
##            if i+1!= len(arh):
##                print "err"
##                break
##            if i%1000==0:
##                print "index",i
##
##            if i>8080:
##                break
##        transaction.commit()
##        db.pack()
##    list(arh.keys())
##    led(arh)
##    connection.close()
##    db.close()
##
# Impgar()
##
##
# aa=np.zeros([10,4])
##
# if __name__ == '__main__':
##    Test1()
