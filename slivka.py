#!/bin/env/python
# -*- coding:utf-8 -*-

import os,glob,shutil
import argparse
import yaml
import zlib
from mc2py.util import PathFind,PathFindDir,newcwd

def correct_list(li,cfg):
    u"""correct file list by user restrictions"""
    incl=set()
    if "include" in cfg:
        for i in cfg["include"]:
            incl.update(set(shutil.fnmatch.filter(li,i)))
    if "exclude" in cfg:
        res=set()
        for i in cfg["exclude"]:
            res.update(set(shutil.fnmatch.filter(li,i)))
        li-=res
    li.update(incl)

def test_correct_list1():
    li = set(["a.for","d/b.for","c.c"])
    cfg = {"exclude":["*.for"]}
    correct_list(li,cfg)
    assert(li==set(["c.c"]))

def test_correct_list2():
    li = set(["a.for","d/b.for","c.c"])
    cfg = {"exclude":["*.for"],"include":["a.for"]}
    correct_list(li,cfg)
    assert(li==set(["c.c","a.for"]))

def test_correct_list3():
    li = set(["d/b.for","c.c"])
    cfg = {"exclude":["*.for"],"include":["a.for"]}
    correct_list(li,cfg)
    assert(li==set(["c.c"]))

def neqval(fil1,fil2):
    s1=os.path.getsize(fil1)
    s2=os.path.getsize(fil2)
    if s1!=s2:
        return True
    return False
    f=open(fil1,"r").read()
    crc1=zlib.crc32(f)
    f=open(fil2,"r").read()
    crc2=zlib.crc32(f)
    return crc1 != crc2

def test_neqval():
    assert(neqval("examples/test2/a.dat","examples/test2/b.dat"))
    assert(not neqval("examples/test2/a.dat","examples/test2/c.dat"))
    assert(neqval("examples/test2/a.dat","examples/test2/d.dat"))

def sliv(new_dir,old_dir,merge_dir,force_lwr=1):
    u"""merge_dirs d1 new_dir"""
    with newcwd(new_dir):
        if os.path.exists("ignore.yaml"):
            with open("ignore.yaml","r") as f:
                cfg=yaml.load(f)
        else:
            cfg={}
        new_files=set(PathFind("*.*"))
        if force_lwr:
            new_files=set([i.lower() for i in new_files])
        correct_list(new_files,cfg)
    with newcwd(old_dir):
        old_files=set(PathFind("*.*"))
        if force_lwr:
            old_files=set([i.lower() for i in old_files])
        correct_list(old_files,cfg)
    added_files = new_files - old_files
    modified=[]
    for i in new_files.intersection(old_files):
        nf=os.path.join(new_dir,i)
        of=os.path.join(old_dir,i)
        if neqval(nf,of):
            modified.append(i)
    removed_files = old_files - new_files

    added_files.update(modified)

# copy files
    for filename in added_files:
        source_name=os.path.join(new_dir,filename)
        target_name=os.path.join(merge_dir,filename)
        target_dir_name=os.path.dirname(target_name)
        if not os.path.exists(target_dir_name):
            os.makedirs(target_dir_name)
        shutil.copy(source_name,target_dir_name)
# mark files for removal
    if not os.path.exists(merge_dir):
        os.makedirs(merge_dir)
    with open(os.path.join(merge_dir,"rm_fil.bat"),"wt") as f:
        for filename in removed_files:
            f.write("del {0}\n".format(filename))


def test1():
    u"""add new file"""
    new_dir = "examples/test1/d1"
    old_dir = "examples/test1/d2"
    merge_dir = "examples/test1/d3"
    sliv("examples/test1/d1", "examples/test1/d2", "examples/test1/d3")
    assert(os.path.exists(r"examples\test1\d3\dd\a.cpp"))
    shutil.rmtree(r"examples\test1\d3")


def test2():
    u"""add new file"""
    rt="examples/test2/"
    sliv(rt+"d1", rt+"d2", rt+"d3")
    assert(os.path.exists(rt+r"d3\dd\a.cpp"))


def test3():
    u"""add new file"""
    rt="examples/test3/"
    sliv(rt+"d1", rt+"d2", rt+"d3")
    assert(os.path.exists(rt+r"d3\dd\a.cpp"))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="slivka",description="make merge of directory")
    parser.add_argument("dirs",nargs=3,help=u"new_dir olddir slivka_dir")
    args = parser.parse_args()
    sliv(*args.dirs)
