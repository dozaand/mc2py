#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        geom.py
# Purpose:     общие для всх станций геометрические данные не работает с командной строки
#
# Author:      and
#
# Created:     25.11.2010
# Copyright:   (c) and 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
import mc2py.geom
import argparse,os
import yaml
from mc2py.showkart import ShowKart
import codecs
import numpy as np


def kshow(args):
    u"""отрисовка данных из файла"""
    nm,ext = os.path.splitext(args.input)
    if ext != ".yaml":
        from mc2py.kart2yaml import data2yaml
        file_name = nm+ ".yaml"
        data2yaml(args.input,file_name,6)
    else:
        file_name = args.input

    with codecs.open(file_name, "r",encoding="utf-8") as f:
        data = yaml.load(f)
        #(self.geom, self.keys, self.data) = cPickle.load(f)
    g = data["geom"]
    gm = mc2py.geom.TPlane(g["bounds"],g["y0"],g["symmetry"])
    keys = data["keys"]
    if args.data:
        dat = np.loadtxt(args.data)
    else:
        dat = data["kart"]
    if not args.output:
        pltshow = 1
    else:
        pltshow = 0
#        if not args.output:
#            args.output = os.path.splitext(args.input)[0]+".png"
    if not dat:
        dat=[" "]*gm.ntot
        fmt="%s"
    else:
        fmt=args.format
    ShowKart(dat, gm, title=args.title, pltshow=pltshow,
             savepath=args.output, format=fmt, dnLim=args.dnLim, upLim=args.upLim)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="show file from yaml format")
    parser.add_argument("input", help=u"show kart")
    parser.add_argument("-d","--data", help=u"show kart")
    parser.add_argument("--title",help=u"title of chart")
    parser.add_argument("--output", help=u"title of chart")
    parser.add_argument("--format", help=u"title of chart")
    parser.add_argument("--dnLim", help=u"title of chart")
    parser.add_argument("--upLim", help=u"title of chart")
    args = parser.parse_args()
    kshow(args)


