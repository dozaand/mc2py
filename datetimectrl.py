#!/usr/bin/env python
# -*- coding: cp1251 -*-

import wx
import wx.lib.masked as masked
import datetime
from time import struct_time, localtime, gmtime, strftime, strptime
import wx.lib.calendar as cal


#-------------------------------------------------------------------------
class date_time_ctrl(wx.Panel):
    def __init__(self, *args, **kwargs):
        self.frm = kwargs.pop('frm', None)
        self.app = kwargs.pop('app', None)
        if self.app is None and hasattr(self.frm, 'app'):
            self.app = self.frm.app
        self.format = kwargs.pop('format', '%Y.%m.%d %H:%M:%S')
        self.autoformat = kwargs.pop(
            'autoformat', 'EUDATE24HRTIMEYYYYMMDD.HHMMSS')
        value = kwargs.pop('value', '')
        if isinstance(value, int):
            value = time_stamp_to_str(value, self.format)
        self.cal_type = kwargs.pop('cal_type', 'BUS')
        wx.Panel.__init__(self, *args, **kwargs)
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.edit_ctrl = masked.Ctrl(
            self, wx.ID_ANY, value, autoformat=self.autoformat)
        self.btn_select = wx.Button(
            self, wx.ID_ANY, u"������� ����", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Bind(wx.EVT_BUTTON, self.event_select_date, self.btn_select)
        sizer.Add(self.edit_ctrl, 0, wx.EXPAND)
        sizer.Add(self.btn_select, 0, wx.ALL, 5)
        self.SetSizer(sizer)

    def event_select_date(self, event):
        current = gmtime()
                         # time_stamp_to_struct_time(str_to_time_stamp(self.edit_ctrl.GetValue(),
                         # self.format))
        dlg = cal.CalenDlg(self, month=current[
                           1], day=current[2], year=current[0])
        dlg.calend.cal_type = self.cal_type
        from datetime import MINYEAR, MAXYEAR
        dlg.y_spin.SetRange(MINYEAR, MAXYEAR)
        #~ dlg.y_spin.SetRange(1000, 3000)
        dlg.y_spin.SetValue(current[0])
        dlg.SetTitle('Please select date')
        dlg.Centre()
        if dlg.ShowModal() == wx.ID_OK:
            day = str(dlg.calend.day)
            month = str(dlg.calend.month)
            if len(day) < 2:
                day = '0' + day
            if len(month) < 2:
                month = '0' + month
            # value = '%d.%s.%s %s' % (dlg.calend.year, month, day,
            # self.edit_ctrl.GetValue()[11:])
            value = '%d.%s.%s 00:00:00' % (dlg.calend.year, month, day)
            self.edit_ctrl.ChangeValue(value)

    def get_as_time_stamp(self, format=None):
        if format is None:
            format = self.format
        try:
            return str_to_time_stamp(self.edit_ctrl.GetValue(), format)
        except:
            return 0

    def GetValue(self):
        return self.edit_ctrl.GetValue()
