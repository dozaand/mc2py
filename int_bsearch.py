#!/usr/bin/env python
# -*- coding: cp1251 -*-


def int_bsearch(a, s):
    u"""�������� ����� � ������� a ������� ������ ������� ��� �������� s"""
    n = len(a)
    j = 0
    while n > 2:
        i = int(n/2)
        if s < a[i+j]:
            n = i+1
        else:
            j += i
            n -= i
    return j


def int_bsearchU(a, s):
    u"""�������� ����� � ������� a ������� ������ ������� ��� �������� s ������� ��������� �������"""
    n = len(a)+1
    j = 0
    while n > 2:
        i = n/2
        if s < a[i+j]:
            n = i+1
        else:
            j += i
            n -= i
    return j
