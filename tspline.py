#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""
������ ������������ ��� ��������� ��������� ������������ � ������� ������
"""
import numpy as np
# from mc2py.oedit import ed
from scipy.interpolate import splrep, splev, splint


class SplineTree(object):
    u"""������ �������������� � ������ �������� ��� ������������ �� �������"""
    def __init__(self):
        u"""������������� �������������"""
        pass

    def append(self, t, obj):
        u"""���������� ����� ����� ������"""
        if not hasattr(self, "_root"):
            self._root = _IInitSplines(obj)
            self._sample = obj
            self.t = [t]
            self.minT = t
            self.maxT = t
        else:
            _AddToSplines(obj, self._root)
            self.t.append(t)
            self.maxT = t

    def Update(self, **kvarg):
        u"""�������������� ��������� � ����� ��������"""
        assert(hasattr(self, "_root"))
        self.kvarg = kvarg
        if "eps" in kvarg:
            print("eps")
            self._spl = self._UpdateEps(self._sample, self._root, kvarg["eps"])
        else:
            self._spl = self._Update(self._sample, self._root)
        delattr(self, "_root")
        delattr(self, "t")
        delattr(self, "_sample")
        delattr(self, "kvarg")

    def MakeEps(self, s):
        """������ ������ ������� �� �����"""
        n = len(self.t)
        return (s*s)*n

    def _UpdateEps(self, obj, splo, eps):
        u"""������������ ��������� �������� �������������� �������������� �� �������� � ������� (���������� � ����������� ���������)"""
        if hasattr(obj, "items"):
            # return dict([(k, self._UpdateEps(i, splo[k], eps[k])) for k, i in obj.iteritems()])
            return dict([(k, self._UpdateEps(i, splo[k], eps[k])) for k, i in obj.items()])
        elif hasattr(obj, "__iter__"):
            return [self._UpdateEps(i, j, e) for i, j, e in zip(obj, splo, eps)]
        else:
            kv = dict(eps)
            kv["s"] = self.MakeEps(kv["s"])
            try:
                res = splrep(self.t, splo, **kv)
            except:
                v = ValueError("spline fail")
                v.tx = (self.t, splo)
                raise v
            return res

    def _Update(self, obj, splo):
        u"""������������ ��������� ��������"""
        # if hasattr(obj, "iteritems"):
            # return dict([(k, self._Update(i, splo[k])) for k, i in obj.iteritems()])
        if hasattr(obj, "items"):
            return dict([(k, self._Update(i, splo[k])) for k, i in obj.items()])
        elif hasattr(obj, "__iter__"):
            return [self._Update(i, j) for i, j in zip(obj, splo)]
        else:
            kv = dict(self.kvarg)
            kv["s"] = self.MakeEps(kv["s"])
            try:
                res = splrep(self.t, splo, **kv)
            except:
                v = ValueError("spline fail")
                v.tx = (self.t, splo)
                raise v
            return res

    def _MakeSrez(self, t):
        u"""������������� ���� �� �������� ��� ��������� ������� �������"""
        return _IMakeSrez(self._spl, t)

    def __call__(self, t, path=None):
        u"""���������� ����� ��� ����� ��� ������ ����� �� ������� � �������� �� ��������� ����
        ���� ���� None - ���������� ���� (��� �������� �� ������)"""
        if path:
            if isinstance(path, basestring):
                path = [TryInt(i) for i in path.split('.')]
            base = self._spl
            for i in path:
                base = base[i]
            return splev(t, base)
        else:
            if hasattr(t, "__iter__"):
                return [self._MakeSrez(i) for i in t]
            else:
                return self._MakeSrez(t)

    def __iter__(self):
        u"""��������� ������ � ��� ������ ���� ���� ���� t"""
        for i in self.t:
            yield self._MakeSrez(i)


def TryInt(v):
    u"""����������� � ����� ���� ���������"""
    try:
        ret = int(v)
    except:
        ret = v
    return ret


def _IInitSplines(obj):
    u"""�������� ��������� �� ���������� � ������� ����� ����� ����������� ������"""
    if hasattr(obj, "items"):
        return dict([(k, _IInitSplines(i)) for k, i in obj.items()])
    elif hasattr(obj, "__iter__"):
        return [_IInitSplines(i) for i in obj]
    else:
        return [obj]


def _IMakeSrez(obj, t):
    u"""������������� ���� �� �������� ��� ��������� ������� �������"""
    if hasattr(obj, "items"):
        return dict([(k, _IMakeSrez(i, t)) for k, i in obj.items()])
    elif isinstance(obj, tuple):
        return splev(t, obj)
    else:
        return [_IMakeSrez(i, t) for i in obj]


def _AddToSplines(obj, to):
    u"""���������� ����� � ���������"""
    if hasattr(obj, "items"):
        for k, i in obj.items():
            _AddToSplines(i, to[k])
    elif hasattr(obj, "__iter__"):
        for x1, x2 in zip(obj, to):
            _AddToSplines(x1, x2)
    else:
        to.append(obj)


def Test():
    t = np.arange(0, 100, 0.5)
    tr = SplineTree()
    for i in t:
        obj = {"a": 2, "b": np.array([1, 2, 3]), "c": {"a": 5, "b": 6*i}}
        tr.append(i, obj)

    pr = {"s": 0.01, "k": 3}
    eps = {"a": pr, "b": [pr]*3, "c": {"a": pr, "b": pr}}
    # tr.Update(k=3,s=0.01)
    tr.Update(eps=eps)
    res = tr([1, 2, 3], "b.2")
    #print res
    res = tr(1)
    #print res
    res = tr([1, 2])
    #print res
    tr.t = np.arange(0, 20, 6.)
    #print [i for i in tr]
