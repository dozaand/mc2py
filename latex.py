#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
������� ��� ������ � LaTeX
"""

import numpy as np
from evaldict import *


def _LatexTableRow(data):
    u"""���������� ����� ������ � ������"""
    return sepr(data, " & ")+r"\\ "


def ToLatexTable(data, firstrow=None, caption=None, label=None):
    u"""�������������� ������������������ ������ � ������ - ������� � ����� LaTeX
>>> print ToLatexTable([[1,1,1],[2,2,2]],firstrow=[1,2,3],caption=u"asd",label="l1")

\begin{table}
  \centering
  \caption{asd}\label{l1}
  \begin{tabular}{|c|c|c|}
\hline
  1 & 2 & 3\\
\hline
1 & 1 & 1\\
2 & 2 & 2\\
    \hline
  \end{tabular}
\end{table}
"""
    if firstrow:
        ncol = len(firstrow)
    else:
        ncol = len(data[0])
    if caption:
        capt = r"\caption{%s}" % caption
    else:
        capt = ''
    aligncode = 'c'
    fmt = "|"+sepr([aligncode for i in range(ncol)], "|")+"|"
    if label:
        lab = r"\label{%s}" % label
    else:
        lab = ""
    if firstrow:
        fr = r"""%(_LatexTableRow(firstrow))s
\hline
""" % EvalDict()
    else:
        fr = ""
    datstr = seprnl(map(_LatexTableRow, data))
    tbl = r"""
\begin{table}
  \centering
  %s%s
  \begin{tabular}{%s}
\hline
  %s%s
    \hline
  \end{tabular}
\end{table}
""" % (capt, lab, fmt, fr, datstr)
    return tbl
