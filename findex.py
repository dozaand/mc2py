import re
from mc2py.util import PathFind


def StrTest(data, li):
    return None not in [re.search(i, data) for i in li]

res = []
for nm in PathFind("*.py"):
    data = open(nm, "rt").read()
    if StrTest(data, ["h5py", "__enter__", "append"]):
        res.append(nm)
print res
