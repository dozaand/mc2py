#!/usr/bin/env python3
"""
вообще простейший скан
"""

import re
import argparse
import json
from itertools import groupby
from pathlib import Path
import sqlite3
import logging
from mc2py.spjiter import spjiter

index_memgroup = 0


typedkt={(1,1):"bool",
         (1,2):"uint16_t",
         (1,4):"uint32_t",
         (2,1):"int8_t",
         (2,2):"short",
         (2,4):"int",
         (4,4):"float",
         (4,8):"double",
         (5,8):"complex<float>",
         (5,16):"complex<double>",
         (6,1):"LQ"
         }

def giwT(t,tl):
    """"""
    if t==3:
        if tl==1:
            return "char"
        else:
            return "char[{}]".format(tl)
    else:
        try:
            return typedkt[(t,tl)]
        except:
            return "xxx"




def link_blocks(li_common):
    """устанавливаем связи блоков и интерфейсов
    """
    global index_memgroup
    li_common.sort(key=lambda x: (x["a"], not x["nm"].startswith("#")))
    lcls = 0
    for i in li_common:
        endpos = i["a"] + i["L"]
        if endpos > lcls:
            lcls = endpos
        i["lcls"] = lcls
    memgroups = []  # одна переменная считается ведущей остальные к ней привязаны
    for k, itr in groupby(li_common, key=lambda x: x["lcls"]):
        objs = list(itr)
        for i in objs:
            memgroups.append([index_memgroup, i["nm"]])
        index_memgroup += 1
        nm = re.sub("#.+#", "", objs[0]["nm"])
        addr0 = objs[0]["a"]
        for el in objs[1:]:
            if "u" in el:
                s, c, a = el["u"][-1]
                off = a - addr0
                res = [s, c]
                if off != 0:
                    res.append(off)
                if nm != el["nm"]:
                    res.append(nm)
                el["u"][-1] = res
    for i in li_common:
        i.pop("a")
        i.pop("lcls")
    return memgroups


def sqlwrite(cursorObj, memblk, ifs):
    cursorObj.execute("""CREATE TABLE IF NOT EXISTS memblk(
                          name text PRIMARY KEY, 
                          cls text, 
                          type test, 
                          sizeof integer)""")
    cursorObj.execute("""DELETE FROM memblk""")

    cursorObj.execute("""CREATE TABLE IF NOT EXISTS ifs(
                          name text PRIMARY KEY, 
                          type text, 
                          sizeof integer, 
                          use text)""")
    cursorObj.execute("""DELETE FROM memblk""")

    # name    cls      type , sizeof
    memblk1 = {v["nm"]: v for v in memblk}  # чтобы избежать противоречивой информации
    dat = [(v["nm"], v["cls"], v["t"], v["L"]) for v in memblk1.values()]
    cursorObj.executemany('INSERT INTO memblk VALUES (?,?,?,?)', dat)

    #         name    type , sizeof           use
    dat = [(v["nm"], v["t"], v["L"], json.dumps(v["u"])) for k, v in ifs.items()]
    cursorObj.executemany('INSERT INTO ifs VALUES (?,?,?,?)', dat)


def create_memgrous(cursorObj):
    """изготовление связи файл-переменная"""
    cursorObj.execute(f"""CREATE TABLE IF NOT EXISTS memgroups(
                          id integer PRIMARY KEY, 
                          grp integer, 
                          var text)""")
    cursorObj.execute(f"""DELETE FROM memgroups""")
    cursorObj.execute(f"CREATE INDEX IF NOT EXISTS idx_memgroups_g ON memgroups (grp);")
    cursorObj.execute(f"CREATE INDEX IF NOT EXISTS idx_memgroups_v ON memgroups (var);")


def save_memgroups(cursorObj, memgroups):
    cursorObj.executemany(f'INSERT INTO memgroups (grp,var) VALUES (?,?)', memgroups)


def mk_db(files_in,con):
    cursorObj = con.cursor()
    ifs = {}
#    create_memgrous(cursorObj)
    memblk = []
    for vtb_name in files_in:
        s = Path(vtb_name).stem[:-1]
        print(s)
        try:
            with open(vtb_name, "r", encoding="cp866") as f:
                data = f.read()
            li_common = []
            res = re.findall("^ *(\d+)\t([A-Z])\t +(\d+)\t(\d+)\*(\d+)\t(.+)", data, re.M)
            for line in res:
                addr = int(line[0])
                cls = line[1]
                L = int(line[2])
                t = int(line[3])
                lt = int(line[4])
                nm = line[5]
                if cls in "WV":
                    obj = dict(nm=nm, cls=cls, t=giwT(t,lt), L=L*lt, a=addr)
                    memblk.append(obj)
                    li_common.append(obj)
                else:
                    iobj = ifs.get(nm, None)
                    key = [s, cls, addr]
                    if iobj:
                        iobj["u"].append(key)
                    else:
                        obj = dict(nm=nm, t=giwT(t,lt), L=L*lt, u=[key], a=addr)
                        ifs[nm] = obj
                        li_common.append(obj)
            memgroups = link_blocks(li_common)
#            save_memgroups(cursorObj, memgroups)
            con.commit()
        except FileNotFoundError as err:
            logging.warning(f"fail {vtb_name} {err}")
    sqlwrite(cursorObj, memblk, ifs)
    con.commit()


def p_rel(x: Path, root: Path) -> str:
    """из объекта path делаем относительный путь"""
    return str(x)
    # try:
    #     return str(x.relative_to(root.parent))
    # except:
    #     logging.error(f"fail make relative path {x} {root.parent}")
    #     return str(x)


def make_src2var(myspj, file2index, db_names):
    "для каждого файла задаем список переменных которые в нем нашлись"
    file_names = {}
    for i, syslist in spjiter(myspj, ["*.for", "*.FOR", "*.cpp", "*.h", "*.idf"], sys_ret=True):
        try:
            try:
                with open(i, "r", encoding="cp1251") as f:
                    data = f.read().upper()
            except:
                with open(i, "r", encoding="utf-8") as f:
                    data = f.read().upper()
            curr_file_vars = []
            vars = set(re.findall(b"([A-Z_\xc0-\xdf0-9]+)", data, re.MULTILINE))
            for j in vars:
                for s in syslist:
                    curr_file_vars.append(f"#{s}#{j}")
            isect_vars = db_names.intersection(set(curr_file_vars))
            if len(isect_vars) > 0:
                file_id = file2index[p_rel(i, myspj)]
                file_names[file_id] = isect_vars
        except:
            logging.warning(f"fry read content for for var<->file link fails {i}")
    return file_names


def make_bin2var(myspj, file2index, db_names):
    "для каждого файла но бинарного типа *.fgi"
    file_names = {}
    for i in spjiter(myspj, ["*.fgi", "*.FGI"]):
        try:
            with open(i, "rb") as f:
                data = f.read().upper()
            curr_file_vars = []
            vars = set(re.findall(b"([A-Z_][A-Z_0-9]+)", data, re.MULTILINE))
            curr_file_vars = set([i.decode("cp1251") for i in vars])
            isect_vars = db_names.intersection(curr_file_vars)
            if len(isect_vars) > 0:
                file_id = file2index[p_rel(i, myspj)]
                file_names[file_id] = isect_vars
        except:
            logging.warning(f"fail on file {i}")
            raise
    return file_names


def file_id_var_iter(id2vars):
    for k, v in id2vars.items():
        for var in v:
            yield k, var


def add_all_project_files(cursorObj, myspj):
    """добавляем все интересные файлы проекта"""
    cursorObj.execute("""CREATE TABLE IF NOT EXISTS files(
                          id integer PRIMARY KEY AUTOINCREMENT, 
                          file text UNIQUE )""")
    cursorObj.execute("""DELETE FROM files""")
    cursorObj.execute("CREATE INDEX IF NOT EXISTS idx_files_file ON files (file);")
    itr = map(lambda x: (p_rel(x, myspj),),
              spjiter(myspj, ["*.for", "*.FOR", "*.idf", "*.IDF", "*.fgi", "*.fgi", "*.cpp", "*.h", "*.spj"]))
    cursorObj.executemany('INSERT INTO files (file) VALUES (?)', itr)


def make_tbl_file2var(cursorObj, table_name, file_names):
    """изготовление связи файл-переменная"""
    cursorObj.execute(f"""CREATE TABLE IF NOT EXISTS {table_name}(
                          id integer PRIMARY KEY, 
                          id_file integer, 
                          var text)""")
    cursorObj.execute(f"""DELETE FROM {table_name}""")
    cursorObj.execute(f"CREATE INDEX IF NOT EXISTS idx_file_{table_name} ON {table_name} (id_file);")
    cursorObj.execute(f"CREATE INDEX IF NOT EXISTS idx_var_{table_name} ON {table_name} (var);")
    cursorObj.executemany(f'INSERT INTO {table_name} (id_file,var) VALUES (?,?)', file_id_var_iter(file_names))


def make_file2var_links( myspj, con):
    cursorObj = con.cursor()
    add_all_project_files(cursorObj, myspj)
    file2index = dict([i for i in cursorObj.execute(f"SELECT file,id FROM files")])

    prog_names = set([i[0] for i in cursorObj.execute(f"SELECT name FROM memblk")])
    file_names = make_src2var(myspj, file2index, prog_names)
    make_tbl_file2var(cursorObj, "src2mem", file_names)
    del prog_names
    del file_names
    con.commit()

    iface_names = set([i[0] for i in cursorObj.execute(f"SELECT name FROM ifs")])
    file_names = make_bin2var(myspj, file2index, iface_names)
    make_tbl_file2var(cursorObj, "src2ifs", file_names)
    del iface_names
    del file_names
    con.commit()


def project2db(myspj,file_out):
    files_in = [i.with_suffix(".vtb") for i in spjiter(myspj, "*.vpt")]
    with sqlite3.connect(file_out) as con:
        mk_db(files_in, con)
        make_file2var_links( myspj, con)


def test_2db():
    myspj = Path("/mnt/sda2/takz/KNPP_TAKZ_F_20200326_olya/VER_202003/prj/models/takz_338.spj")
    file_out = "takz338.sqlite3"
    #files_in = [i.with_suffix(".vtb") for i in myspj.parent.glob("**/*.vpt")]
    project2db(myspj,file_out)

#test_2db()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="vpt2db",description="get")
    parser.add_argument("-o","--out",help="output database")
#    parser.add_argument("vtb", nargs="+",help=u"root spj")
    parser.add_argument("spj",help=u"root spj")
    args = parser.parse_args()
    project2db(Path(args.spj), args.out)
