#!/usr/bin/env python
# -*- coding: cp1251 -*-
import operator
import types

class Tmultibool:
    u"""������������ ��������
�������� �� ���������� ���� �����������.
������������ ��� ��������� ������ ����������� ������
���� ������� ������������� �� ������������ "|" � ������ ����� ������� ����������� �� ����
���� ������ �������� � ������ � ������ ������� �� ������� ������ ������� �� ���� ��� �������� "&"
    """
    def __init__(self,id=0):
        self.value=id
    def __and__(self,el):
        res=Tmultibool()
        res.value=max(self.value,el.value)
        return res
    def __or__(self,el):
        res=Tmultibool()
        res.value=min(self.value,el.value)
        return res
    def __repr__(self):
        return "mbool:"+str(self.value)
#  0- false 1- true 2-maybe
_tribooland=(
(0,0,0),
(0,1,2),
(0,2,2)
)
_triboolor=(
(0,1,2),
(1,1,1),
(2,1,2)
)
class T3bool:
    u"""��� ������ �������� 0 -false 1 - true 2 -maybe
    """
    def __init__(self,id=0):
        if id.__class__ is types.IntType:
            if 0>id<2:
                raise RuntimeError("T3bool must be inited by 0,1,2")
            self.value=id
        elif id.__class__ is types.StringType:
            self.value={'T':1,'F':0,'MB':2}[id]
        else:
            self.value=id
#            raise RuntimeError("invalit type init T3bool")
    def __and__(self,el):
		return T3bool(_tribooland[self.value][el.value])
    def __or__(self,el):
		return T3bool(_triboolor[self.value][el.value])
    def __repr__(self):
        return ['F','T','MB'][self.value]

def to_Tdmap(obj):
    u"""�������������� � ���� Tdmap"""
    if obj.__class__ is Tdmap:
        return obj
    if obj.__class__ is types.IntType:
        return Tdmap("",float(obj))
    if obj.__class__ is types.FloatType:
        return Tdmap("",obj)
    return None

class Tdmap:
    u"���� ��������� �������� ��� ������� xy ������ �������� f"
    def __init__(self,dsc="",vals={},keys={}):
        self.dsc=dsc
        self.op=None
        self.arg=[]
        if (vals.__class__ is types.FloatType) or (vals.__class__ is types.IntType) or (vals.__class__ is types.NoneType) or (vals.__class__ is T3bool):
            self.is_val=1
            self.val=vals
        else:
            self.is_val=0
            if len(keys) != len(vals):
                raise RuntimeError("number of koords != number of data " + str(len(keys)) +str(len(vals)))
            self.data=dict(zip(keys,vals))

    def __getitem__(self,key):
        if self.is_val:
            return val
        else:
            return self.data[key]

    def __setitem__(self,key,val):
        if self.is_val:
            self.val=val
        else:
            self.data[key]=val
# ���������� ���� �� ��������� � �����
    def __coerce__(self,obj):
        if obj.__class__ is types.IntType:
            res=Tdmap("",float(obj))
            return self,res
        elif obj.__class__ is types.FloatType:
            res=Tdmap("",obj)
            return self,res


# operations
    def __add__(self,val):
        return dispf(self,val,operator.add)
    def __radd__(self,val):
        return dispf(self,val,operator.add)
    def __sub__(self,val):
        return dispf(self,val,operator.sub)
    def __rsub__(self,val):
        return dispf(self,val,operator.rsub)
    def __mul__(self,val):
        return dispf(self,val,operator.mul)
    def __div__(self,val):
        return dispf(self,val,operator.div)
    def __rdiv__(self,val):
        return dispf(self,val,operator.rdiv)
# double->3bool
    def __lt__(self,val):
        return dispf(self,to_Tdmap(val),operator.lt)
    def __le__(self,val):
        return dispf(self,to_Tdmap(val),operator.le)
    def __eq__(self,val):
        return dispf(self,to_Tdmap(val),operator.eq)
    def __ne__(self,val):
        return dispf(self,to_Tdmap(val),operator.ne)
    def __ge__(self,val):
        return dispf(self,to_Tdmap(val),operator.ge)
    def __gt__(self,val):
        return dispf(self,to_Tdmap(val),operator.gt)
# multibool->3bool
    def __or__(self,val):
        return dispf(self,val,operator.or_)
    def __and__(self,val):
        return dispf(self,val,operator.and_)

    def __repr__(self):
        if self.is_val:
            return str(self.val)
        else:
            return str(self.data)


def donop(v1,v2,op):
    u"""��� ���������� �������� ���������� ��� ������ � None"""
    if op in (operator.le,operator.lt,operator.ge,operator.gt,operator.ne,operator.eq):
        if v1 is None or v2 is None:
            return T3bool(2)
        else:
            return T3bool(op(v1,v2))
    if v1 is None or v2 is None:
        return None
    return op(v1,v2)

def ddop(el,v1,v2,f):
    u"""���������� �������� ��� ����� ��������"""
    keys=set.intersection(set(v1.data.keys()),set(v2.data.keys()))
    for key in keys:
        el.data[key]=donop(v1.data[key],v2.data[key],f)
    return el

def dnop(el,v1,v2,f):
    u"""���������� �������� ��� ������� � ������"""
    for (key,v) in v1.data.iteritems():
        el.data[key]=donop(v,v2.val,f)
    return el

def ndop(el,v1,v2,f):
    u"""���������� �������� ��� ������ � �������"""
    for (key,v) in v2.data.iteritems():
        el.data[key]=donop(v1.val,v,f)
    return el

def nnop(el,v1,v2,f):
    u"""���������� �������� ��� ������ � ������"""
    el.is_val=1
    el.val=donop(v1.val,v2.val,f)
    return el

def dispf(v1,v2,op):
    u"���������� ��������������� �� ����� ����������"
    el=Tdmap()
    el.is_val=0
    el.op=op
# ��� ���������� �������� �������� ��� ���������
    if op is v1.op:
        el.arg=v1.arg+[v2]
    else:
        el.arg=[v1,v2]
# ��������������� �� ���� ������
    if v1.is_val :
        if v2.is_val:
            return nnop(el,v1,v2,op)
        else:
            return ndop(el,v1,v2,op)
    else:
        if v2.is_val:
            return dnop(el,v1,v2,op)
        else:
            return ddop(el,v1,v2,op)

class Tcalc:
    u"""����� ��� ���������� ���������
�� ����� formula_file,
������ ����� �� ����� data_file
����� ������������ ������ ���������� � geometry_file"""
    def __init__(self,formula_file,data_file,geometry_file):
        self.formula_file=formulas
        self.data_file=data_file
        self.geometry_file=geometry_file
    def update():
        u"""��������� ���������� formula_file � data_file"""
        pass



def main():
    kart1=[(i,i) for i in range(5)]
    kart2=[(i,i) for i in range(5) if i % 2 ==0]
    po=Tdmap(u"�������� ��������",3000)
    a=Tdmap(u"�������� a",[2.2*i for i in range(5)],kart1)
    b=Tdmap(u"�������� b",[3.3*i for i in range(5) if i % 2 ==0],kart2)
    a[0,0]=None
    cond1=33.3<a
    cond1.dsc=u"������� 1"
    cond2=3<a<100
    cond2.dsc=u"������� 1"
    res= cond1 | cond2
    res.dsc=u"�������� �������"
    print res



if __name__ == '__main__':
    main()