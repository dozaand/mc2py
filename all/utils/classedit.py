#!/usr/bin/env python
# -*- coding: cp1251 -*-
#-------------------------------------------------------------------------------
# Name:        classed
# Purpose:  �������� ��� ������
#
# Author:      and
#
# Created:     02.09.2010
# Copyright:   (c) and 2010
# Licence:
#-------------------------------------------------------------------------------
import wx
from wx.lib.mixins.listctrl import TextEditMixin

class EditableTextListCtrl(wx.ListCtrl, TextEditMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        TextEditMixin.__init__(self)

class FrameClassEd(wx.Frame):
    def __init__(self,obj,parent=None, id=-1, title=""):
        self.edObj=obj
        wx.Frame.__init__(self, parent, id, title)
        self.list = EditableTextListCtrl(self, -1, style=wx.LC_REPORT|wx.LC_VRULES|wx.LC_HRULES)
        self.list.InsertColumn(0,"���")
        self.list.InsertColumn(1,"��������")
        self.list.InsertColumn(2,"���")
        i=0
        for x in dir(self.edObj):
            index = self.list.InsertStringItem(i,x)
            v=getattr(self.edObj,x)
            self.list.SetStringItem(index, 1, str(v))
            self.list.SetStringItem(index, 2, str(type(v)))
            i+=1
        self.list.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.list.SetColumnWidth(1, wx.LIST_AUTOSIZE)
        self.list.SetColumnWidth(2, wx.LIST_AUTOSIZE)

        self.Bind(wx.EVT_LIST_END_LABEL_EDIT, self.OnEditItem,
        self.list)
        pass

    def OnEditItem(self, evt):
        item=evt.GetItem()
        if not item.IsEditCancelled():
            txt=GetLabel()


class Tf:
    def __init__(self):
        self.a=5
        self.b="asd"
        self.c=[3.45,2]


class AppFrameClassEd(wx.App):
    def __init__(self, obj,redirect=True):
        self.edObj=obj
        wx.App.__init__(self, redirect)
        pass
    def OnInit(self):
        self.frame = FrameClassEd(self.edObj,parent=None,id=-1, title='Spare')
        self.frame.Show()
        self.SetTopWindow(self.frame)
        return True

if __name__ == '__main__':
    obj=Tf()
    app = AppFrameClassEd(obj,redirect=True)
    app.MainLoop()

