#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      and
#
# Created:     12.10.2010
# Copyright:   (c) and 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import datetime,os,glob,zlib
from ZODB import FileStorage, DB
from persistent import Persistent
from BTrees.OOBTree import OOBTree

import transaction

class TDirFiles(Persistent):
    def __init__(self):
        self.files={}

    def ImportDir(self,direct,tim):
        currfiles={}
        for i in glob.glob(direct+'/*'):
            fname=os.path.basename(i)
            currfiles[fname]=zlib.compress(open(i,'rb').read())
        for nm,data in currfiles.iteritems():
            if nm in self.files:
                savedFileArray=self.files[nm]
                if savedFileArray.values()[-1]!=data:
                    savedFileArray[tim]=data
            else:
                tr=OOBTree()
                tr[tim]=data
                self.files[nm]=tr

    def WriteSrez(self,tim):
        for i,arr in files:
            key=arr.keys(arr.begin(),tim)
            if len(key)!=0:
                data=arr[key[-1]]
                f=open(dr+"/"+name,)

def main():
    storage = FileStorage.FileStorage('dirdb.fs')
    db = DB(storage)
    connection = db.open()
    root = connection.root()
    str="kursk_b1"
    if str not in root:
        obj=TDirFiles()
        root[str]=obj
    else:
        obj=root[str]
    obj.ImportDir("dat",datetime.datetime.today())
    transaction.commit()
    connection.close()
    db.close()


if __name__ == '__main__':
    main()