import wx
import wx.gizmos

def browse(obj,displayPrivates=False,displayCallables=False,verboseDict=False) :
	print "OTS Object Browser, www.opentradingsystem.com"
	print "Copyright 2009, Konstantin Aslanidi"
	_otsObjectBrowserApp = wx.App(False)
	try:
		ObjectBrowser(obj,displayPrivates,displayCallables,verboseDict).Show()
		_otsObjectBrowserApp.MainLoop()
	finally:
		del _otsObjectBrowserApp


class ObjectBrowser:
	_toBeExpandedTag="ToBeExpanded"
	def __init__(self,obj,displayPrivates,displayCallables,verboseDict):
		self._object=obj
		self._displayPrivates=displayPrivates
		self._displayCallables=displayCallables
		self._verboseDict=verboseDict
		self._frame=wx.Frame( None, wx.ID_ANY, title="OTS Python Object Browser", size=(500,500))    
		self._tree = wx.gizmos.TreeListCtrl(self._frame, style = wx.TR_DEFAULT_STYLE | wx.TR_FULL_ROW_HIGHLIGHT)
		self._frame.Bind(wx.EVT_TREE_ITEM_EXPANDED, self._OnItemExpanded, self._tree)
		self._tree.AddColumn("Name")
		self._tree.AddColumn("Type")
		if self._displayCallables :
			self._tree.AddColumn("Callable")
		self._tree.SetMainColumn(0) 
		self._tree.SetColumnWidth(0,200)
		self._tree.SetColumnWidth(1,200)
		if self._displayCallables :
			self._tree.SetColumnWidth(2,200)
		root = self._tree.AddRoot(self._rootObjectDescriptor())
		self._tree.SetItemText(root, type(self._object).__name__, 1)
		if self._displayCallables :
			self._tree.SetItemText(root, self._toStr(callable(self._object)), 2)
		self._populateItem(root,self._object)
		self._tree.Expand(root)
		
	def _rootObjectDescriptor(self) :
		if type(self._object)==type("") :
			return self._object
		return "root"
		
	def _toStr(self,o) :
		try :
			return o.__str__()
		except :
			return "__str__() is not defined"
	
	def Show(self) :
		self._frame.Show()
		
	def _populateItem(self,parent,obj) :
		handled=False
		if type(obj)==type('') :
			self._addToTree( \
				parent=parent, \
				itemStr="'"+obj+"'", \
				typeStr=type(obj).__name__, \
				isCallableBool=False, \
				hasChildrenBool=False, \
				treeData=None \
				)
			handled=True
		if type(obj)==type([]) or type(obj)==type(()) :
			i=0
			for x in obj :
				self._addToTree( \
					parent=parent, \
					itemStr='['+self._toStr(i)+']', \
					typeStr='', \
					isCallableBool=False, \
					hasChildrenBool=True, \
					treeData=x \
					)
				i=i+1
			handled=True
		if type(obj)==type({}) :
			i=0
			for x in obj :
				if self._verboseDict :
					self._addToTree( \
						parent=parent, \
						itemStr='['+self._toStr(x)+']', \
						typeStr='', \
						isCallableBool=False, \
						hasChildrenBool=True, \
						treeData=(x,obj[x]) \
						)
				else :
					self._addToTree( \
						parent=parent, \
						itemStr='['+self._toStr(x)+']', \
						typeStr=type(obj[x]).__name__, \
						isCallableBool=False, \
						hasChildrenBool=True, \
						treeData=obj[x] \
						)
				i=i+1
			handled=True
		if type(obj)==type(1) or type(obj)==type(0.1) :
			self._addToTree( \
				parent=parent, \
				itemStr=self._toStr(obj), \
				typeStr=type(obj).__name__, \
				isCallableBool=False, \
				hasChildrenBool=False, \
				treeData=None \
				)			
			handled=True
		if not handled :
			self._addToTree( \
				parent=parent, \
				itemStr=self._toStr(obj), \
				typeStr=type(obj).__name__, \
				isCallableBool=False, \
				hasChildrenBool=False, \
				treeData=obj \
				)					
		for item in dir(obj) :
			if not self._displayPrivates and item[0]=='_' :
				continue
			attr=getattr(obj,item)
			if not self._displayCallables and callable(attr) :
				continue
			ni=self._addToTree( \
				parent=parent, \
				itemStr=item, \
				typeStr=type(attr).__name__, \
				isCallableBool=callable(attr), \
				hasChildrenBool=self._hasChildren(attr), \
				treeData=attr \
				)
			if self._hasValue(attr) : 
				self._addToTree( \
					parent=ni, \
					itemStr=self._getValueStr(attr), \
					typeStr='', \
					isCallableBool=False, \
					hasChildrenBool=False, \
					treeData=attr \
					)
					
	def _hasValue(self,attr) :
		return type(attr)==type(1) or type(attr)==type(0.1) or type(attr)==type('')
		
	def _getValueStr(self,attr) :
		return self._toStr(attr)
				
	def _addToTree(self,parent,itemStr,typeStr,isCallableBool,hasChildrenBool,treeData) :
			newItem=self._tree.AppendItem(parent,itemStr)
			self._tree.SetItemText(newItem,typeStr,1)
			if self._displayCallables :
				self._tree.SetItemText(newItem,isCallableBool.__str__(),2)
			if not treeData is None :
				self._tree.SetItemPyData(newItem,treeData)
			if hasChildrenBool :
				self._tree.AppendItem(newItem,self._toBeExpandedTag)    
			return newItem
			

	def _hasChildren(self,obj) :
		for item in dir(obj) :
			if not self._displayPrivates and item[0]=='_' :
				continue
			return True
		return False
        
	def _OnItemExpanded(self, evt):
		item=evt.GetItem()
		child=self._tree.GetFirstChild(item)[0]
		if self._tree.GetItemText(child,0)==self._toBeExpandedTag :
			self._tree.DeleteChildren(item)
			obj=self._tree.GetItemPyData(item)
			self._populateItem(item,obj)
        
