#include "planedsk.h"
//#include <INCLUDE/err_msg.h>
#include <algorithm>
#include <vector>

using namespace std;
const int NG=2,NX=15,NY=15,NZ=10;
const int pbounds[2][NY]={{1, 0, 0, 0, 0, 0, 0, 1, 1, 2, 3, 4, 5, 6, 8}, {7, 9, 10, 11, 12, 13, 14,14, 15, 15, 15, 15, 15, 15, 14}};
enum DIRECTIONS {DIR_Z,DIR_X,DIR_Y,DIR_XY};//!< ���� �����������



 Tplane_dsk::Tplane_dsk(){beg=NULL;set(0,15,pbounds[0],pbounds[1]);}
// ! �������� ��������� �������� ����
 void Tplane_dsk::set(int y0_,int y1_,const int *begs,const int * ends)
 {
  y0=y0_;
  y1=y1_;
  int size=y1-y0;
  x0=begs[0];x1=ends[0];
  if(beg!=NULL) delete [] beg;
  beg=new int[size*3];
  end=beg+size;
  offset=end+size;
  int i;
  for(i=0;i<size;i++)
  {
   beg[i]=begs[i];
   end[i]=ends[i];
   x0=min(x0,beg[i]);
   x1=max(x1,end[i]);
  }
  offset[0]=0;
  for(i=1;i<size;i++)
  {
   offset[i]=offset[i-1]+end[i-1]-beg[i-1];
  }
  ntot=offset[size-1]+end[size-1]-beg[size-1];
 }
 void Tplane_dsk::set(int y0_,int y1_,const int (*bnd)[2])
 {
  y0=y0_;
  y1=y1_;
  int size=y1-y0;
  x0=bnd[0][0];x1=bnd[0][1];
  if(beg!=NULL) delete [] beg;
  beg=new int[size*3];
  end=beg+size;
  offset=end+size;
  int i;
  for(i=0;i<size;i++)
  {
   beg[i]=bnd[i][0];
   end[i]=bnd[i][1];
   x0=min(x0,beg[i]);
   x1=max(x1,end[i]);
  }
  offset[0]=0;
  for(i=1;i<size;i++)
  {
   offset[i]=offset[i-1]+end[i-1]-beg[i-1];
  }
  ntot=offset[size-1]+end[size-1]-beg[size-1];
 }

 //! ����������� ��������� ������ � ������� ��� ������� �������� ������
 int Tplane_dsk::ofs(int y,int x) const
 {
  if(y<0||y>=y1)
  {
   return -1;
  }
  if(x<beg[y]||x>=end[y])
  {
   return -1;
  }
  return offset[y]+(x-beg[y]);
 }
 //! �������������� �������� (�������� )����������� � �����������
 void short_to_long(const Tplane_dsk* pl,const int * s_k,int * l_k)
 {
  int x,y,dx,dy,i;
  dx=pl->x1-pl->x0;
  dy=pl->y1-pl->y0;
  for(i=0;i<dx*dy;i++)l_k[i]=-1;
  for(y=pl->y0,i=0;y<pl->y1;y++)
  {
   for(x=pl->beg[y];x<pl->end[y];x++)
   {
    l_k[(y-pl->y0)*dx+x-pl->x0]=s_k[i++];
   }
  }
 }
 //! �������������� ����������� (� �������������� )����������� � �����������
 void long_to_short(const Tplane_dsk* pl,const int * l_k,int * s_k)
 {
  int x,y,i,dx;
  dx=pl->x1-pl->x0;
  for(y=pl->y0,i=0;y<pl->y1;y++)
  {
   for(x=pl->beg[y];x<pl->end[y];x++)
   {
    s_k[i++]=l_k[(y-pl->y0)*dx+x-pl->x0];
   }
  }
 }
 //! �������� ������� ��� �����������
 /*
 \param y0 ����� ������ ������
 \param y1 ����� �� ��������� �������
 \param b ������ ����� �����
 \param e ������ �� ������ �����
 */
 Tplane_d::Tplane_d(int y0_,int y1_,const int *b,const int * e):Tplane_dsk(){Tplane_dsk::set(y0_,y1_,b,e);y0=y0_;}
 Tplane_d::Tplane_d(int y0_,int y1_,const int (*rg)[2]):Tplane_dsk(y0_,y1_,rg){y0=y0_;}

 //! ����������� ��������� ������ � ������� ��� ������� �������� ������
 int Tplane_d::ofs(int y,int x) const
 {
  if( (y<y0) || (y>=y1) )
  {
   return -1;
  }
  y-=y0;
  if(x<beg[y]||x>=end[y])
  {
   return -1;
  }
  return offset[y]+(x-beg[y]);
 }
 Tplane_dsk_wb::Tplane_dsk_wb():Tplane_dsk(),bnd_type(6){fill_bnd();}

 void Tplane_dsk_wb::fill_bnd()
 {
  int x,y,l,l1;
  int ny=y1-y0;
  vector<Tbound_dsk> bnd;
  Tbound_dsk el;
  for(y=0,l=0;y<ny;y++)
  {
   for(x=beg[y];x<end[y];x++,l++)
   {
	l1=ofs(y,x-1);if(l1<0){el.x=x;el.y=y;el.l=l;el.dir=DIR_X;bnd.push_back(el);}
	l1=ofs(y,x+1);if(l1<0){el.x=x;el.y=y;el.l=l;el.dir=DIR_X;bnd.push_back(el);}
	l1=ofs(y-1,x);if(l1<0){el.x=x;el.y=y;el.l=l;el.dir=DIR_Y;bnd.push_back(el);}
	l1=ofs(y+1,x);if(l1<0){el.x=x;el.y=y;el.l=l;el.dir=DIR_Y;bnd.push_back(el);}
	if(bnd_type==6)
	{
	 l1=ofs(y-1,x-1);if(l1<0){el.x=x;el.y=y;el.l=l;el.dir=DIR_XY;bnd.push_back(el);}
	 l1=ofs(y+1,x-1);if(l1<0){el.x=x;el.y=y;el.l=l;el.dir=DIR_XY;bnd.push_back(el);}
	}
   }
  }
  nbnd=bnd.size();
  bound=new Tbound_dsk[nbnd];
  int i;
  for(i=0;i<nbnd;i++)
  {
   bound[i]=bnd[i];
  }
 }

Tplane_d* wwer1000_geom()
{
 return new Tplane_d(0,NY,pbounds[0],pbounds[1]);
}

int rbmk2488_y0=0;
int rbmk2488_y1=56;
int be_rbmk2488[][2]={{23, 33}, {18, 38}, {16, 40}, {14, 42}, {12, 44}, {11, 45}, {10, 46}, {9,
      47}, {8, 48}, {7, 49}, {6, 50}, {5, 51}, {4, 52}, {4, 52}, {3, 53}, {3,
      53}, {2, 54}, {2, 54}, {1, 55}, {1, 55}, {1, 55}, {1, 55}, {1, 55}, {0,
      56}, {0, 56}, {0, 56}, {0, 56}, {0, 56}, {0, 56}, {0, 56}, {0, 56}, {0,
      56}, {0, 56}, {1, 55}, {1, 55}, {1, 55}, {1, 55}, {1, 55}, {2, 54}, {2,
      54}, {3, 53}, {3, 53}, {4, 52}, {4, 52}, {5, 51}, {6, 50}, {7, 49}, {8,
      48}, {9, 47}, {10, 46}, {11, 45}, {12, 44}, {14, 42}, {16, 40}, {18,
      38}, {23, 33}};

int rbmk1884_y0=8;
int rbmk1884_y1=56;
int be_rbmk1884[][2]=
{{25, 39}, {22, 42}, {20, 44}, {18, 46}, {17, 47}, {16, 48}, {15, 49}, {14,
    50}, {13, 51}, {12, 52}, {11, 53}, {11, 53}, {10, 54}, {10, 54}, {9,
    55}, {9, 55}, {9, 55}, {8, 56}, {8, 56}, {8, 56}, {8, 56}, {8, 56}, {8,
    56}, {8, 56}, {8, 56}, {8, 56}, {8, 56}, {8, 56}, {8, 56}, {8, 56}, {8,
    56}, {9, 55}, {9, 55}, {9, 55}, {10, 54}, {10, 54}, {11, 53}, {11,
    53}, {12, 52}, {13, 51}, {14, 50}, {15, 49}, {16, 48}, {17, 47}, {18,
    46}, {20, 44}, {22, 42}, {25, 39}};

Tplane_d* rbmk1884_geom()
{
 return new Tplane_d(rbmk1884_y0,rbmk1884_y1,be_rbmk1884);
}
Tplane_d* rbmk2488_geom()
{
 return new Tplane_d(rbmk2488_y0,rbmk2488_y1,be_rbmk2488);
}