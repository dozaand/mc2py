object plotform: Tplotform
  Left = 237
  Top = 244
  Width = 704
  Height = 496
  Caption = 'miniplot'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Chart: TChart
    Left = 0
    Top = 0
    Width = 696
    Height = 454
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    View3D = False
    Align = alClient
    TabOrder = 0
    OnClick = ChartClick
    object StatusBar1: TStatusBar
      Left = 1
      Top = 452
      Width = 694
      Height = 1
      Panels = <>
      SimplePanel = False
    end
  end
  object StatusBar2: TStatusBar
    Left = 0
    Top = 454
    Width = 696
    Height = 15
    Panels = <>
    SimplePanel = False
  end
end
