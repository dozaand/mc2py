//---------------------------------------------------------------------------

#ifndef plt_dataH
#define plt_dataH
//---------------------------------------------------------------------------
#include <vector>
using namespace std;

class Tplt_data
{
 void def_mima();
public:
 int nz;
 int nxy;
 vector<float> data;
 float mi,ma;
 void set(int nz_,int nxy_,const double * data);
 void set(int nz_,int nxy_,const float * data);
 void load(const char* nm);
 float operator[](int i){return data[i];}
};
#endif
