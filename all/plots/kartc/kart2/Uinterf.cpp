//---------------------------------------------------------------------------


#pragma hdrstop

#include "Uinterf.h"
#include <iostream>
#include "Ukart_view.h"
#include "plot.h"
using namespace std;
//---------------------------------------------------------------------------

#pragma package(smart_init)

void skart(const Tplane_d* geom,int geom_type,const double* d1,const double* d2,const char* caption);

extern "C"
{
int __declspec(dllexport) Kart2Create(int nrow,const int* beg,const int* end)
{
 cout<<"create";
 return 0;
}
void __declspec(dllexport) Kart2Show(int id)
{
}
void __declspec(dllexport) Kart2Hide(int id)
{
}
void __declspec(dllexport) Kart2SetCaption(int id,const char* caption)
{
}

void __declspec(dllexport) Kart2Delete(int)
{
 cout<<"delete";
}
void __declspec(dllexport) Kart2SetDataFullD(int id,double *set1,double* set2)
{
 cout<<"delete";
}
void __declspec(dllexport) Kart2SetDataFullF(int id,float *set1,float* set2)
{
 cout<<"delete";
}

void __declspec(dllexport) Skart1(int nrow,const int* beg,const int* end,int geom_type,const double* d1,const double* d2,const char* caption)
{
 Tplane_d geom(0,nrow,beg,end);
 skart(&geom,geom_type,d1,d2,caption);
}

void __declspec(dllexport) SlineD1(double* d,int n)
{
 sline(d,n);
}
void __declspec(dllexport) SlineD2(double* t,double* d,int n)
{
 sline(t,d,n);
}
void __declspec(dllexport) SlineDN(double** d,int ngraph,int npoint)
{
 sline(d,ngraph,npoint);
}

void __declspec(dllexport) SlineF1(float* d,int n)
{
 sline(d,n);
}
void __declspec(dllexport) SlineF2(float* t,float* d,int n)
{
 sline(t,d,n);
}
void __declspec(dllexport) SlineFN(float** d,int ngraph,int npoint)
{
 sline(d,ngraph,npoint);
}

}
