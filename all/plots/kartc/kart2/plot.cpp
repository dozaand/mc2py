//---------------------------------------------------------------------------


#include <vcl.h>
#pragma hdrstop

#include <arrowcha.hpp>
#include <bubblech.hpp>
#include <teeshape.hpp>
#include "plot.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

 Taxies_info::Taxies_info(TChartAxis * ax):axi(ax){}
 double Taxies_info::min(){return axi->Minimum;}
 double Taxies_info::max(){return axi->Maximum;}
 void Taxies_info::min(double v){axi->AutomaticMinimum=0;axi->Minimum=v;}
 void Taxies_info::max(double v){axi->AutomaticMaximum=0;axi->Maximum=v;}
 void Taxies_info::name(char* str){axi->Title->Caption=str;}
 char* Taxies_info::name()
 {
  AnsiString s(axi->Title->Caption);
  return s.c_str();
 }
 void Taxies_info::set_logscale(int i){axi->Logarithmic=(i!=0);}
 void Taxies_info::automatic(int i){axi->Automatic=i;}
 void Taxies_info::set_grid(int i){}
 void Taxies_info::automin(int i){axi->AutomaticMinimum=i;}
 void Taxies_info::automax(int i){axi->AutomaticMaximum=i;}
 void Taxies_info::visible(int i){axi->Visible=i;}


Tbline::Tbline(TChart* chart)
{
 S=new TFastLineSeries(chart);
 init(chart);
}
Tbpoint::Tbpoint(TChart* chart)
{
 S=new TPointSeries(chart);
 init(chart);
}
Tbbar::Tbbar(TChart* chart)
{
 S=new TBarSeries(chart);
 init(chart);
}
Tbarrow::Tbarrow(TChart* chart)
{
 S=new TArrowSeries(chart);
 init(chart);
}
Tbbubble::Tbbubble(TChart* chart)
{
 S=new TBubbleSeries(chart);
 init(chart);
}
Tbshape::Tbshape(TChart* chart)
{
 S=new TChartShape(chart);
 init(chart);
}

 Tseries_info::Tseries_info():np(500)
 {
 }
 char* Tseries_info::name()
 {
  AnsiString s(S->Title);
  return s.c_str();
 }
 void Tseries_info::name(char* nm){AnsiString ss=nm;S->Title=ss;show_legend();}
 void Tseries_info::color(TColor c){S->SeriesColor=c;}
 void Tseries_info::color(int r,int g,int b){S->SeriesColor=(TColor)RGB(r,g,b);}
 void Tseries_info::color(double r,double g,double b){S->SeriesColor=(TColor)RGB(int(255*r),int(255*g),int(255*b));}
 void Tseries_info::x1(int i)
 {
  if(i){S->HorizAxis=aTopAxis;}
  else{S->HorizAxis=aBottomAxis;}
 }
 void Tseries_info::y1(int i)
 {
  if(i){S->VertAxis=aRightAxis;}
  else{S->VertAxis=aLeftAxis;}
 }
 void Tseries_info::show_legend(int i){S->ShowInLegend=i;}
 void Tseries_info::npoint(int n){np=n;}
 void Tseries_info::operator()(double * x,double * y,int n)
 {
  int i;
  if(S->Count()!=0){S->Clear();}
  S->AddArray(y,n-1);
  for(i=0;i<n;i++)
  {
   S->XValues->Value[i]=x[i];
  }
 }
 void Tseries_info::operator()(double * y,int n)
 {
  if(S->Count()!=0){S->Clear();}
  S->AddArray(y,n-1);
 }
 void Tseries_info::operator()(float * x,float * y,int n)
 {
  int i;
  double * ax,*ay;
  ax=new double[n];
  ay=new double[n];
  for(i=0;i<n;i++){ax[i]=x[i];ay[i]=y[i];}
  (*this)(ax,ay,n);
  delete [] ax;
  delete [] ay;
 }
 void Tseries_info::operator()(float * y,int n)
 {
  int i;
  double * arr;
  arr=new double[n];
  for(i=0;i<n;i++)arr[i]=y[i];
  (*this)(arr,n);
  delete [] arr;
 }
 void Tseries_info::clear()
 {
  S->Clear();
 }



Tbplot::Tbplot():
form(iniform()),
axi_x(form->Chart->BottomAxis),
axi_y(form->Chart->LeftAxis),
axi_x1(form->Chart->TopAxis),
axi_y1(form->Chart->RightAxis)
{
}
Tplotform* Tbplot::iniform()
{
 Tplotform* f;
 Application->CreateForm(__classid(Tplotform), &f);
 return f;
}


Tbplot::~Tbplot()
{
 clear();
 delete form;
}

void Tbplot::operator()()
{
 form->ShowModal();
 mx=form->mouse_x;
 my=form->mouse_y;
}
void Tbplot::clear()
{
 int i;
 for(i=0;i<(int)ser.size();i++)
 {
  ser[i]->clear();
  delete ser[i];
 }
 ser.clear();
}

Tseries_info& Tbplot::operator[](int i)
{
 return *(ser[i]);
}

Tseries_info::~Tseries_info()
{
 S->ParentChart->RemoveSeries(S);
}

Tseries_info* Tbplot::addline()
{
 Tseries_info* e=new Tbline(form->Chart);
 ser.push_back(e);
 return e;
}
Tseries_info* Tbplot::addpoint()
{
 Tseries_info* e=new Tbpoint(form->Chart);
 ser.push_back(e);
 return e;
}
Tseries_info* Tbplot::addbar()
{
 Tseries_info* e=new Tbbar(form->Chart);
 ser.push_back(e);
 return e;
}
Tseries_info* Tbplot::addbubble()
{
 Tseries_info* e=new Tbbubble(form->Chart);
 ser.push_back(e);
 return e;
}
Tseries_info* Tbplot::addarrow()
{
 Tseries_info* e=new Tbarrow(form->Chart);
 ser.push_back(e);
 return e;
}
Tseries_info* Tbplot::addshape()
{
 Tseries_info* e=new Tbshape(form->Chart);
 ser.push_back(e);
 return e;
}
int Tbplot::count(){return ser.size();}




void Tseries_info::init(TChart* chart)
{
 chart->AddSeries(S);
 S->ShowInLegend=false;
}

void sline(double* d,int n)
{
 Tbplot bp;
 Tseries_info* s;
 s=bp.addline();
 (*s)(d,n);
 bp();
}
void sline(double* t,double* d,int n)
{
 Tbplot bp;
 Tseries_info* s;
 s=bp.addline();
 (*s)(t,d,n);
 bp();
}
void sline(double** d,int ngraph,int npoint)
{
 Tbplot bp;
 Tseries_info* s;
 int i;
 for(i=1;i<ngraph;i++)
 {
  s=bp.addline();
  (*s)(d[0],d[i],npoint);
 }
 bp();
}
void sline(float* d,int n)
{
 Tbplot bp;
 Tseries_info* s;
 s=bp.addline();
 (*s)(d,n);
 bp();
}
void sline(float *t,float* d,int n)
{
 Tbplot bp;
 Tseries_info* s;
 s=bp.addline();
 (*s)(t,d,n);
 bp();
}
void sline(float** d,int ngraph,int npoint)
{
 Tbplot bp;
 Tseries_info* s;
 int i;
 for(i=1;i<ngraph;i++)
 {
  s=bp.addline();
  (*s)(d[0],d[i],npoint);
 }
 bp();
}
#include <iostream.h>
void sline(vector<double>& v)
{
 Tbplot bp;
// Tseries_info* s;
// s=bp.addline();
 cout<<"int sline double\n";
 cout.flush();
 cout<<"begin:"<<hex<<&v<<" len:"<<v.size();
 cout.flush();
// (*s)(v.begin(),v.size());
// bp();
}
void sline(vector<double>& t,vector<double>& v)
{
 Tbplot bp;
 Tseries_info* s;
 s=bp.addline();
 int l;
 l=min(t.size(),v.size());
 (*s)(&t[0],&t[0]+t.size(),l);
 bp();
}

void Tbplot::lines(double** d,int ngraph,int npoint)
{
 Tseries_info* s;
 int i;
 for(i=1;i<ngraph;i++)
 {
  s=addline();
  (*s)(d[0],d[i],npoint);
 }
 (*this)();
}
void Tbplot::lines(float** d,int ngraph,int npoint)
{
 Tseries_info* s;
 int i,nlc=ser.size();
 for(i=0;i<nlc-ngraph;i++)
 {
  addline();
 }
 for(i=1;i<ngraph;i++)
 {
  (*this)[i-1](d[0],d[i],npoint);
 }
 (*this)();
}

void Tbplot::rect_all()
{
 axi_x.automatic(1);
 axi_y.automatic(1);
 axi_x1.automatic(1);
 axi_y1.automatic(1);
}


void Tbplot::save_wmf(char* nm)
{
 AnsiString s=nm;
 form->Chart->SaveToMetafile(s);
}

void Tbplot::save_bmp(char* nm)
{
 AnsiString s=nm;
 form->Chart->SaveToBitmapFile(s);
}

void Tbplot::save_emf(char* nm)
{
 AnsiString s=nm;
 form->Chart->SaveToMetafileEnh(s);
}

void Tbplot::name(char* nm)
{
 AnsiString s=nm;
 if(nm==NULL){form->Chart->Title->Visible=0;}
 else{form->Chart->Title->Visible=1;}
 form->Chart->Title->Text->Clear();
 form->Chart->Title->Text->Add(s);
}

void Tbplot::rect(double xmi,double ymi,double xma,double yma)
{
 axi_x.min(xmi);
 axi_y.min(ymi);
 axi_x.max(xma);
 axi_y.max(yma);
}
void Tbplot::rect1(double xmi,double ymi,double xma,double yma)
{
 axi_x1.min(xmi);
 axi_y1.min(ymi);
 axi_x1.max(xma);
 axi_y1.max(yma);
}

