//---------------------------------------------------------------------------

#ifndef Utext_editorH
#define Utext_editorH
//---------------------------------------------------------------------------
#ifndef __MAKECINT__

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <vector.h>
#include <Menus.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class Ttext_editor : public TForm
{
__published:	// IDE-managed Components
        TRichEdit *Editor;
        TStatusBar *StatusBar1;
        TMainMenu *MainMenu1;
        TMenuItem *Save1;
        TMenuItem *cans1;
        TMenuItem *SaveColose1;
        TSaveDialog *SaveDialog1;
        void __fastcall Save1Click(TObject *Sender);
        void __fastcall cans1Click(TObject *Sender);
        void __fastcall EditorChange(TObject *Sender);
        void __fastcall SaveColose1Click(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall EditorSelectionChange(TObject *Sender);
private:	// User declarations
public:
        int ok;		// User declarations
        __fastcall Ttext_editor(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Ttext_editor *text_editor;

#endif
class Ttext_dlg
{
#ifndef __MAKECINT__
 Ttext_editor* edit;
#endif
public:
        Ttext_dlg();
        ~Ttext_dlg();
};
void ed(double* d,int n,char *format=NULL);
void ed(double* d,int n,int n1,char *format=NULL);
void ed(float* d,int n,char *format=NULL);
void ed(float* d,int n,int n1,char *format=NULL);
void ed(int* d,int n,char *format=NULL);
void ed(int* d,int n,int n1,char *format=NULL);
void ed(short* d,int n,char *format=NULL);
void ed(short* d,int n,int n1,char *format=NULL);
/*
void ed(vector<double>& d,char *format=NULL);
void ed(vector<float>& d,char *format=NULL);
void ed(vector<int>& d,char *format=NULL);
*/
void ed(char* d,int nx=0);

//---------------------------------------------------------------------------
#endif
