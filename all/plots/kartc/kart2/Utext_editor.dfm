object text_editor: Ttext_editor
  Left = 192
  Top = 125
  Caption = 'text_editor'
  ClientHeight = 505
  ClientWidth = 315
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Editor: TRichEdit
    Left = 0
    Top = 0
    Width = 315
    Height = 490
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Courier'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
    OnChange = EditorChange
    OnSelectionChange = EditorSelectionChange
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 490
    Width = 315
    Height = 15
    Panels = <>
    ExplicitTop = 470
  end
  object MainMenu1: TMainMenu
    Left = 72
    Top = 48
    object Save1: TMenuItem
      Caption = 'Save'
      OnClick = Save1Click
    end
    object cans1: TMenuItem
      Caption = 'Cans'
      OnClick = cans1Click
    end
    object SaveColose1: TMenuItem
      Caption = 'Save to &file'
      OnClick = SaveColose1Click
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 160
    Top = 40
  end
end
