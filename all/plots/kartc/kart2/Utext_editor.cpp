//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Utext_editor.h"
#include <strstrea.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Ttext_editor *text_editor;
//---------------------------------------------------------------------------
__fastcall Ttext_editor::Ttext_editor(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
Ttext_dlg::Ttext_dlg()
{
 edit=new Ttext_editor(NULL);
}


Ttext_dlg::~Ttext_dlg()
{
 delete edit;
}

void ed(double* d,int n,char *format)
{
 Ttext_editor* edit=new Ttext_editor(NULL);
 AnsiString s;
 char *fmt,fmtdft[]="%g";
 if(!format){fmt=fmtdft;}
 else{fmt=format;}
 int i;
 for(i=0;i<n;i++)
 {
  s.printf(fmt,d[i]);
  edit->Editor->Lines->Append(s);
 }
 edit->ShowModal();
 if(edit->ok)
 {
 s=edit->Editor->Text;
 istrstream si((char*)s.data(),s.Length());
 for(i=0;i<n;i++)
 {
  si>>d[i];
 }
 }
 delete edit;
}

void ed(double* d,int n,int n1,char *format)
{
 Ttext_editor* edit=new Ttext_editor(NULL);
 AnsiString s;
 char *fmt,fmtdft[]="%g ";
 if(!format){fmt=fmtdft;}
 else{fmt=format;}
 int i;
 for(i=0;i<n;i++)
 {
  if(i%n1==0&&i!=0)
  {
   edit->Editor->Lines->Append(s);
   s="";
  }
  s.cat_printf(fmt,d[i]);
 }
 edit->Editor->Lines->Append(s);
 edit->ShowModal();
 if(edit->ok)
 {
 s=edit->Editor->Text;
 istrstream si((char*)s.data(),s.Length());
 for(i=0;i<n;i++)
 {
  si>>d[i];
 }
 }
 delete edit;
}

void ed(vector<double>& d,char *format)
{
 ed(&d[0],d.size(),format);
}

void ed(float* d,int n,char *format)
{
 Ttext_editor* edit=new Ttext_editor(NULL);
 AnsiString s;
 char *fmt,fmtdft[]="%g";
 if(!format){fmt=fmtdft;}
 else{fmt=format;}
 int i;
 for(i=0;i<n;i++)
 {
  s.printf(fmt,d[i]);
  edit->Editor->Lines->Append(s);
 }
 edit->ShowModal();
 if(edit->ok)
 {
 s=edit->Editor->Text;
 istrstream si((char*)s.data(),s.Length());
 for(i=0;i<n;i++)
 {
  si>>d[i];
 }
 }
 delete edit;
}
void ed(float* d,int n,int n1,char *format)
{
 Ttext_editor* edit=new Ttext_editor(NULL);
 AnsiString s;
 char *fmt,fmtdft[]="%g ";
 if(!format){fmt=fmtdft;}
 else{fmt=format;}
 int i;
 for(i=0;i<n;i++)
 {
  if(i%n1==0&&i!=0)
  {
   edit->Editor->Lines->Append(s);
   s="";
  }
  s.cat_printf(fmt,d[i]);
 }
 edit->Editor->Lines->Append(s);
 edit->ShowModal();
 if(edit->ok)
 {
 s=edit->Editor->Text;
 istrstream si((char*)s.data(),s.Length());
 for(i=0;i<n;i++)
 {
  si>>d[i];
 }
 }
 delete edit;
}

void ed(vector<float>& d,char *format)
{
 ed(&d[0],d.size(),format);
}

void ed(int* d,int n,char *format)
{
 Ttext_editor* edit=new Ttext_editor(NULL);
 AnsiString s;
 char *fmt,fmtdft[]="%ld";
 if(!format){fmt=fmtdft;}
 else{fmt=format;}
 int i;
 for(i=0;i<n;i++)
 {
  s.printf(fmt,d[i]);
  edit->Editor->Lines->Append(s);
 }
 edit->ShowModal();
 if(edit->ok)
 {
 s=edit->Editor->Text;
 istrstream si((char*)s.data(),s.Length());
 for(i=0;i<n;i++)
 {
  si>>d[i];
 }
 }
 delete edit;
}
void ed(int* d,int n,int n1,char *format)
{
 Ttext_editor* edit=new Ttext_editor(NULL);
 AnsiString s;
 char *fmt,fmtdft[]="%ld ";
 if(!format){fmt=fmtdft;}
 else{fmt=format;}
 int i;
 for(i=0;i<n;i++)
 {
  if(i%n1==0&&i!=0)
  {
   edit->Editor->Lines->Append(s);
   s="";
  }
  s.cat_printf(fmt,d[i]);
 }
 edit->Editor->Lines->Append(s);
 edit->ShowModal();
 if(edit->ok)
 {
 s=edit->Editor->Text;
 istrstream si((char*)s.data(),s.Length());
 for(i=0;i<n;i++)
 {
  si>>d[i];
 }
 }
 delete edit;
}
void ed(vector<int>& d,char *format)
{
 ed(&d[0],d.size(),format);
}

void ed(short* d,int n,char *format)
{
 Ttext_editor* edit=new Ttext_editor(NULL);
 AnsiString s;
 char *fmt,fmtdft[]="%d";
 if(!format){fmt=fmtdft;}
 else{fmt=format;}
 int i;
 for(i=0;i<n;i++)
 {
  s.printf(fmt,d[i]);
  edit->Editor->Lines->Append(s);
 }
 edit->ShowModal();
 if(edit->ok)
 {
 s=edit->Editor->Text;
 istrstream si((char*)s.data(),s.Length());
 for(i=0;i<n;i++)
 {
  si>>d[i];
 }
 }
 delete edit;
}
void ed(short* d,int n,int n1,char *format)
{
 Ttext_editor* edit=new Ttext_editor(NULL);
 AnsiString s;
 char *fmt,fmtdft[]="%d ";
 if(!format){fmt=fmtdft;}
 else{fmt=format;}
 int i;
 for(i=0;i<n;i++)
 {
  if(i%n1==0&&i!=0)
  {
   edit->Editor->Lines->Append(s);
   s="";
  }
  s.cat_printf(fmt,d[i]);
 }
 edit->Editor->Lines->Append(s);
 edit->ShowModal();
 if(edit->ok)
 {
 s=edit->Editor->Text;
 istrstream si((char*)s.data(),s.Length());
 for(i=0;i<n;i++)
 {
  si>>d[i];
 }
 }
 delete edit;
}
void ed(vector<short>& d,char *format)
{
 ed(&d[0],d.size(),format);
}

void ed(char* d,int nx)
{
 int n,nr;
 if(!nx){n=strlen(d);}
 else{n=nx;}
 Ttext_editor* edit=new Ttext_editor(NULL);
 edit->Editor->Text=d;
 edit->ShowModal();
 if(edit->ok)
 {
 int nout,i,j;
 char* p;
 p=(char*)(edit->Editor->Text.data());
 nout=edit->Editor->Text.Length();
 for(i=0,j=0;i<n&&j<nout;j++)
 {
  if(p[j]==NULL)break;
  if(p[j]!='\r'){d[i++]=p[j];}
 }
 d[i]=0;
 }
 delete edit;
}

void __fastcall Ttext_editor::Save1Click(TObject *Sender)
{
 ok=1;
 Close();
}
//---------------------------------------------------------------------------

void __fastcall Ttext_editor::cans1Click(TObject *Sender)
{
 ok=0;
 Close();
}
//---------------------------------------------------------------------------


void __fastcall Ttext_editor::EditorChange(TObject *Sender)
{
 AnsiString s;
 TPoint p;
 p=Editor->CaretPos;
 s.printf("line=%ld,col=%ld",p.y,p.x);
 this->StatusBar1->SimpleText=s;
}
//---------------------------------------------------------------------------


void __fastcall Ttext_editor::SaveColose1Click(TObject *Sender)
{
 if(SaveDialog1->Execute())
 {
//  Editor->DefaultConverter
  ((TCustomMemo*)Editor)->Lines->SaveToFile(SaveDialog1->FileName);
 }
}
//---------------------------------------------------------------------------

void __fastcall Ttext_editor::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
 // �.� ������ ������� ESC
 if(Key==27)
 {
  this->Close();
 }
}
//---------------------------------------------------------------------------

void __fastcall Ttext_editor::EditorSelectionChange(TObject *Sender)
{
 AnsiString s;
 TPoint p;
 p=Editor->CaretPos;
 s.printf("line=%ld,col=%ld",p.y,p.x);
 this->StatusBar1->SimpleText=s;
}
//---------------------------------------------------------------------------



