object Form_kart3D: TForm_kart3D
  Left = 175
  Top = 182
  Caption = '1-'#1081' '#1101#1085#1077#1088#1075#1086#1073#1083#1086#1082' '#1050#1091#1088#1089#1082#1086#1081' '#1040#1069#1057' '#1088#1077#1072#1082#1090#1086#1088' '#1056#1041#1052#1050'-1000'
  ClientHeight = 635
  ClientWidth = 944
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 553
    Top = 0
    Width = 8
    Height = 616
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 616
    Width = 944
    Height = 19
    Panels = <
      item
        Text = #1058#1086#1095#1082#1072' '
        Width = 120
      end
      item
        Text = #1047#1085#1072#1095#1077#1085#1080#1077' = '
        Width = 180
      end
      item
        Text = #1047#1072#1075#1088#1091#1078#1077#1085' '#1092#1072#1081#1083' '
        Width = 50
      end>
  end
  object PlotsPanel: TPanel
    Left = 0
    Top = 0
    Width = 553
    Height = 616
    Align = alLeft
    BevelOuter = bvLowered
    BevelWidth = 2
    TabOrder = 1
    object Chart_top: TChart
      Left = 0
      Top = 232
      Width = 217
      Height = 233
      BackWall.Pen.Color = clYellow
      BackWall.Pen.Width = 2
      Border.Color = clYellow
      Border.Width = 2
      Legend.Visible = False
      MarginBottom = 0
      MarginLeft = 0
      MarginRight = 0
      MarginTop = 0
      Title.Text.Strings = (
        #1042#1080#1076' '#1089#1074#1077#1088#1093#1091)
      OnZoom = Chart_topZoom
      Frame.Color = clYellow
      Frame.Width = 2
      LeftAxis.AxisValuesFormat = '0'
      LeftAxis.ExactDateTime = False
      LeftAxis.Increment = 1.000000000000000000
      LeftAxis.LabelsFont.Gradient.Balance = 74
      LeftAxis.LabelsFont.Gradient.Direction = gdBottomTop
      LeftAxis.LabelsFont.Gradient.EndColor = clBlack
      LeftAxis.LabelsFont.Gradient.MidColor = 16744576
      LeftAxis.LabelsFont.Gradient.StartColor = clBlack
      LeftAxis.LabelsFont.InterCharSize = -1
      LeftAxis.LabelStyle = talValue
      View3D = False
      View3DWalls = False
      Align = alCustom
      Color = clWhite
      TabOrder = 0
      OnClick = Chart_topClick
      OnMouseUp = Chart_topMouseUp
      PrintMargins = (
        25
        15
        25
        15)
      object STop: TMapSeries
        HorizAxis = aBothHorizAxis
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Font.Color = clWhite
        Marks.Transparent = True
        Marks.Visible = False
        VertAxis = aBothVertAxis
        Pen.Color = clWhite
        Pen.Visible = False
        Shapes = <
          item
            Text = 'A'
            Z = 0.667000000000000000
          end
          item
            Text = 'B'
            Z = 0.714000000000000000
          end
          item
            Text = 'C'
            Z = 0.513000000000000000
          end
          item
            Text = 'D'
            Z = 0.366000000000000000
          end
          item
            Text = 'E'
            Z = 0.486000000000000000
          end
          item
            Text = 'F'
            Z = 0.367000000000000000
          end
          item
            Text = 'G'
            Z = 0.546000000000000000
          end
          item
            Text = 'H'
            Z = 0.307000000000000000
          end
          item
            Text = 'I'
            Z = 0.649000000000000000
          end
          item
            Text = 'J'
            Z = 0.404000000000000000
          end
          item
            Text = 'K'
            Z = 0.453000000000000000
          end
          item
            Text = 'L'
            Z = 0.552000000000000000
          end>
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        ZValues.Name = 'Z'
        ZValues.Order = loNone
      end
      object ChartTool_top: TCursorTool
        Series = STop
        OnChange = ChartTool_topChange
      end
    end
    object Chart_left: TChart
      Left = 216
      Top = 0
      Width = 217
      Height = 233
      Border.Color = clYellow
      Border.Width = 2
      Legend.Visible = False
      MarginBottom = 0
      MarginLeft = 0
      MarginRight = 0
      MarginTop = 0
      Title.Text.Strings = (
        #1042#1080#1076' '#1089#1083#1077#1074#1072)
      OnZoom = Chart_leftZoom
      BottomAxis.Inverted = True
      LeftAxis.Inverted = True
      RightAxis.Inverted = True
      View3D = False
      View3DWalls = False
      Align = alCustom
      Color = clWhite
      TabOrder = 1
      OnClick = Chart_leftClick
      OnMouseUp = Chart_leftMouseUp
      object SLeft: TMapSeries
        HorizAxis = aBothHorizAxis
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Font.Color = clWhite
        Marks.Transparent = True
        Marks.Visible = False
        VertAxis = aBothVertAxis
        Pen.Color = clWhite
        Pen.Visible = False
        Shapes = <
          item
            Text = 'A'
            Z = 0.440000000000000000
          end
          item
            Text = 'B'
            Z = 0.534000000000000000
          end
          item
            Text = 'C'
            Z = 0.602000000000000000
          end
          item
            Text = 'D'
            Z = 0.928000000000000000
          end
          item
            Text = 'E'
            Z = 0.734000000000000000
          end
          item
            Text = 'F'
            Z = 0.116000000000000000
          end
          item
            Text = 'G'
            Z = 0.316000000000000000
          end
          item
            Text = 'H'
            Z = 0.694000000000000000
          end
          item
            Text = 'I'
            Z = 0.386000000000000000
          end
          item
            Text = 'J'
            Z = 0.650000000000000000
          end
          item
            Text = 'K'
            Z = 0.221000000000000000
          end
          item
            Text = 'L'
            Z = 0.757000000000000000
          end>
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        ZValues.Name = 'Z'
        ZValues.Order = loNone
      end
      object ChartTool_left: TCursorTool
        Series = SLeft
        OnChange = ChartTool_leftChange
      end
    end
    object Chart_front: TChart
      Left = 0
      Top = 0
      Width = 217
      Height = 233
      Border.Color = clYellow
      Border.Width = 2
      Border.EndStyle = esSquare
      Legend.Visible = False
      MarginBottom = 0
      MarginLeft = 0
      MarginRight = 0
      MarginTop = 0
      Title.Text.Strings = (
        #1042#1080#1076' '#1089#1087#1077#1088#1077#1076#1080)
      OnZoom = Chart_frontZoom
      LeftAxis.Inverted = True
      RightAxis.Inverted = True
      View3D = False
      View3DWalls = False
      Align = alCustom
      Color = clWhite
      TabOrder = 2
      OnClick = Chart_frontClick
      OnMouseUp = Chart_frontMouseUp
      object SFront: TMapSeries
        HorizAxis = aBothHorizAxis
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Font.Color = clWhite
        Marks.Transparent = True
        Marks.Visible = False
        VertAxis = aBothVertAxis
        Pen.Color = clSilver
        Pen.Visible = False
        Shapes = <
          item
            Text = 'A'
            Z = 0.563999999999999900
          end
          item
            Text = 'B'
            Z = 0.790000000000000000
          end
          item
            Text = 'C'
            Z = 0.699000000000000000
          end
          item
            Text = 'D'
            Z = 0.173000000000000000
          end
          item
            Text = 'E'
            Z = 0.634000000000000000
          end
          item
            Text = 'F'
            Z = 0.756000000000000000
          end
          item
            Text = 'G'
            Z = 0.646000000000000000
          end
          item
            Text = 'H'
            Z = 0.609000000000000000
          end
          item
            Text = 'I'
            Z = 0.867000000000000000
          end
          item
            Text = 'J'
            Z = 0.995000000000000000
          end
          item
            Text = 'K'
            Z = 0.249000000000000000
          end
          item
            Text = 'L'
            Z = 0.842000000000000000
          end>
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        ZValues.Name = 'Z'
        ZValues.Order = loNone
      end
      object ChartTool_front: TCursorTool
        Series = SFront
        OnChange = ChartTool_frontChange
      end
    end
    object Chart_add: TChart
      Left = 224
      Top = 240
      Width = 217
      Height = 201
      Legend.Title.Text.Strings = (
        #1052#1086#1097#1085#1086#1089#1090#1100)
      Legend.Visible = False
      Title.Text.Strings = (
        #1087#1072#1088#1072#1084#1077#1090#1088
        '')
      BottomAxis.Title.Caption = #1074#1088#1077#1084#1103
      View3D = False
      TabOrder = 3
      object Series_power: TLineSeries
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Visible = False
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
    end
  end
  object Panel2: TPanel
    Left = 561
    Top = 0
    Width = 383
    Height = 616
    Align = alClient
    BevelWidth = 2
    Caption = 'Panel2'
    TabOrder = 2
    OnResize = Panel2Resize
    object Splitter2: TSplitter
      Left = 2
      Top = 233
      Width = 379
      Height = 16
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 449
    end
    object Panel_pal: TPanel
      Left = 2
      Top = 2
      Width = 379
      Height = 127
      Align = alTop
      TabOrder = 0
      DesignSize = (
        379
        127)
      object Chart4: TChart
        Left = 1
        Top = 1
        Width = 377
        Height = 47
        Legend.Visible = False
        MarginBottom = 0
        MarginLeft = 0
        MarginRight = 0
        MarginTop = 0
        Title.Text.Strings = (
          #1055#1072#1083#1080#1090#1088#1072' '#1094#1074#1077#1090#1086#1074)
        AxisBehind = False
        BottomAxis.AxisValuesFormat = '0'
        BottomAxis.Visible = False
        CustomAxes = <
          item
            Horizontal = True
            OtherSide = False
          end>
        LeftAxis.LabelsExponent = True
        LeftAxis.Visible = False
        RightAxis.Visible = False
        TopAxis.Visible = False
        View3D = False
        View3DWalls = False
        Align = alTop
        TabOrder = 0
        object SPalitra: TColorGridSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Callout.Length = 0
          Marks.Visible = False
          NumXValues = 1
          NumZValues = 1
          TimesZOrder = 1
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loNone
          ZValues.Name = 'Z'
          ZValues.Order = loNone
        end
      end
      object Chart5: TChart
        Left = 1
        Top = 48
        Width = 377
        Height = 17
        Legend.Visible = False
        MarginBottom = 0
        MarginLeft = 0
        MarginRight = 0
        MarginTop = 0
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        AxisBehind = False
        BottomAxis.Axis.Visible = False
        BottomAxis.AxisValuesFormat = '#.# x10E-#'
        LeftAxis.LabelsExponent = True
        LeftAxis.Visible = False
        RightAxis.Visible = False
        TopAxis.Visible = False
        View3D = False
        View3DWalls = False
        Align = alTop
        TabOrder = 1
        object Series1: TPointSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          SeriesColor = 13948116
          ClickableLine = False
          Pointer.InflateMargins = True
          Pointer.Style = psNothing
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object CheckBox_auto: TCheckBox
        Left = 131
        Top = 64
        Width = 49
        Height = 33
        Anchors = [akTop, akRight]
        Caption = #1072#1074#1090#1086
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object Edit_auto_min: TEdit
        Left = 187
        Top = 72
        Width = 81
        Height = 21
        Anchors = [akTop, akRight]
        TabOrder = 3
        Text = '0'
      end
      object Edit_auto_max: TEdit
        Left = 275
        Top = 72
        Width = 89
        Height = 21
        Anchors = [akTop, akRight]
        TabOrder = 4
        Text = '1'
      end
      object Button_scale: TButton
        Left = 304
        Top = 104
        Width = 57
        Height = 17
        Caption = 'Set'
        TabOrder = 5
        OnClick = Button_scaleClick
      end
    end
    object Panel_drive: TPanel
      Left = 2
      Top = 129
      Width = 379
      Height = 104
      Align = alTop
      TabOrder = 1
      object RadioGroup_xyz: TRadioGroup
        Left = 4
        Top = 6
        Width = 113
        Height = 81
        Caption = 'xyz'
        ItemIndex = 2
        Items.Strings = (
          'x'
          'y'
          'z')
        TabOrder = 0
        OnClick = RadioGroup_xyzClick
      end
    end
    object StringGrid: TStringGrid
      Left = 2
      Top = 249
      Width = 379
      Height = 365
      Align = alClient
      ColCount = 2
      FixedCols = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing]
      ParentShowHint = False
      ScrollBars = ssVertical
      ShowHint = False
      TabOrder = 2
      OnSelectCell = StringGridSelectCell
      ColWidths = (
        64
        64)
    end
  end
  object MainMenu1: TMainMenu
    Left = 488
    Top = 48
    object N1: TMenuItem
      Caption = #1052#1077#1085#1102
      object N4: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083
      end
    end
    object N3: TMenuItem
      Caption = #1055#1072#1083#1080#1090#1088#1072
      object N5: TMenuItem
        Caption = #1062#1074#1077#1090#1085#1072#1103
      end
      object N6: TMenuItem
        Caption = #1052#1086#1085#1086#1090#1086#1085#1085#1072#1103
      end
      object N7: TMenuItem
        Caption = #1063#1077#1088#1085#1086'-'#1073#1077#1083#1072#1103
      end
    end
    object N2: TMenuItem
      Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
      object N8: TMenuItem
        Caption = #1042#1077#1088#1089#1080#1103
        OnClick = N8Click
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 488
    Top = 16
  end
end
