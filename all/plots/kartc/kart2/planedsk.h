#ifndef planedsk_h
#define planedsk_h
#include <stdlib.h>


class Tplane_dsk
{
public:
 Tplane_dsk();
 Tplane_dsk(int y1_,const int *b,const int * e){beg=NULL;set(0,y1_,b,e);}
 Tplane_dsk(int y1_,const int (*rg)[2]){beg=NULL;set(0,y1_,rg);}
 Tplane_dsk(int y0_,int y1_,const int (*rg)[2]){beg=NULL;set(y0_,y1_,rg);}
 void set(int y0_,int y1_,const int *begs,const int * ends);
 void set(int y0_,int y1_,const int (*rg)[2]);
 ~Tplane_dsk()
 {
  delete [] beg;
 }
 //! ������ � ��������� �������� ����
 int *beg,*end,*offset;
 //! ������ ����� ��������� ���������
 int ntot;
 //! ������� �������
 int y0,y1,x0,x1;
 //! ����������� ��������� ������ � ������� ��� ������� �������� ������
 int ofs(int y,int x) const;
};
class Tbound_dsk
{
public:
 int y;
 int x;
 int l;
 int dir;
};

//! �������� ������� � �� �������

class Tplane_dsk_wb:public Tplane_dsk
{
 void fill_bnd();
 int nbnd;
public:
 int bnd_type;
 Tplane_dsk_wb();
 Tplane_dsk_wb(int y1_,const int *b,const int * e,int bnd_type_=6):Tplane_dsk(y1_,b,e),bnd_type(bnd_type_){fill_bnd();}
 Tplane_dsk_wb(int y1_,const int (*rg)[2],int bnd_type_=6):Tplane_dsk(y1_,rg),bnd_type(bnd_type_){fill_bnd();}
 ~Tplane_dsk_wb(){delete [] bound;}
 Tbound_dsk* bound;
 int bound_size() const {return nbnd;}
};
 void short_to_long(const Tplane_dsk* pl,const int * s_k,int * lk);
 void long_to_short(const Tplane_dsk* pl,const int * s_k,int * lk);

class Tplane_d:public Tplane_dsk
{
public:
 Tplane_d(int y0_,int y1_,const int *b,const int * e);
 Tplane_d(int y0_,int y1_,const int (*rg)[2]);
 //! ���������� ������ � ������ �������� �� y
 int Beg(int i) const {return beg[i-y0];}
 int End(int i) const {return end[i-y0];}
 //! ����������� ��������� ������ � ������� ��� ������� �������� ������
 int ofs(int y,int x) const;
};

Tplane_d* wwer1000_geom();
Tplane_d* rbmk1884_geom();
Tplane_d* rbmk2488_geom();

#endif
