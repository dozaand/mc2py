//---------------------------------------------------------------------------

#ifndef UinterfH
#define UinterfH
//---------------------------------------------------------------------------
extern "C"
{
int __declspec(dllexport) Kart2Create(int nrow,const int* beg,const int* end);
void __declspec(dllexport) Kart2Show(int id);
void __declspec(dllexport) Kart2Hide(int id);
void __declspec(dllexport) Kart2SetCaption(int id,const char* caption);
void __declspec(dllexport) Kart2SetDataFullD(int id,double *set1,double* set2);
void __declspec(dllexport) Kart2SetDataFullF(int id,float *set1,float* set2);
void __declspec(dllexport) Skart1(int nrow,const int* beg,const int* end,int geom_type,const double* d1,const double* d2,const char* caption);
 void __declspec(dllexport) Kart3D(
  int nz,int nrow,const int* beg,const int* end,
  const double* d1,const double* d2,
  const char* caption);
}
#endif
