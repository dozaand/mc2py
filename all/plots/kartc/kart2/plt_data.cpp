//---------------------------------------------------------------------------


#pragma hdrstop

#include "plt_data.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#include <algorithm>
#include <include/err_msg.h>

void Tplt_data::set(int nz_,int nxy_,const double * d)
{
 int i;
 nz=nz_;
 nxy=nxy_;
 data.resize(nxy*nz);
 for(i=0;i<nxy*nz;i++)
 {
  data[i]=d[i];
 }
 def_mima();
}
void Tplt_data::set(int nz_,int nxy_,const float * d)
{
 int i;
 nz=nz_;
 nxy=nxy_;
 data.resize(nxy*nz);
 for(i=0;i<nxy*nz;i++)
 {
  data[i]=d[i];
 }
 def_mima();
}
void Tplt_data::load(const char* nm)
{
 ifstream s(nm);
 if(!s) throw Err_msg()<<"error open file "<<nm;
 data.clear();
 data.reserve(32*nxy);
 double d;
 for(;;)
 {
  s>>d;
  if(!s)break;
  data.push_back(d);
 }
 nz=data.size()/nxy;
 if(nz*nxy!=data.size())
 {
  throw Err_msg()<<"invalid size of data file (1884*nz) "<<nm;
 }
 def_mima();
}

void Tplt_data::def_mima()
{
 mi=*min_element(data.begin(),data.end());
 ma=*max_element(data.begin(),data.end());
}