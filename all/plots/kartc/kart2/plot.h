//---------------------------------------------------------------------------

#ifndef plotH
#define plotH
//---------------------------------------------------------------------------
#ifndef __MAKECINT__
#include "miniplotform.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Series.hpp>
#endif
#include <vector.h>
#ifdef __MAKECINT__
typedef void TChart;
typedef int TColor;
typedef void TChartAxis;
typedef void TChartSeries;
#endif
//! ������� ���������� ������� �� �������� ������ � ������� d
/*!
 \param d - ������ ������� ���� ����������
 \param n - ����� �������
*/
void sline(double* d,int n);
void sline(double* t,double* d,int n);
void sline(double** d,int ngraph,int npoint);
void sline(float* d,int n);
void sline(float *t,float* d,int n);
void sline(float** d,int ngraph,int npoint);
void sline(vector<double>& v);
void sline(vector<double>& t,vector<double>& v);
void sline(vector<float>& v);
void sline(vector<float>& t,vector<float>& v);

class Taxies_info
{
 TChartAxis * axi;
public:
 Taxies_info(TChartAxis * ax);
 double min();
 double max();
 void min(double v);
 void max(double v);
 void name(char* str);
 char* name();
 void set_logscale(int i=1);
 void set_grid(int i=1);
 void automatic(int i);
 void automin(int i);
 void automax(int i);
 void visible(int i);
};

class Tseries_info
{
public:
 TChartSeries *S;
 int np;
 Tseries_info();
 void name(char* str);
 char* name();
 void color(TColor c);
 void color(int r,int g,int b);
 void color(double r,double g,double b);
 void x1(int i=1);
 void y1(int i=1);
 void show_legend(int i=1);
 void npoint(int n);
 virtual void operator()(double * x,double * y,int n);
 virtual void operator()(double * y,int n);
 virtual void operator()(float * x,float * y,int n);
 virtual void operator()(float * y,int n);
 void clear();
        ~Tseries_info();
        void init(TChart* chart);
};

class Tbline:public Tseries_info
{
public:
 Tbline(TChart* chart);
};
class Tbpoint:public Tseries_info
{
public:
 Tbpoint(TChart* chart);
};
class Tbbar:public Tseries_info
{
public:
 Tbbar(TChart* chart);
};
class Tbarrow:public Tseries_info
{
public:
 Tbarrow(TChart* chart);
};
class Tbbubble:public Tseries_info
{
public:
 Tbbubble(TChart* chart);
};
class Tbshape:public Tseries_info
{
public:
 Tbshape(TChart* chart);
};

class Tbplot
{
#ifndef __MAKECINT__
  Tplotform* form;
  Tplotform* iniform();
#endif
public:
 Taxies_info axi_x,axi_y,axi_x1,axi_y1;
 double mx,my;
 Tbplot();
 ~Tbplot();
  void operator()();
  Tseries_info* addline();
  Tseries_info* addpoint();
  Tseries_info* addbar();
  Tseries_info* addbubble();
  Tseries_info* addarrow();
  Tseries_info* addshape();
  int count();
  Tseries_info& operator[](int);
  void clear();
        void save_wmf(char* nm);
        void save_bmp(char* nm);
        void save_emf(char* nm);
        void name(char*);
 void lines(double** d,int ngraph,int npoint);
 void lines(float** d,int ngraph,int npoint);
 void rect(double xmi,double ymi,double xma,double yma);
 void rect1(double xmi,double ymi,double xma,double yma);
 void rect_all();
private:
#ifndef __MAKECINT__
 vector<Tseries_info*> ser;
#endif
};
#endif
