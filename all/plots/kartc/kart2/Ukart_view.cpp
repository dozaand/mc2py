//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Ukart_view.h"
#include <algorithm>
#include <util/palette/upal.h>
#include <math.h>
#include <strstream>
#include <iomanip>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "TeeMapSeries"
#pragma link "TeeSurfa"
#pragma resource "*.dfm"
TForm_kart *Form_kart;

//---------------------------------------------------------------------------

double h=1;       //!< ��������� ������ ��� ���� 23.5
double R=h/sqrt(3);  //!< ������ ����������, � ������� ������ ������������
// ������������� ��� ������� ������� �������� ��� ����������� ��������������
double scx=2./h;
double scy=1./(h*cos(M_PI/6));

float x_hex[6]={0};
float y_hex[6]={0};
float x_kv[4]={0};
float y_kv[4]={0};
void init_polygone_arrays(double R);

//---------------------------------------------------------------------------
__fastcall TForm_kart::TForm_kart(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
//! ���������� ������� �������
void TForm_kart::add_data(int g6_,const Tplane_d* geom_, const double* x1, const double* x2)
{
 geom=geom_;
 geom_type=g6_;
 v1.resize(geom->ntot);
 v2.resize(geom->ntot);
 inf.resize(geom->ntot);
 int i;
 for(i=0;i<geom->ntot;i++)
 {
  v1[i]=x1[i];
  v2[i]=x2[i];
 }
 add_finish();
}
//! ���������� ������� �������
void TForm_kart::add_data(int g6_,const Tplane_d* geom_, const float* x1, const float* x2)
{
 geom=geom_;
 geom_type=g6_;
 v1.resize(geom->ntot);
 v2.resize(geom->ntot);
 inf.resize(geom->ntot);
 int i;
 for(i=0;i<geom->ntot;i++)
 {
  v1[i]=x1[i];
  v2[i]=x2[i];
 }
 add_finish();
}
//! ������ ��� ����� ������ ������
void TForm_kart::expand_color_range(const vector<double>& v)
{
 double mi,ma;
 mi=*min_element(v.begin(),v.end());
 ma=*max_element(v.begin(),v.end());
 color_min=min(color_min,mi);
 color_max=max(color_max,ma);
 AnsiString str;
 str.printf("%lg",color_min);
 Label_min->Caption=str;
 str.printf("%lg",color_max);
 Label_max->Caption=str;
}

void init_polygone_arrays(double R)
{
 int k=0;
 double fi,dfi;
 for(k=0,fi=0,dfi=M_PI/3;k<6;k++,fi+=dfi)
 {
  x_hex[k]=R*sin(fi);
  y_hex[k]=R*cos(fi);
 }
 for(k=0,fi=M_PI/4,dfi=M_PI/2;k<4;k++,fi+=dfi)
 {
  x_kv[k]=R*sin(fi);
  y_kv[k]=R*cos(fi);
 }
}
void TForm_kart::push_item(int yy, int xx, double v)
{
 int l=0,k;
 AnsiString str;
 l=Series1->Shapes->Count;
 Series1->Shapes->Add();
 if(geom_type==6)
 {
  for(k=0;k<6;k++)
  {
   Series1->Shapes->Polygon[l]->AddXY((xx+x_hex[k]-0.5*yy)*scx,(yy/scy+y_hex[k]));
  }
 }
 else
 {
  for(k=0;k<4;k++)
  {
   Series1->Shapes->Polygon[l]->AddXY(xx+x_kv[k],yy+y_kv[k]);
  }
 }
 Series1->Shapes->Polygon[l]->Color =pal_gbr.col(color_min,v,color_max);
 AnsiString s(Edit_fmt->Text.c_str());
 str.printf(s.c_str(),v);
 Series1->Shapes->Polygon[l]->Text=str;
 Series1->Shapes->Polygon[l]->Z=v;
// Series1->Shapes->Polygon[l]->DisplayName="Name";
}

void skart(const Tplane_d* geom,int geom_type,const double* d)
{
 TForm_kart * Form_kart;
 Application->CreateForm(__classid(TForm_kart), &Form_kart);
 Form_kart->add_data(geom_type,geom, d, d);
 Form_kart->ShowModal();
 delete Form_kart;
}
void skart(const Tplane_d* geom,int geom_type,const double* d1,const double* d2,const char* caption)
{
 TForm_kart * Form_kart;
 Application->CreateForm(__classid(TForm_kart), &Form_kart);
 if(caption!=NULL)
 {
  Form_kart->Caption=caption;
 }
 Form_kart->add_data(geom_type,geom, d1, d2);
 Form_kart->ShowModal();
 delete Form_kart;
}

void skart(const Tplane_d* geom,int geom_type,const float* d)
{
 TForm_kart * Form_kart;
 Application->CreateForm(__classid(TForm_kart), &Form_kart);
 Form_kart->add_data(geom_type,geom, d, d);
 Form_kart->ShowModal();
 delete Form_kart;
}
void skart(const Tplane_d* geom,int geom_type,const float* d1,const float* d2)
{
 TForm_kart * Form_kart;
 Application->CreateForm(__classid(TForm_kart), &Form_kart);
 Form_kart->add_data(geom_type,geom, d1, d2);
 Form_kart->ShowModal();
 delete Form_kart;
}

double TForm_kart::val(int i)
{
 switch(RadioGroup_showform->ItemIndex)
 {
  case 3:
  case 0: return v1[i];
  case 1: return v2[i];
  case 2: return v1[i]-v2[i];
 }
}

void TForm_kart::update_data()
{
 int i,n;
 double v;
 AnsiString str;
 vector<double>* vv=NULL;
 vector<double> delta;
 n=v1.size();

 switch(RadioGroup_showform->ItemIndex)
 {
  case 3:
  case 0: {vv=&v1;break;}
  case 1: {vv=&v2;break;}
  case 2:
  {
   delta.resize(n);
   for(i=0;i<n;i++)
   {
	delta[i]=v1[i]-v2[i];
   }
   vv=&delta;
   break;
  }
 }
 if(CheckBox_auto->Checked)
 {
  set_color_range(*vv);
 }
 for(i=0;i<n;i++)
 {
  Series1->Shapes->Polygon[i]->Z=v1[i];
  v=(*vv)[i];
  Series1->Shapes->Polygon[i]->Color =pal_gbr.col(color_min,v,color_max);
  AnsiString s(Edit_fmt->Text.c_str());
  str.printf(s.c_str(),v);
  Series1->Shapes->Polygon[i]->Text=str;
 }
 check_mark_visible();
}
void __fastcall TForm_kart::RadioGroup_showformClick(TObject *Sender)
{
 update_data();
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart::Series1Click(TChartSeries *Sender,
      int ValueIndex, TMouseButton Button, TShiftState Shift, int X, int Y)
{
 ostrstream str;
 int i=ValueIndex;
 if(i>=0&&i<v1.size())
 {
  if(CheckBox_oct->Checked)
  {
//   str<<"v1="<<v1[i]<<" v2="<<v2[i]<<" x="<<oct<<inf[i].x<<" y="<<inf[i].y<<ends;
   str<<"v1="<<v1[i]<<"    v2="<<v2[i]<<"   x="<<oct<<inf[i].x<<"   y="<<inf[i].y<<"  l="<<i<<ends;
  }
  else
  {
   str<<"v1="<<v1[i]<<"       v2="<<v2[i]<<"       v1-v2="<<v1[i]-v2[i]<<"   x="<<inf[i].x<<"  y="<<inf[i].y<<"   l="<<i<<ends;
  }
  StatusBar1->SimpleText=str.str();
 }
 else
 {
  StatusBar1->SimpleText="";
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart::CheckBox_autoClick(TObject *Sender)
{
 if(CheckBox_auto->Checked)
 {
  update_data();
 }
 else
 {
  color_min=Edit_min->Text.ToDouble();
  color_max=Edit_max->Text.ToDouble();
  update_data();
 }
}
//---------------------------------------------------------------------------


void TForm_kart::set_color_range(vector<double>& v)
{
 color_min=1e308;
 color_max=-1e308;
 expand_color_range(v);
}

void TForm_kart::add_finish()
{
 color_min=1e308;
 color_max=-1e308;
 expand_color_range(v1);
 expand_color_range(v2);
// ������������� ��������
 int x,y,l;
 Series1->Clear();
 init_polygone_arrays(R);
 for(y=geom->y0,l=0;y<geom->y1;y++)
 {
  for(x=geom->Beg(y);x<geom->End(y);x++,l++)
  {
   push_item(y,x,v1[l]);
   inf[l].x=x;
   inf[l].y=y;
  }
 }
 check_mark_visible();
}
void __fastcall TForm_kart::Edit_minKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
 if(Key==VK_RETURN)
 {
  CheckBox_autoClick(NULL);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart::Edit_maxKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
 if(Key==VK_RETURN)
 {
  CheckBox_autoClick(NULL);
 }
}
//---------------------------------------------------------------------------


void TForm_kart::check_mark_visible()
{
 int cellsize;
 cellsize=Chart1->BottomAxis->CalcSizeValue(h);
 if(Series1->Marks->Font->Size*n_letters>cellsize)
 {
  Series1->Marks->Visible=0;
 }
 else
 {
  Series1->Marks->Visible=1;
 }
}

void __fastcall TForm_kart::Chart1Zoom(TObject *Sender)
{
 Timer1->Enabled=1;
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart::Chart1Resize(TObject *Sender)
{
 Timer1->Enabled=1;
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart::Chart1UndoZoom(TObject *Sender)
{
 Timer1->Enabled=1;
}
//---------------------------------------------------------------------------



void __fastcall TForm_kart::Timer1Timer(TObject *Sender)
{
 Timer1->Enabled=0;
 update_data();
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart::Edit_l_numbKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
 if(Key==VK_RETURN)
 {
  n_letters=Edit_l_numb->Text.ToDouble();
  update_data();
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart::FormShow(TObject *Sender)
{
 n_letters=Edit_l_numb->Text.ToDouble();
}
//---------------------------------------------------------------------------

