//---------------------------------------------------------------------------

#include <vcl.h>
#include <vector>
#pragma hdrstop
#include "Unit_kart3d.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "TeePoin3"
#pragma link "TeeSurfa"
#pragma link "TeeMapSeries"
#pragma link "TeeDraw3D"
#pragma link "TeeEdit"
#pragma link "MyPoint"
#pragma link "CurvFitt"
#pragma link "StatChar"
#pragma link "TeeTools"
#pragma link "TeeMapSeries"
#pragma link "TeeSurfa"
#pragma link "TeeTools"
#pragma link "TeeMapSeries"
#pragma link "TeeSurfa"
#pragma link "TeeTools"
#pragma resource "*.dfm"
#include <include/err_msg.h>
#include <vector>
#include <algorithm>

using namespace std;

double pcs=0.5;

Tplane_d* geom1;

TForm_kart3D *Form1;
//---------------------------------------------------------------------------
//��������������� �������� ����� ������� �����������
__fastcall TForm_kart3D::TForm_kart3D(TComponent* Owner)
		: TForm(Owner)
{
 r(XX) = 24;
 r(YY) = 24;
 r(ZZ) = 0;
 pal=&pal_gbr;
 ind_front.reserve(50*32);
 ind_left.reserve(50*32);
 ind_top.reserve(3000);
 curr_lin=&z_lin;
}
//---------------------------------------------------------------------------
//�������� ����������� ���� ��� �������� �����
//������� �� ������� �������
//������� �� ���������� ������� (�������)
//������� �� �����-����� �������


//��������������� ���� �������

void __fastcall TForm_kart3D::Chart_topClick(TObject *Sender)
{
 if(Chart_top->Border->Visible == false)
 {Chart_top->Border->Visible = true;Chart_left->Border->Visible = false;Chart_front->Border->Visible = false;}
 else{Chart_top->Border->Visible = false;}
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::Chart_frontClick(TObject *Sender)
{
 if(Chart_front->Border->Visible == false)
 {Chart_top->Border->Visible = false;Chart_left->Border->Visible = false;Chart_front->Border->Visible = true;}
 else{Chart_front->Border->Visible = false;}
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::Chart_leftClick(TObject *Sender)
{
 if(Chart_left->Border->Visible == false)
 {Chart_top->Border->Visible = false;Chart_left->Border->Visible = true;Chart_front->Border->Visible = false;}
 else{Chart_left->Border->Visible = false;}
}
//---------------------------------------------------------------------------




void __fastcall TForm_kart3D::FormResize(TObject *Sender)
{
 int ps=ClientHeight;
 PlotsPanel->Width=ps;
 PlotsPanel->Height=ps;
 int dh=PlotsPanel->Height/2;
 int dw=PlotsPanel->Width/2;

 Chart_front->Height=dh;
 Chart_front->Width=dw;
 Chart_front->Top=0;
 Chart_front->Left=0;

 Chart_left->Height=dh;
 Chart_left->Width=dw;
 Chart_left->Top=0;
 Chart_left->Left=dw;

 Chart_top->Height=dh;
 Chart_top->Width=dw;
 Chart_top->Top=dh;
 Chart_top->Left=0;

 Chart_add->Height=dh;
 Chart_add->Width=dw;
 Chart_add->Top=dh;
 Chart_add->Left=dw;

 StringGrid->ColWidths[1]=StringGrid->Width;
}
//---------------------------------------------------------------------------
void TForm_kart3D::set_data(double * data, int nz_)
{
}
int change_r_loc=0;
void TForm_kart3D::set_krests(MatNM<int,3,1>& v)
{
// change_r_loc=1;
 ChartTool_front->XValue=v(XX);
 ChartTool_front->YValue=v(ZZ);
 ChartTool_left->XValue=v(YY);
 ChartTool_left->YValue=v(ZZ);
 ChartTool_top->XValue=v(XX);
 ChartTool_top->YValue=v(YY);
 r=v;
 AnsiString str;
 str.printf("x=%ld,y=%ld,z=%ld",v(XX),v(YY),v(ZZ));
 StatusBar1->Panels->Items[0]->Text=str;
}
enum {SG_KOORD,SG_NAME,SG_VAL,SG_ESTIAMTE};
AnsiString time_stamp;

//---------------------------------------------------------------------------
void __fastcall TForm_kart3D::FormCreate(TObject *Sender)
{
 geom=geom1;
 v_min=data.mi;
 v_max=data.ma;
 r(XX)=24;
 r(YY)=24;
 r(ZZ) = 8;
 r_old=r;
 SFront->XValues->Order=0;
 STop->XValues->Order=0;
 SLeft->XValues->Order=0;
 def_mima();
 fill_serieses();
 int len=100;
 StringGrid->ColWidths[0]=len;
 StringGrid->ColWidths[1]=StringGrid->Width;
 StringGrid->Cells[SG_KOORD][0]="�����";
 StringGrid->Cells[SG_NAME][0]="��������";
 mk_err_list();
}
//---------------------------------------------------------------------------
int rge(double v1,double v,double v2)
{
 return v1<=v&&v<=v2;
}

void TForm_kart3D::fill_serieses()
{
 int x,y,z,l;
 double d;
 AnsiString txt=" ";
 ind_front.clear();ind_top.clear();ind_left.clear();
 SFront->Clear();
 SLeft->Clear();
 STop->Clear();
 Series_power->Clear();
 x_lin.clear(),y_lin.clear(),z_lin.clear();
 if(CheckBox_auto->Checked)
 {
  v_min=data.mi;
  v_max=data.ma;
  AnsiString ss;
  ss.printf("%g",v_min);
  Edit_auto_min->Text=ss;
  ss.printf("%g",v_max);
  Edit_auto_max->Text=ss;
 }
 l=0;
   for(z=0;z<data.nz;z++)
   {
 for(y=geom->y0;y<geom->y1;y++)
 {
  for(x=geom->Beg(y);x<geom->End(y);x++)
  {
	if(z==r(ZZ))
	{
	 d=data[l];
	 add_poly(STop,x,y,pcs,pcs,pal->col(v_min,d,v_max),txt);
	 ind_top.push_back(l);
	 if(x==r(XX))
	 {
	  y_lin.push_back(d);
	 }
	 if(y==r(YY))
	 {
	  x_lin.push_back(d);
	 }
	}
	if(y==r(YY))
	{
	 d=data[l];
	 add_poly(SFront,x,z,pcs,pcs,pal->col(v_min,d,v_max),txt);
	 ind_front.push_back(l);
	 if(x==r(XX))
	 {
	  z_lin.push_back(d);
	 }
	}
	if(x==r(XX))
	{
	 add_poly(SLeft,y,z,pcs,pcs,pal->col(v_min,data[l],v_max),txt);
	 ind_left.push_back(l);
	}
	l++;
   }
  }
 }
 RadioGroup_xyzClick(NULL);

 Chart_top->BottomAxis->SetMinMax(r_min(XX)-1,r_max(XX)+1);
 Chart_top->LeftAxis->SetMinMax(r_min(YY)-1,r_max(YY)+1);
 Chart_front->BottomAxis->SetMinMax(r_min(XX)-1,r_max(XX)+1);
 Chart_front->LeftAxis->SetMinMax(r_min(ZZ)-1,r_max(ZZ)+1);
 Chart_left->BottomAxis->SetMinMax(r_min(XX)-1,r_max(XX)+1);
 Chart_left->LeftAxis->SetMinMax(r_min(ZZ)-1,r_max(ZZ)+1);
// chart_auto_on(Chart_top);
// chart_auto_off(Chart_top);
}
//---------------------------------------------------------------------------

//! ���������� ������ ��������������� � ����� map
void add_poly(TMapSeries* map,float x,float y,float dx,float dy,TColor col,const AnsiString& text)
{
 int i;
 float x1,x2,y1,y2;
 map->Shapes->Add();
 i=map->Shapes->Count-1;

 x1=x-dx;
 x2=x+dx;
 y1=y-dy;
 y2=y+dy;
// x1=x;x2=x+dx;
// y1=y;y2=y+dy;

 map->Shapes->Polygon[i]->AddXY(x1,y1);
 map->Shapes->Polygon[i]->AddXY(x1,y2);
 map->Shapes->Polygon[i]->AddXY(x2,y2);
 map->Shapes->Polygon[i]->AddXY(x2,y1);
 map->Shapes->Polygon[i]->Color =col;
 map->Shapes->Polygon[i]->Text =text;
}
//---------------------------------------------------------------------------

void chart_auto_off(TChart* Chart1)
{
 Chart1->BottomAxis->Automatic=0;
 Chart1->LeftAxis->Automatic=0;
}
void chart_auto_on(TChart* Chart1)
{
 Chart1->BottomAxis->Automatic=1;
 Chart1->LeftAxis->Automatic=1;
}


void TForm_kart3D::def_mima()
{
 r_min(XX)=geom->y0;
 r_max(XX)=geom->y1-1;
 r_min(YY)=geom->y0;
 r_max(YY)=geom->y1-1;
 r_min(ZZ)=0;
 r_max(ZZ)=data.nz-1;

}

MatNM<double,3,1> r_double;//!< ����� ���������� ������������� tool

void __fastcall TForm_kart3D::ChartTool_frontChange(TCursorTool *Sender, int x, int y,
	  const double XValue, const double YValue, TChartSeries *Series,
	  int ValueIndex)
{
 AnsiString str;
 if(!change_r_loc)
 {
  r(XX)=r_double(XX)=XValue;
// r(YY)=r_double(YY)=
  r(ZZ)=r_double(ZZ)=YValue;
 }
// ChartTool_front->XValue=r_double(XX);
// ChartTool_front->YValue=r_double(ZZ);
 ChartTool_left->XValue=r_double(YY);
 ChartTool_left->YValue=r_double(ZZ);
 ChartTool_top->XValue=r_double(XX);
 ChartTool_top->YValue=r_double(YY);

 str.printf("�������� = %lg",data[curr_ind()]);
 StatusBar1->Panels->Items[1]->Text=str;

 str.printf("x=%ld,y=%ld,z=%ld",r(XX),r(YY),r(ZZ));
 StatusBar1->Panels->Items[0]->Text=str;
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::ChartTool_leftChange(TCursorTool *Sender, int x, int y,
	  const double XValue, const double YValue, TChartSeries *Series,
	  int ValueIndex)
{
 AnsiString str;
// r(XX)=r_double(XX)=
 if(!change_r_loc)
 {
  r(YY)=r_double(YY)=XValue;
  r(ZZ)=r_double(ZZ)=YValue;
 }
 ChartTool_front->XValue=r_double(XX);
 ChartTool_front->YValue=r_double(ZZ);
// ChartTool_left->XValue=r_double(YY);
// ChartTool_left->YValue=r_double(ZZ);
 ChartTool_top->XValue=r_double(XX);
 ChartTool_top->YValue=r_double(YY);
 str.printf("�������� = %lg",data[curr_ind()]);
 StatusBar1->Panels->Items[1]->Text=str;
// check_and_fill();

 str.printf("x=%ld,y=%ld,z=%ld",r(XX),r(YY),r(ZZ));
 StatusBar1->Panels->Items[0]->Text=str;
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::ChartTool_topChange(TCursorTool *Sender, int x, int y,
	  const double XValue, const double YValue, TChartSeries *Series,
	  int ValueIndex)
{
 AnsiString str;
 if(!change_r_loc)
 {
  r(XX)=r_double(XX)=XValue;
  r(YY)=r_double(YY)=YValue;
 }
// r(ZZ)=r_double(ZZ)=
 ChartTool_front->XValue=r_double(XX);
 ChartTool_front->YValue=r_double(ZZ);
 ChartTool_left->XValue=r_double(YY);
 ChartTool_left->YValue=r_double(ZZ);
// ChartTool_top->XValue=r_double(XX);
// ChartTool_top->YValue=r_double(YY);
 str.printf("�������� = %lg",data[curr_ind()]);
 StatusBar1->Panels->Items[1]->Text=str;
// check_and_fill();

 str.printf("x=%ld,y=%ld,z=%ld",r(XX),r(YY),r(ZZ));
 StatusBar1->Panels->Items[0]->Text=str;
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::FormShow(TObject *Sender)
{
 MatNM<int,3,1> v={24,24,data.nz/2};
 set_krests(v);
}
//---------------------------------------------------------------------------
int iii=0;


int TForm_kart3D::curr_ind()
{
 return geom->ofs(r(YY),r(XX))*data.nz+r(ZZ);
}

void TForm_kart3D::check_and_fill()
{
 if(r(XX)!=r_old(XX)||r(YY)!=r_old(YY)||r(XX)!=r_old(YY))
 {
  r_old=r;
  fill_serieses();
 }
}
void __fastcall TForm_kart3D::N8Click(TObject *Sender)
{
 fill_serieses();
}
//---------------------------------------------------------------------------




void __fastcall TForm_kart3D::Chart_topMouseUp(TObject *Sender,
	  TMouseButton Button, TShiftState Shift, int X, int Y)
{
 check_and_fill();
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::Chart_frontMouseUp(TObject *Sender,
	  TMouseButton Button, TShiftState Shift, int X, int Y)
{
 check_and_fill();
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::Chart_leftMouseUp(TObject *Sender,
	  TMouseButton Button, TShiftState Shift, int X, int Y)
{
 check_and_fill();
}
//---------------------------------------------------------------------------
class Terr
{
public:
 Terr(){}
 Terr(AnsiString koord_,AnsiString name_,double v1,double v2):
 koord(koord_),
 name(name_),
 val(v1),
 est(v2)
 {
 }
 AnsiString koord;
 AnsiString name;
 double val;
 double est;
};

AnsiString ptd(double d){AnsiString s; s.printf("%lg",d);return s;}

void TForm_kart3D::mk_err_list()
{
 int i;
 Tposition_info pi;
 AnsiString s;
 StringGrid->RowCount=positions.size()+1;
 for(i=0;i<positions.size();i++)
 {
  pi=positions[i];
  s.printf("x=%ld,y=%ld,z=%ld",pi.x,pi.y,pi.z);
  StringGrid->Cells[SG_KOORD][i+1]=s;
  s=pi.dsk;
  StringGrid->Cells[SG_NAME][i+1]=s;
 }
}
// ����������� ������
void TForm_kart3D::update_series_colors()
{
 recolor(STop,ind_top);
 recolor(SFront,ind_front);
 recolor(SLeft,ind_left);
}
//---------------------------------------------------------------------------
void TForm_kart3D::recolor(TMapSeries* ser, vector<int>& ind)
{
 unsigned i;
 for(i=0;i<ind.size();i++)
 {
  ser->Shapes->Polygon[i]->Color=pal->col(v_min,data[ind[i]],v_max);
 }
}
// get data
enum {RGO_RESID,RGO_W,RGO_Wv,RGO_SUZ,RGO_DPZ,RGO_DNFR,RGO_DVOSST,RGO_DR};
void TForm_kart3D::ds(int nz,const float * dat_in)
{
 data.set(nz,geom->ntot,dat_in);
}


void __fastcall TForm_kart3D::Button_scaleClick(TObject *Sender)
{
 if(CheckBox_auto->Checked==0)
 {
  v_min=Edit_auto_min->Text.ToDouble();
  v_max=Edit_auto_max->Text.ToDouble();
 }
 else
 {
  v_min=data.mi;
  v_max=data.ma;
  AnsiString ss;
  ss.printf("%g",v_min);
  Edit_auto_min->Text=ss;
  ss.printf("%g",v_max);
  Edit_auto_max->Text=ss;
 }
 update_series_colors();
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::RadioGroup_xyzClick(TObject *Sender)
{
 int i,n;
 switch(RadioGroup_xyz->ItemIndex)
 {
  case 0: {curr_lin=&x_lin;Chart_add->BottomAxis->Title->Caption="x"; break;}
  case 1: {curr_lin=&y_lin;Chart_add->BottomAxis->Title->Caption="y";break;}
  case 2: {curr_lin=&z_lin;Chart_add->BottomAxis->Title->Caption="z";break;}
  default: {throw Err_msg()<<"invalid chois of direction";}
 }
 n=curr_lin->size();
 Series_power->Clear();
 for(i=0;i<n;i++)
 {
  Series_power->AddXY(i,(*curr_lin)[i]);
 }
}
//---------------------------------------------------------------------------
void chartrange(TChart* chart,MatNM<double,2,1>& rx,MatNM<double,2,1>& ry,double& ma)
{
 rx(0)=chart->BottomAxis->Minimum;
 rx(1)=chart->BottomAxis->Maximum;
 ry(0)=chart->LeftAxis->Minimum;
 ry(1)=chart->LeftAxis->Maximum;
 ma=max(ry(1)-ry(0),rx(1)-rx(0));
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::Chart_frontZoom(TObject *Sender)
{
// xz
/*  MatNM<double,2,1> rx,ry,ma;
  chartrange((TChart*)Sender,rx,ry,ma);
  r_min(XX)=rx(0);
  r_min(ZZ)=ry(0);
  set_kubik_size(ma);*/
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::Chart_leftZoom(TObject *Sender)
{
// yz
/*  MatNM<double,2,1> rx,ry;
  chartrange((Tchart*)Sender,rx,ry,ma);
  r_min(XX)=rx(0);
  r_min(ZZ)=ry(0);
  set_kubik_size(ma);*/
}
//---------------------------------------------------------------------------

void __fastcall TForm_kart3D::Chart_topZoom(TObject *Sender)
{
// xy
/*  MatNM<double,2,1> rx,ry;
  chartrange((Tchart*)Sender,rx,ry,ma);
  r_min(XX)=rx(0);
  r_min(ZZ)=ry(0);
  set_kubik_size(ma);*/
}

void TForm_kart3D::set_kubik_size(double ma)
{
 r_max(XX)=r_min(XX)+ma;
 r_max(YY)=r_min(YY)+ma;
 r_max(ZZ)=r_min(ZZ)+ma;
}


void __fastcall TForm_kart3D::StringGridSelectCell(TObject *Sender, int ACol,
	  int ARow, bool &CanSelect)
{
 Tposition_info pi=positions[ARow-1];
 MatNM<int,3,1> v;
 v(XX)=pi.x;
 v(YY)=pi.y;
 v(ZZ)=pi.z;
 set_krests(v);
 check_and_fill();
}
//---------------------------------------------------------------------------


extern "C"
{
// ��������� ������ �������� (����������� �� ������������)
 void __declspec(dllexport) Kart3D(
  int nz,int y0,int y1,const int* beg,const int* end,
  const double* d1,const wchar_t* caption,int npos,Tposition_info* pos)
 {
 TForm_kart3D * Form_kart3D;

 Tplane_d geom(y0,y1,beg,end);
 geom1=&geom;
 Application->CreateForm(__classid(TForm_kart3D), &Form_kart3D);
 if(caption!=NULL)
 {
  Form_kart3D->Caption=caption;
 }
 Form_kart3D->data.set(nz,geom.ntot,d1);
 Form_kart3D->def_mima();
 Form_kart3D->positions.clear();
 for(int i=0;i<npos;++i)
 {
  Form_kart3D->positions.push_back(pos[i]);
 }
 Form_kart3D->mk_err_list();
 Form_kart3D->fill_serieses();
 Form_kart3D->ShowModal();
 delete Form_kart3D;
 }
}



void __fastcall TForm_kart3D::Panel2Resize(TObject *Sender)
{
 StringGrid->ColWidths[1]=StringGrid->Width;
}
//---------------------------------------------------------------------------

