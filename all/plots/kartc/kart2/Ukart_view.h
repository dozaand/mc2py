//---------------------------------------------------------------------------

#ifndef Ukart_viewH
#define Ukart_viewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TeeMapSeries.hpp"
#include "TeeSurfa.hpp"
#include <Chart.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <util/planedsk/planedsk.h>
#include <vector>

using namespace std;

class Tvinfo
{
public:
 int x,y;
};

//---------------------------------------------------------------------------
class TForm_kart : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar1;
        TChart *Chart1;
        TMapSeries *Series1;
        TPanel *Panel1;
        TChart *Chart2;
        TMapSeries *Series2;
        TPanel *Panel2;
        TSplitter *Splitter1;
        TRadioGroup *RadioGroup_showform;
        TEdit *Edit_min;
        TEdit *Edit_max;
        TCheckBox *CheckBox_auto;
        TCheckBox *CheckBox_oct;
        TLabel *Label_min;
        TLabel *Label_max;
        TEdit *Edit_fmt;
        TLabel *Label1;
        TTimer *Timer1;
        TEdit *Edit_l_numb;
        void __fastcall RadioGroup_showformClick(TObject *Sender);
        void __fastcall Series1Click(TChartSeries *Sender, int ValueIndex,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall CheckBox_autoClick(TObject *Sender);
        void __fastcall Edit_minKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall Edit_maxKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall Chart1Zoom(TObject *Sender);
        void __fastcall Chart1Resize(TObject *Sender);
        void __fastcall Chart1UndoZoom(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall Edit_l_numbKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
        const Tplane_d* geom;
        int geom_type;
        double color_min,color_max;
        vector<double> v1,v2;
        vector<Tvinfo> inf;
        double n_letters;
public:		// User declarations
        __fastcall TForm_kart(TComponent* Owner);
        void add_data(int g6_,const Tplane_d* geom_, const double* x1, const double* x2);
        void add_data(int g6_,const Tplane_d* geom_, const float* x1, const float* x2);
        void expand_color_range(const vector<double>& v);
        void push_item(int yy, int xx, double val);
        double val(int i);
        void update_data();
        void set_color_range(vector<double>& v);
        void add_finish();
        void check_mark_visible();
};
//---------------------------------------------------------------------------
extern PACKAGE TForm_kart *Form_kart;
//---------------------------------------------------------------------------
#endif
