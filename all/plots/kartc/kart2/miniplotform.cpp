//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "miniplotform.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tplotform *plotform;
//---------------------------------------------------------------------------
__fastcall Tplotform::Tplotform(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tplotform::ChartClick(TObject *Sender)
{
 if(Chart->SeriesCount())
 {
  AnsiString s;
  double x,y;
  TChartSeries * ser;
  ser=Chart->Series[0];
  ser->GetCursorValues(x,y);
  s.printf("x=%g y=%g",x,y);
  StatusBar2->SimpleText=s;
  mouse_x=x;
  mouse_y=y;
 }
}
//---------------------------------------------------------------------------

