//---------------------------------------------------------------------------

#ifndef miniplotformH
#define miniplotformH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Series.hpp>
//---------------------------------------------------------------------------
class Tplotform : public TForm
{
__published:	// IDE-managed Components
        TChart *Chart;
        TStatusBar *StatusBar2;
        void __fastcall ChartClick(TObject *Sender);
private:	// User declarations
public:
        double mouse_x;
        double mouse_y;		// User declarations
        __fastcall Tplotform(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tplotform *plotform;
//---------------------------------------------------------------------------
#endif
