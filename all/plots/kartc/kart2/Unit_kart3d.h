//---------------------------------------------------------------------------

#ifndef Unit_kart3dH
#define Unit_kart3dH
#include "plt_data.h"
#include "TeeMapSeries.hpp"
#include "TeeSurfa.hpp"
#include "TeeTools.hpp"
#include <Chart.hpp>
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include "TeeMapSeries.hpp"
#include "TeeSurfa.hpp"
#include "TeeTools.hpp"
#include <StdCtrls.hpp>
#include <Grids.hpp>
#include <TeeShape.hpp>
//---------------------------------------------------------------------------
#include <include/TMATR.H>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "TeeDraw3D.hpp"
#include "TeeEdit.hpp"
#include "MyPoint.hpp"
#include <BubbleCh.hpp>
#include <GanttCh.hpp>
#include "CurvFitt.hpp"
#include "StatChar.hpp"
#include <TeeFunci.hpp>
#include "TeeTools.hpp"
#include "react_const.h"
#include "Upal.h"
#include "UTIL/planedsk/planedsk.h"
//---------------------------------------------------------------------------
using namespace std;

class Tposition_info
{
public:
 int x,y,z;
 char dsk[50];
};

class TForm_kart3D : public TForm
{
__published:	// IDE-managed Components
		TChart *Chart_top;
		TStatusBar *StatusBar1;
		TMainMenu *MainMenu1;
		TMenuItem *N1;
		TMenuItem *N2;
		TMenuItem *N3;
		TChart *Chart_left;
		TChart *Chart_front;
		TMenuItem *N4;
		TOpenDialog *OpenDialog1;
		TMapSeries *STop;
		TMapSeries *SLeft;
		TMapSeries *SFront;
		TChart *Chart4;
		TColorGridSeries *SPalitra;
		TChart *Chart5;
		TPointSeries *Series1;
		TMenuItem *N5;
		TMenuItem *N6;
		TMenuItem *N7;
		TMenuItem *N8;
		TPanel *PlotsPanel;
		TSplitter *Splitter1;
		TPanel *Panel2;
		TCursorTool *ChartTool_front;
		TCursorTool *ChartTool_left;
		TCursorTool *ChartTool_top;
		TPanel *Panel_pal;
		TCheckBox *CheckBox_auto;
		TEdit *Edit_auto_min;
		TEdit *Edit_auto_max;
		TPanel *Panel_drive;
		TSplitter *Splitter2;
		TStringGrid *StringGrid;
		TChart *Chart_add;
		TLineSeries *Series_power;
		TButton *Button_scale;
		TRadioGroup *RadioGroup_xyz;
		void __fastcall Chart_topClick(TObject *Sender);
		void __fastcall Chart_frontClick(TObject *Sender);
		void __fastcall Chart_leftClick(TObject *Sender);
		void __fastcall FormResize(TObject *Sender);
		void __fastcall FormCreate(TObject *Sender);
		void __fastcall ChartTool_frontChange(TCursorTool *Sender, int x, int y,
		  const double XValue, const double YValue, TChartSeries *Series,
		  int ValueIndex);
		void __fastcall ChartTool_leftChange(TCursorTool *Sender, int x, int y,
		  const double XValue, const double YValue, TChartSeries *Series,
		  int ValueIndex);
		void __fastcall ChartTool_topChange(TCursorTool *Sender, int x, int y,
		  const double XValue, const double YValue, TChartSeries *Series,
		  int ValueIndex);
		void __fastcall FormShow(TObject *Sender);
		void __fastcall N8Click(TObject *Sender);
		void __fastcall Chart_topMouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y);
		void __fastcall Chart_frontMouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y);
		void __fastcall Chart_leftMouseUp(TObject *Sender,
		  TMouseButton Button, TShiftState Shift, int X, int Y);
		void __fastcall Button_scaleClick(TObject *Sender);
		void __fastcall RadioGroup_xyzClick(TObject *Sender);
		void __fastcall Chart_frontZoom(TObject *Sender);
		void __fastcall Chart_leftZoom(TObject *Sender);
		void __fastcall Chart_topZoom(TObject *Sender);
		void __fastcall StringGridSelectCell(TObject *Sender, int ACol,
		  int ARow, bool &CanSelect);
	void __fastcall Panel2Resize(TObject *Sender);
private:	// User declarations
public:		// User declarations
		MatNM<int,3,1> r,r_min,r_max;
		MatNM<int,3,1> r_old;//!< ����� ��������� ������� �������������
		vector<int> ind_front,ind_top,ind_left;//!<ind_front[j]- ������ � data ���� j ����� � �����
		vector<float> x_lin,y_lin,z_lin;
		vector<float>* curr_lin;
		vector<Tposition_info> positions;
		double v_min,v_max;
		Tvectpal* pal;
		Tplt_data data;
		int r_current;
		__fastcall TForm_kart3D(TComponent* Owner);
		void set_data(double * data, int nz);
		void set_krests(MatNM<int,3,1>& v);
		void fill_serieses();
		void def_mima();
		int curr_ind();
		void check_and_fill();
		void mk_err_list();
		void update_series_colors();
		void recolor(TMapSeries* ser, vector<int>& ind);
		void set_kubik_size(double ma);
		void krest_to_range();
		void ds(int nz,const float * data);
		void update_kart();
        Tplane_d* geom;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm_kart3D *Form_kart3D;
//---------------------------------------------------------------------------
void add_poly(TMapSeries* map,float x,float y,float dx,float dy,TColor col,const AnsiString& text);
void chart_auto_off(TChart* Chart1);
void chart_auto_on(TChart* Chart1);
#endif
