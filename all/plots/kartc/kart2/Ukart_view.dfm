object Form_kart: TForm_kart
  Left = 279
  Top = 117
  Caption = 'Form_kart'
  ClientHeight = 548
  ClientWidth = 748
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 553
    Top = 0
    Width = 9
    Height = 528
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 528
    Width = 748
    Height = 20
    Panels = <>
    SimplePanel = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 553
    Height = 528
    Align = alLeft
    TabOrder = 1
    object Chart1: TChart
      Left = 1
      Top = 1
      Width = 511
      Height = 526
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      OnUndoZoom = Chart1UndoZoom
      OnZoom = Chart1Zoom
      View3D = False
      Align = alClient
      TabOrder = 0
      OnResize = Chart1Resize
      object Series1: TMapSeries
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.BackColor = 4194368
        Marks.Clip = True
        Marks.Color = 4194368
        Marks.MultiLine = True
        Marks.ShapeStyle = fosRoundRectangle
        Marks.Transparent = True
        Marks.Visible = True
        ShowInLegend = False
        Shapes = <
          item
            Text = 'A'
            Z = 0.286000000000000000
          end
          item
            Text = 'B'
            Z = 0.453000000000000000
          end
          item
            Text = 'C'
            Z = 0.751000000000000000
          end
          item
            Text = 'D'
            Z = 0.711000000000000000
          end
          item
            Text = 'E'
            Z = 0.903000000000000000
          end
          item
            Text = 'F'
            Z = 0.251000000000000000
          end
          item
            Text = 'G'
            Z = 0.376000000000000000
          end
          item
            Text = 'H'
            Z = 0.351000000000000000
          end
          item
            Text = 'I'
            Z = 0.298000000000000000
          end
          item
            Text = 'J'
            Z = 0.396000000000000000
          end
          item
            Text = 'K'
            Z = 0.843000000000000000
          end
          item
            Text = 'L'
            Z = 0.862000000000000000
          end>
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        ZValues.Name = 'Z'
        ZValues.Order = loNone
        OnClick = Series1Click
      end
    end
    object Chart2: TChart
      Left = 512
      Top = 1
      Width = 40
      Height = 526
      Legend.Visible = False
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      AxisVisible = False
      View3D = False
      View3DWalls = False
      Align = alRight
      TabOrder = 1
      object Series2: TMapSeries
        Marks.Arrow.Visible = True
        Marks.Callout.Brush.Color = clBlack
        Marks.Callout.Arrow.Visible = True
        Marks.Visible = False
        Shapes = <
          item
            Text = 'A'
            Z = 0.842000000000000000
          end
          item
            Text = 'B'
            Z = 0.764000000000000000
          end
          item
            Text = 'C'
            Z = 0.938999999999999900
          end
          item
            Text = 'D'
            Z = 0.948000000000000000
          end
          item
            Text = 'E'
            Z = 0.485000000000000000
          end
          item
            Text = 'F'
            Z = 0.001000000000000000
          end
          item
            Text = 'G'
            Z = 0.828000000000000000
          end
          item
            Text = 'H'
            Z = 0.885000000000000000
          end
          item
            Text = 'I'
            Z = 0.448000000000000000
          end
          item
            Text = 'J'
            Z = 0.316000000000000000
          end
          item
            Text = 'K'
            Z = 0.392000000000000000
          end
          item
            Text = 'L'
            Z = 0.049000000000000000
          end>
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        ZValues.Name = 'Z'
        ZValues.Order = loNone
      end
    end
  end
  object Panel2: TPanel
    Left = 562
    Top = 0
    Width = 186
    Height = 528
    Align = alClient
    TabOrder = 2
    object Label_min: TLabel
      Left = 16
      Top = 40
      Width = 48
      Height = 13
      Caption = 'Label_min'
    end
    object Label_max: TLabel
      Left = 112
      Top = 40
      Width = 51
      Height = 13
      Caption = 'Label_max'
    end
    object Label1: TLabel
      Left = 40
      Top = 152
      Width = 29
      Height = 13
      Caption = 'format'
    end
    object RadioGroup_showform: TRadioGroup
      Left = 80
      Top = 64
      Width = 121
      Height = 81
      Caption = #1087#1086#1082#1072#1079#1099#1074#1072#1090#1100
      ItemIndex = 0
      Items.Strings = (
        'x1'
        'x2'
        'x1-x2'
        'x1,x2')
      TabOrder = 0
      OnClick = RadioGroup_showformClick
    end
    object Edit_min: TEdit
      Left = 8
      Top = 8
      Width = 89
      Height = 21
      TabOrder = 1
      Text = '0'
      OnKeyDown = Edit_minKeyDown
    end
    object Edit_max: TEdit
      Left = 112
      Top = 8
      Width = 89
      Height = 21
      TabOrder = 2
      Text = '1'
      OnKeyDown = Edit_maxKeyDown
    end
    object CheckBox_auto: TCheckBox
      Left = 16
      Top = 80
      Width = 49
      Height = 25
      Caption = 'All'
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = CheckBox_autoClick
    end
    object CheckBox_oct: TCheckBox
      Left = 16
      Top = 104
      Width = 49
      Height = 25
      Caption = 'oct'
      TabOrder = 4
    end
    object Edit_fmt: TEdit
      Left = 32
      Top = 168
      Width = 129
      Height = 21
      TabOrder = 5
      Text = '%lg'
    end
    object Edit_l_numb: TEdit
      Left = 32
      Top = 192
      Width = 129
      Height = 21
      TabOrder = 6
      Text = '5.'
      OnKeyDown = Edit_l_numbKeyDown
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 248
    Top = 24
  end
end
