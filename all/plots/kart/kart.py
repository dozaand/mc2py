#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      and
#
# Created:     11.06.2010
# Copyright:   (c) and 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as pat
#import pylab


koordwwer=[[1, 7], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13], [0, 14], [1,14], [1, 15], [2, 15], [3, 15], [4, 15], [5, 15], [6, 15], [8, 14]]
koordrbmk2488=[[23, 33], [18, 38], [16, 40], [14, 42], [12, 44], [11, 45], [10, 46], [9,
      47], [8, 48], [7, 49], [6, 50], [5, 51], [4, 52], [4, 52], [3, 53], [3,
      53], [2, 54], [2, 54], [1, 55], [1, 55], [1, 55], [1, 55], [1, 55], [0,
      56], [0, 56], [0, 56], [0, 56], [0, 56], [0, 56], [0, 56], [0, 56], [0,
      56], [0, 56], [1, 55], [1, 55], [1, 55], [1, 55], [1, 55], [2, 54], [2,
      54], [3, 53], [3, 53], [4, 52], [4, 52], [5, 51], [6, 50], [7, 49], [8,
      48], [9, 47], [10, 46], [11, 45], [12, 44], [14, 42], [16, 40], [18,
      38], [23, 33]]
koordrbmk1884=[[25, 39], [22, 42], [20, 44], [18, 46], [17, 47], [16, 48], [15, 49], [14,
    50], [13, 51], [12, 52], [11, 53], [11, 53], [10, 54], [10, 54], [9,
    55], [9, 55], [9, 55], [8, 56], [8, 56], [8, 56], [8, 56], [8, 56], [8,
    56], [8, 56], [8, 56], [8, 56], [8, 56], [8, 56], [8, 56], [8, 56], [8,
    56], [9, 55], [9, 55], [9, 55], [10, 54], [10, 54], [11, 53], [11,
    53], [12, 52], [13, 51], [14, 50], [15, 49], [16, 48], [17, 47], [18,
    46], [20, 44], [22, 42], [25, 39]];


def hextrans(x,y):
    return (x - y*0.5, y*0.866025)#sqrt(3)/2
def etrans(x,y):
    return (x,y)

class Tkartshow:
    u"kartogramm"
    def __init__(self,data,koord=koordwwer,geom=6,title=""):
        self.title=title
        if geom==6:
            trans=hextrans
            r=1.1547/2 #2/sqrt(3)
            angle=0
        else:
            trans=etrans
            r=1.4/2 #2/sqrt(3)
            angle=3.1418/4
        y=0
        i=0
        ymin=0
        ymax=0
        xmin=koord[0][0]
        xmax=koord[0][0]
        self.patches = []
        self.fig=plt.figure()
        self.ax=self.fig.add_subplot(111, aspect='equal')
        for [beg,end] in koord:
            y+=1
            for x in range(beg,end):
                xt,yt=trans(x,y)
                xmin=min(xmin,xt)
                ymin=min(ymin,yt)
                xmax=max(xmax,xt)
                ymax=max(ymax,yt)

                e=pat.RegularPolygon((xt,yt), geom,radius=r,orientation=angle)
                self.patches.append(e)
                e=self.ax.text(xt,yt,str(data[i]),horizontalalignment='center',verticalalignment='center')
                i+=1
        self.rect=[xmin-1, xmax+1, ymin-1, ymax+1]
        self.poly = matplotlib.collections.PatchCollection(self.patches, cmap=matplotlib.cm.jet)
        self.ax.add_collection(self.poly)
        self.ax.axis(self.rect)
        self.poly.set_array(data)
        self.fig.colorbar(self.poly)
        slef.plt=plt
        plt.title(title)
        
        plt.show()

    def saveto(nm):
        self.plt.savefig(nm)

    def show(self,data):
        u" show data kartogramm"
        for txt,d in zip(self.texts,data):
            txt.set_text(str(d))
        self.poly.set_array(data)
#        plt.set_title(self.title)
#        plt.colorbar(self.poly)
        plt.show()

def main():
    data=np.arange(163)
#    data1884=np.arange(1884)
#    data2488=np.arange(3000)
    a=Tkartshow(data,title=r"$\Sigma_f$")
#    b=Tkartshow(data1884,title=r"Rbmk",koord=koordrbmk1884,geom=4)
#    b=Tkartshow(data2488,title=r"Rbmk",koord=koordrbmk2488,geom=4)
    pass

if __name__ == '__main__':
    main()