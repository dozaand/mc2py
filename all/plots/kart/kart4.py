#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      and
#
# Created:     11.06.2010
# Copyright:   (c) and 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as pat


koord=[[1, 7], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13], [0, 14], [1,14], [1, 15], [2, 15], [3, 15], [4, 15], [5, 15], [6, 15], [8, 14]]

fig = plt.figure()
ax = fig.add_subplot(111, aspect='equal')

y=0
for [beg,end] in koord:
    y+=1
    for x in range(beg,end):
#            e=pat.Annotation("asd",(x,y))
#	    e=pat.RegularPolygon((x,y), 6,radius=0.5)
            e=pat.RegularPolygon((x,y), 6,radius=0.5)
	    plt.text(x,y,"aa")
#	    e.fill=1
	    ax.add_artist(e)
#	    ax.add_artist(e1)
#	    e.set_clip_box(ax.bbox)
#        plt.text(x,y,str((y,x)))

plt.axis([0, 15, 0, 15])

#plt.draw()
plt.show()

def main():
    plt.draw()
    pass

if __name__ == '__main__':
    main()