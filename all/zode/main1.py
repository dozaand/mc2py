from ZODB import FileStorage, DB
from persistent import Persistent
from BTrees.OOBTree import OOBTree

import transaction

class Tx(Persistent):
    def __init__(self,nm):
        self.name=nm
        self.adr="eqewqw"
#        self.newvar="eqewqw"
        self.db1=OOBTree()
        self.db2=OOBTree()


storage = FileStorage.FileStorage('mydatabase.fs')
db = DB(storage)
connection = db.open()
root = connection.root()

if not root.has_key('Txst'):
    root['Txst'] = OOBTree()

st=root['Txst']

#for i in xrange(20):
#	st[str(i)]=Tx(i)


transaction.commit()
