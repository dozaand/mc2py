#!/usr/bin/env python
# -*- coding: cp1251 -*-

from scipy import sparse as s
from numpy.random import rand
import matplotlib.pyplot as plt

A = s.lil_matrix((1000, 1000))
for i in xrange(0,500):
    A[i, 100] = 100
for i in xrange(9,500):
    A[i, 500] = 10000

#B=A.toarray()
#plt.imshow(A)
#plt.colorbar()
#plt.show()

plt.spy(A,marker="*",precision=0)
plt.show()







