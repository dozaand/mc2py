#!/usr/bin/env python
# -*- coding: cp1251 -*-

import numpy as np
import scipy.sparse as sp

def IntoCoo(B,porog):
    """"������ �� ������� ������� B ����������� �� ������� porog"""
    a=B<porog
    data=[]
    row=[]
    col=[]
    for index,element in enumerate(a):
        if elementd == True:
            data.append(B[index])
            row.append(index)
            col.append(0)
    A=sp.coo_matrix((data,(row,col)),shape=[len(B),1])
    return A

def CooDot(A,B):
    """��������� ������������ Coo-������� �� ������ ������

������ �������������
from numpy.random import rand
B=rand(100)
d=CooDot(IntoCoo(B,0.5),B)
    """
    dot=A.T*B
    return dot[0]

def MatrGtEps(ny,nx,it,eps):
    """�������� lil ������� �� ��������� �������� np �������� ������ ���� ������ eps
>>> n=50000
>>> x=np.linspace(0,30,n)
>>> def vit(nv):
...    for delta in np.linspace(0,4,nv):
...        yield np.sin(x+delta)
>>> A=MatrGtEps(5,n,vit(5),0.98)
>>> print (A*x)>0
[False False False  True  True]
    """
    A = sp.lil_matrix((ny,nx),dtype='f')
    for i,row in enumerate(it):
        A[i,:]=row*(np.abs(row)>eps)
    return A.tocsr()


