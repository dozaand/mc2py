#!/usr/bin/env python
# -*- coding: cp1251 -*-
#-------------------------------------------------------------------------------
# Name:        module1
# Purpose: ���������� ��� ������ ���� A x'=f(t,x) � ����������� xn=obj.f(t,x) xn=A^{-1} f(t,x), obj.esolve(t,x0,b): xn:xn+h f(t,xn)==b
#
# Author:      and
#
# Created:     29.05.2010
# Copyright:   (c) and 2010
# Licence:
#-------------------------------------------------------------------------------
import matplotlib
matplotlib.interactive(True)
matplotlib.use("WXAgg")
import matplotlib.pyplot as plt
import copy

import numpy as num
import numpy.linalg

class Tode_stat:
    u"���������� ������ �����������"
    def __init__(self):
        nfun=0
        nsolve=0

class Tode_fixed_step:
    u"�������� ������ ���� � ������������� �����"
    def __init__(self,x0,oper,tbeg=0,tend=1e308,dt=1):
        self.t=tbeg
        self.tend=tend
        self.oper=oper
        self.x=x0.copy()
        self.dt=dt
    def __iter__(self):
        return self

class ee(Tode_fixed_step):
    u"����� ����� ������"
    def next(self):
        if self.t> self.tend: raise StopIteration
        self.x+=self.dt*self.oper.f(self.t,self.x)
        self.t+=self.dt
        return self

class ie(Tode_fixed_step):
    u"������� ����� ������"
    def next(self):
        if self.t> self.tend: raise StopIteration
        f1=self.oper.esolve;dt=self.dt;x=self.x
        f1(self.t,dt,x,x)
        self.t+=self.dt
        return self

class pk(Tode_fixed_step):
    u"��������� - ���������"
    def next(self):
        if self.t> self.tend: raise StopIteration
        f=self.oper.f
        x1=self.x+self.dt*f(self.t,self.x)
        t1=self.t+self.dt
        self.x+=(self.dt/2)*(f(self.t,self.x)+f(t1,x1))
        self.t=t1
        return self

class tr(Tode_fixed_step):
    u"������� ����� ��������"
    def next(self):
        if self.t> self.tend: raise StopIteration
        f=self.oper.f;ef=self.oper.esolve;dt=self.dt;x=self.x;t=self.t
        b=x+dt/2*f(t,x)
        ef(t,-dt/2,b,x)
        self.t+=self.dt
        return self

class rk4(Tode_fixed_step):
    u"�����-����� 4"
    def next(self):
        if self.t> self.tend: raise StopIteration
        f=self.oper.f;dt=self.dt;t=self.t;x=self.x

        k1=dt*f(t,x)
        k2=dt*f(t+dt/2,x+0.5*k1)
        k3=dt*f(t+dt/2,x+0.5*k2)
        k4=dt*f(t+dt,x+k2)
        x+=(k1+k4+2*(k2+k3))/6
        self.t+=dt;

        return self

class gear(Tode_fixed_step):
    u"����� ����"
    gear_a=(
(1.0000000000000000000, 0, 0, 0, 0, 0,0),
(1.3333333333333333333, -0.33333333333333333333, 0, 0, 0, 0,0),
(1.6363636363636363636, -0.81818181818181818182,0.18181818181818181818, 0, 0, 0,0),
(1.9200000000000000000, -1.4400000000000000000,0.64000000000000000000, -0.12000000000000000000, 0, 0,0),
(2.1897810218978102190, -2.1897810218978102190,1.4598540145985401460, -0.54744525547445255474, 0.087591240875912408759,0, 0),
(2.4489795918367346939, -3.0612244897959183673,2.7210884353741496599, -1.5306122448979591837,0.48979591836734693878, -0.068027210884353741497,0),
(2.6997245179063360882, -4.0495867768595041322,4.4995408631772268136, -3.3746556473829201102,1.6198347107438016529, -0.44995408631772268136, 0.055096418732782369146)
    )
    gear_b=(1.0000000000000000000, 0.66666666666666666667, 0.54545454545454545455,0.48000000000000000000, 0.43795620437956204380, 0.40816326530612244898,0.38567493112947658402)
    max_order=7
    def rot(self):
        u"������ ������ ��������� ��� ����� ���������� ��� � �������� 0"
        v=self.xvals.pop()
        self.xvals.insert(0,v)


    def __init__(self,x0,oper,tbeg=0,tend=1e308,dt=1,order=5):
        Tode_fixed_step.__init__(self,x0,oper,tbeg,tend,dt)
        self.xvals=[x0.copy() for i in range(gear.max_order+1)]
        self.b=x0.copy()
        self.x=self.xvals[0]
        self.order=min(order,gear.max_order-1)

    def next(self):
        f=self.oper.f;ef=self.oper.esolve;dt=self.dt;xvals=self.xvals;t=self.t;order=self.order;b=self.b
        if self.t > self.tend: raise StopIteration

        b=gear.gear_a[order-1][0]*xvals[0]
        for i in range(1,self.order):
            b+=gear.gear_a[order-1][i]*xvals[i]
        self.rot()
        self.x=self.xvals[0]
        ef(t+dt,-dt*gear.gear_b[order-1],b,self.x)
        self.t+=self.dt
        return self


class Tsin:
    u"������ � ������� ������� ���������� �����"
    def __init__(self,omega=1,lam=-0.0):
        self.omega=omega
        self.lam=lam
        self.matr=num.array([[lam,omega],[-omega,lam]])
        self.imatr=num.linalg.inv(self.matr)

    def f(self,t,x):
        u"���������� �����������"
        return num.array([
          self.lam*x[0]+self.omega*x[1],
         -self.omega*x[0]+self.lam*x[1]
         ])
    def esolve(self,t,h,b,x):
        u"������� ������� x+h*f(t,x)==b  � ��������� ������������ x0 ��������� ������ � x0"
        o0 = h**2; o1 = (self.omega)**2; o2 = h*self.lam; o3 = 1 + o2
        x[0]=(b[0]+h*b[0]*self.lam-h*b[1]*self.omega)/(1+o0*o1+2*h*self.lam+o0*(self.lam)**(2))
        x[1]=(-1*o3*b[1]-h*b[0]*self.omega)/(-o0*o1-(o3)**(2))

def main():
    res=[]
    for st in gear(num.array([0.,1.]),Tsin(0.2,0.),0,50,0.4,order=3):
        res.append([st.t,st.x[0],st.x[1]])
#        print st.x
    v=num.array(res)
    plt.plot(v[:,0],v[:,1])
    plt.plot(v[:,0],v[:,2])
    plt.show()


if __name__ == '__main__':
    main()