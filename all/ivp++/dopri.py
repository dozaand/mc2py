class Tode_stat:
    u"���������� ������ �����������"
    def __init__(self):
        self.nfun=0
        self.nsolve=0
		self.naccept=0
		self.nreject=0

c2=0.2
c3=0.3
c4=0.8
c5=8./9.
a21=0.2
a31=3./40.
a32=9./40.
a41=44./45.
a42=-56./15.
a43=32./9.
a51=19372./6561.
a52=-25360./2187.
a53=64448./6561.
a54=-212./729.
a61=9017./3168.
a62=-355./33.
a63=46732./5247.
a64=49./176.
a65=-5103./18656.
a71=35./384.
a73=500./1113.
a74=125./192.
a75=-2187./6784.
a76=11./84.
e1=71./57600.
e3=-71./16695.
e4=71./1920.
e5=-17253./339200.
e6=22./525.
e7=-1./40.  
# ---- dense output of shampine (1986)
d1=-12715105075./11282082432.
d3=87487479700./32700410799.
d4=-10690763975./1880347072.
d5=701980252875./199316789632.
d6=-1453857185./822651844.
d7=69997945./29380423.

def dot2(a):
	return dot(a,a)

class dopri_error:
	def __init__(self,msg):
		self.msg=msg

class Tdopri_out:
	def __init__(self,t0,t,x0):
		con=[x0.copy() for i in xrange(5)]
	def set(t0,t,x):
		self.x=x
		self.t=t
		self.t0=t0
	def interp(tin):
		if t0<tin<t: dopri_error("interpolation out of range")
		theta=(self.t-self.t0)/self.h
		theta1=1.-theta
		return con[0]+theta*(con[1]+theta1*(con[2]+theta*(con[3]+theta1*con[4])))

def dopri5(self,x0,oper,tbeg=0,tend=1e308,dt=0.01,nmax=100000,nstiff=1000,hmax=0,
	uround=2.3e-16,safe=0.9e0,fac1=0.2e0,fac2=10,beta=0.04,atoli=1e-2,rtoli=1e-2,dense_out=0):
	if hmax==0:
		hmax=tend-tbeg
	else:
		hmax=min(tend-tbeg,hmax)
    u"����� �������� ������ ������ �������"
	assert self.nmax>0
	assert self.uround>0
	assert 1e-4<self.safe<1
	assert (self.fac1<self.fac2) and 0<self.fac1 and 0<self.fac2
	assert 0<=self.beta<0.2
	assert 0<self.hmax<=(self.tend-self.tbeg)
	assert self.tbeg<self.tend
	facold=1.e-4  
	expo1=0.2-beta*0.75
	facc1=1./fac1
	facc2=1./fac2
	last=.false. 
	hlamb=0.d0
	iasti=0
	k1=oper.f(t,x)
	iord=5
	out=Tdopri_out(x0)
	
	assert 0<dt<hmax
	h=dt
	  
#	if (h.eq.0.d0) h=hinit(n,fcn,x,y,xend,posneg,k1,k2,k3,iord,
#     &			     hmax,atol,rtol,itol,rpar,ipar)
	
	nfcn+=2
	reject=0
	told=t
	while t<=tend:
		if nstep < nmax: raise dopri_error(' more than nmax =%d steps are needed'%nmax )
		if 0.1*abs(h)<=abs(t)*uround: raise dopri_error(' step size t0o small, h=%d'%h)
		if (t+1.01*h-tend)>0:
			h=tend-t 
			last=1
		nstep+=1
# --- the first 6 stages
		k1=oper.f(t,x)
		y1=y+h*a21*k1
		k2=oper.f(t+c2*h,y1)
		y1=y+h*(a31*k1+a32*k2)
		k3=oper.f(t+c3*h,y1)
		y1=y+h*(a41*k1+a42*k2+a43*k3)
		k4=oper.f(t+c4*h,y1)
		y1=y+h*(a51*k1+a52*k2+a53*k3+a54*k4)
		k5=oper.f(t+c5*h,y1)
		ysti=y+h*(a61*k1+a62*k2+a63*k3+a64*k4+a65*k5)
		tph=t+h
		k6=oper.f(tph,ysti)
		y1=y+h*(a71*k1+a73*k3+a74*k4+a75*k5+a76*k6)
		k2=oper.f(tph,y1)
		if dense_out:
			out.con[4]=h*(d1*k1+d3*k3+d4*k4+d5*k5+d6*k6+d7*k2)
		k4=(e1*k1+e3*k3+e4*k4+e5*k5+e6*k6+e7*k2)*h
		nfcn+=6
# --- error estimation  
		err=0.
		err=sqrt(dot(k4,k4))
# --- computation of hnew
		fac11=err**expo1
# --- lund-stabilization		
		fac=fac11/facold**beta
		fac=max(facc2,min(facc1,fac/safe))
		hnew=h/fac
		if (err+atoli)/rtoli<1.:
			facold=max(err,1.0e-4)
			naccpt+=1
# ------- stiffness detection
			if naccpt % nstiff==0 or iasti>0):
				stnum=dot2(k2-k6)
				stden=dot2(y1-ysti)
				if stden>0.: hlamb=h*sqrt(stnum/stden)
				if hlamb>3.25:
					nonsti=0
					iasti+=1
					if iasti==15: raise dopri_error(' the problem seems to become stiff at t = %g',t )
				else
					nonsti+=1
					if nonsti==6: iasti=0
			
			if dense_out:
				cont0=y
				cont1=y1-y
				cont2=h*k1-cont1
				cont3=-h*k2(i)+cont1-cont2
			k1=k2
			y=y1
			hout=h
			out.set(told,t,y)
			yield out
			if last :
				h=hnew
				idid=1
				return None
			if hnew>hmax: hnew=hmax
			if reject: hnew=min(hnew,h)
			reject=0
		else:
# --- step is rejected   			
			hnew=h/min(facc1,fac11/safe)
			reject=1
			if naccpt>=1: nrejct=nrejct+1
			last=0
		
		h=hnew
	
			
