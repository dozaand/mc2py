#!/usr/bin/env python
# -*- coding: cp1251 -*-
import os,sys,glob,string,getopt,re,cPickle

def Find(dir,mask):
    "����� ����� �� �������� �����"
    for root, dirs, files in os.walk(dir):
        for name in files:
            if glob.fnmatch.fnmatch(name,mask):
                yield os.path.abspath(os.path.join(root, name)) # �������� ������ ��� �����

def ParseIdf(nm):
    nameidf=(os.path.split(nm)[1]).lower()
    intdef=re.compile(r"^=(\w+) +(\w+).+")
    vardef=re.compile(r"^([iosuphkfjnq]) +(\w+)(\(.+?\))?.+")
    varlist=[]
    for line in open(nm,"r").xreadlines():
        lineL=line.lower()
        res=re.match(vardef,lineL)
        if res:
            a=list(res.groups())
            a.append(nameidf)
            varlist.append(a)
    return varlist


def VarDb():
    res=[]
    for i in Find(".","*.idf"):
       res.extend(ParseIdf(i))
    return res

def DumpVars():
    res=VarDb()
    f=open("var.db",'wb')
    cPickle.dump(res,f,1)
    f.close()

def VarsDb():
    if not os.path.exists("var.db"):
    	DumpVars()
    f=open("var.db",'rb')
    res=cPickle.load(f)
    f.close()
    return res


def UndefVars(nm,db):
    needvars=[i[1] for i in db if i[-1]==nm and i[0]=='i']
    m={}
    notdeflist=[]
    for i in db:
        try:
            m[i[1]].add(i[0])
        except KeyError:
            m[i[1]]=set([i[0]])
    for i in needvars:
        if 'o' not in m[i]:
            notdeflist.append(i)
    return notdeflist


res=UndefVars("ysvrk.idf",res)
print res



