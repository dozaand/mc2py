       Common /constwrk/constwrk_i1(9010)
       integer*1 constwrk_i1
       Common /geom/geom_i1(680)
       integer*1 geom_i1
       Common /ngrbul/ngrbul_i1(20)
       integer*1 ngrbul_i1
       Common /o/o_i1(4)
       integer*1 o_i1
       Common /m/m_i1(28)
       integer*1 m_i1
       Common /q/q_i1(32)
       integer*1 q_i1
       Common /icon/icon_i1(12)
       integer*1 icon_i1
       Common /bokr_par/bokr_par_i1(12)
       integer*1 bokr_par_i1
       Common /parbl/parbl_i1(4)
       integer*1 parbl_i1
       Common /l/l_i1(112)
       integer*1 l_i1
       Common /is/is_i1(24)
       integer*1 is_i1
       Common /names/names_i1(405)
       integer*1 names_i1
       Common /types/types_i1(120)
       integer*1 types_i1
       Common /zagrbk/zagrbk_i1(14928)
       integer*1 zagrbk_i1
       Common /h/h_i1(956)
       integer*1 h_i1
       Common /w/w_i1(29856)
       integer*1 w_i1
       Common /bl12/bl12_i1(4)
       integer*1 bl12_i1
       Common /rr/rr_i1(60)
       integer*1 rr_i1
       Common /sech_tk/sech_tk_i1(69664)
       integer*1 sech_tk_i1
       Common /inner/inner_i1(119424)
       integer*1 inner_i1
       Common /temp_var/temp_var_i1(27440)
       integer*1 temp_var_i1
       Common /mss/mss_i1(360)
       integer*1 mss_i1
       Common /tvsp/tvsp_i1(12)
       integer*1 tvsp_i1
      real*4 p1_wrk(45)
       Equivalence (constwrk_i1(1), p1_wrk(1))
      real*4 p2_wrk(45)
       Equivalence (constwrk_i1(181), p2_wrk(1))
      real*4 d1_wrk(45)
       Equivalence (constwrk_i1(361), d1_wrk(1))
      real*4 d2_wrk(45)
       Equivalence (constwrk_i1(541), d2_wrk(1))
      real*4 ss_wrk(45)
       Equivalence (constwrk_i1(721), ss_wrk(1))
      real*4 a_wrk(15, 16)
       Equivalence (constwrk_i1(901), a_wrk(1, 1))
      real*4 b_wrk(15, 16)
       Equivalence (constwrk_i1(1861), b_wrk(1, 1))
      real*4 c_wrk(15, 16)
       Equivalence (constwrk_i1(2821), c_wrk(1, 1))
      real*4 d_wrk(15, 16)
       Equivalence (constwrk_i1(3781), d_wrk(1, 1))
      real*4 h_wrk(15, 16)
       Equivalence (constwrk_i1(4741), h_wrk(1, 1))
      real*4 v_wrk(15, 16)
       Equivalence (constwrk_i1(5701), v_wrk(1, 1))
      real*4 z_wrk(15, 16)
       Equivalence (constwrk_i1(6661), z_wrk(1, 1))
      character*4 tip_sbor(50)
       Equivalence (constwrk_i1(7621), tip_sbor(1))
      integer*4 n_tip(50)
       Equivalence (constwrk_i1(7821), n_tip(1))
      integer*4 in_cont(50)
       Equivalence (constwrk_i1(8021), in_cont(1))
      integer*4 n_sbor(55)
       Equivalence (constwrk_i1(8221), n_sbor(1))
      integer*4 tips2lib(50)
       Equivalence (constwrk_i1(8441), tips2lib(1))
      integer*4 n_cons
       Equivalence (constwrk_i1(8641), n_cons)
      integer*4 cons2zgr(60)
       Equivalence (constwrk_i1(8645), cons2zgr(1))
      integer*4 nom_sost
       Equivalence (constwrk_i1(8885), nom_sost)
      integer*4 dlt_sost
       Equivalence (constwrk_i1(8889), dlt_sost)
      integer*4 if_bibl
       Equivalence (constwrk_i1(8893), if_bibl)
      integer*4 if_file
       Equivalence (constwrk_i1(8897), if_file)
      character h2o_sbor(50)
       Equivalence (constwrk_i1(8901), h2o_sbor(1))
      integer*1 cons_repl(60)
       Equivalence (constwrk_i1(8951), cons_repl(1))
      integer*4 irow
       Equivalence (geom_i1(1), irow)
      integer*4 ibeg(56)
       Equivalence (geom_i1(5), ibeg(1))
      integer*4 iend(56)
       Equivalence (geom_i1(229), iend(1))
      integer*4 itot(56)
       Equivalence (geom_i1(453), itot(1))
      integer*4 mo
       Equivalence (geom_i1(677), mo)
      integer*4 niter2
       Equivalence (ngrbul_i1(1), niter2)
      integer*4 mjs
       Equivalence (ngrbul_i1(5), mjs)
      integer*4 mjt
       Equivalence (ngrbul_i1(9), mjt)
      integer*4 npopr
       Equivalence (ngrbul_i1(13), npopr)
      real*4 ti
       Equivalence (ngrbul_i1(17), ti)
      integer*4 ni
       Equivalence (o_i1(1), ni)
      real*4 rk_wnom(6)
       Equivalence (m_i1(1), rk_wnom(1))
      integer*4 nr
       Equivalence (m_i1(25), nr)
      real*4 q
       Equivalence (q_i1(1), q)
      real*4 u
       Equivalence (q_i1(5), u)
      real*4 bz
       Equivalence (q_i1(9), bz)
      real*4 x
       Equivalence (q_i1(13), x)
      real*4 y
       Equivalence (q_i1(17), y)
      real*4 eps_f
       Equivalence (q_i1(21), eps_f)
      real*4 eps_k
       Equivalence (q_i1(25), eps_k)
      integer*4 nw
       Equivalence (q_i1(29), nw)
      integer*4 icon
       Equivalence (icon_i1(1), icon)
      integer*2 i_kmpc
       Equivalence (icon_i1(5), i_kmpc)
      integer*2 i_kosuz
       Equivalence (icon_i1(7), i_kosuz)
      integer*4 i_sost
       Equivalence (icon_i1(9), i_sost)
      integer*4 irod
       Equivalence (bokr_par_i1(1), irod)
      integer*4 iprint
       Equivalence (bokr_par_i1(5), iprint)
      integer*4 ip6
       Equivalence (bokr_par_i1(9), ip6)
      real*4 pr
       Equivalence (parbl_i1(1), pr)
      real*4 rezerv_l(2)
       Equivalence (l_i1(1), rezerv_l(1))
      real*4 o(3)
       Equivalence (l_i1(9), o(1))
      real*4 heff
       Equivalence (l_i1(21), heff)
      real*4 pi2heff
       Equivalence (l_i1(25), pi2heff)
      real*4 h_flux(20)
       Equivalence (l_i1(29), h_flux(1))
      integer*4 nzb
       Equivalence (l_i1(109), nzb)
      real*4 uc(6)
       Equivalence (is_i1(1), uc(1))
      character*256 baz_name
       Equivalence (names_i1(1), baz_name)
      character*8 name_ctr(8)
       Equivalence (names_i1(257), name_ctr(1))
      character*1 aes
       Equivalence (names_i1(321), aes)
      character*1 unit
       Equivalence (names_i1(322), unit)
      character*8 namep1
       Equivalence (names_i1(323), namep1)
      character*35 prog_ver
       Equivalence (names_i1(331), prog_ver)
      character*40 pase
       Equivalence (names_i1(366), pase)
      character*4 type1(30)
       Equivalence (types_i1(1), type1(1))
      integer*2 kb(2488)
       Equivalence (zagrbk_i1(1), kb(1))
      real*4 p(2488)
       Equivalence (zagrbk_i1(4977), p(1))
      real*4 hr(235)
       Equivalence (h_i1(1), hr(1))
      integer*4 na
       Equivalence (h_i1(941), na)
      integer*4 np
       Equivalence (h_i1(945), np)
      integer*4 ny
       Equivalence (h_i1(949), ny)
      integer*4 ip
       Equivalence (h_i1(953), ip)
      real*4 wk(2488)
       Equivalence (w_i1(1), wk(1))
      real*4 s(2488)
       Equivalence (w_i1(9953), s(1))
      real*4 t(2488)
       Equivalence (w_i1(19905), t(1))
      real*4 q1
       Equivalence (bl12_i1(1), q1)
      real*4 rez_bokr(15)
       Equivalence (rr_i1(1), rez_bokr(1))
      real*4 ds(2488)
       Equivalence (sech_tk_i1(1), ds(1))
      real*4 dt(2488)
       Equivalence (sech_tk_i1(9953), dt(1))
      real*4 ps(2488)
       Equivalence (sech_tk_i1(19905), ps(1))
      real*4 pt(2488)
       Equivalence (sech_tk_i1(29857), pt(1))
      real*4 st(2488)
       Equivalence (sech_tk_i1(39809), st(1))
      real*4 ft(2488)
       Equivalence (sech_tk_i1(49761), ft(1))
      real*4 fs(2488)
       Equivalence (sech_tk_i1(59713), fs(1))
      real*4 ds1(2488)
       Equivalence (inner_i1(1), ds1(1))
      real*4 dt1(2488)
       Equivalence (inner_i1(9953), dt1(1))
      real*4 ps1(2488)
       Equivalence (inner_i1(19905), ps1(1))
      real*4 pt1(2488)
       Equivalence (inner_i1(29857), pt1(1))
      real*4 sja(2488)
       Equivalence (inner_i1(39809), sja(1))
      real*4 sjb(2488)
       Equivalence (inner_i1(49761), sjb(1))
      real*4 sjc(2488)
       Equivalence (inner_i1(59713), sjc(1))
      real*4 sjd(2488)
       Equivalence (inner_i1(69665), sjd(1))
      real*4 tja(2488)
       Equivalence (inner_i1(79617), tja(1))
      real*4 tjb(2488)
       Equivalence (inner_i1(89569), tjb(1))
      real*4 tjc(2488)
       Equivalence (inner_i1(99521), tjc(1))
      real*4 tjd(2488)
       Equivalence (inner_i1(109473), tjd(1))
      real*4 temp2488(2488)
       Equivalence (temp_var_i1(1), temp2488(1))
      real*4 src_popr(2488)
       Equivalence (temp_var_i1(9953), src_popr(1))
      real*4 temp1884(1884)
       Equivalence (temp_var_i1(19905), temp1884(1))
      real*4 conc(10)
       Equivalence (mss_i1(1), conc(1))
      real*4 ssp(10)
       Equivalence (mss_i1(41), ssp(1))
      real*4 ttp(10)
       Equivalence (mss_i1(81), ttp(1))
      real*4 ccbp(10)
       Equivalence (mss_i1(121), ccbp(1))
      real*4 qqa(10)
       Equivalence (mss_i1(161), qqa(1))
      integer*4 npg(10)
       Equivalence (mss_i1(201), npg(1))
      integer*4 nbt(10)
       Equivalence (mss_i1(241), nbt(1))
      integer*4 nsh(10)
       Equivalence (mss_i1(281), nsh(1))
      integer*4 nbo(10)
       Equivalence (mss_i1(321), nbo(1))
      real*4 part_wt(3)
       Equivalence (tvsp_i1(1), part_wt(1))
