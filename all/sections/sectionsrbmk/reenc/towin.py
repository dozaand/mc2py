import os,glob

def dos2win(file):
    f=open(file,"r")
    lines=unicode(f.read(),'cp866')
    f.close()
    f=open(file,"w")
    f.write(lines.encode("cp1251",'ignore'))
    f.close()

for file in glob.glob("*.for"):
	print file
	dos2win("BOKRBA.FOR")