#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      and
#
# Created:     26.06.2010
# Copyright:   (c) and 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import os,glob,sys

def filedos2win(file):
    f=open(file,"r")
    lines=unicode(f.read(),'cp866')
    f.close()
    f=open(file,"w")
    f.write(lines.encode("cp1251",'ignore'))
    f.close()

def dos2win(pattr):
    for file in glob.glob(pattr):
	filedos2win(file)

def main():
    for i in sys.argv:
        dos2win(i)

if __name__ == '__main__':
    main()