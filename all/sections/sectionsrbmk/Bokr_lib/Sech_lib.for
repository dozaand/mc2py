      subroutine read_lib(file_name)
      character file_name*256, namefl*256, char4*4
      real*4 r(112)
       include 'bc_vars.h'
       include 'bokr_var.h'

      open(1, file=file_name, form='binary')
         read(1) catalog_i1, cons_dat_i1, dop_cons_i1
      close(1)
      U=0.5*U*U

      write(7,*) '���������� ����-���� ', bcvernum, ' �� ', date_time

      do l_name=1,256
         j = ichar(file_name(l_name:l_name))
         if(j.le.32) exit
      enddo
      l_path= l_name
      do i=1, l_name - 1
         l_path = l_path - 1
         if(file_name(l_path:l_path) .eq. '\') then         
            exit
         endif
      enddo
      if(l_path .le. 1)  namefl = 'BokrRBMK.inp'
      if(l_path .gt. 1)  namefl = file_name(:l_path) // 'BokrRBMK.inp'
      write(7,*) '���� ���������� ��������: ', file_name(:l_name)

! ��������� ��������� ��������� ����� ����� ����-1000
      open(1, file=namefl, status='old', err=400)
      write(*,*) '��������� ��������� ��-��� �������� �� �����: '
      write(*,*) namefl(:l_path+12)
      write(7,*) '���������� ����-���� ����������� ����������� ��������� ��-��� ��������'
      write(7,*) '�� �����: ', namefl(:l_path+12)
      do jj = 1,100
        read(1, *, end=10, err = 50) char4, itip
        if(char4.eq.'����' .or. char4.eq.'����') then
          do icon = 1, n_ntk
             if(char4 .eq. char4_nf(icon)) then
             kkk = 0
             do k=1, n_sost
                if( itip .eq. con_sost(k) ) then
                  kkk = kkk + 1
                  read(1, *, end=10, err = 50) (tempa7(j), j=1, 5)
                  do m=1, 5
                     cons_nf(icon, (k-1)*5+m) = tempa7(m)
                  enddo
                  con_fill(n_tvs+icon,k) = 1
                  write(*,'(i3,i5,1x,a4,5es13.6, a35)') icon, itip, char4, (tempa7(j),j=1,5)
                endif
             enddo
             endif
          enddo
          write(7,'(a36,a5,i5)') ' ��������� ��������� ���������� ���� ', char4, itip
        elseif(char4.eq.'���') then
          do icon = 1, n_tvs
             if(char4 .eq. char4_f(icon)) then
             backspace(1)
             read(1, *, err = 50) char4, itip, wn
             kkk = 0
             do k=1, n_sost
                if( itip .eq. con_sost(k) ) then
                  kkk = kkk + 1
                  read(1,*) r
                  if(wn .gt. 0) w_nom(icon, k) = wn
                  do m=1, 112
                     cons_f(icon, (k-1)*112+m) = r(m)
                  enddo
                  con_fill(icon,k) = 1
                  write(*,'(i3,i5,1x,a4,a22)') icon, itip, char4, ' - �������� ��������� '
                endif
             enddo
             endif
          enddo
          write(7,'(a36,a5,i5)') ' ��������� ��������� ���������� ���� ', char4, itip
        endif
      enddo


  10  write(*,*) ' ��������� ����� ����� ', namefl
      close(1)

      goto 400


  50  write(*,*) ' ������ ������ ����� ', namefl;  write(7,*) ' ������ ������ ����� ', namefl
      pause; stop 2

 400  continue

      write(7,*) ' '

      return; end

      subroutine read_zagr_tip(file_name)
       include 'bokr_var.h'
       include 'bc_vars.h'
      character char1, char4*4, char5*5, file_name*256

      h2o_sbor = ' '
      tip_sbor = '    '
      open(1, file=file_name)                  !  ��������� ������ ������������
       do i=1,100                              !  ����� �������� ���������
          read(1,*, end=10) char5, int4        !  ���������� ����-����
          char5 = adjustl(char5)
          if(int4.gt.0 .and. int4.le.50) then
            char1 = char5(:1)
            h2o_sbor(int4) = char1
            if(char1 .eq. "+") then
               char4 = char5(2:5)
            elseif(char1 .eq. "-") then
               char4 = char5(2:5)
            else
               char4 = char5(1:4)
               h2o_sbor(int4) = ' '
            endif
          else
            write(*,*) ' ������� ����� ��� ��������', int4;      write(7,*) ' ������� ����� ��� ��������', int4 
            write(*,*) ' � ����� ', file_name;                   write(7,*) ' � ����� ', file_name
            write(*,*) ' ����������� �� ���� �������� 0<tip<51'; write(7,*) ' ����������� �� ���� �������� 0<tip<51'
            pause; stop 2 
          endif
          tip_sbor(int4) = char4

       enddo
10    close(1)

      call check_zagr_tip

      return; end

      subroutine check_zagr_tip
       include 'bokr_var.h'
       include 'bc_vars.h'
      character char1, char4*4

      do i=1,50
        char1 = h2o_sbor(i)
        if(char1.ne.'+'.and.char1.ne.'-') h2o_sbor(i)=' '
        char4 = tip_sbor(i)
        itemp1=0; do j=1,4; if(ichar(char4(j:j)).gt.32) itemp1=itemp1+1; enddo
        if(itemp1.gt.0) then
          ii = 0
          do j=1, n_types                      !  ����� �������� ���������
             if(char4 .eq. char4_types(j) )  ii = ii+1
          enddo
          if(ii.eq.0) then
            write(*,*) ' ������� ������ �������� ����� �������� ',char4; write(7,*)' ������� ������ �������� ����� �������� ',char4
!            if(if_file.gt.0) then; write(*,*) ' � ����� ', file_name;  write(7,*)' � ����� ', file_name;  endif
            write(*,*) ' ���������� ���� �������� (4 �������):';         write(7,*)' ���������� ���� �������� (4 �������):'
            pause
            do j=1, n_types
               write(*,'(a4,1x,a74)') char4_types(j), descr_types(j);    write(7,'(a4,1x,a74)') char4_types(j), descr_types(j)
            enddo
            pause; stop 2 
          endif
        endif
      enddo

      return; end

      subroutine check_tips
       include 'bokr_var.h'
       include 'bc_vars.h'

      n_tip = 0
      do i=1,mo
         if(kb(i).gt.0 .and. kb(i).le.50) then
           n_tip(kb(i))  = n_tip(kb(i)) + 1
             else
             write(*,*) '  � ����������� �������� ������������ ���',i,nyx(i),kb(i)
             write(*,*) '  ���� �������� ������ ���� � ��������� 1 - 50'
             pause
             stop 2
         endif
      enddo

       write(7,10)
   10  format(
     >'',/
     >'                   ���p���� �������� ����                                ',/
     >'',/
     >' ���  �����.���-���������� �������� �������� � ����������             ',/
     >'')
  20   format(                                                                  
     >'',i4,'  ',a1,a4,' 'i4,'  ',a52'')                                   
  30   format(                                                                  
     >'')


      n_sbor = 0
      n_tips = 0
      tips2lib = 0;  nexl=0
      itemp2 = 0
      do ii=1, 50 ! n_types
       if(n_tip(ii) .ne. 0) then
        do i=1,n_types
          if(char4_types(i) .eq. tip_sbor(ii) ) then
             n_sbor(i) = n_sbor(i) + n_tip(ii)
             in_cont(ii) = in_contur(i)
             write(7,20) ii, h2o_sbor(ii),tip_sbor(ii), n_tip(ii), descr_types(i)
          endif
        enddo

        itemp2 = itemp2 + n_tip(ii)
        n_tips = n_tips + 1

        if(h2o_sbor(ii).eq.' ') then 
!  �������� ������� � ���������� ����-���� ��������� �������� �.�.
         itemp1 = 0
         if(ii .le. nr) then                  ! ��������� �������� ����
           do i=1, n_tvs ! 15
             if(char4_f(i) .eq. tip_sbor(ii)) tips2lib(ii) = i
             if(char4_f(i) .eq. tip_sbor(ii)) iii = i
           enddo
           elseif(ii.gt.20 .and. ii.le.40) then  ! ������� ���
             do i=1, n_cr
               if(char4_cr(i) .eq. tip_sbor(ii)) tips2lib(ii) = i
               if(char4_cr(i) .eq. tip_sbor(ii)) iii = i
             enddo
             else                                  ! �� ��������� �������� ����
               do i=1, n_ntk ! 45
                 if(char4_nf(i) .eq. tip_sbor(ii)) tips2lib(ii) = i
                 if(char4_nf(i) .eq. tip_sbor(ii)) iii = i + n_tvs
               enddo
         endif
         if(tips2lib(ii) .eq. 0 ) then
           write(*,*) ' � �������� ������ �� �����  tip_zagr:'
           write(*,*) ' ���',ii,',  �������� ����������� ',tip_sbor(ii)
           write(*,*) ' � ���������� ����-���� ��� ������ �������� �������� '
           pause
           stop 2
         endif

!  �������� �� �������������� ������� ���� ��� �����
         if((ii.gt.20 .and. ii.le.40) .and. in_cont(ii) .ne. 2) then
            write(*,*)' ������. ������ ������� ������ ���� � �����'
            write(*,*)' ���',ii,',  �������� ����������� ',tip_sbor(ii)
            pause
            stop 2
         endif

         elseif(h2o_sbor(ii) .eq. "+") then
           nexl = nexl+1
           tips2lib(ii) = n_ntk+nexl
           call cons_wrk1(tip_sbor(ii), nexl, 1)
           elseif(h2o_sbor(ii) .eq. "-") then
             nexl = nexl+1
             tips2lib(ii) = n_ntk+nexl
             call cons_wrk1(tip_sbor(ii), nexl, 0)
        endif

       endif
      enddo

      write(7,30)
      write(7,*) '� �������� �������� ',itemp2,' ����� ',n_tips,'  �����.'

      end

      subroutine cons_wrk
!---���������� �������� �������� �� ���������� � ������� �������-------------
      character char4*4
       include 'bc_vars.h'
       include 'bokr_var.h'

      cons_repl = 0

! ���������� i_sost ��������� �������� 0, 1, 20 � 284 
      nom_sost = 0
      do i=1, n_sost
         if(i_sost .eq. con_sost(i) ) nom_sost = i
      enddo
      if(nom_sost .eq. 0) then
         write(*,*) ' ������ � ����������� ����',i_sost,' ����������'
         write(7,*) ' ������ � ����������� ����',i_sost,' ����������'
         pause
         stop 2
      endif

      do i=1, n_tvs !   i - ����� ���� �������� ��������. ������ � ���
        nm_sost = nom_sost + 1 - i_kmpc
        if(con_fill(i, nm_sost) .ne. 0) then
        ii = i
          else                               ! ������ ������������� � ���������� 
          char4 = char4_replace(i)           ! �������� �� ���������������
          ii = 0
          do j=1,n_tvs
             if( char4.eq.char4_f(j) ) ii=j  ! ii - ����� ������
          enddo
          if(ii .ne. 0) then                 ! ���������� ������� ������   
             cons_repl(i) = cons_repl(i) + 1 ! �������� �� ���������������
               else
               write(*,*) 
     >         '� ���������� ��� �������� ',char4_const(i),con_sost(nm_sost),
     >         '� ��� ��������������� ������'
               pause
               stop 2
          endif
        endif
        do j=1, 16  !   j - ������ ������������ ��������
           A_wrk(i,j) = cons_f(ii, 7*(j-1+(nm_sost-1)*16) + 1)
           B_wrk(i,j) = cons_f(ii, 7*(j-1+(nm_sost-1)*16) + 2)
           C_wrk(i,j) = cons_f(ii, 7*(j-1+(nm_sost-1)*16) + 3)
           D_wrk(i,j) = cons_f(ii, 7*(j-1+(nm_sost-1)*16) + 4)
           H_wrk(i,j) = cons_f(ii, 7*(j-1+(nm_sost-1)*16) + 5)
           V_wrk(i,j) = cons_f(ii, 7*(j-1+(nm_sost-1)*16) + 6)
           Z_wrk(i,j) = cons_f(ii, 7*(j-1+(nm_sost-1)*16) + 7)
        enddo
      enddo

      do i=1, n_ntk !   i - ����� ���� �������� ��������. ����������� ������
       itemp2 = 0
       do j=1,n_types
           if(char4_types(j).eq.char4_nf(i)) itemp2 = j
       enddo
       if(itemp2 .ne. 0) then
          nm_sost=nom_sost !����������� ���������� (in_contur=0) ������ � �����
          if(in_contur(itemp2) .eq. 1) nm_sost = nom_sost + 1 - i_kmpc  ! ����
          if(in_contur(itemp2) .eq. 2) nm_sost = nom_sost + 1 - i_kosuz ! �����
             else
             nm_sost = nom_sost + 1 - i_kosuz                           ! �����
       endif
       if(con_fill(i+n_tvs, nm_sost) .ne. 0) then
         ii = i
           else                             ! ������ ������������� � ����������
           char4 = char4_replace(i+n_tvs)   ! �������� �� ���������������
           ii = 0
           do j=1,n_ntk
              if(char4.eq.char4_nf(j)) ii=j ! ii - ����� ������
           enddo
           if(ii .ne. 0) then                    
              ij = i+ n_tvs                      ! ���������� ������� ������
              cons_repl(ij) = cons_repl(ij) + 1  ! �������� �� ���������������
                else
                write(*,*) 
     >          '� ���������� ��� �������� ',char4_const(i+n_tvs),con_sost(nm_sost),
     >          '� ��� ��������������� ������'
                pause
                stop 2
           endif
       endif
       P1_wrk(i) = cons_nf(ii, 5*(nm_sost-1) + 1)
       P2_wrk(i) = cons_nf(ii, 5*(nm_sost-1) + 2)
       D1_wrk(i) = cons_nf(ii, 5*(nm_sost-1) + 3)
       D2_wrk(i) = cons_nf(ii, 5*(nm_sost-1) + 4)
       SS_wrk(i) = cons_nf(ii, 5*(nm_sost-1) + 5)
      enddo

      return; end

      Subroutine cons_wrk1(char4, nexl, j_kmpc)
       include 'bc_vars.h'
       include 'bokr_var.h'
       character char4*4
       integer*4 nexl, j_kmpc
      if(n_ntk+nexl .gt. 45) then
        write(*,*) ' ����� ���������� � �������� ', nexl, ' �� ���������������'
        write(*,*) ' �������������/���������� ����� > ����������� ', 45-n_ntk
        write(7,*) ' ����� ���������� � �������� ', nexl, ' �� ���������������'
        write(7,*) ' �������������/���������� ����� > ����������� ', 45-n_ntk
        pause
        stop 2
      endif 

      if(i_sost == 1) then
         nm_sost = 2 + 1 - j_kmpc  !!!  ���� 1
      elseif(i_sost == 20) then
         nm_sost = 4 + 1 - j_kmpc  !!!  ���� 20
      elseif(i_sost == 284) then
         nm_sost = 6 + 1 - j_kmpc  !!!  ���� 284
      else
        write(*,*) ' ���������� � �������� �� �������������/����������'
        write(*,*) ' ����� � ����������� ���� 0 �����������'
        write(7,*) ' ���������� � �������� �� �������������/����������'
        write(7,*) ' ����� � ����������� ���� 0 �����������'
        pause
        stop 2
      endif

      do i=1, n_ntk !   i - ����� ���� �������� ��������. ����������� ������
       if(char4 .eq. char4_nf(i)) then
       if(con_fill(i+n_tvs, nm_sost) .ne. 0) then
         ii = i
           else                             ! ������ ������������� � ����������
           char4 = char4_replace(i+n_tvs)   ! �������� �� ���������������
           ii = 0
           do j=1,n_ntk
              if(char4.eq.char4_nf(j)) ii=j ! ii - ����� ������
           enddo
           if(ii .ne. 0) then
              ij = i+ n_tvs                      ! ���������� ������� ������
              cons_repl(ij) = cons_repl(ij) + 1  ! �������� �� ���������������
                else
                write(*,*) 
     >          '� ���������� ��� �������� ',char4_const(i+n_tvs),con_sost(nm_sost),
     >          '� ��� ��������������� ������'
                pause
                stop 2
           endif
       endif
       P1_wrk(n_ntk+nexl) = cons_nf(ii, 5*(nm_sost-1) + 1)
       P2_wrk(n_ntk+nexl) = cons_nf(ii, 5*(nm_sost-1) + 2)
       D1_wrk(n_ntk+nexl) = cons_nf(ii, 5*(nm_sost-1) + 3)
       D2_wrk(n_ntk+nexl) = cons_nf(ii, 5*(nm_sost-1) + 4)
       SS_wrk(n_ntk+nexl) = cons_nf(ii, 5*(nm_sost-1) + 5)
       char4_nf(n_ntk+nexl) = char4
       endif
      enddo

      return; end

      Subroutine convert_icon
       include 'bc_vars.h'
       include 'bokr_var.h'

      if(icon.eq.0) then
         i_sost=0; i_kmpc=1; i_kosuz=1

        elseif(icon.eq.1) then
         i_sost=1; i_kmpc=1; i_kosuz=1
        elseif(icon.eq.-1) then
         i_sost=1; i_kmpc=0; i_kosuz=1
        elseif(icon.eq.199) then
         i_sost=1; i_kmpc=1; i_kosuz=0
        elseif(icon.eq.-199) then
         i_sost=1; i_kmpc=0; i_kosuz=0

        elseif(icon.eq.20) then
         i_sost=20; i_kmpc=1; i_kosuz=1
        elseif(icon.eq.-20) then
         i_sost=20; i_kmpc=0; i_kosuz=1
        elseif(icon.eq.2099) then
         i_sost=20; i_kmpc=1; i_kosuz=0
        elseif(icon.eq.-2099) then
         i_sost=20; i_kmpc=0; i_kosuz=0

        elseif(icon.eq.284) then
         i_sost=284; i_kmpc=1; i_kosuz=1
        elseif(icon.eq.-284) then
         i_sost=284; i_kmpc=0; i_kosuz=1
        elseif(icon.eq.28499) then
         i_sost=284; i_kmpc=1; i_kosuz=0
        elseif(icon.eq.-28499) then
         i_sost=284; i_kmpc=0; i_kosuz=0

        else
         write(*,*) ' ������ � ����������� ����',icon,' ����������'
         write(7,*) ' ������ � ����������� ����',icon,' ����������'
         pause
         stop 2
      endif

      return; end
     

      Subroutine Print_nfc_sg0
!  ������ ��������, ����������� ��� ������� ������ �������� (sigma-0)
       include 'bc_vars.h'
       include 'bokr_var.h'
      n_cons = 0
      do ii=1,50 ! n_types
       if(n_tip(ii) .ne. 0) then
         i = tips2lib(ii)
         if(ii .le. nr) then                       ! ��������� �������� ����
           if(tip_sbor(ii).ne.'30� ') then
             call add_cons_zgr(i)
             write(7,'(1x,a4,7es14.6)') char4_f(i),
     >        A_wrk(i,1), B_wrk(i,1), C_wrk(i,1), D_wrk(i,1),
     >        H_wrk(i,1), V_wrk(i,1), Z_wrk(i,1)
           else
             do kkj=1,3
                i=tvsp_nom_cns(kkj)
                call add_cons_zgr(i)
                write(7,'(1x,a4,7es14.6)') char4_f(i),
     >           A_wrk(i,1), B_wrk(i,1), C_wrk(i,1), D_wrk(i,1),
     >           H_wrk(i,1), V_wrk(i,1), Z_wrk(i,1)
             enddo
           endif
           elseif(ii.gt.20 .and. ii.le.40) then    ! ������� ���
             call print_cons_suz(i,tip_sbor(ii))
             else                                  ! �� ��������� �������� ����
             call add_cons_zgr(i+n_tvs)
               write(7,'(1x,a4,7es14.6)') char4_nf(i),  0., 0.,
     >          p1_wrk(i), p2_wrk(i), d1_wrk(i), d2_wrk(i), ss_wrk(i)
         endif
       endif
      enddo

      write(7,'(a43,i3,a15)') 
     > '��� ������� �������� �������� ������������', n_cons,' ����� ��������'
      do i=1, n_cons
         ii = cons2zgr(i)
         if(cons_repl(ii) .ne. 0) then
           write(7,*) char4_const(ii), 
     >     ' !!! ��������� �������� �� ���������������  ', char4_replace(ii)
!         else
!           write(7,*) char4_const(ii), ' ��������� �������'
         endif
      enddo
      return; end

      Subroutine Print_nfc_pol
!  ������ �������� ���, ����������� ��� ������� ������ �������� (��������)
       include 'bc_vars.h'
       include 'bokr_var.h'

      n_cons = 0
      do ii=1,50 ! n_types
       if(n_tip(ii) .ne. 0) then
         if(tip_sbor(ii).ne.'30� ') then
           i = tips2lib(ii)
           if(ii .le. nr) then                  ! ��������� �������� ����
             write(7,'(1x,a4)') char4_f(i)
             do j=2, 16  !   j - ������ ������������ ��������
                write(7,'(5x,7es14.6)')
     >           A_wrk(i,j), B_wrk(i,j), C_wrk(i,j), D_wrk(i,j),
     >           H_wrk(i,j), V_wrk(i,j), Z_wrk(i,j)
             enddo
           endif
         else
           do kkj=1,3
             i=tvsp_nom_cns(kkj)
             write(7,'(1x,a4)') char4_f(i)
             do j=2, 16  !   j - ������ ������������ ��������
                write(7,'(5x,7es14.6)')
     >           A_wrk(i,j), B_wrk(i,j), C_wrk(i,j), D_wrk(i,j),
     >           H_wrk(i,j), V_wrk(i,j), Z_wrk(i,j)
             enddo
           enddo
         endif
       endif
      enddo
      return; end

      subroutine print_cons_suz(iii,char4)
       include 'bc_vars.h'
       include 'bokr_var.h'
      character*4 char4

      write(7,*) ' ��������� ��� ������� ����� ��. ���   ' ,char4
      i = ntip_pog(iii); call add_cons_zgr(i+n_tvs)
      write(7,'(a12,1x,a4,16x,5es14.6)') ' �����������',
     > char4_nf(i),  p1_wrk(i), p2_wrk(i), d1_wrk(i), d2_wrk(i), ss_wrk(i)
      i = ntip_vyt(iii); call add_cons_zgr(i+n_tvs)
      write(7,'(a12,1x,a4,16x,5es14.6)') ' �����������',
     > char4_nf(i),  p1_wrk(i), p2_wrk(i), d1_wrk(i), d2_wrk(i), ss_wrk(i)
      i = ntip_sht(iii); call add_cons_zgr(i+n_tvs)
      write(7,'(a12,1x,a4,16x,5es14.6)') ' ������     ',
     > char4_nf(i),  p1_wrk(i), p2_wrk(i), d1_wrk(i), d2_wrk(i), ss_wrk(i)
      i = ntip_chan(iii); call add_cons_zgr(i+n_tvs)
      write(7,'(a12,1x,a4,16x,5es14.6)') ' �����      ',
     > char4_nf(i),  p1_wrk(i), p2_wrk(i), d1_wrk(i), d2_wrk(i), ss_wrk(i)

      if(char4(:2) .eq. '��') then
        i = ntip_rvp1; if(char4(3:3) .eq. '0') i = ntip_rvp0
        call add_cons_zgr(i+n_tvs)
        write(7,'(a12,1x,a4,16x,5es14.6)') ' ����. ����.',
     >   char4_nf(i),  p1_wrk(i), p2_wrk(i), d1_wrk(i), d2_wrk(i), ss_wrk(i)
        i = ntip_rvl1; if(char4(3:3) .eq. '0') i = ntip_rvl0
        call add_cons_zgr(i+n_tvs)
        write(7,'(a12,1x,a4,16x,5es14.6)') ' ����.� ���.',
     >   char4_nf(i),  p1_wrk(i), p2_wrk(i), d1_wrk(i), d2_wrk(i), ss_wrk(i)
      endif

      return; end

      subroutine add_cons_zgr(i)
       include 'bc_vars.h'
       include 'bokr_var.h'
      itemp1 = 0
      do j=1, n_cons
         if(i .eq. cons2zgr(j) ) itemp1 = j
      enddo
      if(itemp1 .eq. 0) then
         n_cons =  n_cons + 1
         cons2zgr(n_cons) =  i
      endif
      return; end

      SUBROUTINE sech_tvs_lib(PK,QV,AS,AT,DSn,DTn,PSn,PTn,QS,KTIP, height)
! height - ���������� �� ����� �.�. �� ��������� ������ (��� < 0.0 ��� ����)
       include 'bc_vars.h'
       include 'bokr_var.h'
      character*4 char4
      DIMENSION R(15)

      R(1)=PK
      R(2)=PK*PK
      R(3)=R(2)*PK
      R(4)=R(3)*PK
      R(5)=QV
      R(6)=QV*QV
      R(7)=QV*PK
      R(8)=R(6)*QV
      R(9)=R(6)*PK
      R(10)=R(2)*QV
      R(11)=R(8)*QV
      R(12)=R(8)*PK
      R(13)=R(9)*PK
      R(14)=R(3)*QV
      R(15)=R(11)*QV

      K=tips2lib(KTIP)
      char4 = tip_sbor(ktip)
      AS= 0.0;      AT= 0.0
      PSn=0.0;      PTn=0.0
      DSn=0.0;      DTn=0.0
      QS= 0.0
      if(char4.ne.'30� ') then
        DO j=1,16
           if(j.eq.1) pk0=1.
           if(j.gt.1) PK0=R(j-1)
           AS=AS+a_wrk(k,j)*PK0
           AT=AT+b_wrk(k,j)*PK0
           PSn=PSn+c_wrk(k,j)*PK0
           PTn=PTn+d_wrk(k,j)*PK0
           DSn=DSn+h_wrk(k,j)*PK0
           DTn=DTn+v_wrk(k,j)*PK0
           QS=QS+z_wrk(k,j)*PK0
        enddo
      else
        zz1 = 0.
        do kkj=1,3
           k = tvsp_nom_cns(kkj)
           zz2 = zz1 + tvsp_size(kkj)
           temp1 = 0.
           if(height.gt.zz1 .and. height.le.zz2) temp1 = 1.0
           if(height.le.0.0) temp1 = part_wt(kkj)
           zz1 = zz2
           DO j=1,16
              if(j.eq.1) pk0=1.
              if(j.gt.1) pk0=r(j-1)
               pk0 = pk0 * temp1
               AS=AS+a_wrk(k,j)*PK0
               AT=AT+b_wrk(k,j)*PK0
               PSn=PSn+c_wrk(k,j)*PK0
               PTn=PTn+d_wrk(k,j)*PK0
               DSn=DSn+h_wrk(k,j)*PK0
               DTn=DTn+v_wrk(k,j)*PK0
               QS=QS+z_wrk(k,j)*PK0
           enddo
        enddo
      endif

      return; end

      subroutine sech_ntk_lib(i, k, nsuz)
!      ������ ������� ����������� �����
!      i    - ���������� ����� ������ � ������� 2488
!      k    - ��� ������
!      nsuz - ����� �������
       include 'bc_vars.h'
       include 'bokr_var.h'
       character*4 char4

      char4 = tip_sbor(k)
      if(k.gt.20 .and. k.le.40) then         ! �������� ���
        ll = tips2lib(k)
        l = k-20
        h_in_az = abs(hr(nsuz))-hsuz_up(ll)  ! abs �� ������ �����������.������
        if(char4(1:3) .eq. '���') then
         pk=hdh + h_in_az
         qa=pk+abs(sht_ln(ll))  !  ccbp
         bs=-abs(pog_ln(ll))
         bt=-abs(vyt_ln(ll))
          else
          pk=hdh + haz - h_in_az 
          qa=amax1(vyt_ypor(ll),pk-sht_ln(ll))  ! qqa(l),pk-ccbp(l))
          bs=pog_ln(ll)  ! ssp(l)
          bt=vyt_ln(ll)  ! ttp(l)
        endif
        if( char4(1:2) .eq. '��') then  ! ��.2477-00 ��� 01 (5 ����� �� ������)
          pk1 = qa + sht_ln(ll)
          pk2 = pk + rv_pog0
          temp1 = fz(qa-bt) ! ��� �����������
          temp2 = fz(qa)    ! ���� �����������
          temp3 = fz(pk)    ! ��� ���������� ����������� � ������������ ������
          temp4 = fz(pk1)   ! ���� ���������� ����������� � ������������ ������
          temp5 = fz(pk2)   ! ��� ��������� �����������
          temp6 = fz(pk+bs) ! ���� ��������� �����������
          ql1=temp4 - temp5 ! �������� �� ����.�����������
          ql2=temp3 - temp4 ! �������� �� ����.����������� � ������������ ������
          QV=temp5 - temp6  ! �������� �O �O��OT�TE��
          QA=temp1 - temp2  ! �������� �HTE�PA� �O B�TECH�TE��
          XS=temp2 - temp3  ! �������� �O �TAH�E
          PK=1.-QA-QV-XS    ! �������� �O BO�E
     >         -ql1 - ql2
          l1 = Ntip_pog(ll)
          l2 = Ntip_vyt(ll)
          l3 = Ntip_chan(ll)
          l4 = Ntip_sht(ll)
          l5 = ntip_rvp1; if(char4(3:3) .eq. '0') l5 = ntip_rvp0
          l6 = ntip_rvl1; if(char4(3:3) .eq. '0') l6 = ntip_rvl0
          DS(I)=1./(QV/D1_wrk(l1)+QA/D1_wrk(l2)+PK/D1_wrk(l3)+XS/D1_wrk(l4)+ql1/D1_wrk(l5)+ql2/D1_wrk(l6))
          DT(I)=1./(QV/D2_wrk(l1)+QA/D2_wrk(l2)+PK/D2_wrk(l3)+XS/D2_wrk(l4)+ql1/D2_wrk(l5)+ql2/D2_wrk(l6))
          PS(I)=    QV*P1_wrk(l1)+QA*P1_wrk(l2)+PK*P1_wrk(l3)+XS*P1_wrk(l4)+ql1*P1_wrk(l5)+ql2*P1_wrk(l6)
          PT(I)=    QV*P2_wrk(l1)+QA*P2_wrk(l2)+PK*P2_wrk(l3)+XS*P2_wrk(l4)+ql1*P2_wrk(l5)+ql2*P2_wrk(l6)
          ST(I)=   (QV*SS_wrk(l1)+QA*SS_wrk(l2)+PK*SS_wrk(l3)+XS*SS_wrk(l4)+ql1*SS_wrk(l5)+ql2*SS_wrk(l6))
        else                            ! �� ��.2477 (3 ���� �� ������)
          QI=FZ(PK)
          XS=FZ(QA)
          QV=ABS(FZ(PK+BS)-QI)          ! QV    - �O �O��OT�TE��
          QA=ABS(FZ(QA-BT)-XS)          ! QA    - �HTE�PA� �O B�TECH�TE��
          XS=ABS(QI-XS)                 ! XS    - �O �TAH�E
          PK=1.-QA-QV-XS                ! PK    - �O BO�E
          l1 = Ntip_pog(ll)
          l2 = Ntip_vyt(ll)
          l3 = Ntip_chan(ll)
          l4 = Ntip_sht(ll)
          DS(I)=1./(QV/D1_wrk(l1)+QA/D1_wrk(l2)+PK/D1_wrk(l3)+XS/D1_wrk(l4))
          DT(I)=1./(QV/D2_wrk(l1)+QA/D2_wrk(l2)+PK/D2_wrk(l3)+XS/D2_wrk(l4))
          PS(I)=    QV*P1_wrk(l1)+QA*P1_wrk(l2)+PK*P1_wrk(l3)+XS*P1_wrk(l4)
          PT(I)=    QV*P2_wrk(l1)+QA*P2_wrk(l2)+PK*P2_wrk(l3)+XS*P2_wrk(l4)
          ST(I)=   (QV*SS_wrk(l1)+QA*SS_wrk(l2)+PK*SS_wrk(l3)+XS*SS_wrk(l4))
        endif
          else
          kk = tips2lib(k)
          DS(I)=D1_wrk(kk)
          DT(I)=D2_wrk(kk)
          PS(I)=P1_wrk(kk)
          PT(I)=P2_wrk(kk)
          ST(I)=SS_wrk(kk)
      endif

      fs(i) = 0.
      ft(i) = 0.

      return; end

