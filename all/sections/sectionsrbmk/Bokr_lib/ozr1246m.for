      subroutine ozr1246m(ozrpr, ro_ster)
       include 'bokr_var.h'
       include 'bc_vars.h'
       include 'intvstu5.h'
      dimension l_krest(4), fmcp(71), fmsum(71), ro_ster(228), oo(3), f_corr(71), w_krest(228)
      character*8 work
      common /fi_ax_corr/ alfa2, alfa3, f_corr8(8)

      call start(baz_name)
      work = name_ctr(7)
      if(if_bibl.eq.0) work(3:4) = 'wd'
      if(if_bibl.eq.1) work(3:4) = 'wm'
      call proba(work,1,1,temp1884,1884,1,npo)
      work(3:4) = 'cr'
      call probpa(work,pase)
      call stop()

      temp1884 = temp1884
!      write(8,'(12f6.3)') temp1884
      call d12(temp1884, temp2488)

!      do i=1,1884
!         write(13,'(2i5,10f7.3)') i, nnyx(i), temp1884(i)
!      enddo

      hdhh = 0.3
      call garm_fi(nzb, oo, h_flux,hdhh)
!      write(*,'(f10.5)') oo
      alfa=3.14159/(haz+2*hdhh)
      do 50 k=1,71
      fmcp(k)=0.
      do 51 j=1,3
      fmcp(k)=fmcp(k)+oo(j)*sin(j*alfa*( (k-1)*0.1+hdhh) )
   51 continue
!      f_corr(k)=1.0 + alfa2 *   sin(2*alfa*((k-1)*0.1+hdhh))  ! Alreadu corrected h_flux
!     >              - alfa3 *   sin(3*alfa*((k-1)*0.1+hdhh))
!     >              + alfa3 *   sin(1*alfa*((k-1)*0.1+hdhh))
!      fmcp(k)=fmcp(k)*f_corr(k)
!      write(71,'(3f7.4)') (k-1)*0.1, fmcp(k)
   50 continue
      fmsum(1)=fmcp(1)**2
      do 52 k=2,71
   52 fmsum(k)=fmsum(k-1)+fmcp(k)**2
!      do k=1,71; write(8,'(f8.3)') fmsum(k); enddo

      nsuzz=0
      do i=1,mo
         kb_tip = kb(i)
         if(kb_tip.gt.20 .and. kb_tip.le.40) nsuzz=nsuzz+1
      enddo

      ll = 0; nomrod=0; ozrpr = 0.
      do i=1,irow
       do j=ibeg(i),iend(i)
        ll = ll+1
        kb_tip = kb(ll)
        if(kb_tip.gt.20 .and. kb_tip.le.40) then
          nomrod=1+nomrod
          l_krest(1)=itot(i-1)+j
          l_krest(2)=ll+1
          l_krest(3)=itot(i+1)+j
          l_krest(4)=ll-1
          ntvso = 0; wtvso=0.
          do k=1,4
             if(kb(l_krest(k)) .le. nr) then
                ntvso=ntvso+1
                wtvso=wtvso+temp2488(l_krest(k))
             endif
          enddo
          wcp=wtvso/ntvso;  w_krest(nomrod)=wcp
          kyxrod = nyx(ll)
          jhrod=nint(10*hr(nomrod))
          call sad1(jhrod,kyxrod,dr,kb_tip,wcp,fmsum,sro,nsuzz)
          ro_ster(nomrod) = dr
          ozrpr = ozrpr + dr
        endif
       enddo
      enddo

      ro_ster = ro_ster /	 sro
      ozrpr = ozrpr / sro

!      ll = 0; nomrod=0
!      write(72,*) '  � K-YX  ��� N���    ����     W_�����   ���_��.'
!      do i=1,mo
!        kb_tip = kb(i)
!        if(kb_tip.gt.20 .and. kb_tip.le.40) then
!          nomrod=1+nomrod
!          write(72,'(4i5,3f10.4)') nomrod, nyx(i), kb_tip, ntvso, hr(nomrod), w_krest(nomrod), ro_ster(nomrod)
!        endif
!      enddo

      return;    end

      subroutine garm_fi(nzb, o, h_flux,hdh)
      dimension hx(3), hz(3,3), o(3), h_flux(nzb)
      haz=7.0

      HEff=hdh+hdh+haz
      pi2heff=3.1415926536/HEff
      DO 6 I=1,3
      HX(I)=0.
      DO 6 II=1,3
    6 HZ(I,II)=0.
      ER=haz/nzb*pi2heff
      HH=hdh*pi2heff-ER*.5
      DO 7 II=1,nzb
      HH=HH+ER
      O(1)=SIN(HH)
      O(2)=SIN(HH+HH)
      O(3)=SIN(3.*HH)
      DO 7 I=1,3

      HX(I)=HX(I)+O(I)*h_flux(II)  ! �������� ���� h_flux ������ ==> ����
      DO 7 M=I,3
      HZ(I,M)=HZ(I,M)+O(I)*O(M)
    7 HZ(M,I)=HZ(I,M)

      CALL SIMQ(HZ,HX,3,M)
      o=hx

      return;    end

      subroutine sad1(jhrod,kyxrod,dr,kb_tip,wcp,fmsum,sro,nsuzz)
      dimension  fmsum(71), vesa(20), vesb(20)
      data vesa/ 211,  211,  211,  253,  211,  211,  243,  211,  211,  253, 10*211/
      data vesb/0.68, 0.68, 0.68, 0.66, 0.68, 0.68, 0.66, 0.68, 0.68, 0.73, 10*0.68/

      kb_tip=kb_tip-20
      bec= vesa(kb_tip) * nsuzz / 211
      ccy3=vesb(kb_tip) ! * nsuzz / 211

      jh=jhrod; if(jh.gt.70) jh=70
      if(kb_tip.eq.5) then
         if(jh.lt.40) then
            cc=1.-(1.-ccy3)/2.*(1.-cos(3.14159*jh/70.))
         else
            cc=1.-(1.-ccy3)/2.*(cos(3.14159*(jh/10.-4.09)/7.)-
     *      cos(3.14159*jh/70.))
         end if
      else
         cc=1.-(1.-ccy3)/2.*(1.-cos(3.14159*jh/70.))
      end if

!      write(72,'(2i4,5f10.3)') kb_tip+20, jh, wcp, wcp/cc, bec, ccy3, cc
!      write(8,'(2i4,4f10.3)') kb_tip, jh, wcp, bec,ccy3,cc
      wcp=    wcp/cc

      sro=sro+wcp**2*(fmsum(71)-fmsum(1))
      if(kb_tip.eq.5) then
        if(jh.gt.40) then
           dr=bec*wcp**2*(fmsum(112-jh)-fmsum(71-jh))
        else
           dr=bec*wcp**2*(fmsum(71)-fmsum(71-jh))
        end if
      else
           dr=bec*wcp**2*(fmsum(jh+1)-fmsum(1))
      end if

      return;    end

      subroutine sad2(jhrod,kyxrod,dr,kb_tip,wcp,fmsum,sro,nsuzz)
      dimension  fmsum(71), vesa(20), vesb(20)
      data vesa/ 211,  211,  211,  253,  211,  211,  238,  211,  211,  211, 10*211/  !  VNIIAES
      data vesb/0.75, 0.75, 0.75, 0.66, 0.75, 0.75, 0.72, 0.75, 0.75, 0.76, 10*0.75/ !  VNIIAES

      kb_tip=kb_tip-20
      bec= vesa(kb_tip) * nsuzz / 211
      ccy3=vesb(kb_tip) ! * nsuzz / 211

      jh=jhrod; if(jh.gt.70) jh=70
      if(kb_tip.eq.5) then
         if(jh.lt.40) then
            cc=1.-(1.-ccy3)/2.*(1.-cos(3.14159*jh/70.))
         else
            cc=1.-(1.-ccy3)/2.*(cos(3.14159*(jh/10.-4.09)/7.)-
     *      cos(3.14159*jh/70.))
         end if
      else
         cc=1.-(1.-ccy3)/2.*(1.-cos(3.14159*jh/70.))
      end if

!      write(8,'(2i4,4f10.3)') kb_tip, jh, wcp, bec,ccy3,cc
      wcp=    wcp/cc

      sro=sro+wcp**2*(fmsum(71)-fmsum(1))
      if(kb_tip.eq.5) then
        if(jh.gt.40) then
           dr=bec*wcp**2*(fmsum(112-jh)-fmsum(71-jh))
        else
           dr=bec*wcp**2*(fmsum(71)-fmsum(71-jh))
        end if
      else
           dr=bec*wcp**2*(fmsum(jh+1)-fmsum(1))
      end if

      return;    end

      subroutine sad3(jhrod,kyxrod,dr,kb_tip,wcp,fmsum,sro,nsuzz)
      dimension  fmsum(71), vesa(20), vesb(20)
! ���� �������� ���������������� �������
!     data vesa/ 211,  211,  211,  253,  211,  211,  238,  211,  211,  211, 10*211/  !  VNIIAES
!     data vesa/ 238,  238,  238,  253,  261,  238,  243,  238,  238,  225, 10*211/
      data vesa/ 198,  198,  198,  253,  257,  198,  233,  198,  198,  228, 10*198/
      data vesb/0.68, 0.68, 0.68, 0.66, 0.68, 0.68, 0.66, 0.68, 0.68, 0.73, 10*0.68/

      kb_tip=kb_tip-20
      bec= vesa(kb_tip) * nsuzz / 211
      ccy3=vesb(kb_tip) ! * nsuzz / 211

      jh=jhrod; if(jh.gt.70) jh=70
      if(kb_tip.eq.5) then
         if(jh.lt.40) then
            cc=1.-(1.-ccy3)/2.*(1.-cos(3.14159*jh/70.))
         else
            cc=1.-(1.-ccy3)/2.*(cos(3.14159*(jh/10.-4.09)/7.)-
     *      cos(3.14159*jh/70.))
         end if
      else
         cc=1.-(1.-ccy3)/2.*(1.-cos(3.14159*jh/70.))
      end if

!      write(8,'(2i4,4f10.3)') kb_tip, jh, wcp, bec,ccy3,cc
      wcp=    wcp/cc

      sro=sro+wcp**2*(fmsum(71)-fmsum(1))
      if(kb_tip.eq.5) then
        if(jh.gt.40) then
           dr=bec*wcp**2*(fmsum(112-jh)-fmsum(71-jh))
        else
           dr=bec*wcp**2*(fmsum(71)-fmsum(71-jh))
        end if
      else
           dr=bec*wcp**2*(fmsum(jh+1)-fmsum(1))
      end if

      return;    end

