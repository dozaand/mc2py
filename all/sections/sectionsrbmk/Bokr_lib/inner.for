      SUBROUTINE INNERR(A,B,C,D,R,P,Q,F,MJ,LI)
      DIMENSION FF(60),
     *A(2488),B(2488),C(2488),D(2488),R(2488),P(2488),Q(2488),F(2488)
      COMMON /GEOM/IR,IB(56),IE(56),IT(56),MO
      DATA N/56/,FF/60*0./
      IF(LI.NE.0) GO TO 7
      DO 1 I=1,N
      JB=IB(I)
      JE=IE(I)
      L=IT(I)+JB
      A(L)=0.
      B(L)=0.
      C(L)=0.
      L2=L
      JB=JB+1
      DO 2 J=JB,JE
      L=IT(I)+J
      B(L)=R(L)*R(L2)/(R(L)+R(L2))
      D(L2)=B(L)
      P(L)=P(L)+B(L)-R(L)
      P(L2)=P(L2)+D(L2)-R(L2)
      A(L)=0.
      C(L)=0.
    2 L2=L
    1 D(L)=0.
      DO 3 I=2,N
      I1=I-1
      JB=MAX0(IB(I1),IB(I))
      JE=MIN0(IE(I1),IE(I))
      DO 4 J=JB,JE
      L1=IT(I1)+J
      L=IT(I)+J
      A(L)=R(L)*R(L1)/(R(L)+R(L1))
      C(L1)=A(L)
      P(L)=P(L)+A(L)-R(L)
    4 P(L1)=P(L1)+C(L1)-R(L1)
    3 CONTINUE
      DO 15 I=1,N
      JB=IB(I)
      JE=IE(I)
      DO 16 J=JB,JE
      L=IT(I)+J
      R(L)=1./(P(L)+R(L)+R(L)+R(L)+R(L)-YLL*B(L))
      YLL=R(L)*D(L)
      A(L)=A(L)*R(L)
      B(L)=B(L)*R(L)
      C(L)=C(L)*R(L)
   16 D(L)=YLL
   15 CONTINUE
    7 DO 10 NI=1,MJ
      IF(NI-NI/2*2.EQ.0) GO TO 17
      DO 8 I=1,N
      JB=IB(I)
      JE=IE(I)
      DO 9 J=JB,JE
      L=IT(I)+J
      YLL=Q(L)*R(L)+A(L)*FF(J)
      P(L)=YLL
      IF(C(L).NE.0.) YLL=YLL+C(L)*F(IT(I+1)+J  )
      Z=B(L)*Z+YLL
    9 FF(J)=Z
      J=JE
      DO 11 JJ=JB,JE
      Z=D(L)*Z+FF(J)
      F(L  )=Z
      FF(J)=Z
      J=J-1
   11 L=L-1
    8 CONTINUE
      GO TO 10
   17 DO 12 II=1,N
      I=N+1-II
      JB=IB(I)
      JE=IE(I)
      DO 13 J=JB,JE
      L=IT(I)+J
      Z=B(L)*Z+P(L)+C(L)*FF(J)
   13 FF(J)=Z
      J=JE
      DO 14 JJ=JB,JE
      Z=D(L)*Z+FF(J)
      F(L  )=Z
      FF(J)=Z
      J=J-1
   14 L=L-1
   12 CONTINUE
   10 CONTINUE
      RETURN
      END
