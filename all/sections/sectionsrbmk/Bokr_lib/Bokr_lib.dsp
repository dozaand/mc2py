# Microsoft Developer Studio Project File - Name="Bokr_lib" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=Bokr_lib - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Bokr_lib.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Bokr_lib.mak" CFG="Bokr_lib - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Bokr_lib - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "Bokr_lib - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "Bokr_lib - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /nologo /warn:nofileopt
# ADD F90 /compile_only /extend_source:132 /include:"../declare" /nologo /optimize:0 /warn:nofileopt
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386 /nodefaultlib:"libcd.lib"

!ELSEIF  "$(CFG)" == "Bokr_lib - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /dbglibs /debug:full /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /check:bounds /compile_only /dbglibs /debug:full /extend_source:132 /fpscomp:ioformat /include:"../declare" /list /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib /nologo /subsystem:console /incremental:no /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Bokr_lib - Win32 Release"
# Name "Bokr_lib - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=.\BKSAM.FOR
DEP_F90_BKSAM=\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# Begin Source File

SOURCE=.\BOKRBA.FOR
DEP_F90_BOKRB=\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# Begin Source File

SOURCE=.\BUSP01.FOR
DEP_F90_BUSP0=\
	"..\Declare\BC_vars.h"\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# Begin Source File

SOURCE=.\Corr_sec.for
DEP_F90_CORR_=\
	"..\Declare\BC_vars.h"\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# Begin Source File

SOURCE=".\FZ$.FOR"
DEP_F90_FZ__F=\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# Begin Source File

SOURCE=.\inner.for
# End Source File
# Begin Source File

SOURCE=".\ITERD$.FOR"
DEP_F90_ITERD=\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# Begin Source File

SOURCE=.\READ_BDB.FOR
DEP_F90_READ_=\
	"..\Declare\BC_vars.h"\
	"..\Declare\BOKR_var.h"\
	"..\Declare\INTVSTU5.H"\
	
# End Source File
# Begin Source File

SOURCE=.\READ_FIL.FOR
DEP_F90_READ_F=\
	"..\Declare\BC_vars.h"\
	"..\Declare\BOKR_var.h"\
	"..\Declare\INTVSTU5.H"\
	
# End Source File
# Begin Source File

SOURCE=.\ReadAsci.for
DEP_F90_READA=\
	"..\Declare\BC_vars.h"\
	"..\Declare\BOKR_var.h"\
	"..\declare\mku_sost.h"\
	
# End Source File
# Begin Source File

SOURCE=.\RODS.FOR
DEP_F90_RODS_=\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# Begin Source File

SOURCE=.\Sech_lib.for
DEP_F90_SECH_=\
	"..\Declare\BC_vars.h"\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# Begin Source File

SOURCE=.\TABLBOKP.FOR
DEP_F90_TABLB=\
	"..\Declare\INTVSTU5.H"\
	
# End Source File
# Begin Source File

SOURCE=.\type.for
# End Source File
# Begin Source File

SOURCE=.\WPREB01.FOR
DEP_F90_WPREB=\
	"..\Declare\BC_vars.h"\
	"..\Declare\BOKR_var.h"\
	
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=..\Declare\BC_vars.h
# End Source File
# Begin Source File

SOURCE=..\Declare\BOKR_var.h
# End Source File
# Begin Source File

SOURCE=..\Declare\INTVSTU5.H
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=..\Declare\WTOS.OBJ
# End Source File
# Begin Source File

SOURCE=..\Declare\dlib.lib
# End Source File
# End Target
# End Project
