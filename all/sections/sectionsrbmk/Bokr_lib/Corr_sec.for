!-----------------------------------------------------------------------------!
!---��� ������������� �������� TVS--------------------------------------------!
      subroutine Sig_tvs_corr(qv,as,at,ds_,dt_,bs,bt,qs,tip_tvs )
       include 'bc_vars.h'
       include 'bokr_var.h'

        character*4 charr4, tip_tvs, char
        common/corr_tvs/ en_vyr1(30),
     >  corr_sf1(4,30),corr_sf2(4,30),corr_sa1(4,30),corr_sa2(4,30),
     >  corr_d1(4,30), corr_d2(4,30),corr_s12(4,30),
     >  iread1, charr4(4)
        data iread1 /0/

!        return

      if( iread1 .eq. 0) then
      open(1, file='SigmaTVS')
      do j=1,4
        read(1,'(a4)', end=1) charr4(j)
        read(1,'(a4)', end=1) char
        do i=1,30
        read(1,*, end=1) en_vyr1(i), corr_sf1(j,i),corr_sf2(j,i),
     >  corr_sa1(j,i),corr_sa2(j,i),corr_d1(j,i),corr_d2(j,i),corr_s12(j,i)  
        enddo
      enddo
1      close(1)
      iread1 = 1
      endif

      do j=1,4

         if(charr4(j) .eq. tip_tvs) then
         do i=1,30
         if( qv .lt. en_vyr1(i) ) then
           as = as * corr_sf1(j,i)
           at = at * corr_sf2(j,i)
           bs = bs * corr_sa1(j,i)
           bt = bt * corr_sa2(j,i)
           ds_= ds_*  corr_d1(j,i)
           dt_= dt_*  corr_d2(j,i)
           qs = qs * corr_s12(j,i)
           exit
         endif
         enddo
         endif
      enddo

      return; end

!-----------------------------------------------------------------------------!
!---��� ������������� �������� �������� �������� �������� � ������� ��������--!
      subroutine cons_wrk_change
      use dflib
       include 'bc_vars.h'
       include 'bokr_var.h'
        character char4*4

!        return

      write(*,*) '  �������� � �������� �������� ��������:'
      write(*,*) '  nSF1    nSF2     SA1     SA2     D1      D2      S12  '
      write(7,*) '  �������� � �������� �������� ��������:'
      write(7,*) '  nSF1    nSF2     SA1     SA2     D1      D2      S12  '
      open(1, file='sigma_0')

 5    continue
      read(1,'(a4)', end=10) char4
!      read(1,*, end=10) temp1  
!      read(1,*, end=10) temp2  
!      read(1,*, end=10) temp3  
!      read(1,*, end=10) temp4  
!      read(1,*, end=10) temp5  
!      read(1,*, end=10) temp6  
!      read(1,*, end=10) temp7  
      temp1=0.0
      temp2=0.0
      temp3=0.0
      temp4=0.0
      temp5=0.0
      temp6=0.0
      temp7=0.0

      read(1,*, end=10) corr1  
      read(1,*, end=10) corr2  
      read(1,*, end=10) corr3  
      read(1,*, end=10) corr4  
      read(1,*, end=10) corr5  
      read(1,*, end=10) corr6  
      read(1,*, end=10) corr7  

      call random_seed()
      call random_number(rand_val); rand_val1 = (0.5-rand_val)*2.
      rez_bokr(9)  = (rand_val1 * temp1 + 1.) * corr1
      call random_number(rand_val); rand_val1 = (0.5-rand_val)*2.
      rez_bokr(10) = (rand_val1 * temp2 + 1.) * corr2
      call random_number(rand_val); rand_val1 = (0.5-rand_val)*2.
      rez_bokr(11) = (rand_val1 * temp3 + 1.) * corr3
      call random_number(rand_val); rand_val1 = (0.5-rand_val)*2.
      rez_bokr(12) = (rand_val1 * temp4 + 1.) * corr4
      call random_number(rand_val); rand_val1 = (0.5-rand_val)*2.
      rez_bokr(13) = (rand_val1 * temp5 + 1.) * corr5
      call random_number(rand_val); rand_val1 = (0.5-rand_val)*2.
      rez_bokr(14) = (rand_val1 * temp6 + 1.) * corr6
      call random_number(rand_val); rand_val1 = (0.5-rand_val)*2.
      rez_bokr(15) = (rand_val1 * temp7 + 1.) * corr7

      do i=1, n_tvs !   i - ����� ���� �������� ��������. ������ � ���
        if( char4 .eq. char4_f(i) ) then
         j = 1
         do j=1, 16  !   j - ������ ������������ ��������
           A_wrk(i,j) = A_wrk(i,j) * rez_bokr(9) 
           B_wrk(i,j) = B_wrk(i,j) * rez_bokr(10)
           C_wrk(i,j) = C_wrk(i,j) * rez_bokr(11)
           D_wrk(i,j) = D_wrk(i,j) * rez_bokr(12)
           H_wrk(i,j) = H_wrk(i,j) * rez_bokr(13)
           V_wrk(i,j) = V_wrk(i,j) * rez_bokr(14)
           Z_wrk(i,j) = Z_wrk(i,j) * rez_bokr(15)
         enddo
        endif
      enddo

      do i=1, n_ntk !   i - ����� ���� �������� ��������. ����������� ������
       if(char4 .eq. char4_nf(i)) then
          P1_wrk(i) = P1_wrk(i) * rez_bokr(11)
          P2_wrk(i) = P2_wrk(i) * rez_bokr(12)
          D1_wrk(i) = D1_wrk(i) * rez_bokr(13)
          D2_wrk(i) = D2_wrk(i) * rez_bokr(14)
          SS_wrk(i) = SS_wrk(i) * rez_bokr(15)
       endif
      enddo

      write(*,'(a4,1x,7f8.5)') char4, (rez_bokr(i), i=9, 15)
      write(7,'(a4,1x,7f8.5)') char4, (rez_bokr(i), i=9, 15)
      write(11,'(a4,1x,7f8.5,i5,2i2)') char4, (rez_bokr(i), i=9, 15), i_sost, i_kmpc, i_kosuz

      goto 5

10    close(1)

      return; end
!--------------------------------------------------------------------------


