      subroutine read_fil_bb
       include 'bokr_var.h'
       include 'bc_vars.h'
       include 'intvstu5.h'
      integer*4 nnf(880), naim(10), nm(2488), kashb(17)
      real*4    HP(200),HY(32),HA(3)
      character ch_aes(5), ch_block(5)
      data ch_aes, ch_block/'K','C','S','L','I','A','B','C','D','E'/
      DATA KASHB   ! ����� ������ �� ����� ��������� � ������ ����� ��������
     > /57,60,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85/

      equivalence (nnf(1), t(1)),  (nm(1), s(1))

      read(9,rec=5) nnf

      aes = ch_aes(nnf(570)); unit = ch_block(nnf(571))

      ihour =  nnf(576) / 10000
      imin  = (nnf(576) - ihour * 10000) / 100
      isec  =  nnf(576) - ihour * 10000 - imin * 100
      write(pase,'(i2,a1,i2,a1,i2,a3,i2,a1,i2,a1,i2,a1)')
     > nnf(573),'/',nnf(574),'/',nnf(572),'   ',ihour,':',imin,':',isec,'\0'C
      if( nnf(573) .le. 9 ) pase(1:1) = '0'
      if( nnf(574) .le. 9 ) pase(4:4) = '0'
      write(7,*) '�������� ��������      ', pase
      write(7,*) '��������������� ���    ', pase
      write(7,*) '������� ��������       ', pase
      write(7,*) '��������� PRE          ', pase
      write(7,*) '��p��.��p����p. BOKPA  ', pase
      write(7,*) '��������� ���          ', pase

      icon  =nnf(821)
      call  convert_icon
      IROD  =nnf(822)      ! �� ������������
      NPOPR =0             ! �� ����� ��� ��������
      iprint=nnf(493)

      READ (9,REC=23) (NM(I),I=1,880)
      READ (9,REC=24) (NM(I),I=881,1760)
      READ (9,REC=25) (NM(I),I=1761,2488)
      DO 520 I=1,2488
520   Kb(I)=NM(I)
      kb(106 ) = 11  !  ������ �������
      kb(752 ) = 11  !  ������ �������
      kb(1683) = 11  !  ������ �������
      kb(2383) = 11  !  ������ �������

!  ��������� ������������ ����� �������� ��������� ����������
!  ��������� ������������ ��������� �������� �.�. �������� ���������� ����-����
      call check_tips
!  ������� ��������� ����-���� ��������������  i_sost, in_kmpc, in_kosuz
      call cons_wrk

!---��� ������������� �������� �������� �������� �������� � ������� ��������--!
!      call cons_wrk_change

      IRR=NNF(568)
      READ (9,REC=IRR  ) (P(I),I=1,880)
      READ (9,REC=IRR+1) (P(I),I=881,1760)
      READ (9,REC=IRR+2) (P(I),I=1761,2488)
      h_flux = 0.
      nzb=nnf(482)
      DO 503 I=1,nzb
503   h_flux(I)=t(461+nzb+1-I)

      pr=t(489)

      TI=t(487)
      BZ=bz_bokr  !  t(490)
      q=t(512)
      NI=nnf(510)
      NW=nnf(491)
      eps_f=amin1(t(485), eps_fmax)
      eps_k=amin1(t(486), eps_kmax)

      DO I=1,nr
       if(n_tip(i).ne.0) then
          Rk_wnom(I) = w_nom(tips2lib(i), nom_sost)
          uc(i) = uc_tvs(tips2lib(i))     !  t(I+34)
       endif
      enddo

      DO I=21, 30
       if(n_tip(i).ne.0) then
          NAIM(I-20)=nnf(512+I)          ! �� ������������
          SSP(I-20) =pog_ln(tips2lib(i))
          TTP(I-20) =vyt_ln(tips2lib(i))
          CCBP(I-20)=sht_ln(tips2lib(i))
          CONC(I-20)=hsuz_up(tips2lib(i))
          QQA(I-20) = vyt_ypor(tips2lib(i))

          NPG(I-20)=ntip_pog(tips2lib(i))
          NBT(I-20)=ntip_vyt(tips2lib(i))
          NSH(I-20)=ntip_sht(tips2lib(i))
          NBO(I-20)=ntip_chan(tips2lib(i))
       endif
      enddo

      READ (9,REC=3) NNF
      DO 10 K=1,200
   10 HP(K)=NNF(K+124)/10.
      DO 20 K=1,32
   20 HY(K)=NNF(K+316)/10.
      DO 30 K=1,3
   30 HA(K)=NNF(K+348)/10.

      NRU=0
      NP=0
      NY=0
      NA=0
      DO 100 I=1,2488
      NT=kb(I)
      IF(NT.GT.30.OR.NT.LE.20) GO TO 100
      NP=NP+1
      IF(NT.EQ.24.OR.NT.GT.25) THEN
        Nru=Nru+1
        HR(NP)=HP(Nru)
        ELSEIF(NT.EQ.25) THEN
          NY=NY+1
          HR(NP)=HY(NY)
          ELSE
            NA=NA+1
            HR(NP)=HA(NT-20)
      ENDIF
  100 CONTINUE
      NA=NA/4

      do i=1,30
          type1(i) = adjustr(tip_sbor(i))
      end do

      return; end

