      subroutine read_fil_asc
       include 'bokr_var.h'
       include 'bc_vars.h'
       include 'mku_sost.h'

      integer*4 nnf(880), naim(10), nm(2488), kashb(17)
      real*4    HP(200),HY(32),HA(3)

      equivalence (nnf(1), t(1)),  (nm(1), s(1))

       open(1, file='mku_dat')

       read(1,'(a1)')  aes_
       read(1,'(a1)')  unit_
       read(1,*)       nyear_
       read(1,*)       nmonth_
       read(1,*)       nday_
!       read(1,*)       nhour_
!       read(1,*)       nmin_
!       read(1,*)       n_sec_
       read(1,*)       in_kmpc
       read(1,*)       in_kosuz
       read(1,*)       t_kmpc
       read(1,*)       t_grafit
       read(1,*)       time_shd
       read(1,*)       wstat_shd
       read(1,*)       if_reload
       read(1,*)       kod_docum
       read(1,*)       roizm_suz
       read(1,*)       roizm_all
       read(1,*)       roizm_az
!       read(1,'(a8)')  date_ks
!       read(1,'(a16)') name_dat1
!       read(1,'(a16)') name_dat2
!       read(1,*)       kb_
!       read(1,*)       p_
!       read(1,*)       hr_
!	 do i=1,50
!       read(1,*,end=1) tip_sbor_(i), n_tip_(i)
!       enddo
1      close(1)

       if((in_kmpc .eq. 1 .and. in_kosuz .eq. 1) .or.       ! ��� ��������
     >    (in_kmpc .eq. 0 .and. in_kosuz .eq. 1) .or.
     >    (in_kmpc .eq. 0 .and. in_kosuz .eq. 0) .or.
     >    (in_kmpc .eq. 1 .and. in_kosuz .eq. 0)  ) then
          i_sost  = 20
       else
          write(*,*) 'in_kmpc =',in_kmpc,';     in_kosuz =',in_kosuz
          write(*,*) 'in_kmpc =',in_kmpc,';     in_kosuz =',in_kosuz
          pause; stop
       endif

       icon  = i_sost
       i_kmpc = in_kmpc
       i_kosuz = in_kosuz

       NPOPR = 0             ! ������� ����� �������� � ���������� �������
       iprint= 111111        ! ������� ������

       aes = aes_
       unit = unit_
      READ (9,REC=23) (NM(I),I=1,880)
      READ (9,REC=24) (NM(I),I=881,1760)
      READ (9,REC=25) (NM(I),I=1761,2488)
      DO 520 I=1,2488
520   Kb(I)=NM(I)
!       kb = kb_
        kb(106 ) =  11 !  ������ �������
        kb(752 ) =  11 !  ������ �������
        kb(1683) =  11 !  ������ �������
        kb(2383) =  11 !  ������ �������
      IRR=13  !  NNF(568)
      READ (9,REC=IRR  ) (P(I),I=1,880)
      READ (9,REC=IRR+1) (P(I),I=881,1760)
      READ (9,REC=IRR+2) (P(I),I=1761,2488)
!       p = p_

!  ��������� ������������ ����� �������� ��������� ����������
!  ��������� ������������ ��������� �������� �.�. �������� ���������� ����-����
      call check_tips
!  ������� ��������� ����-���� �������������� i_sost, in_kmpc, in_kosuz
      call cons_wrk

!---��� ������������� �������� �������� �������� �������� � ������� ��������--!
!      call cons_wrk_change

      h_flux = 0.
      nzb=7
      h_flux(1) = 1.9
      h_flux(2) = 2.5
      h_flux(3) = 1.7
      h_flux(4) = 0.6
      h_flux(5) = 0.3
      h_flux(6) = 0.2
      h_flux(7) = 0.15

      pr=0.01
      do I=1,2488
         if( kb(i) .le. nr )  pr = pr + 1
      enddo

      TI=1.
      BZ=bz_bokr
      q=0.85
      NI=9
      NW=9 ! nnf(2)
      eps_f = eps_fmax
      eps_k = eps_kmax

      DO I=1,nr
       if(n_tip(i).ne.0) then
          Rk_wnom(I) = w_nom(tips2lib(i), nom_sost)
          uc(i) = uc_tvs(tips2lib(i))     !  t(I+34)
       endif
      enddo

      DO I=21,30
       if(n_tip(i).ne.0) then
          NAIM(I-20)=nnf(512+I)          ! �� ������������
          SSP (I-20) = pog_ln(tips2lib(i))
          TTP (I-20) = vyt_ln(tips2lib(i))
          CCBP(I-20) = sht_ln(tips2lib(i))
          CONC(I-20) = hsuz_up(tips2lib(i))
          QQA (I-20) = vyt_ypor(tips2lib(i))

          NPG(I-20)=ntip_pog(tips2lib(i))
          NBT(I-20)=ntip_vyt(tips2lib(i))
          NSH(I-20)=ntip_sht(tips2lib(i))
          NBO(I-20)=ntip_chan(tips2lib(i))
       endif
      enddo

      READ (9,REC=3) NNF
!      DO 10 K=1,200         ! 1_SUZ_Massiv
!   10 HP(K)=NNF(K+124)/10.
!      DO 20 K=1,32
!   20 HY(K)=NNF(K+316)/10.
!      DO 30 K=1,3
!   30 HA(K)=NNF(K+348)/10.  ! 1_SUZ_Massiv

      NRU=0
      NP=0
      NY=0
      NA=0
      DO 100 I=1,2488
      NT=kb(I)
      IF(NT.GT.30.OR.NT.LE.20) GO TO 100
      NP=NP+1
      IF(NT.EQ.24.OR.NT.GT.25) THEN
        Nru=Nru+1
 ! 1_SUZ_Massiv!        HR(NP)=HP(Nru)
        ELSEIF(NT.EQ.25) THEN
          NY=NY+1
 ! 1_SUZ_Massiv!          HR(NP)=HY(NY)
          ELSE
            NA=NA+1
 ! 1_SUZ_Massiv!            HR(NP)=HA(NT-20)
      ENDIF
      HR(NP)=NNF(NP+124)/10.     ! 1_SUZ_Massiv
  100 CONTINUE
      NA=NA/4

!      np=0               !  ����� �������� � ��������
!      do i=1,2488
!      if(kb(i).gt.20) then
!         np=np+1
!         hr(np)=NM(np)/10.
!      endif
!      end do

      do i=1,30
          type1(i) = adjustr(tip_sbor(i))
      end do

      return; end

      subroutine read_fil_asc1(file_name)
       include 'bokr_var.h'
       include 'bc_vars.h'

      character file_name*256
      integer*4 nnf(880), naim(10), nm(2488), kashb(17)
      real*4    HP(200),HY(32),HA(3)

      equivalence (nnf(1), t(1)),  (nm(1), s(1))

      open(3, file=file_name)
        read(3,*) (kb(i), i=1,2488)
        kb(106 ) =  11 !  ������ �������
        kb(752 ) =  11 !  ������ �������
        kb(1683) =  11 !  ������ �������
        kb(2383) =  11 !  ������ �������
        read(3,*) p
        read(3,*) hr
        nzb=7
        read(3,*) (h_flux(i), i= 1,7)
        read(3,*) pr
        read(3,'(i1,6x,i2,1x,i2,1x,i4,2x,i2,1x,i2)') nblock,nday,nmonth,nyear,nhour,nmin
1     close(3)

      aes='L'; naes=4
      if(nblock.eq.1)  unit='A'
      if(nblock.eq.2)  unit='B'
      if(nblock.eq.3)  unit='C'
      if(nblock.eq.4)  unit='D'
      in_kmpc=1
      in_kosuz=1
      i_sost  = 0
      icon  = i_sost
      i_kmpc = in_kmpc
      i_kosuz = in_kosuz
      NPOPR = 0             ! ������� ����� �������� � ���������� �������
      iprint= 111111        ! ������� ������
      TI=1.
      BZ=bz_bokr
      q=0.85
      NI=9
      NW=9
      eps_f = eps_fmax
      eps_k = eps_kmax


!  ��������� ������������ ����� �������� ��������� ����������
!  ��������� ������������ ��������� �������� �.�. �������� ���������� ����-����
      call check_tips
!  ������� ��������� ����-���� �������������� i_sost, in_kmpc, in_kosuz
      call cons_wrk

      DO I=1,nr
       if(n_tip(i).ne.0) then
          Rk_wnom(I) = w_nom(tips2lib(i), nom_sost)
          uc(i) = uc_tvs(tips2lib(i))     !  t(I+34)
       endif
      enddo

      do i=1,mo
         if(kb(i) .le. nr) p(i) = p(i)/uc(kb(i))
      enddo

      DO I=21,30
       if(n_tip(i).ne.0) then
          NAIM(I-20)=nnf(512+I)          ! �� ������������
          SSP (I-20) = pog_ln(tips2lib(i))
          TTP (I-20) = vyt_ln(tips2lib(i))
          CCBP(I-20) = sht_ln(tips2lib(i))
          CONC(I-20) = hsuz_up(tips2lib(i))
          QQA (I-20) = vyt_ypor(tips2lib(i))

          NPG(I-20)=ntip_pog(tips2lib(i))
          NBT(I-20)=ntip_vyt(tips2lib(i))
          NSH(I-20)=ntip_sht(tips2lib(i))
          NBO(I-20)=ntip_chan(tips2lib(i))
       endif
      enddo

      do i=1,30
         type1(i) = adjustr(tip_sbor(i))
      end do

      return; end


      subroutine wri_txt1
      include 'bokr_var.h'

      open(1, file='W_bokr.txt')
        write(1,'(10f7.4)') wk*pr
      close(1)

      return; end

