#include <IO.H>
#include <sys\types.h>
#include <sys\stat.h>
#include <STDIO.H>
#include <STDLIB.H>
#include <MALLOC.H>
#include <memory.h>
#include <string.h>
#include <DOS.H>

#define PI 3.14159
#define MAXKAT 30
#define MAX 614
#define logg

int ALBL(short);

int NBL ;    /* ����� ��������*/
short int MAXBLK;   /* ������������ ����� ������ �� ����� */
short int NUKAT;
/*                                         
   BUST- ������������� ��������� ������ �����

   NUKAT �������� ����� ����� ������������ ���������*/

typedef unsigned short int rad50;
typedef
struct KAT		                      /* �������*/
        {
	rad50 NAMTOS[3];                  /* ��� tos �*/
    int NBTOS;                        /* ����� ����� ������ tos �*/
   	unsigned short int NOBJ;		  /* ����� �������� */
   	unsigned short int NCB;		      /* ����� ������� */
   	unsigned char FLATOS;	          /* ���� ���������*/
	  /*
	    0- �����p�;
	    1- ����� ��������;
	    2- ����� �������;
	    3- �����;
	    4- 0 - ��������, 1 - �����;
	    5- ������
	    6- �p������� ���	*/
        } KATALOG; /* ����� : 20 ����. � ����� 25 ���������
//� 11 K 1228 ���������       */

short int *BUST;
KATALOG *UK;                 /* ��������� �� ������� */
KATALOG *UKAT;               /* ��������� �� ������� */
float *MEMM=0;

/*     ������� N:0 - ��������   
                    1 - BUST
                    N - N-TYJ tos */

short int TMPTOS[MAXKAT];    /* ������ ��������� ��������� tos */
short int NU;                /* ���������� ��������� tos ,
                                 ������������ ������ */

short int PRR;               /* ������ BUST,MAXBUF � �������� � ������*/
char MESR[]="������ ������ � �����";
char MESD[]="������ ������ �� ����";
char MESO[]="������ ��������� � �����";

FILE *FP;

void  TST(void);
void  ALLOC(int *K1,char *NM,int *N,int *M,int *L,int *SC);
short int CMPST(rad50 *N,rad50 *M);
short int HASH(unsigned short int *T);
short int strl(char *N);
short int FIND(char *NAME);
void  ascr50(short int N,char *IN1,unsigned short int *OU);
int   ALBL(short N);
void  START(char *PAR);
void  ERR(char *TXT);
void  erro(char *TXT);
void  RKAT(unsigned short int NKAT);
void  STOP(void);
void  FSB(short int NB,short int N,short int K);
unsigned short int sizetos();
int   sizeline();
int   sizepart(unsigned char *b,short int count);
void  ALLOC1(char *NM,int *N,int *M,int *L,int *SC);
void  APPREC(char *NM,int *N,int *M);
void  DELTOS(int *K1,char *NM);
void  FRBY(int *NF,int *NB);
void  GETP(float **IA,int *K);
void  INITOS(short int argn,char *argc[]);
void  PR(int T,int N,char *DD,short int F);
void  PROBOW(char *NM,int *I,char *DD,int *N);
void  PROBOA(char *NM,int *I,char  *DD,int *N);
void  PROBPA(char *NM,char  *DD);
void  PROBPW(char *NM,char  *DD);
void  PROBNM(char *NAME,int *N,int *M);
void  PROBIS(char *NM,char  *DD,int *N);
void  PROBCW(char *NM,int *I,char  *DD,int *N);
void  PROBW(char *NM,int *I,int *J,char  *STR,int *N,int *M,int *K);
void  PROBCA(char *NM,int *I,char  *DD,int *N);
void  RENAME(int *K1,char *NM);
void  rasc(unsigned short int *IN,char *OU);

/*  ���� ALLOC --------------------------------------------------- */
void TST(void)
{
if(NUKAT>MAX-50) {/* ShowMessage("������� ��� ����������");*/ }
}
/*  --------------------------------------------------- */
void  ALLOC(int *K1,char *NM,int *N,int *M,int *L,int *SC)
/*
        �������������� ����� � ������.
        ����: k- ���������� ���� tos,
               NM- ������ ���� tos,
               N- ������ ���������� ��������
               M- ������ ��������� �������
	       L- ������ ��������� tos.
	       SC- ������ ���� �����*/
{
short int I;
int K;
short int MEM,kl;
int mm;
char *B;

K=*K1;
for(;K;K--,NM+=8,L++,SC++,N++,M++)
        {
  if((I=FIND(NM))==-1)
                {
	MEM=060;
	kl=strl(NM);
	if(kl>=7)kl=7;
		else *(NM+kl+1)=0;
	  /* ������ �������� ����� */
	  		do {
		*(NM+kl)=(char )MEM++;
		if(MEM>=58&&MEM<=0100)MEM=0101;
                }
			 while((I=FIND(NM))==-1);
			}
    mm=4;
    if(*L&1)mm+=40;
    if(*L&2)mm+=*N*8;
    if(*L&4)mm+=*M*8;
    mm=(mm+511+*N*(*M)*(*SC))/512;
    MEM=mm;
         /* MEM-���������� ��������� ������ �� ����� (������)*/
/*
   ����� ��������� ����� �� ����� (����������)
        �������� ���������
        �� ���� �������� ����� ��������
           */

        if((mm=ALBL(MEM))==-1) /* ����� ���������� �����*/
                        {
                  /* ShowMessage("������: ��� ���������� ����� � �p����");*/
                   return;
			          }
        RKAT(I);
        UK->NBTOS=NBL=mm;

	B=(char *) malloc(512);
        memset(B,0,512);
	fseek(FP,NBL,SEEK_SET);
	for(;MEM;MEM--)	fwrite(B,512,1,FP);
	ERR(MESR);
	free(B);

        fseek(FP,NBL,SEEK_SET);
	fwrite((char *)&I,2,1,FP);
	MEM=*SC;
	fwrite((char *)&MEM,2,1,FP);
	   ERR(MESR);

/* �������� �������� */

        ascr50(strl(NM),NM,UK->NAMTOS);
        UK->NCB=*M;
        UK->NOBJ=*N;
	UK->FLATOS=(char)(*L&07);
	UK->FLATOS|=16;
	if(*L&64)TMPTOS[NU++]=I;	/* ��������� tos� */
        ++NUKAT;
	TST();
	fseek(FP,NBL+4,SEEK_SET);
	mm=0;
	if(*L&1)mm+=5;
	if(*L&2)mm+=*N;
	if(*L&4)mm+=*M;
	for(;mm;mm--) fwrite("        ",8,1,FP);
	*L=0;
        }
}

/*  ���� FIND --------------------------------------------------- */
/*   ------------------------------------------- */
short int CMPST(rad50 *N,rad50 *M)
/*   -------------------------------------------
  ��������� ���������� �����(RAD50)
  ����������: 0-���� �� �����,
                   1-���� �����.                    */

{
register short I;

for(I=3;I;I--)
        {
        if(*N++!=*M++){return(0);}
        }
        return(1);
}
/*  -------------------------------------------------- */
short int HASH(unsigned short int *T)
/* ----------------------------------------------------
    ������� ����������
          ���� : ��� tos (RAD50),
          �����: >0 ����� ��������  */

{
unsigned short int E;

E=*T+*(T+1)*2+*(T+2)*4;
return((short int)(E%MAX)+1);
}
/*  --------------------------------------------------- */
short int strl(char *N)
/*  ----------------------------------------------------
        ����������� ����� ������*/

{
short int I;

for(I=8;*N++>057&&I;I--);
return(8-I);
}
/*  --------------------------------------------------- */
short int FIND(char *NAME)
/*  ---------------------------------------------------
        ����� � ������ ��������� tos. 
        �����: =-1-������(UK-> �� ����),
              >0 - ����� ���������� �������� */

{
unsigned short int I,J,TMP;
rad50 T[3];

TMP=0;
ascr50(strl(NAME),NAME,T);
J=HASH(T);      /* ����������� */

for(I=0;;I++)
{
	RKAT(J);  /* ����������� �� �������� �������*/
	if(UK->FLATOS&32) TMP=J;
	else
        {
	if((UK->FLATOS&16)==0)
		{
                if(TMP==0) TMP=J;
                return(TMP); /* ������� �� ������.UK ��������� �� ���������*/
		}
        if(CMPST(UK->NAMTOS,T)==1) { return(-1); } /* ������*/
        }

/*  kolliziq */
        /* ����� �������� ���������
             ������������ �����������*/

        J+=24+13*I;
        while(J>MAX)
        J-=MAX;
}
}
/*  --------------------------------------------------- */
void RKAT(unsigned short int NKAT)
/*  ---------------------------------------------------
        ����������� UK �� ��������� �������  
        NKAT- ����� �������� */
{
UK=UKAT;
UK+=NKAT-1;
}
//*****************************************************************
void ascr50(short int N,char *IN1,unsigned short int *OU)         /* �������������� ���������� ������
                                   � ����  RADIX-50 */
{
short int W;
short int I,J;

        for(I=0;I<3;I++) *(OU+I)=0;
        for(I=0, J=0;I<N;I++,IN1++)
          {
	  *IN1=(char) toupper(*IN1);
          if(*IN1>0100) W=*IN1-0100;
          else if(*IN1>=060) W=*IN1-022;
          else W=036;
            if(I==0) *OU=W;
            else  *OU=(*OU)*050+W;

          J++;
          if(J==3)  {J=0;OU++; }
          }

}
//-���� ALBL--------------------------------------------------------------------------void
//---------------------------------------------------------------------------
/*-----------------------------------------------------------*/
/*          A L B L - ��������� ��������� ������ � ������   */
/*-----------------------------------------------------------*/
/*                                                           */
/*      ����� : A=ALBL(N);                                   */
/*    ���, N- ������������� ������������ � ������;           */
/*         A- ������������ ����� ����������� ���������� �����  */
/*           �������;                                       */
/*         A=-1, ������ �� ������������.                */
/*      ������������ ���������� :ADR,MAXBLK                 */
/*                                                          */
/*----------------------------------------------------------*/

int  ALBL(short int N)         /* ��������� ��������� ������*/
   {
      short int I,C,A0,
         C1,M,W;
	  short int  *A;
        C=C1=A0=M=0;
        A=BUST;          /* ����� BIT MAP */

          for(I=0;I<MAXBLK/16;I++)      /* ���� ���� BIT MAP */
          {
          W=*(A+I);     /*������� ����� */
          if(W==0177777)
                {
                C=A0=0;
          continue;               /* ������� �������� �����*/
                }
          if(W==0)
                {
                C+=16;
                A0=16*(I+1)-C;          /* ����� ������ �������*/
                if(C<N) continue;       /* 16 ��������� ������*/
         FSB(A0,N,1);             /* ��������� ����� ���������*/
        return(A0*512l+PRR);             /* ������� � ������� �������*/
                }
          else
                {
                C1=0;
                if(C!=0)
      {                     /* ����������� ������ � ����. ����� */
                  for(C1=-1,M=0;M==0;C1++,M=W&1,W>>=1);
                  if((C+C1)>=N)
                   {               /* ���������� ������������*/
                FSB(A0,N,1);     /* ��������� ����� ���������*/
                        return(A0*512l+PRR);
                        }
                  else C=0;
                  }
                  W=*(A+I);             /* ������� �����*/
                  if((16-C1)>=N)
                 {               /* ����� � �������� �����*/
                        W=W>>C1;
                        M=1;
                        for(C=0;C1<=16;C1++,M=W&1,W>>=1)
                          {
                          if(M==0) C++;
                          if(C>=N)
                                {
                                A0=16*I+C1-C;
                                FSB(A0,N,1);
                                return(A0*512l+PRR);
                                }
                          if(M!=0) C=0;
                          }
                        }
                  W=*(A+I);
     for(C=0;W>0;C++,W<<=1);  /* ����� ������������ ����� */
        A0=16*(I+1)-C;           /* ����� ����� ��������� */
           continue;                /* � ��������� ������*/
                  }
        }
return(-1);                        /* ������ �� ������������ */
}

//-���� STARTTOS--------------------------------------------------------------------------void
//---------------------------------------------------------------------------

void START(char *PAR)
/* ------------------------------------------

  ����������� ������ � �������*/
{
int i;
short int j;
char m[45];
void pystart();
#ifdef logg
 printf("START: %s\n",PAR);
#endif
for(j=0;*PAR>040;j++) m[j]=*PAR++;
m[j]=0;
if((FP=fopen(m,"r+b"))==NULL) /*ShowMessage("������ �������� �����")*/;

PAR=(char *)(0xc7*4);
i=fread((char *)&MAXBLK,2,1,FP);
ERR(MESR);    fread((char *)&NUKAT,2,1,FP);
ERR(MESR);
PRR=(MAXBLK+15)/16;  /* ������ RAJONA ��� BUST */
if((BUST=(short int  *)malloc(PRR*2))==NULL) /*ShowMessage("��� ������")*/;

fread((char *)BUST,2,PRR,FP);
ERR(MESR);
i=MAX;
if((UKAT=(KATALOG  *)malloc((short int)i*sizeof(KATALOG)))==NULL) /*ShowMessage("��� ������")*/;

fread((char *)UKAT,sizeof(KATALOG),MAX,FP);

ERR(MESR);
PRR=PRR*2+4+MAX*sizeof(KATALOG)-4; /* � ������ */
}
/*  ---------------------------------------- */
void ERR(char *TXT) {
  if(ferror(FP)) {
    perror(TXT);
    clearerr(FP);
  }
}

 /*   --------------------------------------- */
void  erro(char *TXT)
/*    -------------------------------------
��������� �� ������� � ������� �� ��������� */
{
//ShowMessage(TXT);
//Basic->Close();
}

/*-----------------------------------------------*/
void STOP()
/*------------------------------------------------
        ����� ������ � �������                     */
{
int J;
char *p;
#ifdef logg
 printf("STOP:\n");
#endif

NUKAT-=NU;

/* ����������� ��������� ���*/

for(;NU!=0;)
        {
	RKAT(TMPTOS[--NU]); /* ����������� �� �������*/
	J=sizetos();
        UK->NAMTOS[0]=0;    /* ������� ����� */
	UK->FLATOS=32;	 /* ������� ������� ��� ���������*/
/*
  �������� ��������� �����
*/
        FSB((short int)((UK->NBTOS-PRR+511)/512),J,0);
        }
/* ������ NUKAT*/
fseek(FP,2l,SEEK_SET);
fwrite((char *)&NUKAT,2,1,FP);
ERR(MESD);
/* ������ �� ���� ��������� � BUST*/
fwrite((char *)BUST,2,(MAXBLK+15)/16,FP);
ERR(MESD);
free((char  *)BUST);
fwrite((char *)UKAT,sizeof(KATALOG),MAX,FP);
ERR(MESD);
free((void *)UKAT);
if(MEMM)free((void *)MEMM);
fclose(FP);
p=(char *)(0xc7*4);
//*p=1;
        }
/*--------------------------------------------------------------*
 *      F S B(NB,N,K) - ������������ � ������� ������ ������    *
 *              NB - ����� ����� � �������� ����������         *
 *              N  - ���������� ������                          *
 *              K=0- ���������� , K=1- ������                      *
 *--------------------------------------------------------------*/

void FSB(short int NB,short int N,short int K) /* ������������ ������� ������ ������*/
 {

     short int *A;
     short int    A0,J,I;

        A=BUST;
        A=A+NB/16;
        A0=NB%16;

          if((16-A0)<N)
                {
                for(I=0,J=1;J<=A0;I=(I<<1)+1,J++);
                if(A0==0)       I=0;
                if(K==0)  *A=(*A)&I;
                else      {
                          I=~I;
                          *A=(*A)|I;
                          }
                A++;
                N=N-16+A0;
                        if((N/16)>=1)
                        for(I=0;I<N/16;I++,A++)
                        if(K==0)   *A=0;
                        else       *A=0177777;
                for(I=0100000,J=1;J<(16-N%16);I=I>>1,J++);
                if(K==0)  *A=(*A)&I;
                else    {
                        I=~I;
                        *A=(*A)|I;
                        }
                }
          else
                {
                for(I=0,J=1;J<=N;I=(I<<1)+1,J++);
                I=I<<A0;
                if(K==0)  {
                           I=~I;
                           *A=(*A)&I;
                          }
                else    *A=(*A)|I;

                }
}

unsigned short int sizetos() /* ������ ��� � ������, �� ������� ��������� ������� */
{
int j,y,t;
unsigned short int i;
unsigned char k;

j=2;
if(UK->FLATOS&1)j+=40; /* ������� */
if(UK->FLATOS&2)j+=UK->NOBJ*8; /* ����� �������� */
if(UK->FLATOS&4)j+=UK->NCB*8;  /* ����� ������� */
if(UK->FLATOS&8) /* ����� */
    {
    t=j+UK->NBTOS;
    j+=UK->NCB;
    y=0;
    fseek(FP,t,SEEK_SET);
    for(i=UK->NCB;i;i--)
			{
				fread(&k,1,1,FP);
				y+=k;	/* len line */
			}
    j+=y*UK->NOBJ;
    }
else
	{
	fseek(FP,UK->NBTOS+2,SEEK_SET);
	fread((char *)&i,2,1,FP);
	j+=UK->NOBJ*UK->NCB*i+2;
	}
i=(j+511)/512;
return(i);
}

/*BIS(unsigned char huge *d,unsigned n,int f)
{
int t;

t=4;
if(UK->FLATOS&1)t+=40;
if(UK->FLATOS&4)t+=UK->NCB*8;
if(UK->FLATOS&2)t+=UK->NOBJ*8;
PR(t,n,d,f);
} */

int sizeline() /* len line */
{
int i,t;
unsigned k,l;

if(UK->FLATOS&8) /* ����� */
    {
t=2;
if(UK->FLATOS&1)t+=40; /* ������� */
if(UK->FLATOS&2)t+=UK->NOBJ*8; /* ����� �������� */
if(UK->FLATOS&4)t+=UK->NCB*8;  /* ����� ������� */
    t+=UK->NBTOS;
    i=0;
    fseek(FP,t,SEEK_SET);
    for(l=UK->NCB;l;l--)
			{
			fread(&k,1,1,FP);
			i+=k;	/* len line */
			}
    }
else
    {
    fseek(FP,UK->NBTOS+2,SEEK_SET);
    fread((char *)&k,2,1,FP);
    i=(int)UK->NCB*k;
    }
return(i);
}
//
int sizepart(unsigned char *b,short int count) /* ����� �� count*/
{
		int i=0;
		count--;
		while(--count>=0)
			 i+=b[count];
		return(i);
}
//---------------------------------------------------------------------------
//-���� ALLOC1----------------------------------------------------------------
void ALLOC1(char *NM,int *N,int *M,int *L,int *SC)
/*  ----------------------------------------------------
        �������������� ����� � ������.
        ����:NM-��� ���,   
               N- ���������� ��������,  
               M- ���������� �������,
	       L- ������� tos.
	       SC- ������ ���� �����*/
{
short int I,K;
short int MEM,kl;
int mm;
char *B;

K=*M-1;
if((I=FIND(NM))==-1)
                {
	MEM=060;
	kl=strl(NM);
	if(kl>=7)kl=7;
		else *(NM+kl+1)=0;
	  /* ������ �������� ����� */
	  		do {
		*(NM+kl)=(char )MEM++;
		if(MEM>=58&&MEM<=0100)MEM=0101;
                }
			 while((I=FIND(NM))==-1);
			}
    mm=2;
    if(*L&1)mm+=40;
    if(*L&2)mm+=*N*8;
    if(*L&4)mm+=*M*8;
		while(K>=0)
				mm+=SC[K--];
    mm=(mm+511)/512;
    MEM=mm;
         /* MEM-���������� ��������� ������ �� ����� (������) */
/*
   ����� ��������� ����� �� ����� (���� ��� ��������)
        �������� ���������
        �� ���� �������� ����� �������� I
           */

        if((mm=ALBL(MEM))==-1) /* ����� ���������� �����*/
                        {
//                    ShowMessage("������: ��� ���������� ����� � ������");
                   return;
			          }
        RKAT(I);
        UK->NBTOS=NBL=mm;

	B=(char *) malloc(512);
        memset(B,0,512);
	fseek(FP,NBL,SEEK_SET);
	for(;MEM;MEM--)	fwrite(B,512,1,FP);
	ERR(MESR);
	free(B);

   fseek(FP,NBL,SEEK_SET);
	fwrite((char *)&I,2,1,FP);
	   ERR(MESR);

/* �������� �������� */

        ascr50(strl(NM),NM,UK->NAMTOS);
        UK->NCB=*M;
        UK->NOBJ=*N;
	UK->FLATOS=(char)(*L&07);
	UK->FLATOS|=24;
	if(*L&64)TMPTOS[NU++]=I;	/* ��������� ��� */
        ++NUKAT;
	TST();
	fseek(FP,NBL+2,SEEK_SET);
	mm=0;
	if(*L&1)mm+=5;
	if(*L&2)mm+=*N;
	if(*L&4)mm+=*M;
	for(;mm;mm--)fwrite("        ",8,1,FP);
	K=*M-1;
	while(K-->=0)
		 fwrite(SC++,1,1,FP);

}
//---���� APPEC------------------------------------------------------------------------
void APPREC(char *NM,int *N,int *M)
/*  ----------------------------------------------------
        �������������� ����� � ������.
        ����:
               NM-���� tos,   
               N- ���������� ��������
               M- number,*/
{
short int I;
short int MEM,ll,tm;
int mm,M1,KK,M2,OB,CB,sizeline(),t,tt;
char *B;

  if((I=FIND(NM))!=-1)
	       return; /* ��� �� */
    M2=M1=UK->NBTOS;
    OB=UK->NOBJ+*N;
  	CB=UK->NCB;
    ll=sizetos();
    tt=sizeline();
    mm=tt*OB+2;
    if(UK->FLATOS&1)mm+=40;
    if(UK->FLATOS&2)mm+=OB*8;
    if(UK->FLATOS&4)mm+=CB*8;
    if(UK->FLATOS&8)mm+=CB;
			else mm+=2;
    MEM=(mm+511)/512;
         /* MEM*/
/*
   ����� ��������� ����� (���� ��� - ��������)
        �������� ���������
        �� ���� �������� ����� ��������
           */

        if((mm=ALBL(MEM))==-1) /* ����� ���������� ����� */
                        {
//                    ShowMessage("������: ��� ���������� ����� � ������");
                   return;
			          }
	FSB((short int)((M2-PRR+511)/512),ll,0);
	UK->NBTOS=NBL=mm;
	UK->NOBJ=OB;

	B=(char *) malloc(512);
	fseek(FP,NBL,SEEK_SET);
	for(;MEM;MEM--)	fwrite(B,512,1,FP);
	ERR(MESR);
	tm=(UK->FLATOS&8)?2:4;
 fseek(FP,M1,SEEK_SET);
	fread((char *)&t,tm,1,FP);
 fseek(FP,NBL,SEEK_SET);
	fwrite((char *)&t,tm,1,FP);
	ERR(MESR);

	fseek(FP,NBL+tm+(UK->FLATOS&1)?40:0,SEEK_SET);
	mm=0;
	if(UK->FLATOS&2)mm+=OB;
	if(UK->FLATOS&4)mm+=CB;
	for(;mm;mm--)fwrite("        ",8,1,FP);
	t=tm;
/* PASPORT */
  if(UK->FLATOS&1)
	{
	B=(char *) realloc(B,40);
        fseek(FP,M1+tm,SEEK_SET);
        fread(B,40,1,FP);
        fseek(FP,NBL+tm,SEEK_SET);
        fwrite(B,40,1,FP);
	   ERR(MESR);
	t+=40;
	}
/* CB */
    if(UK->FLATOS&4)
	{
	B=(char *) realloc(B,(short int)CB*8);
	fseek(FP,M1+t,SEEK_SET);
        fread(B,(short int)CB,8,FP);
	fseek(FP,NBL+t,SEEK_SET);
        fwrite(B,(short int)CB,8,FP);
	   ERR(MESR);
	t+=CB*8;
	}
/* OBJ */
    if(UK->FLATOS&2)
	{
	B=(char *) realloc(B,(short int)((OB-*N)*8));
	fseek(FP,M1+t,SEEK_SET);
        fread(B,(short int)(OB-*N),8,FP);
	fseek(FP,NBL+t,SEEK_SET);
        fwrite(B,(short int)(OB-*N),8,FP);
	   ERR(MESR);
	t+=(OB-*N)*8;
	}
/* SHKAL */
    if(UK->FLATOS&8)
	{
	B=(char *) realloc(B,(short int)CB);
	fseek(FP,M1+t,SEEK_SET);
        fread(B,(short int)CB,1,FP);
	fseek(FP,NBL+t+(UK->FLATOS&2)?*N*8:0,SEEK_SET);
	fwrite(B,(short int)CB,1,FP);
	   ERR(MESR);
	t+=CB;
	}
/*TOS */
	B=(char *) realloc(B,(short int)tt);
	M1+=t;
	mm=NBL+t+(UK->FLATOS&2)?*N*8:0;
	for(KK=(OB-*N);KK--;M1+=tt,mm+=tt)
	{
        fseek(FP,M1,SEEK_SET);
	fread(B,(short int)tt,1,FP);

        fseek(FP,mm,SEEK_SET);
	fwrite(B,(short int)tt,1,FP);
	   ERR(MESR);
	}
	free(B);
	*M=OB-*N+1;
}
//---------------------------------------------------------------------------
//--��� DELETE-------------------------------------------------------------------------
void DELTOS(int *K1,char *NM)
/*         �������� TOS.
         K-���������� TOS
         NM-����� TOS         */

{
short int J,K;

K=*K1;
for(;K;K--,NM+=8)
        {
			if(FIND(NM)!=-1)continue;  /* tos �� �������  */
			J=sizetos();
       UK->NAMTOS[0]=0;
			UK->FLATOS=32;	      /* ������� */
       NUKAT--;
/*
   �������� ��������� ����� �  bust*/
        FSB((short int)((UK->NBTOS-PRR+511)/512),J,0);
        }
}
//---------------------------------------------------------------------
//--���� FRBY----------------------------------------------------------------
void FRBY(int *NF,int *NB)
 /*-----------------------------------------------------------*/
/*     F R B Y - ����� ���������� � �������� ������������    */
/*               � ������ ���							 */
/*-----------------------------------------------------------*/
/*                                                           */
/*      ����� : FRBY(&NF,&NB);                               */
/*    ���, NF-���������� ���������� ������������ � ������;   */
/*         NB-���������� �������� ������������.              */
/*                                                           */
/*      ������������ ����������  :ADR,MAXBLK                 */
/*                                                           */
/*-----------------------------------------------------------*/
  {

        short int I;
	   short int J,W;
	   int C;
        short int  *A;
        C=*NF=*NB=0;
        A=BUST;          /* ����� BIT MAP */

          for(I=0;I<MAXBLK/16;I++)      /* ���� ���� BIT MAP */

	    {
	    if(*(A+I)==0) C+=16l;
	    else if(*(A+I)==0177777)
	    	{
		*NB=*NB+16l;
		*NF=C;
		}
		else
		  {
		  W=*(A+I);
		  for(J=0;J<16;J++,W=W<<1)
		    {
		    if(W>=0) C++;
		    else (*NB)++;
		    }
		  W=*(A+I);
  		  for(J=0;W>=0;J++,W=W<<1);
		  *NF=C-J;
		  }
	    }
*NF*=512;
*NB*=512;
}
//--���� GETPC------------------------------------------------------------------
//--------------------------------------------------------------------
void GETP(float **IA,int *K)
/*-----------------------------------
        ��������� ������ �������� KOS*2 ����
    �����: KOS-������ ���������� ������(� 4-� ������� ������) */
{
static short i=0;
if(i){i=0;free(MEMM);}
while((*IA=MEMM=(float  *)calloc(*K,sizeof(float)))==NULL&&*K>1l)
	*K=*K*(1.-1./10.);
if(*K<=1l) /*ShowMessage("������: ��� ������")*/;
 else i=1;
}

//---------------------------------------------------------------------
//---���� INITOS------------------------------------------------------------------
void INITOS(short int argn,char *argc[])
{
FILE *f;
char name[52];
short int cou,n;
char *mm;
int off;

if(argn>1)
	{
	strcpy(name,argc[1]);
	}
else
	{
//	ShowMessage("������: ��� ����� ������ ? ");
	scanf("%s",name);
	}
if((cou=creat(name,S_IREAD|S_IWRITE))==-1)
/*ShowMessage("������: Create error\n")*/;
/*if(argn>2)
	{
	n=atoi(argc[2]);
	}
else
	{
	printf("������ ����� (Kb) ? ");
	scanf("%d",&n);
	}*/
n=14;
off=14*1024l;
if(chsize(cou,off)!=0) /*ShowMessage("������: Chsize error\n")*/;
close(cou);
f=fopen(name,"r+");
if(f==NULL)/*ShowMessage("������: Fopen error\n")*/;
n=8000;
fseek(f,0l,SEEK_SET);
fwrite((char *)&n,2,1,f);
mm=(char *)malloc(1024);
memset(mm,0,1024);
n=13;
while(n--) fwrite(mm,1,1024,f);
fclose(f);
}
//--���� PR--------------------------------------------------------------------

void  PR(int T,int N,char  *DD,short int F)
/*----------------------------------------------------------
        ������-������
        DD-���� ������ ������
        N-������� ������
        F=0-������,=1-������   */

{
//int d;
//int K;

NBL=UK->NBTOS+T;                 /* ����� ����� */
fseek(FP,NBL,SEEK_SET);
if(F)
	{
	while(N>32000){fwrite((char *)DD,1,32000,FP);N-=32000;DD+=32000;}
	fwrite((char *)DD,1,(short int)N,FP);
	}
     else
	{
	while(N>32000){fread((char *)DD,1,32000,FP);N-=32000;DD+=32000;}
	fread((char *)DD,1,(short int)N,FP);
	}
ERR(MESO);
}
//-------------------------------------------------------------------
//---��������� ������-������----------------------------------------------------------------
void  PROBA(char *NM,int *I,int *J,char *STR,int *N,int *M,int *K)
/*-----------------------------------------------
        ������ �������� �� ������.
        NM-��� TOS
        I-��������� ����� �������
        J-��������� ����� ��������
        STR-������(N*M)
        N-���������� ��������
        M-���������� �������
        K-�������(=0,���� ���� ������,=12345 -���������������*/
{
int sizepart(unsigned char *,short int),FIRST=0,N1,NN;
short int M1,m2,i_1,n=*N;
unsigned char *bt,ii;
#ifdef logg
 printf("PROBA: %s %ld %ld %ld %ld\n",NM,*I,*J,*N,*M);
#endif

if(FIND(NM)!=-1)
        {
        *K=0;
        return;          /* TOS �� ������ */
        }
if(*I+*N-UK->NOBJ>1||*J+*M-UK->NCB>1)

        {
        *K=0;
        return;         /*������ */
        }

NBL=UK->NBTOS+2;/*������ ���� TOS*/
if(UK->FLATOS&1)NBL+=40;
if(UK->FLATOS&2)NBL+=UK->NOBJ*8;
if(UK->FLATOS&4)NBL+=UK->NCB*8;
bt=(unsigned char *) malloc(UK->NCB);
if(UK->FLATOS&8)
       {
       fseek(FP,NBL,SEEK_SET);
       fread(bt,UK->NCB,1,FP);
       NBL+=UK->NCB;
       }
else
    {
		NBL+=2;
    fseek(FP,UK->NBTOS+2,SEEK_SET);
    fread((char *)&ii,1,1,FP);
    memset(bt,ii,UK->NCB);
    }



FIRST=NBL;
if(*J>1)
 		FIRST+=UK->NOBJ*(sizepart(bt,*J)); /* ��砫� ��� �� */
M1=*M;
fseek(FP,FIRST,SEEK_SET);
/* ne transponiruq */

m2=*J-1;
i_1=*I-1;
NN=bt[m2]*i_1;
for(;M1-->0;m2++)
        {
        fseek(FP,NN,SEEK_CUR);
        N1=bt[m2]*n;
/*        while(N1>32000)
                {
                fread((char *)STR,1,32000,FP);
                N1-=32000;
                STR+=32000;
                }
        if(N1>0)*/
					{
                    fread((char *)STR,1,(short int)N1,FP);
					STR+=N1;
					}
  /*      ERR(MESO);
        fseek(FP,NN,SEEK_CUR);*/
   NN=bt[m2]*(UK->NOBJ-n-i_1)+bt[m2+1]*i_1;
        }
free(bt);
}
//--------------------------------------------------------------------------
void  PROBW(char *NM,int *I,int *J,char  *STR,int *N,int *M,int *K)
/*-----------------------------------------------*/
{
int sizepart(unsigned char *,short int),FIRST=0,N1,NN;
short int M1,m2,i_1,n=*N;
unsigned char *bt,ii;
#ifdef logg
 printf("PROBW: %s %ld %ld %ld %ld\n",NM,*I,*J,*N,*M);
#endif

if(FIND(NM)!=-1)
        {
        *K=0;
        return;          /* TOS �� ������ */
        }
if(*I+*N-UK->NOBJ>1||*J+*M-UK->NCB>1)

        {
        *K=0;
        return;         /*������ */
        }

NBL=UK->NBTOS+2;/* ������ ���� TOS*/
if(UK->FLATOS&1)NBL+=40;
if(UK->FLATOS&2)NBL+=UK->NOBJ*8;
if(UK->FLATOS&4)NBL+=UK->NCB*8;
bt=(unsigned char *) malloc(UK->NCB);
if(UK->FLATOS&8)
       {
       fseek(FP,NBL,SEEK_SET);
       fread(bt,UK->NCB,1,FP);
       NBL+=UK->NCB;
       }
else
    {
		NBL+=2;
    fseek(FP,UK->NBTOS+2,SEEK_SET);
    fread((char *)&ii,1,1,FP);
    memset(bt,ii,UK->NCB);
    }



FIRST=NBL;
if(*J>1)
 		FIRST+=UK->NOBJ*(sizepart(bt,*J)); /* ��砫� ��� �� */
M1=*M;
fseek(FP,FIRST,SEEK_SET);
/* ne transponiruq */

m2=*J-1;
i_1=*I-1;
NN=bt[m2]*i_1;
for(;M1-->0;m2++)
        {
        fseek(FP,NN,SEEK_CUR);
        N1=bt[m2]*n;
/*        while(N1>32000)
                {
                fwrite((char *)STR,1,32000,FP);
                N1-=32000;
                STR+=32000;
                }
        if(N1>0)*/
					{
                    fwrite((char *)STR,1,(short int)N1,FP);
					STR+=N1;
					}
/*        ERR(MESO);
        fseek(FP,NN,SEEK_CUR); */
   NN=bt[m2]*(UK->NOBJ-n-i_1)+bt[m2+1]*i_1;
        }
free(bt);
}
//--------------------------------------------------------------------------
void PROBCA(char *NM,int *I,char  *DD,int *N)
/*----------------------------------------------------------
        ������-������ ���� �������
        NM-��� TOS
        I-��������� ����� ��������
        DD-����-������ ������
        N - ������� ������
                                */
{
int T;

#ifdef logg
 printf("PROBCA: %s %ld %ld\n",NM,*I,*N);
#endif
if(FIND(NM)!=-1)return;
if(UK->FLATOS&4)
		{
			T=2;
			if(UK->FLATOS&1)T+=40;
			if(!(UK->FLATOS&8))T+=2;
			T+=(*I-1)*8;
			PR(T,*N*8,DD,0);
		}
}
//-------------------------------------------------------------------------------
void PROBCW(char *NM,int *I,char  *DD,int *N)
/*----------------------------------------------------------
        ������-������ ���� �������
        NM-��� TOS
        I-��������� ����� ��������
        DD-����-������ ������
        N-������� ������
                                */
{
int T;
#ifdef logg
 printf("PROBCW: %s %ld %ld\n",NM,*I,*N);
#endif

if(FIND(NM)!=-1)return;
if(UK->FLATOS&4)
		{
			T=2;
			if(UK->FLATOS&1)T+=40;
			if(!(UK->FLATOS&8))T+=2;
			T+=(*I-1)*8;
			PR(T,*N*8,DD,1);
		}
}
//------------------------------------------------------------------------------
void PROBIS(char *NM,char  *DD,int *N)
/*----------------------------------------------------------
        ������-������ �����
        NM-��� TOS
        DD-����-������ ������
        N-������� ������ (�� 4 �����)   */

{
int T;
short int i=*N;

#ifdef logg
 printf("PROBIS: %s %ld\n",NM,*N);
#endif
if(FIND(NM)!=-1)return;
T=2;
if(UK->FLATOS&8)
		{
			if(UK->FLATOS&1)T+=40;
			if(UK->FLATOS&2)T+=UK->NOBJ*8;
			if(UK->FLATOS&4)T+=UK->NCB*8;
			PR(T,*N,DD,0);
			while(i>0)
					{
						*(DD+i*4-1)=0;
						*(DD+i*4-2)=0;
						*(DD+i*4-3)=0;
						*(DD+i*4-4)=DD[i-1];
						i--;
					}
		}
		else
				{
			PR(T,1l,DD,0);
			while(i>0)
					{
						*(DD+i*4-1)=0;
						*(DD+i*4-2)=0;
						*(DD+i*4-3)=0;
						*(DD+i*4-4)=DD[0];
						i--;
					}
				}
}
//------------------------------------------------------------------------------
void PROBNM(char *NAME,int *N,int *M)
/*  ---------------------------------------
        ����������� ������� ��� � ������ � �� �������
        ����������� tos � ������
       �����: N-���������� ��������
              M- ���������� �������*/
{
 /* ����� ��������� tos � ��������*/
#ifdef logg
 printf("PROBNM: %s %d %d \n",NAME,*N,*M);
#endif
if(FIND(NAME)==-1)
        {
 /* ������� ������ */
        *N=UK->NOBJ;
        *M=UK->NCB;
//        printf("kat found\n");
        }
else {*N=*M=0;
 //       printf("kat not found\n");    
}

}
//-----------------------------------------------------------------------------
void PROBPA(char *NM,char  *DD)
/*----------------------------------------------------------
        ������ ��������
        NM-��� TOS
        DD-����-������ ������*/
{

#ifdef logg
 printf("PROBPA: %s\n",NM);
#endif
if(FIND(NM)!=-1)return;
if(UK->FLATOS&1)
		{
			NBL=2;
			if(!(UK->FLATOS&8))NBL+=2;
			PR(NBL,40l,DD,0);
		}
}
//------------------------------------------------------------------------------
void PROBPW(char *NM,char  *DD)
/*----------------------------------------------------------
        ������ ��������
        NM-��� TOS
        DD-���� ������ ������*/
{

#ifdef logg
 printf("PROBPW: %s\n",NM);
#endif
if(FIND(NM)!=-1)return;
if(UK->FLATOS&1)
		{
			NBL=2;
			if(!(UK->FLATOS&8))NBL+=2;
			PR(NBL,40l,DD,1);
		}
}
//------------------------------------------------------------------------------
void  PROBOA(char *NM,int *I,char  *DD,int *N)
/*----------------------------------------------------------
        ������-������ ���� ��������
        NM-��� TOS
        I-��������� ����� �������
        DD-����-������ ������
        N-������� ������  */
{
int T;

#ifdef logg
 printf("PROBOA: %s %ld %ld\n",NM,*I,*N);
#endif

if(FIND(NM)!=-1)return;
if(UK->FLATOS&2)
		{
			T=2;
			if(UK->FLATOS&1)T+=40;
			if(!(UK->FLATOS&8))T+=2;
			if(UK->FLATOS&4)T+=UK->NCB*8;
			T+=(*I-1)*8;
			PR(T,*N*8,DD,0);
		}
}
//------------------------------------------------------------------------------
void PROBOW(char *NM,int *I,char  *DD,int *N)
/*----------------------------------------------------------
        ������-������ ���� ��������
        NM-��� TOS
        I-��������� ����� �������
        DD-����-������ ������
        N-������� ������   */
{
int T;

#ifdef logg
 printf("PROBOW: %s %ld %ld\n",NM,*I,*N);
#endif
if(FIND(NM)!=-1)return;
if(UK->FLATOS&2)
		{
			T=2;
			if(UK->FLATOS&1)T+=40;
			if(!(UK->FLATOS&8))T+=2;
			if(UK->FLATOS&4)T+=UK->NCB*8;
			T+=(*I-1)*8;
			PR(T,*N*8,DD,1);
		}
}
//------------------------------------------------------------------------------
//---------- ����� ���������� ������-������ ------------------------------------
//------------------------------------------------------------------------------
void rasc(unsigned short int *IN1,char *OU)     /* �������������� RADIX-50 - ASCII */

/*..........................................................*/
{
register unsigned short int W,V;
short int J,I;

  for(J=0;J<3;J++,IN1++)
  for(I=1600,V=W=*IN1;I>0;I/=40,OU++)
  {
        W=V/I;
        V=V%I;
  if(W==0)
        OU--;
     else  if(W>=1&&W<=032) *OU=W+0100;
           else  if(W>=036&&W<=047) *OU=W+022;
                else
                {
                *OU=0;
                }
  }
*OU=0;
}
//------------------------------------------------------------------------------
void RENAME(int *K1,char *NM)
/*  ---------------------------------------------------
        �������������� tos��.  
        k-����������,
         NM-������ ������ � ����� ���� */
{

short int OBJ,CB,J;
short int I,K;
char C[2];

K=(short int)*K1;
for(;K;K--,NM+=16)
        {
        if((I=FIND(NM+8))==-1)   /* �������� �� �������� */
                  {
//	       ShowMessage("������: �������� �����");
                   continue;
                }
if(FIND(NM)!=-1)     /* ����� �������� */
        {
//	ShowMessage("������: ��� �� ������� ");
        continue;
        }

/* ���������� ��������*/

J=UK->NBTOS;
OBJ=UK->NOBJ;
CB=UK->NCB;
C[1]=UK->FLATOS;

/* �������� ��� ��������� �������*/
UK->NAMTOS[0]=0;
UK->FLATOS=32;

RKAT(I);
        ascr50(strl(NM+8),NM+8,UK->NAMTOS);
        UK->NBTOS=J;
        UK->NOBJ=OBJ;
        UK->NCB=CB;
        UK->FLATOS=C[1];

/* ���������� ������ �������� � ��� */
fseek(FP,J,SEEK_SET);
fwrite((char *)&I,2,1,FP);
ERR(MESD);
        }
}





