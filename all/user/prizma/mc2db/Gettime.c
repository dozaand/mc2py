#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void pase40(int *,int *,int *,int *,int *,int *,char *);
void gettime(int *,int *,int *,char *);

void gettime(int *n_hour,int *n_min,int *n_sec,char *pase)    {
int i,j,k;
      pase40(&i,&j,&k,n_hour,n_min,n_sec,pase);
}

void pase40(int *n_month,int *n_day,int *n_year,
			int *n_hour, int *n_min,int *n_sec,char *buf)
{
char timebuf[9], datebuf[9];
char buffer[10];
char *pdest;

    _strtime( timebuf );
     strcpy( buffer ," ");
     pdest = strncat( buffer, timebuf , 2 );
     *n_hour = atoi(buffer);
     strcpy( buffer ," ");
     pdest = strncat( buffer, timebuf+3,2 );
     *n_min = atoi(buffer);
     strcpy( buffer ," ");
     pdest = strncat( buffer, timebuf+6,2 );
     *n_sec = atoi(buffer);

    _strdate( datebuf );

     strcpy( buffer ," ");
     pdest = strncat( buffer, datebuf , 2 );
     *n_month = atoi(buffer);
     strcpy( buffer ," ");
     pdest = strncat( buffer, datebuf+3,2 );
     *n_day = atoi(buffer);
     strcpy( buffer ," ");
     pdest = strncat( buffer, datebuf+6,2 );
     *n_year = atoi(buffer);

     strcpy(buf,datebuf);
     strcat(buf,"   ");
     strcat(buf,timebuf);
}
