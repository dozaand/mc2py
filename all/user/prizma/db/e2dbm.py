# -*- coding: cp1251 -*-
#-------------------------------------------------------------------------------
# Name:        module1
# Purpose: dump energ to dbm file
#
# Author:      and
#
# Created:     30.05.2010
# Copyright:   (c) and 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
import shelve
import numpy as nu
import sys
sys.path.append("C:/PROJECTS/mc2py/db/energ/")
import energ_db as en

def dump_e2dbm(prefix,infile,outfile,datalist):
    u"����� ������ �� ������� � dbm ����"
    en.bz_start(infile)
    f=shelve.open(outfile+".dbm")
    for it in datalist:
        nm=prefix+it[0]
        res=en.bz_get(nm,it[1])
        f[nm]=res
    f.close()

class Tdbobj:
    pass

def bas2obj(prefix,infile,datalist):
    u"�������������� ����� ���� � ������ ������"
    en.bz_start(infile)
    obj=Tdbobj()
    for it in datalist:
        nm=prefix+it[0]
        res=en.bz_get(nm,it[1])
        exec "obj.%s = res" % nm
    en.bz_stop()
    return obj


def main():
    lst=[
['ZR','h',u'��������'],
['CT','i',u'��������� ����� �����������'], #char[4]
['PR','f',u'������ ������'],
['ER','f',u'���p����p������ �������'],
['GR','f',u'p������'],# ���� �������� �� 0.1
['CR','h',u'��������� ���'],
['FS','f',u'���p����� ��'],
['DR','f',u'p��������� �������'],
['DH','f',u'�������� �������'],
['SW','h',u'p������� ���'],
['TB','f',u'��������� ������'],
['FO','f',u'���p����� �� ������'],
['PR','f',u'���������������� ��p����p� ����'],

# p��������� p�������
['WD','f',u'�������� �������'],
['FI','f',u'��p�����p�����'],
['KZ','f',u'� ������'],
['PD','f',u'�p���� ��p����p�']
    ]
    lst2=[
['KD','H',u'���������� ����'],
    ]
#    dump_e2dbm("SC","baz","baz",lst)
    res=bas2obj("SC","baz",lst)
    f=shelve.open("tmp.dat")
    for i in range(50):
        res=bas2obj("SC","baz",lst)
        f[str(i)]=res
    f.close()

    pass

if __name__ == '__main__':
    main()