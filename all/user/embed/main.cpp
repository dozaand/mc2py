#include <iostream>
#include <Python.h>


using namespace std;

int main()
{
 double d[20]={0};
 int i;
 long l;
 for(i=0;i<20;++i)
 {
  cout<<d[i]<<endl;
 }
 cout<<"hello"<<endl;
 
 PyObject *pName,*module,*adr;

 Py_Initialize();
 module=Py_InitModule("__main__",NULL);
 PyRun_SimpleString("from time import time,ctime\n"
                     "print 'Today is',ctime(time())\n");
 PyRun_SimpleString("import numpy as num\n");
 PyRun_SimpleString("a=num.array([1,2,3])\nprint a");
 PyRun_SimpleString("adr=a.__array_interface__['data'][0]");
 PyRun_SimpleString("bufsize=len(a.data)");
 PyRun_SimpleString("itemsize=a.itemsize");
 pName = PyObject_GetAttrString(module,"adr");
 cout<<pName<<endl;
 l=PyInt_AsLong(pName);
 cout<<l<<endl;
 Py_DECREF(pName);
 pName = PyObject_GetAttrString(module,"bufsize");
 l=PyInt_AsLong(pName);
 cout<<l<<endl;
 Py_DECREF(pName);
 pName = PyObject_GetAttrString(module,"itemsize");
 l=PyInt_AsLong(pName);
 cout<<l<<endl;
 Py_DECREF(pName);

 cout<<pName<<endl;

 Py_Finalize();

 return 0;
}