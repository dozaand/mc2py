/* File : example.i */
%module(docstring="energia database interface implementation: class Tdb_energ") energ_db


%include "typemaps.i"
%include "carrays.i"



%array_class(double, doubleArray);
%array_class(float, floatArray);
%array_class(int, intArray);
%array_class(short, shortArray);
%array_class(unsigned char, uArray);


%{
#include "wtos.h"
#include "wtos_a.h"
using namespace db_energ;
%}
%exception pt {
try
{
 $action
}
catch(const char* str)
{
 PyErr_SetString(PyExc_MemoryError,str);
 return NULL;
}
}


%feature("autodoc","1") bz_proba; 
%feature("docstring") bz_proba 
" get data from file
  NM - table identifier
  ob  - object start index (from 1) (row number);
  sw  - property start index (from 1) (col number);
  STR - data buffer;
  n   - number of objects (rows);
  m   - number of props (cols);
  priz- error code.
"; 
%feature("autodoc","1") bz_probw; 
%feature("docstring") bz_probw 
" put data to file
  NM - table identifier
  ob  - object start index (from 1) (row number);
  sw  - property start index (from 1) (col number);
  STR - data buffer;
  n   - number of objects (rows);
  m   - number of props (cols);
  priz- error code.
"; 
%feature("autodoc","1") bz_probnm; 
%feature("docstring") bz_probnm 
" get table dimensions by name
N- number of nrows
M- number columns
"; 
%apply int *OUTPUT { int *K };
void      bz_proba(const char *NM,int ob,int sw,void* STR,int N,int M,int *K);
void      bz_probw(const char *NM,int ob,int sw,void* STR,int N,int M,int *K);
%clear int *K;
//! ����������� ������ � �������
void      bz_start(const char *PAR);
//! ����� ������ � �������
void      bz_stop();

%apply int *OUTPUT { int *N };
%apply int *OUTPUT { int *M };
void      bz_probnm(const char *NAME,int * N,int* M);
%clear int *N;
%clear int *M;
%feature("autodoc","1") v_get_h; 
void listkat(const char* file);
void  bz_probpa(const char *NM,char *out);

%pythoncode %{

import numpy

class Tenrg_error:
    pass
def bz_get(nm,typ):
    "file[nm] -> numpy.array('typ')"
    sizes=bz_probnm(nm)
    if sizes == [0,0]: return None
    sizes=[i for i in sizes if i>1]
    if typ=='h':
    	sizes[0]*=2
    ret=numpy.zeros(sizes,typ)
    adr=ret.__array_interface__['data'][0]
    k=1
    k=bz_proba(nm,1,1,adr,len(ret.data)/4,1)
    if k!=0:
        err=Tenrg_error()
        err.msg="read fail"
    return ret

def bz_put(nm,arr):
    "numpy.array('typ') -> file[nm]"
    sizes=bz_probnm(nm)
    if sizes == [0,0]:
        err=Tenrg_error()
        err.msg="element not found"
        raise err
    adr=arr.__array_interface__['data'][0]
    bufsize=len(arr.data)
    k=1
    k=bz_probw(nm,1,1,adr,bufsize/4,1)
    if k!=0:
        err=Tenrg_error()
        err.msg="write fail"
        raise err
%}    
