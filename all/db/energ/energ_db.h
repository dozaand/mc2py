#ifndef energiacls_h
#define energiacls_h
#include <string>

using namespace std;

typedef unsigned short int rad50;
#define MAXKAT 30
#define MAX 614
#pragma pack(2)

typedef
struct KAT		                      /* �������*/
        {
	rad50 NAMTOS[3];                  /* ��� tos �*/
    int NBTOS;                        /* ����� ����� ������ tos �*/
   	unsigned short int NOBJ;		  /* ����� �������� */
   	unsigned short int NCB;		      /* ����� ������� */
   	unsigned char FLATOS;	          /* ���� ���������*/
	  /*
	    0- �����p�;
	    1- ����� ��������;
	    2- ����� �������;
	    3- �����;
	    4- 0 - ��������, 1 - �����;
	    5- ������
	    6- �p������� ���	*/
        } KATALOG; /* ����� : 20 ����. � ����� 25 ���������*/
#pragma pack()

class Tdb_energ
{
 int NBL ;    /* ����� ��������*/
 short int MAXBLK;   /* ������������ ����� ������ �� ����� */
 short int NUKAT;
 short int *BUST;
 KATALOG *UK;                 /* ��������� �� ������� */
 KATALOG *UKAT;               /* ��������� �� ������� */
 float *MEMM;

/*     ������� N:0 - ��������   
                    1 - BUST
                    N - N-TYJ tos */

 short int TMPTOS[MAXKAT];    /* ������ ��������� ��������� tos */
 short int NU;                /* ���������� ��������� tos ,
                                 ������������ ������ */

short int PRR;               /* ������ BUST,MAXBUF � �������� � ������*/

FILE *FP;
//-----------------------------------------------
void  TST(void);
//! �������������� ����� � ������.
/*!
       \param[in]: k- ���������� ���� tos,
       \param[in]        NM- ������ ���� tos,
        \param[in]       N- ������ ���������� ��������
        \param[in]      M- ������ ��������� �������
        \param[in]       L- ������ ��������� tos.
        \param[in]   SC- ������ ���� �����
 */
void  ALLOC(int *K1,char *NM,int *N,int *M,int *L,int *SC);
//! ������� �����������
/*!
 \param[in]  T- ��� tos (RAD50),
 \return if >0 ����� ��������  
*/
short int HASH(unsigned short int *T);
//!����������� ����� ������
short int strl(char *N);
//!����� � ������ ��������� tos. 
/*!
\param[in]  NAME - ��� TOS
\return  �����: =-1-������(UK-> �� ����),   >0 - ����� ���������� �������� 
*/
short int FIND(char *NAME);
//! �������������� ���������� ������  � ����  RADIX-50
/*!
\param[in] N- ����� ������
\param[in] IN1- ������� ������
\param[out] OU- �������� ������
*/
void  ascr50(short int N,char *IN1,unsigned short int *OU);
//! ��������� ��������� ������ � ������
int   ALBL(short int N);
//! ��������� �� �������
void  ERR(char *TXT);
//! ��������� �� ������� � ������� �� ���������
void  erro(char *TXT);
//!         ����������� UK �� ��������� �������  NKAT- ����� �������� */
void  RKAT(unsigned short int NKAT);
//! ������������ � ������� ������ ������ 
/*!
\param[in]  NB - ����� ����� � �������� ����������         *
\param[in]  N  - ���������� ������                          *
\param[in]   K=0- ���������� , K=1- ������                      *
 */
void  FSB(short int NB,short int N,short int K);
//! ������ ��� � ������, �� ������� ��������� �������
unsigned short int sizetos();
//! len line
int   sizeline();
//! ����� �� count
int   sizepart(unsigned char *b,short int count);
//!  �������������� ����� � ������.
/*!
    \param[in]    ����:NM-��� ���,
    \param[in]    N- ���������� ��������, 
     \param[in]  M- ���������� �������
     \param[in]  L- ������� tos.
     \param[in]  SC- ������ ���� �����
*/
void  ALLOC1(char *NM,int *N,int *M,int *L,int *SC);
//!        �������������� ����� � ������.
/*!
 \param[in] NM-��� tos, 
\param[in] N- ���������� ��������
\param[in]  M- number,
 */
void  APPREC(char *NM,int *N,int *M);
//!        �������� TOS.
/* 
\param[in] K-���������� TOS
\param[in]    NM-����� TOS
 */
void  DELTOS(int *K1,char *NM);
//! ����� ���������� � �������� ������������  � ������ ���
/*!
\param[in]  NF-���������� ���������� ������������ � ������;
\param[in] NB-���������� �������� ������������.
������������ ����������  :ADR,MAXBLK
*/
void  FRBY(int *NF,int *NB);
//! ��������� ������ ��������
/*!
        ��������� ������ �������� KOS*2 ����
    �����: KOS-������ ���������� ������(� 4-� ������� ������) 
 */
void  GETP(float **IA,int *K);
//! �������� ���� ������
void  INITOS(short int argn,char *argc[]);
//! ������-������
/*!
\param[in,out] DD-���� ������ ������
\param[in] N-������� ������
\param[in] F=0-������,=1-������   */
void  PR(int T,int N,char *DD,short int F);
//!  ������-������ ���� �������
/*!
\param[in] NM-��� TOS
\param[in] I-��������� ����� ��������
\param[in,out] DD-����-������ ������
\param[in] N - ������� ������
*/
void  PROBOW(char *NM,int *I,char *DD,int *N);
//! ������ �������� �� ������.
/*!
\parm[in]  NM-��� TOS
\parm[in]         I-��������� ����� �������
\parm[in]         J-��������� ����� ��������
\parm[in,out]         STR-������(N*M)
\parm[in]         N-���������� ��������
\parm[in]         M-���������� �������
\parm[in]         K-�������(=0,���� ���� ������,=12345 -���������������*/
void  PROBOA(char *NM,int *I,char  *DD,int *N);
//! ������ ��������
/*----------------------------------------------------------
\param[in]  NM-��� TOS
\param[in,out]  DD-���� ������ ������
*/
void  PROBPA(char *NM,char  *DD);
//! ������ ��������
/*----------------------------------------------------------
\param[in]  NM-��� TOS
\param[in,out]  DD-���� ������ ������
*/
void  PROBPW(char *NM,char  *DD);
//! ����������� ������� ��� � ������ � �� ������� ����������� tos � ������
/*!
 \param[in] N-���������� ��������
 \param[in]   M- ���������� �������
*/
void  PROBNM(char *NAME,int *N,int *M);
//!  ������-������ ���� �������
/*!
\param[in] NM-��� TOS
\param[in,out] DD-����-������ ������
\param[in] N - ������� ������
*/
void  PROBIS(char *NM,char  *DD,int *N);
//!  ������-������ ���� �������
/*!
\param[in] NM-��� TOS
\param[in] I-��������� ����� ��������
\param[in,out] DD-����-������ ������
\param[in] N - ������� ������
*/
void  PROBCW(char *NM,int *I,char  *DD,int *N);
void  PROBW(char *NM,int *I,int *J,char  *STR,int *N,int *M,int *K);
//!  ������-������ ���� �������
/*!
\param[in] NM-��� TOS
\param[in] I-��������� ����� ��������
\param[in,out] DD-����-������ ������
\param[in] N - ������� ������
*/
void  PROBCA(char *NM,int *I,char  *DD,int *N);
//! �������������� tos��.
/*  ---------------------------------------------------
 \param[in]   k-����������
 \param[in]  NM-������ ������ � ����� ���� 
 */
void  RENAME(int *K1,char *NM);
//! �������������� RADIX-50 - ASCII
void  rasc(unsigned short int *IN,char *OU);
/*!-----------------------------------------------
        ������ �������� �� ������.
        NM-��� TOS
        I-��������� ����� �������
        J-��������� ����� ��������
        STR-������(N*M)
        N-���������� ��������
        M-���������� �������
        K-�������(=0,���� ���� ������,=12345 -���������������*/
void  PROBA(char *NM,int *I,int *J,char *STR,int *N,int *M,int *K);
//-----------------------------------------------
short int CMPST(rad50 *N,rad50 *M);
void ShowMessage(const char* nm);
 string filename;
public:
//! ����������� ������ � �������
void  START(const char *PAR);
//! ����� ������ � �������
void  STOP();
 Tdb_energ(const char* nm):MEMM(NULL)
 {
  filename=nm;
  START(nm);
 }
 ~Tdb_energ()
 {
   STOP();
 }
 void  bz_probnm(const char *NAME,int * N,int* M);
 void  bz_proba(const char *NM,int ob,int sw,void *STR,int N,int M,int *K);
 void  bz_probw(const char *NM,int ob,int sw,void *STR,int N,int M,int *K);
};

void pt(int a);


#endif