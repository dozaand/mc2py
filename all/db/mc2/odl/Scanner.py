
import sys

class Token( object ):
   def __init__( self ):
      self.kind   = 0     # token kind
      self.pos    = 0     # token position in the source text (starting at 0)
      self.col    = 0     # token column (starting at 0)
      self.line   = 0     # token line (starting at 1)
      self.val    = u''   # token value
      self.next   = None  # AW 2003-03-07 Tokens are kept in linked list


class Position( object ):    # position of source code stretch (e.g. semantic action, resolver expressions)
   def __init__( self, buf, beg, len, col ):
      assert isinstance( buf, Buffer )
      assert isinstance( beg, int )
      assert isinstance( len, int )
      assert isinstance( col, int )
      
      self.buf = buf
      self.beg = beg   # start relative to the beginning of the file
      self.len = len   # length of stretch
      self.col = col   # column number of start position

   def getSubstring( self ):
      return self.buf.readPosition( self )

class Buffer( object ):
   EOF      = u'\u0100'     # 256

   def __init__( self, s ):
      self.buf    = s
      self.bufLen = len(s)
      self.pos    = 0
      self.lines  = s.splitlines( True )

   def Read( self ):
      if self.pos < self.bufLen:
         result = unichr(ord(self.buf[self.pos]) & 0xff)   # mask out sign bits
         self.pos += 1
         return result
      else:
         return Buffer.EOF

   def ReadChars( self, numBytes=1 ):
      result = self.buf[ self.pos : self.pos + numBytes ]
      self.pos += numBytes
      return result

   def Peek( self ):
      if self.pos < self.bufLen:
         return unichr(ord(self.buf[self.pos]) & 0xff)    # mask out sign bits
      else:
         return Scanner.buffer.EOF

   def getString( self, beg, end ):
      s = ''
      oldPos = self.getPos( )
      self.setPos( beg )
      while beg < end:
         s += self.Read( )
         beg += 1
      self.setPos( oldPos )
      return s

   def getPos( self ):
      return self.pos

   def setPos( self, value ):
      if value < 0:
         self.pos = 0
      elif value >= self.bufLen:
         self.pos = self.bufLen
      else:
         self.pos = value

   def readPosition( self, pos ):
      assert isinstance( pos, Position )
      self.setPos( pos.beg )
      return self.ReadChars( pos.len )

   def __iter__( self ):
      return iter(self.lines)

class Scanner(object):
   EOL     = u'\n'
   eofSym  = 0

   charSetSize = 256
   maxT = 44
   noSym = 44
   start = [
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  6,  0,  0,  0,  0,  0, 26, 25, 27, 22, 16, 23,  0, 24,
     1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  9, 12, 19, 21, 20,  0,
     0,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,
     2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, 17,  0, 18,  0,  2,
     0,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,
    28,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, 10,  0, 11,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     -1]


   def __init__( self, s ):
      self.buffer = Buffer( unicode(s) ) # the buffer instance
      
      self.ch        = u'\0'       # current input character
      self.pos       = -1          # column number of current character
      self.line      = 1           # line number of current character
      self.lineStart = 0           # start position of current line
      self.oldEols   = 0           # EOLs that appeared in a comment;
      self.NextCh( )
      self.ignore    = set( )      # set of characters to be ignored by the scanner
      self.ignore.add( ord(' ') )  # blanks are always white space
      self.ignore.add(9) 
      self.ignore.add(10) 
      self.ignore.add(13) 

      # fill token list
      self.tokens = Token( )       # the complete input token stream
      node   = self.tokens

      node.next = self.NextToken( )
      node = node.next
      while node.kind != Scanner.eofSym:
         node.next = self.NextToken( )
         node = node.next

      node.next = node
      node.val  = u'EOF'
      self.t  = self.tokens     # current token
      self.pt = self.tokens     # current peek token

   def NextCh( self ):
      if self.oldEols > 0:
         self.ch = Scanner.EOL
         self.oldEols -= 1
      else:
         self.ch = self.buffer.Read( )
         self.pos += 1
         # replace isolated '\r' by '\n' in order to make
         # eol handling uniform across Windows, Unix and Mac
         if (self.ch == u'\r') and (self.buffer.Peek() != u'\n'):
            self.ch = Scanner.EOL
         if self.ch == Scanner.EOL:
            self.line += 1
            self.lineStart = self.pos + 1
      



   def Comment0( self ):
      level = 1
      line0 = self.line
      lineStart0 = self.lineStart
      self.NextCh()
      if self.ch == '/':
         self.NextCh()
         while True:
            if ord(self.ch) == 10:
               level -= 1
               if level == 0:
                  self.oldEols = self.line - line0
                  self.NextCh()
                  return True
               self.NextCh()
            elif self.ch == Buffer.EOF:
               return False
            else:
               self.NextCh()
      else:
         if self.ch == Scanner.EOL:
            self.line -= 1
            self.lineStart = lineStart0
         self.pos = self.pos - 2
         self.buffer.setPos(self.pos+1)
         self.NextCh()
      return False

   def Comment1( self ):
      level = 1
      line0 = self.line
      lineStart0 = self.lineStart
      self.NextCh()
      if self.ch == '*':
         self.NextCh()
         while True:
            if self.ch == '*':
               self.NextCh()
               if self.ch == '/':
                  level -= 1
                  if level == 0:
                     self.oldEols = self.line - line0
                     self.NextCh()
                     return True
                  self.NextCh()
            elif self.ch == '/':
               self.NextCh()
               if self.ch == '*':
                  level += 1
                  self.NextCh()
            elif self.ch == Buffer.EOF:
               return False
            else:
               self.NextCh()
      else:
         if self.ch == Scanner.EOL:
            self.line -= 1
            self.lineStart = lineStart0
         self.pos = self.pos - 2
         self.buffer.setPos(self.pos+1)
         self.NextCh()
      return False


   def CheckLiteral( self ):
      lit = self.t.val
      if lit == "class":
         self.t.kind = 6
      elif lit == "static":
         self.t.kind = 14
      elif lit == "public":
         self.t.kind = 16
      elif lit == "char":
         self.t.kind = 19
      elif lit == "byte":
         self.t.kind = 20
      elif lit == "short":
         self.t.kind = 21
      elif lit == "int":
         self.t.kind = 22
      elif lit == "longlong":
         self.t.kind = 23
      elif lit == "long":
         self.t.kind = 24
      elif lit == "float":
         self.t.kind = 25
      elif lit == "double":
         self.t.kind = 26
      elif lit == "Tdatetime":
         self.t.kind = 27
      elif lit == "vector":
         self.t.kind = 29
      elif lit == "queue":
         self.t.kind = 32
      elif lit == "const_vector":
         self.t.kind = 33
      elif lit == "map":
         self.t.kind = 34
      elif lit == "database":
         self.t.kind = 35
      elif lit == "typedef":
         self.t.kind = 36
      elif lit == "const":
         self.t.kind = 38


   def NextToken( self ):
      while ord(self.ch) in self.ignore:
         self.NextCh( )
      if (self.ch == '/' and self.Comment0() or self.ch == '/' and self.Comment1()):
         return self.NextToken()

      self.t = Token( )
      self.t.pos = self.pos
      self.t.col = self.pos - self.lineStart + 1
      self.t.line = self.line
      state = self.start[ord(self.ch)]
      buf = u''
      buf += unicode(self.ch)
      self.NextCh()

      done = False
      while not done:
         if state == -1:
            self.t.kind = Scanner.eofSym     # NextCh already done
            done = True
         elif state == 0:
            self.t.kind = Scanner.noSym      # NextCh already done
            done = True
         elif state == 1:
            if (self.ch >= '0' and self.ch <= '9'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 1
            else:
               self.t.kind = 1
               done = True
         elif state == 2:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 3:
            if self.ch == '*':
               buf += unicode(self.ch)
               self.NextCh()
               state = 4
            else:
               self.t.kind = Scanner.noSym
               done = True
         elif state == 4:
            self.t.kind = 3
            done = True
         elif state == 5:
            self.t.kind = 4
            done = True
         elif state == 6:
            if (ord(self.ch) == 9
                 or self.ch == ' '
                 or self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 7
            else:
               self.t.kind = Scanner.noSym
               done = True
         elif state == 7:
            if (ord(self.ch) == 9
                 or self.ch == ' '
                 or self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 7
            elif self.ch == '"':
               buf += unicode(self.ch)
               self.NextCh()
               state = 8
            else:
               self.t.kind = Scanner.noSym
               done = True
         elif state == 8:
            self.t.kind = 5
            done = True
         elif state == 9:
            self.t.kind = 7
            done = True
         elif state == 10:
            self.t.kind = 8
            done = True
         elif state == 11:
            self.t.kind = 9
            done = True
         elif state == 12:
            self.t.kind = 10
            done = True
         elif state == 13:
            self.t.kind = 11
            done = True
         elif state == 14:
            self.t.kind = 12
            done = True
         elif state == 15:
            self.t.kind = 13
            done = True
         elif state == 16:
            self.t.kind = 15
            done = True
         elif state == 17:
            self.t.kind = 17
            done = True
         elif state == 18:
            self.t.kind = 18
            done = True
         elif state == 19:
            self.t.kind = 30
            done = True
         elif state == 20:
            self.t.kind = 31
            done = True
         elif state == 21:
            self.t.kind = 37
            done = True
         elif state == 22:
            self.t.kind = 39
            done = True
         elif state == 23:
            self.t.kind = 40
            done = True
         elif state == 24:
            self.t.kind = 41
            done = True
         elif state == 25:
            self.t.kind = 43
            done = True
         elif state == 26:
            if self.ch == '*':
               buf += unicode(self.ch)
               self.NextCh()
               state = 3
            else:
               self.t.kind = 42
               done = True
         elif state == 27:
            if self.ch == ')':
               buf += unicode(self.ch)
               self.NextCh()
               state = 5
            else:
               self.t.kind = 28
               done = True
         elif state == 28:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'q'
                 or self.ch >= 's' and self.ch <= 't'
                 or self.ch >= 'v' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'u':
               buf += unicode(self.ch)
               self.NextCh()
               state = 29
            elif self.ch == 'r':
               buf += unicode(self.ch)
               self.NextCh()
               state = 30
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 29:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch == 'a'
                 or self.ch >= 'c' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'b':
               buf += unicode(self.ch)
               self.NextCh()
               state = 31
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 30:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'h'
                 or self.ch >= 'j' and self.ch <= 'n'
                 or self.ch >= 'p' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'o':
               buf += unicode(self.ch)
               self.NextCh()
               state = 32
            elif self.ch == 'i':
               buf += unicode(self.ch)
               self.NextCh()
               state = 33
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 31:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'k'
                 or self.ch >= 'm' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'l':
               buf += unicode(self.ch)
               self.NextCh()
               state = 34
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 32:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 's'
                 or self.ch >= 'u' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 't':
               buf += unicode(self.ch)
               self.NextCh()
               state = 35
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 33:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'u'
                 or self.ch >= 'w' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'v':
               buf += unicode(self.ch)
               self.NextCh()
               state = 36
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 34:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'h'
                 or self.ch >= 'j' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'i':
               buf += unicode(self.ch)
               self.NextCh()
               state = 37
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 35:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'd'
                 or self.ch >= 'f' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'e':
               buf += unicode(self.ch)
               self.NextCh()
               state = 38
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 36:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'b' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'a':
               buf += unicode(self.ch)
               self.NextCh()
               state = 39
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 37:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'b'
                 or self.ch >= 'd' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'c':
               buf += unicode(self.ch)
               self.NextCh()
               state = 40
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 38:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'b'
                 or self.ch >= 'd' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'c':
               buf += unicode(self.ch)
               self.NextCh()
               state = 41
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 39:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 's'
                 or self.ch >= 'u' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 't':
               buf += unicode(self.ch)
               self.NextCh()
               state = 42
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 40:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == ':':
               buf += unicode(self.ch)
               self.NextCh()
               state = 13
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 41:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 's'
                 or self.ch >= 'u' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 't':
               buf += unicode(self.ch)
               self.NextCh()
               state = 43
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 42:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'd'
                 or self.ch >= 'f' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'e':
               buf += unicode(self.ch)
               self.NextCh()
               state = 44
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 43:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'd'
                 or self.ch >= 'f' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'e':
               buf += unicode(self.ch)
               self.NextCh()
               state = 45
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 44:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == ':':
               buf += unicode(self.ch)
               self.NextCh()
               state = 15
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 45:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'c'
                 or self.ch >= 'e' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == 'd':
               buf += unicode(self.ch)
               self.NextCh()
               state = 46
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t
         elif state == 46:
            if (self.ch >= '0' and self.ch <= '9'
                 or self.ch >= 'A' and self.ch <= 'Z'
                 or self.ch == '_'
                 or self.ch >= 'a' and self.ch <= 'z'):
               buf += unicode(self.ch)
               self.NextCh()
               state = 2
            elif self.ch == ':':
               buf += unicode(self.ch)
               self.NextCh()
               state = 14
            else:
               self.t.kind = 2
               self.t.val = buf
               self.CheckLiteral()
               return self.t

      self.t.val = buf
      return self.t

   def Scan( self ):
      self.t = self.t.next
      self.pt = self.t.next
      return self.t

   def Peek( self ):
      self.pt = self.pt.next
      while self.pt.kind > self.maxT:
         self.pt = self.pt.next

      return self.pt

   def ResetPeek( self ):
      self.pt = self.t

