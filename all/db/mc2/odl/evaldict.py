import sys

class EvalDict:
    """usage: "___%(ech(a,'asd'))s _____" % evaldict.EvalDict() - interpolate strings"""
    def __init__(self, globals=None, locals=None):
        if globals is None:
            globals = sys._getframe(1).f_globals
        self.globals = globals
        if locals is None:
            locals = sys._getframe(1).f_locals
        self.locals = locals

    def __getitem__(self, key):
        key = key % self
        return eval(key, self.globals, self.locals)
