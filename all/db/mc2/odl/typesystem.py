#from struct import *

typedata=[
["Tbyte","byte",1,"b"],
["Tchar","char",1,"c"],
["Tshort","short",2,"b"],
["Tint","int",4,"i"],
["Tlong","long",4,"l"],
["Tlonglong","long long",8,"q"],
["Tfloat","float",4,"f"],
["Tdouble","double",8,"d"],
["Tdatetime","Tdatetime",8,"d"]
]
psize=8

class Ttype:
    "base class for all types"
    pass

for li in typedata:
    exec("class %(typename)s(Ttype):\n\tname=\"%(name)s\"\n\tsize=%(size)d\n\tfmt=\"%(fmt)s\"" % {"typename":li[0],"name":li[1],"size":li[2],"fmt":li[3]})

class Tarray(Ttype):
    "type for array of fixed size"
    def __init__(self,typ,n):
        self.typ=typ
        self.n=n
        self.size=n*typ.size
def MultiArray(typ,arr):
    "for int a[5][4] define array recursevely arr(arr(arr(int),4),5) typ= is type arr - list of dimensions"
    if len(arr) == 1:
        return Tarray(typ,arr[0])
    else:
        arrn=arr[1:];
        n=arr[0]
        return Tarray(MultiArray(typ,arrn),n)

class Tvector(Ttype):
    "type for array of var size"
    def __init__(self,typ):
        self.typ=typ
        self.size=3*psize

class Tconst_vector(Ttype):
    "type for array of var size"
    def __init__(self,typ,n):
        self.typ=typ
        self.n=n
        self.size=psize+n*typ.size

class Tqueue(Ttype):
    "type for queue "
    def __init__(self,typ,n):
        self.typ=typ
        self.n=n
        self.size=psize+n*typ.size

class Tmap(Ttype):
    "type for queue "
    def __init__(self,typkey,typval):
        self.typkey=typkey
        self.typval=typval
        self.size=3*psize

class Tref(Ttype):
    "type for data reference (virtual) "
    def __init__(self,typ):
        self.typ=typ
        self.size=psize+psize

class Tdb(Ttype):
    "type for reference to database"
    def __init__(self,nm):
        self.size=300
#        self.filename=nm

class Tmember:
    "description of class member"
    def __init__(self,isStatic,typ,name):
        self.static=isStatic
        self.name=name
        self.typ=typ
        self.size=typ.size

class Tclass(Ttype):
    "type class def"
    def __init__(self):
        self.meta={}
        self.name=""
        self.members=[]
        self.bases=[]

    def Update(self):
        "check for name duplicates and calcualte members offset"
        self.membnames=[x.name for x in self.members]
        if len(self.membnames)!=len(set(self.membnames)):
            raise Exception("name redefinition")
        self.UpdateSize()

    def UpdateSize(self):
        su=0
        for obj in self.bases:
            obj.offset=su
            su+=obj.size
        for obj in self.members:
            obj.offset=su
            su+=obj.size
        self.size=su;

def maincls():
    "test type descriptor creation"
    Ta=Tint()
    Tarr=Tarray(Tint(),8)
    print 32,"==",Tarr.size
    Tvect=Tvector(Tint())
    Tque=Tqueue(Tdouble(),20)
    memba=Tmember(Tque,"a")
    membb=Tmember(Tarr,"b")
    Tcls=Tclass("Tcls",[memba,membb])
    Tclsb=Tclass("Tclsb",
    [
    Tmember(Tint(),"a"),
    Tmember(Tvect,"b"),
    Tmember(Tque,"b"),
    ])
    print Tcls
#    print Tclsb.size
def sf(a,v):
    print a,v
    v+=1
    if len(a) != 0:
        a.pop()
        sf(a,v)


#    Ta=Tclass("Ta",[Tmember(Tint,"a"),Tmember(Tint,"b")])
def main():
    a=[1,2,3]
    a=MultiArray(Tint(),[3,4,5])
    print a

if __name__ == '__main__':
    main()