import re
import yaml
from itertools import groupby
from collections import defaultdict
import argparse

rr = re.compile(r"^(\W*)([\w\-]+)( +(.+))?$")

class grpcnt:
    def __init__(self):
        self.count = 0

    def __call__(self, obj):
        if len(obj[0]) == 0:
            self.count += 1
        return self.count


def D(x):
    "преобразование свойства в массивы и интерпретация массивов чисел"
    res = {}
    res['a'] = x[1]
    if x[-1] is None:
        return res
    if x[-1].endswith("BW"):
        dsc = x[-1][:-2].split()
    else:
        dsc = x[-1].split()
    try:
        val = [int(i) for i in dsc]
        if len(val)==1:
            val=val[0]
    except ValueError:
        try:
            val = [float(i) for i in dsc]
            if len(val)==1:
                val=val[0]
        except ValueError:
            val = x[-1].strip()
            if val == "UNDEFINED":
                val=None
    if val:
        res['v'] = val
    return res

#dkt={"x":2, "ATTRIBUTE13" : "Hfluid","asd-ATTRIBUTE14" : "Hgas","ATTRIBUTE15" : "MassFluxFluid","ATTRIBUTE16" : "MassFluxGas","ATTRIBUTE17" : "BoundCell1"}

reattr = re.compile(r"((\w+)-)?ATTRIBUTE(\d+)")

def attr_compress(dkt):
    "сжатие атрибутов в массив сжатие делаем на месте"
    attrdata = defaultdict(dict)
    rmkeys = []
    # все что похоже на нумеровынные аттрибуты добавляем в список на удаление а данные запоминаем
    for k in dkt:
        fnd = re.match(reattr,k)
        if fnd:
            tmp,grpnm,ind = fnd.groups()
            if grpnm is None:
                grpnm = "ATTR"
            ind=int(ind)
            attrdata[grpnm][ind] = dkt[k]
            rmkeys.append(k)
    # чистим исходный словарик
    for i in rmkeys:
        del dkt[i]
    # создаем списки
    for k,v in attrdata.items():
        cnt = max(v)
        vv=[""]*cnt
        for kk,dktvals in v.items():
            vv[kk-1] = dktvals
        dkt[k] = vv
    return dkt

def pcf2yaml(file_in, file_out):
    "трубопроводы парсим и запихиваем в yaml"

    # чтение
    rex = []
    # продублировал чтение чтобы можно было в двух кодировках читать
    try:
        with open(file_in, "rt", encoding="cp1251") as f:
            for i in f:
                rex.append(re.match(rr, i).groups())
    except:
        with open(file_in, "rt", encoding="utf-8") as f:
            for cnt,i in enumerate(f):
                try:
                    rex.append(re.match(rr, i).groups())
                except:
                    print("fail on line ",cnt)
                    print(i)
                    raise

    # конвертация
    res = []
    for k, vv in groupby(rex, grpcnt()):
        x = list(vv)
        xx = x[0]
        obj = {"a": xx[1]}
        if xx[-1]:
            obj['d'] = xx[-1]
        if len(x) > 1:
            dkt={}
            dktcnt={}
            for i in x[1:]: # сую все в словарь в надежде что ключи не будут повторяться но если повторятся буду значения совать в список
                item = D(i)
                if 'v' in item:
                    k = item['a']
                    v = item['v']
                    if k in dktcnt:
                        if dktcnt[k] == 1:
                            dkt[k] = [dkt[k],v]
                            dktcnt[k] = 2
                        else:
                            dkt[k].append(v)
                    else:
                        dktcnt[k]=1
                        dkt[k] = v
            attr_compress(dkt)
            obj['i'] = dkt
        res.append(obj)

    # запись
    with open(file_out, "wt", encoding="utf-8") as f:
        yaml.dump(res,f,allow_unicode=True)

# import glob

# for i in glob.glob("TG/*.pcf"):
#     print(i)
#     pcf2yaml(i, i+".yaml")


# pcf2yaml("RY/3RY10Z01.pcf", "x.yaml")
# pcf2yaml(r"C:\PROJECTS\tmp\14_2\PCFConverter\TESTS\10JEC11BR001.pcf", "y.yaml")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="convert pcf to yaml file")
    parser.add_argument("input", help=u"input file")
    parser.add_argument("-o","--output", help=u"output name")
    args = parser.parse_args()
    pcf2yaml(args.input, args.output)
