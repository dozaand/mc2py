#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

def write_namelist(f,namelist_name,datadict,term='/'):
    u"""write to file dictionary as namelist"""
    f.write("&"+namelist_name+"\n")
    for k,v in datadict.iteritems():
        if type(v) is np.ndarray:
            v=list(v.flatten())
        datastr = str(v)
        datastr=datastr.replace('[',"")
        datastr=datastr.replace(']',"")
        f.write("{0} = {1},\n".format(k,datastr))
    f.write(term+"\n")

def write_namelist_array(f,arr,term='/'):
    u"""write to file array of namelists"""
    for v in arr:
        write_namelist(f,v[0],v[1],term)
