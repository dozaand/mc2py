#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
����������� ������ ������ �� ������ ����� � ������
����� GUI
"""
import wx
import os
import argparse
import shutil


class SFrame(wx.Frame):
    def __init__(self, parent, src, dest, name='Copy Files', wildcard="*.*", move=0):
        wx.Frame.__init__(self, parent, -1, name, size=(300, 100))
        res = _RunDialog(self, src, dest, name, wildcard, move)
        self.Close()


def _RunDialog(parent, src, dest, name='Copy Files', wildcard="*.*", move=0):
    dlg = wx.FileDialog(
        parent, message=name,
        defaultDir=src,
        defaultFile="",
        wildcard=wildcard,
        style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
    )
    res = []
    dest = os.path.abspath(dest)
    if dlg.ShowModal() == wx.ID_OK:
        # This returns a Python list of files that were selected.
        paths = dlg.GetPaths()
        if move:
            for i in paths:
                p, nm = os.path.split(i)
                shutil.move(i, os.path.join(dest, nm))
                res.append(os.path.join(dest, nm))
        else:
            for i in paths:
                p, nm = os.path.split(i)
                shutil.copy(i, os.path.join(dest, nm))
                res.append(os.path.join(dest, nm))
    dlg.Destroy()
    return res


def CopyTo(parent=None, src=".", dest=".", name=u"�������� �����", wildcard="*.*", move=0):
    u"�������������� ������������� �������"
    if not parent:
        global app
        app = wx.PySimpleApp(redirect=True)
        frame = SFrame(None, src, dest, name, wildcard, move)
        frame.Show(True)
        app.MainLoop()
    else:
        return _RunDialog(parent, src, dest, name, wildcard, move)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="select and copy files from src directory to dest directory")
    parser.add_argument("--src", default=".", help=u"sourse directory")
    parser.add_argument(
        "--name", default=u"�������� �����", help=u"dialog title")
    parser.add_argument(
        "-m", "--move", nargs="?", const=1, default=0, help=u"dialog title")
    parser.add_argument(
        "--wildcard", default="*.*", help=u"wildcards for inpyut files")
    parser.add_argument("-t", "--dest", default=".", help=u"target directory")
    args = parser.parse_args()
    CopyTo(None, args.src, args.dest, name=args.name,
           wildcard=args.wildcard, move=args.move)
