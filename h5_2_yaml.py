#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mc2py.h52obj import load
from mc2py.fold import conv_val,fmap
import numpy as np
import yaml
import argparse
import os


def from_h5_to_yaml(h5_name="kaln.h5",yaml_name="sup.yaml"):
    u"""����� ������ �� h5 � yaml
    h5_name ��� h5 �����
    yaml_name ��� yaml �����"""
    obj=load(h5_name)

    data_to_save=fmap(obj,conv_val,conv_val)

    with open(yaml_name,"w") as f:
        yaml.dump(data_to_save,f)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="convert h5 to yaml format")
    parser.add_argument("file", help=u"sourse directory")
    args = parser.parse_args()
    outname = os.path.splitext(args.file)[0]+".yaml"
    from_h5_to_yaml(h5_name=args.file,yaml_name=outname)
