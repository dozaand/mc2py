#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
�������� ���������� �� ���������� ��� ������ �� ���������� �����������

"""
import numpy as np


class TLinearPerson(object):
    u"""����� ������� ��� ������� ����� � �������� �����������
    �� ����� �������� - ����������� �������� � ��������
    �������� ������� ���
    \\sum_prop love.prop*li[i].prop
    �� ������ ������ ������������� �� �������� ��������"""
    def __init__(self, lovedict):
        self.lovedict = lovedict
        self.k0 = set(lovedict.keys())

    def __call__(self, obj):
        k1 = set(obj.keys())
#        k1=set(obj.__dict__.keys())
        isect = self.k0.intersection(k1)
        amount = np.array([obj[nm] for nm in isect], dtype='f')
        value = np.array([self.lovedict[nm] for nm in isect], dtype='f')
        return -np.dot(amount, value)  # ����� ����������� �� ��������

# ��������� �������� ������������ ��������
dsc = dict(
    priseNow=u"������� ��������",
    priseMax=u"���������� �������� ����� 10 ���",
    personal=u"��������� ������� � ������ �� ������ (���� �������� �� ������������� ���� ��� - �������������) �������� - ������������ - (-1) ������ ������� -1",
    science=u"����������� ���������� ������",
    flat=u"��������� �����",
    career=u"����������� ���������� ����� (�� ��������������� ������� �� ���������� ������� ������)"
)

# ��� ��������� ��������� ����������� ������� �
AndreyDruzaev = TLinearPerson(dict(
                              priseNow=1.,
                              priseMax=0.9,
                              # �������� ������ �������� ������ - ����� ������ �� ��� ����� �������� �� �� ������������, ����� ��� ������ ����������� ������ ����� 10 ��� 0.9 ������� �������� �������� ������� �������.
                              personal=50000.,
                              # ��������� ������� � ������ � ����� - �� ��� ������ ������ �������  �� �������� � �������� ��������
                              science=0.,
                              # �� ������� ����������� ������ ����� ��������� ... ��� � �����
                              flat=-20000.,
                              # ��� ���������� ����� ����� ���������� ����� �������� �� 20000 � � ����� � �� �� 5000 � ���������...
                              career=300.
                              ))

variants = [
    dict(name=u"������ + ������� ������ -> ������� ��������� ������",
         priseNow=2e4,
         priseMax=1.2e5,
         personal=0.5,
         science=1,
         flat=1,
         career=0.5
         ),
dict(name=u"������ -> ������� ��������� ������",
     priseNow=2e4,
     priseMax=1.8e5,
     personal=0.5,
     science=0.5,
     flat=0,
     career=0.8
     ),
dict(name=u"����� -> ���� �������",
     priseNow=2e3,
     priseMax=0.8e5,
     personal=0.5,
     science=1,
     flat=1,
     career=0.1
     ),
dict(name=u"����� -> ������� ����� ����",
     priseNow=2e3,
     priseMax=1.2e5,
     personal=0.5,
     science=1,
     flat=1,
     career=0.3
     ),
dict(name=u"����� -> ������ �� �� �������������",
     priseNow=2e4,
     priseMax=1.8e5,
     personal=0.,
     science=0,
     flat=0,
     career=1.
     ),
dict(name=u"����� -> ������",
     priseNow=2e4,
     priseMax=1.e5,
     personal=0.5,
     science=0.2,
     flat=0,
     career=0.
     ),
dict(name=u"����� -> ��� ���",
     priseNow=2e4,
     priseMax=0.7e5,
     personal=0.2,
     science=0,
     flat=0,
     career=0.5
     ),
dict(name=u"����� -> ���������",  # ???
     priseNow=2e4,
     priseMax=2.2e5,
     personal=0.5,
     science=1,
     flat=0.5,
     career=0.5
     ),
dict(name=u"������ -> ���������",
     priseNow=2e4,
     priseMax=2.2e5,
     personal=0.5,
     science=1,
     flat=0.5,
     career=0.7
     ),
dict(name=u"?? -> �������",
     priseNow=2e4,
     priseMax=1.5e5,
     personal=0.5,
     science=0,
     flat=0,
     career=0.3
     ),
dict(name=u"?? -> ����������",
     priseNow=2e4,
     priseMax=0.5e5,
     personal=-0.5,
     science=0,
     flat=0.2,
     career=0.5
     ),
dict(name=u"���-�� ��������",
     priseNow=2e4,
     priseMax=1.2e5,
     personal=0.,
     science=1,
     flat=0,
     career=0.
     ),
dict(name=u"���-�� ����������",
     priseNow=2e4,
     priseMax=1.8e5,
     personal=0.,
     science=1,
     flat=0,
     career=0.
     ),
dict(name=u"���-�� �������",
     priseNow=2e4,
    priseMax=1.e5,
    personal=0.,
    science=1,
    flat=0,
    career=0.
),
dict(name=u"���-�� �������",
    priseNow=2e4,
    priseMax=0.5e5,
    personal=0.,
    science=1,
    flat=0,
    career=0.
),
dict(name=u"������� �������������",
    priseNow=0,
    priseMax=0.8e5,
    personal=-1.,
    science=0,
    flat=0,
    career=0.
),
dict(name=u"���������� ��� -> �������",
    priseNow=0,
    priseMax=2.e5,
    personal=-1.,
    science=0,
    flat=0,
    career=1.1
),
dict(name=u"����������� ��� -> �������",
    priseNow=0,
    priseMax=2.e5,
    personal=-1.,
    science=0,
    flat=0,
    career=1.
)
]

"""��������� ������ �������� �� ����� ��������
� ��������� ������ li ������� �� ���������� (�������� ������������ ������� � �.�.)
person - ����� ��������� �������� ����������� ��� ��������� ����� (person(obj))
(����� ���� ����� ������� ��������� �������������� - ��� �� ��� ������ ���� � ����� ����� �����
����������� ����������� ������� ������ � ������ ������� �� ���� �� ���� ������)
"""

ss = sorted(variants, key=AndreyDruzaev.__call__)

# � ��� �������� �������
for d in ss[:10]:
    print d["name"], -AndreyDruzaev(d)
