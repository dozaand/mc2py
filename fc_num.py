#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import re,sys
from itertools import izip

_floatnum=re.compile(r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?")

diff=None

def sc_num(data1,data2,rtol=1e-5,atol=1e-6):
    u"""compare string numbers with predefined presision"""
    global diff
    for ref1,ref2 in izip(_floatnum.finditer(data1),_floatnum.finditer(data2)):
        try:
            f1=float(ref1.group(0))
            f2=float(ref2.group(0))
        except:
            print "strange numbers:",ref1.group(0),ref2.group(0)
        denom=max(abs(f1),abs(f2),atol)
        if abs(f1-f2)/denom>rtol:
            diff=(ref1.group(0),ref2.group(0))
            return 0
    return 1

def fc_num(file1,file2,rtol=1e-5,atol=1e-6):
    u"""compare file numbers with predefined presision"""
    with open(file1,"rt") as f:
        data1=f.read()
    with open(file2,"rt") as f:
        data2=f.read()
    return sc_num(data1,data2,rtol,atol)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="fc_num",description="compare files numerically")
    parser.add_argument("-a","--atol",default=1e-7,type=float,help=u"absolute error")
    parser.add_argument("-r","--rtol",default=1e-5,type=float,help=u"relative error")
    parser.add_argument("files",nargs=2,help=u"files to compare")
    args = parser.parse_args()
    i=fc_num(args.files[0],args.files[1],rtol=args.rtol,atol=args.atol)
    if i:
        sys.exit(0)
    else:
        print args.files,diff
        sys.exit(-1)