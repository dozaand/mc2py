#!/usr/bin/env python
# -*- coding: utf-8 -*-

from operator import *
import numpy as np
import copy


def FoldList(f, x0, li):
    u"""FoldList(f,x0,[a,b,c]) -> [f(x0,a),f(f(x0,a),b),f(f(f(x0,a),b),c)]
>>> FoldList(lambda x,y:[x,y],55,range(5))
[[55, 0],
 [[55, 0], 1],
 [[[55, 0], 1], 2],
 [[[[55, 0], 1], 2], 3],
 [[[[[55, 0], 1], 2], 3], 4]]
"""
    if len(li) == 0:
        return []
    r = f(x0, li[0])
    if len(li) == 1:
        return [r]
    else:
        return [r]+FoldList(f, r, li[1:])


def Total(li):
    u"""сложение всего что есть в списке"""
    reduce(lambda a, b: a+b, li)


def Accumulate(li):
    u"""[a, a + b, a + b + c, a + b + c + d]"""
    return FoldList(lambda x, y: x+y, 0, li)


def Multipulate(li):
    u"""[a, a * b, a * b * c, a * b * c * d]"""
    return FoldList(lambda x, y: x*y, 1, li)


def Fold(f, x0, li):
    u"""Fold(f,x0,[a,b,c]) -> f(f(f(x0,a),b),c)
>>> Fold(lambda x,y:[x,y],55,range(5))
[[[[[55, 0], 1], 2], 3], 4]"""
    r = f(x0, li[0])
    if len(li) == 1:
        return r
    else:
        return Fold(f, r, li[1:])


def ZipFlatten(*arg):
    u"""строит ряды данных рядом уничтожая внутреннюю структуру данных:
>>> li1=range(1,6)
>>> li2=[[i,'x'] for i in range(6)]
>>> list(ZipFlatten(li1,li2))
[[1, 0, 'x'], [2, 1, 'x'], [3, 2, 'x'], [4, 3, 'x'], [5, 4, 'x']]"""
    for li in zip(*arg):
        yield Flatten(li)


def NestList(f, x0, n):
    u"""NestList(f,3) -> [f(x0),f(f(x0)),f(f(f(x0)))]
>>> NestList(lambda x:[x],1,5)
[[1], [[1]], [[[1]]], [[[[1]]]], [[[[[1]]]]]]"""
    r = f(x0)
    if n == 1:
        return [r]
    else:
        return [r]+NestList(f, r, n-1)


def Nest(f, x0, n):
    u"""Nest(f,3) -> f(f(f(x0)))
>>> Nest(lambda x:[x],1,5)
[[[[[1]]]]]"""
    if n == 1:
        return f(x0)
    else:
        return f(Nest(f, x0, n-1))


def Split(li, Ftst=None,key = None):
    u"""split seqvience [a,b,c,d,e...]in parts of same elements [[a,b,c],[d,e],...] Ftst(a,b)=1,Ftst(b,c)=1,Ftst(c,d)=0 ...
>>> Split([1,1,1,2,1,2,2,2,3,3,3])
[[1, 1, 1], [2], [1], [2, 2, 2], [3, 3, 3]]
>>> Split([1,1,1,2,1,2,2,2,5,5,5],lambda a,b:abs(a-b)<2)
[[1, 1, 1, 2, 1, 2, 2, 2], [5, 5, 5]]"""
    if Ftst is None:
        Ftst = lambda a, b: a == b
    if not key is None:
        Ftst = lambda a, b: key(a) == key(b)

    res = []
    blk = []
    for i in li:
        if len(blk) == 0:
            blk.append(i)
        else:
            if Ftst(blk[-1], i):
                blk.append(i)
            else:
                res.append(blk)
                blk = [i]
    if len(blk) != 0:
        res.append(blk)
    return res


def IterFlatten(iterable):
    u"""итератор по вложенным перечисляемым объектам
>>> [i for i in IterFlatten([1,34.5,2,3,[[],[6,7,{'a':'b','c':'d'}]]])]
[1, 34.5, 2, 3, 6, 7, 'a', 'c']"""
    if type(iterable) is np.matrix:
        iterable = np.array(iterable)
    if hasattr(iterable, "__iter__"):
        for seq in iterable:
            for e in IterFlatten(seq):
                yield e
    else:
        yield iterable


def Flatten(iterable, level=None):
    u"""удаление внутренней структуры у списков
>>> Flatten([1,34.5,2,3,[[],[6,7,{'a':'b','c':'d'}]]])
[1, 34.5, 2, 3, 6, 7, 'a', 'c']"""
    return list(IterFlatten(iterable))


def NSplit(li, form):
    u"""разбиение списка на куски заданной длины если
    список длинее то остаток тоже выделяется в кусочек
    если
>>> NSplit(range(10),[2,2,6])
[[0, 1], [2, 3], [4, 5, 6, 7, 8, 9]]
>>> NSplit(range(10),[2,2])
[[0, 1], [2, 3], [4, 5, 6, 7, 8, 9]]
"""
    delta = len(li)-sum(form)
    f = form if delta == 0 else form+[delta]
    beg = 0
    res = []
    for i in f:
        res.append(li[beg:beg+i])
        beg += i
    return res


# from scipy.interpolate import interp1d
##
# def _TypTransform(obj,N,objname=None,parent=None):
##    """преобразование объекта с числами в объект с пустыми массивами, служебная для Struct2Splines"""
##    if hasattr(obj,"__dict__"):
##        for nm,v in obj.__dict__.iteritems():
##            _TypTransform(v,N,nm,obj)
##    elif hasattr(obj,"__len__"):
##        for i,v in enumerate(obj):
##            _TypTransform(v,N,i,obj)
##    else:
##        if type(objname) is type(0):
##            #parent=list(parent)
##            parent[objname]=np.zeros(N,dtype='f')
##        else:
##            setattr(parent,objname,np.zeros(N,dtype='f'))
##
# def _Array2List(obj):
##    """ищет в obj np.array и заменяет их на list вызываем только не для numpy array"""
##    if hasattr(obj,"__dict__"):
##        for nm,v in obj.__dict__.iteritems():
##            if type(v) is np.ndarray:
##                setattr(obj,nm,list(v))
##            else:
##                _Array2List(v)
##    elif hasattr(obj,"__len__"):
##        for i,v in enumerate(obj):
##            if type(v) is np.ndarray:
##                obj[i]=list(v)
##            else:
##                _Array2List(v)
# class Ta:
##    def __init__(self):
##        self.a=4
##        self.b=np.zeros(8)
# a=Ta()
##
# def _SetObj(obj,el,i,objname=None,parent=None,parentsrc=None):
##    """установка значенич по заданному индексу
# clone, объект в который устанавливаем значения
# el, откуда берем значения
# i, индекс
# objname имя объекта
# parent родители текущих объектов
##, служебная для Struct2Splines
##"""
##    if hasattr(obj,"__dict__"):
##        for nm,v in obj.__dict__.iteritems():
##            src=getattr(el,nm)
##            _SetObj(v,src,i,nm,obj,el)
##    elif hasattr(obj,"__len__") and not hasattr(obj,"shape"):
##        for ii,v in enumerate(obj):
##            src=el[ii]
##            _SetObj(v,src,i,ii,obj,el)
##    else:
##        try:
##            if type(objname) is type(0):
##                obj[i]=el
##            else:
##                obj[i]=el
##        except:
##            pass
##
##
# def _ToSplines(t,obj,N,objname=None,parent=None):
##    """преобразование массивов в сплайны, служебная для Struct2Splines"""
##    if hasattr(obj,"__dict__"):
##        for nm,v in obj.__dict__.iteritems():
##            _ToSplines(t,v,N,nm,obj)
##    elif hasattr(obj,"__len__") and not hasattr(obj,"shape"):
##        for i,v in enumerate(obj):
##            _ToSplines(t,v,N,i,obj)
##    else:
##        if type(objname) is type(0):
##            # элемент [ ... , ...]
##            parent[objname]=interp1d(t,parent[objname])
##        else:
##            # вещественное число
##            setattr(parent,objname,interp1d(t,getattr(parent,objname)))
##
# def Struct2Splines(tim,arr):
##    """На входе время и массив структур
# tim - время - на входе упорядоченый массив плавающих чисел
# arr - массив структур, структуры всех объектов совпадают со структурой первого элемента (тонее не меньше)
##на выходе структура из сплайнов по времени.
# from fold import Struct2Splines
# import numpy as np
##
##>>> class Tb(object):
##>>>     def __init__(self):
##>>>         self.a=5.
##>>>         self.b=6
##>>>
##>>> class Ta(object):
##>>>     def __init__(self):
##>>>         self.a=5.
##>>>         self.b=6
##>>>         self.c=Tb()
##>>>         self.d=[10,20,30]
##>>>
##>>> time=np.arange(10)
##>>> a=[Ta() for i in range(10)]
##>>> a[3].a=300
##>>> b=Struct2Splines(time,a)
##>>> print b.b([1.1,2.2,3.3])
##[ 6.  6.  6.]
##"""
##    _Array2List(arr)
##
##    clone=copy.deepcopy(arr[0])
##    _TypTransform(clone,len(arr))
##    for i,el in enumerate(arr):
##        _SetObj(clone,el,i)
##    #_ToSplines(tim,clone,len(arr))
##    return clone

def Swap(li, i, j):
    u"""пересавление в списке li местами элементов с индексами i,j
>>> Lswap(range(5),2,4)
[0, 1, 4, 3, 2]
"""
    tmp = li[i]
    li[i] = li[j]
    li[j] = tmp
    return li


def MoveUp(li, index):
    u"""в списке li элемент с индексом index двигаем к началу списка
>>> MoveUp(range(5),0)
[0, 1, 2, 3, 4]
>>> MoveUp(range(5),1)
[1, 0, 2, 3, 4]
"""
    if index > 0:
        Swap(li, index-1, index)
    return li


def MoveDn(li, index):
    u"""в списке li элемент с индексом index двигаем к концу списка
>>> MoveDn(range(5),0)
[1, 0, 2, 3, 4]
>>> MoveDn(range(5),4)
[0, 1, 2, 3, 4]
>>> MoveDn(range(5),3)
[0, 1, 2, 4, 3]
"""
    if index < len(li)-1:
        Swap(li, index, index+1)
    return li


def ISelect(li1, li2):
    return [li1[i] for i in li2]


def JoinLists(li):
    u"""быстрое слияние списка списков (flasten level 1)"""
    res = []
    for i in li:
        res.extend(i)
    return res


def ObjectIterator(obj, root="", level=None, mapbypoint=0):
    u"""итерирование подобъектов для данного объекта ! только для древовидных объектов - не проверяю ссылки
>>> class Ta:
>>>     def __init__(self):
>>>         self.a={"a":1,"b":2}
>>>         self.b=range(5)
>>> for i,v in ObjectIterator([Ta(),1,2,3,[4,{"a":1,"b":2}],5,"asd"],"xxx"):
>>>     print i,v
xxx[0].a[a] 1
xxx[0].a[b] 2
xxx[0].b[0] 0
xxx[0].b[1] 1
xxx[0].b[2] 2
xxx[0].b[3] 3
xxx[0].b[4] 4
xxx[1] 1
xxx[2] 2
xxx[3] 3
xxx[4][0] 4
xxx[4][1][a] 1
xxx[4][1][b] 2
xxx[5] 5
xxx[6] asd
"""
    if not level is None:
        if level == -1:
            return
        level -= 1
    if type(obj) is np.matrix:
        obj = np.array(obj)
    if hasattr(obj, "iteritems"):  # словари
        for key, val in obj.iteritems():
            if mapbypoint:
                for k1, v1 in ObjectIterator(val, "{root}.{key}".format(key=key, root=root), level=level, mapbypoint=mapbypoint):
                    yield k1, v1
            else:
                for k1, v1 in ObjectIterator(val, "{root}[{key}]".format(key=key, root=root), level=level, mapbypoint=mapbypoint):
                    yield k1, v1
    elif hasattr(obj, "__iter__"):  # списки те объекты с произвольным доступом
        for key, val in enumerate(obj):
            for k1, v1 in ObjectIterator(val, "{root}[{key}]".format(key=key, root=root), level=level, mapbypoint=mapbypoint):
                yield k1, v1
    elif hasattr(obj, "__dict__") or hasattr(obj, "_fields_"):  # классы
        if hasattr(obj, "__dict__"):
            for key, val in obj.__dict__.iteritems():
                for k1, v1 in ObjectIterator(val, "{root}.{key}".format(key=key, root=root), level=level, mapbypoint=mapbypoint):
                    yield k1, v1
        if hasattr(obj, "_fields_"):
            for key, val in obj._fields_.iteritems():
                for k1, v1 in ObjectIterator(val, "{root}.{key}".format(key=key, root=root), level=level, mapbypoint=mapbypoint):
                    yield k1, v1
    else:
        yield root, obj


def Transpose(li):
    u"""транспонирование списка"""
    n = len(li[0])
    return [[row[i] for row in li] for i in range(n)]


class seq2dict(object):
    u"""преобразователь поледовательности данных в словарик ключ - значение
    >>> a=seq2dict("a b c")
    >>> print a(1,2,3)
    {'a': 1, 'b': 2, 'c': 3}"""
    def __init__(self, names):
        self.names = names.split()

    def __call__(self, *arg):
        return dict(zip(self.names, arg))

######################################################################


def NpIteritems(obj):
    u"""итерирование подобъектов для данного объекта ! только для древовидных объектов - не проверяю ссылки
"""
    if type(obj) is np.matrix:
        obj = np.array(obj)
    if hasattr(obj, "iteritems"):  # словари
        for key, val in obj.iteritems():
            for k1, v1 in NpIteritems(val):
                yield [key]+k1, v1
    elif hasattr(obj, "shape"):  # это np array возвращаем как есть
        yield [], obj
    elif hasattr(obj, "__iter__"):  # списки те объекты с произвольным доступом
        for key, val in enumerate(obj):
            for k1, v1 in NpIteritems(val):
                yield [key]+k1, v1
    elif hasattr(obj, "__dict__") or hasattr(obj, "_fields_"):  # классы
        if hasattr(obj, "__dict__"):
            for key, val in obj.__dict__.iteritems():
                for k1, v1 in NpIteritems(val):
                    yield k1, v1
        if hasattr(obj, "_fields_"):
            for key, val in obj._fields_.iteritems():
                for k1, v1 in NpIteritems(val):
                    yield k1, v1
    else:
        yield [], obj


def NpClone(obj, l):
    u"""итерирование подобъектов для данного объекта ! только для древовидных объектов - не проверяю ссылки
"""
    if type(obj) is np.matrix:
        obj = np.array(obj)
    if isinstance(l, int):
        l = (l,)
    if hasattr(obj, "iteritems"):  # словари
        return dict([(key, NpClone(v, l)) for key, v in obj.iteritems()])
    elif hasattr(obj, "shape"):  # это np array возвращаем как есть
        return np.zeros(l+obj.shape, dtype=obj.dtype)
    elif hasattr(obj, "__iter__"):  # списки те объекты с произвольным доступом
        return [NpClone(v, l) for v in obj]
    elif hasattr(obj, "__dict__") or hasattr(obj, "_fields_"):  # классы
        if hasattr(obj, "__dict__"):
            return dict([(key, NpClone(v, l)) for key, v in obj.__dict__.iteritems()])
        if hasattr(obj, "_fields_"):
            return dict([(key, NpClone(v, l)) for key, v in obj._fields_.iteritems()])
    else:
        dt = 'f'
        if isinstance(obj, int):
            dt = 'i'
        return np.zeros(l, dtype=dt)


def NpIter(obj):
    u"""итерирование подобъектов для данного объекта ! только для древовидных объектов - не проверяю ссылки
"""
    if hasattr(obj, "iteritems"):  # словари
        for key, val in obj.iteritems():
            for v1 in NpIter(val):
                yield v1
    elif hasattr(obj, "shape"):  # это np array возвращаем как есть
        yield obj
    elif hasattr(obj, "__iter__"):  # списки те объекты с произвольным доступом
        for val in obj:
            for v1 in NpIter(val):
                yield v1
    elif hasattr(obj, "__dict__") or hasattr(obj, "_fields_"):  # классы
        if hasattr(obj, "__dict__"):
            for key, val in obj.__dict__.iteritems():
                for v1 in NpIter(val):
                    yield v1
        if hasattr(obj, "_fields_"):
            for key, val in obj._fields_.iteritems():
                for v1 in NpIter(val):
                    yield v1
    else:
        yield obj


def Part(obj, ind):
    u"""выбор части объекта по набору индексов"""
    s = obj
    for i in ind:
        s = s[i]
    return s


def NpTranspose(obj):
    """транспонирование данных - превращение их в набор временных рядов"""
    l = len(obj)
    cln = NpClone(obj[0], l)
    spl = list(NpIter(cln))
    for (i, tobj) in enumerate(obj):
        for a, b in zip(spl, NpIter(tobj)):
            if isinstance(b, np.ndarray):
                a[i, ::] = b[::]
            else:
                a[i] = b
    return cln


def Partition(lst, siz):
    """Разбиение списка на куски равной длины размером siz"""
    return [lst[i:i+siz] for i in range(0, len(lst), siz)]

def fmap(obj,f=None,g=None):
    u"""map function to any level
>>> fmap([1,2,[{3:1,4:"asd"},4]],lambda x:x*2)
[2, 4, [{3: 2, 4: 'asdasd'}, 8]]    
    """
    if hasattr(obj,"shape"):
        return obj.tolist()
    if hasattr(obj,"iteritems"):
        if g:
            return dict([(g(k),fmap(v,f,g)) for k,v in obj.iteritems()])
        else:
            return dict([(k,fmap(v,f,g)) for k,v in obj.iteritems()])
    if hasattr(obj,"__iter__"):
        return [fmap(i,f,g) for i in obj]
    if f:
        return f(obj)
    else:
        return obj

def conv_val(v):
    u"""simplify values
>>> print type(conv_val('1')),type(conv_val('1.1')),type(conv_val('aaa')),type(conv_val(u'aaa')),type(conv_val(u'фыв'))
<type 'int'> <type 'float'> <type 'str'> <type 'str'> <type 'unicode'>
"""
    try:
        vv=int(v)
    except:
        vv=v
        try:
            vv=float(v)
        except:
            try:
                vv=str(v)
            except:
                vv=v
    return vv



def mapf(obj,fun):
    """делает объект такой-же как дали но применяет к элементам заданную функцию"""
    if type(obj) is list:
        return [mapf(i,fun) for i in obj]
    elif type(obj) is dict:
        return dict([(k,mapf(i,fun)) for k,i in obj.items()])
    else:
        return fun(obj)

def test_mapf():
    obj = [["a","b",["d"],["df"]],"c",{"a":4}]
    res= mapf(obj,lambda x:x*2)
    print(res)
