#! /bin/env/python
# -*- coding:utf-8 -*-
u"""manage tasks in text files"""

import argparse
import yaml
import os
import subprocess as sp
import datetime
import codecs


if not os.path.exists("cfg.yaml"):
    with open("done.yaml","w") as f:
        yaml.dump_all(dict(editor="notepad.exe",tags=[]),f,allow_unicode=1,default_flow_style = False)

if not os.path.exists("planned.yaml"):
    open("planned.yaml","w")

if not os.path.exists("done.yaml"):
    open("done.yaml","w")

with open("cfg.yaml","r") as f:
    cfg = yaml.load(f)

def remove_done(data):
    l_done=[]
    l_plan=[]
    for i in data:
        if "done" in i and i["done"]>=1:
            l_done.append(i)
        else:
            l_plan.append(i)
    tend=datetime.datetime.now()
    for i in l_done:
        i["tend"]=tend
    nm = "done.yaml"
    with codecs.open(nm,"a",encoding="utf-8") as f:
        yaml.dump_all(l_done,f,allow_unicode=1,default_flow_style = False)
    return l_plan


def edit_task_list(add_new_task):
    u""" edit new task or task list"""
    tc=datetime.datetime.now()
    nm = "planned.yaml"
    if os.path.exists(nm):
        with codecs.open(nm,"r",encoding="utf-8") as f:
            data=list(yaml.load_all(f))
    else:
        data=[]
    if add_new_task:
        data+=[dict(dsc="",nm="",tags=cfg["tags"],t=tc,done=0,deadline=tc+datetime.timedelta(1))]
    with codecs.open(nm,"w",encoding="utf-8") as f:
        yaml.dump_all(data,f,allow_unicode=1,default_flow_style = False)
    dkt=dict([(i["nm"],i) for i in data])# создали словарик чтобы легче было обрабатывать
    sp.check_call("{0} {1}".format(cfg["editor"],nm))

    with codecs.open(nm,"r",encoding="utf-8") as f:
        data1=list(yaml.load_all(f))

    data1 = remove_done(data1)
    for el_new in data1:
        key=el_new["nm"]
        if not key in dkt:
            continue

    with codecs.open(nm,"w",encoding="utf-8") as f:
        yaml.dump_all(data1,f,allow_unicode=1,default_flow_style = False)


def list_tasks():
    nm = "planned.yaml"
    with codecs.open(nm,"r",encoding="utf-8") as f:
        data=list(yaml.load_all(f))
    n=cfg["nitem"]
    if cfg["sort"] == "unsorted":
        li=data[:n]
    else:
        key=cfg["sort"]
        data.sort(lambda x:x[key])
        li=data[:n]
    nm="current.yaml"
    with codecs.open(nm,"w",encoding="utf-8") as f:
        yaml.dump_all(li,f,allow_unicode=1,default_flow_style = False)
    sp.check_call("{0} {1}".format(cfg["editor"],nm))
    os.remove(nm)

# интерфейс для консоли
if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="tm",description="manage tasklist")
    parser.add_argument("-n","--new",action='store_true', help=u"create new task")
    parser.add_argument("-e","--edit",action='store_true', help=u"edit task list")
    parser.add_argument("-l","--list",action='store_true',help=u"list tasks acoding to options")
    args = parser.parse_args()

    if args.edit or args.new:
        edit_task_list(args.new)
    if args.list:
        list_tasks()
