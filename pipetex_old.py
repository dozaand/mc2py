#!/usr/bin/env python
# -*- coding: cp1251 -*-

import os
import sys
import re
from mc2py.util import newcwd


def PipeTex(nm):
    u""" pipeout tex code expanding input statements"""
    fullnm = os.path.abspath(nm)
    dr, fl = os.path.split(fullnm)
    s = open(nm, "rb").read()
    with newcwd(dr):
        p0 = 0
        for g in re.finditer(r"\\input{(.+)}", s):
            p1 = g.start()
            yield s[p0:p1-1]
            p0 = g.end()
            filename = g.group(1)+".tex"
            for i in PipeTex(filename):
                yield i
    yield s[p0:]

if __name__ == '__main__':
    for i in PipeTex(sys.argv[1]):
        sys.stdout.write(i)
