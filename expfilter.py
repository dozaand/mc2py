#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
���������������� �������
"""

import math
import numpy as np


class ExpFilter(object):
    u"""������ ������� ������ ���� A E** lambda*t"""
    def __init__(self, n, dt, A, lam):
        self.__n = n
        self.__dt = dt
        self.__A = A
        self.__lam = lam
        self.__Q = []
        self.__s = None
        self.k = math.exp(lam*dt)
        self.ek = 1./self.k
        self.__s = None

    def __call__(self, x=None):
        u"""���������� ������"""
        if not x:
            return self.__s
        Q = self.__Q
        Q.append(x)
        L = len(Q)
        if L == 1:
            self.__s = A*x
            self.kk = A
        else:
            self.kk *= self.k
            self.__s += self.kk*x
        if L > self.__n:
            y = Q.pop()
            self.__s -= self.__A*y
            self.__s *= self.ek
        return self.__s


def Test1()
    import profile
    x = np.arange(0, 10, 0.1)
    f = np.sin(x)
    n = 5
    sh = np.exp(x[:n])
    conv = np.convolve(f, sh, mode='same')
    profile.run("conv=np.convolve(f,sh,mode='same')")
    from pylab import plot, show
    plot(x, f, x, conv, "c--")
    show()

if __name__ == '__main__':
    Test1()
