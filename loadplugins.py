#!/usr/bin/env python
# -*- coding: cp1251 -*-
"""
 ������ ������������ ��� �������� ��������
 � ���������� ���������� ������ ��������
 [ {"name":���������� ���,"exe": ����������� ������,"help":"�������� ������"},
   {"name":���������� ���,"items": [������ �������],"help":"�������� ������"},
 ]

���� ������� ������ ��������� ����:
 * menuname - ��� �������
 * menu     - �������� ����
 * __doc__  - ������������ �� �������
"""

import os
import glob
import sys
import codecs
import argparse
import subprocess as sp
from mc2py.util import newcwd
import threading
import datetime
import re


def LoadPlugins(directory):
    u"""�������� �������� �� �������� ����������"""
    sys.path.append(directory)
    res = []
    for nm in glob.glob(directory+"/*.py"):
        (plgdir, fil) = os.path.split(nm)
        (filnm, ext) = os.path.splitext(fil)
        mod = __import__(filnm, globals(), locals(), [], -1)
        if hasattr(mod, "menuname") and hasattr(mod, "menu") and hasattr(mod, "__doc__"):
            res.append({
                       "name": mod.menuname, "items": mod.menu, "help": mod.__doc__})
    sys.path.pop()
    return res

###################### ��� ������ � wx python ######################
import wx


def Plugin2Menu(frame, plg):
    u"""������ �� ������ �������� - ����������� ����"""
    menu = wx.Menu()
    for item in plg:
        if "items" in item:
            submenu = Plugin2Menu(frame, item["items"])
            menu.AppendMenu(wx.ID_ANY, item["name"], submenu)
        else:
            menuitem = menu.Append(wx.ID_ANY, item["name"], item["help"])
#            f=item['exe']
#            fun=lambda event:f()
            frame.Bind(wx.EVT_MENU, item['exe'], menuitem)
    return menu


def Add2MenuBar(frame, bar, plg):
    u"""��������� ������� �� �������� � ���� ���"""
    for item in plg:
        mnu = Plugin2Menu(frame, item["items"])
        bar.Append(mnu, item["name"])


class PluginFrame (wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"������ ��������", pos=wx.DefaultPosition, size=wx.Size(
            500, 300), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        self.m_panel = wx.Panel(
            self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer1.Add(self.m_panel, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(bSizer1)
        self.Layout()
        self.m_menubar = wx.MenuBar(0)
        self.SetMenuBar(self.m_menubar)

        self.m_statusBar1 = self.CreateStatusBar(1, wx.ST_SIZEGRIP, wx.ID_ANY)

        self.Centre(wx.BOTH)

        def __del__(self):
            pass


def SetStatus(event, msg):
    u"""��������� ������� � �������� ����"""
    if hasattr(event, "EventObject"):
        event.EventObject.SetStatusText(msg)
    else:
        print msg


class bat(object):
    u"""�������� ������� - ������� ����������� �������� ��������� ���� - ���������� �������� ������������� � ������������
          cmd,           - ����� ����������� �������
          msgOk=None,    - ��������� � ������ ������
          msgFail=None,  - ��������� � ������ �������
          shell=1,       - ����� ���������� � ��������� �������
          thread=0       - ����� ���������� � ��������� �����
          workdir        - �� ����� ������� ���������� �������� ����������
     ������ �������������:
        a=bat("dir >a\n cat a")
        a()
     !!! �������� !!! - ��� bat ���� - �� ��� UNIX �� ������"""
    def __init__(self, cmd, msgOk=None, msgFail=None, shell=1, thread=0, workdir=None):
        u"""
         �������� bat ��������

         * cmd,           - ����� ����������� �������
         * msgOk=None,    - ��������� � ������ ������
         * msgFail=None,  - ��������� � ������ �������
         * shell=1,       - ����� ���������� � ��������� �������
         * thread=0       - ����� ���������� � ��������� �����
         * workdir        - �� ����� ������� ���������� �������� ����������

         ����������� ����� ������������� � ����
         "exe":bat("scons -f SConstruct.py",workdir=r"."),

>>> a=bat("dir >a\\ndir >b",msgOk=u"all Ok")
>>> a()
all Ok
"""
        self.__dict__.update(locals())
        delattr(self, 'self')

    def DoCommand(self, event, filename):
        sp.check_call(filename, shell=self.shell)
        os.remove(filename)
        if self.msgOk:
            SetStatus(event, self.msgOk)

    def __call__(self, event=None):
        try:
#            filename=os.tmpnam()
            with newcwd(self.workdir):
                filename = "f"+re.sub(r"[\.:\- ]", "_", str(
                    datetime.datetime.now()))+".bat"
                with codecs.open(filename, "w") as f:
                    f.write(self.cmd)
                if self.thread:
                    thrd = threading.Thread(
                        target=self.DoCommand, args=(event, filename))
                    thrd.start()
                else:
                    self.DoCommand(event, filename)
        except sp.CalledProcessError:
            if self.msgFail:
                SetStatus(event, self.msgFail)
        finally:
            try:
                os.remove(filename)
            except:
                pass


class Start(object):
    u"""
    ������ ������� � ��������� ��������
    ����������� ����� ������������� � ����
    "exe":Start("notepad",msgOk=u"��� ������",msgFail=u"�� ������� ���������"),
"""
    def __init__(self, cmd, msgOk=None, msgFail=None, workdir=None):
        self.__dict__.update(locals())
        delattr(self, 'self')

    def Stcmd(self, event):
        sp.Popen(self.cmd)
        if self.msgOk:
            SetStatus(event, self.msgOk)

    def __call__(self, event=None):
        try:
            if self.workdir:
                with newcwd(self.workdir):
                    self.Stcmd(event)
            else:
                self.Stcmd(event)
        except sp.CalledProcessError:
            if self.msgFail:
                SetStatus(event, self.msgFail)


def PlgWindow(dirlist):
    u"""�������� ������ ����������� ���� ��������
    dirlist - ������ ���������� � ���������"""
    if "app" not in globals():
        global app
        app = wx.PySimpleApp(redirect=0)
    frame = PluginFrame(None)
    bar = wx.MenuBar(0)
    for d in dirlist:
        res = LoadPlugins(d)
        Add2MenuBar(frame, bar, res)
    frame.SetMenuBar(bar)
    frame.Show(True)
    app.MainLoop()

# b=bat("dir >a\npause",thread=1)
# b()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="loadplugins - create menu for plugins directory's")
    parser.add_argument(
        "dir", nargs="+", help=u"���������� � ������� ��������� �������")
    args = parser.parse_args()
    PlgWindow(args.dir)
