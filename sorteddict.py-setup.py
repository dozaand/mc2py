#!/usr/bin/env python
# Copyright (c) 2007 Qtrac Ltd. All rights reserved.
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.

from distutils.core import setup

setup(name='sorteddict',
      version='1.2.1',
      author="Mark Summerfield",
      author_email="mark@qtrac.eu",
      url="http://www.qtrac.eu",
      download_url="http://www.qtrac.eu/sorteddict.py",
      classifiers=[
      "Development Status :: 4 - Beta",
      "Intended Audience :: Developers",
      "License :: OSI Approved :: GNU General Public License (GPL)",
      "Natural Language :: English",
      "Operating System :: OS Independent",
      "Programming Language :: Python",
      "Topic :: Software Development :: Libraries :: Python Modules",
      ],
      py_modules=['sorteddict'],
      platforms=["Any"],
      license="GPL v 3",
      keywords=["sorted", "ordered", "dict", "data structure"],
      description="A dictionary that is sorted by key or by the given cmp or key function",
      long_description="""
Provides a dictionary with the same methods and behavior as a standard
dict and that can be used as a drop-in replacement for a dict (apart
from the constructor), but which always returns iterators and lists
(whether of keys or values) in sorted order. It does not matter when
items are inserted or when removed, the items in the sorteddict are
always returned in sorted order. The ordering is implicitly based on the
key's __lt__() (or failing that __cmp__()) method if no cmp or key
function is given.

The main benefit of sorteddicts is that you never have to explicitly
sort.

This particular implementation has reasonable performance if the pattern
of use is: lots of edits, lots of lookups, ..., but gives its worst
performance if the pattern of use is: edit, lookup, edit, lookup, ...,
in which case using a plain dict and sorted() will probably be better.
"""
      )
