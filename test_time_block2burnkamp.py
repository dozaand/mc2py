#!/usr/bin/env
# -*- coding: utf-8 -*-

from nose.tools import raises
from time_block2burnkamp import get_kamp_burn
import datetime

def test_time_block1():
    u"""начальная точка"""
    kamp,burn = get_kamp_burn("xipi.h5","kaln","b02",datetime.datetime(1991,6,2,0,0,0))
    assert(kamp==u"k05")
    assert(burn<0.1)

def test_time_block2():
    u"""точка в середине"""
    kamp,burn = get_kamp_burn("xipi.h5","kaln","b02",datetime.datetime(1992,6,11,2,23,1))
    assert(kamp==u"k05")
    assert(abs(burn-300)<1)


@raises(ValueError)
def test_time_block3():
    u"""точка в момент перегрузки с кампании на кампанию"""
    kamp,burn = get_kamp_burn("xipi.h5","kaln","b02",datetime.datetime(1992,6,11,3,23,1))
    assert(kamp==u"k05")
    assert(abs(burn-300)<1)

@raises(IOError)
def test_time_block4():
    u"""не найден файл"""
    kamp,burn = get_kamp_burn("xipi_some.h5","kaln","b02",datetime.datetime(1992,6,11,3,23,1))

@raises(KeyError)
def test_time_block5():
    u"""не найдена станция"""
    kamp,burn = get_kamp_burn("xipi.h5","kaln_","b02",datetime.datetime(1992,6,11,3,23,1))

@raises(KeyError)
def test_time_block6():
    u"""не найдена станция"""
    kamp,burn = get_kamp_burn("xipi.h5","kaln","b02_",datetime.datetime(1992,6,11,3,23,1))
