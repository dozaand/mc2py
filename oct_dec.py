#!/usr/bin/env python
# -*- coding: cp1251 -*-
#-------------------------------------------------------------------------


def YX2Oct(Y, X):
    u"""����������� ���� ��������� � ��� ��������"""
    try:
        s = "{:2o}{:2o}".format(Y,X)
    except:
        s = ""
    return s


def Oct2YX(octyyxx):
    u"""��� ��������  � ���� ���������"""
    sYX = str(octyyxx)
    if len(sYX) == 4:
        return int(sYX[:2], 8), int(sYX[2:], 8)
    else:
        return 0, 0

if __name__ == '__main__':
    Y=8
    X=8
    stra =YX2Oct(Y, X)
    print ("dec: {}-{}".format(Y,X), "oct: {}".format(stra) )

