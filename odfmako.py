#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
simple transformation of odt document
"""

import os
import yaml
from mako.template import Template
import zipfile as z
import argparse
import codecs

def odtmako(in_name,out_name,var):
    cont="content.xml"
    encoding="utf-8"
    with z.ZipFile(in_name,"r") as zi:
        data = zi.read(cont).decode(encoding)
        tpl=Template(data)
        res=tpl.render(**var)
        with z.ZipFile(out_name,"w") as zo:
            for zinfo in zi.infolist():
                name = zinfo.filename
                if name == cont:
                    zo.writestr(cont,res.encode(encoding))
                else:
                    zo.writestr(zinfo, zi.read(name))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="import unk macrosection library to h5 format file")
    parser.add_argument("-o","--out", help=u"output file")
    parser.add_argument("-v","--vars", help=u"yaml data file")
    parser.add_argument("input", help=u"input file")
    args = parser.parse_args()
    with codecs.open(args.vars,"r",encoding="utf-8") as f:
        var=yaml.safe_load(f)
    odtmako(args.input,args.out,var)
