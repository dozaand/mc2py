#!/usr/bin/env python3
"""
итераторы по проекту
"""
import re
from pathlib import Path
import logging


def spjiter(rtspj, masks, is_out=True, is_file=True,sys_ret=False):
    """
    итератор по файлам проекта описанного в spj файле
    :param rtspj - spj проекта
    :param masks - одна или несклько масок "*.for" , ["*.for","*.FOR"]
    :param is_out - вести поиск в разделе out
    :param is_file - вести поиск в разделе file
    :param sys_ret - вместе с файлом возвращать имя системы
    """
    rtspj=Path(rtspj)
    srcdir = rtspj.parent
    try:
#        print(f"==== try {rtspj}")
        with open(rtspj, "r", encoding="cp1251") as f:
            data = f.read()
        data = data.replace("\\","/")
        files=[]
        for i in re.findall("^file=(.+)\x1d",data,flags=re.M):
            try:
                files.append((srcdir/i).resolve())
            except Exception:
                logging.warning(f"fail on {i} {err}")

#        files = [(srcdir/i).resolve() for i in re.findall("^file=(.+)\x1d",data,flags=re.M)]
        outs=[]
        for i in re.findall("^out=(.+)\x1d(?![A-Za-z0-9]+:ffffffff:ffffffff:ffffffff)",data,flags=re.M):
            try:
                outs.append((srcdir/i).resolve())
            except Exception:
                logging.warning(f"fail on {i} {err}")
#        outs = [(srcdir/i).resolve() for i in re.findall("^out=(.+)\x1d(?!10:ffffffff:ffffffff:ffffffff)",data,flags=re.M)]
        # вывод системы
        if sys_ret:
            idfs = [i.stem.upper() for i in files if i.match("*.idf") or i.match("*.")]
            if idfs:
                sysname = idfs
            else:
                sysname = ""
        if not type(masks) is list:
            masks=[masks]
        if is_file:
            for file in files:
                for mask in masks:
                    if file.match(mask):
                        if sys_ret:
                            yield file,sysname
                        else:
                            yield file
                        break
                        
        if is_out:
            for file in outs:
                for mask in masks:
                    if file.match(mask):
                        if sys_ret:
                            yield file,sysname
                        else:
                            yield file
                        break
        for file in files:
            if file.match("*.spj") or file.match("*.SPJ"):
                yield from spjiter(file, masks, is_out, is_file,sys_ret)
    except FileNotFoundError as err:
        logging.warning(f"fail open spj {err}")


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(prog="spjiter",description="iterate over project file")
    parser.add_argument("-f","--no_files",action="store_true",help="do not scan file= records")
    parser.add_argument("-i","--idf_names",action="store_true",help="output idf for file")
    parser.add_argument("-o","--no_out",action="store_true",help="do not scan out= records")
    parser.add_argument("-s", "--file",help="spj to scan")
    parser.add_argument("masks",default=["*.*"],nargs="+",help="file masks to find *.for...")
    args = parser.parse_args()
    for i in spjiter(args.file, args.masks, not args.no_files, not args.no_out, args.idf_names):
        print(i)

