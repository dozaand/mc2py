#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        geom.py
# Purpose:     общие для всх станций геометрические данные
#
# Author:      and
#
# Created:     25.11.2010
# Copyright:   (c) and 2010
# Licence:     Gplv3
#-------------------------------------------------------------------------------
#!/usr/bin/env python
from mc2py.fold import Accumulate
import numpy as np
import re
import math
import codecs

koordtest = np.array([[1,2],[0,3],[1,2]])

koordwwer = np.array([[1, 7], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13], [0, 14],
                      [1, 14], [1, 15], [2, 15], [3, 15], [4, 15], [5, 15], [6, 15], [8, 14]])


# координаты граничных ячеек для ТВС КЛТ-40
koordklt40 = np.array([[1, 6], [0, 8],[0, 9], [0, 10], [0, 11], [0, 12],
                       [1, 12],
                       [1, 13], [2, 13], [3, 13], [4, 13], [5, 13], [7, 12] ])

# координаты Шельф  c отражателем
koord_shelf_r = np.array([[ 0, 12],
       [ 0, 13],
       [ 0, 14],
       [ 0, 15],
       [ 0, 16],
       [ 0, 17],
       [ 0, 18],
       [ 0, 19],
       [ 0, 20],
       [ 0, 21],
       [ 0, 22],
       [ 0, 23],
       [ 1, 23],
       [ 2, 23],
       [ 3, 23],
       [ 4, 23],
       [ 5, 23],
       [ 6, 23],
       [ 7, 23],
       [ 8, 23],
       [ 9, 23],
       [10, 23],
       [11, 23]])

# координаты Шельф без отражателя
koord_shelf = np.array([[3, 7], [1, 10], [1, 11], [0, 13], [0, 14], [0, 15], [0, 16], [1, 16], [1, 17], [1, 18], [2, 18], [3, 18], [3, 19], [4, 19], [5, 19], [6, 19], [8, 18], [9, 18], [12, 16]])

coord_rithm_core =np.array([[2, 7], [1, 9], [0, 11], [0, 12], [0, 13], [0, 14], [0, 15], [1, 15], [1, 16], [2, 16], [2, 17], [3, 17], [4, 17], [5, 17], [6, 17], [8, 16], [10, 15]])

# координаты брест ОД 300 активная зона
koord_brest = np.array([[ 0,  8],
       [ 0,  9],
       [ 0, 10],
       [ 0, 11],
       [ 0, 12],
       [ 0, 13],
       [ 0, 14],
       [ 0, 15],
       [ 1, 15],
       [ 2, 15],
       [ 3, 15],
       [ 4, 15],
       [ 5, 15],
       [ 6, 15],
       [ 7, 15]])

# координаты граничных ячеек для ТВС КЛТ-40
koordsvbr = np.array([[0, 5], [0, 6],[0, 7],  [0, 8],
                      [0, 9],
                       [1, 9], [2, 9], [3, 9], [4, 9] ])

# координаты граничных ячеек для ТВС ВВЭР-1000
koordwwer1000_tvs=np.array(
[
 [0, 11], [0, 12], [0, 13], [0, 14], [0, 15], [0, 16], [0, 17], [0, 18], [0, 19], [0, 20],
 [0, 21],
 [1, 21], [2, 21], [3, 21], [4, 21], [5, 21], [6, 21], [7, 21], [8, 21], [9, 21], [10, 21]
]
)

# координаты 1/6 ТВС для ТВС с ВВЭР-1000
koordwwer1000_66=np.array(
[
 #[0, 11], [0, 12], [0, 13], [0, 14], [0, 15], [0, 16], [0, 17], [0, 18], [0, 19], [0, 20],
 [10, 21],
 [11, 21], [12, 21], [13, 21], [14, 21], [15, 21], [16, 21], [17, 21], [18, 21], [19, 21], [20, 21]
]
)

# индексы ячеек пэл для ТВС ВВЭР-1000 начиная с 0
ind_pel_wwer1000_tvs=np.array([57,70,75,102,106,111,127,142,160,170,188,203,219,224,228,255,260,273])
# индекс центральной ячейки (индексация с 0)
ind_central_wwer1000_tvs = 165


# координаты записаны в октах, но при обращении система работает с ними как с нормальными dec
koordrbmk2488 = np.array(
    [[0o033, 0o045], [0o026, 0o052], [0o024, 0o054], [0o022, 0o056], [0o020, 0o060], [0o017, 0o061], [0o016, 0o062],
     [0o015, 0o063], [0o014, 0o064], [0o013, 0o065], [0o012, 0o066], [0o011, 0o067], [0o010, 0o070], [0o010, 0o070],
     [0o007, 0o071], [0o007, 0o071], [0o006, 0o072], [0o006, 0o072], [0o005, 0o073], [0o005, 0o073], [0o005, 0o073],
     [0o005, 0o073], [0o005, 0o073], [0o004, 0o074], [0o004, 0o074], [0o004, 0o074], [0o004, 0o074], [0o004, 0o074],
     [0o004, 0o074], [0o004, 0o074], [0o004, 0o074], [0o004, 0o074], [0o004, 0o074], [0o005, 0o073], [0o005, 0o073],
     [0o005, 0o073], [0o005, 0o073], [0o005, 0o073], [0o006, 0o072], [0o006, 0o072], [0o007, 0o071], [0o007, 0o071],
     [0o010, 0o070], [0o010, 0o070], [0o011, 0o067], [0o012, 0o066], [0o013, 0o065], [0o014, 0o064], [0o015, 0o063],
     [0o016, 0o062], [0o017, 0o061], [0o020, 0o060], [0o022, 0o056], [0o024, 0o054], [0o026, 0o052], [0o033, 0o045]])

# координаты записаны в октах, но при обращении система работает с ними как с нормальными dec
koordrbmk1884 = np.array(
    [[0o031, 0o047], [0o026, 0o052], [0o024, 0o054], [0o022, 0o056], [0o021, 0o057], [0o020, 0o060], [0o017, 0o061],
     [0o016, 0o062], [0o015, 0o063], [0o014, 0o064], [0o013, 0o065], [0o013, 0o065], [0o012, 0o066], [0o012, 0o066],
     [0o011, 0o067], [0o011, 0o067], [0o011, 0o067], [0o010, 0o070], [0o010, 0o070], [0o010, 0o070], [0o010, 0o070],
     [0o010, 0o070], [0o010, 0o070], [0o010, 0o070], [0o010, 0o070], [0o010, 0o070], [0o010, 0o070], [0o010, 0o070],
     [0o010, 0o070], [0o010, 0o070], [0o010, 0o070], [0o011, 0o067], [0o011, 0o067], [0o011, 0o067], [0o012, 0o066],
     [0o012, 0o066], [0o013, 0o065], [0o013, 0o065], [0o014, 0o064], [0o015, 0o063], [0o016, 0o062], [0o017, 0o061],
     [0o020, 0o060], [0o021, 0o057], [0o022, 0o056], [0o024, 0o054], [0o026, 0o052], [0o031, 0o047]])


koordwwer440 = [[4, 8],[2, 11],[1, 13],[1, 14],[0, 16],[0, 17],[0, 18],[0, 19],[1, 19],[1, 20],[1, 21],
                [2, 21],[2, 22],[3, 22],[4, 22],[4, 23],[5, 23],[6, 23],[7, 23],[9, 22],[10, 22],[12, 21],[15, 19]]

# координаты ТВС для PWR
koordpwr_tvs = [[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17],[0, 17]]

class TKartRowIter:
    u"""итератор по ряду картограммы старт ряда - l0"""
    def __init__(self, y, b, b1, e):
        self.y = y
        self.l0 = b1-b
        self.rg = range(b, e)

    def __iter__(self):
        return self.rg.__iter__()

        
class TPlane(object):
    u"""описание области путем задания границ"""
    def __init__(self, bounds, y0=0, symmetry=4, updn=0):
        self.bounds = np.array(bounds)
        self.symmetry = symmetry
        self.y0 = y0
        self._rowStarts = [0]+Accumulate([i[1]-i[
                                         0] for i in self.bounds])  # номера ячеек с которых начинаются ряды
        self.ntot = self._rowStarts.pop(-1)
        self.updn = updn

    def IterYX(self):
        u"""итератор точек картограммы"""
        y = self.y0
        for bnd in self.bounds:
            for x in range(*bnd):
                yield y, x
            y += 1

    def IterYXReverse(self):
        u"""обратный итератор точек картограммы"""
        y = len(self.bounds) - 1 + self.y0
        for bnd in self.bounds:
            for x in range(*bnd):
                yield y, x
            y -= 1

    def FIterYX(self):
        u"""итератор точек картограммы в плавающих координатах"""
        y = self.y0
        for bnd in self.bounds:
            for i in range(*bnd):
                fy, fx = self.FKoord(y, i)
                yield fy, fx
            y += 1

    def IterRow(self):
        u"""итератор рядов"""
        y = self.y0
        for y, i0, bnd in zip(range(self.y0, self.y0+len(self.bounds)), self._rowStarts, self.bounds):
            b, e = bnd
            b1 = i0
            yield TKartRowIter(y, b, b1, e)

    def IterYXL(self):
        u"""итератор точек картограммы и линейного адреса"""
        y = self.y0
        l = 0
        for bnd in self.bounds:
            for x in range(*bnd):
                yield y, x, l
                l += 1
            y += 1

    def IterBounds(self):
        u"""итератор граничных ячеек картограммы (у которых нет одного из соседей)"""
        if self.symmetry == 4:
            neib = [[-1, 0], [0, -1], [0, 0], [1, 0], [0, 1]]
        elif self.symmetry == 6:
            neib = [[-1, -1], [-1, 0], [0, -1], [0, 0], [1, 0], [0, 1], [1, 1]]
        for (y, x) in self.IterYX():
            cond = 1
            for (yy, xx)in neib:
                if not self.IsIn(y+yy, x+xx):
                    cond = 0
                    break
            if cond:
                yield y, x

    def Offset(self, y, x):
        u"""линейный сдвиг по заданным y,x: индекс снизу-вверх слева на право"""
        yrow = y-self.y0
        b = self.bounds[yrow]
        if x < b[0] or x >= b[1] or yrow < 0 or yrow >= len(self.bounds):
            raise IndexError("in row index (x) out of range")
        return self._rowStarts[yrow]+(x-b[0])

    def OffsetReverse(self, y, x):
        u"""обратный линейный сдвиг по заданным y,x: индекс сверху-вниз слева на право"""
        yrow = len(self.bounds) - y + self.y0 - 1
        b = self.bounds[yrow]
        if x < b[0] or x >= b[1] or yrow < 0 or yrow >= len(self.bounds):
            raise IndexError("in row index (x) out of range")
        return self._rowStarts[yrow]+(x-b[0])

    def R(self):
        if self.symmetry == 4:
            return 1./math.sqrt(2)
        elif self.symmetry == 6:
            return 1./math.sqrt(3)

    def FKoord(self, y, x):
        u"""определяем плавающие координаты ячейки"""
        if self.updn:
            if self.symmetry == 4:
                return -y, x
            elif self.symmetry == 6:
                return -y*0.8660254037844386, x-(y-self.y0)*0.5
        else:
            if self.symmetry == 4:
                return y, x
            elif self.symmetry == 6:
                return y*0.8660254037844386, x-(y-self.y0)*0.5

    def IsIn(self, y, x):
        u"""проверка принадлежности зоне y,x"""
        yrow = y-self.y0
        if yrow < 0 or yrow >= len(self.bounds):
            return False
        b = self.bounds[yrow]
        if x < b[0] or x >= b[1]:
            return False
        else:
            return True

    def YX(self, l):
        u"""по линйному сдвигу получаем координату"""
        if 0 <= l and l < self.ntot:
            # ищем номер ряда, который идет следом за рядом, в котором расположен наш индекс
            for irow, begRowIndex in enumerate(self._rowStarts):
                if begRowIndex > l:
                    break
            # если индекс расположен в последнем ряду, требуется дополнительная проверка,
            # чтобы увеличить индекс на 1 (на всякий случай используем для этого функцию len)
            if l >= self._rowStarts[-1]:
                irow = len(self._rowStarts)
            # выставляем номер ряда, в котором нахдится наш индекс
            irow = irow-1

            y = irow + self.y0
            sh = l-self._rowStarts[irow]
            x = self.bounds[irow][0]+sh
            return y, x
        else:
            raise IndexError("line offset index %s is out of range" % l)

    def YXReverse(self, l):
        u"""по обратному линйному сдвигу получаем координату"""
        if 0 <= l and l < self.ntot:
            # ищем номер ряда, который идет следом за рядом, в котором расположен наш индекс
            for irow, begRowIndex in enumerate(self._rowStarts):
                if begRowIndex > l:
                    break
            # если индекс расположен в последнем ряду, требуется дополнительная проверка,
            # чтобы увеличить индекс на 1 (на всякий случай используем для этого функцию len)
            if l >= self._rowStarts[-1]:
                irow = len(self._rowStarts)
            # выставляем номер ряда, в котором нахдится наш индекс
            irow = irow-1

            y = len(self.bounds) - irow + self.y0 - 1
            sh = l-self._rowStarts[irow]
            x = self.bounds[irow][0]+sh
            return y, x
        else:
            raise IndexError("line offset index %s is out of range" % l)

    def GetBorderRect(self):
        u"""получить координаты внешних границ картограммы: x0,y0,x1,y1 """
        y0 = self.y0
        y1 = self.y0 + len(self.bounds)
        x0 = min(self.bounds[:, 0])
        x1 = max(self.bounds[:, 1])
        return x0, y0, x1, y1

    def DataUpDn(self, inp):
        u"""переворот данных в картограмме вверх ногами (изменение порядка рядов на обратный)"""
        res = []
        rs = self._rowStarts+[self.ntot]
        for i in range(len(self.bounds), 0, -1):
            res.extend(inp[rs[i-1]:rs[i]])
        return res



def g_test():
    u"""геометрия тест"""
    return TPlane(koordtest,y0=0,symmetry=6)

def Wwer1000():
    u"""геометрия ВВЭР-1000"""
    return TPlane(koordwwer, y0=0, symmetry=6)

def Wwer440():
    u"""геометрия ВВЭР-440"""
    return TPlane(koordwwer440, y0=0, symmetry=6)

def Rbmk1884():
    u"""геометрия РБМК-1000 - топливо"""
    return TPlane(koordrbmk1884, y0=0o010, symmetry=4)

def Rbmk2488():
    u"""геометрия РБМК-1000 - топливо и отражатель"""
    return TPlane(koordrbmk2488, y0=0o04, symmetry=4)

def Klt40():
    u"""геометрия ТВС КЛТ-40"""
    return TPlane(koordklt40, y0=0, symmetry=6)

def Wwer1000_tvs():
    u"""геометрия твс ВВЭР-1000 - топливо"""
    return TPlane(koordwwer1000_tvs, y0=0, symmetry=6)

def Svbr_tvs():
    u"""геометрия твс SVBR - топливо"""
    return TPlane(koordsvbr, y0=0, symmetry=6)



class TVolume(object):
    def __init__(self, bounds, y0, nz, symmetry=4):
        self.plane = TPlane(bounds, y0, symmetry)
        self.nz = nz

    def IterZYX(self):
        u"""итератор точек картограммы"""
        for y, x in self.plane.IterYX():
            for z in range(self.nz):
                yield z, y, x

    def FIterZYX(self):
        u"""итератор точек картограммы в плавающих координатах"""
        for y, x in self.plane.FIterYX():
            for z in range(self.nz):
                yield float(z), y, x

    def IterZYXL(self):
        u"""итератор точек картограммы и линейного адреса"""
        l = 0
        for y, x in self.plane.IterYX():
            for z in range(self.nz):
                yield z, y, x, l
                l += 1

    def Offset(self, z, y, x):
        u"""линейный сдвиг по заданным z,y,x"""
        yx = self.plane.Offset(y, x)
        if z < 0 or z >= nz:
            raise IndexError("in row index (z) out of range")
        return yx*self.plane.ntot+z

    def __eq__(self, el):
        u"""сравнение картограмм на совпадение"""
        if self.symmetry != el.symmetry:
            return 0
        if self.y0 != el.y0:
            return 0
        if not np.all(self.bounds == el.bounds):
            return 0
        return 1


def neib6(gm):
    u"""соседи в шестиграннике"""
    mp = {}
    for y, x, l in gm.IterYXL():
        mp[(y, x)] = l
    res = []
    for y, x, l in gm.IterYXL():
        nl = []
        for dy, dx in [(1, 1), (-1, -1), (1, 0), (-1, 0), (0, 1), (0, -1)]:
            xx = x+dx
            yy = y+dy
            if (yy, xx) in mp:
                nl.append(mp[(yy, xx)])
        res.append(nl)
    return res


def neib4(gm):
    u"""соседи у квадрата"""
    mp = {}
    for y, x, l in gm.IterYXL():
        mp[(y, x)] = l
    res = []
    for y, x, l in gm.IterYXL():
        nl = []
        for dy, dx in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
            xx = x+dx
            yy = y+dy
            if (yy, xx) in mp:
                nl.append(mp[(yy, xx)])
        res.append(nl)
    return res

# 3
# форматированный ввод вывод


def _SplitEqvallyLine(lin):
    u"""пробуем разбить строку на равные кусочки - и определить смещение в единицах кусочков от начала строки
    возвращаем
    rowshift начало строки в длинах кусочков
    medlen средняя длина кусочка в символах
    objli список загруженных объектов"""
    objli = re.findall("[^ \t]+", lin)
    nitem = len(objli)
    shift = re.match("^([ ]+)", lin)
    if shift:
        nspaces = len(shift.group(0))
    else:
        nspaces = 0
    if nitem == 0:
        return 0, []
    medlen = round(float(len(lin)-nspaces)/nitem)
    rowshift = nspaces/medlen
    return rowshift, medlen, objli


def Text2Kart(data):
    u"""ws - размер поля для букв"""
    return list(map(_SplitEqvallyLine, filter(lambda x: len(x), data.splitlines())))


def Text2Geom(stri, dropstart=1):
    u"""преобразуем текст в геометрию и даные  dropstart - Не учитывать начальные пробелы
    возвращаем геометрию и данные"""
    data = Text2Kart(stri)
# выбираем тип геометрии
    lens = np.array([len(i[-1]) for i in data], dtype='i')
    if len(data) == 1:
        geompyp = 4
    elif abs(math.modf(abs(data[0][0]-data[1][0]))[0]-0.5) < 0.1:
        geompyp = 6
    else:
        geompyp = 4
    if geompyp == 4:
        starts = np.array([sh[0] for sh in data], dtype='i')
    else:
        starts = np.array([sh[
                          0]+0.5*i for i, sh in enumerate(data)], dtype='i')

    ends = starts+lens
    if dropstart:
        mi = min(starts)
        starts -= mi
        ends -= mi
    alldat = []
    for i in data:
        alldat += i[-1]
    koord = np.array([starts, ends]).T
    gm = TPlane(koord, y0=0, symmetry=geompyp)
    return gm, alldat


def File2Geom(nm, dropstart=1):
    u"""Переводим содержание файла в геометрию и данные"""
    with codecs.open(nm, "r", encoding = "utf-8") as f:
        s = f.read()
    return Text2Geom(s, dropstart)


def FormatKart(gm, data):
    u"""возвращаем форматированную картограмму из данных в виде строки"""
    strd = [str(i) for i in data]
    maxlen = max([len(i) for i in strd])+1
    rowlen = [j-i for i, j in gm.bounds]
    # делаем длину поля четной
    if maxlen & 1:
        maxlen += 1
    if gm.symmetry == 4:
        starts = np.array([i[0] for i in gm.bounds])
    else:
        starts = np.array([b[0]-0.5*i for i, b in enumerate(gm.bounds)])
    minpos = min(starts)
    starts -= minpos

    fmt = "%"+str(maxlen)+"s"
    s = ""
    j = 0
    for sh, n in zip(starts, rowlen):
        s += " "*(maxlen*sh)
        for i in range(n):
            s += fmt % strd[j]
            j += 1
        s += "\n"
    return s


def FormatKoord(koord, dat, symmetry=4):
    u"""форматирование данных в соответствии с геометрией"""
    sdat = map(unicode, dat)
    maxlen = max(map(len, sdat))
    reclen = maxlen+1

    if reclen & 1:
        reclen += 1
    reclen_2 = reclen/2
    fmt = "%{0}s".format(reclen)
    eqsdat = map(lambda x: fmt % x, sdat)
    res = []
    i = 0
    irow = 0
    nrow = len(koord)
    for b, e in koord:
        pref = u" "*b*(maxlen+1)
        if symmetry == 6:
            pref += u" "*(nrow-irow)*reclen_2
        di = e-b
        res.append(pref+u"".join(eqsdat[i:i+di]))
        i += di
        irow += 1
    return u"\n".join(res)


def To48x48(data, defClearValue=0):
    u"""переводим картограмму 2488 или 1884 в квадрат вокруг картограммы
       defClearValue оределяет значения, которым заполняем пустые ячейки вокруг картограммы"""
    if len(data) != 2488 and len(data) != 1884:
        raise ValueError

    if len(data) == 2488:
        gm = Rbmk2488()
    if len(data) == 1884:
        gm = Rbmk1884()

    x0, y0, x1, y1 = gm.GetBorderRect()
    x_end=x1 - x0
    y_end=y1 - y0
    mas = []
    # перебираем размеры квадратной картограммы
    for y in range(0, y_end):
        for x in range(0, x_end):
            # производим свдиг на y0,x0,
            # поскольку картограмма 2304 сдвинута на 8 по х и на 8 по y относительно начала координат
            YY = y + y0
            XX = x + x0
            # проверяем,  попадают ли они в нашу геометрию 2488 или 1884
            if gm.IsIn(YY, XX):
                # если попадают, получаем линейный сдвиг для массива входных данных (2488 или 1884)
                index = gm.OffsetReverse(YY, XX)
                val = data[index]
                mas.append(val)
            else:
                # если не попадают - заполняем 0
                mas.append(defClearValue)
    return mas


def To1884(data2304):
    u"""переводим картограмму 2304 (квадрат вокруг картограммы 1884) в картограмму 1884"""
    if len(data2304) != 2304:
        raise ValueError

    gm = Rbmk1884()
    mas1884 = np.zeros(1884)
    x0, y0, x1, y1 = gm.GetBorderRect()
    # перебираем размеры картограммы 48 на 48
    index = 0
    for y in range(0, 48):
        for x in range(0, 48):
            # производим свдиг, поскольку картограмма 2304 сдвинута на 8 по х и на 8 по y
            # относительно начала координат
            YY = y + y0
            XX = x + x0
            # проверяем,  попадают ли они в нашу геометрию 1884
            if gm.IsIn(YY, XX):
                # если попадают, получаем линейный сдвиг для массива выходных данных (1884)
                index1884 = gm.Offset(YY, XX)
                val = data2304[index]
                mas1884[index1884] = val
            index = index+1

    return mas1884

### тест на линейные сдвиги
# gm=Rbmk1884()
##
# print gm.Offset(067,034)            # 1873
# print gm.OffsetReverse(067,034)     # 3
# print gm.Offset(010,034)            # 3
# print gm.OffsetReverse(010,034)     # 1873
# print gm.YX(1873)                   # dec(55, 28) = oct(067,034)
# print gm.YXReverse(3)               # dec(55, 28) = oct(067,034)
# print gm.YX(3)                      # dec(8, 28)  = oct(010,034)
# print gm.YXReverse(1873)            # dec(8, 28)  = oct(010,034)




# from pylab import *
##
# gm=TPlane(koordwwer,symmetry=6)
# dataf=np.array([gm.FKoord(y,x) for (y,x) in  gm.IterYX()])
# data=np.array([gm.FKoord(y,x) for (y,x) in  gm.IterBounds()])
# plot(dataf[:,1],dataf[:,0],'+',data[:,1],data[:,0],'o')
##
# autoscale(enable=True, axis='both', tight=1)
# for y,x in gm.FIterYX():
##    text(x,y,"(%3.2f,%3.2f)"%(y,x),fontsize=9)
# plot(dataf[:,1],dataf[:,0],'+',data[:,1],data[:,0],'o')
##
# data=np.array([(y,x) for (y,x) in  gm.IterYX()])
# for row in gm.IterRow():
##    y=row.y
##    for x in row:
##        text(x,row.y,"(%s,%s,%s)"%(y,x,row.l0+x),fontsize=9)
# plot(data[:,1],data[:,0],'o')


# Массив координат X в зависимости от линейного индекса для ВВЭР-1000
yzxcrd=[
                      24, 26, 28, 30, 32, 34,
                21, 23, 25, 27, 29, 31, 33, 35, 37,
              20, 22, 24, 26, 28, 30, 32, 34, 36, 38,
            19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39,
          18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40,
        17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41,
      16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42,
        17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41,
      16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42,
        17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41,
          18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40,
            19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39,
              20, 22, 24, 26, 28, 30, 32, 34, 36, 38,
                21, 23, 25, 27, 29, 31, 33, 35, 37,
                      24, 26, 28, 30, 32, 34
]

# Массив координат Y в зависимости от линейного индекса  для ВВЭР-1000
yzycrd=[
                      1,  1,  1,  1,  1,  1,
                2,  2,  2,  2,  2,  2,  2,  2,  2,
              3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
            4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
          5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,
        6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,
      7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
        8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,
      9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,
        10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
          11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
            12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,
              13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
                14, 14, 14, 14, 14, 14, 14, 14, 14,
                      15, 15, 15, 15, 15, 15
]



def get_line_index_wwer1000(yy,xx):
    u"""получение по координате в гексоганальной сетке линейного индекса ТВС"""
    koord=[]
    for index in range(163):
        if yy== yzycrd[index] and xx == yzxcrd[index]:
            return index


#print get_line_index_wwer1000(yy=1, xx=24)
#print get_line_index_wwer1000(yy=15, xx=34)

















