#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""
������ �������� ������������ ������� �� �������
������������ ������������� ����������� �� ������ numpy
"""

import numpy as np
import cPickle
import math
from scipy.interpolate import splrep, splev, splint
from pylab import *


class TsplBlock:
    def __init__(self, t0, t1, spl):
        self.t0 = t0
        self.t1 = t1
        self.spl = spl


class TSmoothedInerpolator(object):
    u"""������������ �������
������������ ��� �������� ������ �� ������������
"""
    def __init__(self, npoints=10000, nowerlap=-1, eps=0, dtype='f', shape=-1):
        u"""
        file=None, ���� ���� ����� ������������ ������
        directory=None, ��� ����� ������������ - ������ ������ ����������
        npoints=10000, ���������� ����� ������� ������ � ���� ������
        nowerlap=-1 ������ ������� ���������� ���������� ������
        rms - ������������������ ������ �������������"""
        bufshape = (npoints,)+shape if hasattr(
            shape, "len") else (npoints, shape)
        self.inBuf = np.zeros(bufshape, dtype=dtype)
        self.inTime = np.zeros(npoints, dtype=dtype)
        self.pos = 0  # ��������� �� ��������� ��������� �������
        self.eps = eps
        self.blocks = []
        self.blockTimes = []
        if nowerlap < 0:
            nowerlap = int(0.1*npoints)
        self.nowerlap = nowerlap
        self.splcurr = None
        self._ndata = 0
        self._ncompress = 1

    def CompressLevel(self):
        return float(self._ndata)/self._ncompress

    def _PushBlock(self):
        u"""�������� ����������� ��� ������������ �����"""
        spl = [splrep(self.inTime, self.inBuf[
                      :, k], s=self.eps) for k in xrange(self.inBuf.shape[1])]
        for el in spl:
            self._ncompress += 2*len(el[0])
        nbnd = self.nowerlap
        self.blocks.append(spl)
        if not self.blockTimes:
            self.blockTimes.append(self.inTime[0])
        self.blockTimes.append(self.inTime[-nbnd])
        # ��������� ������
        self.inTime[:nbnd] = self.inTime[-nbnd:]
        self.inBuf[:-nbnd, :] = self.inBuf[nbnd:, :]
        self.pos = nbnd

    def append(self, t, data):
        u"""������ ������ ����� ������"""
        pos = self.pos
        self.inTime[pos] = t
        self.inBuf[pos, :] = data[:]
        self._ndata += len(data)+1
        if pos+1 == len(self.inTime):
            self._PushBlock()
        else:
            self.pos = pos+1
##    def Get(self,t,buf=None,index=[]):
##        """��������� ��������� �����"""
##        ind=
##        return buf
##
##    def __call__(self,t,buffer=None,index=[]):
##        """�� ����� ��� ���� ����� ��� ������
##        �� ������ ����� ��� ����� ��� �������� �� ������
##        ���� ����� ����� - �� ������ ����� ���������� � ����
##        """
##        if t<
##
##        pass

    def clear(self):
        self.blocks = []
        self.blockTimes = []
        self.pos = 0
        self._ndata = 0
        self._ncompress = 1

# n=50
# smi=TSmoothedInerpolator(shape=50,npoints=300,eps=3*10.)

for i in xrange(10000):
    v = np.random.rand(n)+0.01*float(i)/10000
    smi.append(i, v)
