#!/usr/bin/env python
# -*- coding: cp1251 -*-
u"""
 ������� ��� ��������� ������ �� �������
 ���������� ��������� scs ��� ������������ ������� �������������� ������� ������ � ��������
"""
try:
    import cPickle
except ImportError:
    import pickle as cPickle

import os
import json
import re
import datetime
import codecs
try:
    import ConfigParser
except ImportError:
    import configparser as ConfigParser


def sv(obj, p, encoding="cp1251"):
    u"""���������� ��������
    obj - ������ ��� ����������
    p - ��� �����, � ������� ����� �������� ������"""
    if os.path.splitext(p)[1] == '.json':
        with codecs.open(p, "w", encoding=encoding) as f:
            json.dump(obj, f, indent=1, ensure_ascii=0)
    else:
        with open(p, "wb") as f:
            cPickle.dump(obj, f, 2)


def GetLocalVar(code):
    exec(code)
    del code
    return locals()


def ld(p, encoding="cp1251"):
    u"""�������� ��������
    p - ��� �����, �� �������� ������������ �������� �������"""
    if os.path.splitext(p)[1] == '.json':
        with codecs.open(p, "r", encoding=encoding) as f:
            return json.load(f)
    elif os.path.splitext(p)[1] in ['.ini','.cfg']:
        cfg=ConfigParser.ConfigParser()
        cfg.read(fil)
        return dict([(sect,dict(cfg.items(sect))) for sect in cfg.sections()])
    elif os.path.splitext(p)[1] == '.py':
        try:
            with codecs.open(p, "r", encoding=encoding) as f:
                s = f.read().replace("\r\n", "\n")
                return GetLocalVar(s)
        except Exception as err:
            print("error read %s" % p)
            raise IOError("error read %s, fail:%s" % (p, err.message))

    else:
        with open(p, "rb") as f:
            return cPickle.load(f)

def ldu(p):
    return ld(p,encoding="utf-8")


def scs(fun):
    u"""Scons ��������� ������ """
    def Sub(target=None,  source=None,  env=None):
        li = [ld(i.abspath) for i in source]
        res = fun(li, env)
        if len(target) == 1:
            sv(res, target[0].abspath)
        else:
            for r, t in zip(res, target):
                sv(r, t.abspath)
    return Sub


def scrun(target=None,  source=None,  env=None, action=None):
    u"""������ ������ ������ �� scons
    target - ������ ����� � �����
    source - ������ ����� � ���������� ������
    env - �������� ����� ���������
    action - ����������� ��������"""
    class TFile:
        def __init__(self, nm):
            self.abspath = nm
    action(map(TFile, target), map(TFile, source), env)

#@scs
# def dosome(src,env):
#    return src[0]


def SplitPath(nm):
    u"""������� ���� �� 3 �����"""
    base, fil = os.path.split(nm)
    f, ext = os.path.splitext(fil)
    return base, f, ext


def JoinPath(dirnm, basename, ext):
    u"""���������� ���� �� 3 ������"""
    return os.path.join(dirnm, basename+ext)


class TNmGen(object):
    u"""����� � �������� ������� ��� ����� � ����� � ������ ����� � ������� ������� prefix,extension,base"""
    def __init__(self, filename):
        self.dir, fil = os.path.split(filename)
        fil, self.ext = os.path.splitext(fil)
        res = re.match("(.+?)_(.+)", fil)
        if res:
            self.prefix = res.group(1)
            self.base = res.group(2)

    def __call__(self, **kvarg):
        a = {}
        a.update(self.__dict__)
        a.update(kvarg)
        return os.path.join(a["dir"], "{prefix}_{base}{ext}".format(**a))

    def File(self, **kvarg):
        a = {}
        a.update(self.__dict__)
        a.update(kvarg)
        return "{prefix}_{base}{ext}".format(**a)


def Time2FileName(t=datetime.datetime.utcnow()):
    u"""����������� ������� ����� � ��� � ��������� ���"""
    return re.sub("[ \-:\.]", "_", str(t))


def dt2sec(dt):
    u"""��������� ����� � �������"""
    return dt.days*(3600*24)+dt.seconds
